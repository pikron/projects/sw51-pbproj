#   Project file pro spektrofotometr LCD4000
#         (C) Pisoft 1991

sf_a.obj  : sf_a.asm sf_adr.h sf_tty.h
	a51 sf_a.asm $(par)

sf_hw.obj : sf_hw.asm sf_adr.h sf_tty.h
	a51 sf_hw.asm $(par)

sf_tty.obj    : sf_tty.asm sf_adr.h sf_tty.h
	a51 sf_tty.asm $(par)

sf_ai.obj : sf_ai.asm sf_tty.h
	a51 sf_ai.asm $(par)

sf_uf.obj : sf_uf.asm sf_tty.h sf_ai.h
	a51 sf_uf.asm $(par)

sf_monit.obj : sf_monit.asm sf_tty.h sf_ai.h sf_adr.h
	a51    sf_monit.asm $(par)

ulan.obj     : ulan.asm
	a51    ulan.asm $(par)

sf_lan.obj: sf_lan.asm sf_adr.h sf_ai.h
	a51 sf_lan.asm $(par)

sf.obj    : sf.asm sf_adr.h sf_tty.h sf_ai.h sf_uf.h ulan.h
	a51 sf.asm

sf.       : sf.obj sf_a.obj sf_hw.obj sf_tty.obj sf_ai.obj sf_uf.obj sf_lan.obj ulan.obj sf_monit.obj
	l51 sf.obj,sf_a.obj,sf_hw.obj,sf_tty.obj,sf_ai.obj,sf_uf.obj,sf_lan.obj,ulan.obj,sf_monit.obj xdata(8000H) ixref

sf.hex    : sf.
	ohs51 sf
	eprem sf.hex