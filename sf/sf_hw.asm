;********************************************************************
;*                    LCD 4000 - SF_HW.ASM                          *
;*     Rutiny LCD4000 vstupu z ADC a vystupu na DAC                 *
;*                  Stav ke dni 22.11.1991                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

	   PUBLIC  RES_STAR
	   PUBLIC  INADC,OUTDAC,INADC1,DAC_INIT
	   PUBLIC  GLO_AUFS,ADC_ACH,ITIM_R,AUXUAL

	   EXTRN   DATA (AKUM,AKUM1)
	   EXTRN   CODE (START,LOADA,POSP,NEGREG)
	   EXTRN   CODE (I_TIME,SFABS,INADCD,LAN_TM)
	   EXTRN   BIT  (U_TIME,U_WLEN,U_AUX,U_ABS)
	   EXTRN   XDATA(ABSFL,SCNFL)

	   PUBLIC  S_W_LEN,S_W_LE0,I_W_LEN,R_W_LEN,F_W_LEN,A_W_LEN
	   PUBLIC  SF_INIT,S_LAMP,S_AUX,TIME,KBDTIMR,TIMR3,TIMR_WAIT
	   PUBLIC  ALRM_BB,PR_ENDC,PR_ENDD,NRE_LEN,uLE_ABS,SNC_ERR

$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_ADR)
$INCLUDE(%INCH_TTY)
$LIST

SF_SRAM SEGMENT XDATA
SF_HW_C SEGMENT CODE
SF_HW_X SEGMENT XDATA
SF_HW_B SEGMENT DATA BITADDRESSABLE

%SVECTOR (RESET,RES_STAR)

%SVECTOR (TIMER0,WLENGHT)
%SVECTOR (EXTI0,I_TIME0)
%SVECTOR (EXTI1,I_TIME1)

RSEG    SF_HW_B

HW_FLG: DS    1
ITIM_DF BIT   HW_FLG.7
ITIM_RF BIT   HW_FLG.6
ALRM_BB BIT   HW_FLG.5
PR_ENDC BIT   HW_FLG.4
PR_ENDD BIT   HW_FLG.3
uLE_ABS BIT   HW_FLG.2

SNC_ERR BIT   EX1  ; Chyba pri synchronizaci se sitovou frekvenci

RSEG    SF_SRAM

RAM_T_B:DS    1
CHEK_SM:DS    1
BEG_SRAM:

END_SRAM:

RSEG    SF_HW_X

N_OF_T  EQU   5    ; Pocet timeru
TIMR1:  DS    1    ; Timery jsou decrementovany
TIMR2:  DS    1    ; s frekvenci 25 Hz
TIMR_WAIT:
TIMR3:  DS    1
KBDTIMR:DS    1
TIMRI:  DS    1
TIME:   DS    2    ; Cas v 0.01 min              =====
GLO_AUFS:DS   1
ADC_ACH:DS    1

R_W_LEN:DS    2

AUXUAL :DS    2

R_LAMPS:DS    2

L_DELAY:DS    1

RSEG    SF_HW_C

; Inicializace hardware
; =====================

RES_STAR:CLR  EA              ; Zacatek programu
	MOV   R2,#1
STARTW: DJNZ  R0,STARTW
	DJNZ  R1,STARTW
	DJNZ  R2,STARTW
	MOV   SP,#048H
;                    1        - seriovy stik
SF_INIT:MOV   IE,#00000001B   ; zakaz preruseni
	MOV   TMOD,#00100001B ; citac 1 mod 2; citac 0 mod 1
	MOV   TCON,#01010101B ; citac 0 a 1 cita ; interapy hranou
	MOV   SCON,#11011000B ; dva stopbity
	MOV   PCON,#10000000B ; Bd = OSC/12/16/(256-TH1)
	MOV   TH1,#0FAH       ; 9600Bd
	MOV   IP,#00010010B   ; priority preruseni
	MOV   PSW,#0          ; banka registru 0

	%VECTOR (TIMER0,WLENGHT)
	%VECTOR (EXTI0,I_TIME0)
	%VECTOR (EXTI1,I_TIME1)

	MOV   DPTR,#MOTOR     ; Vypnuti motoru
	MOV   A,#0FFH
	MOVX  @DPTR,A

	MOV   A,#000H
	MOV   DPTR,#AUX_OUT
	MOVX  @DPTR,A         ; vypnuti vystupu

	MOV   A,#07           ; vypnuti lamp
	MOV   DPTR,#LAMPS_CTRL
	MOVX  @DPTR,A

	MOV   LED_FLG,#0FFH
	CALL  LEDWR

	MOV   HW_FLG,#040H    ; povoleni casu smazani chyb

	CALL  LCDINST         ; inicializace displeje

	MOV   R2,#000H        ; RAM test
	SJMP  SF_INI2

SF_INI1:MOV   DPTR,#T_RAM_E
	CALL  cPRINT
SF_INID:DJNZ  R4,SF_INID
	DJNZ  R3,SF_INID
	DJNZ  R2,SF_INI2
	JMP   RESET

SF_INI2:MOV   DPTR,#RAM_T_B
	MOV   A,#055H
	MOVX  @DPTR,A
	MOVX  A,@DPTR
	CJNE  A,#055H,SF_INI1
	MOV   A,#-55H
	MOVX  @DPTR,A
	MOVX  A,@DPTR
	CJNE  A,#-55H,SF_INI1
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	CALL  SRAMSUM
	MOV   A,R2
	CLR   C
	SUBB  A,R3
	JZ    SF_INI4
	MOV   DPTR,#T_SRAME
;	CALL  cPRINT           ; !!!!!!!!
SF_INI3:CALL  SCANKEY

;        JZ    SF_INI3         ; !!!!!!!!

	ADD   A,#-K_1
	JNZ   SF_INI4
	MOV   DPTR,#BEG_SRAM+3
	MOV   DPTR,#BEG_SRAM+3
	MOV   R2,#LOW(END_SRAM-BEG_SRAM)
	MOV   R3,#HIGH(END_SRAM-BEG_SRAM)
;        CALL  xNULs                      ; !!!!!!!!!!!!
	MOV   A,#55
	MOV   DPTR,#CHEK_SM
	MOVX  @DPTR,A
SF_INI4:MOV   DPTR,#ADC_ACH
	MOV   A,#SFABSRQ
	MOVX  @DPTR,A
	CLR   A
	MOV   DPTR,#TIMR1
	MOV   R1,#N_OF_T
SF_INI5:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R1,SF_INI5
	MOV   DPTR,#R_LAMPS
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#007H         ; vypnuti lamp
	MOVX  @DPTR,A
	CLR   A
	MOV   DPTR,#SCNFL     ; Vysilani scanu
	MOVX  @DPTR,A
	MOV   LED_FLG,A       ; nulovani LED diod a modu
	CALL  LEDWR
	MOV   DPTR,#ABSFL
	MOV   A,#SFABSRQ
	MOVX  @DPTR,A
	CALL  DAC_INIT
	MOV   R7,#25
	CLR   A
SF_INI6:JNB   IE0,SF_INI7
	CLR   IE0
	INC   A
SF_INI7:JNB   IE1,SF_INI6
	CLR   IE1
	DJNZ  R7,SF_INI6
	ADD   A,#-45
	ADD   A,#-20
	JNC   SF_INI8
	CLR   EX0
	SETB  EX1
SF_INI8:JMP   START

; Inicializace DAC a ADC
; ======================

DAC_INIT:
	MOV   DPTR,#CNT53_CW
	MOV   A,#MOD53_0
	MOVX  @DPTR,A
	MOV   A,#MOD53_1
	MOVX  @DPTR,A
	MOV   A,#MOD53_2
	MOVX  @DPTR,A
	MOV   DPTR,#CNT53_0
	MOV   A,#VAL53_0L
	MOVX  @DPTR,A
	MOV   A,#VAL53_0H
	MOVX  @DPTR,A
	MOV   DPTR,#I8255_C1
	MOV   A,#I8255_M1
	MOVX  @DPTR,A
	MOV   DPTR,#I8255_C2
	MOV   A,#I8255_M2
	MOVX  @DPTR,A
	MOV   A,#07           ; vypnuti lamp
	MOV   DPTR,#LAMPS_CTRL
	MOVX  @DPTR,A
	MOV   DPTR,#DAC_LA
	CLR   A
	MOVX  @DPTR,A
	MOV   DPTR,#DAC_H
	MOV   A,#080H
	MOVX  @DPTR,A
	MOV   DPTR,#ADC_CTRL
	MOV   A,#0FFH
	MOVX  @DPTR,A
	JMP   INADC1

; Vypocet kontrolniho souctu SRAM
; ===============================
; vystup : R2
; meni   : DPTR,R4,R5

SRAMSUM:MOV   DPTR,#BEG_SRAM+3
	MOV   R4,#LOW(END_SRAM-BEG_SRAM)
	MOV   R5,#HIGH(END_SRAM-BEG_SRAM)
	MOV   R2,#055H
SRAMSU1:MOV   A,R4
	ORL   A,R5
	JZ    SRAMRET
	MOVX  A,@DPTR
	ADD   A,R2
	MOV   R2,A
	MOV   A,R4
	DEC   R4
	JNZ   SRAMSU1
	DEC   R5
	SJMP  SRAMSU1

SRAMRET:RET

; Vstup z ADC do AKUM
; ===================
; meni A,PD,R0

INADC  :MOV   DPTR,#ADC_IN1
	MOVX  A,@DPTR
	MOV   R0,A
	SWAP  A
	ANL   A,#00FH
	MOV   AKUM,A
	MOV   DPTR,#CNT53_1
	MOVX  A,@DPTR
	CPL   A
	SWAP  A
	MOV   AKUM+1,A
	ANL   AKUM+1,#00FH
	ANL   A,#0F0H
	ORL   AKUM,A
	MOVX  A,@DPTR
	CPL   A
	MOV   C,ACC.7
	SWAP  A
	MOV   AKUM+2,A
	ANL   AKUM+2,#00FH
	ANL   A,#0F0H
	ORL   AKUM+1,A
	SETB  F0
	JC    INADC1
	MOV   A,R0
	ANL   A,#ADC_OVR
	JNZ   INADC1
	CLR   F0
	MOV   A,R0
	ANL   A,#ADC_NR
	JZ    INADC1
	SETB  F0
	MOV   A,#ADC_NR
	RET

INADC1: MOV   DPTR,#ADC_ACH   ; dalsi prevod
	MOVX  A,@DPTR
	ANL   A,#0C0H
	MOV   R0,A

	MOV   DPTR,#ADC_CTRL  ; spusteni dalsiho prevodu
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	ANL   A,#03FH AND NOT ADC_CAD
	ORL   A,R0
	MOVX  @DPTR,A
	MOV   R0,A

	MOV   DPTR,#CNT53_1   ; delka integracni casti
	MOV   A,#0FFH
	MOVX  @DPTR,A
	MOV   A,#03FH
	MOVX  @DPTR,A

	MOV   DPTR,#ADC_CTRL
	MOV   A,R0
	ORL   A,#ADC_CAD
	MOVX  @DPTR,A
	MOV   EA,C

	MOV   A,#ADC_OVR
	RET


; Vystup na DAC z AKUM
; ====================
;

OUTDAC :CALL  LOADA
	MOV   A,R6
	MOV   R0,A
	ORL   A,#080H
	MOV   R6,A
	MOV   R1,#0
	MOV   A,R7
	ADD   A,#07EH      ; vystupni rozsah
	JC    OUTDACE
	ADD   A,#008H      ; pocet rozsahu + 1
	JC    OUTDAC3
	CPL   A
	INC   A
	CJNE  A,#12,OUTDAC1; presnost - porovnani 0
OUTDAC1:JC    OUTDAC2
	CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
OUTDAC2:MOV   R1,A
	CLR   A
OUTDAC3:MOV   R3,A
	MOV   A,#080H
	INC   R3
OUTDAC4:RL    A
	DJNZ  R3,OUTDAC4
	MOV   R2,A
	INC   R1
	CALL  POSP
	MOV   A,R6
	ORL   A,#080H
	MOV   R6,A
OUTDACP:MOV   A,R0
	ANL   A,#080H
	JZ    OUTDAC5
	CLR   C
	CALL  NEGREG+4
OUTDAC5:MOV   DPTR,#GLO_AUFS
	MOVX  A,@DPTR
	ANL   A,#00FH
	MOV   DPTR,#DAC_LA
	MOV   R0,A
	MOV   A,R5
	ANL   A,#0F0H
	ORL   A,R0
	MOVX  @DPTR,A
	MOV   A,R6
	MOV   DPTR,#DAC_H
	MOVX  @DPTR,A
	MOV   DPTR,#CNT53_2
	MOV   A,R2
	MOVX  @DPTR,A
	RET

OUTDACE:MOV   A,#0FFH
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R2,#080H
	SJMP  OUTDACP

;********************************************************************
;
;       Zpracovani vstupu a vystupu

; Nastaveni AUX podle R4
; ======================

S_AUX:  MOV   A,R4
	ANL   A,#00FH
	MOV   R2,A
	MOV   DPTR,#AUX_OUT
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	ANL   A,#0F0H
	ORL   A,R2
	MOVX  @DPTR,A
	MOV   EA,C
	SETB  U_AUX
G_AUX:  MOV   DPTR,#AUX_OUT
	MOVX  A,@DPTR
	ANL   A,#00FH
	MOV   R2,A
	MOVX  A,@DPTR
	RR    A
	RR    A
	ANL   A,#030H
	ORL   A,R2
	MOV   R2,A
	MOV   DPTR,#AUXUAL
	MOVX  A,@DPTR
	XRL   A,R2
	JZ    G_AUXR
	SETB  U_AUX
	MOV   A,R2
	MOVX  @DPTR,A
G_AUXR: RET



; *******************************************************************

; Ovladani motoru

; Inicializace motoru
; ===================

I_W_LEN:CLR   ET0
	MOV   F_W_LEN,#080H
	MOV   TH0,#0FFH
	MOV   F_W_LEN+6,#10
	MOV   F_W_LEN+7,#033H

; Otoci motor na zadanou pozici
; =============================

S_W_LEN:MOV   DPTR,#R_W_LEN
	CALL  xLDR45i
S_W_LE0:CLR   ET0
	MOV   A,F_W_LEN
	JB    ACC.5,S_W_LE1
	MOV   RIW_LEN,R4
	MOV   RIW_LEN+1,R5
	SETB  ET0
	SETB  TR0
S_W_LE1:SETB  U_WLEN
	RET

USING 3

RIW_LEN DATA  AR4
A_W_LEN DATA  AR2
F_W_LEN DATA  AR0
NRE_LEN BIT   ET0

WLENIV: JMP   WLENI

WLENGHT:CLR   TF0
	PUSH  PSW
	PUSH  ACC
	MOV   PSW,#00011000B  ; Banka registru 3
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#MOT_IN
WLENB1: MOV   TL0,#0
	MOV   TH0,#0C0H
	MOV   A,R0
	JB    ACC.7,WLENIV    ; inicializace
	MOV    A,R6
	JNZ    WLENB2
	MOV    A,R2
	ORL    A,R3
	JZ     WLENB3
	MOVX   A,@DPTR
	ANL    A,#MOT_MSK
	JZ     WLENEER
	SJMP   WLENB3
WLENB2: ADD    A,#-008H
	ADD    A,#-0B8H
	JC     WLENB3
	MOVX   A,@DPTR
	ANL    A,#MOT_MSK
	JNZ    WLENEER
WLENB3: MOV    A,R0
	JB     ACC.4,WLENB5
	CLR    C
	MOV    A,R4
	SUBB   A,R2
	MOV    R1,A
	MOV    A,R5
	SUBB   A,R3
	CJNE   R0,#8,WLENB4
	XCH    A,R1
	ADD    A,#LOW (19)    ; Pocet kroku pro prejeti
	XCH    A,R1
	ADDC   A,#HIGH (19)
WLENB4:	JB     ACC.7,WLENDO
	ORL    A,R1
	JNZ    WLENUP
	CJNE   R0,#8,WLENEND  ; Dokonceni prejeti
	MOV    R0,#17H
	MOV    DPTR,#MOTOR
	MOVX   A,@DPTR
	ORL    A,#010H
	MOVX   @DPTR,A
WLENB5: DEC    R0
	SJMP   WLENRE1
WLENDO: MOV    A,R0
	MOV    R0,#004H
	JB     ACC.3,WLENRE1
	DEC    R2
	CJNE   R2,#0FFH,WLEND1
	DEC    R3
WLEND1: DEC    R6
	CJNE   R6,#0FFH,WLEND2
	MOV    R6,#199
WLEND2: MOV    A,R7
	RR     A
	MOV    R7,A
WLEND3: ANL    A,#00FH
	MOV    R1,A
	MOV    DPTR,#MOTOR
	MOVX   A,@DPTR
	ANL    A,#0E0H
	ORL    A,R1
	MOVX   @DPTR,A
WLENRE1:SETB   ET0
	SETB   TR0
WLENRE: SETB   U_WLEN
	POP    DPH
	POP    DPL
	POP    ACC
	POP    PSW
	RETI

WLENUP: MOV    A,R0
	MOV    R0,#008H
	JB     ACC.2,WLENRE1
	INC    R2
	CJNE   R2,#000H,WLENU1
	INC    R3
WLENU1: INC    R6
	CJNE   R6,#200,WLENU2
	MOV    R6,#0
WLENU2: MOV    A,R7
	RL     A
	MOV    R7,A
	SJMP   WLEND3

WLENEER:MOV    A,R0
	ORL    A,#020H
	MOV    R0,A
	DB     090H
WLENEND:MOV    R0,#0
WLENE4: MOV    DPTR,#MOTOR
	MOVX   A,@DPTR
	ORL    A,#010H
	MOVX   @DPTR,A
	CLR    TR0
	CLR    ET0
	SJMP   WLENRE

WLENI:  ANL    A,#003H
	JNZ    WLENI1
	MOVX   A,@DPTR
	ANL    A,#MOT_MSK
	JNZ    WLENI4
	DJNZ   R6,WLENU2
	INC    R0
	INC    A
	MOV    R6,#220
WLENI1: MOV    C,ACC.0
	MOVX   A,@DPTR
	ANL    A,#MOT_MSK
	JNC    WLENI2
	JNZ    WLENE1
	DJNZ   R6,WLENU2
	MOV    A,R0
	JB     ACC.1,WLENEER
	INC    R0
	SJMP   WLENRE1
WLENI2: JZ     WLENI3
	MOV    R6,#10
WLENI3: DJNZ   R6,WLEND2
	INC    R0
	MOV    R6,#20
	SJMP   WLENRE1
WLENI4: MOV    R0,#082H
	SJMP   WLENRE1

WLENE1: MOV    R6,#199
	SWAP   A
	RL     A
	MOV    R1,A
	MOV    DPTR,#WL_TAB-2
	MOVC   A,@A+DPTR
	MOV    R2,A
	MOV    A,R1
	INC    A
	MOVC   A,@A+DPTR
	MOV    R3,A
	MOV    R0,#0
	JMP    WLENRE1

WL_TAB: DB     LOW 199 ,HIGH 199
	DB     LOW 399 ,HIGH 399
	DB     LOW 599 ,HIGH 599
	DB     LOW 799 ,HIGH 799
	DB     LOW 999 ,HIGH 999
	DB     LOW 1199,HIGH 1199
	DB     LOW 1399,HIGH 1399
	DB     LOW 1599,HIGH 1599
	DB     LOW 1799,HIGH 1799

;********************************************************************
;
;       Rizeni lamp

S_LAMP: MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	MOV   DPTR,#R_LAMPS
	CALL  xSVR45i
	RET

;********************************************************************
;
;       Absorbance nebo data z ADC podle ADC_ACH

G_SFABS:MOV   DPTR,#ADC_ACH
	MOVX  A,@DPTR
	JNB   ACC.SFABSRQB,G_SFAB1
	JMP   SFABS
G_SFAB1:JNB   ACC.INADCRQB,G_SFABR
	JMP   INADCD
G_SFABR:RET

;********************************************************************
;
;       Casove preruseni

USING   2

I_TIME1:ANL   HW_FLG,#NOT 080H; Vstup v pripade vypadku padesatky
	SETB  IE1

I_TIME0:PUSH  ACC             ; Vstup do casoveho preruseni od 50Hz
	PUSH  PSW
	CLR   IE0
	MOV   PSW,#00010000B  ; Banka 2
	PUSH  DPL
	PUSH  DPH

	XRL   HW_FLG,#080H
	MOV   A,HW_FLG
	JNB   ACC.7,I_TIM_3
	JNB   ITIM_RF,I_TIM_3

	PUSH  B               ; vstupni prevody
	PUSH  P2
	CALL  G_SFABS
	MOV   DPTR,#ABSFL
	MOVX  A,@DPTR
	ANL   A,#ADC_NR
	JNZ   I_TIM_2
	SETB  U_ABS
I_TIM_2:CALL  LAN_TM
	POP   P2
	POP   B

I_TIM_3:JNB   IE1,ITIM_R1
	CLR   IE1
	MOV   DPTR,#TIMR1-1
	MOV   R1,#N_OF_T
I_TIM_4:INC   DPTR
	MOVX  A,@DPTR
	JZ    I_TIM_5
	DEC   A
	MOVX  @DPTR,A
I_TIM_5:DJNZ  R1,I_TIM_4
	JZ    I_TIM_6

	DB    90H             ; MOV DPTR,#
ITIM_R: SETB  ITIM_RF
ITIM_R1:POP   DPH
	POP   DPL
	POP   PSW
	POP   ACC
C_IRET: RETI

I_TIM_6:MOV   A,#15
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	INC   DPTR
	JNZ   I_TIM_7
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
I_TIM_7:SETB  U_TIME
	JNB   ITIM_RF,ITIM_R1

	CALL  C_IRET
	JNB   %ALRM_FL,I_TIM_8
	XRL   LED_FLG,#080H
	CALL  LEDWR
I_TIM_8:CALL  G_AUX
	MOV   DPTR,#R_LAMPS
	CLR   EA
	CALL  xLDR45i
	SETB  EA
	MOV   DPTR,#LAMPS_CTRL
	MOVX  A,@DPTR
	XRL   A,R5
	ANL   A,#007H
	JZ    I_TIM13
	ANL   LED_FLG,#11001011B
	MOVX  A,@DPTR
	ANL   A,#6
	JZ    I_TIM10
	MOV   A,R5
	ANL   A,#0F0H
	JZ    I_TIM10
	JB    ACC.5,I_TIM_9
	MOV   A,#15           ; Doba potrebna pro zhaveni
	MOV   DPTR,#L_DELAY
	MOVX  @DPTR,A
	MOV   A,R5
	ORL   A,#020H
	MOV   DPTR,#R_LAMPS+1
	MOVX  @DPTR,A
	MOV   R5,#4
	SJMP  I_TIM10
I_TIM_9:MOV   DPTR,#L_DELAY
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	JB    ACC.0,I_TIM12
	JNZ   I_TIM11
I_TIM10:MOV   A,R5
	ANL   A,#07H
	MOV   R5,A
	MOV   DPTR,#LAMPS_CTRL
	MOVX  A,@DPTR
	ANL   A,#0F8H
	ORL   A,R5
	MOVX  @DPTR,A
I_TIM11:MOV   A,R4
	ORL   LED_FLG,A
I_TIM12:CALL  LEDWR
I_TIM13:JMP   I_TIME

T_RAM_E:DB    C_CLR, 'RAM unreachable'
	DB    C_LIN2,' Trying again ',0
T_SRAME:DB    C_CLR, 'SRAM data error'
	DB    C_LIN2,'0-Ignore 1-Clear',0

END