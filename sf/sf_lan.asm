;********************************************************************
;*                    LCP 4000 - LP_LAN.ASM                         *
;*     Vykonavani rizeni ze site uLAN a vysilani dat                *
;*                  Stav ke dni 18.10.1993                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

EXTRN	BIT(uLE_ABS)
EXTRN	DATA(A_W_LEN)
EXTRN	XDATA(ABS,R_W_LEN)
EXTRN	CODE(f2IEEE,S_W_LEN,S_ZERO,S_LAMP)

PUBLIC	uL_IDLE,uL_IDLK,LAN_INI,LAN_TM,LAN_MRK

$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_AL)
$INCLUDE(%INCH_ADR)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_UF)
$INCLUDE(%INCH_ULAN)
$INCLUDE(%INCH_UL_OI)
$LIST

LAN___B SEGMENT DATA BITADDRESSABLE

LAN___C SEGMENT CODE

LAN___X SEGMENT XDATA

RSEG LAN___X

uL_SBPO:DS    2
uLABS:  DS    4

PAR_LEN EQU   64

RSEG LAN___C

; Identifikace typu pristroje
PUBLIC	uL_IDB,uL_IDE
uL_IDB: DB    '.mt %VERSION .uP 51x',0
uL_IDE:

LAN_INI:CLR   A
	MOV   DPTR,#uL_SBPO
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#uL_FORM
	MOVX  A,@DPTR
	JZ    LAN_INR
	CLR   ES
	;CLR   A
	;MOV   R0,#1
	;CALL  uL_FNC	; Speed
	;MOV   A,#3
	;MOV   R0,#2
	;CALL  uL_FNC	; Adr
	MOV   R2,#0	; R2 poc IB, R3 len IB, R4 len OB
	MOV   R0,#3
	CALL  uL_FNC	; IB, OB
	MOV   R2,#0	; R2 uL_SBLE, R3 uL_SBCO, uL_SBB=R45
	MOV   R0,#4
	CALL  uL_FNC	; DO
	MOV   R0,#0
	CALL  uL_FNC
	%LDR45i (OID_1IN) ; Zapnout objektovou komunikaci
	%LDR67i (OID_1OUT)
	JMP    US_INIT
LAN_INR:CLR   ES
	RET

LAN_TM: CALL  uL_STR
	JNB   ES,LAN_TME
	MOV   DPTR,#uL_SBP
	CALL  xLDR23i
	MOV   DPTR,#uL_SBPO
	CALL  xLDR45i
	CALL  xSVR23i
	MOV   DPTR,#uL_SBP
	CALL  xSVR45i
	MOV   DPTR,#uL_FORM
	MOVX  A,@DPTR
	CJNE  A,#2,LAN_TMR
	JMP   LAN_A
LAN_TMR:MOV   DPTR,#uL_SBP
	CALL  xLDR23i
	MOV   DPTR,#uL_SBPO
	CALL  xLDR45i
	CALL  xSVR23i
	MOV   DPTR,#uL_SBP
	CALL  xSVR45i
LAN_TME:RET

LAN_A1: MOV   DPTR,#uL_GRP
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#4FH
	CLR   F0
	CALL  uL_S_OP
	JNB   F0,LAN_A
	SETB  uLE_ABS
	CLR   A
	MOV   DPTR,#uL_FORM
	MOVX  @DPTR,A
	JMP   LAN_TMR

LAN_A:	MOV   DPTR,#ABS
	MOV   R0,#4
	MOV   R1,#0
	MOV   R4,#LOW  uLABS
	MOV   R5,#HIGH uLABS
	CALL  xxMOVE1
	MOV   DPTR,#uLABS+2
	MOVX  A,@DPTR
	RLC   A
	INC   DPTR
	MOVX  A,@DPTR
	JZ    LAN_A2
	RRC   A
	DEC   A
	MOVX  @DPTR,A
	MOV   DPTR,#uLABS+2
	MOVX  A,@DPTR
	MOV   ACC.7,C
LAN_A2:	MOV   DPTR,#uLABS+2
	MOVX  @DPTR,A
	MOV   DPTR,#uLABS
	MOV   R4,#4
	MOV   R5,#0
	CLR   F0
	CALL  uL_S_WR
	JB    F0,LAN_A1
	JMP   LAN_TMR

LAN_MRK:JNB   ES,L_MRKR
	MOV   DPTR,#uL_FORM
	MOVX  A,@DPTR
	CJNE  A,#2,L_MRKR

	MOV   DPTR,#uL_SBP
	MOVX  A,@DPTR
	PUSH  ACC
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	PUSH  ACC
	CLR   A
	MOVX  @DPTR,A

	MOV   DPTR,#uL_GRP
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#4EH
	CLR   F0
	CALL  uL_S_OP
	MOV   DPTR,#L_MRKD
	MOV   R4,#4
	MOV   R5,#0
	CALL  uL_S_WR
	CALL  uL_S_CL

	JNB   F0,L_MRK1
	SETB  uLE_ABS
L_MRK1:
	MOV   DPTR,#uL_SBP+1
	POP   ACC
	MOVX  @DPTR,A
	CALL  DECDPTR
	POP   ACC
	MOVX  @DPTR,A
L_MRKR: RET

L_MRKD: DB    0,7,0,0

uL_IDLE:

uL_IDLK:CLR   C
	JNB   uLF_INE,uL_IDLR
	CALL  UI_PR		; objektova komunikace

	SETB  C
uL_IDLR:CLR   A
	RET

; *******************************************************************
; Komunikace s ostatnimi jednotkami pres uLan

; Definice jmenprikazu

I_WLEN	  EQU	204
%OID_ADES(AI_WLEN,WLEN,u2)
I_LAMPC	  EQU   207
%OID_ADES(AI_LAMPC,LAMPC,u2)
I_ADCFILT EQU   208
%OID_ADES(AI_ADCFILT,ADCFILT,u2)
I_ADCMODE EQU   209
%OID_ADES(AI_ADCMODE,ADCMODE,u2)
I_ABS	  EQU   220
%OID_ADES(AI_ABS,ABS,f4)
I_AUXUAL  EQU	240
%OID_ADES(AI_AUXUAL,AUXUAL,u2)
I_OFF	  EQU   250
%OID_ADES(AI_OFF,OFF,e)
I_ON	  EQU   251
%OID_ADES(AI_ON,ON,e)
I_ZERO	  EQU   255
%OID_ADES(AI_ZERO,ZERO,e)

; *******************************************************************
; Komunikace pres uLan - cast slave

RSEG	LAN___X

TMP_U:	DS    16

RSEG	LAN___C

; Status

G_STATUS:
	%LDR45i (0)
	RET

ERRCLR_U:RET

; Cteni absorbance/hodnoty v pohyblive radove carce

UO_ABSf:MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R0
	MOV   DPH,A
	MOV   C,EA
	CLR   EA
	CALL  xLDl	; R4567 = absorbance float
	MOV   EA,C
	CALL  f2IEEE
	MOV   DPTR,#TMP_U
	CALL  xSVl
	MOV   DPTR,#TMP_U
	%LDR45i(4)
	%VJMP (UV_WR)

; Nastaveni vlnove delky

UI_WLEN:MOV   DPTR,#R_W_LEN
	MOV   C,EA
	CLR   EA
	PUSH  PSW
	CLR   C
	MOV   A,R4
	RLC   A
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	RLC   A
	MOVX  @DPTR,A
	POP   PSW
	MOV   EA,C
	JMP   S_W_LEN

UO_WLEN:MOV   C,EA
	CLR   EA
	PUSH  PSW
	CLR   C
	MOV   A,A_W_LEN+1
	RRC   A
	MOV   R5,A
	MOV   A,A_W_LEN
	RRC   A
	MOV   R4,A
	POP   PSW
	MOV   EA,C
	RET

; Nastaveni zdroje svetla

LAMPC_U:MOV   A,R4
	JNZ   LAMPC_U1
	MOV   R2,#00H
	MOV   R3,#07H
	JMP   S_LAMP
LAMPC_U1:DJNZ R4,LAMPC_U2
	MOV   R2,#10H
	MOV   R3,#11H
	JMP   S_LAMP
LAMPC_U2:DJNZ R4,LAMPC_U3
	MOV   R2,#20H
	MOV   R3,#10H
	JMP   S_LAMP
LAMPC_U3:RET

; Prijimane prikazy

OID_T	SET   $
	%W    (I_ERRCLR)
	%W    (OID_ISTD)
	%W    (0)
	%W    (ERRCLR_U)

%OID_NEW(I_ZERO,AI_ZERO)
	%W    (S_ZERO)

%OID_NEW(I_LAMPC,AI_LAMPC)
	%W    (UI_INT)
	%W    (0)
	%W    (LAMPC_U)

%OID_NEW(I_WLEN,AI_WLEN)
	%W    (UI_INT)
	%W    (0)
	%W    (UI_WLEN)

OID_1IN SET   OID_T

; Vysilane hodnoty

OID_T	SET   0

%OID_NEW(I_WLEN,AI_WLEN)
	%W    (UO_INT)
	%W    (0)
	%W    (UO_WLEN)

%OID_NEW(I_STATUS,0)
	%W    (UO_INT)
	%W    (0)
	%W    (G_STATUS)

%OID_NEW(I_ABS,AI_ABS)
	%W    (UO_ABSf)
	%W    (ABS)

OID_1OUT SET  OID_T

END
