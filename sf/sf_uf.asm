;********************************************************************
;*                    LCP 4000 - LP_UF.ASM                          *
;*                       User interface                             *
;*                  Stav ke dni 11.11.1992                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

EXTRN   CODE(uL_IDLE,uL_IDLK)
EXTRN   DATA(WR_UPDA,WR_MASK)
EXTRN   CODE(WRT_1,LNT_DIR,ERR_CRY,HELP_HL)
EXTRN   NUMBER(LNT_MAX)
EXTRN   XDATA(TIMR_WAIT)
EXTRN   XDATA(PROGRAM,PROGR_E)

$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_REGS)
$LIST

PUBLIC  SET_DIT,SET_POT,SET_PO1,R_CHAR,RD_RET
PUBLIC  SEL_FN,SEL_FNC,SP_TIME,SP_TIMS,DEL_LN
PUBLIC  DEL_LN0,DEL_SPC,INS_LN,INS_SPC,INS_SP0
PUBLIC  NUL_SPC,FND_PGE,PREV_LN,PREV_L0,NEXT_LN
PUBLIC  SET_PRE,SET_NEX,ENT_PRG,MOD_LNP,PROG_IN
PUBLIC  SET_LN,SET_LNP,SET_LN1,SEL_LT,SEL_LT1
PUBLIC  SET_IMP,R_INTEG,R_AUXU,QUES_YN,INPUTc
PUBLIC  INPUTc1,ERR_DPB,ERR_DPT,ERR_DP1,WAIT_P
PUBLIC  WAIT_1,WR_FLEX,xJMPDPP,cMDPDP,xMDPDP
PUBLIC  xMDPDPA
PUBLIC  cxMOVEt,cxMOVE1,ADDATDP,MENU_FD,MENU_F
PUBLIC  HELP_1,WR_FLIN,cWR_DPP,SEL_FN2,WR_RET
PUBLIC  UP_BDP,DO_BDP,R_JMACT,ENT_PR1,MO_BDP
PUBLIC  GET_LAD,TST_LI0,TST_LIN,GO_PRBE
PUBLIC  cxMOVE,xxMOVEt,xxMOVE,xxMOVE1,SET_LAD
PUBLIC  SET_MENU,XCDPR01,R_TEXT,R_TEXTP
PUBLIC	SET_PROG,INS_PRG,WR_PROG,DEL_PRG,TST_PRG

PUBLIC  DATA_CH,PROG_SF,PROG_NU
PUBLIC  PROG_BE,PROG_EN,LINE_P,LINE_PO

PUBLIC  POT,DIT

PUBLIC  WR_TAB,HLP_TXT,WR_RETB,RD_DP,RD_V_DP,RD_ACTP

; SEL_VEC,WR_POS,RD_VEC

;WR_MSKB,F_LINE,DP_FLEX

;R_BUFF,HLP_DIS,HLP_FN,HLP_POS

SF_UF_C SEGMENT CODE
SF_UF_X SEGMENT XDATA

RSEG SF_UF_X

DATA_CH:DS    1     ; 0 - pokud se nemenila data
PROG_SF:DS    1     ; 0AAH pri programu v poradku, ABH pri editaci
PROG_NU:DS    1     ; Cislo programu
PROG_BE:DS    2     ; Pocatek programu
PROG_EN:DS    2     ; Konec programu
LINE_P: DS    2     ; Ukazatel na raku programu
LINE_PO:DS    1     ; Pozice v radce

POT:
SEL_VEC:DS    2     ; Ukazatel pro rutinu zpracovani klaves
HLP_TXT:DS    2     ; Ukazatel na prislusny help
WR_POS: DS    1     ; Editovana pozice na displeji
RD_VEC: DS    2     ; Ukazatel na vstupni rutinu
RD_DP:  DS    2     ; Ukazatel na menena data
RD_V_DP:DS    2     ; Ukazatel pro vstupni rutinu
RD_ACTP:DS    2     ; Akce volana po cteni
DIT:
WR_MSKB:DS    1     ; Buffer WR_MASK
F_LINE: DS    2     ; Ukazatel na text prvni radky
WR_TAB: DS    2     ; Ukazatel na tabulku ukazatelu na vystup
DP_FLEX:DS    2     ; Ukazatel na vystup FLEX
WR_RETB:DS    2

R_BUFF: DS    32    ; Editace radky textu

HLP_DIS:DS    2     ; Pozice displeje
HLP_FN: DS    1     ; Funkce : 0 - help
HLP_POS:DS    2     ; Pozice v helpu

RSEG SF_UF_C

SET_DIT:POP   ACC
	POP   ACC
	MOV   R4,#LOW DIT
	MOV   R5,#HIGH DIT
	MOV   R0,#009H
	CALL  cxMOVEt
	MOV   WR_UPDA,#0FFH
	MOV   DPTR,#WR_MSKB
	MOVX  A,@DPTR
	MOV   WR_MASK,A
	MOV   DPTR,#WR_RETB
	CALL  xLDR23i
	PUSH  ACC
	PUSH  ACC

SET_POT:POP   ACC
	POP   ACC
	MOV   R4,#LOW POT
	MOV   R5,#HIGH POT
	MOV   R0,#00DH
	CALL  cxMOVEt
	JNB   %PROG_FL,SET_PO1
	MOV   DPTR,#LINE_P    ; Zmodifikuje RD_DP
	CALL  xLDR23i	      ; pro danou radku programu
	MOV   DPTR,#RD_DP
	CALL  xLDR45i
	CALL  ADDi
	CALL  xSVR45i
SET_PO1:MOV   DPTR,#WR_POS
	MOVX  A,@DPTR
	ORL   A,#LCD_HOM
	CALL  LCDWCOM
	MOV   DPTR,#DATA_CH
	CLR   A
	MOVX  @DPTR,A
	MOV   DPTR,#RD_VEC
	JMP   xJMPDPP

SET_MENU:POP  ACC
	POP   ACC
	CALL  MENU_F
SET_ME1:CALL  MENU_F1
	JMP   SET_ME1

R_CHAR: CALL  INPUTc1
RD_RET: MOV   R7,A
	MOV   DPTR,#SEL_VEC
SEL_FN: MOV   A,#LOW SET_PO1
	PUSH  ACC
	MOV   A,#HIGH SET_PO1
	PUSH  ACC
SEL_FN1:CALL  xMDPDP
SEL_FNC:
SEL_FN2:CLR   A
	MOVC  A,@A+DPTR
	JZ    SEL_FNR
	INC   DPTR
	INC   A
	JZ    SEL_FN4
	DEC   A
	XRL   A,R7
	JNZ   SEL_FN3
	CALL  cLDR23i
	JMP   cJMPDPP
SEL_FN3:INC   DPTR
	INC   DPTR
	INC   DPTR
	INC   DPTR
	SJMP  SEL_FN2
SEL_FN4:CALL  cLDR23i
	MOV   DPL,R2
	MOV   DPH,R3
	JMP   SEL_FN2
SEL_FNR:RET

; Vyber programu podle [PROG_NU]

SET_PROG:MOV  DPTR,#PROG_NU
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   R2,#0
	MOV   DPTR,#PROGRAM
	CALL  PRP_PR1
SET_PR1:INC   R2
	MOV   A,R2
	CLR   C
	SUBB  A,R3
	JNC   SET_PRR
	MOV   A,#1
	MOVC  A,@A+DPTR
	JZ    SET_PRR
	CALL  NEXT_L0
	JZ    SET_PRR
SET_PR2:CALL  NEXT_L
	JNZ   SET_PR2
	JMP   SET_PR1
SET_PRR:CALL  SET_LAD
	MOV   DPTR,#PROG_BE
	CALL  xSVR45i
	MOV   A,R2
	MOV   DPTR,#PROG_NU
	MOVX  @DPTR,A
	RET

; Priprava na praci s programem

PRP_PRG:MOV   DPTR,#PROG_BE
	CALL  xMDPDP
PRP_PR1:CALL  SET_LAD
	MOV   DPTR,#PROG_SF
	MOV   A,#0ABH
	MOVX  @DPTR,A
	MOV   WR_UPDA,#0FFH
	CALL  GET_LAD
TST_PRR:RET

; Kontrola programu

TST_PRG:CLR   F0
	CALL  PRP_PRG
	MOV   A,#1
	MOVC  A,@A+DPTR
	XRL   A,#040H
	JZ    TST_PRR

; Zalozeni noveho programu

INS_PRG:CALL  PRP_PRG
INS_PR1:MOV   R2,#010H
	MOV   R3,#040H
	CALL  INS_SPC
	JNB    F0,INS_PR2
	JMP   INS_LNE
INS_PR2:CALL  xSVR23i
	MOV   R0,#10
	MOV   A,#' '
INS_PR3:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,INS_PR3
DEL_PRR:RET

; Smazani programu

DEL_PRG:MOV   DPTR,#T_DL_PR
	CALL  QUES_YN
	JZ    DEL_PRR
DEL_PR0:MOV   DPTR,#DATA_CH
	MOV   A,#2
	MOVX  @DPTR,A
	MOV   DPTR,#PROG_EN
	CALL  xLDR23i
	CALL  PRP_PRG
	MOV   R4,DPL
	MOV   R5,DPH
	CALL  NEXT_L0
DEL_PR1:CALL  NEXT_L
	JNZ   DEL_PR1
	JMP   DEL_SP1

; Tisk programu na displej

WR_PROG:CALL  LCDNBUS
	MOV   A,#LCD_HOM+040H
	CALL  LCDWCO1
	MOV   DPTR,#PROG_NU
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#0
	MOV   R7,#030H
	CALL  PRINTi
	CALL  LCDNBUS
	MOV   DPTR,#PROG_BE
	CALL  xMDPDP
	INC   DPTR
	MOVX  A,@DPTR
	XRL   A,#040H
	JNZ   WR_PRO2
	MOV   R0,#11
	MOV   A,#':'
WR_PRO1:CALL  LCDWR1
	INC   DPTR
	CALL  LCDNBUS
	MOVX  A,@DPTR
	DJNZ  R0,WR_PRO1
WR_PRO2:RET

; Kontrola zadavaneho casu v posloupnosti

SP_TIME:CALL  PREV_LN
	CALL  SP_TIMS
	JZ    SP_TIM1
	JC    SP_TIM2
SP_TIM1:CALL  NEXT_LN
	JZ    SP_TIM2
	CALL  SP_TIMS
	JZ    SP_TIMR
	JC    SP_TIMR
SP_TIM2:CALL  GET_LAD
	INC   DPTR
	INC   DPTR
	CALL  xSVR23i
	MOV   DPTR,#ERR_TIT
	SETB  F0
	JMP   ERR_DPT

SP_TIMS:CALL  xLDR23i
	ANL   A,#03FH
	JZ    SP_TIMR
	MOV   A,R2
	JZ    SP_TIMR
	CALL  xLDR23i
	CALL  CMPi
SP_TIMR:RET

; Smaze aktualni radku

DEL_LN: CALL  DEL_LN0
	JMP   SET_LN

DEL_LN0:MOV   DPTR,#DATA_CH
	MOV   A,#2
	MOVX  @DPTR,A
	MOV   DPTR,#PROG_EN
	CALL  xLDR23i
	CALL  GET_LAD
	MOV   A,#1
	MOVC  A,@A+DPTR
	ANL   A,#03FH
	JZ    SP_TIMR
DEL_SPC:MOVX  A,@DPTR
	MOV   R4,DPL
	MOV   R5,DPH
	CALL  ADDATDP
DEL_SP1:CLR   C
	MOV   A,R2
	SUBB  A,DPL
	MOV   R0,A
	MOV   A,R3
	SUBB  A,DPH
	MOV   R1,A
	CALL  xxMOVE1
	MOV   DPTR,#PROG_EN
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   DPL,R4
	MOV   DPH,R5
	CLR   A
	MOVX  @DPTR,A
	RET

; Vloz novou radku typu R3

INS_LN: MOV   A,R3
	CALL  SEL_LT4
	MOVX  A,@DPTR
	MOV   R2,A
	CALL  INS_SPC
	JNB   F0,INS_LN1
INS_LNE:MOV   DPTR,#T_MEM_O
	JMP   ERR_DPT
INS_LN1:CALL  xSVR23i
	PUSH  DPL
	PUSH  DPH
	CALL  PREV_LN
	INC   DPTR
	INC   DPTR
	CALL  xLDR23i
	POP   DPH
	POP   DPL
	CALL  xSVR23i
	JMP   SET_LN

; Vloz R2 bytu do programu
; F0=1 pri preplneni pameti programu

INS_SPC:MOV   DPTR,#LINE_P
	CALL  xLDR45i
INS_SP0:MOV   DPTR,#PROG_EN
	CALL  xMDPDP
	MOV   R0,DPL
	MOV   R1,DPH
	MOV   A,R2
	CALL  ADDATDP
	MOV   R6,DPL
	MOV   R7,DPH
	SETB  F0
	MOV   A,R7
	XRL   A,#HIGH PROGR_E
	JNZ   INS_SP1
	RET
INS_SP1:CLR   F0
	MOV   DPTR,#PROG_EN
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	MOV   DPTR,#DATA_CH
	MOV   A,#2
	MOVX  @DPTR,A
INS_SP2:MOV   DPL,R0
	MOV   DPH,R1
	MOVX  A,@DPTR
	MOV   DPL,R6
	MOV   DPH,R7
	MOVX  @DPTR,A
	MOV   A,R0
	XRL   A,R4
	JNZ   INS_SP3
	MOV   A,R1
	XRL   A,R5
	JZ    INS_SP6
INS_SP3:DEC   R0
	CJNE  R0,#-1,INS_SP4
	DEC   R1
INS_SP4:DEC   R6
	CJNE  R6,#-1,INS_SP5
	DEC   R7
INS_SP5:JMP   INS_SP2
INS_SP6:MOV   DPL,R0
	MOV   DPH,R1
	MOV   A,R2
NUL_SPC:MOV   R0,A
	PUSH  DPL
	PUSH  DPH
INS_SP7:MOV   A,R0
	JZ    INS_SP8
	DEC   R0
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	JMP   INS_SP7
INS_SP8:POP   DPH
	POP   DPL
	RET

; Nastavi PROG_EN

FND_PGE:MOV   DPTR,#PROGRAM
FND_PG1:CALL  NEXT_L0
	JZ    FND_PG2
	MOV   A,#01
	MOVC  A,@A+DPTR
	JNZ   FND_PG1
FND_PG2:CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   R4,DPL
	MOV   R5,DPH
	MOV   DPTR,#PROG_EN
	CALL  xSVR45i
	RET

; Najde predchazejici radku

PREV_LN:MOV   DPTR,#LINE_P
	CALL  xLDR23i
	MOV   DPTR,#PROG_BE
	CALL  xMDPDP
	MOV   A,#01
	MOVC  A,@A+DPTR
	XRL   A,#040H
	JNZ   PREV_L0
	CALL  NEXT_L0
PREV_L0:MOV   R6,DPL
	MOV   R7,DPH
PREV_L1:MOV   A,DPL
	XRL   A,R2
	MOV   R1,A
	MOV   A,DPH
	XRL   A,R3
	ORL   A,R1
	JZ    PREV_LR
	MOV   R6,DPL
	MOV   R7,DPH
	CALL  NEXT_L
	JNZ   PREV_L1
PREV_LR:MOV   DPL,R6
	MOV   DPH,R7
	RET

; Prechod na nasledujici radku
; Vystup:DP .. dalsi radka pri A<>0
;         A =0 konec programu
;         F0=1 prusvih

NEXT_LN:CALL  GET_LAD
NEXT_L: MOV   A,#001
	MOVC  A,@A+DPTR
	ANL   A,#03FH
	JZ    NEXT_L1
NEXT_L0:MOVX  A,@DPTR
	JZ    NEXT_L1
	ADD   A,DPL
	MOV   DPL,A
	JNC   NEXT_L1
	INC   DPH
	MOV   A,DPH
	XRL   A,#HIGH PROGR_E
	JNZ   NEXT_L1
	SETB  F0
NEXT_L1:RET

; Jdi o radku nahoru

SET_PRE:CALL  PREV_LN
	JMP   SET_NE1

; Jdi o radku dolu

SET_NEX:CALL  NEXT_LN
SET_NE1:CALL  SET_LAD
	JMP   SET_LN

; Pomocna akce pri ENTER

ENT_PRG:CALL  SEL_LT
	INC   DPTR
	MOVX  A,@DPTR
	DEC   A
ENT_PR1:MOV   R0,A
	MOV   DPTR,#LINE_PO
	MOVX  A,@DPTR
	XRL   A,R0
	JZ    SET_NEX
	MOV   R2,#1

; Modifikuje LINE_PO podle R2
; POZOR : sp=sp-2

MOD_LNP:MOV   DPTR,#LINE_PO
	MOVX  A,@DPTR
	ADD   A,R2
	MOVX  @DPTR,A
	JMP   SET_LNP

; Jdi na zacatek programu

GO_PRBE:MOV   DPTR,#PROG_BE
	CALL  xMDPDP
	MOVX  A,@DPTR
	JZ    SET_LAD
	MOV   A,#1
	MOVC  A,@A+DPTR
	CJNE  A,#040H,SET_LAD
	CALL  NEXT_L0

; Nastavi radku na DPTR

SET_LAD:MOV   R4,DPL
	MOV   R5,DPH
	MOV   DPTR,#LINE_P
	CALL  xSVR45i
PROG_IR:RET

; Prejde do modu editovani programu
; POZOR : sp=sp-2

PROG_IN:CALL  TST_PRG
	JB    F0,PROG_IR
	MOV   DPTR,#PROG_SF
	MOV   A,#0ABH
	MOVX  @DPTR,A
	CALL  GO_PRBE
	SETB  %PROG_FL
	CALL  LEDWR
	CALL  FND_PGE

; Nastaveni radku programu
; nastavi DIT a POT
; POZOR : sp=sp-2

SET_LN: MOV   DPTR,#LINE_PO
	CLR   A
	MOVX  @DPTR,A
	CALL  SEL_LT
	INC   DPTR
	INC   DPTR
	CALL  cLDR23i
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#F_LINE
	CALL  xSVR23i
	MOV   A,#LOW  WRT_1
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH WRT_1
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
	CALL  cLDR23i
	MOV   DPTR,#DP_FLEX
	CALL  xSVR23i
	MOV   DPTR,#WR_MSKB
	MOV   A,#10000001B
	MOV   WR_MASK,A
	MOVX  @DPTR,A
	MOV   WR_UPDA,#0FFH

; Nastavi POT podle LNT a LINE_PO
; POZOR : sp=sp-2

SET_LNP:MOV   DPTR,#LINE_PO
	MOVX  A,@DPTR
	MOV   R7,A
	CALL  SEL_LT
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R6,A
	MOV   A,R7
	CLR   C
	SUBB  A,R6
	MOV   A,R7
	JC    SET_LN2
	JNB   ACC.7,SET_LN1
	MOV   R6,#1
SET_LN1:MOV   DPTR,#LINE_PO
	MOV   A,R6
	DEC   A
	MOVX  @DPTR,A
	JMP   SET_LNP
SET_LN2:RL    A
	ADD   A,#5
	CALL  ADDATDP
	CALL  cLDR23i
	JMP   SET_POT

; DPTR naplni ukazatelem na LNT
; zpracovavane radky

SEL_LT: CALL  GET_LAD
	MOVX  A,@DPTR
	INC   DPTR
	JNZ   SEL_LT2
SEL_LT1:CLR   A
	MOVX  @DPTR,A
SEL_LT2:MOVX  A,@DPTR
	ANL   A,#03FH
	CJNE  A,#LNT_MAX,SEL_LT3
SEL_LT3:JNC   SEL_LT1
SEL_LT4:ANL   A,#03FH
	RL    A
	MOV   DPTR,#LNT_DIR
	CALL  ADDATDP
	JMP   cMDPDP

; Orovani R2 xorovani R3 typu radky

SET_IMP:CALL  GET_LAD
	INC   DPTR
	MOVX  A,@DPTR
	ORL   A,R2
	XRL   A,R3
	MOVX  @DPTR,A
	MOV   WR_UPDA,#0FFH
	RET

TST_LI0:MOV   R1,#0

; Testuje radek na R3 s maskou R2
; R23=Time
; DPTR = [LINE_P]+R1

TST_LIN:CALL  GET_LAD
	MOVX  A,@DPTR
	JZ    TST_LI1
	INC   DPTR
	MOVX  A,@DPTR
	ANL   A,R2
	XRL   A,R3
	MOV   R0,A
	INC   DPTR
	CALL  xLDR23i
	MOV   A,R1
	CALL  ADDATDP
	MOV   A,R0
	RET
TST_LI1:SETB  F0
	MOV   A,#-1
	RET

; Jednotlive vstupy
			      ; Obecny vstup cisla
R_INTEG:CALL  R_GET_V0	      ; ve tvaru podle RD_V_DP
	CLR   A               ; ulozi ho na RD_DP
	MOVC  A,@A+DPTR       ; vola RD_ACTP
	ANL   A,#0F3H
	MOV   R7,A
	MOV   DPTR,#WR_POS
	MOVX  A,@DPTR
	MOV   R6,A
	MOV   DPTR,#R_BUFF
	MOVX  @DPTR,A
	CLR   F0
	CALL  INPUTi
	MOV   R2,A
	MOV   DPTR,#R_BUFF
	MOVX  A,@DPTR
	MOV   DPTR,#WR_POS
	MOVX  @DPTR,A
	CALL  R_GET_V0
	MOV   A,R2
	JZ    R_INTE1
	ORL   WR_UPDA,#07FH
	CLR   F0
	CJNE  A,#-1,R_INTRE
R_INTER:MOV   A,#7            ; Spatne zadani
	CALL  R_GET_V
	CALL  ERR_DPT
	SETB  F0
R_INTRE:JMP   RD_RET

R_INTE1:CLR   A
	MOVC  A,@A+DPTR
	INC   DPTR
	MOV   R1,A
	CALL  cLDR23i
	MOV   A,R1
	CPL   A
	ORL   A,R5
	JNB   ACC.7,R_INTE2
	CALL  CMPi
	JC    R_INTER
R_INTE2:CALL  cLDR23i
	MOV   A,R1
	ANL   A,R5
	JB    ACC.7,R_INTE3
	CALL  CMPi
	JZ    R_INTE3
	JNC   R_INTER
R_INTE3:CALL  cLDR23i
	MOV   A,R1
	ANL   A,#00CH
	JZ    R_INTE4
	CALL  MULi
	MOV   A,R1
	ANL   A,#008H
	JZ    R_INTE4
	MOV   A,R5
	MOV   R4,A
	MOV   A,R6
	MOV   R5,A
	MOV   A,R1
	ANL   A,#004H
	JZ    R_INTE4
	CALL  MR45R67
R_INTE4:MOV   DPTR,#DATA_CH
	MOV   A,#001H
	MOVX  @DPTR,A
	MOV   DPTR,#RD_DP
	CALL  xMDPDP
	MOV   C,EA
	CLR   EA
	CALL  xSVR45i
	MOV   EA,C
	CALL  R_JMACT
	MOV   A,#-2
	JB    F0,R_INTE5
	MOV   A,#K_ENTER
R_INTE5:JMP   RD_RET

; cteni jmena programu

R_TEXTP:CALL   PRP_PRG
	MOV    R2,#2
	MOV    R3,#0
	CALL   ADDi
	MOV    DPTR,#RD_DP
	CALL   xSVR45i
	CALL   GET_LAD
	INC    DPTR
	MOVX   A,@DPTR
	XRL    A,#040H
	JZ     R_TEXT
	JMP    R_CHAR

; cteni textu
; podle tvaru v [RD_V_DP]
; -pocet zobrazovanych znaku
; -maximalni delka+80H pri ins,del
; -


R_TEXT: CLR    A
	MOV    Xi,A
	MOV    Xi+1,A

R_TEXT3:CALL   INPUTc1
	MOV    R2,A
	CALL   R_TXTS
	JZ     R_TEXT6
	MOV    A,Xi
	MOV    DPTR,#RD_DP
	CALL   xMDPDPA
	MOVX   A,@DPTR
	ADD    A,R4
	JNB    ACC.7,R_TEXT4
	MOV    A,#07FH
R_TEXT4:MOVX   @DPTR,A
	ANL    A,#0E0H
	JNZ    R_TEXT5
	MOV    A,#020H
	MOVX   @DPTR,A
R_TEXT5:ORL    WR_UPDA,#07FH
	SJMP   R_TEXT3
R_TEXT6:MOV    DPTR,#RD_V_DP
	CALL   xLDR45i
	MOV    DPTR,#WR_POS
	MOVX   A,@DPTR
	CJNE   R2,#K_RIGHT,R_TEXT7
	INC    A
	MOVX   @DPTR,A
	INC    Xi
	MOV    A,Xi
	XRL    A,R4
	JNZ    R_TEXT3
	MOVX   A,@DPTR
	SJMP   R_TEXT7+3
R_TEXT7:CJNE   R2,#K_LEFT,R_TEXT8
	DEC    A
	MOVX   @DPTR,A
	DEC    Xi
	MOV    R3,Xi
	CJNE   R3,#-1,R_TEXT3
R_TEXT8:CLR    C
	SUBB   A,Xi
	MOVX   @DPTR,A
R_TEXT9:MOV    A,R2
	JMP    RD_RET

R_TXTS:	CJNE   R3,#0,R_TXTS1
	MOV    Xi+1,#0
R_TXTS1:MOV    A,Xi+1
	MOV    R5,#1
	ADD    A,#-8
	JNC    R_TXTS2
	MOV    R5,#4
	ADD    A,#-8
	JNC    R_TXTS2
	MOV    R5,#8
	DEC    Xi+1
R_TXTS2:INC    Xi+1
	MOV    R4,#0
	CJNE   R2,#K_UP,R_TXTS3
	MOV    A,R5
	MOV    R4,A
R_TXTS3:CJNE   R2,#K_DOWN,R_TXTS4
	MOV    A,R5
	CPL    A
	INC    A
	MOV    R4,A
R_TXTS4:MOV    A,R4
	RET

; plus 1
UP_BDP: MOV    R1,#1
	DB     90H            ; MOV DPTR,#n
; minus 1
DO_BDP: MOV    R1,#-1
	MOV    DPTR,#RD_DP

; Pricte k [RD_DP] R1,pri rovnosti
;    R2 == [RD_DP] nastavi [RD_DP] = R3

MO_BDP:	CALL   xMDPDP
	MOV    R0,IE
	CLR    EA
	MOVX   A,@DPTR
	ADD    A,R1
	XRL    A,R2
	JNZ    DO_BDP1
	MOV    A,R3
	XRL    A,R2
DO_BDP1:XRL    A,R2
	JMP    R_AUXU1

; Data na RD_DP oruje R2 a xoruje R3

R_AUXU: MOV   DPTR,#RD_DP
	CALL  xMDPDP
	MOV   R0,IE
	CLR   EA
	MOVX  A,@DPTR
	ORL   A,R2
	XRL   A,R3
R_AUXU1:MOVX  @DPTR,A
	MOV   R4,A
	MOV   IE,R0
	ORL   WR_UPDA,#07FH
R_JMACT:MOV   DPTR,#RD_ACTP
	CALL  xLDR23i
	MOV   A,R2
	ORL   A,R3
	JZ    R_AUXU2
JMPR23: MOV   DPL,R2
	MOV   DPH,R3
	CLR   A
	JMP   @A+DPTR

QUES_YN:MOV   WR_UPDA,#0FFH
	CALL  cPRINT
	MOV   DPTR,#QUES_YT
	CALL  cPRINT
	MOV   R7,#0
	MOV   A,#LCD_DON OR LCD_CON
	CALL  LCDWCOM
QUES_Y1:MOV   A,#LCD_HOM+49H
	CJNE  R7,#0,QUES_Y2
	MOV   A,#LCD_HOM+41H
QUES_Y2:CALL  LCDWCOM
QUES_Y3:MOV    A,R7           ; Komunikace uLan
	PUSH   ACC
	CALL   uL_IDLE
	POP   ACC
	MOV   R7,A
	CALL  ERR_CRY
	SETB  F0
	JNZ   QUES_Y4+3
	CALL  SCANKEY
	JZ    QUES_Y3
	CLR   F0
	CJNE  A,#K_1,QUES_Y4
	MOV   A,#1
R_AUXU2:RET
QUES_Y4:CJNE  A,#K_0,QUES_Y5
	CLR   A
	RET
QUES_Y5:CJNE  A,#K_LEFT,QUES_Y6
	MOV   R7,#0
QUES_Y6:CJNE  A,#K_RIGHT,QUES_Y7
	MOV   R7,#1
QUES_Y7:CJNE  A,#K_ENTER,QUES_Y1
	MOV   A,R7
	RET

; Vstup z klavesnice a vypis displeje
; ===================================

INPUTc: MOV    DPTR,#WR_POS
	MOVX   @DPTR,A
INPUTc1:MOV    DPTR,#WR_POS
	MOVX   A,@DPTR
	ORL    A,#LCD_HOM
	CALL   LCDWCOM
	MOV    A,#LCD_DON OR LCD_CON
	CALL   LCDWCOM

INPUTc2:CALL   uL_IDLK
	JNZ    INPUTcR
	CALL   SCANKEY
	JNZ    INPUTcR
	MOV    A,WR_MASK
	ANL    A,WR_UPDA
	JNZ    INPUTc3
	CALL   ERR_CRY        ; Hlidani kritickych chyb
	JNZ    INPUTcR
	SJMP   INPUTc2
INPUTc3:MOV    R2,A
	MOV    A,#LCD_DON
	CALL   LCDWCOM
	MOV    R3,#001H
	MOV    R4,#0FFH
INPUTc4:INC    R4
	MOV    A,R3
	RR     A
	MOV    R3,A
	ANL    A,R2
	JZ     INPUTc4
	CPL    A
	ANL    WR_UPDA,A
	MOV    DPTR,#WR_TAB
	CALL   xMDPDP
	MOV    A,R4           ; *3
	RL     A
	ADD    A,R4
	CALL   ADDATDP
	CLR    A
	MOVC   A,@A+DPTR
	INC    DPTR
	CALL   LCDWCOM
	MOV    A,#LOW  INPUTc1
	PUSH   ACC
	MOV    A,#HIGH INPUTc1
	PUSH   ACC
cJMPDPP:CLR    A
	MOVC   A,@A+DPTR
	INC    DPTR
	MOV    R1,A
	CLR    A
	MOVC   A,@A+DPTR
	INC    DPTR
	MOV    DPH,A
	MOV    DPL,R1
	CLR    A
	JMP    @A+DPTR

INPUTcR:MOV    R1,A
	MOV    A,#LCD_DON
	CALL   LCDWCOM
	MOV    A,R1
	CJNE   A,#K_HELP,WR_RET
HELP_ME:MOV    DPTR,#HLP_TXT
	CALL   xLDR45i
	CLR    A
	JMP    HELP_1
WR_RET: RET

ERR_DPB:SETB   %ALRM_FL
ERR_DPT:MOV    R1,#040H
	PUSH   DPL
	PUSH   DPH
ERR_DP0:SETB   %BEEP_FL
	CALL   LEDWR
	DJNZ   R2,ERR_DP0
	DJNZ   R1,ERR_DP0
	CLR    %BEEP_FL
	CALL   LEDWR
	POP    DPH
	POP    DPL
ERR_DP1:MOV    WR_UPDA,#0FFH
	CALL   cPRINT
	PUSH   DPL
	PUSH   DPH
ERR_DP2:CALL   uL_IDLE        ; Komunikace uLan
	CALL   SCANKEY
	JZ     ERR_DP2
	MOV    R1,A
	CLR    %ALRM_FL
	CLR    %BEEP_FL
	CALL   LEDWR
	MOV    A,R1
	POP    DPH
	POP    DPL
	CJNE   A,#K_HELP,ERR_DP3
	CLR    A
	MOVC   A,@A+DPTR
	JNZ    ERR_DP1
	MOV    A,#0FEH
ERR_DP3:CJNE   A,#K_ENTER,ERR_DP4
	MOV    A,#0FEH
ERR_DP4:RET

WAIT_P: MOV    DPTR,#TIMR_WAIT
	MOVX   @DPTR,A
WAIT_1: CALL   SCANKEY
	SETB   F0
	JNZ    WAIT_R
	CLR    F0
	MOV    DPTR,#TIMR_WAIT
	MOVX   A,@DPTR
	JNZ    WAIT_1
WAIT_R: RET

WR_FLIN:MOV    DPTR,#F_LINE   ; Vypis 1 radky
cWR_DPP:CALL   xMDPDP
	JMP    cPRINT

WR_FLEX:MOV    DPTR,#DP_FLEX  ; Vypis flexible
xJMPDPP:MOV    C,EA           ; Skok na x:[DPTR]
	CLR    EA
	MOVX   A,@DPTR
	PUSH   ACC
	INC    DPTR
	MOVX   A,@DPTR
	PUSH   ACC
	INC    DPTR
	MOV    EA,C
	RET

cMDPDP: CLR    A              ; DPTR = c:[DPTR]
	MOVC   A,@A+DPTR
	MOV    R0,A
	INC    DPTR
	CLR    A
	MOVC   A,@A+DPTR
	SJMP   xMDPDPR

R_GET_V0:CLR   A
R_GET_V:MOV    DPTR,#RD_V_DP
xMDPDPA:MOV    R0,A
	MOVX   A,@DPTR
	ADD    A,R0
	MOV    R0,A
	INC    DPTR
	MOVX   A,@DPTR
	ADDC   A,#0
	SJMP   xMDPDPR

GET_LAD:MOV    DPTR,#LINE_P
xMDPDP:	MOV    C,EA           ; DPTR = x:[DPTR]
	CLR    EA
	MOVX   A,@DPTR
	MOV    R0,A
	INC    DPTR
	MOVX   A,@DPTR
	MOV    EA,C
xMDPDPR:MOV    DPH,A
	MOV    DPL,R0
cxMOVER:RET

cxMOVEt:MOV    R1,#000H
cxMOVE: MOV    DPL,R2         ; R01 x x:[R45] = c:[R23]
	MOV    DPH,R3
cxMOVE1:MOV    A,R0           ; R01 x x:[R45] = c:[DPTR]
	ORL    A,R1
	JZ     cxMOVER
	CLR    A
	MOVC   A,@A+DPTR
	INC    DPTR
	MOV    R2,DPL
	MOV    R3,DPH
	MOV    DPL,R4
	MOV    DPH,R5
	MOVX   @DPTR,A
	INC    DPTR
	MOV    R4,DPL
	MOV    R5,DPH
	MOV    A,R0
	DEC    R0
	JNZ    cxMOVE
	DEC    R1
	SJMP   cxMOVE

xxMOVEt:MOV    R1,#000H
xxMOVE: MOV    DPL,R2         ; R01 x x:[R45] = x:[R23]
	MOV    DPH,R3
xxMOVE1:MOV    A,R0           ; R01 x x:[R45] = x:[DPTR]
	ORL    A,R1
	JZ     cxMOVER
	MOVX   A,@DPTR
	INC    DPTR
	MOV    R2,DPL
	MOV    R3,DPH
	MOV    DPL,R4
	MOV    DPH,R5
	MOVX   @DPTR,A
	INC    DPTR
	MOV    R4,DPL
	MOV    R5,DPH
	MOV    A,R0
	DEC    R0
	JNZ    xxMOVE
	DEC    R1
	SJMP   xxMOVE

XCDPR01:XCH    A,R0           ; DPTR <-> R01
	XCH    A,DPL
	XCH    A,R0
	XCH    A,R1
	XCH    A,DPH
	XCH    A,R1
	RET

ADDATDP:ADD    A,DPL          ; DPTR = DPTR + A
	MOV    DPL,A
	CLR    A
	ADDC   A,DPH
	MOV    DPH,A
	RET

; Funkci se preda v DP ukazatel na tabulku :
; +0 .. Ukazatel na text menu - format HELP
; +2 .. Ukazatel na text helpu
; +4 .. Ukazatel na SFT
; +6 .. Seznam operaci pri funkcich (4byte)

MENU_FD:MOV    R2,DPL
	MOV    R3,DPH
MENU_F: MOV    A,R2
	MOV    R4,A
	MOV    A,R3
	MOV    R5,A
	MOV    DPTR,#SEL_VEC
	CALL   xSVR45i
MENU_F1:MOV    DPTR,#SEL_VEC
	CALL   xMDPDP
	CALL   xMDPDP
	MOV    R4,DPL
	MOV    R5,DPH
	MOV    A,#001H

HELP_1: MOV    DPTR,#HLP_FN
	MOVX   @DPTR,A
HELP_2: MOV    DPTR,#HLP_POS
	CALL   xSVR45i
	CLR    A
HELP_3: MOV    DPTR,#HLP_DIS
	MOVX   @DPTR,A
HELP_4: MOV    DPTR,#HLP_DIS
	MOVX   A,@DPTR
	ADD    A,#40H
	JNC    HELP_5
	MOVX   @DPTR,A        ; Posun okna nahoru
	MOV    DPTR,#HLP_POS
	CALL   HELP_LU
	JC     HELP_6
	MOV    R4,DPL
	MOV    R5,DPH
	MOV    DPTR,#HLP_POS
	CALL   xSVR45i
	SJMP   HELP_6
HELP_5: ADD    A,#40H
	JNC    HELP_6
	ADD    A,#40H         ; Posun okna dolu
	MOVX   @DPTR,A
	MOV    R1,#3
	MOV    DPTR,#HLP_POS
	CALL   HELP_LN
	CJNE   R1,#0 ,HELP_6
	MOV    R4,DPL
	MOV    R5,DPH
	MOV    DPTR,#HLP_POS
	CALL   xSVR45i
HELP_6: MOV    DPTR,#HLP_POS
	MOV    A,#LCD_CLR
	CALL   LCDWCOM
	MOV    R1,#1
	CALL   HELP_LN        ; Tisk 1. radky
	MOV    A,#LCD_HOM+040H
	CALL   LCDWCOM
	CALL   HELP_L1        ; Tisk 2. radky
	MOV    DPTR,#HLP_DIS
	MOVX   A,@DPTR
HELP_7: MOV    R6,A
	MOV    DPTR,#HLP_POS
	CALL   xMDPDP
	MOV    A,R6
	ADD    A,#-40H
	JNC    HELP_8
	MOV    R1,#3          ; Druha radka
	CALL   HELP_L1
	MOV    A,R1
	JZ     HELP_8
	MOV    A,R6
	ADD    A,#-40H
	SJMP   HELP_7
HELP_8: MOV    A,R6
	ANL    A,#03FH
	MOV    R7,A
	MOV    R1,#0
HELP_9: MOV    A,R1
	MOV    R0,A
	CALL   cLDR23i
	CLR    A
	MOVC   A,@A+DPTR
	INC    DPTR
	CJNE   A,#0FFH,HELP_10
	MOV    R1,#0FH
	SJMP   HELP_11
HELP_10:DEC    A
	MOV    R1,A
	CLR    C
	SUBB   A,R7
	JC     HELP_9
HELP_11:MOV    DPTR,#HLP_DIS
	MOV    A,R6
	ANL    A,#0C0H
	ORL    A,R1
	MOVX   @DPTR,A
	ORL    A,#LCD_HOM
	CALL   LCDWCOM
	MOV    A,#LCD_DON OR LCD_CON
	CALL   LCDWCOM
	MOV    A,R2
	MOV    R4,A
	MOV    A,R3
	MOV    R5,A
HELP_12:MOV    A,R0           ; Komunikace uLan
        PUSH   ACC
        MOV    A,R4
        PUSH   ACC
        MOV    A,R5
        PUSH   ACC
        CALL   uL_IDLE
        POP    ACC
        MOV    R5,A
        POP    ACC
        MOV    R4,A
        POP    ACC
        MOV    R0,A
	CALL   ERR_CRY        ; Hlidani kritickych chyb
	JNZ    HELP_19
	CALL   SCANKEY
	JZ     HELP_12
	CJNE   A,#K_ENTER,HELP_14
	MOV    A,R4
	ANL    A,R5
        INC    A
	JZ     HELP_13
        MOV    DPTR,#HLP_FN
	MOVX   A,@DPTR
	JNZ    MENU_F2
	JMP    HELP_2
HELP_13:MOV    A,#080H
	JMP    HELP_3
HELP_14:MOV    DPTR,#HLP_DIS
	CJNE   A,#K_RIGHT,HELP_15
        MOVX   A,@DPTR
	INC    A
	JMP    HELP_3
HELP_15:CJNE   A,#K_UP,HELP_16
        MOVX   A,@DPTR
	ADD    A,#-40H
        JMP    HELP_3
HELP_16:CJNE   A,#K_DOWN,HELP_17
        MOVX   A,@DPTR
	ADD    A,#40H
        JMP    HELP_3
HELP_17:CJNE   A,#K_LEFT,HELP_18
        MOVX   A,@DPTR
	ANL    A,#0C0H
	ORL    A,R0
	JMP    HELP_3
HELP_18:MOV    DPTR,#HLP_FN
	MOVX   A,@DPTR
	CJNE   R2,#K_HELP,MENU_F2
	JNZ    MENU_F2
        MOV    R4,#LOW HELP_HL
	MOV    R5,#HIGH HELP_HL
	JMP    HELP_2
HELP_19:MOV    A,#0FEH
        MOV    WR_UPDA,#0FFH
        RET

MENU_F2:CJNE   A,#1,HELP_19
	MOV    DPTR,#SEL_VEC
	CALL   xMDPDP
	CJNE   R2,#K_ENTER,MENU_F4
	MOV    A,R4
        MOV    R2,A
	MOV    A,R5
	MOV    R3,A
	CJNE   A,#0FFH,MENU_F3
	MOV    A,R2
	RL     A
        RL     A
	ADD    A,#006H
	CALL   ADDATDP
	CALL   cLDR23i
        JMP    cJMPDPP
MENU_F3:JMP    MENU_F
MENU_F4:INC    DPTR
	INC    DPTR
	CJNE   R2,#K_HELP,MENU_F5
	CALL   xLDR45i
        CLR    A
	CALL   HELP_1
	JMP    MENU_F1
MENU_F5:INC    DPTR
	INC    DPTR
	CALL   xMDPDP
	MOV    A,R2
	MOV    R7,A
	JMP    SEL_FNC

HELP_LN:CALL   xMDPDP         ; Prechod na nasledujici
HELP_L1:INC    DPTR           ; radku a pri R1=1 i tisk
	INC    DPTR
	CLR    A
	MOVC   A,@A+DPTR
	INC    DPTR
	ADD    A,#02
	JZ     HELP_LR
	CJNE   A,#001H,HELP_L1
	CJNE   R1,#1,HELP_L2
	JMP    cPRINT
HELP_L2:
HELP_L3:CLR    A
	MOVC   A,@A+DPTR
	INC    DPTR
	JNZ    HELP_L3
	CJNE   R1,#3,HELP_L4
	CLR    A
	MOVC   A,@A+DPTR
	ADD    A,#-0FEH
	JZ     HELP_LR
	MOV    R1,#0
HELP_L4:
HELP_LR:RET

HELP_LU:CALL   xMDPDP
HELP_U1:CALL   DECDPTR
	CLR    A
	MOVC   A,@A+DPTR
	ADD    A,#2
	JZ     HELP_UR
	CJNE   A,#001H,HELP_U1
HELP_U2:CALL   DECDPTR
	CALL   DECDPTR
	CALL   DECDPTR
	CLR    A
	MOVC   A,@A+DPTR
	JZ     HELP_U3
	CJNE   A,#0FEH,HELP_U2
HELP_U3:CLR    C
HELP_UR:INC    DPTR
	RET

QUES_YT:DB    C_LIN2,' No [0]  Yes [1]',0

ERR_TIT:DB    LCD_HOME,'Time not cont.  ',0
	DB    LCD_HOME,'Check time of   '
	DB    C_LIN2,  'prev and next ln',0,0

T_DL_PR:DB    LCD_CLR,'Clear program',0

T_MEM_O:DB    LCD_HOME,'Memory full !   ',0
	DB    LCD_HOME,'Write shorter   ',0,0


	END