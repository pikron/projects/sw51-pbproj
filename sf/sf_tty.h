;********************************************************************
;*                    LCP 4000 - SF_TTY.H                           *
;*     Include file se scankody pro KBD  a Headery                  *
;*                  Stav ke dni 24.03.1991                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

$INCLUDE(%INCH_LCD)

;********************************************************************

; Scan kody klaves

K_0      EQU   01BH
K_1      EQU   015H
K_2      EQU   018H
K_3      EQU   016H
K_4      EQU   021H
K_5      EQU   024H
K_6      EQU   022H
K_7      EQU   00FH
K_8      EQU   012H
K_9      EQU   010H
K_DP     EQU   01EH
K_PM     EQU   0FFH ; Neni definovana
K_ENTER  EQU   01CH

K_PROG   EQU   011H
K_CTRL   EQU   00DH
K_LEFT   EQU   023H
K_RIGHT  EQU   01FH
K_UP     EQU   00EH
K_DOWN   EQU   020H
K_INS    EQU   017H
K_DEL    EQU   014H
K_MODE   EQU   013H
K_HELP   EQU   00BH
K_LIST   EQU   008H
K_CYCLE  EQU   007H
K_RUN    EQU   004H
K_END    EQU   003H

K_ZERO   EQU   006H
K_ABS    EQU   00AH
K_UVH    EQU   001H
K_UVL    EQU   002H
K_VIS    EQU   005H
K_LOFF   EQU   01DH
K_PRET   EQU   01AH
K_MARK   EQU   019H
K_ATTEN  EQU   00CH
K_AUX    EQU   009H

K_H_A    EQU   K_LOFF
K_H_B    EQU   K_PRET
K_H_C    EQU   K_MARK
K_H_D    EQU   K_INS
K_H_E    EQU   K_DEL
K_H_F    EQU   K_MODE

;********************************************************************

; Kody led diod

LFB_BEEP   EQU  7
LFB_ALRM   EQU  6
LFB_LHIG   EQU  5
LFB_LLOW   EQU  4

LFB_LVIS   EQU  2
LFB_RUN    EQU  1
LFB_PROG   EQU  0

%DEFINE (BEEP_FL)  (LED_FLG.LFB_BEEP)
%DEFINE (ALRM_FL)  (LED_FLG.LFB_ALRM)
%DEFINE (LHIG_FL)  (LED_FLG.LFB_LHIG)
%DEFINE (LLOW_FL)  (LED_FLG.LFB_LLOW)
%DEFINE (LVIS_FL)  (LED_FLG.LFB_LVIS)
%DEFINE (RUN_FL)   (LED_FLG.LFB_RUN)
%DEFINE (PROG_FL)  (LED_FLG.LFB_PROG)

EXTRN    CODE(LCDINST,LCDNBUS,LCDWCOM,LCDWCO1,LCDWR,LCDWR1)
EXTRN    CODE(PRINT,PRINTH,xPRINT,cPRINT)

EXTRN    CODE(SCANKEY)

EXTRN    CODE(LEDWR)
EXTRN    DATA(LED_FLG)

