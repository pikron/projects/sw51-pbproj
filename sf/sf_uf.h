;********************************************************************
;*                    LCP 4000 - LP_UF.H                            *
;*                       User interface                             *
;*                  Stav ke dni 11.11.1992                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

; Rutiny vyuzivatelne z USER INTER-FACE modulu

EXTRN   CODE(SET_DIT,SET_POT,SET_PO1,R_CHAR,RD_RET)
EXTRN   CODE(SEL_FN,SEL_FNC,SP_TIME,SP_TIMS,DEL_LN)
EXTRN   CODE(DEL_LN0,DEL_SPC,INS_LN,INS_SPC,INS_SP0)
EXTRN   CODE(NUL_SPC,FND_PGE,PREV_LN,PREV_L0,NEXT_LN)
EXTRN   CODE(SET_PRE,SET_NEX,ENT_PRG,MOD_LNP,PROG_IN)
EXTRN   CODE(SET_LN,SET_LNP,SET_LN1,SEL_LT,SEL_LT1)
EXTRN   CODE(SET_IMP,R_INTEG,R_AUXU,QUES_YN,INPUTc)
EXTRN   CODE(INPUTc1,ERR_DPB,ERR_DPT,ERR_DP1,WAIT_P)
EXTRN   CODE(WAIT_1,WR_FLEX,xJMPDPP,cMDPDP,xMDPDP)
EXTRN   CODE(xMDPDPA)
EXTRN   CODE(cxMOVEt,cxMOVE1,ADDATDP,MENU_FD,MENU_F)
EXTRN   CODE(HELP_1,WR_FLIN,cWR_DPP,WR_RET,SEL_FN2)
EXTRN   CODE(UP_BDP,DO_BDP,R_JMACT,ENT_PR1,MO_BDP)
EXTRN   CODE(GET_LAD,TST_LI0,TST_LIN,GO_PRBE)
EXTRN   CODE(cxMOVE,xxMOVEt,xxMOVE,xxMOVE1,SET_LAD)
EXTRN   CODE(SET_MENU,XCDPR01,SET_PROG,WR_PROG)
EXTRN   CODE(INS_PRG,DEL_PRG,TST_PRG,R_TEXT,R_TEXTP)

EXTRN   XDATA(DATA_CH,PROG_SF,PROG_NU)
EXTRN   XDATA(PROG_BE,PROG_EN,LINE_P,LINE_PO)

EXTRN   XDATA(POT,DIT)

EXTRN   XDATA(WR_TAB,HLP_TXT,WR_RETB,RD_DP,RD_V_DP,RD_ACTP)

; SEL_VEC,HLP_TXT,WR_POS,RD_VEC,RD_DP,RD_V_DP,RD_ACTP

;WR_MSKB,F_LINE,WR_TAB,DP_FLEX,WR_RETB,

;R_INTEB,HLP_DIS,HLP_FN,HLP_POS

; Vyuziva rutiny z techto modulu
;
;   LP_TTY.H,LP_AI.H,LP_ADR.H

; Potrebuje tyto vnejsi data z hlavniho modulu

;   WR_UPDA   .. priznaky zmeny udaju
;   WR_MASK   .. ktere udaje prepisovat na displeji
;   WRT_1     .. tabulka vyuzivana pro program - pouze WR_FLEX,F_line
;   LNT_DIR   .. tabulka ukazatelu na popisy radek programu
;   ERR_CRY   .. rutina pro zpracovani krytickych chyb
;        A=0    vse v poradku - nesmi menit registry - smi pouzit DPTR
;        A=0FEh chyba - A je vraceno jako precteny znak
;                       smi menit registry
;   HELP_HL   .. help-help  uvod do systemu napoved - xdata
;   LNT_MAX   .. pocet typu radek programu = poctu ukazatelu v LNT_DIR
;   TIMR_WAIT .. casovac pro cekani s vypisy
;   PROGRAM   .. xdata - pocatek oblasti pro program
;   PROGR_E   .. konec teto pameti
