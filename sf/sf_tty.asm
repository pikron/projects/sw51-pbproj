;********************************************************************
;*                    LCP 4000 - SF_TTY.ASM                         *
;*     Obsluha LCD displaye a klavesnice                            *
;*                  Stav ke dni 18.06.1991                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

	  EXTRN   XDATA(KBDTIMR)	; softvareovy timer napojeny na cas

	  PUBLIC LCDINST,LCDINSM,LCDNBUS,LCDWCOM,LCDWCO1,LCDWR,LCDWR1
          PUBLIC PRINT,PRINTH,xPRINT,cPRINT

	  PUBLIC SCANKEY,TESTKEY
	  PUBLIC KBDSTDB

	  PUBLIC LEDWR,LED_FLG,LED_FLH,KBD_FLG

$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_ADR)
$INCLUDE(%INCH_LCD)

TTY___B SEGMENT DATA BITADDRESSABLE
RSEG TTY___B
LED_FLG:DS    1
LED_FLH:
KBD_FLG:DS    1

TTY___C SEGMENT CODE

TTY___X SEGMENT XDATA

RSEG TTY___X

KBDFL:  DS    1

RSEG TTY___C

LCDINST:MOV   A,#LCD_MOD
LCDINSM:MOV   DPTR,#LCD_INST
	MOVX  @DPTR,A
        MOV   R0,#0
LCDINS1:DJNZ  R0,LCDINS1
        MOVX  @DPTR,A
LCDINS2:DJNZ  R0,LCDINS2
        MOV   A,#LCD_CLR
	CALL  LCDWCOM
        MOV   A,#LCD_NROL
        CALL  LCDWCOM
        MOV   A,#LCD_DON OR LCD_CON
        CALL  LCDWCOM
        MOV   A,#LCD_NSH
        CALL  LCDWCOM
        MOV   A,#LCD_CLR
        CALL  LCDWCOM
        RET


LCDNBUS:PUSH  DPH
        PUSH  DPL
LCDNBU1:MOV   DPTR,#LCD_STAT
        MOVX  A,@DPTR
        ANL   A,#LCD_BF
        JNZ   LCDNBU1
        MOVX  A,@DPTR
        POP   DPL
        POP   DPH
        RET

LCDWCOM:PUSH  ACC
        CALL  LCDNBUS
        POP   ACC
LCDWCO1:PUSH  DPH
        PUSH  DPL
        MOV   DPTR,#LCD_INST
        MOVX  @DPTR,A
        POP   DPL
        POP   DPH
        RET

LCDWR:  PUSH  ACC
        CALL  LCDNBUS
        POP   ACC
LCDWR1: CJNE  A,#C_LIN2,LCDWR2
        MOV   A,#LCD_HOM+040H
        SJMP  LCDWCO1
LCDWR2: JC    LCDWCO1
        PUSH  DPH
        PUSH  DPL
        MOV   DPTR,#LCD_WDATA
        MOVX  @DPTR,A
        POP   DPL
        POP   DPH
PRINTRE:RET

PRINTH: CALL  LCDNBUS
        MOV   A,#LCD_HOM
        CALL  LCDWCO1
PRINT:  MOV   DPL,R1
        MOV   DPH,R2
        CJNE  R3,#002H,cPRINT  ; NEDORESENE - NAVAZNOST NA C

xPRINT: CALL  LCDNBUS
        MOVX  A,@DPTR
        INC   DPTR
        JZ    PRINTRE
        CALL  LCDWR1
        SJMP  xPRINT

cPRINT: CALL  LCDNBUS
	CLR   A
	MOVC  A,@A+DPTR
	INC   DPTR
	JZ    PRINTRE
	CALL  LCDWR1
	SJMP  cPRINT

; Test kodu klavesy v ACC
; =======================
; vraci: A ..  0 je stisknuta jinak neni
; rusi:  DP,R0

TESTKEY:DEC   A
	ANL   A,#3FH
	MOV   R0,B
	MOV   B,#6
	DIV   AB
	XCH   A,R0
	XCH   A,B
	ADD   A,#TESTKET-TESTKE1
	MOVC  A,@A+PC
TESTKE1:MOV   DPTR,#KBD
	MOV   C,LED_FLH.7
	MOV   ACC.7,C
	MOV   C,LED_FLH.6
	MOV   ACC.6,C
	MOVX  @DPTR,A
	MOV   DPTR,#KBD
	MOVX  A,@DPTR
	XCH   A,R0
	ADD   A,#TESTKET-TESTKE2
	MOVC  A,@A+PC
TESTKE2:XRL   A,R0
	ANL   A,#3FH
	RET

TESTKET:DB    11111110B
	DB    11111101B
	DB    11111011B
	DB    11110111B
	DB    11101111B
	DB    11011111B

; Cteni klavesnice
; ================
; vraci: A .. kod stisknute klavesy nebo 0
; meni : A, DP, R2, R3

TIM_REP EQU   5  ; Pocet taktu repeatu
TIM_PUS EQU   20 ; Cekani po stisku
TIM_OFF EQU   3  ; Delka uvolneni

SCANKEY:MOV   DPTR,#KBDTIMR
	MOVX  A,@DPTR
	MOV   R2,A
	MOV   DPTR,#KBD
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   DPTR,#KBDFL
	MOVX  A,@DPTR
	JZ    SCANKE1
	XCH   A,R3
	CPL   A
	CJNE  R2,#0,SCANKR1
	XRL   A,R3
	ANL   A,#03FH
	JZ    SCANKE2
SCANKE1:MOV   A,#TIM_PUS-TIM_REP
SCANKE2:ADD   A,#TIM_REP      ; rychlost repeatu
	MOV   DPTR,#KBDTIMR
	MOVX  @DPTR,A

	MOV   DPTR,#KBD
	MOV   R2,#006H
	MOV   R3,#NOT 040H
SCANKE3:MOV   A,R3
	RR    A
	MOV   R3,A
	MOVX  @DPTR,A
	MOVX  A,@DPTR
	CPL   A
	ANL   A,#03FH
	JNZ   SCANKE4
	DJNZ  R2,SCANKE3
SCANKE4:MOV   DPTR,#KBDFL
	MOV   R3,A
	MOVX  A,@DPTR
	XCH   A,R3
	MOVX  @DPTR,A
	JZ    SCANKR3
	MOV   DPL,R3
	MOV   R3,#0FFH
SCANKE5:INC   R3
	RRC   A
	JNC   SCANKE5
	MOV   A,R3
	RL    A
	ADD   A,R3
	RL    A
	ADD   A,R2
	MOV   R2,A
	MOV   R3,DPL
	MOV   A,#1
	JMP   KBDBEEP

SCANKR1:ORL   A,#080H
	ANL   A,R3
	MOV   R2,#TIM_OFF     ; doba uvolneni
	JZ    SCANKR2
	XRL   A,#080H
	JZ    SCANKRE
	JB    ACC.7,SCANKRE
	MOV   R2,#TIM_PUS     ; penalta na kartac
SCANKR2:MOV   A,R3
	XRL   A,#080H
	MOVX  @DPTR,A
	MOV   A,R2
SCANKR3:MOV   DPTR,#KBDTIMR
	MOVX  @DPTR,A
SCANKRE:CLR   A
	RET

KBDBEEP:
KBDSTDB:MOV   DPTR,#LED       ; Pipnuti
	ORL   LED_FLG,#080H
	MOV   A,LED_FLG
	MOVX  @DPTR,A
	MOV   A,#040H
KBDSTB1:DJNZ  ACC,KBDSTB1
	ANL   LED_FLG,#NOT 080H
	MOV   A,LED_FLG
	MOVX  @DPTR,A
	MOV   A,R2
	RET


; Nastaveni indikacnich LED podle A
; =================================

LEDWR:  MOV   A,LED_FLG
        MOV   DPTR,#LED
        MOVX  @DPTR,A
        RET

END
