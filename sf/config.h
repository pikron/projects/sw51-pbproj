;********************************************************************
;*                     CONFIG.ASM                                   *
;*     Konfiguracni soubor pro kompilaci LCD4000                    *
;*                  Stav ke dni 08.03.1996                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

%DEFINE (VERSION) (LCD4000 v1.1)
%DEFINE (HW_VERSION) (SF)
%DEFINE (USE_WR_MASK) (1)
%DEFINE (PBLIBDIR) (..\PBLIB\)

%IF (%EQS(%HW_VERSION,SF)) THEN (
; Stara verze detektoru
%DEFINE (IN_TTY)(0)
%DEFINE (INCH_TTY) (SF_TTY.H)
%DEFINE (INCH_LCD) (%PBLIBDIR%()PB_LCD.H)
%DEFINE (INCH_AI)  (%PBLIBDIR%()PB_AI.H)
%DEFINE (INCH_AL)  (%PBLIBDIR%()PB_AL.H)
%DEFINE (INCH_ADR) (SF_ADR.H)
%DEFINE (INCH_UF)  (SF_UF.H)
%DEFINE (INCH_ULAN)(%PBLIBDIR%()ULAN.H)
%DEFINE (INCH_UL_OI)(%PBLIBDIR%()UL_OI.H)
%DEFINE (INCH_MMAC)(%PBLIBDIR%()PB_MMAC.H)
%DEFINE (PROCESSOR_TYPE) (31)
%DEFINE (VECTOR_TYPE) (DYNAMIC)
%DEFINE (WATCHDOG) (
)
)FI

%IF (%EQS(%HW_VERSION,PB)) THEN (
; Nova verze
%DEFINE (INCH_TTY) (%PBLIBDIR%()PB_TTY.H)
%DEFINE (INCH_LCD) (%PBLIBDIR%()PB_LCD.H)
%DEFINE (INCH_AI)  (%PBLIBDIR%()PB_AI.H)
%DEFINE (INCH_AL)  (%PBLIBDIR%()PB_AL.H)
%DEFINE (INCH_ADR) (%PBLIBDIR%()PB_ADR.H)
%DEFINE (INCH_UF)  (SF_UF.H)
%DEFINE (INCH_ULAN)(%PBLIBDIR%()ULAN.H)
%DEFINE (INCH_UL_OI)(%PBLIBDIR%()UL_OI.H)
%DEFINE (INCH_MMAC)(%PBLIBDIR%()PB_MMAC.H)

%DEFINE (PROCESSOR_TYPE) (552)
%DEFINE (VECTOR_TYPE) (DYNAMIC)
%DEFINE (WATCHDOG) (
	ORL   PCON,#10H
	MOV   T3,#080H
)
)FI

; Processor depended features

%IF (%EQS(%PROCESSOR_TYPE,552)) THEN (
%DEFINE (INCH_REGS) (%PBLIBDIR%()REG552.H)
)FI

%IF (%EQS(%PROCESSOR_TYPE,31)) THEN (
%DEFINE (INCH_REGS) (%PBLIBDIR%()NULL.H)
)FI

; Zpusob definice vektoru preruseni a sluzeb

%IF (%EQS(%VECTOR_TYPE,STATIC)) THEN (
%*DEFINE (VECTOR  (VECNUM,VECADR)) ()
%*DEFINE (SVECTOR (VECNUM,VECADR)) (
CSEG    AT    %VECNUM
	JMP   %VECADR
)
)FI

%IF (%EQS(%VECTOR_TYPE,DYNAMIC)) THEN (
	EXTRN CODE(VEC_SET)
%*DEFINE (SVECTOR (VECNUM,VECADR)) ()
%*DEFINE (VECTOR  (VECNUM,VECADR)) (
	MOV   R4,#%VECNUM
	MOV   DPTR,#%VECADR
	CALL  VEC_SET
)
)FI

