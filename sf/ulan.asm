;********************************************************************
;*                    uLAN pro 8051 - ULAN.ASM                      *
;*     Komunikace  RS - 232  a  RS - 485                            *
;*                  Stav ke dni  3. 4.1992                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

%DEFINE (DEBUG_FL)  (0)       ; Povoleni vlozeni debug rutin
%DEFINE (VECTOR_FL) (1)       ; Vyuzivaji se vektory preruseni v RAM

%IF (%DEBUG_FL) THEN (
  EXTRN   CODE(SCANKEY,PRINThw,PRINThb,LCDWCOM,LCDWR)
)FI

%IF (%VECTOR_FL) THEN (
  EXTRN    CODE(VEC_SET)
  V_uL_ADD EQU   1EH
  V_uL_FNC EQU   26H
)ELSE(
  EXTRN    CODE(uL_R_BU,uL_R_CO)
)FI

EXTRN	CODE(uL_IDB,uL_IDE)	; Jmeno modulu

PUBLIC  uL_FNC,uL_ADR,uL_GRP,uL_SPD,uL_HBIB,uL_IB_L,uL_OB_L
PUBLIC  uL_FORM,uL_INIT
PUBLIC  uL_STR,uL_ERR,uLF_ERR,uLF_INE,uLF_KMP,uLF_TRP
PUBLIC  uL_FLG,uL_FLH

PUBLIC  uL_O_OP,uL_WR,uL_O_CL,uL_WRB,uL_O_LN
PUBLIC  uL_I_OP,uL_RD,uL_I_CL,uL_RDB,uL_I_LN
PUBLIC  uL_S_OP,uL_S_CL,uL_S_WR,uL_O_DO
PUBLIC  uL_SBBB,uL_SBP,uL_SBCO,uL_SBLE

; Funkce volane z rutin zpracovani prikazu
PUBLIC  ACK_CMD,SND_BEB,SND_CHC,SND_END,S_WAITD,NAK_CMD
PUBLIC	        REC_BEG,REC_CHR,REC_END,REC_CME
PUBLIC  REC_Bi, SND_Bi, REC_Bx, SND_Bx, S_R0FB, S_EQP
; Data
PUBLIC  uL_SA,uL_CMD


DR_EO   BIT   P1.7    ; Aktivni v 0
DR_EI   BIT   P1.6    ; Aktivni v 0 pro RS-486
		      ; Aktivni v 1 pro RS-232
END_RAM XDATA 0F800H  ; Konec pameti
LENG_IB EQU   8       ; Delka vstupniho bufferu v 256 bytu
LENG_OB EQU   8       ; Delka vystupniho bufferu
S_SPEED EQU   6       ; Bd=OSC/12/16/S_SPEED
S_FRLN  EQU   1024    ; maximalni delka ramce
C_SBCO  EQU   7       ; pocet rychlych bufferu
C_SBLE  EQU   128     ; delka bufferu

SER___C SEGMENT CODE
SER___X SEGMENT XDATA
SER___B SEGMENT DATA BITADDRESSABLE
SER_STACK SEGMENT DATA

%IF (NOT %VECTOR_FL)THEN(
CSEG AT SINT
	JMP   S_INT
	JMP   uL_FNC

CSEG AT TIMER1
	JMP   S_INT
)FI

RSEG SER___X

;********************************************************************
; Datova oblast site uLAN

uL_SBLE:DS    1  ;    Speed block len
uL_SBCO:DS    1  ;    Speed blocks count
uL_SBBB:DS    2  ;    Speed blocks buffer begin
uL_FORM:DS    1  ;    Nastaveni prenosu
uL_GRP: DS    1  ;    Skupina ucastnika site
uL_ADR: DS    1  ;    Adresa daneho ucastnika site
uL_HBIB:DS    1  ;    Vyssi cast adresy zacatku IB
uL_IB_L:DS    1  ;    Delka IB v 256
uL_OB_L:DS    1  ;    Delka OB v 256
uL_SPD: DS    1  ;    Delitel frekvence 57.6 kHz
uL_CMD: DS    1  ;    Prave prenaseny prikaz
uL_SA:  DS    1  ;    Adresa vysilace
uL_FRLN:DS    2  ;    Maximalni delka ramce

; Prijem dat

P_NDB:  DS    2  ;R23 Ukazatel na pocatek prijimaneho bloku
H_BIB:  DS    1  ;R4  Pocatek vstupniho buferu
H_EIB:  DS    1  ;R5  Konec vstupniho buferu
P_NPD:  DS    2  ;R67 Ukazatel na pocatek 1. nezprac. bloku
P_AID:  DS    2  ;    Ukazatel na zpracovavana data

; Vysilani dat

P_AOB:  DS    2  ;R23 Ukazatel na vysilany blok
H_BOB:  DS    1  ;R4  Pocatek vystupniho buferu
H_EOB:  DS    1  ;R5  Konec vystupniho buferu
P_NOB:  DS    2  ;R67 Ukazatel na zapisovany blok
P_AOD:  DS    2  ;    Ukazatel na zapisovana data

P_EID:  DS    2  ;    Zjisteny konec vstupnich dat
uL_IADR:DS    1  ;    SA zpracovavaneho bloku
uL_ICOM:DS    1  ;    Command zpracovavaneho bloku
uL_DOP: DS    2  ;    Direct output pointer
uL_SBP: DS    2  ;    Ukazatel na zapisovany buffer

PROC_BUF:DS   17 ; Buffer prikazu proceed
PROC_BUFE:

RSEG SER___B

uL_FLG: DS    1
uLF_ER0 BIT   uL_FLG.0  ; Chyba pri vysilani
uLF_ER1 BIT   uL_FLG.1  ; Chyba pri vysilani
uLF_ERR BIT   uL_FLG.2  ; Chyba pri vysilani
uLF_SN  BIT   uL_FLG.3  ; uLan vysila - master mode
uLF_RS  BIT   uL_FLG.4  ; Potreba vysilat
uLF_NB  BIT   uL_FLG.5  ; Zbernice neobsazena
uLF_NA  BIT   uL_FLG.6  ; Mazano kazdou akci
uLF_TRP BIT   uL_FLG.7  ; Jiny duvod preruseni

uL_FLH: DS    1
uLF_INE BIT   uL_FLH.0  ; Zprava presunuta do vstupni fronty
uLF_KMP BIT   uL_FLH.1  ; Keyboard macro in progress
uLF_DOP BIT   uL_FLH.2  ; Direct output in progress

RSEG	SER_STACK
	DS    5

XSEG AT END_RAM-256*LENG_IB-256*LENG_OB

BEG_IB: DS    100H*LENG_IB
END_IB:
BEG_OB: DS    100H*LENG_OB
END_OB:

; Rizeni linky - prikazy

uL_ERRI EQU   0FFH ; Ignoruj vse doslo k chybe
uL_ERR  EQU   07FH ; Chyba v datech
uL_END  EQU   07CH ; Konec dat
uL_ARQ  EQU   07AH ; Konec dat - vysli ACK
uL_PRQ  EQU   079H ; Konec dat - proved prikaz
uL_AAP  EQU   076H ; ARQ + PRQ
uL_BEG  EQU   075H ; Zacatek dat

; Potvrzovaci zpravy
uL_ACK  EQU   019H ; Potvrzeni
uL_NAK  EQU   07FH ; Doslo k chybe
uL_WAK  EQU   025H ; Ted nemohu splnit

; Prikazy posilane linkou
; 00H .. 3FH    uloz do bufferu
; 40H .. 7FH    uloz do bufferu bez ACK
; 80H .. 9FH    okamzite proved a konci
; A0H .. BFH    proved a prijimej
; C0H .. FFH    proved a vysilej

uL_RES  EQU   080H ; Znovu inicializuj RS485
uL_SFT  EQU   081H ; Otestuj velikost volne pameti
uL_SID  EQU   0F0H ; Predstav se
uL_SFI  EQU   0F1H ; Vysli velikost volne pameti v IB
uL_TF0  EQU   098H ; Konec krokovani
uL_TF1  EQU   099H ; Pocatek krokovani
uL_STP	EQU   09AH ; Krok
uL_DEB	EQU   09BH ; Dalsi prikazy pro debug
uL_SPC  EQU   0DAH ; Vysle PCL PCH PSW ACC

uL_RDM  EQU   0F8H ; Cte pamet typu   T T B B L L
uL_WRM  EQU   0B8H ; Zapise do pameti T T B B L L

RSEG SER___C

USING   1

; Cekani na znak R0 - 1 znaku  jinak S_ERR
WTF_CHR:DJNZ  R0,SND_SPC
V2_ERR: JMP   S_ERR

; Cekani po dobu 1 znaku
SND_SPT:CLR   TI
	JNB   TXD,S_RET
	CLR   AC
	RET

; Cekani po dobu 1 znaku
SND_SPC:CLR   SM2
	SETB  REN
SND_SP1:SETB  DR_EO           ; *** Ceka 1 znak
	CLR   A
	SJMP  SND_CH2

SND_CTR:SETB  C		      ; *** Send control character
	SJMP  SND_CH1
SND_CHC:XRL   AR1,A           ; *** Send data + add chk sum
	INC   R1
SND_CHR:CLR   C               ; *** Send data character
SND_CH1:MOV   TB8,C           ; ACC .. character
	CLR   REN
	CLR   DR_EO
SND_CH2:MOV   SBUF,A
	CLR   TI
	SJMP  S_RET

REC_CTR:SETB  C               ; *** Receive control character
	DB    74H	      ; MOV A,#d8
REC_CHR:CLR   C               ; *** Receive character
REC_CH1:MOV   SM2,C	      ; ACC .. rec. char
	SETB  DR_EO	      ; CY  .. RB8
	SETB  REN
	CLR   TI

RS232:
S_RET:  JB    uLF_TRP,S_INT_T
S_RETI: MOV   A,SP
	XCH   A,SER_STACK
	MOV   SP,A
	POP   ACC
	POP   PSW
	RETI

; *************************************
; Interupt TI nebo RI
; *************************************

S_INT  :PUSH  PSW
	PUSH  ACC
	MOV   A,SP
	XCH   A,SER_STACK
	JZ    S_ERROR
	MOV   SP,A

S_INT_T:MOV   PSW,#AR0; Banka1
;	JB    DR_EI,RS232
	JBC   RI,S_INT_2
	JNB   TI,S_RET
S_INT_1:CLR   uLF_NA
	RET
S_INT_2:SETB  AC	      ; Preruseni od RI => AC=1
	CLR   uLF_NB
	MOV   A,SBUF
	MOV   C,RB8
	JNC   S_INT_1
	JNB   ACC.7,S_INT_1
	SETB  uLF_NB
	CJNE  A,#uL_ERRI,S_INT_1
S_ERROR:MOV   PSW,#AR0; Banka1
	CLR   uLF_SN
	JMP   S_ERR

; Prijem bloku do XDATA

REC_Bx: CLR   SM2	      ; R23 .. where
	SETB  REN	      ; R45 .. buffer bot/top
	SETB  DR_EO	      ; R67 .. end of buffer
	CLR   TI              ; R1  =  check sum
REC_Bx1:CALL  S_RET           ; CY  =  1 when ended by CTR
	JB    AC,REC_Bx2
	CLR   TI
	SJMP  REC_Bx1
REC_Bx2:JC    REC_BxR
	CALL  S_RCTB1
	JNZ   REC_Bx1
	CLR   C
REC_BxR:RET

; Vyslani bloku z XDATA

SND_Bx: CALL  SND_SPC         ; R23 .. where
SND_BX0:CLR   REN	      ; R45 .. buffer bot/top
	CLR   DR_EO	      ; R67 .. end of buffer
	CLR   RI              ; R1  =  check sum
	CLR   TB8
SND_Bx1:CALL  S_SNFB
	CLR   TI
	JZ    SND_BxR
SND_Bx2:CALL  S_RET
	JB    AC,SND_Bx2
	SJMP  SND_Bx1
SND_BxR:CALL  S_RET
	CLR   TI
	RET

; Prijem bloku do IDATA

REC_Bi: CLR   SM2	      ; R2  .. where
	SETB  REN	      ; X R45 .. buffer bot/top
	SETB  DR_EO	      ; R6  .. end of buffer
	CLR   TI              ; R1  =  check sum
REC_Bi1:CALL  S_RET           ; CY  =  1 when ended by CTR
	JB    AC,REC_Bi2
	CLR   TI
	SJMP  REC_Bi1
REC_Bi2:JC    REC_BiR
	PUSH  AR1
	MOV   R1,AR2
	MOV   @R1,A
	POP   AR1
	XRL   AR1,A
	INC   R1
	INC   R2
	MOV   A,R2
	XRL   A,R6
	JNZ   REC_Bi1
	CLR   C
REC_BiR:RET

; Vyslani bloku z IDATA

SND_Bi: CALL  SND_SPC         ; R2  .. where
SND_Bi0:CLR   REN	      ; X R45 .. buffer bot/top
	CLR   DR_EO	      ; R6  .. end of buffer
	CLR   RI              ; R1  =  check sum
	CLR   TB8
SND_Bi1:PUSH  AR1
	MOV   R1,AR2
	MOV   A,@R1
	MOV   SBUF,A
	POP   AR1
	XRL   AR1,A
	INC   R1
	INC   R2
	MOV   A,R2
	XRL   A,R6
	CLR   TI
	JZ    SND_BiR
SND_Bi2:CALL  S_RET
	JB    AC,SND_Bi2
	SJMP  SND_Bi1
SND_BiR:CALL  S_RET
	CLR   TI
	RET

; Vyslani bloku z SDATA

SND_Bs: CALL  SND_SPC         ; R2  .. where
SND_Bs0:CLR   REN	      ; X R45 .. buffer bot/top
	CLR   DR_EO	      ; R6  .. end of buffer
	CLR   RI              ; R1  =  check sum
	CLR   TB8
SND_Bs1:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#PROC_BUF
	MOV   A,#0E5H         ; MOV A,dir
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R2            ; adresa
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#022H         ; RET
	MOVX  @DPTR,A
	INC   DPTR
	POP   DPH
	POP   DPL
	DB    12H             ; CALL PROC_BUF
	DW    PROC_BUF
	MOV   SBUF,A
	XRL   AR1,A
	INC   R1
	INC   R2
	MOV   A,R2
	XRL   A,R6
	CLR   TI
	JZ    SND_BsR
SND_Bs2:CALL  S_RET
	JB    AC,SND_Bs2
	SJMP  SND_Bs1
SND_BsR:CALL  S_RET
	CLR   TI
	RET

; Prijem bloku do SDATA

REC_Bs: CLR   SM2	      ; R2  .. where
	SETB  REN	      ; X R45 .. buffer bot/top
	SETB  DR_EO	      ; R6  .. end of buffer
	CLR   TI              ; R1  =  check sum
REC_Bs1:CALL  S_RET           ; CY  =  1 when ended by CTR
	JB    AC,REC_Bs2
	CLR   TI
	SJMP  REC_Bs1
REC_Bs2:JC    REC_BsR
	XRL   AR1,A
	INC   R1
	PUSH  DPL
	PUSH  DPH
	MOV   R3,A
	MOV   DPTR,#PROC_BUF
	MOV   A,#08BH         ; MOV dir,R3
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R2            ; adresa
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#022H         ; RET
	MOVX  @DPTR,A
	INC   DPTR
	POP   DPH
	POP   DPL
	DB    12H             ; CALL PROC_BUF
	DW    PROC_BUF
	INC   R2
	MOV   A,R2
	XRL   A,R6
	JNZ   REC_Bs1
	CLR   C
REC_BsR:RET

; Vyslani konce ramce
; podle CMD vysle uL_ARQ, uL_AAP nebo uL_END

SND_END:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#uL_CMD    ; Vysilany prikaz
	MOVX  A,@DPTR
	XCH   A,R0
	MOV   DPTR,#uL_SA     ; Adresa spojeni
	MOVX  A,@DPTR
	POP   DPH
	POP   DPL
	JZ    SND_EN1
	JB    ACC.6,SND_EN1
	JNB   uLF_SN,SND_EN1
	XCH   A,R0
	MOV   R0,#uL_AAP
	JB    ACC.7,SND_EN2
	MOV   R0,#uL_ARQ
	JNB   ACC.6,SND_EN2
SND_EN1:MOV   R0,#uL_END
SND_EN2:MOV   A,R0
	XRL   AR1,A
	INC   R1
	CALL  SND_CTR         ; Vyslani zakonceni
	MOV   A,R1
	CALL  SND_CHR         ; Vyslani checksum
	CJNE  R0,#uL_END,SND_EN3
	RET
SND_EN3:MOV   R0,#5
SND_EN4:CALL  WTF_CHR
	JNB   AC,SND_EN4      ; Prijeti uL_ACK
	JC    V3_ERR
	CJNE  A,#uL_ACK,V3_ERR
	RET

; Vyslani uL_NAK pri chybe v nektere z  rutin zpracovani prikazu
REC_ERR:CALL  REC_CTR
	JNB   AC,REC_ERR
REC_ER2:CALL  CMP_END         ; Vstup s prijatym znakem v ACC a CY
	JC    V3_ERR
	MOV   R1,AR0
	MOV   R0,#5
REC_ER3:CALL  WTF_CHR         ; Prijem checksum
	JNB   AC,REC_EN3
	MOV   R0,AR1
	MOV   R1,#uL_NAK
	SJMP  REC_EN4

; Potvrzeni prikazu
ACK_CMD:MOV   R1,#uL_ACK
	SJMP  REC_EN4

; Nacteni konce ramce
; ret: ACC = CMP_END

REC_END:MOV   R0,#5
REC_EN1:CALL  WTF_CHR         ; Prijem zakonceni ramce
	JNB   AC,REC_EN1
REC_EN2:XRL   AR1,A           ; Vstup s prijatym znakem v ACC a CY
	INC   R1
	CALL  CMP_END
	JC    V3_ERR
	CALL  SND_SPC         ; Prijem checksum
	JB    AC,REC_EN3
	CALL  SND_SPC
	JB    AC,REC_EN3
	CALL  SND_SPC
	JB    AC,REC_EN3
	CALL  SND_SPC
	JB    AC,REC_EN3
REC_E3N:CALL  SND_SPC
	JB    AC,REC_EN3
	SJMP  V3_ERR
REC_EN3:JC    V3_ERR
	XRL   A,R1
	MOV   R1,#uL_NAK
	JNZ   REC_EN4         ; Pri chybe a ARQ nebo AAP vysle NAK
	MOV   R1,#uL_ACK      ; Pri   OK  a ARQ vysle ACK
	MOV   A,R0            ; Pri   OK  a AAP je R1=ACK ale vysle
	JB    ACC.1,REC_EN5   ;  az prooceed rutina
REC_EN4:MOV   A,R0
	JNB   ACC.0,REC_EN5
	CLR   ACC.0
	MOV   R0,A
	SETB  REN
	CALL  SND_SPT
	CALL  SND_SPC
	MOV   A,R1
	CALL  SND_CHR
REC_EN5:CJNE  R1,#uL_NAK,REC_EN6
V3_ERR: JMP   S_ERR
REC_EN6:MOV   A,R0
	RET

; Pocatek ramce bez urceni Destignation Address

SND_BEB:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#uL_CMD    ; Vysilany prikaz
	MOVX  A,@DPTR
	ANL   A,#7FH
	MOV   R0,A
	POP   DPH
	POP   DPL
	MOV   A,#uL_BEG

; Vyslani zacatku ramce
; call: ACC  Destignation Address
;       R0   CoMmanD

SND_BEG:ANL   A,#07FH
	MOV   R1,A
	CALL  SND_SPT
	CALL  SND_SPC
	PUSH  DPL
	PUSH  DPH
	MOV   A,R0
	MOV   DPTR,#uL_CMD    ; Vysilany prikaz
	MOVX  @DPTR,A
	MOV   A,R1
	INC   DPTR            ; Sdresa spojeni
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
	INC   R1
	CALL  SND_CTR         ; Destignation Address nebo uL_BEG
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#uL_ADR    ; Vlastni adresa
	MOVX  A,@DPTR
	POP   DPH
	POP   DPL
	XRL   AR1,A
	INC   R1
	CALL  SND_CHR
	MOV   A,R0
	XRL   AR1,A
	INC   R1
	JMP   SND_CHR

; Nacteni zacatku ramce
; call: R1=uL_BEG je Selected WAIT jinak Pasive WAIT
; ret:  ACC  Source Address
;       R0   CoMmanD

REC_BEG:MOV   R1,#uL_BEG       ; Cekani ze SWAIT
	MOV   R0,#9
REC_BE1:CALL  WTF_CHR
	JNB   AC,REC_BE1
REC_BE2:JNC   V4_ERR
	JB    uLF_NB,S_ERR    ; Cekani z  PWAIT s R1=0
	MOV   R0,A
	JZ    REC_BE3         ; Obecna adresa
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#uL_ADR    ; Vlastni adresa
	MOVX  A,@DPTR
	POP   DPH
	POP   DPL
	XRL   A,R0
	JZ    REC_BE3         ; Vlastni adresa
	CJNE  R1,#uL_BEG,S_ERR
	CJNE  A,#uL_BEG,S_EWAIT    ; S_BEG a SWAIT
REC_BE3:MOV   AR1,R0          ; **********************
	INC   R1     	      ; V R1 se bude pocitat chksum
	MOV   R0,#5
REC_BE4:CALL  WTF_CHR
	JNB   AC,REC_BE4
	JC    S_ERR
	XRL   AR1,A
	INC   R1
	MOV   R0,A
REC_BE5:CALL  REC_CHR
	JNB   AC,REC_BE5      ; Cekani na  CMD -prikaz
	JC    S_ERR
	XRL   AR1,A
	INC   R1
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#uL_CMD
	MOVX  @DPTR,A         ; Zapis CMD
	INC   DPTR
	XCH   A,R0
	MOVX  @DPTR,A         ; Zapis SA
	JMP   S_POPDP

; Cekani na konec bloku pri REC_BEG a SWAIT
S_EWAIT:CALL  REC_CTR
	JNB   AC,S_EWAIT
	CALL  CMP_END
	JC    S_ERR
	MOV   A,R0
	JNB   ACC.0,REC_BEG
	MOV   R0,#5
S_EWAI1:CALL  WTF_CHR
	JNB   AC,S_EWAI1
	JNC   REC_BEG
V4_ERR:	JMP   S_ERR

; Vrati v R0  0..S_END,1..S_ARQ,2..S_PRQ,3..S_AAP,JINAK CY
CMP_END:JNC   CMP_EN4
CMP_EN0:MOV   R0,#0
	CJNE  A,#uL_END,CMP_EN1
	RET
CMP_EN1:INC   R0
	CJNE  A,#uL_ARQ,CMP_EN2
	RET
CMP_EN2:INC   R0
	CJNE  A,#uL_PRQ,CMP_EN3
	RET
CMP_EN3:INC   R0
	CJNE  A,#uL_AAP,CMP_EN4
	RET
CMP_EN4:SETB  C
	RET

; Doslo k chybe pri prijmu nebo vysilani

S_ERR:  MOV   SP,#SER_STACK
	JNB   uLF_SN,S_WAITD
	MOV   R0,#3           ; Pocet vyslani ERRI pri normalni chybe
	JNB   F0,S_ERR_1
	MOV   R0,#10          ; Pocet vyslani ERRI pri zavazne chybe
S_ERR_1:MOV   A,#uL_ERRI
	CALL  SND_CTR
	DJNZ  R0,S_ERR_1
S_ERR_2:INC   uL_FLG
	MOV   A,uL_FLG
	ANL   A,#3
	JNZ   S_END
	DEC   uL_FLG
S_ERR_3:SETB  uLF_ERR
	MOV   R0,#0FFH
	SJMP  S_ENDTE

; Rutina maze posledni zpravu

S_ENDT: MOV   R0,#0
S_ENDTE:MOV   SP,#SER_STACK
	ANL   uL_FLG,#0FCH
	SETB  DR_EO
	JNB   uLF_DOP,S_ENDT0 ; Nebyl rychly vystup
	CALL  DEL_DOP
	JNZ   S_END
S_ENDT0:CALL  GET_OBA
	CALL  GET_NXT
	JBC   uLF_DOP,S_ENDT1
	JNB   ACC.7,S_ENDT1   ; Neexistuje
	CALL  SET_NXT
	CALL  GET_NXT
S_ENDT1:JB    ACC.7,S_END     ; Neexistuje
	CLR   uLF_RS

; Konec vysilani nebo prijmu

S_END:  MOV   SP,#SER_STACK
	JNB   uLF_SN,S_WAITD
	JB    uLF_NB,S_WAITD
	CALL  SND_SPT
	JB    AC,S_WAITD
	CALL  SND_SPC
	JB    AC,S_WAITD
	CALL  G_MADR
	ORL   A,#80H
	CALL  SND_CTR
	SETB  uLF_NB

; Cekani na komunikaci

S_WAITD:MOV   SP,#SER_STACK
	CLR   TI
	SETB  F0
	SETB  REN
	SETB  TXD

; Cekai na vlastni adresu nebo pozadavek k vysilani

S_WAIT: CLR   uLF_SN
	JNB   uLF_NB,S_WAIT1  ; Nelze vysilat
	JNB   uLF_RS,S_WAIT1  ; Neni potreba vysilat

	SETB  uLF_SN
	CALL  G_MADR
	MOV   R0,A            ; Vlastni adresa
	SETB  C
	SUBB  A,R1            ;  - Posledni vysilajici
	ANL   A,#00FH         ; Token pasing delay
	ADD   A,#4            ; min 4
	JNB   F0,S_CONN1
	ADD   A,#010H         ; Prodlouzit - byla chyba
S_CONN1:MOV   R1,A
S_CONN2:CALL  SND_SPC         ; Cekani token pasing delay
	JB    AC,S_WAIT2
	DJNZ  R1,S_CONN2
	CALL  S_ARB	      ; Arbitrace pristupu
	JB    F0,S_WAIT1
	CALL  TST_DOP         ; Test dat pro primy vystup
	SETB  uLF_DOP
	JNZ   SND_OB0
	CLR   uLF_DOP
SND_OB:	CALL  GET_OBA         ; R23 R4 R5 do OB
SND_OB0:CALL  GET_NXT         ; R67 pristi zprava
	JB    ACC.7,SND_OBV   ; Neexistuje
	JMP   S_ENDT
SND_OBV:JMP   SND_OB1         ; Vysli blok

S_WAIT1:MOV   C,uLF_NB
	CPL   C
	CALL  REC_CH1         ; Ceka se na znak
S_WAIT2:CLR   uLF_SN
	JNB   AC,S_WAITD      ; Vybuzovaci TI
	MOV   R1,A
	JNC   S_WAITD         ; Neni ridici
	JB    ACC.7,S_WAIT    ; Ukonceni vysilani
	MOV   R1,#0
	CALL  REC_BE2         ; Cekani na prikaz v R0
	SJMP  REC_CM1

REC_CMD:CALL  REC_BEG         ; Cekani na prikaz v R0
REC_CM1:MOV   A,R0
	JB    ACC.7,REC_CM2
	CALL  GET_IBA         ; Prijem do IB
	CALL  IB_BEGR
	JZ    REC_CMEV
	CALL  REC_Bx
	JNC   REC_CMEV
	CALL  REC_EN2
	CALL  IB_ENDR
	SETB  uLF_INE
	JB    ACC.1,REC_CM0
%IF (%VECTOR_FL) THEN (
	MOV   R0,#8
	JMP   REC_CM0
)ELSE(
	CALL  TST_CAL
	CALL  uL_R_BU
)FI
V1_WAIT:JMP   S_WAITD

REC_CMEV:JMP  REC_CME

REC_CM2:CALL  GET_PRB         ; Prijem do PROC_BUF
	JZ    REC_CMEV
	CALL  REC_Bx
	JNC   REC_CMEV
	CALL  REC_EN2
%IF (NOT %VECTOR_FL)THEN(
	JNB   ACC.1,V1_WAIT
)FI
	MOV   A,R2
	MOV   R6,A
	MOV   A,R3
	MOV   R7,A
	CALL  GET_PR1
REC_CM0:CALL  G_CMD
%IF (%VECTOR_FL)THEN(
	JMP   V_uL_ADD        ; Moznost rozsirit funkce uzivatelem

uL_EADD:XCH   A,R0
	JB    ACC.3,V1_WAIT
	JNB   ACC.1,V1_WAIT
	XCH   A,R0
)FI
	CJNE  A,#uL_SID,REC_CM3

; Vysle svoji identifikaci
	CALL  ACK_CMD
	CALL  SND_BEB
	MOV   R2,#LOW uL_IDB; Vysle svoji identifikaci
	MOV   R3,#HIGH uL_IDB
	MOV   R6,#LOW uL_IDE
	MOV   R7,#HIGH uL_IDE
REC_C_0:MOV   R5,AR4
	CALL  SND_Bx
REC_C_1:CALL  SND_END
	SJMP  V1_WAIT

; Test volne pameti
REC_CM3:CJNE  A,#uL_SFT,REC_CM4
	MOV   A,R0
	JNB   ACC.0,V1_WAIT
	CALL  GET_IBA
	CALL  LEN_DAT
	MOV   AR4,R0
	CALL  GET_PRB
	CALL  S_R0FB
	MOV   A,R4
	CLR   C
	SUBB  A,#6
	JNC   REC_3C1
	INC   R1
REC_3C1:CLR   C
	SUBB  A,R0
	PUSH  PSW
	CALL  SND_SPC
	CALL  S_R0FB
	POP   PSW
	MOV   A,R1
	SUBB  A,R0
	MOV   A,#uL_NAK
	JC    REC_3C2
	MOV   A,#uL_ACK
REC_3C2:CALL  SND_CHR
	JMP   S_WAITD

; Krokovani pocatek
REC_CM4:CJNE  A,#uL_TF0,REC_CM5
	CLR   uLF_TRP      ; Ukonci krokovani
REC_C_2:CALL  ACK_CMD
	JMP   S_WAITD
REC_CM5:CJNE  A,#uL_TF1,REC_CM6
	SETB  uLF_TRP       ; Zacne krokovani
	SJMP  REC_C_2
REC_CM6:CJNE  A,#uL_STP,REC_CM7
	SETB  TI	    ; Provede jednu instrukci
	CALL  S_RETI
	CLR   TI
	CLR   A
	SJMP  REC_C_2
REC_CM7:CJNE  A,#uL_SPC,REC_CM8
	CALL  ACK_CMD
	CALL  SND_BEB
	MOV   A,SER_STACK   ; Vysle PCL PCH PSW ACC
	INC   A
	MOV   R6,A
	ADD   A,#-4
	MOV   R2,A
	CALL  SND_Bi
	JMP   REC_C_1
REC_CM8:CJNE  A,#uL_RDM,REC_CM9

; Vyslani pameti
	CALL  ACK_CMD
	CALL  SND_BEB
	CALL  S_PRPMM
	CJNE  R4,#1,S_RDM1
	CALL  SND_Bi
S_RDM1: CJNE  R4,#2,S_RDM2
	CALL  SND_Bx
S_RDM2: CJNE  R4,#4,S_RDM3
	CALL  SND_Bs
S_RDM3: JMP   REC_C_1

REC_CM9:CJNE  A,#uL_WRM,REC_C10
	CALL  ACK_CMD
	CALL  REC_BEG
	CJNE  R0,#uL_WRM AND 7FH,REC_CME
	CALL  S_PRPMM
	MOV   A,#1
	CJNE  R4,#1,S_WRM1
	CALL  REC_Bi
S_WRM1: CJNE  R4,#2,S_WRM2
	CALL  REC_Bx
S_WRM2: CJNE  R4,#4,S_WRM3
	CALL  REC_Bs
S_WRM3: JNZ   REC_CME
	CALL  REC_END
	JMP   S_WAITD

REC_C10:CJNE  A,#uL_DEB,NAK_CMD
S_DEB:	CALL  S_EQP
	JZ    NAK_CMD
	CALL  ACK_CMD
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#PROC_BUF
	MOVX  A,@DPTR
	INC   DPTR
	CJNE  A,#10H,S_DEB98
	INC   DPTR          ; Prikaz GO xxxx
	INC   DPTR
	MOV   A,SER_STACK   ; Zapise  PCL PCH
	ADD   A,#-3
	MOV   R1,A
	MOVX  A,@DPTR
	MOV   @R1,A
	INC   DPTR
	INC   R1
	MOVX  A,@DPTR
	MOV   @R1,A

S_DEB98:POP   DPH
	POP   DPL
	JMP   S_WAITD


NAK_CMD:MOV   R1,#uL_NAK
	JMP   REC_EN4

WAK_CMD:MOV   R1,#uL_WAK
	JMP   REC_EN4

REC_CME:CALL  REC_ERR
	JMP   REC_CMD

SND_OB1:CJNE  R1,#0,SND_OB2
	CALL  SND_BEG
	SJMP  SND_OB3
SND_OB2:CALL  SND_BEG
	CALL  SND_Bx
SND_OB3:CALL  SND_END
	JMP   S_ENDT

; Priprava pro cteni a zapis pameti

S_PRPMM:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#PROC_BUF
	MOV   R0,#AR2
S_PRPM1:MOVX  A,@DPTR
	MOV   @R0,A
	INC   R0
	INC   DPTR
	CJNE  R0,#AR7+1,S_PRPM1
	MOV   A,R4
	XCH   A,R2
	MOV   R4,A
	MOV   A,R5
	XCH   A,R3
	MOV   R0,A
	POP   DPH
	POP   DPL
	CJNE  R4,#9,S_PRPM2
	MOV   A,#SER_STACK
	SJMP  S_PRPM4
S_PRPM2:CJNE  R4,#8,S_PRPM3
	MOV   A,SER_STACK
	INC   A
	SJMP  S_PRPM4
S_PRPM3:CJNE  R4,#7,S_PRPM5
	MOV   R0,SER_STACK
	DEC   R0
	MOV   A,@R0
	ANL   A,#18H
S_PRPM4:ADD   A,R2
	MOV   R2,A
	MOV   R4,#1
S_PRPM5:MOV   AR5,R4

; Pricte k R67 R23

AR67R23:MOV   A,R6
	ADD   A,R2
	MOV   R6,A
	MOV   A,R7
	ADDC  A,R3
	MOV   R7,A
	CLR   C
	SUBB  A,R5
	JC    AR67R6R
	ADD   A,R4
	MOV   R7,A
AR67R6R:RET

; Priprav IB pro prijem dat

IB_BEGR:MOV   R0,#4
IB_BEG1:CLR   A
	CALL  S_ACCTB
	JZ    IB_BEGG
	DJNZ  R0,IB_BEG1
IB_BEGG:RET

; Zakonceni prijmu do IB
; call: [R23] konec dat
;       SA    adresa vysilace
; Nastavi P_NDB a ukazatel na dalsi blok
; ret:  [R23] zacatek dat
;       [R67] za konec dat
;       R0    prikaz CMD

IB_ENDR:PUSH  DPL
	PUSH  DPH
	MOV   DPL,R2
	MOV   DPH,R3
	CLR   A
	MOVX  @DPTR,A
	MOV   DPTR,#P_NDB
	MOVX  A,@DPTR
	XCH   A,R2
	MOV   R6,A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R3
	MOV   R7,A
	MOVX  @DPTR,A
	MOV   DPTR,#uL_CMD
	MOVX  A,@DPTR
	MOV   R0,A
	MOV   DPTR,#uL_SA
	MOVX  A,@DPTR
	ORL   A,#80H
	POP   DPH
	POP   DPL
	CALL  S_ACCTB
	MOV   A,R6
	CALL  S_ACCTB
	MOV   A,R7
	CALL  S_ACCTB
	MOV   A,R0
	CALL  S_ACCTB
	RET

; Naplni registry pointry do PROC_BUF
GET_PRB:MOV   R6,#LOW  PROC_BUFE ; Prijem do PROC_BUF
	MOV   R7,#HIGH PROC_BUFE
GET_PR1:MOV   R2,#LOW  PROC_BUF
	MOV   R3,#HIGH PROC_BUF
	MOV   AR5,R4
	RET

; R67 naplni ukazately na pristi blok
; ret: ACC .. sadr
;      R0  .. command

GET_NXT:CALL  S_R0FB
	MOV   A,R0
	MOV   R1,A
	CALL  S_R0FB
	MOV   A,R0
	MOV   R6,A
	CALL  S_R0FB
	MOV   A,R0
	MOV   R7,A
	CALL  S_R0FB
	XCH   A,R1
	RET

SET_NXT:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#P_AOB
	MOV   A,R6
	MOV   R2,A
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOV   R3,A
	MOVX  @DPTR,A
	SJMP  S_POPDP

; Naplni registry pointry do OB

GET_OBA:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#P_AOB
	JMP   GET_BA1

; Naplni registry pointry do IB

GET_IBA:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#P_NDB
GET_BA1:MOV   R0,#AR2
GET_BA2:MOVX  A,@DPTR
	INC   DPTR
	MOV   @R0,A
	INC   R0
	CJNE  R0,#AR7+1,GET_BA2
	SJMP  S_POPDP

; Nacte prikaz CMD
G_CMD:  PUSH  DPL
	PUSH  DPH
G_CMD1:	MOV   DPTR,#uL_CMD    ; Prikaz
	SJMP  G_MADR2

; Nacte vlastni adresu

G_MADR:	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#uL_ADR    ; Vlastni adresa
G_MADR2:MOVX  A,@DPTR
S_POPDP:POP   DPH
	POP   DPL
	RET

; Pripojeni mastera ke sbernici
; vstup vlastni adresy v R0

S_ARB: 	MOV   R1,#3+1         ; 3x2 bitu arbitrace
	CLR   uLF_NB
S_ARB1:	SETB  DR_EO
	JNB   RXD,S_ARBE
	JB    AC,S_ARBE
	CLR   TXD
	CALL  SND_CH1
	SETB  TXD
	SETB  DR_EO
S_ARB2:	CALL  SND_CH2
	JB    AC,S_ARB2
	MOV   A,R0          ; Rotace adresy
	RR    A
	RR    A
	XCH   A,R0
	ANL   A,#3          ; Nejnizsi 2 bity
	INC   A
	MOV   R2,A	    ; Pocet cekani
	DJNZ  R1,S_ARB4
	RET
S_ARB3: JB    AC,S_ARBE
	CALL  SND_SPC
S_ARB4:	DJNZ  R2,S_ARB3
	SJMP  S_ARB1

S_ARBE: SETB  F0
	RET


LEN_DAT:CLR   C             ; Delka dat = R67-R23
	MOV   A,R6          ; Kdyz je vetsi nez 127 bytu
	SUBB  A,R2          ;       A = 80h+Delka/256
	MOV   R0,A          ; Jinak A = Delka
	MOV   A,R7
	SUBB  A,R3
	JNC   LEN_DA1
	CLR   C
	SUBB  A,R4
	ADD   A,R5
LEN_DA1:MOV   R1,A
	JNZ   LEN_DA2
	MOV   A,R0
	JNB   ACC.7,LEN_DA3
	CLR   A
LEN_DA2:ORL   A,#080H
LEN_DA3:RET

S_CHKSA:XCH   A,R1          ; Check sum
	XRL   A,R1
	INC   A
	XCH   A,R1
	RET

S_SNFB: XCH   A,R3          ; Vyslani znaku z bufferu
	XCH   A,DPH
	XCH   A,R3
	XCH   A,R2
	XCH   A,DPL
	XCH   A,R2
	MOVX  A,@DPTR
	MOV   SBUF,A
	XCH   A,R1          ; Check sum
	XRL   A,R1
	INC   A
	XCH   A,R1
	SJMP  S_ACCT1

S_R0FB: XCH   A,R3          ; Naplneni R0 z bufferu
	XCH   A,DPH
	XCH   A,R3
	XCH   A,R2
	XCH   A,DPL
	XCH   A,R2
	MOVX  A,@DPTR
	MOV   R0,A
	SJMP  S_ACCT1

S_RCTB: MOV   A,SBUF        ; Prijem znaku do bufferu
S_RCTB1:XCH   A,R1          ; Check sum
	XRL   A,R1
	INC   A
	XCH   A,R1
S_ACCTB:XCH   A,R3
	XCH   A,DPH
	XCH   A,R3
	XCH   A,R2
	XCH   A,DPL
	XCH   A,R2
	MOVX  @DPTR,A
S_ACCT1:XCH   A,R3
	XCH   A,DPH
	XCH   A,R3
	XCH   A,R2
	XCH   A,DPL
	XCH   A,R2

S_INCP: INC   R2            ; Pripraveni nasledujici adresy
	CJNE  R2,#0,S_INCP1
	INC   R3
S_INCP1:MOV   A,R5
	XRL   A,R3
	JNZ   S_EQP
	MOV   A,R4
	MOV   R3,A
S_EQP:  MOV   A,R2          ; Kontrola prostoru pro data
	XRL   A,R6          ; ACC=0 => konec dat nebo preteceni IB
	JNZ   S_EQP1
	MOV   A,R3
	XRL   A,R7
S_EQP1: RET

TST_CAL:MOV   R1,SP         ; Preskoci instrukci LJMP nebo LCALL
	PUSH  DPL           ; za instrukci CALL TST_CAL pokud
	PUSH  DPH           ; je jeji adresa rovna 0
	MOV   DPH,@R1
	DEC   R1
	MOV   DPL,@R1
	INC   DPTR
	MOVX  A,@DPTR
	JNZ   TST_CA1
	INC   DPTR
	MOVX  A,@DPTR
	JNZ   TST_CA1
	INC   DPTR
	MOV   @R1,DPL
	INC   R1
	MOV   @R1,DPH
TST_CA1:POP   DPH
	POP   DPL
	RET

TST_DOP:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#uL_DOP
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	ORL   A,R0
	JZ    SND_DOR
	MOV   DPL,R0
	INC   DPTR
	INC   DPTR
	MOVX  A,@DPTR
	ORL   A,#80H
	MOVX  @DPTR,A
	MOV   R2,DPL
	MOV   R3,DPH
	MOV   R4,AR5
	CLR   A
	MOV   R6,A
	MOV   R7,A
	INC   A
SND_DOR:POP   DPH
	POP   DPL
	RET

DEL_DOP:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#uL_DOP
	MOVX  A,@DPTR
	MOV   R1,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	ORL   A,R1
	JZ    DEL_DOR
	MOV   DPL,R1
	MOVX  A,@DPTR
	MOV   R2,A
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	MOV   DPTR,#uL_DOP
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	ORL   A,R2
DEL_DOR:POP   DPH
	POP   DPL
	RET

VS_FNC1:JMP   S_FNC1

; *************************************
; Inicializace a sluzby RS-485
; *************************************

uL_INIT:MOV   R0,#0
uL_FNC: CJNE  R0,#0,VS_FNC1
S_FN0:  CLR   ES            ; Po nastaveni ryclosti a adresy se
	CLR   ET1           ; touto funkci spusti RS485
	SETB  DR_EO
	CLR   DR_EI
	CLR   PT1
	ANL   TMOD,#00FH
	ORL   TMOD,#020H
	SETB  TR1
	MOV   DPTR,#uL_SPD
	MOVX  A,@DPTR
	JNZ   S_FN0_0
	CALL  S_FN1_0
S_FN0_0:MOV   TH1,A
S_FN6_0:MOV   SCON,#11010000B
	SETB  PS
	CLR   A		    ; Vstupni bod pro uzivatelsky baud gen
	MOV   DPTR,#uL_CMD
	MOV   R1,#PROC_BUFE-uL_CMD
S_FN0_1:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R1,S_FN0_1
	MOV   DPTR,#uL_HBIB
	MOV   R1,#3
	CALL  LDRFDP
	CJNE  R2,#0,S_FN0_2
	CALL  S_FN3_0
S_FN0_2:MOV   DPTR,#P_NDB+1
	MOV   A,R2
	MOVX  @DPTR,A       ; P_NDB=BEG_IB
	INC   DPTR
	MOVX  @DPTR,A       ; H_BIB=BEG_IB
	MOV   DPTR,#P_NPD+1
	MOVX  @DPTR,A       ; P_NPD=BEG_IB
	INC   DPTR
	INC   DPTR
	MOVX  @DPTR,A       ; P_AID=BEG_IB
	MOV   DPH,A
	CLR   A
	MOV   DPL,A
	MOVX  @DPTR,A
	MOV   DPTR,#H_EIB
	MOV   A,R2
	ADD   A,R3
	MOVX  @DPTR,A       ; H_EIB=END_IB
	MOV   DPTR,#P_AOB+1
	MOVX  @DPTR,A       ; P_AOB=BEG_OB
	INC   DPTR
	MOVX  @DPTR,A       ; H_BOB=BEG_OB
	MOV   DPTR,#P_NOB+1
	MOVX  @DPTR,A       ; P_NOB=BEG_OB
	INC   DPTR
	INC   DPTR
	MOVX  @DPTR,A       ; P_AOD=BEG_OB
	MOV   DPH,A
	CLR   A
	MOV   DPL,A
	MOVX  @DPTR,A
	MOV   A,R2
	ADD   A,R3
	ADD   A,R4
	MOV   DPTR,#H_EOB   ; H_EOB=END_OB
	MOVX  @DPTR,A
	MOV   A,R2
	MOV   DPTR,#P_EID+1
	MOVX  @DPTR,A       ; P_EID=BEG_IB
	MOV   DPTR,#uL_FRLN
	MOV   A,#LOW  S_FRLN
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH S_FRLN
	MOVX  @DPTR,A
	MOV   DPTR,#uL_SBLE   ; Speed blocks init
	MOVX  A,@DPTR
	JNZ   S_FN0_3
	MOV   R4,A
	MOV   R5,A
	CALL  S_FN4_0
S_FN0_3:MOV   DPTR,#uL_SBLE   ; Speed blocks clear
	MOVX  A,@DPTR
	ADD   A,#6
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	JZ    S_FN0_6
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R1,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R1
S_FN0_4:MOV   A,R2
	MOV   R1,A
	CLR   A
S_FN0_5:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R1,S_FN0_5
	DJNZ  R3,S_FN0_4
S_FN0_6:CLR   A               ; Spusteni uLan communication
	MOV   SER_STACK,A
	MOV   uL_FLG,A
	MOV   uL_FLH,A
%IF (%VECTOR_FL)THEN(
	MOV   R4,#SINT
	MOV   DPTR,#S_INT
	CALL  VEC_SET
	MOV   R4,#V_uL_FNC
	MOV   DPTR,#uL_FNC
	CALL  VEC_SET
	MOV   R4,#V_uL_ADD
	MOV   DPTR,#uL_EADD
	CALL  VEC_SET
)FI
	SETB  ES
	RET

S_FNC1: CJNE  R0,#1,S_FNC2  ; Nastavi ryclost 57.6 kBd/ACC
S_FN1_0:MOV   DPTR,#uL_SPD
	JNZ   S_FN1_1
	MOV   A,#S_SPEED
S_FN1_1:CPL   A
	INC   A
	MOVX  @DPTR,A
	MOV   TH1,A
	RET

S_FNC2: CJNE  R0,#2,S_FNC3  ; Nastavuje adresu podle ACC
	MOV   DPTR,#uL_ADR
	MOVX  @DPTR,A
	RET

S_FNC3: CJNE  R0,#3,S_FNC4  ; Nastavi pocatek IB na R2
	CLR   ES
	CLR   A
	MOV   DPTR,#uL_SBCO
	MOVX  @DPTR,A
	MOV   SER_STACK,A
	CJNE  R2,#0,S_FN3_1 ; delku IB na R3 a delku OB na R4
S_FN3_0:MOV   R2,#HIGH BEG_IB ; Kdyz je R2=0 provede se autoinicializace
	MOV   R3,#LENG_IB
	MOV   R4,#LENG_OB
S_FN3_1:MOV   DPTR,#uL_HBIB
	MOV   R1,#3

; Ulozi od R2 pres DP R1 registru

SVRBDP :MOV   A,PSW
	ANL   A,#018H
	MOV   R0,A
	INC   R0
S_FN3_2:INC   R0
	MOV   A,@R0
	MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R1,S_FN3_2
	RET

S_FNC4: CJNE  R0,#4,S_FNC5  ; Nastavi uL_SBLE=R2 uL_SBCO=R3 uL_SBB=R45
	CLR   ES
	MOV   A,R2
	JNZ   S_FN4_1
S_FN4_0:MOV   R2,#C_SBLE
	MOV   R3,#C_SBCO
	SJMP  S_FN4_2
S_FN4_1:MOV   A,R4
	ORL   A,R5
	JNZ   S_FN4_3
S_FN4_2:MOV   A,R2
	MOV   B,R3
	ADD   A,#6
	MUL   AB
	CPL   A
	ADD   A,#1
	CPL   C
	MOV   R4,A
	MOV   DPTR,#uL_HBIB
	MOVX  A,@DPTR
	SUBB  A,B
	MOV   R5,A
S_FN4_3:MOV   R1,#4
	MOV   DPTR,#uL_SBLE
	JMP   SVRBDP

S_FNC5: CJNE  R0,#5,S_FNC6    ; Nastavuje skupinu podle ACC
	MOV   DPTR,#uL_GRP
	MOVX  @DPTR,A
	RET

S_FNC6: CJNE  R0,#6,S_FNC7    ; Spusti komunikaci s baud generatorem
	CLR   ES	      ; pripravenym uzivatelem
	SETB  DR_EO
	JMP   S_FN6_0
S_FNC7:

S_FNC10:CJNE  R0,#10h,S_FNC11
uL_O_OP:MOV   R6,DPL          ; Otevre vystupni zpravu pro R4 s Com R5
	MOV   R7,DPH
	MOV   DPTR,#P_NOB
	CALL  S_GER23
	CALL  S_PUR23
	MOV   A,R4
	ANL   A,#7FH
	CALL  uL_WRB0
	CALL  uL_WRB0
	CALL  uL_WRB0
	MOV   A,R5
	CALL  uL_WRB0
	MOV   DPL,R6
	MOV   DPH,R7
	RET

S_GER23:MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	INC   DPTR
	RET

S_PUR23:MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	INC   DPTR
	RET

S_FNC11:CJNE  R0,#11h,S_FNC12
uL_WR:  MOV   A,R4            ; Zapise R45 bytu z @DP - Rusi R012345
	ORL   A,R5
	JZ    uL_WRR
	JB    F0,uL_WRR
	CALL  uL_WRB
	DEC   R4
	CJNE  R4,#-1,uL_WR
	DEC   R5
	SJMP  uL_WR
uL_WRR:	RET

S_FNC12:CJNE  R0,#12h,S_FNC13
uL_O_CL:JB    F0,uL_O_CE      ; Uzavre vystupni zpravu
	MOV   R6,DPL
	MOV   R7,DPH
	MOV   DPTR,#P_AOD
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CLR   A
	CALL  uL_WRB0
	CLR   F0
	MOV   DPTR,#P_NOB
	MOVX  A,@DPTR
	MOV   R2,A
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	XRL   A,R3
	JNZ   uL_O_C1
	MOV   A,R4
	XRL   A,R2
	JZ    uL_O_C2
uL_O_C1:CALL  S_PUR23
	CALL  uL_I23O
	MOV   A,R4
	MOVX  @DPTR,A
	CALL  uL_I23O
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   DPTR,#P_AOD
	MOVX  A,@DPTR
	XCH   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R5
	MOVX  @DPTR,A
	MOV   DPL,R4
	MOV   DPH,R5
	MOVX  A,@DPTR
	MOV   R4,A
	ORL   A,#80H		; Adresa
	MOVX  @DPTR,A
uL_O_C2:MOV   DPL,R6
	MOV   DPH,R7
uL_O_ST:SETB  uLF_RS
	CLR   ES
	JNB   uLF_NB,uL_O_SU
	JB    uLF_SN,uL_O_SU
	SETB  TI
uL_O_SU:SETB  ES
uL_O_CE:RET

uL_I23O:INC   R2
	CJNE  R2,#0,uL_I23R
	INC   R3
	MOV   DPTR,#H_EOB
	MOVX  A,@DPTR
	XRL   A,R3
	JNZ   uL_I23R
	MOV   DPTR,#H_BOB
	MOVX  A,@DPTR
	MOV   R3,A
uL_I23R:MOV   DPL,R2
	MOV   DPH,R3
	RET

S_FNC13:CJNE  R0,#13h,S_FNC14
uL_WRB: MOVX  A,@DPTR         ; Zapise byte z @DP - Rusi R0123
uL_WRB0:MOV   R3,A
	MOV   R0,DPL
	MOV   R1,DPH
	JB    F0,uL_WRBE
	MOV   DPTR,#P_AOD
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	XCH   A,R3
	MOV   DPL,R2
	MOVX  @DPTR,A
	INC   R2
	CJNE  R2,#0,uL_WRB1
	INC   R3
	MOV   DPTR,#H_EOB
	MOVX  A,@DPTR
	XRL   A,R3
	JNZ   uL_WRB1
	MOV   DPTR,#H_BOB
	MOVX  A,@DPTR
	MOV   R3,A
uL_WRB1:MOV   DPTR,#P_AOB+1
	MOVX  A,@DPTR
	XRL   A,R3
	JNZ   uL_WRB3
	MOV   DPTR,#P_AOB
	MOVX  A,@DPTR
	XRL   A,R2
	JNZ   uL_WRB3
	INC   DPTR
	MOVX  A,@DPTR
	XRL   A,R3
	JNZ   uL_WRB3
uL_WRBE:SETB  F0
	MOV   DPL,R0
	MOV   DPH,R1
	RET

uL_WRB3:MOV   DPTR,#P_AOD
uL_WRB4:MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	MOV   DPL,R0
	MOV   DPH,R1
	MOVX  A,@DPTR
	INC   DPTR
	RET

S_FNC14:CJNE  R0,#14h,S_FNC15
uL_O_LN:MOV   R0,DPL
	MOV   R1,DPH
	MOV   DPTR,#P_AOD
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   DPTR,#P_AOB
	SETB  C
	MOVX  A,@DPTR
	SUBB  A,R2
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	INC   DPTR
	JMP   uL_O_L1

S_FNC15:

S_FNC23:CJNE  R0,#23h,S_FNC20
uL_RDB: MOV   R0,DPL	      ; Nacte byte na @DP - Rusi R0123
	MOV   R1,DPH
	JB    F0,uL_WRBE
	MOV   DPTR,#P_AID
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   DPTR,#P_EID
	MOVX  A,@DPTR
	XRL   A,R2
	JNZ   uL_RDB1
	INC   DPTR
	MOVX  A,@DPTR
	XRL   A,R3
	JZ    uL_WRBE
uL_RDB1:MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR
	MOV   DPL,R0
	MOV   DPH,R1
	MOVX  @DPTR,A
	INC   R2
	CJNE  R2,#0,uL_RDB2
	INC   R3
	MOV   DPTR,#H_EIB
	MOVX  A,@DPTR
	XRL   A,R3
	JNZ   uL_RDB2
	MOV   DPTR,#H_BIB
	MOVX  A,@DPTR
	MOV   R3,A
uL_RDB2:MOV   DPTR,#P_AID
	JMP   uL_WRB4

S_FNC20:CJNE  R0,#20h,S_FNC21
uL_I_OP:MOV   R6,DPL          ; Otevre vstupni zpravu
	MOV   R7,DPH          ; vraci R4 Adr a R5 Com
	JB    F0,uL_I_OR
	MOV   DPTR,#P_NPD
	CALL  S_GER23
	CALL  S_PUR23
	INC   R3
	MOV   DPTR,#P_EID
	CALL  S_PUR23
	MOV   DPTR,#uL_IADR
	CALL  uL_RDB
	JNB   ACC.7,uL_I_OE
	ANL   A,#7FH
	MOV   R4,A
	MOV   DPTR,#P_EID
	CALL  uL_RDB
	CALL  uL_RDB
	MOV   DPTR,#uL_ICOM
	CALL  uL_RDB
	MOV   R5,A
uL_I_OR:MOV   DPL,R6
	MOV   DPH,R7
	RET

uL_I_OE:SETB  F0
	MOV   DPTR,#P_NPD
	CALL  S_GER23
	MOV   DPTR,#P_EID
	CALL  S_PUR23
	SJMP  uL_I_OR

S_FNC21:CJNE  R0,#21h,S_FNC22
uL_RD:  MOV   A,R4            ; Nacte R45 bytu na @DP - Rusi R012345
	ORL   A,R5
	JZ    uL_RDR
	JB    F0,uL_RDR
	CALL  uL_RDB
	DEC   R4
	CJNE  R4,#-1,uL_RD
	DEC   R5
	SJMP  uL_RD
uL_RDR:	RET

S_FNC22:CJNE  R0,#22h,S_FNC24
uL_I_CL:MOV   R6,DPL          ; Uzavre vstupni zpravu
	MOV   R7,DPH
	JB    F0,uL_I_CR
	MOV   DPTR,#P_EID
	CALL  S_GER23
	MOV   DPTR,#P_NPD
	CALL  S_PUR23
uL_I_CR:MOV   DPL,R6
	MOV   DPH,R7
	RET

S_FNC24:CJNE  R0,#24h,S_FNC25
uL_I_LN:MOV   R0,DPL
	MOV   R1,DPH
	MOV   DPTR,#P_AID
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   DPTR,#P_EID
	CLR   C
	MOVX  A,@DPTR
	SUBB  A,R2
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPTR,#H_BIB
uL_O_L1:SUBB  A,R3
	MOV   R3,A
	JNC   uL_I_L1
	MOVX  A,@DPTR
	XCH   A,R3
	CLR   C
	SUBB  A,R4
	MOV   R3,A
	INC   DPTR
	MOVX  A,@DPTR
	ADD   A,R3
	MOV   R3,A
uL_I_L1:CLR   C
	MOV   A,R2
	SUBB  A,R4
	MOV   A,R3
	SUBB  A,R5
	JNC   uL_I_L3
uL_I_L2:SETB  F0
	MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
uL_I_L3:CLR   C
	MOV   DPTR,#uL_FRLN
	MOVX  A,@DPTR
	MOV   R2,A
	SUBB  A,R4
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	SUBB  A,R5
	JC    uL_I_L2
	MOV   DPL,R0
	MOV   DPH,R1
	RET

S_FNC25:

S_FNC30:CJNE  R0,#30H,S_FNC31
	JMP   uL_S_OP

S_FNC31:CJNE  R0,#31H,S_FNC32
uL_S_WR:MOV   R0,DPL
	MOV   R1,DPH
	MOV   DPTR,#uL_SBP
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	ORL   A,R2
	JZ    uL_S_WG
	MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	INC   DPTR
	INC   DPTR
	MOVX  A,@DPTR
	PUSH  ACC
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R0
	MOV   DPH,R1
	MOV   R1,A
	POP   ACC
	MOV   R0,A
	JB    F0,uL_S_WF
uL_S_W1:MOV   A,R4            ; Zapise R45 bytu z @DP - Rusi R012345
	ORL   A,R5
	JZ    uL_S_WF
	MOV   A,R0
	XRL   A,R2
	JNZ   uL_S_W2
	MOV   A,R1
	XRL   A,R3
	JZ    uL_S_WE
uL_S_W2:MOVX  A,@DPTR
	INC   DPTR
	XCH   A,R0
	XCH   A,DPL
	XCH   A,R0
	XCH   A,R1
	XCH   A,DPH
	XCH   A,R1
	MOVX  @DPTR,A
	INC   DPTR
	XCH   A,R0
	XCH   A,DPL
	XCH   A,R0
	XCH   A,R1
	XCH   A,DPH
	XCH   A,R1
	DEC   R4
	CJNE  R4,#-1,uL_S_W1
	DEC   R5
	SJMP  uL_S_W1
uL_S_WG:MOV   DPL,R0
	MOV   DPH,R1
uL_S_WE:SETB  F0
	RET
uL_S_WF:MOV   R2,DPL
	MOV   R3,DPH
	MOV   DPTR,#uL_SBP
	MOVX  A,@DPTR
	PUSH  ACC
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	POP   DPL
	INC   DPTR
	INC   DPTR
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A
	MOV   DPL,R2
	MOV   DPH,R3
	RET

S_FNC32:CJNE  R0,#32H,S_FNC33
uL_S_CL:MOV   R2,DPL          ; Uzavre speed buffer
	MOV   R3,DPH	      ; Rusi R12345
	MOV   DPTR,#uL_SBP
	MOVX  A,@DPTR
	MOV   R4,A
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CLR   A
	MOVX  @DPTR,A
	MOV   A,R4
	ORL   A,R5
	JZ    uL_O_D3
	SJMP  uL_O_D0

S_FNC33:CJNE  R0,#33H,S_FNC34
uL_O_DO:MOV   R2,DPL
	MOV   R3,DPH
uL_O_D0:JB    F0,uL_O_D3
	MOV   DPL,R4
	MOV   DPH,R5
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   R0,#LOW  uL_DOP
	MOV   R1,#HIGH uL_DOP
	MOV   C,EA
	MOV   F0,C
	CLR   EA
uL_O_D1:MOV   DPL,R0
	MOV   DPH,R1
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R1,A
	ORL   A,R0
	JNZ   uL_O_D1
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPL             ;:DPTR--
	DJNZ  DPL,uL_O_D2
	DEC   DPH
uL_O_D2:DEC   DPL
	MOV   A,R4
	MOVX  @DPTR,A
	MOV   C,F0
	MOV   EA,C
	CLR   F0
	MOV   DPL,R2
	MOV   DPH,R3
	JMP   uL_O_ST
uL_O_D3:MOV   DPL,R2
	MOV   DPH,R3
	RET

S_FNC34:

S_FNC40:CJNE  R0,#40H,S_FNC41
S_FN40:	CALL  uL_I_OP
	JBC   F0,S_FN40R
	CALL  uL_O_OP
S_FN401:MOV   DPTR,#uL_IADR
	CALL  uL_RDB
	JBC   F0,S_FN402
	MOV   DPTR,#uL_IADR
	CALL  uL_WRB
	SJMP  S_FN401

S_FN402:CALL  uL_I_CL
	CALL  uL_O_CL
	SJMP  S_FN40
S_FN40R:RET

S_FNC41:
%IF (%DEBUG_FL) THEN (
	CJNE  R0,#080H,S_FNCR
S_FN801:MOV   A,#080H
	CALL  LCDWCOM
	MOV   A,uL_FLG
	CALL  PRINThb
	MOV   A,#' '
	CALL  LCDWR
	MOV   A,AR0
	CALL  PRINThb
	MOV   A,#' '
	CALL  LCDWR
	MOV   R4,AR2
	MOV   R5,AR3
	CALL  PRINThw
	MOV   A,#' '
	CALL  LCDWR
	MOV   A,AR1
	CALL  PRINThb
	MOV   A,#' '
	CALL  LCDWR
	MOV   A,AR2
	CALL  PRINThb
	MOV   A,#0C0H
	CALL  LCDWCOM
	MOV   DPL,#SER_STACK
	MOV   DPH,#5
S_FN802:MOV   R0,DPL
	INC   DPL
	MOV   A,@R0
	CALL  PRINThb
	MOV   A,#' '
	CALL  LCDWR
	DJNZ  DPH,S_FN802
	CALL  SCANKEY
	JZ    S_FN801
	MOV   R0,#080H
)FI
S_FNCR: RET

uL_S_OP:MOV   A,R4            ; Otevre speed buffer
	MOV   R6,A            ; vstup R4 Adr a R5 Com
	MOV   A,R5            ; rusi R01234567
	MOV   R7,A
	MOV   R2,DPL
	MOV   R3,DPH
	MOV   DPTR,#uL_SBP
	MOVX  A,@DPTR
	JNZ   uL_S_O0
	INC   DPTR
	MOVX  A,@DPTR
	JZ    uL_S_O1
uL_S_O0:MOV   DPL,R2
	MOV   DPH,R3
	CALL  uL_S_CL
	MOV   R2,DPL
	MOV   R3,DPH
uL_S_O1:JB    F0,uL_S_OR
	SETB  F0
	MOV   DPTR,#uL_SBLE
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	JZ    uL_S_O4
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R1,A
uL_S_O2:MOV   DPTR,#uL_SBP
	MOV   A,R0
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A
	MOV   DPL,R0
	MOV   DPH,R1
	MOV   A,R4
	ADD   A,#6
	ADD   A,R0
	MOV   R0,A
	CLR   A
	ADDC  A,R1
	MOV   R1,A
	INC   DPTR
	INC   DPTR
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	JZ    uL_S_O5
	INC   A
	JZ    uL_S_O5
	MOV   EA,C
uL_S_O3:DJNZ  R5,uL_S_O2
uL_S_O4:MOV   DPTR,#uL_SBP
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	SJMP  uL_S_OR
uL_S_O5:MOV   F0,C
	MOV   A,DPL
	ADD   A,#LOW (-2)
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#HIGH (-2)
	MOV   DPH,A
	MOV   A,R0
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	ORL   A,#80H
	MOVX  @DPTR,A
	MOV   C,F0
	MOV   EA,C
	INC   DPTR
	MOV   A,#3
	ADD   A,DPL
	MOVX  @DPTR,A
	CLR   A
	ADDC  A,DPH
	INC   DPTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	CLR   F0
uL_S_OR:MOV   DPL,R2
	MOV   DPH,R3
	RET

; Nacte od R2 pres DP R1 registru

LDRFDP :MOV   A,PSW
	ANL   A,#018H
	MOV   R0,A
	INC   R0
LDRFDP1:INC   R0
	MOVX  A,@DPTR
	MOV   @R0,A
	INC   DPTR
	DJNZ  R1,LDRFDP1
	RET

uL_STR: JBC   ES,uL_STRG
	RET
uL_STRG:JNB   uLF_NA,uL_STRE
	JNB   uLF_NB,uL_STRF
	JNB   uLF_RS,uL_STRF
	SETB  TI
uL_STRF:SETB  uLF_NB
uL_STRE:SETB  uLF_NA
	SETB  ES
	RET

	END
