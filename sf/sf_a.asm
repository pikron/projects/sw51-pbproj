;********************************************************************
;*                    LCD 4000 - SF_A.ASM                           *
;*     Soubor programu pro prustup cisla z ADC na DAC a displej     *
;*                  Stav ke dni 24.03.1991                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

;
;  SFABS podprogram vracejici absorbanci
; program modifikuje aktualni banku registru,ACC,B,PSW a P2
; vstupem podprogramu SFABS jsou promene v XDATA
;   ADC      ... tri byte z prevodniku - pozdeji bude vhodne
;                presmerovat primo na porty
;   FILTNUM  ... casova konstanta - delka plovouciho prumeru
;                0-5    0 .. odpovida bez filtrace
;                       1 .. prumer ze 2
;                       ..
;                       5 .. prumer z 32
;   ABSFL    ... ZER  - nastavenim bitu se bude po nasledujicich
;                       32 vzorku nulovat, bit je po prvnim vzorku
;                       automaticky vynulovan
;                ZERIP- je nastaven po dobu nulovani, zvenku
;                       nenastavovat!
;                OVR  - je nastavovan pri hodnotach mimo rozsah
;                       -.99999 a 9.99999
;
; vystupy jsou tez v XDATA
;
;   ABS      ... absorbance ve float
;   ABSTXT   ... pokud neni OVR obsahuje cislo v rozsahu
;                -.99999 az 9.99999
;
;  INADCD rutina vracejici primo dekadickou hodnotu ADC v
;   ABSTXT ve shodnem tvaru jako absorbance
;

$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_ADR)
$LIST

	   PUBLIC ABSFL,FILTNUM,ABSTXT,ABS,SFABS,INADCD,VALFADC
	   PUBLIC AKUM,AKUM1,AKUM2,EXP
	   PUBLIC ADDf,SUBf,MULf,DIVf
	   PUBLIC ADDREG,SUBREG,NEGREG,POSP,LOADA,STORA
	   PUBLIC SCNFL,SCNPTR,SCNBUF,ABSZER
	   PUBLIC f2IEEE,IEEE2f

	   EXTRN CODE(INADC,OUTDAC,S_W_LEN,S_W_LE0,FND_WLE,SUB_WLE)
	   EXTRN XDATA(ZER_TAB,R_W_LEN,PROG_SF)
	   EXTRN DATA(A_W_LEN)
	   EXTRN BIT(NRE_LEN)

ASM_CODE  SEGMENT CODE INBLOCK
ASM_DATA  SEGMENT DATA
ASM_XDATA SEGMENT XDATA INPAGE

RSEG ASM_XDATA

MYSEG:

SCNPTR: DS    2         ; +0..STP +2..LO +4..'HI
SCNFL:  DS    1         ; 1 .. start scan; 2 .. in process

SCNBUF: DS    1
SCNWLEN:DS    2
SCNDAT: DS    4

ABSFL:  DS    1
FILTNUM:DS    1
ABSTXT: DS    8
ABS:    DS    4
ABSZER: DS    4
FILTDAT:DS    32*3
FILTSUM:DS    3
SGN:    DS    1
VALFADC:DS    3
MWZ_DEL:DS    1

RSEG ASM_DATA

; pro celou knihovnu
AKUM:   DS    4         ; operand Af

; pro prevody
; pro logaritmus
AKUM1:  DS    4
AKUM2:  DS    4
EXP:    DS    1
KONSTP: DS    1

; pro vystup textu do pameti
pTXTP:  DS    1
FILTP:  DS    1
FILTN:  DS    1

RSEG ASM_CODE

SCN_DO0:MOV   DPTR,#PROG_SF
	MOVX  A,@DPTR
	XRL   A,#0AAH
	JNZ   SCN_DO4
	MOV   R1,#LOW SCNPTR
	MOVX  A,@R1
	MOV   DPL,A
	INC   R1
	MOVX  A,@R1
	MOV   DPH,A
	ORL   A,DPL
	MOV   R1,#LOW SCNFL
	JZ    SCN_DO4
	MOVX  A,@R1
	CJNE  A,#1,SCN_DO1
	MOV   A,#2
	MOVX  @R1,A
	INC   DPTR
	INC   DPTR
	INC   DPTR
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	JMP   SCN_DO3
SCN_DO1:CJNE  A,#3,SCN_DO5
	MOV   A,#2
	MOVX  @R1,A
	MOV   R4,A_W_LEN
	MOV   R5,A_W_LEN+1
	MOV   R1,#LOW SCNWLEN
	MOV   A,R4
	MOVX  @R1,A
	INC   R1
	MOV   A,R5
	MOVX  @R1,A
	CALL  xLDR23i
	CALL  SUBi
	JC    SCN_DO4
	CALL  xLDR23i
	CALL  CMPi
	JC    SCN_DO4
	MOV   R2,#4
	MOV   R0,#LOW ABS
	MOV   R1,#LOW SCNDAT
SCN_DO2:MOVX  A,@R0
	MOVX  @R1,A
	INC   R0
	INC   R1
	DJNZ  R2,SCN_DO2
	MOV   R1,#LOW SCNBUF
	MOV   A,#0FFH
	MOVX  @R1,A
SCN_DO3:CALL  S_W_LE0
	JMP   MWZ_SCN
SCN_DO4:MOV   R1,#LOW SCNFL
	CLR   A
	MOVX  @R1,A
	CALL  S_W_LEN
	JMP   MWZ_SCN
SCN_DO5:MOV   A,#3
	MOVX  @R1,A
	JMP   SFABS4

MWZ_SE0:MOVX  @R1,A
	MOV   DPTR,#R_W_LEN
	CALL  xLDR45i
MWZ_SCN:CALL  FND_WLE
	JB    F0,SFABS
	MOV   R1,#low ABSZER
	MOV   R0,#4
MWZ_SE1:MOVX  A,@DPTR
	MOVX  @R1,A
	INC   DPTR
	INC   R1
	DJNZ  R0,MWZ_SE1
	MOV   R1,#low ABSFL
	MOVX  A,@R1
	ORL   A,#CHN_TAU
	MOVX  @R1,A
	SJMP  SFABS
MWZ_SET:SJMP  MWZ_SE0

MWZ_DO:	MOV   R4,A_W_LEN
	MOV   R5,A_W_LEN+1
	CALL  FND_WLE
	JB    F0,MWZ_ERR
	JNZ   MWZ_ERR
	MOV   R1,#low ABSZER
	MOV   R0,#4
MWZ_DO1:MOVX  A,@R1
	MOVX  @DPTR,A
	INC   DPTR
	INC   R1
	DJNZ  R0,MWZ_DO1
	MOV   A,#6
	CALL  SUB_WLE
	JC    MWZ_END
	SJMP  MWZ_BE1

MWZ_ERR:
MWZ_END:MOV   R1,#low ABSFL
	MOVX  A,@R1
	CLR   ACC.MWZERIPB
	SETB  ACC.SWZERB
	MOVX  @R1,A
	CALL  S_W_LEN
	SJMP  SFABS

SCN_DO: JMP   SCN_DO0

; multiwavelength zerro system

MWZ_BEG:MOVX  @R1,A
	MOV   DPTR,#ZER_TAB
	MOVX  A,@DPTR
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
	ORL   A,R6
	JZ    MWZ_ERR
	CLR   A
	CALL  SUB_WLE
	JC    MWZ_ERR
MWZ_BE1:CALL  xLDR45i
	CALL  S_W_LE0
	MOV   R1,#low ABSFL
	MOVX  A,@R1
	ORL   A,#ZER or MWZERIP
	MOVX  @R1,A

; vstup a zpracovani absorbance

SFABS:  CLR   F0
	MOV   P2,#high MYSEG
	MOV   R1,#low MWZ_DEL
	MOV   A,#10
	SETB  C
	JB    NRE_LEN,SFABS1
	MOVX  A,@R1
	CLR   C
	JZ    SFABS1
	DEC   A
	SETB  C
SFABS1:	MOVX  @R1,A
	MOV   R1,#low ABSFL
	MOVX  A,@R1
	JBC   ACC.SWZERB,MWZ_SET
	JC    SFABS2
	JBC   ACC.ZERB,ZEROB
SFABS2:	JB    ACC.ZERIPB,ZEROIP
	JC    SFABS3
	JBC   ACC.MWZERB,MWZ_BEG
	JB    ACC.MWZERIPB,MWZ_DO
SFABS3:	ANL   A,#NOT (ADC_OVR OR ADC_NR)
	MOVX  @R1,A
	JC    SFABS4
	MOV   R1,#LOW SCNFL
	MOVX  A,@R1
;	CLR   A      ;!!!!!!!!!!!
;	MOVX  @R1,A  ;!!!!!!!!!!!
	JNZ   SCN_DO
SFABS4:

; nacteni filtrovane absorbance
; jeji logaritmovani a vystup

	ACALL INFILAB
	MOV   R1,#low ABSFL
	MOVX  A,@R1
	JB    ACC.ZERB,ABSRET
	JB    F0,ABSERR+2
	ACALL NORMABS
	ACALL LOGFRf
	MOV   R0,#low ABSZER
	MOV   R2,#4
	ACALL piMOVEs-2
	ACALL SUBF
	MOV   R1,#low ABS
	MOV   R2,#4
	ACALL ipMOVEs-2

; vystup absorbance v Af na DAC

	CALL  OUTDAC

; vystup absorbance z Af do ABSTXT

OUTABS: MOV   pTXTP,#low ABSTXT
	MOV   P2,#high MYSEG
	ACALL TODECf
	CLR   A
	CALL  WRITE
	JB    F0,ABSERR
	MOV   R1,#low SGN
	MOVX  A,@R1
	JZ    ABSRET
	MOV   R1,#low ABSTXT
	MOVX  A,@R1
	CJNE  A,#'0',ABSERR
	MOV   A,#'-'
	MOVX  @R1,A
	SJMP  ABSRET
ABSERR: MOV   A,#ADC_OVR
	MOV   DPTR,#ABSFL
	MOV   R0,A
	MOVX  A,@DPTR
	ORL   A,R0
	MOVX  @DPTR,A
ABSRET: RET

ZEROB:  ORL   A,#ZERIP
	MOVX  @R1,A
	MOV   FILTN,#5
	MOV   R1,#low FILTDAT
	MOV   R2,#3*32+3
	ACALL pNULs
	MOV   FILTP,#0
ZEROIP: ACALL INFILAB
	MOV   A,FILTP
	JNZ   ABSRET

	MOV   R1,#low ABSFL
	MOVX  A,@R1
	JNB   ACC.ZER1B,CHNTAU1
	CLR   ACC.ZER1B
	MOVX  @R1,A

	ACALL NORMABS
	ACALL LOGFRf
	MOV   R1,#low ABSZER
	MOV   R2,#4
	ACALL ipMOVEs-2

CHNTAU1:MOV   R1,#low ABSFL
	MOVX  A,@R1
	ANL   A,#NOT ZERIP
	MOVX  @R1,A

	MOV   R1,#low FILTNUM
	MOVX  A,@R1
	MOV   FILTN,A
	MOV   R3,#1
	JZ    ZERO2
	MOV   R1,A
	MOV   A,#1
ZERO1:  RL    A
	DJNZ  R1,ZERO1
	MOV   R3,A
ZERO2:  CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R0,#low FILTDAT
ZERO3:  ACALL piMOVEt
	CLR   C
	ACALL ADDREG+5
	DJNZ  R3,ZERO3
	ACALL STORA
	MOV   R1,#low FILTSUM
	ACALL ipMOVEt
	RET


; rutina vraci v ABSTXT relativni hodnotu vstupu
;   ADC ve tvaru null terminated stringu

INADCD: CLR   F0
	MOV   P2,#high MYSEG
	MOV   R1,#low ABSFL
	MOVX  A,@R1
	ANL   A,#NOT (ADC_OVR OR ADC_NR)
	MOVX  @R1,A
	CALL  INADC
	JB    F0,ABSERR+2

	MOV   R1,#low VALFADC
	ACALL ipMOVEt
	ACALL LOADA
	MOV   R7,#086H
	ACALL NORMIN1-2
	ACALL STORA
	CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   R6,#020H
	MOV   R7,#084H
	ACALL MULF
	AJMP  OUTABS

; prevod vyfilrtovaneho integer cisla v registrech
; na float v Rf, zaroven deli cislo delkou filtrace

NORMABS:MOV   A,FILTN
	CPL   A
	INC   A
	ADD   A,#086H
	MOV   R7,A
	MOV   R2,#0
NORMIN1:ACALL NORMA+2
	MOV   A,R2
	CPL   A
	ANL   A,#080H
	XRL   A,R6
	MOV   R6,A
	RET

; rutina nacte pomoci INADC stav na ADC
; a aktualizuje filtracni soucet FILTSUM

INFILAB:MOV   R0,#low FILTSUM
	ACALL piMOVEt
	ACALL LOADA
	MOV   A,FILTP
	RL    A
	ADD   A,FILTP
	ADD   A,#low FILTDAT
	MOV   R3,A
	MOV   R0,A
	ACALL piMOVEt
	ACALL SUBREG
	CALL  INADC    ; vstup z ADC
	JB    F0,FILTR3; prevod neprobehl
	MOV   P2,#high MYSEG
	CLR   C
	ACALL ADDREG+5
	MOV   A,R3
	MOV   R1,A
	ACALL ipMOVEt
	ACALL STORA
	MOV   R1,#low FILTSUM
	ACALL ipMOVEt
	INC   FILTP
	MOV   R1,FILTN
	MOV   A,R1
	JZ    FILTR2
	MOV   A,#1
FILTR1: RL    A
	DJNZ  R1,FILTR1
	CLR   C
	DEC   A
	SUBB  A,FILTP
	JNC   FILTR3
FILTR2: MOV   FILTP,#0
FILTR3: RET


pNULs:  CLR   A
	MOVX  @R1,A
	INC   R1
	DJNZ  R2,pNULs+1
	RET

iNULs:  CLR   A
	MOV   @R1,A
	INC   R1
	DJNZ  R2,iNULs+1
	RET

ipMOVEt:MOV   R2,#3
	MOV   R0,#AKUM
ipMOVEs:MOV   A,@R0
	INC   R0
	MOVX  @R1,A
	INC   R1
	DJNZ  R2,ipMOVEs
	RET

piMOVEt:MOV   R2,#3
	MOV   R1,#AKUM
piMOVEs:MOVX  A,@R0
	INC   R0
	MOV   @R1,A
	INC   R1
	DJNZ  R2,piMOVEs
	RET

iMOVEf1:MOV   R0,#AKUM
iMOVEf: MOV   R2,#4
iMOVEs: MOV   A,@R0
	INC   R0
	MOV   @R1,A
	INC   R1
	DJNZ  R2,iMOVEs
	RET

;====================================================================

; Odcitani Rf,Af:=Af-Rf
; =====================

SUBF:                  ;:Rf,Af:=Af-Rf
ODEC:   MOV   A,#80H
	XRL   A,R6
	MOV   R6,A

; Scitani Rf,Af:=Af+Rf
; ====================

ADDF:                  ;:Rf,Af:=Af+Rf
SCITA:  MOV   A,R7
	JZ    LOADA    ; operand v Rf=0
	MOV   A,AKUM+3
	JZ    STORA    ; operand v Af=0
	CLR   C
	SUBB  A,R7     ; rozdil exponentu
	MOV   R1,A     ; do R1
	JNC   SCIT1    ; rozdil kladny
	CPL   A
	INC   A        ; negace rozdilu
	MOV   R1,A
	ACALL CHANGE   ; vymena operandu
SCIT1:  CJNE  R1,#25,SCIT12 ; rozdil <> 25

LOADA:  MOV   R4,AKUM  ;:Rf:=Af
	MOV   R5,AKUM+1
	MOV   R6,AKUM+2
	MOV   R7,AKUM+3
	RET

SCIT12: JNC   LOADA    ; rozdil > 24
	ACALL UZNAM    ; uprava znamenek
	MOV   R0,A
	ACALL POSP     ; posun registru na stejny
	MOV   A,R0     ; exponent
	RLC   A        ; ruzna znamenka
	JNC   SCIT2    ; ano
	ACALL ADDREG   ; secteni AKUM a REGISTRU
	JNC   ULOZ     ; neni preteceni
	INC   R7       ; exponent+1
	MOV   A,R7
	JZ    OVER     ; preteceni
	MOV   R1,#1
	ACALL POSP3+1  ; posun REG. o 1 misto vpravo
ULOZ:   ACALL ZAOK     ; zaokrouhleni podle MSB R3
	MOV   A,R2     ; znamenko vysledku
	ANL   A,#80H   ; vymaskovani
	XRL   A,R6     ; zapis do REG.
	MOV   R6,A

STORA:  MOV   AKUM,R4  ;:Af:=Rf
	MOV   AKUM+1,R5
	MOV   AKUM+2,R6
	MOV   AKUM+3,R7
	RET

SCIT2:  ACALL SUBRE0   ; odecteni REG-AKUM
	SJMP  ULOZ

OVER:   SETB  F0       ; doslo k preteceni
	RET            ; F0 = 1

; uprava znamenek

UZNAM:  MOV   A,AKUM+2
	CPL   A
	MOV   R2,A     ; uloz znamenko Af
	ORL   AKUM+2,#80H
	MOV   A,R6     ; obnov MSB Af
	ORL   A,#80H
	XCH   A,R6     ; obnov MSB Rf
	XRL   A,R2     ; porovnej znamenka
	RET

; posun cisla v REG o R1 bitu vpravo

POSP:   MOV   R3,#0
	CJNE  R1,#0,POSP1
	RET            ; bez posunu
POSP1:  MOV   A,R1
	SUBB  A,#8     ; posun >= 8 bitu ?
	JC    POSP3    ; ne
	MOV   R1,A
	CLR   A        ; posun o 8 vpravo
	XCH   A,R6
	XCH   A,R5
	XCH   A,R4
	XCH   A,R3
	SJMP  POSP+2
POSP3:  CLR   C
	MOV   A,R6     ; posun o 1 bit
	RRC   A
	MOV   R6,A
	MOV   A,R5
	RRC   A
	MOV   R5,A
	MOV   A,R4
	RRC   A
	MOV   R4,A
	MOV   R3,PSW
	DJNZ  R1,POSP3 ; opakuj
	RET

; zaokrouhleni podle MSB R3

ZAOK:   MOV   A,R3
	RLC   A        ; zaokrouhlovat ?
	JNC   NEZAOK   ; ne
REGA1:  MOV   R1,#1    ; pricti 1
	MOV   A,R4
	ADD   A,R1
	MOV   R4,A
	MOV   A,R5
	ADDC  A,#0
	MOV   R5,A
	MOV   A,R6
	ADDC  A,#0
	MOV   R6,A
	JC    $+3      ; preteceni
NEZAOK: RET
	INC   R7
	MOV   A,R7
	JZ    OVER
	SJMP  POSP3+1  ; posun vpravo

; vymena operandu

CHANGE: MOV   A,R4     ;:Af <--> Rf
	XCH   A,AKUM
	MOV   R4,A
	MOV   A,R5
	XCH   A,AKUM+1
	MOV   R5,A
	MOV   A,R6
	XCH   A,AKUM+2
	MOV   R6,A
	MOV   A,R7
	XCH   A,AKUM+3
	MOV   R7,A
	RET

; secteni mantisy REG+AKUM

ADDREG: MOV   A,#80H
	ADD   A,R3
	MOV   R3,#0
	MOV   A,R4
	ADDC  A,AKUM
	MOV   R4,A
	MOV   A,R5
	ADDC  A,AKUM+1
	MOV   R5,A
	MOV   A,R6
	ADDC  A,AKUM+2
	MOV   R6,A
	MOV   R7,AKUM+3
	RET

; odecteni mantisy REG-AKUM

SUBRE0: CLR   C
	MOV   A,R4
	SUBB  A,AKUM
	MOV   R4,A
	MOV   A,R5
	SUBB  A,AKUM+1
	MOV   R5,A
	MOV   A,R6
	SUBB  A,AKUM+2
	MOV   R6,A
	MOV   R7,AKUM+3
	JC    SUBRE1   ; AKUM vetsi
	MOV   A,R2
	CPL   A        ; zmena znamenka
	MOV   R2,A     ; vysledku
	SJMP  NORMA+4
SUBRE1: ACALL NEGREG   ; negace vysledku
	SJMP  NORMA+4  ; v REG

NEGREG: CLR   C
	CLR   A
	SUBB  A,R3
	MOV   R3,A
	CLR   A
	SUBB  A,R4
	MOV   R4,A
	CLR   A
	SUBB  A,R5
	MOV   R5,A
	CLR   A
	SUBB  A,R6
	MOV   R6,A
	RET

; normalizace cisla v REG a MSB R3=1

NORMA:  ACALL LOADA
	MOV   R3,#0
	CLR   A
	MOV   R1,A
	MOV   A,R6
	JNZ   NORM1
	XCH   A,R3     ; posun o 8 bitu
	XCH   A,R4     ; vlevo
	XCH   A,R5
	XCH   A,R6
	MOV   A,R1
	ADD   A,#8
	CJNE  A,#24,NORMA+5
CLEAR:  MOV   R7,#0
	RET
NORM1:  JB    ACC.7,NORM2
	MOV   A,R3     ; posun o bit
	RLC   A        ; vlevo
	MOV   R3,A
	MOV   A,R4
	RLC   A
	MOV   R4,A
	MOV   A,R5
	RLC   A
	MOV   R5,A
	MOV   A,R6
	RLC   A
	MOV   R6,A
	INC   R1
	SJMP  NORM1
NORM2:  CLR   C
	MOV   A,R7
	SUBB  A,R1     ; uprava exponentu
	JC    CLEAR
	MOV   R7,A
	RET

; nasobeni Rf,Af:=Af*Rf
; =====================

MULF:                  ;:Rf,Af:=Af*Rf
MUL0:   MOV   R1,#0
	ACALL ADDEXP   ; soucet exponentu
	JZ    CLEAA    ; jedno cislo = 0
	ACALL UZNAM    ; uprava znamenek
	MOV   R2,A     ; vysledne znamenko
	MOV   A,R1
	JZ    OVER1    ; preteceni
	MOV   A,R6
	PUSH  ACC
	MOV   A,R5
	PUSH  ACC
	MOV   A,R4
	PUSH  ACC
	MOV   R1,#3
	CLR   A
	MOV   R6,A
	MOV   R5,A
	MOV   R4,A
MUL1:   CLR   A
	XCH   A,R6
	XCH   A,R5
	XCH   A,R4
	XCH   A,R3
	POP   ACC
	JZ    MUL2
	PUSH  ACC
	MOV   B,A
	MOV   A,AKUM
	MUL   AB
	ADD   A,R3
	MOV   R3,A
	MOV   A,B
	ADDC  A,R4
	MOV   R4,A
	JNC   MUL3     ; pro zrychleni
	CLR   A
	ADDC  A,R5
	MOV   R5,A
	CLR   A
	ADDC  A,R6
	MOV   R6,A
MUL3:   POP   B
	PUSH  B
	MOV   A,AKUM+1
	MUL   AB
	ADD   A,R4
	MOV   R4,A
	MOV   A,B
	ADDC  A,R5
	MOV   R5,A
	CLR   A
	ADDC  A,R6
	MOV   R6,A
	POP   B
	MOV   A,AKUM+2
	MUL   AB
	ADD   A,R5
	MOV   R5,A
	MOV   A,B
	ADDC  A,R6
	MOV   R6,A
MUL2:   DJNZ  R1,MUL1
	ACALL NORMA+4
	AJMP  ULOZ

CLEAA:  MOV   R7,A
	MOV   AKUM+3,A
	RET

OVER1:  AJMP  OVER

; soucet/rozdil exponentu
; -----------------------

ADDEXP: MOV   A,AKUM+3
	JZ    KADDE
	MOV   A,R7
	JZ    KADDE
	XRL   A,R1
	INC   R1
	INC   R1
	DJNZ  R1,$+3
	INC   A
	ADD   A,AKUM+3
	MOV   R7,A
	XRL   A,PSW
	RLC   A
	MOV   A,R7
	JNC   ADDE1
	XRL   A,#80H
	MOV   R7,A
	INC   R1
KADDE:  RET
ADDE1:  ANL   A,#80H
	MOV   R1,#0
	RET

ERRDIV: SJMP  ERRDIV

; deleni Af,Rf:=Af/Rf
; ===================

DIVF:                  ;:Af,Rf:=Af/Rf
DIV0:   MOV   A,R7
	JZ    ERRDIV
	MOV   R1,#0FFH
	ACALL ADDEXP
	JZ    CLEAA
	INC   A
	JZ    OVER1
	MOV   AKUM+3,A
	MOV   A,R1
	JZ    OVER1
	ACALL UZNAM
	MOV   R2,A
	ACALL CHANGE
	CLR   A
	XCH   A,R6
	MOV   R3,A
	CLR   A
	XCH   A,R5
	MOV   R1,A
	CLR   A
	MOV   B,A
	XCH   A,R4
	MOV   R0,A
DIV2:   MOV   A,R3
	PUSH  ACC
	MOV   A,R1
	PUSH  ACC
	MOV   A,R0
	PUSH  ACC
DIV8:   CLR   C
	MOV   A,R0
	SUBB  A,AKUM
	MOV   R0,A
	MOV   A,R1
	SUBB  A,AKUM+1
	MOV   R1,A
	MOV   A,R3
	SUBB  A,AKUM+2
	MOV   R3,A
	JC    DIV1
	DEC   SP
	DEC   SP
	DEC   SP
	SETB  C
DIV5:   MOV   A,R6
	JB    ACC.7,DIV6
	MOV   A,R4
	RLC   A
	MOV   R4,A
	MOV   A,R5
	RLC   A
	MOV   R5,A
	MOV   A,R6
	RLC   A
	MOV   R6,A
	ORL   A,R5
	ORL   A,R4
	JNZ   $+3
	DEC   R7
	MOV   A,R0
	RLC   A
	MOV   R0,A
	MOV   A,R1
	RLC   A
	MOV   R1,A
	MOV   A,R3
	RLC   A
	MOV   R3,A
	JNC   DIV2
	INC   B
	SJMP  DIV8
DIV1:   JBC   B.0,DIV5
	POP   ACC
	MOV   R0,A
	POP   ACC
	MOV   R1,A
	POP   ACC
	MOV   R3,A
	CLR   C
	SJMP  DIV5
DIV6:   MOV   R3,PSW
	AJMP  ULOZ

; porovnani Af-Rf -> Carry a Zero
; ===============================

COMPF:  ACALL PORO     ;:Af-Rf
	JZ    COMPFK
	MOV   A,R6
	XRL   A,PSW
	RLC   A
	MOV   A,R6
	XRL   A,AKUM+2
	JNB   ACC.7,COMPF1
	MOV   A,AKUM+2
	RLC   A
COMPF1: MOV   A,#1
COMPFK: RET

; porovnani AKUM-REG se shodnymi znamenky

PORO:   MOV   A,AKUM+3
	CLR   C
	SUBB  A,R7
	JNZ   POROK
	MOV   A,R7
	JZ    POROK
	MOV   A,AKUM
	SUBB  A,R4
	MOV   A,AKUM+1
	SUBB  A,R5
	MOV   A,AKUM+2
	SUBB  A,R6
POROK:  RET


; logaritmus Af,Rf:=LOG(Af)
; =========================

LOGF:   ACALL LOADA    ;:Af,Rf:=LOG(Af)
LOGFRf: MOV   KONSTP,#LOGK-LOADKB-1
	ACALL cLOADAf
	MOV   A,#80H
	XCH   A,R7
	ADD   A,R7     ; Rf e <0.5;1)
	MOV   EXP,A    ; EXP:=exp.Rf
	DEC   AKUM+3   ;         _
	ACALL SCITA    ; Rf:=Rf+V2/2
	MOV   KONSTP,#LOGK-LOADKB-1
	ACALL cLOADAf  ;      _
	ACALL DIV0     ; Rf:=V2/Rf
	ACALL cLOADAf
	ACALL ODEC     ; Af:=1-Rf
	MOV   R1,#AKUM2; A2f:=Af
	ACALL iMOVEf1
	ACALL MUL0     ; Rf,A1f:=Af*Af
	MOV   R1,#AKUM1
	ACALL iMOVEf1
	ACALL cLOADAf  ; nacti konstantu
	SJMP  LOGF3

LOGF1:  ACALL SCITA    ; generator rad
LOGF2:  MOV   R0,#AKUM1
	MOV   R1,#AKUM
	ACALL iMOVEf   ; Af:=A1f
LOGF3:  ACALL MUL0
	ACALL cLOADAf  ; nacti konstantu
	ADD   A,#10H
	JNC   LOGF1
;
	MOV   R3,A
	DEC   R1
	MOV   R2,#1
	ACALL cLOADs
;
	ACALL SCITA
	MOV   R0,#AKUM2
	MOV   R1,#AKUM
	ACALL iMOVEf
	ACALL MUL0
	ACALL cLOADAf
	ACALL SCITA    ; Af:=Rf-0.5
	MOV   A,EXP
	JZ    LOGF4
	ACALL NORMRB
	ACALL SCITA    ; Af:=Af+exp
LOGF4:  ACALL cLOADAf
	ACALL MUL0     ; Af,Rf:=Rf*log(2) (ln(2))
	RET

; prevod bytu v A na float v Rf

NORMRB: MOV   R4,#0
	MOV   R5,#0
	MOV   R6,A
	CLR   C
	RLC   A
	JZ    CLEARRf
	JNC   NORMRB1
	CPL   A
	INC   A
NORMRB1:MOV   R7,#87H
NORMRB2:CLR   C
	RLC   A
	JC    NORMRB3
	DEC   R7
	SJMP  NORMRB2
NORMRB3:RRC   A
	XCH   A,R6
	CPL   A
	ANL   A,#80H
	XRL   A,R6
	MOV   R6,A
	RET
CLEARRf:MOV   R7,#0
	RET


cLOADAf:MOV   R2,#4
	MOV   R1,#AKUM
cLOADs :MOV   A,KONSTP
	INC   A
	MOV   KONSTP,A
	MOVC  A,@A+PC
LOADKB: MOV   @R1,A
	INC   R1
	DJNZ  R2,cLOADs
	RET

; konstanty pro vypocet logaritmu
				  ;  _
LOGK:   DB    0F3H,004H,035H,081H ; V2
	DB    000H,000H,000H,081H ; 1
	DB    0AAH,056H,019H,080H ; 1/5*2/ln(2)
	DB    0F1H,022H,076H,080H ; 1/3*2/ln(2)
	DB    045H,0AAH,038H,0FFH,082H; 2/ln(2)
	DB    000H,000H,080H,080H ; -0.5
; dekadicky logaritmus
	DB    09BH,020H,01AH,07FH ; log(2)
; prirozeny logaritmus
;       DB    018H,072H,031H,080H ; ln(2)

; konstanty pro prevod do desitkove soustavy

TODECK: DB    0A0H,086H,001H ; 1E5
	DB    010H,027H,000H ; 1E4
	DB    0E8H,003H,000H ; 1E3
	DB    064H,000H,000H ; 1E2
	DB    00AH,000H,000H ; 10
	DB    001H,000H,000H ; 1

; nasobeni deseti

MULTEN :ACALL LOADA
	INC   R7
	INC   R7
	INC   R7
	INC   AKUM+3
	AJMP  SCITA

; deleni deseti

DIVTEN :MOV   R4,#0CDH ; 1E-1
	MOV   R5,#0CCH
	MOV   R6,#04CH
	MOV   R7,#07DH
	AJMP  MUL0

SUBREG: CLR   C
	MOV   A,R4
	SUBB  A,AKUM
	MOV   R4,A
	MOV   A,R5
	SUBB  A,AKUM+1
	MOV   R5,A
	MOV   A,R6
	SUBB  A,AKUM+2
	MOV   R6,A
	RET

; nacteni cisla ze vstupu READ do Af
; ==================================

DECTOf: CALL  READ
	SETB  F0       ; moznost chyby
	MOV   AKUM1,#0     ; plus
	MOV   AKUM1+2,#0   ; exponent
	MOV   AKUM1+3,#0   ; neni des. tecka
	MOV   AKUM+3,#0    ; cislo 0
	CJNE  A,#'-',DECTOf1
	MOV   AKUM1,#080H  ; minus

DECTOf0:CALL  READ
DECTOf1:CJNE  A,#'.',DECTOf2
	MOV   A,#0FFH
	XCH   A,AKUM1+3    ; je des. tecka
	JNZ   DECTOf3
	CALL  READ
DECTOf2:ACALL ISANUM
	JC    DECTOf4
	CLR   F0       ; neni chyba
	MOV   AKUM1+1,A; uloz cislici
	ACALL MULTEN
	MOV   A,AKUM1+1; cislici zpet
	ACALL NORMRB
	MOV   A,R6
	XRL   A,AKUM1  ; pridej znaminko
	MOV   R6,A
	ACALL SCITA
	MOV   A,AKUM1+3
	JZ    DECTOf0  ; neni tecka
	DEC   AKUM1+2  ; sniz exponent
	SJMP  DECTOf0
DECTOf3:MOV   A,#'.'-48
DECTOf4:ADD   A,#48
	MOV   R1,#0    ; exponent
	MOV   R2,#0    ; exp. +
	CJNE  A,#'e',DECTOf5
	SJMP  DECTOf6
DECTOf5:CJNE  A,#'E',DECTOf9 ; bez exp
DECTOf6:CALL  READ
	MOV   R0,#3    ; max cislic v exp.
	CJNE  A,#'+',DECTO15
	SJMP  DECTO14
DECTO15:CJNE  A,#'-',DECTOf7
	MOV   R2,#1    ; exp -
DECTO14:CALL  READ
DECTOf7:ACALL ISANUM
	JC    DECTOf8  ; konec exp
	MOV   R3,A
	MOV   A,R1
	RL    A
	RL    A
	ADD   A,R1
	RL    A        ; exp*10
	ADD   A,R3     ; +cislice
	MOV   R1,A
	DJNZ  R0,DECTO14
	SETB  F0       ; chyba v exponentu
	RET
DECTOf8:ADD   A,#48
DECTOf9:PUSH  ACC
	MOV   A,R1     ; znam exp
	DJNZ  R2,DECTO10
	CPL   A
	INC   A
DECTO10:ADD   A,AKUM1+2; pricti pom exp
DECTO12:MOV   AKUM1+2,A
	JZ    DECTOfK
	JNB   ACC.7,DECTO13
	ACALL DIVTEN
	MOV   A,AKUM1+2
	INC   A
	SJMP  DECTO12
DECTO13:ACALL MULTEN
	DJNZ  AKUM1+2,DECTO13
DECTOfK:POP   ACC
	RET

; vystup cisla v Af na zarizeni
; =============================
; ve tvaru X.XXXX
; nebo     -.XXXX

TODECf: MOV   A,AKUM+2
	ORL   AKUM+2,#080H
	ANL   A,#080H
	MOV   R1,#low SGN
	MOVX  @R1,A
	MOV   R4,#000H ;1E5
	MOV   R5,#050H
	MOV   R6,#043H
	MOV   R7,#091H
	ACALL MUL0
	CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R7,#080H
	ACALL ODEC
	MOV   R4,#000H
	MOV   R5,#024H
	MOV   R6,#0F4H
	MOV   R7,#094H
	ACALL PORO
	JC    TODECf5
	SETB  F0
	RET

TODECf5:ACALL LOADA
	MOV   A,R7
	ADD   A,#07FH
	JC    TODECf6
	CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R1,#low SGN
	MOVX  @R1,A
	SJMP  TODECf7

TODECf6:MOV   A,#098H
	CLR   C
	SUBB  A,R7
	MOV   R1,A
	ACALL POSP
TODECf7:MOV   R0,#1

	MOV   KONSTP,#TODECK-LOADKB-1
	MOV   R3,#6

TODECf8:MOV   A,R0
	JNZ   TODECf9
	MOV   A,#'.'
	CALL  WRITE
TODECf9:DEC   R0
	MOV   R2,#3
	ACALL cLOADAf+2  ; PRASI R1,R2
	MOV   R1,#'0'-1

TODEC10:INC   R1
	ACALL SUBREG
	JNC   TODEC10
	CLR   C
	ACALL ADDREG+5
	MOV   A,R1
	ACALL WRITE
TODEC11:DJNZ  R3,TODECf8
	RET

ISANUM: ADD   A,#-58
	ADD   A,#10
	CPL   C
ISANUM1:RET

WRITE  :XCH   A,R1
	PUSH  ACC
;
	MOV   A,R1
	MOV   R1,pTXTP
	MOVX  @R1,A
	INC   pTXTP
	POP   ACC
	MOV   R1,A
	RET

READ:   MOV   A,R1
	PUSH  ACC
;
	MOV   R1,pTXTP
	MOVX  A,@R1
	INC   pTXTP
	MOV   R1,A
	POP   ACC
	XCH   A,R1
	RET

; Prevod vlastniho formatu do IEEE formatu
f2IEEE: MOV   A,R6
	RLC   A
	MOV   A,R7
	JZ    f2IEEE1
	DJNZ  ACC,f2IEEE2
f2IEEE1:MOV   R6,A
	MOV   R5,A
	MOV   R4,A
	RRC   A
	MOV   R7,A
	RET
f2IEEE2:DEC   A
	RRC   A
	MOV   R7,A
	MOV   A,R6
	MOV   ACC.7,C
	MOV   R6,A
	RET

; Prevod z IEEE do vlastniho formatu
IEEE2f: MOV   A,R7
	RLC   A
	MOV   A,R6
	MOV   ACC.7,C
	XCH   A,R6
	RLC   A
	MOV   A,R7
	RLC   A
	JZ    IEEE2f2
	ADD   A,#2
	JC    IEEE2f1
	MOV   R7,A
	RET
IEEE2f1:MOV   A,#0FFH
	MOV   R7,A
	RET
IEEE2f2:MOV   R7,A	; nula
	RRC   A
	MOV   R6,A
	RET

END