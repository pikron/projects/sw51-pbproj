;********************************************************************
;*                    LCD4000 -SF_ADR.H                             *
;*     Headr file fyzickych adres pro spektrofotometr LCD4000       *
;*                  Stav ke dni 29.08.1991                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

;------------------ Vstupni prevodnik - ADC -------------------------

ADC_IN1     XDATA 0FA01H    ; cteni ADC - LSB

; Bity a masky v ABSFL
  CHN_TAU   EQU  020H       ; ZMENA TAU
  ADC_NR    EQU  002H       ; prevod neskoncil
  ADC_OVR   EQU  001H       ; vstupni zesilovace prebuzeny
  ZER       EQU  028H       ; vstup  - vynuluj
  ZERB      EQU   5         ; zmen FILTNUM
  ZER1B     EQU   3         ; donuluj
  ZERIP     EQU  010H       ; vystup - nuluje se
  ZERIPB    EQU   4
  MWZER     EQU  080H       ; multi wavelength zerro
  MWZERB    EQU   7
  MWZERIP   EQU  040H       ; multi wavelength zerro provadeno
  MWZERIPB  EQU   6
  SWZER     EQU  004H       ; nastav ABSZER podle R_W_LEN
  SWZERB    EQU   2

; Bity a masky v ADC_ACH
  SFABSRQ   EQU  020H       ; pozadavek absorbance
  SFABSRQB  EQU   5
  INADCRQ   EQU  010H       ; pozadavek cisla z ADC
  INADCRQB  EQU   4

ADC_CAD     EQU  020H       ; spusteni prevodu

;------------------ Citac/casovac 53 - pouze out --------------------

CNT53_0     XDATA 0F800H    ; citac 0
CNT53_1     XDATA 0F801H    ; citac 1
CNT53_2     XDATA 0F802H    ; citac 2
CNT53_CW    XDATA 0F803H    ; ridici kanal

                ; SSRR   B
                ; CCWWMMMC
                ; 1010210D
MOD53_0     EQU   00110110B ; citac 0 - mod 3 - delicka 1:1 - lo/hi
MOD53_1     EQU   01111000B ; citac 1 - mod 4 - puls a podteceni
MOD53_2     EQU   10010010B ; citac 2 - mod 1 - astabilni - rad DAC

VAL53_0L    EQU   LOW  44236; strida citace 0
VAL53_0H    EQU   HIGH 44236;   => 25 Hz

;------------------ Vystupni prevodnik - DAC ------------------------

DAC_LA      XDATA 0F900H    ; DAC - LSN       |  Attenuator
                            ;  D3  D2  D1  D0  A3  A2  A1  A0
DAC_H       XDATA 0F901H    ; DAC - MSB
ADC_CTRL    XDATA 0FA00H    ; rizeni ADC   | motor monochromatoru
MOTOR       XDATA 0FA00H    ; ACH1 ACH0 CAD  MB   M8  M7  M6  M5
MOT_IN      XDATA 0FA02H    ; ohlas motoru a filt| rizeni lamp a filt
LAMPS_CTRL  XDATA 0FA02H    ; FIN  MIN2 MIN1 MIN0 FC  LC2 LC1 LC0
MOT_MSK     EQU   070H

;------------------ Ovladani vstupu a vystupu -----------------------

AUX_OUT     XDATA 0F902H    ;  I2  I1   X   X   O4  O3   O2  O1

I8255_C1    XDATA 0F903H
I8255_M1    SET   10001000B ; vystup A B CL , vstup CH

I8255_C2    XDATA 0FA03H
I8255_M2    SET   10001010B ; vystup A CL , vstup B CH

;------------------ Ovladani LCD displeje ---------------------------

LCD_INST    XDATA 0FD00H    ; zapis instrukce
LCD_WDATA   XDATA 0FD01H    ; zapis dat
LCD_STAT    XDATA 0FD02H    ; cteni statusu
LCD_RDATA   XDATA 0FD03H    ; cteni dat

;------------------ Ovladani LED diod -------------------------------

LED         XDATA 0FE00H

;------------------ Cteni/zapis do klavesnice -----------------------

KBD         XDATA 0FF00H

;------------------ Definice vnitrnich adres ------------------------

PCON        DATA  087H
