#   Project file pro podavac AAA
#         (C) Pisoft 1996

aa_pod.obj: aa_pod.asm
	a51 aa_pod.asm $(par)

	  : aa_pod.obj
	del aa_pod.

aa_pod.   : aa_pod.obj ..\pblib\pb.lib
	l51 aa_pod.obj,..\pblib\pb.lib xdata(8000H) ramsize(100h) ixref

aa_pod.hex : aa_pod.
	ohs51 aa_pod
	del aa_pod.
