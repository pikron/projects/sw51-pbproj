#   Project file pro podavac AAA
#         (C) Pisoft 1996

aa_pod.obj: aa_pod.asm config.h
	a51 aa_pod.asm $(par) debug

#pb_ui.obj : pb_ui.asm
#	a51 pb_ui.asm $(par) debug

aa_pod.   : aa_pod.obj ..\pblib\pb.lib
	l51 aa_pod.obj,..\pblib\pb.lib code(09000H) xdata(8000H) ramsize(100h) ixref

aa_pod.hex : aa_pod.
	ohs51 aa_pod

	  : aa_pod.hex
#	sendhex aa_pod.hex /p4 /m3 /t2 /g36864 /b16000
#	sendhex aa_pod.hex /p4 /m3 /t2 /g36864
#	sendhex aa_pod.hex /p4 /m7 /t2 /g36864 /b19200
#	sendhex aa_pod.hex /p4 /m7 /t2 /g36864 /b25000
	ul_sendhex -m 7 -g 0
	pause
	ul_sendhex -m 7 -g 0
	ul_sendhex -m 7 -g 36864 aa_pod.hex 