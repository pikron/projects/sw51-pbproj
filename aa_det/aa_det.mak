#   Project file pro detektor AAA
#         (C) Pisoft 1996

aa_det.obj: aa_det.asm config.h
	a51 aa_det.asm $(par) debug

aa_det.   : aa_det.obj ..\pblib\pb.lib
	l51 aa_det.obj,..\pblib\pb.lib code(0A000H) xdata(8000H) ramsize(100h) ixref

aa_det.hex : aa_det.
	ohs51 aa_det

	  : aa_det.hex
#	sendhex aa_det.hex /p4 /m6 /t2 /g40960 /b19200
#	sendhex aa_det.hex /p4 /m3 /t2 /g40960
	ul_sendhex -m 6 -g 0
	ul_sendhex -m 6 -g 40960 aa_det.hex
