$NOMOD51
;********************************************************************
;*                    DETEKTOR - AA_DET.ASM                         *
;*                       Hlavni modul                               *
;*                  Stav ke dni 10.07.1996                          *
;*                      (C) Pisoft 1996                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_AL)
$INCLUDE(%INCH_AF)
$INCLUDE(%INCH_ADR)
$INCLUDE(%INCH_ULAN)
$INCLUDE(%INCH_UL_OI)
$INCLUDE(%INCH_REGS)
$INCLUDE(%INCH_MR_DEFS)
$LIST

; *******************************************************************

%DEFINE (WITH_ADS_G)	(1)	; s pameti zesileni AD7710

EXTRN	CODE(PRINThb,PRINThw,INPUThw,SEL_FNC,VEC_CLR)
EXTRN	CODE(MONITOR)
EXTRN	CODE(xMDPDP)
PUBLIC	INPUTc,KBDBEEP,RES_STAR

AA_DE_C SEGMENT CODE
AA_DE_D SEGMENT DATA
AA_DE_B SEGMENT DATA BITADDRESSABLE
AA_DE_X SEGMENT XDATA

RSEG	AA_DE_B

HW_FLG: DS    1
ITIM_RF BIT   HW_FLG.7
FL_DIPR	BIT   HW_FLG.6		; Displej pripojen

RSEG	AA_DE_X

STATUS:	DS    2

TMP:	DS    16
TMP1:	DS    16

RSEG	AA_DE_C

RES_STAR:
	MOV   IEN0,#0
	MOV   IEN1,#0
	MOV   SP,#80H
	MOV   PCON,#10000000B ; Bd = OSC/12/16/(256-TH1)
	MOV   CTCON,#00000010B
	MOV   TCON, #01010101B ; citac 0 a 1 cita ; interapy hranou
	CALL  RAM_TEST
	CALL  VEC_CLR	      ; ?!
	%VECTOR(EXTI1,I_TIME1); Realny cas z vnejsiho zdroje
	MOV   CINT25,#1
	%VECTOR(T2CAP0,I_ADS)
	SETB  EX1
	MOV   HW_FLG,#0
	MOV   LED_FLG,#0

	CALL  I_TIMRI
	CALL  I_TIMRI

	CALL  LCDINST
	JNZ   STRT30
	SETB  FL_DIPR
	CALL  LEDWR
STRT30:
	CLR   A
	MOV   DPTR,#uL_SBPO
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#STATUS
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A

	MOV   A,#6
	CALL  I_U_LAN
	CALL  uL_OINI

	MOV   FL_ABS,#ZER_MSK
	MOV   A,#4
	MOV   DPTR,#ABS_FI
	MOVX  @DPTR,A
	CLR   A
	MOV   DPTR,#CAL_PH
	MOVX  @DPTR,A
	%LDMXi (ADC_IFI,ADC_IFI_C)
	%LDMXi (ADC_CTR1,ADC_CTR_C)
	%LDMXi (ADC_CTR2,ADC_CTR_C)

	JMP   L0

; Test pameti RAM
RAM_TEST:
	MOV   R0,#0
RAM_T2:	MOV   DPTR,#TIME	; testovaci bunky pameti
	MOV   A,R0
	MOVX  @DPTR,A
	CPL   A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#TIME	; testovaci bunky pameti
	MOVX  A,@DPTR
	XRL   A,R0
	JNZ   RAM_T5
	INC   A
	MOVC  A,@A+DPTR
	CPL   A
	XRL   A,R0
	JNZ   RAM_T5
	%WATCHDOG
	DJNZ  R0,RAM_T2
	RET
RAM_T5:	CALL  LCDINST
	JNZ   RAM_T9
	MOV   DPTR,#T_RAM_E
	CALL  cPRINT
	MOV   R0,#0
RAM_T7:	MOVX  A,@DPTR
	DJNZ  R1,RAM_T7
	DJNZ  R0,RAM_T7
RAM_T9:	MOV   SP,#8
	JMP   RESET

T_RAM_E:DB    'RAM unreachable',0

; Vstup znaku s cekanim

INPUTc:	CALL  SCANKEY
	JZ    INPUTc
	RET

; Pipnuti na klavese klavesnice

;KBDBEEP:JMP   KBDSTDB
KBDBEEP:MOV   A,#2
BEEP_D:	MOV   DPTR,#BEEPTIM
	MOVX  @DPTR,A
	SETB  %BEEP_FL
	MOV   DPTR,#LED       ; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
	MOV   A,R2
	RET

I_U_LAN:PUSH  ACC
;	CLR   A
;	MOV   A,#10	; 9600 pro krystal 18432 kHz
	MOV   A,#3	; 19200 pro krystal 110592
	MOV   R0,#1
	CALL  uL_FNC	; Rychlost
	POP   ACC
	MOV   R0,#2
	CALL  uL_FNC	; Adresa
	MOV   R2,#0
	MOV   R0,#3
	CALL  uL_FNC	; Delka IB OB
	MOV   R2,#0
	MOV   R0,#4
	CALL  uL_FNC	; Rychle bloky
	MOV   R0,#0
	CALL  uL_FNC	; Start
	RET


; *******************************************************************
;
; Casove preruseni

PUBLIC  KBDTIMR

RSEG	AA_DE_D

DINT25  EQU   27   ; Delitel EXINT1 na 25 Hz
CINT25: DS    1

RSEG	AA_DE_X

BEEPTIM:DS    1	   ; Timer delky pipani

N_OF_T  EQU   5    ; Pocet timeru
TIMR1:  DS    1    ; Timery jsou decrementovany
TIMR2:  DS    1    ; s frekvenci 25 Hz
TIMR_WAIT:
TIMR3:  DS    1
KBDTIMR:DS    1
TIMRI:  DS    1
TIME:   DS    2    ; Cas v 0.01 min              =====

RSEG	AA_DE_C

USING   3
I_TIME1:CLR   IE1		; Cast s pruchodem 675 Hz
	PUSH  ACC
	PUSH  PSW
	MOV   PSW,#AR0
	PUSH  B
	PUSH  DPL
	PUSH  DPH

	DJNZ  CINT25,I_TIMR1	; Konec casti spruchodem 675 Hz
	MOV   CINT25,#DINT25	; Pruchod s frekvenci 25 Hz

	MOV   DPTR,#ADS_WDG	; Kontrola spravne cinnosti ADC
	MOVX  A,@DPTR
	JNZ   I_TIM22
	MOV   DPTR,#STATUS	; Chyba v cinnosti ADC
	MOV   A,#01H
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#0FAH
	MOVX  @DPTR,A
	SJMP  I_TIM23
I_TIM22:DEC   A
	MOVX  @DPTR,A
I_TIM23:

	MOV   DPTR,#BEEPTIM
	MOVX  A,@DPTR
	JZ    I_TIM80
	DEC   A
	MOVX  @DPTR,A
	JNZ   I_TIM80
	CLR   %BEEP_FL
	MOV   DPTR,#LED       ; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
I_TIM80:%WATCHDOG
	CALL  TMC_REG	      ; Regulace teploty

	MOV   DPTR,#TIMR1
	MOV   B,#N_OF_T-1
I_TIME2:MOVX  A,@DPTR
	JZ    I_TIME3
	DEC   A
	MOVX  @DPTR,A
I_TIME3:INC   DPTR
	DJNZ  B,I_TIME2
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	JNB   ITIM_RF,I_TIMR1
;!!!!!	JB    ACC.7,I_TIME4
I_TIMR1:POP   DPH
	POP   DPL
	POP   B
	POP   PSW
	POP   ACC
	SETB  EA
I_TIMRI:RETI

; *******************************************************************
; Spoluprace s AD7710

AD_DR	BIT   P1.0
AD_DI	BIT   P1.1
AD_DO	BIT   P4.5
AD_CLK	BIT   P4.6	; Positive clock impuls
AD_CS	BIT   P4.7

AD_RFS1	EQU   NOT 01H	; Cteni z ADC 1
AD_TFS1	EQU   NOT 02H	; Zapis do ADC 1
AD_RFS2	EQU   NOT 04H	; Cteni z ADC 2
AD_TFS2	EQU   NOT 08H	; Zapis do ADC 2
AD_SYN1	EQU   NOT 10H	; SYNC pro ADC1
AD_SYN2	EQU   NOT 20H	; SYNC pro ADC2
AD_NA0	EQU   NOT 80H	; Vystup 0 na vodici A0

EADS	BIT   ECT0	; int T2CAP0
ADSI	BIT   CTI0

%DEFINE (ADSDELAY) (
	NOP
)

ADS8IN: CLR   AD_CS
	MOV   A,#1
ADS8IN1:%ADSDELAY
	SETB  AD_CLK
	;ADSDELAY
	PUSH  ACC
	CLR   A
	MOV   C,AD_DI
	ADDC  A,#0
	MOV   C,AD_DI
	ADDC  A,#0
	MOV   C,AD_DI
	ADDC  A,#0
	MOV   C,ACC.1
	POP   ACC
	CLR   AD_CLK
	RLC   A
	JNC   ADS8IN1
ADS8INR:RET

ADS_CTR:SETB  AD_CS
	SETB  C
	SJMP  ADS8OU2

ADS8OUT:CLR   AD_CS
	SETB  C
	SJMP  ADS8OU2
ADS8OU1:MOV   AD_DO,C
	%ADSDELAY
	SETB  AD_CLK
	CLR   C
ADS8OU2:%ADSDELAY
	CLR   AD_CLK
	RLC   A
	JNZ   ADS8OU1
	RET

ADC_RD:	CALL  ADS_CTR
	CLR   AD_CS
	MOV   R7,#0
	CALL  ADS8IN
	MOV   R6,A
	CALL  ADS8IN
	MOV   R5,A
	CALL  ADS8IN
	MOV   R4,A
	SETB  AD_CS
	RET

ADC_WR:	CALL  ADS_CTR
	CLR   AD_CS
	MOV   A,R6
	CALL  ADS8OUT
	MOV   A,R5
	CALL  ADS8OUT
	MOV   A,R4
	CALL  ADS8OUT
	SETB  AD_CS
	RET

ADC_WRC:MOV   A,#AD_TFS1 AND AD_NA0
	CALL  ADC_WR
	MOV   A,#AD_TFS2 AND AD_NA0
	CALL  ADC_WR
	RET

; *******************************************************************
; Plovouci filtr

RSEG	AA_DE_X

FI_MLEN	EQU   64	; Maximalni delka plovouciho filtru

FI1_DAT:
FI1_LEN:DS    1		; Aktualni delka filtru
FI1_POS:DS    1		; Aktualni pozice
FI1_TRD:DS    1		; Pocet prevodu za ktere bude filtr redy
FI1_SUM:DS    4		; Suma prvniho filtru
FI1_BUF:DS    3*FI_MLEN	; Buffer prvniho filtru

FI2_DAT:
FI2_LEN:DS    1		; Druhy filtr
FI2_POS:DS    1		; Aktualni pozice
FI2_TRD:DS    1
FI2_SUM:DS    4
FI2_BUF:DS    3*FI_MLEN

RSEG	AA_DE_C

; Nastvi delku filtru [DPTR] na hodnotu v ACC

FI_SETL:MOV   R1,A
	ADD   A,#-FI_MLEN
	JNC   FI_SL10
	MOV   R1,#FI_MLEN
FI_SL10:MOV   A,R1
	MOVX  @DPTR,A		; FI_LEN
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A		; FI_POS
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A		; FI_TRD
	INC   DPTR
	MOV   R0,#4		; FI_SUM
	CLR   A
FI_SL20:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,FI_SL20
	RET

; Provede filtraci pro filtr [DPTR], vstup R456 NEW, vystup R4567

FI_DO:	MOVX  A,@DPTR	; FI_LEN
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR	; FI_POS
	INC   A
	MOV   R1,A
	XRL   A,R0
	JNZ   FI_DO10
	MOV   R1,#0
FI_DO10:MOV   A,R1
	MOVX  @DPTR,A
	RL    A		; FI_POS*3
	ADD   A,R1
	ADD   A,#4
	MOV   R1,A
	INC   DPTR
	MOVX  A,@DPTR	; FI_TRD
	JZ    FI_DO15
	DEC   A
	MOVX  @DPTR,A
	SETB  F0
	INC   DPTR
	MOV   A,DPL
	MOV   R2,A
	ADD   A,R1
	MOV   DPL,A
	MOV   A,DPH
	MOV   R3,A
	ADDC  A,#0
	MOV   DPH,A
	MOV   A,R4	; Ulozit NEW
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	MOV   R7,#0     ; R4567 = NEW
	SJMP  FI_DO20
FI_DO15:CLR   F0
	INC   DPTR
	MOV   A,DPL
	MOV   R2,A
	ADD   A,R1
	MOV   DPL,A
	MOV   A,DPH
	MOV   R3,A
	ADDC  A,#0
	MOV   DPH,A
	CLR   C		; Ulozit NEW
	MOVX  A,@DPTR	; R4567 = NEW - OLD
	XCH   A,R4
	MOVX  @DPTR,A
	SUBB  A,R4
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R5
	MOVX  @DPTR,A
	SUBB  A,R5
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R6
	MOVX  @DPTR,A
	SUBB  A,R6
	MOV   R6,A
	CLR   A
	SUBB  A,#0
	MOV   R7,A
FI_DO20:MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR	; FI_SUM = FI_SUM + NEW - OLD
	ADD   A,R4	; R4567 = FI_SUM
	MOV   R4,A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOV   R5,A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R6
	MOV   R6,A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R7
	MOV   R7,A
	MOVX  @DPTR,A
	RET

; *******************************************************************
; Vypocty absorbanci

; Nastavi prevodnik na (OAD_CTR or R7<<8) a ADC_IFI
; Konfiguracni registr prevodniku
;   MD2 MD1 MD0 G2 G1 G0 CH PD | WL IO BO B/U FS11-8 | FS8-0
; Operating Mode MD2-0
;	0     Normal
;	1     Vnitrni nula a vnitrni scale
;	2     Active step 1 - external zero
;	3     Active step 2 - external full scale
;	4     Vnejsi nula a vnitrni scale
;	5     Active background cal.
;	6     Read/write zero scale coeficient with A0=1
;	7     Read/write full-scale coeficient with A0=1
; PGA Gain G2-G0
;	0->1; 1->2; 2->4; ... 6->64; 7->128
; Channel selection CH
;	0->AIN1; 1->AIN2
; Power down PD
;	1->Power down
; Word length WL
;	0->16-bit; 1->24-bit
; Output compensation current IO
;	0->off; 1->on
; Burn out current BO
;	0->off; 1->on
; Bipolar/Unipolar selection B/U
;	0->bipolar ; 1->unipolar
;
; Spodni ctyri bity OAD_CTR jsou v MDETu vyuzity nasledovne
; bit 3	0->linear; 1->logaritmic/absorbance
; bit 2	0->nop;    1->zerro
; bit 1	0->nop;    1->change
; bit 0	0->nop;    1->stop

; Defautni nastaveni AD7710
ADC_IFI_C SET 1954	; Zlomova frekvence filtru
ADC_CTR_C SET 0090H	; Konfigurace ADC

ADC_CTRGM SET 1C00H	; Maska zesileni v ADC_CTR1/2

RSEG	AA_DE_B

FL_ABS:	DS    1
FL_ZER1	BIT   FL_ABS.7	; Vynuluj 1. absorbanci
FL_ZER2	BIT   FL_ABS.6	; Vynuluj 2. absorbanci
FL_FICH	BIT   FL_ABS.5	; Zmen rad filtru
FL_RAW	BIT   FL_ABS.4	; Vystup dat bez logaritmu
FL_CH2	BIT   FL_ABS.3	; Vystup druheho kanalu
ZER_MSK SET   0E0H
CH2_MSK	SET   008H
RAW_MSK	SET   010H

RSEG	AA_DE_X

CAL_MPH	EQU   3+20	; Pocatecni phase interniho nulovani
CAL_PH:	DS    1		; Phase interniho nulovani
ADC_IFI:DS    2		; Rad filtru v AD7710
ADC_CTR1:DS   2		; Ridici slovo pro 1. AD7710
ADC_CTR2:DS   2		; Ridici slovo pro 2. AD7710

ABS_FI:	DS    1		; Hodnota filtru

ADS1:	DS    4		; Hodnota vyctena z 1. prevodniku
ADS2:	DS    4		; Hodnota vyctena z 2. prevodniku

ABS1:	DS    4		; 1. absorbance
ABS1_Z:	DS    4		; nula 1. absorbance
ABS2:	DS    4		; 2. absorbance
ABS2_Z:	DS    4		; nula 2. absorbance

ADS_SAV:DS    14	; Pro ulozeni AKUM z knihovny PB_AF

ADS_WDG:DS    1		; Watchdog zpravne cinnosti ADC
ADS_WDT	EQU   25*3	; 3s

RSEG	AA_DE_C

USING   2
I_ADS:  CLR   ADSI
	PUSH  ACC
	PUSH  PSW
	MOV   PSW,#AR0
	PUSH  B
	PUSH  DPL
	PUSH  DPH
	MOV   R0,#AKUM		; Ulozeni stvu PB_AF
	MOV   R2,#14
	MOV   DPTR,#ADS_SAV
	CALL  xiSVs
	MOV   DPTR,#ADS_WDG	; ADC v cinnosti
	MOV   A,#ADS_WDT
	MOVX  @DPTR,A

	JB    AD_DR,I_ADS05
	JNB   AD_DR,I_ADS10
I_ADS05:MOV   A,#10		; Prislo ruseni
	CALL  BEEP_D
	JMP   I_ADSR

I_ADS10:MOV   DPTR,#CAL_PH
	MOVX  A,@DPTR
	JZ    I_ADS11
	JMP   I_ADS50		; Vnitrni nulovani AD7710
I_ADS11:JNB   FL_FICH,I_ADS15
	CLR   FL_FICH		; Zmena radu filtru
	MOV   DPTR,#ABS_FI
	MOVX  A,@DPTR
	INC   A
	MOV   R0,A
	MOV   A,#080H
I_ADS12:RL    A
	DJNZ  R0,I_ADS12
	MOV   R7,A
	MOV   DPTR,#FI1_DAT
	CALL  FI_SETL
	MOV   A,R7
	MOV   DPTR,#FI2_DAT
	CALL  FI_SETL
I_ADS15:
	MOV   A,#AD_RFS1	; Nacteni dat z 1. prevodniku
	CALL  ADC_RD
	MOV   DPTR,#ADS1
	CALL  xSVl
	MOV   DPTR,#FI1_DAT	; Filtrace
	CALL  FI_DO
	JB    F0,I_ADS29
	MOV   DPTR,#ABS_FI
	MOVX  A,@DPTR
	CPL   A
	INC   A
	CALL  CONVl_S		; Prevod na float
	MOV   A,R6
	ANL   A,#07FH
	MOV   R6,A
	JB    FL_RAW,I_ADS27
	CALL  LOGfRf		; Logaritmus
	JBC   FL_ZER1,I_ADS28
	MOV   DPTR,#ABS1_Z
	MOV   R0,#AKUM
	CALL  xiLDl		; Odecteni nuly
	CALL  SUBf
I_ADS27:MOV   DPTR,#ABS1
	CALL  xSVl
	SJMP  I_ADS29
I_ADS28:MOV   DPTR,#ABS1_Z	; Vynulovani absorbance
	CALL  xSVl
	JNB   %LVIS_FL,I_ADS29
	MOV   DPTR,#ADS1+2
	MOVX  A,@DPTR
	ADD   A,#-10H
	JC    I_ADS29
	MOV   DPTR,#STATUS	; Chyba zarovky
	MOV   A,#01H
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#0FBH
	MOVX  @DPTR,A
I_ADS29:

	MOV   A,#AD_RFS2	; Nacteni dat z 2. prevodniku
	CALL  ADC_RD
	MOV   DPTR,#ADS2
	CALL  xSVl
	MOV   DPTR,#FI2_DAT	; Filtrace
	CALL  FI_DO
	JB    F0,I_ADS39
	MOV   DPTR,#ABS_FI
	MOVX  A,@DPTR
	CPL   A
	INC   A
	CALL  CONVl_S		; Prevod na float
	MOV   A,R6
	ANL   A,#07FH
	MOV   R6,A
	JB    FL_RAW,I_ADS37
	CALL  LOGfRf		; Logaritmus
	JBC   FL_ZER2,I_ADS38
	MOV   DPTR,#ABS2_Z
	MOV   R0,#AKUM
	CALL  xiLDl		; Odecteni nuly
	CALL  SUBf
I_ADS37:MOV   DPTR,#ABS2
	CALL  xSVl
	SJMP  I_ADS39
I_ADS38:MOV   DPTR,#ABS2_Z	; Vynulovani absorbance
	CALL  xSVl
	JNB   %LVIS_FL,I_ADS39
	MOV   DPTR,#ADS2+2
	MOVX  A,@DPTR
	ADD   A,#-10H
	JC    I_ADS39
	MOV   DPTR,#STATUS	; Chyba zarovky
	MOV   A,#02H
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#0FBH
	MOVX  @DPTR,A
I_ADS39:
	JMP   I_ADS80

I_ADS50:DEC   A			; Vnitrni kalibrace prevodniku
	MOVX  @DPTR,A
	JNZ   I_ADS52
	JNB   %LVIS_FL,I_ADS51
	MOV   PWM0,#0FFH	; 1) Ukonceni a synchronizace
I_ADS51:MOV   A,#AD_SYN1 AND AD_SYN2
	CALL  ADS_CTR
	NOP
	CLR   AD_CS
	NOP
	NOP
	NOP
	NOP
	SETB  AD_CS
	MOV   A,#AD_RFS1
	CALL  ADC_RD
	MOV   A,#AD_RFS2
	CALL  ADC_RD
	SJMP  I_ADS59
I_ADS52:DEC   A
	JNZ   I_ADS53
	MOV   R7,#80H		; 2) Vnejsi nulovani
	CALL  ADC_SCF
	SJMP  I_ADS59
I_ADS53:DEC   A
	JNZ   I_ADS54
	MOV   PWM0,#000H	; 3) Vnitrni nulovani
	MOV   R7,#80H
	CALL  ADC_SCF
	SJMP  I_ADS59
I_ADS54:MOV   A,#AD_RFS1	; 4..) cekat
	CALL  ADC_RD
	MOV   A,#AD_RFS2
	CALL  ADC_RD
	MOV   PWM0,#000H

I_ADS59:

I_ADS80:MOV   DPTR,#TMP
	MOVX  A,@DPTR
	ADD   A,#1
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#0
	MOVX  @DPTR,A

;	CALL  LAN_TM		; !!!!!!!!!!!!!!!!!!!!!

I_ADSR:	MOV   R0,#AKUM
	MOV   R2,#14
	MOV   DPTR,#ADS_SAV
	CALL  xiLDs
	POP   DPH
	POP   DPL
	POP   B
	POP   PSW
	POP   ACC
	RETI

; Nastavi prevodnik na (ADC_CTR1/2 or R7<<8) a ADC_IFI

ADC_SCF:MOV   DPTR,#ADC_IFI
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   DPTR,#ADC_CTR1
	MOVX  A,@DPTR
	ANL   A,#0F0H
	XCH   A,R5
	ANL   A,#00FH
	ORL   A,R5
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	ORL   A,R7
	MOV   R6,A
	MOV   A,#AD_TFS1 AND AD_NA0
	CALL  ADC_WR		; Set parameters of 1. ADC
	MOV   DPTR,#ADC_CTR2
	MOVX  A,@DPTR
	ANL   A,#0F0H
	XCH   A,R5
	ANL   A,#00FH
	ORL   A,R5
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	ORL   A,R7
	MOV   R6,A
	MOV   A,#AD_TFS2 AND AD_NA0
	CALL  ADC_WR		; Set parameters of 2. ADC
	RET

; Ceka na AD_DR nevyuziva-li se preruseni
ADS_WAIT:MOV  DPTR,#ADS_WDG	; Kontrola spravne cinnosti ADC
	MOV   A,#ADS_WDT
	MOVX  @DPTR,A
ADS_WA2:JNB   AD_DR,ADS_WA9
	MOVX  A,@DPTR
	JNZ   ADS_WA2
ADS_WA9:RET

; Inicializace prevodniku
ADS_INI:MOV   C,EADS
	PUSH  PSW
	CLR   EADS
	CALL  ADS_WAIT		; Cekani na AD_DR
	MOV   R7,#20H		; Vnitrni nula
	CALL  ADC_SCF
	CALL  DB_W_10
	CALL  ADS_WAIT		; Cekani na AD_DR

	MOV   R7,#000H		; Normal rezim
	CALL  ADC_SCF

	MOV   A,#AD_SYN1 AND AD_SYN2
	CALL  ADS_CTR
	CLR   AD_CS
	CALL  DB_W_10
	SETB  AD_CS
	POP   PSW
	MOV   EADS,C
	RET

; ADC_CTR_C SET 0090H	; Konfigurace ADC
; ADC_CTRGM SET 1C00H	; Maska zesileni v ADC_CTR1/2

; Ponecha pouze zesileni v ridicim slove
ADS_CHK_G:
	CALL  xLDR45i
	ANL   A,R4
	CJNE  A,#0FFH,ADS_CHK_G1
	MOV   R5,#HIGH ADC_CTR_C
ADS_CHK_G1:
	MOV   R4,#LOW ADC_CTR_C
	MOV   A,R5
	ANL   A,#HIGH ADC_CTRGM
	ORL   A,#(HIGH ADC_CTR_C) AND NOT (HIGH ADC_CTRGM)
	MOV   R5,A
	JMP   xSVR45i

S_ADS_G_U:
	CALL  xMDPDP
	INC   DPTR
	MOV   A,R4
	RL    A
	RL    A
	ANL   A,#HIGH ADC_CTRGM
	MOV   R4,A
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	ANL   A,#NOT HIGH ADC_CTRGM
	ORL   A,R4
	MOVX  @DPTR,A
	MOV   EA,C
	MOV   A,#1
	JMP   ADS_INI

G_ADS_G_U:
	CALL  xMDPDP
	INC   DPTR
	MOVX  A,@DPTR
	ANL   A,#HIGH ADC_CTRGM
	RR    A
	RR    A
	MOV   R4,A
	CLR   A
	MOV   R5,A
	INC   A
	RET

; *******************************************************************

RSEG	AA_DE_X

uL_SBPO:DS    2
LAN_TMB:DS    15

RSEG	AA_DE_C

XC_SBPO:MOV   DPTR,#uL_SBPO
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R1,A
	MOV   DPTR,#uL_SBP
	MOVX  A,@DPTR
	XCH   A,R0
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R1
	MOVX  @DPTR,A
	MOV   DPTR,#uL_SBPO
	MOV   A,R0
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A
	RET

LAN_TM: CALL  uL_STR
	JNB   ES,LAN_TME
	CALL  XC_SBPO
	JMP   LAN_A

LAN_TMR:CALL  XC_SBPO
LAN_TME:RET

LAN_A1:	;MOV   DPTR,#uL_GRP
	;MOVX  A,@DPTR
	MOV   A,#2
	MOV   R4,A
	MOV   R5,#4FH
	CLR   F0
	CALL  uL_S_OP
	JNB   F0,LAN_A
	; informovat o chybe
	JMP   LAN_TMR

LAN_A:  MOV   DPTR,#ABS1
	JNB   FL_CH2,LAN_A2
	MOV   DPTR,#ABS2
LAN_A2:	CALL  xLDl

LAN_A3:	MOV   A,R6
	RLC   A
	MOV   A,R7
	JZ    LAN_A4
	RRC   A
	DEC   A
	MOV   R7,A
	MOV   A,R6
	MOV   ACC.7,C
LAN_A4:	MOV   R6,A
	MOV   DPTR,#LAN_TMB
	CALL  xSVl

	MOV   DPTR,#LAN_TMB
	MOV   R4,#4
	MOV   R5,#0
	CLR   F0
	CALL  uL_S_WR
	JB    F0,LAN_A1
	JMP   LAN_TMR

; *******************************************************************
; Regulace teploty

EXTRN	CODE  (MR_PIDP,cxMOVE)

TMC_ADC	EQU   6
TMC_OUT	BIT   P4.2

RSEG	AA_DE_X

TMC1:	DS    OMR_LEN
TMC1_FLG XDATA TMC1+OMR_FLG
TMC1_AT	XDATA TMC1+OMR_AP
TMC1_AG	XDATA TMC1+OMR_AS
TMC1_RT	XDATA TMC1+OMR_RPI
TMC1_EN	XDATA TMC1+OMR_ENE
TMC1_FL:DS    2
TMC1_RD:DS    2

TMC1_OC:DS    2
TMC1_MC:DS    2

TMC_PWC:DS    1

RSEG	AA_DE_C

TMC_REG:MOV   DPTR,#TMC1_FLG
	MOVX  A,@DPTR
	JNB   ACC.BMR_ENI,TMC_39	; mereni vypnuto
	MOV   R7,A
; Mereni
TMC_R10:MOV   A,ADCON
	JNB   ACC.3,TMC_R15
	RET
TMC_R15:
    %IF(1)THEN(
	RL    A		; rozsah ADC do 0FFC0h
	RL    A
	RL    A
	ANL   A,#7
	MOV   R4,A
	MOV   A,ADCH
	RL    A
	RL    A
	RL    A
	MOV   R5,A
	ANL   A,#7
	XCH   A,R5
	ANL   A,#NOT 7
    )ELSE(
	RL    A		; rozsah ADC do 07FE0h
	RL    A
	ANL   A,#3
	MOV   R4,A
	MOV   A,ADCH
	RL    A
	RL    A
	MOV   R5,A
	ANL   A,#3
	XCH   A,R5
	ANL   A,#NOT 3
    )FI
	ORL   A,R4
	MOV   R4,A
	MOV   DPTR,#TMC1_FL
	MOVX  A,@DPTR
	ADD   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOVX  @DPTR,A
	MOV   A,#TMC_ADC	; Spustit dalsi prevod
	MOV   ADCON,A
	SETB  ACC.3
	MOV   ADCON,A
; Generovani PWM
	MOV   DPTR,#TMC_PWC
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	ANL   A,#1FH
	MOV   R4,A			; PWM counter
	SETB  C
	MOV   A,R7
	JNB   ACC.BMR_ENR,TMC_35	; Vypnuto
	MOV   DPTR,#TMC1_EN+1
	MOVX  A,@DPTR
	SETB  C
	JB    ACC.7,TMC_35
	SUBB  A,R4
TMC_35:	MOV   TMC_OUT,C
	MOV   DPTR,#TMC_PWC
	MOVX  A,@DPTR
	ANL   A,#1FH
	JZ    TMC_R50
TMC_39:	RET
; Nacteni filtru a prepocet
TMC_R50:CLR   F0
	MOV   DPTR,#TMC1_FL
	MOVX  A,@DPTR
	MOV   R2,A
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A		; R23 = TMCx_FL
	CLR   A			; TMCx_FL = 0
	MOVX  @DPTR,A
	MOV   DPTR,#TMC1_RD	; Ulozeni raw data
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	ORL   A,R2		; Kontrola zkratu
	JNZ   TMC_R51
	SETB  F0
TMC_R51:MOV   DPTR,#TMC1_MC
	MOVX  A,@DPTR
	MOV   R4,A		; R45=R23*TMCx_MC/2^16
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	JNB   ACC.7,TMC_R52
	CALL  MULi		; Zaporna konstanta TMCx_MC
	MOV   A,R6
	SUBB  A,R2
	MOV   R4,A
	MOV   A,R7
	SUBB  A,R3
	MOV   R5,A
	SJMP  TMC_R53
TMC_R52:CALL  MULi		; Kladna konstanta TMCx_MC
	MOV   A,R6
	MOV   R4,A
	MOV   A,R7
	MOV   R5,A
TMC_R53:MOV   DPTR,#TMC1_OC
	MOVX  A,@DPTR
	ADD   A,R4
	MOV   R4,A		; R45=R45+TMCx_OC
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOV   R5,A
	JNB   OV,TMC_R54
	%LDR45i(7FFFH)
	JB    ACC.7,TMC_R54
	%LDR45i(-7FFFH)
TMC_R54:
; Rozdil
TMC_R55:CLR   C
	MOV   DPTR,#TMC1_AT	; TMC1_AT=R45
	MOVX  A,@DPTR           ; R45-=old TMC1_AT
	XCH   A,R4
	MOVX  @DPTR,A
	SUBB  A,R4
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R5
	MOVX  @DPTR,A
	SUBB  A,R5
	MOV   R5,A
	MOV   DPTR,#TMC1_AG
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
; Vlastni regulator
TMC_R60:MOV   DPTR,#TMC1_FLG
	MOVX  A,@DPTR
	JNB   ACC.BMR_ENR,TMC_R69
	MOV   DPTR,#TMC1+OMR_ERC
	CLR   A
	MOVX  @DPTR,A
	MOV   DPTR,#TMC1
	CALL  MR_PIDP
	MOV   DPTR,#TMC1_EN
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
TMC_R69:RET

TMC_INI:%LDR45i (TMC1)
	%LDR23i (TMC1PPR)
	%LDR01i (OMR_LEN)
	CALL  cxMOVE
	MOV   DPTR,#TMC1_FL
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	%LDMXi (TMC1_OC,0)
	%LDMXi (TMC1_MC,16017)	; 32031
; Vypne topeni reaktoru
TMC_OFF:MOV   DPTR,#TMC1_FLG
	MOV   A,#MMR_ENI
	MOVX  @DPTR,A
	SETB  TMC_OUT		; Vypnout topeni
	RET

; Zapne regulaci teploty reaktoru
TMC_ON:	MOV   DPTR,#TMC1_FLG
	MOV   A,#MMR_ENI OR MMR_ENR
	MOVX  @DPTR,A
	RET

G_TMC_ST:MOV  DPTR,#TMC1_FLG
	MOVX  A,@DPTR
	JNB   ACC.BMR_ERR,TMC_ST1
	%LDR45i(0FFF1H)
	RET
TMC_ST1:MOV   R5,#0
	JNB   ACC.BMR_ENR,TMC_ST3
	MOV   A,#OMR_ENE
	MOVC  A,@A+DPTR
	MOV   R4,#1
	JZ    TMC_ST2
	MOV   R4,#2
	JNB   ACC.7,TMC_ST2
	MOV   R4,#3
TMC_ST2:RET
TMC_ST3:MOV   R4,#0
	RET

TMC1PPR:DB    0, 0,0,0, 0,0
	DB    0
	DB    0,0   			; RP
	DB    0
	DB    0,0,0			; RS
	DB    006H,0, 006H,0, 001H,0	; P I D
	DB    000H,0, 000H,0, 0,060H	; 1 2 ME
	DB    080H,0, 010H,0		; MS MA
	DB    0,0,0,0,0,0,0

; *******************************************************************
; Prace s pameti EEPROM 8582

EEP_ADR	EQU   0A0H		; Adresa EEPROM na IIC sbernici
C_S1CON EQU   11000000B ; 11000001B ; Pocatecni stav S1CON
			; 11000000B ; S1CON s delenim 960
			; 01000000B ; S1CON pro X 24.000 MHz

EEA_RD	EQU   1		; akce cteni
EEA_WR	EQU   2		; akce zapisu

RSEG	AA_DE_D

RSEG	AA_DE_X

EE_PTR:	DS    2		; ukazatel do MEM_BUF na data
EEP_TAB:DS    2		; popis dat ulozenych v bloku EEPROM

MEM_BUF:DS    100H	; Buffer pameti EEPROM

RSEG	AA_DE_C

EE_WAIT:JNB   SI,$
	MOV   A,S1STA
	RET

EE_WRA:	MOV   S1CON,#C_S1CON	; OR 20H
	SETB  STO
	CLR   SI
	SETB  STA		; START
	CALL  EE_WAIT
	CLR   STA
	CJNE  A,#008H,EE_WRA
	MOV   A,R5
	MOV   S1DAT,A		; SLA and W
	CLR   SI
	CALL  EE_WAIT
	XRL   A,#018H
	JNZ   EE_WRA9
	MOV   A,R4
	MOV   S1DAT,A		; Adresa v EEPROM
	CLR   SI
	CALL  EE_WAIT
	XRL   A,#028H
EE_WRA9:RET

; Cteni pameti EEPROM
;	DPTR .. pointer na buffer
;	R2   .. pocet ctenych byte
;	R4   .. adresa, od ktere se cte

EE_RD:	MOV   R5,#EEP_ADR
	MOV   R1,#0
EE_RD10:CLR   ES1
	CALL  EE_WRA
	JNZ   EE_ERR
	SETB  STA		; repeated START
	CLR   SI
	CALL  EE_WAIT
	CLR   STA
	CJNE  A,#010H,EE_ERR
	MOV   A,R5
	ORL   A,#1
	MOV   S1DAT,A		; SLA and R
	CLR   SI
	CALL  EE_WAIT
	CJNE  A,#040H,EE_ERR
	SETB  AA
EE_RD30:CLR   SI
	CALL  EE_WAIT
	CJNE  A,#050H,EE_ERR
	MOV   A,S1DAT		; DATA
	MOVX  @DPTR,A
	XRL   A,R1
	INC   A
	MOV   R1,A
	INC   DPTR
	DJNZ  R2,EE_RD30
	CLR   AA
	CLR   SI
	CALL  EE_WAIT
	CJNE  A,#058H,EE_ERR
	MOV   A,S1DAT		; XORSUM
	XRL   A,R1
	SETB  STO		; STOP
	CLR   SI
	RET

EE_ERR:	SETB  STO
	CLR   SI
	SETB  F0
	ORL   A,#07H
	RET

; Zapis do pameti EEPROM
;	DPTR .. pointer na buffer
;	R2   .. pocet zapisovanych byte
;	R4   .. adresa, od ktere se zapisuje

EE_WR:	MOV   R5,#EEP_ADR
	MOV   R1,#0
EE_WR10:CLR   ES1
EE_WR20:MOV   R0,#0
EE_WR21:NOP
	DJNZ  R0,EE_WR21
	MOV   R0,#100
	MOV   A,R2
	JZ    EE_WR30
EE_WR22:CALL  EE_WRA
	JZ    EE_WR23
	DJNZ  R0,EE_WR22
	SJMP  EE_ERR
EE_WR23:MOV   R0,#4
EE_WR24:MOVX  A,@DPTR
	MOV   S1DAT,A
	CLR   SI
	XRL   A,R1
	INC   A
	MOV   R1,A
	INC   DPTR
	DEC   R2
	INC   R4
	CALL  EE_WAIT
	CJNE  A,#028H,EE_ERR
	MOV   A,R2
	JZ    EE_WR26
	DJNZ  R0,EE_WR24
EE_WR26:SETB  STO
	CLR   SI
	SJMP  EE_WR20
EE_WR30:CALL  EE_WRA
	JZ    EE_WR34
	DJNZ  R0,EE_WR30
	SJMP  EE_ERR
EE_WR34:MOV   A,R1
	MOV   S1DAT,A
	CLR   SI
	CALL  EE_WAIT
	CJNE  A,#028H,EE_ERR
	SETB  STO
	CLR   SI
	RET

; R45 := [EE_PTR++]
EEA_RDi:MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOV   R0,DPH
	MOV   A,DPL
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	RET

; [EE_PTR++] := R45
EEA_WRi:MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   R0,DPH
	MOV   A,DPL
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	RET

; provadi EEA_RD, EEA_WR pro iteger cislo
EEA_Mi:	CJNE  R0,#EEA_RD,EEA_Mi5
	CALL  EEA_RDi
	MOV   DPL,R2
	MOV   DPH,R3
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	RET
EEA_Mi5:CJNE  R0,#EEA_WR,EEA_Mi9
	MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  EEA_WRi
EEA_Mi9:RET

EEA_PRO:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#EE_PTR
	MOV   A,#LOW MEM_BUF
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH MEM_BUF
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
EEA_PR2:MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	ORL   A,R4
	JZ    EEA_Mi9
	PUSH  DPL
	PUSH  DPH
	MOV   A,R0
	PUSH  ACC
	CALL  JMPR45
	POP   ACC
	MOV   R0,A
	POP   DPH
	POP   DPL
	JMP   EEA_PR2

JMPR45:	MOV   A,R4
	PUSH  ACC
	MOV   A,R5
	PUSH  ACC
	RET

; Nastavi EEP_TAB a DPTR na R23
EEP_PTS:MOV   DPTR,#EEP_TAB
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	MOV   DPL,R2
	MOV   DPH,R3
	RET

; Ulozit data podle tabulky R23
EEP_WRS:CALL  EEP_PTS
	INC   DPTR
	INC   DPTR	; popis akci
	MOV   R0,#EEA_WR
	CALL  EEA_PRO
	MOV   DPTR,#EEP_TAB
	CALL  xMDPDP
	MOVX  A,@DPTR
	MOV   R2,A	; pocet prenasenych byte
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A	; pocatecni adresa v EEPROM
	MOV   DPTR,#MEM_BUF
	JMP   EE_WR

; Nacist data podle tabulky R23
EEP_RDS:CALL  EEP_PTS
	MOVX  A,@DPTR
	MOV   R2,A	; pocet prenasenych byte
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A	; pocatecni adresa v EEPROM
	MOV   DPTR,#MEM_BUF
	CALL  EE_RD
	JNZ   EEP_RD9
	MOV   DPTR,#EEP_TAB
	CALL  xMDPDP
	INC   DPTR
	INC   DPTR	; popis akci
	MOV   R0,#EEA_RD
	JMP   EEA_PRO
EEP_RD9:RET

; *******************************************************************
; Ukladani a cteni nastaveni z EEPROM

; Akce TMC ON/OFF
; R2 cislo regulatoru
EEA_TMC:CJNE  R0,#EEA_RD,EEA_TMC5
	CALL  EEA_RDi
	MOV   A,R2
	MOV   R1,A
	MOV   A,R4
	JZ    EEA_TMC2
	JMP   TMC_ON
EEA_TMC2:JMP  TMC_OFF
EEA_TMC5:CJNE R0,#EEA_WR,EEA_TMC9
	MOV   A,R2
	MOV   R1,A
	CALL  G_TMC_ST
	CALL  EEA_WRi
EEA_TMC9:RET

; Tabulky popisu akci pro ulozeni a cteni dat

; Tabulka pro SERVICE
EEC_SER:DB    030H	; pocet byte ukladanych dat
	DB    020H	; pocatecni adresa v EEPROM

	%W    (TMC1_OC)
	%W    (EEA_Mi)

	%W    (TMC1_MC)
	%W    (EEA_Mi)

    %IF(%WITH_ADS_G)THEN(
	%W    (ADC_CTR1)
	%W    (EEA_Mi)

	%W    (ADC_CTR2)
	%W    (EEA_Mi)
    )FI

	%W    (0)
	%W    (0)


; *******************************************************************

xPR_AD: MOV   A,#' '
	CALL  LCDWR
	MOV   R1,#3
	SJMP  xPRThl1

xPRThl:	MOV   R1,#4
xPRThl1:MOV   A,R1
	DEC   A
	MOVC  A,@A+DPTR
	CALL  PRINThb
	DJNZ  R1,xPRThl1
	RET

DB_W_10:MOV   R0,#10H
	SJMP  DB_WAI1

DB_WAIT:MOV   R0,#0H
DB_WAI1:%WATCHDOG
	DJNZ  R1,DB_WAI1
	DJNZ  R0,DB_WAI1
	RET

L0:	MOV   SP,#80H
	CLR   EADS
	MOV   DPTR,#ADS_WDG	; Zrusit ADS watchdog
	MOV   A,#-1
	MOVX  @DPTR,A
	SETB  EA
	JNB   FL_DIPR,L007
	MOV   DPTR,#DEVER_T
	CALL  cPRINT
L007:	CALL  DB_W_10

	CALL  TMC_INI		; Inicializace regulace reactoru

	%LDR23i(EEC_SER)	; Cteni parametru z EEPROM
	CALL  EEP_RDS
    %IF(%WITH_ADS_G)THEN(
	MOV   DPTR,#ADC_CTR1
	CALL  ADS_CHK_G
	MOV   DPTR,#ADC_CTR2
	CALL  ADS_CHK_G
    )FI
	CALL  ADS_INI		; Inicializace prevodniku

	JB    FL_DIPR,L090
	SETB  EADS		; Bez displeje
L080:	CALL  UD_OI
	JMP   L080

L090:	MOV   A,#LCD_CLR
	CALL  LCDWCOM
	SETB  EADS

L1:     %WATCHDOG
	JB    EADS,L180		; Cteni prevodniku mimo I_ADS

	MOV   A,#AD_RFS1
	CALL  ADC_RD
	MOV   DPTR,#ADS1
	CALL  xSVl

	MOV   A,#AD_RFS1 AND AD_NA0
	CALL  ADC_RD
	MOV   DPTR,#TMP
	CALL  xSVl

	MOV   A,#AD_RFS2
	CALL  ADC_RD
	MOV   DPTR,#ADS2
	CALL  xSVl

	MOV   A,#AD_RFS2 AND AD_NA0
	CALL  ADC_RD
	MOV   DPTR,#TMP+4
	CALL  xSVl

L180:	MOV   A,#LCD_HOM
	CALL  LCDWCOM
	MOV   DPTR,#ADS1	; Zobrazeni 1. prevodniku
	CALL  xPR_AD
%IF (0) THEN (
	MOV   DPTR,#TMP
	CALL  xPR_AD
	MOV   A,#LCD_HOM+040H
	CALL  LCDWCOM
)FI
%IF (1) THEN (
	MOV   DPTR,#ADS2	; Zobrazeni 2. prevodniku
	CALL  xPR_AD
) ELSE (
	MOV   DPTR,#FI1_SUM	; Zobrazeni 1. filtru
	CALL  xPRThl
)FI

%IF (0) THEN (
	MOV   A,#' '
	CALL  LCDWR
	MOV   DPTR,#CAL_PH
	MOVX  A,@DPTR
	CALL  PRINThb
)FI

	MOV   A,#LCD_HOM+040H
	CALL  LCDWCOM

	MOV   A,#' '
	CALL  LCDWR
	MOV   DPTR,#TMC1_AT	; RD AT
	CALL  xLDR45i
	MOV   R7,#62H
	CALL  PRINTi

	MOV   A,#' '
	CALL  LCDWR
	MOV   DPTR,#TMC1_RD
	CALL  xLDR45i
	CALL  PRINThw

L2:	CALL  SCANKEY
	JZ    L3
	MOV   R7,A
	MOV   DPTR,#SFT_D1
	CALL  SEL_FNC
	SJMP  L035
L3:	CALL  UD_OI
L035:	JB    EADS,L4
	%WATCHDOG
	JB    AD_DR,L2
L4:
	JMP   L1

T_RDVAL:MOV   DPL,R2
	MOV   DPH,R3
T_RDVA1:MOVX  A,@DPTR
	MOV   R0,A
	PUSH  ACC
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R1,A
	PUSH  ACC
	INC   DPTR
	CALL  cPRINT
	MOV   A,#LCD_HOM+008H
	CALL  LCDWCOM
	MOV   DPL,R0
	MOV   DPH,R1
	CALL  xLDR45i
	MOV   R7,#40H
	CALL  PRINThw
	MOV   R6,#8
	MOV   R7,#40H
	CALL  INPUThw
	POP   DPH
	POP   DPL
	JB    F0,T_RDV99
	CALL  xSVR45i
T_RDV99:RET

T_LON:	MOV   PWM0,#0FFH
	SETB  %LVIS_FL
	CALL  LEDWR
	RET

T_LOFF:	MOV   PWM0,#0
	CLR   %LVIS_FL
	CALL  LEDWR
	RET

RSEG	AA_DE_C

T_ADST1:%W    (ADC_CTR1)
	DB    LCD_CLR,'Contr1 :',0

T_ADST2:%W    (ADC_CTR2)
	DB    LCD_CLR,'Contr2 :',0

T_ADST3:%W    (ADC_IFI)
	DB    LCD_CLR,'Filter :',0

T_ADS_S:MOV   C,EADS
	CLR   EADS
	PUSH  PSW
	MOV   DPTR,#T_ADST1
	CALL  T_RDVA1
	MOV   DPTR,#T_ADST2
	CALL  T_RDVA1
	MOV   DPTR,#T_ADST3
	CALL  T_RDVA1
T_ADS10:JB    AD_DR,T_ADS10
	MOV   R7,#0
	CALL  ADC_SCF
	CALL  DB_W_10
T_ADS20:JB    AD_DR,T_ADS20
	MOV   A,#AD_SYN1 AND AD_SYN2
	CALL  ADS_CTR
	CLR   AD_CS
	CALL  DB_W_10
	SETB  AD_CS
	POP   PSW
	MOV   EADS,C
	RET

T_ADSSI:SETB  EADS
	RET

T_ADSFL:MOV   A,R2
	CPL   A
	ANL   FL_ABS,A
	MOV   A,R3
	ORL   FL_ABS,A
	RET

T_ADSCN:MOV   A,#CAL_MPH		; Spusteni vnitrniho nulovani
	MOV   DPTR,#CAL_PH
	MOVX  @DPTR,A
	RET

T_TMC1T:DB    LCD_CLR,'Temper.:',0

T_TMC1S:MOV   DPTR,#T_TMC1T
	CALL  cPRINT
	MOV   R6,#008H
	MOV   R7,#072H
	CALL  INPUTi
	JB    F0,T_TMC1S9
	MOV   DPTR,#TMC1_RT
	CALL  xSVR45i
	CALL  TMC_ON
T_TMC1S9:MOV  A,#LCD_CLR
	CALL  LCDWCOM
	RET


SFT_D1:	DB    K_RUN
	%W    (0)
	%W    (L0)

	DB    K_DP
	%W    (0)
	%W    (MONITOR)

	DB    K_DP
	%W    (0)
	%W    (MONITOR)

	DB    K_LOFF
	%W    (0)
	%W    (T_LOFF)

	DB    K_VIS
	%W    (0)
	%W    (T_LON)

	DB    K_UVH
	%W    (0)
	%W    (T_ADS_S)

	DB    K_UVL
	%W    (0)
	%W    (T_ADSSI)

	DB    K_ZERO
	DB    RAW_MSK,ZER_MSK
	%W    (T_ADSFL)

	DB    K_PRET
	%W    (0)
	%W    (T_TMC1S)

	DB    K_1
	DB    RAW_MSK OR CH2_MSK,0
	%W    (T_ADSFL)

	DB    K_2
	DB    RAW_MSK,CH2_MSK
	%W    (T_ADSFL)

	DB    K_3
	DB    CH2_MSK,RAW_MSK
	%W    (T_ADSFL)

	DB    K_4
	DB    0,CH2_MSK OR RAW_MSK
	%W    (T_ADSFL)

	DB    K_0
	DB    0,0
	%W    (T_ADSCN)

	DB    0

DEVER_T:DB    LCD_CLR,'%VERSION'
	DB    C_LIN2 ,' (c) PiKRON 1996',0

; *******************************************************************
; Komunikace pres uLan

RSEG	AA_DE_X

TMP_U:	DS    16

RSEG	AA_DE_C

uL_OINI:%LDR45i (OID_1IN)
	%LDR67i (OID_1OUT)
	JMP   US_INIT

; Identifikace typu pristroje
PUBLIC	uL_IDB,uL_IDE
uL_IDB: DB    '.mt %VERSION .uP 51x',0
uL_IDE:

; Subsystem komunikace

UD_OI:	JMP   UI_PR

; Status

G_STATUS:
	MOV   DPTR,#STATUS
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOV   EA,C
	MOVX  A,@DPTR
	MOV   R5,A
	ORL   A,R4
	JNZ   G_STAT9
	JB    %LVIS_FL,G_STAT1
	%LDR45i (0)
	SJMP  G_STAT9
G_STAT1:%LDR45i (1)
G_STAT9:RET

ERRCLR_U:
	MOV   DPTR,#ADS_WDG
	MOVX  A,@DPTR
	JNZ   ERRCLR_U1
	CALL  ADS_INI		; Revitalizace prevodniku
ERRCLR_U1:
	CLR   A
	MOV   DPTR,#STATUS
	MOV   C,EA
	CLR   EA
	MOVX  @DPTR,A
	INC   DPTR
	MOV   EA,C
	MOVX  @DPTR,A
	RET

; Vypnuti a zapnuti

OFF_U:	CALL  T_LOFF
	RET
ON_U:   JB    %LVIS_FL,ON_U10
	MOV   DPTR,#CAL_PH
	MOV   A,#CAL_MPH
	MOVX  @DPTR,A
ON_U10:	CALL  T_LON
	RET

; Ukladani konfigurace

SAVECFG_U:%LDR23i(EEC_SER); Ulozeni parametru do EEPROM
	JMP   EEP_WRS

; Nulovani a prikazy pro prevodnik

ADSFL_U:CALL  xLDR23i
	JMP   T_ADSFL

; Prime cteni longint cisla

UO_INTRl:MOVX A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R0
	MOV   DPH,A
	MOV   C,EA
	CLR   EA
	CALL  xLDl
	MOV   EA,C
	MOV   DPTR,#TMP_U
	CALL  xSVl
	MOV   DPTR,#TMP_U
	%LDR45i(4)
	%VJMP (UV_WR)

; Cteni kanalu v pohyblive radove carce

UO_ABSf:MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R0
	MOV   DPH,A
	MOV   C,EA
	CLR   EA
	CALL  xLDl	; R4567 = absorbance float
	MOV   EA,C
        CALL  f2IEEE
	MOV   DPTR,#TMP_U
	CALL  xSVl
	MOV   DPTR,#TMP_U
	%LDR45i(4)
	%VJMP (UV_WR)

; Cteni kanalu v pevne radove carce

G_CHAi_U:MOV  DPTR,#ABS1
	SJMP  G_CHA01
G_CHBi_U:MOV  DPTR,#ABS2
G_CHA01:MOV   R0,#AKUM
	MOV   C,EA
	CLR   EA
	CALL  xiLDl
	MOV   EA,C
	MOV   R4,#000H
	MOV   R5,#040H
	MOV   R6,#01CH
	MOV   R7,#08EH	; 10000 = 00 40 1C 8E
	CALL  MULf	;  1000 = 00 00 7A 8A
	MOV   A,R7
	ADD   A,#-81H-15
	JNC   G_CHA40
	MOV   R4,#0FFH
	MOV   R5,#07FH
	MOV   A,R6
	MOV   R2,A
	SJMP  G_CHA80
G_CHA40:MOV   A,R6
	MOV   R2,A
	ORL   A,#80H
	MOV   R6,A
	MOV   A,R7
	JB    ACC.7,G_CHA50
	MOV   R4,#0
	MOV   R5,#0
	MOV   R6,#0
	MOV   R7,#0
	CLR   F0
	RET
G_CHA50:CPL   A
	ADD   A,#98H+1
	MOV   R7,#0
G_CHA70:CALL  SHRl
G_CHA80:MOV   A,R2
	JNB   ACC.7,G_CHA90
	CALL  NEGl
G_CHA90:CLR   F0
	RET

; Nastaveni radu filtru
S_FILT_U:MOV  A,R4
	MOV   DPTR,#ABS_FI
	ANL   A,#0FH
	MOV   C,EA
	CLR   EA
	MOVX  @DPTR,A
	SETB  FL_FICH
	MOV   EA,C
	RET

; Prenos teploty

G_TEMP:	%LDR23i(10)
	CALL  xMDPDP
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   EA,C
	JB    ACC.7,G_TEMP1
	CALL  DIVi	; R45=R45/R23
	MOV   A,R4
	ADDC  A,#0
	MOV   R4,A
	MOV   A,R5
	ADDC  A,#0
	MOV   R5,A
	ORL   A,#1
	RET
G_TEMP1:CALL  NEGi
	CALL  DIVi	; R45=R45/R23
	CLR   A
	SUBB  A,R4
	MOV   R4,A
	CLR   A
	SUBB  A,R5
	MOV   R5,A
	ORL   A,#1
	RET


S_TEMP:	%LDR23i(10)
	CALL  MULsi
	CALL  xMDPDP
	MOV   C,EA
	CLR   EA
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   EA,C
	JMP   TMC_ON

; Kody prikazu

%OID_ADES(AI_STATUS,STATUS,u2)
%OID_ADES(AI_ERRCLR,ERRCLR,e)
I_ADCFILT EQU   208
%OID_ADES(AI_ADCFILT,ADCFILT,u2)
I_ADCAl	  EQU   210
%OID_ADES(AI_ADCAl,ADCAl,s4)
I_ADCBl	  EQU   211
%OID_ADES(AI_ADCBl,ADCBl,s4)
I_CHA	  EQU   220
%OID_ADES(AI_CHA,CHA,f4)
I_CHB	  EQU   221
%OID_ADES(AI_CHB,CHB,f4)
I_CHAi	  EQU   230
%OID_ADES(AI_CHAi,CHAi,s2)
I_CHBi	  EQU   231
%OID_ADES(AI_CHBi,CHBi,s2)
I_CHA_GAIN EQU  244
I_CHB_GAIN EQU  245
I_OFF	  EQU   250
%OID_ADES(AI_ON,ON,e)
I_ON	  EQU   251
%OID_ADES(AI_OFF,OFF,e)
I_ZERO	  EQU   255
%OID_ADES(AI_ZERO,ZERO,e)
I_FIC	  EQU   256
%OID_ADES(AI_FIC,FIC,e)
I_TEMP1	  EQU   301
%OID_ADES(AI_TEMP1,TEMP1,s2/.1)
I_TEMP1RQ EQU   311
%OID_ADES(AI_TEMP1RQ,TEMP1RQ,s2/.1)
I_TEMP_OFF EQU  334
%OID_ADES(AI_TEMP_OFF,TEMP_OFF,e)
I_TEMP_ON EQU   335
%OID_ADES(AI_TEMP_ON,TEMP_ON,e)
I_TEMP_ST EQU   336
%OID_ADES(AI_TEMP_ST,TEMP_ST,s2)
I_TEMP1MC EQU   351
I_TEMP1OC EQU   361
I_TEMP1RD EQU   371
I_SAVECFG EQU   451


; Prijimane prikazy

OID_T	SET   $
	%W    (I_ERRCLR)
	%W    (OID_ISTD)
	%W    (AI_ERRCLR)
	%W    (ERRCLR_U)

%OID_NEW(I_SAVECFG ,0)
	%W    (SAVECFG_U)

%OID_NEW(I_OFF,AI_OFF)
	%W    (OFF_U)

%OID_NEW(I_ON,AI_ON)
	%W    (ON_U)

%OID_NEW(I_ZERO,AI_ZERO)
	%W    (ADSFL_U)
	DB    RAW_MSK,ZER_MSK

%OID_NEW(I_FIC,AI_FIC)
	%W    (T_ADSCN)

%OID_NEW(I_TEMP1,AI_TEMP1)
	%W    (UI_INT)
	%W    (0)
	%W    (S_TEMP)
	%W    (TMC1_RT)

%OID_NEW(I_TEMP1RQ,AI_TEMP1RQ)
	%W    (UI_INT)
	%W    (0)
	%W    (S_TEMP)
	%W    (TMC1_RT)

%OID_NEW(I_TEMP_OFF,AI_TEMP_OFF)
	%W    (TMC_OFF)

%OID_NEW(I_TEMP_ON,AI_TEMP_ON)
	%W    (TMC_ON)

%OID_NEW(I_TEMP1MC,0)
	%W    (UI_INT)
	%W    (TMC1_MC)
	%W    (0)

%OID_NEW(I_TEMP1OC,0)
	%W    (UI_INT)
	%W    (TMC1_OC)
	%W    (0)

%OID_NEW(I_ADCFILT,AI_ADCFILT)
	%W    (UI_INT)
	%W    (0)
	%W    (S_FILT_U)

%OID_NEW(I_CHA_GAIN,0)
	%W    (UI_INT)
	%W    (0)
	%W    (S_ADS_G_U)
	%W    (ADC_CTR1)

%OID_NEW(I_CHB_GAIN,0)
	%W    (UI_INT)
	%W    (0)
	%W    (S_ADS_G_U)
	%W    (ADC_CTR2)

OID_1IN SET   OID_T

; Vysilane hodnoty

OID_T	SET   0

%OID_NEW(I_STATUS,AI_STATUS)
	%W    (UO_INT)
	%W    (0)
	%W    (G_STATUS)

%OID_NEW(I_CHA_GAIN,0)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ADS_G_U)
	%W    (ADC_CTR1)

%OID_NEW(I_CHB_GAIN,0)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ADS_G_U)
	%W    (ADC_CTR2)

%OID_NEW(I_ADCAl,AI_ADCAl)
	%W    (UO_INTRl)
	%W    (ADS1)

%OID_NEW(I_ADCBl,AI_ADCBl)
	%W    (UO_INTRl)
	%W    (ADS2)

%OID_NEW(I_CHA,AI_CHA)
	%W    (UO_ABSf)
	%W    (ABS1)

%OID_NEW(I_CHB,AI_CHB)
	%W    (UO_ABSf)
	%W    (ABS2)

%OID_NEW(I_CHAi,AI_CHAi)
	%W    (UO_INT)
	%W    (0)
	%W    (G_CHAi_U)

%OID_NEW(I_CHBi,AI_CHBi)
	%W    (UO_INT)
	%W    (0)
	%W    (G_CHBi_U)

%OID_NEW(I_TEMP1,AI_TEMP1)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TEMP)
	%W    (TMC1_AT)

%OID_NEW(I_TEMP1RQ,AI_TEMP1RQ)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TEMP)
	%W    (TMC1_RT)

%OID_NEW(I_TEMP_ST,AI_TEMP_ST)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TMC_ST)

%OID_NEW(I_TEMP1MC,0)
	%W    (UO_INT)
	%W    (TMC1_MC)
	%W    (0)

%OID_NEW(I_TEMP1OC,0)
	%W    (UO_INT)
	%W    (TMC1_OC)
	%W    (0)

%OID_NEW(I_TEMP1RD,0)
	%W    (UO_INT)
	%W    (TMC1_RD)
	%W    (0)

OID_1OUT SET  OID_T

END