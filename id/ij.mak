#   Project file pro cerpadlo LCP4000
#         (C) Pisoft 1992

ii_iic.obj: ii_iic.asm
	a51 ii_iic.asm $(par)

ii_ai.obj : ii_ai.asm
	a51 ii_ai.asm  $(par)

ii_plan.obj : ii_plan.asm
	a51   ii_plan.asm  $(par)

ij.obj    : ij.asm ii_ai.h ii_msg.h ii_iic.h ii_plan.h
	a51 ij.asm     $(par)

ij.       : ij.obj ii_ai.obj ii_iic.obj ii_plan.obj
	l51 ij.obj,ii_ai.obj,ii_iic.obj,ii_plan.obj xdata(8000H) ramsize(100h) stack(STACK) ixref

ij.hex    : ij.
	ohs51 ij
	copy ij.hex ij.he1
	ihexcsum -o ij.hex -l 0x2000 -L 0x1fee -A 0x1fee -F bc ij.hex
