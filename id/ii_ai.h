;********************************************************************
;*                    ID 2050  - II_AI.ASM                          *
;*     Aritmetika v pevne radove carce - Include file               *
;*                  Stav ke dni 18.06.1994                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

EXTRN   CODE(ADDi,SUBi,NEGi,CMPi,MULi,SMULi,DIVi,DIVi1,DIVihf,MODi)
EXTRN   CODE(DIVi0,SHRi,SHR1i)
EXTRN   CODE(iLDR23i,iLDR45i,iSVR45i,cLDR23i,DECDPTR,ADDATDP)
EXTRN   CODE(MR45R67,O10E4i,rLDR23i,iLDR123,iSVR23i,iSVR123)
