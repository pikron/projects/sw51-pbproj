$NOMOD51
$INCLUDE(REG552.H)
;********************************************************************
;*                    IDK      - II_KBD.ASM                         *
;*     Obsluha klavesnice pripojene vstupy k P4.0..3                *
;*                               a vystupy k P4.4..7 a P1.0..2      *
;*                  Stav ke dni 16.10.1994                          *
;*                      (C) Pisoft 1994                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

        EXTRN   DATA(KBDTIMR)

	PUBLIC  SCANKEY,KBDFL,TESTKEY
	PUBLIC  MAX_KEY,VIR_KEY1,VIR_KEY2,VIR_KEY3

TTY___C SEGMENT CODE

TTY___D SEGMENT DATA

RSEG TTY___D

KBDFL:  DS    1

RSEG TTY___C

; Test kodu klavesy v ACC
; =======================
; vraci: A ..  0 je stisknuta jinak neni
; rusi:  R0

TESTKEY:DEC   A
	MOV   R0,A
        ANL   A,#3
        ADD   A,#SCANKT-TESTK20
        MOVC  A,@A+PC
TESTK20:MOV   P4,A
        MOV   A,R0
	ANL   A,#NOT 3
        RR    A
        RR    A
        ADD   A,#TESTKT-TESTK30
        MOVC  A,@A+PC
TESTK30:JBC   ACC.7,TESTK40
        ANL   A,P1
        RET
TESTK40:RL    A
	ANL   A,P4
	RET

TESTKT: DB    001H
	DB    002H
	DB    004H
	DB    088H
	DB    090H
	DB    0A0H
	DB    0C0H

; Cteni klavesnice
; ================
; vraci: A .. kod stisknute klavesy nebo 0
; meni : A, R0, R1, R2, R3

TIM_REP EQU   4   ; Pocet taktu repeatu
TIM_PUS EQU   20  ; Cekani po stisku
TIM_OFF EQU   2   ; Delka uvolneni
MAX_KEY EQU   1CH ; Maximalni kod klavesy

SCANKEY:MOV   R0,#0
SCANK10:MOV   R3,#0
	MOV   R2,#0
SCANK21:MOV   A,R3
	ANL   A,#3
	ADD   A,#SCANKT-SCANK22
	MOVC  A,@A+PC
SCANK22:MOV   P4,A
	MOV   A,P4
	ANL   A,#11110000B
	SETB  C
        RRC   A
        MOV   R1,A
        MOV   A,P1
        ANL   A,#00000111B
        ORL   A,R1
SCANK23:INC   R3
        CLR   C
        RRC   A
	JC    SCANK30
        MOV   R1,A
        MOV   A,R3
        XCH   A,R2
        JZ    SCANK29
        MOV   R2,A
	ADD   A,#-MAX_KEY-1
        JC    SCANK27
	ADD   A,#SCANVT-SCANV1+MAX_KEY+1
        MOVC  A,@A+PC
SCANV1: JZ    SCANK26
        ADD   A,R3
        MOV   R2,A
        SJMP  SCANK29
SCANK26:MOV   A,R3
        ADD   A,#SCANVT-SCANV2
        MOVC  A,@A+PC
SCANV2: JZ    SCANK27
	ADD   A,R2
        MOV   R2,A
        SJMP  SCANK29
SCANK27:MOV   R2,#0FFH
SCANK29:MOV   A,R1
SCANK30:INC   R3
        INC   R3
        INC   R3
        CJNE  A,#1,SCANK23
        MOV   A,R3
        ADD   A,#1-(4*7)
        MOV   R3,A
        CJNE  A,#4,SCANK21

        MOV   A,KBDFL
        CJNE  A,#0FFH,SCANK35
        MOV   A,R2
        JZ    SCANK40
	SJMP  SCANK39

SCANK35:XRL   A,R2
        MOV   R3,#TIM_REP
        JZ    SCANK40
        MOV   R3,#TIM_PUS
        MOV   C,ACC.7
        CLR   ACC.7
        JZ    SCANK1E
        MOV   A,KBDTIMR
        JZ    SCANK41
        JC    SCANK_E
        ORL   KBDFL,#80H
SCANK39:MOV   R3,#TIM_OFF
        SJMP  SCANK2E

SCANK40:MOV   A,KBDTIMR
        JNZ   SCANK_E
SCANK41:MOV   A,R2
	JZ    SCANK43
	XCH   A,R0
	JZ    SCANK10
	XRL   A,R2
	JZ    SCANK42
	MOV   R2,#0
SCANK42:MOV   A,R2
	MOV   KBDTIMR,R3
SCANK43:MOV   KBDFL,A
SCANK_R:RET

SCANK1E:MOV   A,R2
	MOV   KBDFL,A
SCANK2E:MOV   KBDTIMR,R3
SCANK_E:CLR   A
        MOV   R2,A
        RET

SCANKT: DB    11111110B
        DB    11111101B
        DB    11111011B
        DB    11110111B

SCANVT: DB    0               ; 00H
        DB    0
        DB    0
        DB    0
	DB    MAX_KEY         ; 04H
	DB    0
	DB    0
	DB    0
	DB    0               ; 08H
	DB    0
	DB    0
	DB    0
	DB    0               ; 0CH
	DB    0
	DB    0
	DB    0
	DB    MAX_KEY*2       ; 10H
	DB    0
	DB    0
	DB    0
	DB    0               ; 14H
	DB    0
	DB    0
	DB    0
	DB    0               ; 18H
	DB    0
	DB    0
	DB    MAX_KEY*3
	DB    0               ; 1CH
	DB    0
	DB    0

VIR_KEY1 EQU  MAX_KEY
VIR_KEY2 EQU  MAX_KEY*2
VIR_KEY3 EQU  MAX_KEY*3

END
