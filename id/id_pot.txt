strnad@linetcompact.cz

Zpracovani potenciometru ve spodnim procesoru

Promenne v DATA
FLOW:   DS    2    ; Prutok v 0.1 ml/hod
CAP_MAX:DS    2    ; Maximalni hodnota z kapacitniho cidal
CAP_DIV:DS    2    ; Hodnota z kapacitniho cidla na [1024*256 irc]
CAP_ACT:DS    2    ; Aktualni vystup z kapacitniho cidla
IRC_ACT:DS    2    ; Aktualni pozice z IRC cidla druheho procesoru

Promenne v IDATA
CAP_LST:DS    2    ; Minula poloha pro kalibraci a kontrolu behu
		   ; to je poloha pred spustenim pohybu
VOL_IN: DS    2    ; Aktualni objem do konce strikacky

Parametry strikacek
O_SPRV  V 	; Objem strikacky v 0.1 ml
O_SPRL  L	; Delka strikacky v 256 irc
O_SPRE		; Konec davkovani v 256 irc
O_SPRT		; 2 znaky nazvu stikacky


Funkce

CAP2IRC:
========
; Prevadi kapacitni cidlo na [256*IRC] podle vztahu
;   R45=(CAP_MAX-CAP_ACT)/CAP_DIV*1024
;
; volano z PPSW1k  - posilani CAP2IRC do horniho procesoru
;          PPSW5   - kontrola konce davky, vypocet zbyvajiciho
;	             objemu
;          PPSW662 - update CAP_LST v dobe, kdy se necerpa
;          PPSWB   - posilani CAP2IRC do horniho procesoru
;	   CHK_CAP - krizova kontrola davkovani pres cas

CHK_CAP:
========
; Kontrolni vypocet casu z polohy
;   R45=CAP_LST-(aktualni CAP2IRC)
;   if (R45>=0) {
;     R4567 = V*R45	      ; V*CAP
;     R45R1 = R4567
;     R45R1 = R45R1/L	      ; vydavkovany objem = V*CAP/L
;     R45 = R45R1*60/FLOW     ; cas v min = V*CAP*60/L/FLOW
;   } else { R45 = 0 }
;   R23 = TIME
;   R45 = R45-R23
;   R45 = abs(R45)
;   if ((FLOW>=128)||(FLOW==0)) R1 = ~3;
;   else {
;     A=FLOW
;     while(!(FLOW&0x80)) {
;       FLOW<<=1; R1<<=1;
;     }
;   }
;   if (R45&R1) {
;      R45/R23*(1<<5+15-1)
;      pokud je overflow nebo vysledek
;      vetsi roven 1<<15 pak je chyba
;        SET_ERR(ERFLTS)
;   }
;
; volano z PPSWA15

PPSW1:
======
; odeslat aktualni CAP2IRC v PPSW1.IIC_OUT+5,IIC_OUT+6


PPSW5:
======
; Kontrola konce strikacky a zbyvajiciho objemu
;   R45=(aktualni CAP2IRC)-G_SPR(O_SPRE)
;   R45=R45<0?0:R45
; vydavkovany objem = V*CAP/L
; ulozit do VOL_IN
; odeslat v PPSW5.IIC_OUT+1,IIC_OUT+2

I_TIM18:
========
kontrola cinnosti kapacitniho cidla nebo potenciometru
jednou za preteceni T2.
neni-li cidlo mereno (FL_CAPA neni nulovan) SET_ERR(ERCAPNP)
pro potenciometr se hlida tez plany rozsah (0x80,0x7F80)
