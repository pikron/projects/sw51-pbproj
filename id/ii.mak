#   Project file pro cerpadlo LCP4000
#         (C) Pisoft 1992

ii_iic.obj: ii_iic.asm
	a51 ii_iic.asm $(par)

ii_ai.obj : ii_ai.asm
	a51 ii_ai.asm  $(par)

ii.obj    : ii.asm ii_tty.h ii_ai.h ii_msg.h ii_iic.h
        a51 ii.asm $(par)

ii_kbd.obj: ii_kbd.asm
	a51 ii_kbd.asm $(par)

ii.       : ii.obj ii_ai.obj ii_kbd.obj ii_iic.obj
	l51 ii.obj,ii_ai.obj,ii_kbd.obj,ii_iic.obj xdata(8000H) ramsize(100h) stack(STACK) ixref

ii.hex    : ii.
	ohs51 ii
	copy ii.hex ii.he1
	ihexcsum -o ii.hex -l 0x2000 -L 0x1ffe -A 0x1ffe -F bc ii.hex

