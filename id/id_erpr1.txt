@CT 1
@LM 1
@RM 65
@PL 60
@TB -----T-----T-----T-----T-----T-----T-----T-----T-----T-----T-----T-----T-----T-----T-----T-----T-----T-----T-----T-----T-----T-----T
@MT 2
@MB 2
@PO 8
@PN 1
@OP 
@LH 6

               Opat�en� pro zaji�t�n� bezpe�nosti
                  injek�n�ho d�vkova�e ID 20/50



     �kol zaji�t�n� bezpe�nosti

     �kolem syst�mu  zaji�t�n� bezpe�nosti v  injek�n�m d�vkova�i�
je zaru�en� zastavit d�vkov�n�,  spustit v�stra�n� zvukov� sign�l�
a rozsv�tit indikaci  chyby. V p��pad�,  �e je to  mo�n�, z�rove�
rozsv�tit  indikaci  d�vodu  vzniku  chybov�ho  stavu. Z�rove� je�
nutn� v p��pad� chyby znemo�nit  spu�t�n� d�vkov�n� a v n�kter�ch�
p��padech i zdroje nap�t�.

     Rozd�len� oblast� kter� mohou ovlivnit bezpe�nost

Chyby obsluhy                                        UZV

Chyby mechanick� ��sti                               MECH

Chyby funkce sp�na�e nap�t�                          SPIN

Chyby funkce zdroje nap�t�                           NAP

Chyby v�konov� ��sti sp�nac� motoru                  VYK

Chyby elektroniky motoru                             MOT

Chyby sn�m�n� polohy a vyd�vkovan�ho objemu          POZ

Chyby sn�m�n� okluzn�ho tlaku                        TLAK

Chyby v obvodech sn�m�n� logick�ch p��znak�          LOG

Chyby v obvodech vyhodnocen� chyby                   CHYB

Chyby v indika�n�ch  prvc�ch                         IND

Chyby v prvc�ch vstupu �daj�                         KLV

Chyby v technick�m vybaven� kontrol�r�               HWE

Chyby v programov�m vybaven� kontrol�r�              SVE


     Zp�soby identifikace chyby

p�ed a p�i zap�n�n� za��zen�                         ON

v pr�b�hu inicializace za��zen� p�i zapnut�          INIT

v pr�b�hu zad�v�n� �daj�                             INP

p�i spu�t�n� d�vkov�n�                               START

v pr�b�hu d�vkov�n�                                  RUN

v pr�b�hu ve�ker�ho �asu po inicializaci             EVR

v servisn�m re�imu                                   SERV


     Jednotliv� chyby


Chyby obsluhy

  �patn� zalo�en� st��ka�ka

  Rozepnut� spojka

  Chybn� zapojen� nebo ucpan� hadi�ky

  Chybn� zadan� hodnoty


Chyby mechanick� ��sti

Chyby funkce sp�na�e nap�t�
     - o�et�eno pouze u�ivatelem
     - identifikace - p��stroj nelze zapnout nebo vypnout

Chyby funkce zdroje nap�t�
  stabiliz�tor 5V
     -  identifikace  podp�t�  procesory   jsou  ve  stavu  reset�
           tranzistory  pro  sp�n�n�  motoru  jsou  vypnuty  a je�
           nastaven  sign�l  ERROR.  p�ep�t�  p�es tranzistorovou�
           logiku vypne motor a nastav� ERROR. mal� odchylky jsou�
           identifikov�ny procesorem
     -  zaji�t�n� vypnut� motoru vyhl��en� chyby
  baterie
     -  identifikace  nap�t�  a  kapacity  procesorem  p�i velk�m�
           poklesu nap�t� hardwarov� vypnut�.
     - zaji�t�n�  upozorn�n�, hl��en� chyby  a zastaven� �erp�n�,�
           p�i velk�m poklesu vypnut�.
  vn�j� zdroj 12V a v�stup s transform�toru
     -  identifikace m��en� nap�t� procesorem
     -  zaji�t�n�  p�i  p�ep�lov�n�  ochrana  diodou. p�i poklesu�
            nap�t� p�echod na bateriov� re�im
  transform�tor
     -

Chyby v�konov� ��sti sp�n�n� motoru
  sp�nac� tranzistory
     - o�et�eno jejich zdvojen�m
     - identifikace  INIT,START,RUN,EVR   -    postupn�  st��dav��
           sepnut�   horn�ho   a   posl�ze   doln�ho  tranzistoru�
           s vyhodnocen�m nap�t� na t�ech sn�mac�ch bodech - ��st�
           kontroly sice  ovliv�uje koncov� sp�na�,  ale kontrola�
           se prov�d� i v START, kdy ji� rozepnut� koncov� sp�na��
           indikuje   jinou   chybu.  V dob�  d�vkov�n�      jsou�
           kontrolov�na   p��slu�n�   nap�t�.Dal�   kontrola  je�
           prov�d�na  nep��mo sledov�n�m  polohy motoru  a proudu�
           proch�zej�c�ho motorem.
     -  zaji�t�n�,t�sn�  po   zapnut�  ��zen�  sp�na��  blokov�no�
           sign�lem   RESET,  hodnotami   v�stupn�ch  port�  obou�
           procesor�, kter� jsou  pro reset p�esn� specifikovan�,�
           chybou zp�sobenou n�zk�m  nap�t�m p��padn� p�ep�t�m. Z�
           d�vodu  zv��en�  bezpe�nosti   se  v  p��pad�  p�ep�t��
           a n�zk�ho nap�jen� a  i n�kter�ch dal�ch chyb vyu��v��
           tranzistorov�  logika,  zabezpe�uj�c�  vypnut�  a�  do�
           hodnoty  nestabilizovan�ho  nap�t�.  V  dal�m �ase je�
           mo�n� zajistit vypnut� sp�na��  nez�visle p��mo z obou�
           procesor� a nep��mo p�es centr�ln� chybov� sign�l.
  brzd�c� tranzistor
     - neo�et�eno
     - identifikace INIT,START - pouze ��ste�n� m��en�m nap�t�
     - zaji�t�n�  - tranzistor nen� nezbytn�  pro �lohu zaji�t�n��
           bezpe�nosti
  logick� obvody  sp�na�e
     -  o�et�eno  zdvojen�m  logiky  HC  i  logikou z bipol�rn�mi�
           tranzistory
     - identifikace spolu s chybami tranzistor�


Chyby elektroniky motoru
     - o�et�eno sledov�n�m polohy, proudu  a nap�t�
     - identifikace - neodpov�daj�c� poloha z �idel
     - zaji�t�n� - vypnut� obou sp�nac�ch tranzistor�

Chyby sn�m�n� polohy a vyd�vkovan�ho objemu
     - o�et�eno  - vyhodnocov�n�m dv�ma  nez�visl�mi syst�my, kde�
           ka�d� je zalo�en� na fyzik�ln� jin�m principu.
     - zaji�t�n�  - oba procesory maj�  nez�visl� vstupy s r�zn�m�
           hardware,  �idla  se  vyhodnocuj�  pod p�eru�en�m, p�i�
           chyb� m� mo�nost ka�d� procesor vypnout motor. Z�rove�
           i p�i  chyb� jednoho z  tranzistor� sp�na�e motoru  je�
           zpr�va p�ed�na  na druh� procesor,  kter� vyp�n� druh�
           tranzistor
  chyba inkrement�ln�ho �idla
     - o�et�eno logikou a procesorem
     - identifikace  - nep�ich�z� ��dn�  impulsy p�i hodnot�  PWM�
           pro motor nad ur�itou hodnotou - rozpozn�no softwarem.
           p�i naru�en� posloupnosti  puls� v�t�m ne� vyfiltruje�
           filtra�n� �len v  obvodu f�zov�ho diskrimin�toru dojde�
           k  hl��en� chyby  procesoru,  kter�  chyby ��t�  a p�i�
           p�ekro�en� hranice chybovosti dojde k vypnut� (hranice�
           je velice n�zk� a bude p�esn� specifikov�na d�le).
     - zaji�t�n� - vypnut� d�vkov�n� procesory
  chyba obvodu f�zov�ho diskrimin�toru a ��ta�� polohy
     - o�et�eno testem p�i zapnut�
     - identifikace - po zapnut�  je obvod p�epnut do testovac�ho�
           re�imu  a  jsou  na  n�j  p�iv�d�ny  testovac� sign�ly�
           a  jsou porovn�ny  v�stupy ��ta��  polohy se spr�vn�mi�
           hodnotami
     - zaji�t�n� - zastaven� �erp�n�
  chyba kapacitn�ho �idla
     - o�et�eno softwarov�
     - identifikace - z hodnoty  mimo rozsah kalibrace nebo pevn�
           absolutn� povolen� rozsah definovan� v kontroleru
     - zaji�t�n� - zastaven� �erp�n�

Chyby sn�m�n� okluzn�ho tlaku
  chyba tenzometrick�ho sn�ma�e pouze pouze pro roz��en� typ
     - o�et�eno softwarov�
     - identifikace    hodnota   mimo    rozsah   kalibrace  nebo�
           absolutn�ho   limitu,  ��ste�n�   kontrola  porovn�n�m�
           s proudem motorem
     - zaji�t�n� - zastaven� �erp�n�
!  chyba m��en� proudu
!     - o�et�eno ��ste�n�
!     -  identifikace -  porovn�n� proudu  s nap�t�m  na motoru  a�
!           hodnotou PWM sp�n�n� motoru
!     - zaji�t�n� - zastaven� �erp�n�

Chyby v obvodech sn�m�n� logick�ch p��znak�
  sn�m�n� typu st��ka�ky
     - o�et�eno ��ste�n�
     - identifikace - p�i v�padku obou optick�ch z�vor d�v� �idlo�
           kombinaci,  kter� nedovol�  �erp�n�.
!           Z�rove�  m��e b�t�
!           m��ena  i analogov�  hodnota sign�lu.
           U�ivatel by m�l sledovat p��slu�n� indikace
     - zaji�t�n� - nedovolen� spu�t�n� nebo zastav� �erp�n�
  chyba sn�m�n� sepnut� spojky
     - neo�et�eno
     - identifikace - vyhodnocen�m �idel polohy
     - zaji�t�n� - zastaven� �erp�n�
  chyba koncov�ho sp�na�e
     - tato chyba ovliv�uje pouze kalibraci a tu prov�d� servisn��
           technik, kter� chybu tak� pozn�

Chyby v obvodech vyhodnocen� chyby
     - o�et�eno  n�sobnost� funkc� a i  softwarov� a r�zn�mi typy�
           logiky - tranzistory a HC
  chyba v reproduktoru
!     -  identifikace  -  m��en�  proudu  reproduktorem po zapnut��
!           p��stroje a t�� sluchov� kontrola
  chyba led diody ERROR
     - o�et�eno - nez�visl�m sp�n�n�m obou polovin diody
     - identifikov�no - po zapnut� m��en�m proudu diodou
  chyba v sou�tov�ch �lenech
     - o�et�eno  odd�len�m - zkrat  na sign�lu nezp�sob�  selh�n��
           dal� funkce vstupu
  chyba ve vyp�n�n� �erp�n� od sign�lu chyby
     - o�et�eno dal�mi nez�visl�mi sign�ly od obou kontrol�r�
  chyba rel�

Chyby v indika�n�ch a zobrazovac�ch prvc�ch
  chyby  v LED indikac�ch
     -  identifikace  -  p�i  zapnut�  m��en� proudu jednotliv�mi�
           diodami  a  i   cel�mi  skupinami.  optick�  kontrola.�
           kontroln� �ten� jednoho sign�lu z ka�d�ho registru
  chyby  v LCD displej�ch
!     -  identifikace  -  p�i   zapnut�  m��en�  proudu  skupinami�
!           segment�  p�i odpojen�  BACK PLANE. kontrola frekvence�
!           a funkce BACK PLANE. optick� kontrola.

Chyby v prvc�ch vstupu �daj�
  Chyby kl�vesnice
     - o�et�eno dv� mo�nosti vypnut� kl�vesa STOP a ZAP/VYP.
     - identifikace m��en� zkrat� po zapnut�

Chyby v technick�m vybaven� kontrol�r�
     - o�et�eno zdvojen�m
  Chyby funkce pam�t� RAM a EPROM
     -  identifikace  -  kontrola  po  zapnut� kontroln�ho sou�tu�
           (E)PROM, kontrola funkce v�ech bit� RAM a adresace
     - zaji�t�n� - znemo�n�n� provozu
  Chyby v pam�ti EEPROM
     -  identifikace pomoc� kontroln�ho  sou�tu  pro ka�d�ch �est�
           byte  v  dob�  jejich  �ten�  (do  sou�tu je zapo�tena�
           i adresa  bloku, zaji�uje spolu s  ulo�en�m na adresy�
           s n�sobky sedmi i kontrolu adresov�n�)
     - zaji�t�n� ignorov�n�m dat a vyhl��en�m chyby
  Chyby speci�ln�ch registr�
     - o�et�eny  jen ty, u kter�ch  to m� v�znam a  lze zaji�t�n��
           prov�st (SP,atd)
  Chyby watch dog timer�
     - o�et�eny zdvojen�m, v ka�d�m procesoru je jeden
     - identifikov�ny p�i zapnut�  p��stroje - procesory st��dav؍
           zp�sob� simulovan�  v�padek software a  druh� procesor�
           v�dy provede m��en� �asu v�padku
  Chyby v komunikaci mezi procesory
     -  jsou o�et�eny op�tn�m vysl�n�m chybn�ho bloku
     -  identifikov�ny  pomoc�  kontroln�ch  sou�t�  a po�adov�ch�
           ��sel zpr�v
  Chyby v komunikaci s dal�mi IIC obvody mimo EEPROM
     -  o�et�en�  opakov�n�  vys�l�n�   �daj�  pro  LCD  displeje�
           periodicky - to zp�sob�, �e chybn� �daj se p�ep��e
     -  kontrola ��d�c�ch obvod� po zapnut�
!     -  identifikace  po  zapnut�  p�i  m��en�  proudu  skupinami�
!           segment�

Chyby v programov�m vybaven� kontrol�r�
  Chyba um�st�n� SP
     - identifikace  - v za��tku �asov�ho  p�eru�en� a v nejni�硍
           �rovni  rutin pro  ��zen� d�vkov�n�,  kde je  SP rovno�
           jeho po��te�n� hodnot�
  Chyba re�imu
     - re�im je podm�n�n n�kolika  �daji a z�rove� je kontrolov�n�
           navz�jem  mezi  procesory  -  povolen�  p�echody  jsou�
           p�esn� definovan�
  Chyba  v�po�tu
     -  r�zn�  metody  v�po�tu  v  obou  procesorech  a�
           d�kladn� kontrola p�i n�vrhu
  Chyba v komunika�n�m protokolu a jeho �asov�n�
     -  kontroln� sou�et,  po�ad�  a  max interval  mezi spr�vami�
           kontrolovan� pod p�eru�en�m
  Chyba v b�hu programu
     -  identifikace -  softwarov� kontrola  pr�chodu kontroln�mi�
           body poka�d�  za ur�it� interval  a nad�azen� kontrola�
           hardwarov�m watchdogem
     - zaji�t�n� vyhl��en�m chyby a zastaven�m �erp�n�
  Chyby perifer�� na �ipu mikrokontrol�ru
     -  identifikace testem  s referen�n�mi  hodnotami a kontrola�
           ��zen� funkce sn�ma�i
  Chyby v rutin� regulace
     -  kontrola druh�m  procesorem  -  jeden po��t�  v relativn��
           poloze, druh� v absolutn�, rozd�ln� je i software

  Vynech�n� �asov�ho p�eru�en�
     - zaji�t�n�  hardwarov�m watchdogem a  komparac� ��ta�� �asu�
           obou kontrol�r�, zp�sob� odstaven� za��zen�


     Popis algoritmu ��zen� a kontroly

  Pro  dal�  popis  bude  procesor  ��zen�  polohy  a kl�vesnice�
ozna�ov�n PRK a procesor kontroly a komunikace PKK.

  Komunikace  mezi procesory  prob�h� po  zb�rnici IIC  ve pevn�m�
po�ad� zpr�v,  kter� jsou ��slov�ny  po�adov�mi ��sly modulo  64.�
Ve�ker� informace je mezi procesory  vym�n�na za 16 zpr�v, znich��
ka�d� p�en�� vyhrazen� informace.  Typ zpr�vy je ur�en po�adov�m�
��slem modulo 16. Sud� zpr�vy jsou  p�en��eny z PRK do PKK, lich��
z PKK  do  PRK.  Ka�d�   zpr�va  je  zaji�t�na  kontroln�m  bytem�
(postupn� xor a inkrement  pro v�echny p�en��en� byte). Maxim�ln��
doba mezi p�ijet�m  dvou zpr�v od druh�ho procesoru  je hl�d�na v�
�asov�m  p�eru�en� (max  p��pustn� interval  je 30/450  = 0.07s).�
Informace o zastaven� d�vkov�n� je p�ed�v�na v ka�d� druh� zpr�v؍
do PKK,  to je jednou za  �ty�i zpr�vy, z toho  vypl�v� maxim�ln��
doba reakce  druh�ho procesoru do 0.15s.  P�ed�n� v�ech informac��
16 zpr�vami trv� maxim�ln�  0.6s. Komunikace v�ak prob�h� t�ikr�t�
rychleji  ne� je  tento  hrani�n�  stav. Pomalej�  komunikace je�
identifikov�na jako chyba.

  Ka�d� z  procesor� m� vlastn� �asov�  p�eru�en� s frekvenci 450�
Hz  a  vlastn�  ��t�n�  �asu.  �asy  jsou  vz�jem� kontrolov�ny a�
synchronizov�ny. V�t�  odchylka ne� 1%  je pova�ov�na za  chybu.�
�sp��n�   ukon�en�   v�ech   �innost�   �asov�ho   p�eru�en�   je�
kontrolov�na  hardwarov�m  watchdogem  s  intervalem 0.07s, kter�
jist�  v�padek ve�ker�ho  software. Toto  p�eru�en� d�le  prov�d��
softwarov�  kontrolu ostatn�ch  ��st� software  a komunikace mezi�
procesory.  P�i sv�m  spu�t�n� tak�  kontroluje stav programov�ho�
z�sobn�ku.  V  procesoru  PRK  se  zde  tak�  po spu�t�n� �erp�n��
prov�d�  tak�   regulace  polohy  a  kontrola   max  odchylky  od�
po�adovan� polohy  a hl�dan� max energie  (st��dy PWM), p�i kter��
se je�t� nemus� motor to�it. V obou procesorech je s frekvenc� 25�
Hz  spou�t�n  cyklus  m��en�  p�evodn�k�.  �daje  o  nap�t�ch  na�
tranzistorech a nap�jen� OZ  jsou vyhodnocov�ny okam�it�, ostatn��
jsou  vyhodnocov�ny   v  cyklu  komunikace,   kter�  je  zaji�t�n�
softwarov�m watchdogem.


  Pomoc�  kl�vesnice  a  displeje  se  zad�v� po�adovan� rychlost�
d�vkov�n� do procesoru PRK, tato hodnota je p�i stavu STOP trvale�
p�episov�na do procesoru PKK. Po stisknut� tla��tka START prob�h��
inicializace (stav INI START). V  tomto stavu je p�ed�na rychlost�
do PKK a z �daje kapacitn�ho  �idla a typu st��ka�ky je vypo�tena�
vzd�lenost  do  konce  st��ka�ky.   Z�rove�  v  PRK  je  vypo�ten�
p��rustek polohy  za 1/450 s pro  vlastn� ��zen�, jsou vynulov�ny�
��ta�e  offset� a  registry rozd�lu  polohy a  doch�z� ke sp�n�n��
kladn�ho  p�lu  zdroje  k  motoru  (kontroluje  se  nevodiv� stav�
tranzistoru u  z�porn�ho p�lu). Vypo��t�vaj�  se limity objemu  a�
�asu   a  doch�z�   k  synchronizaci   procesor�.  P�i   jak�koli�
nesrovnalosti  se  p�ech�z�  do  stavu  ALARM.
  Po usp��n�m cyklu komunikace a v�ech inicializac�ch se p�ech�z��
do  stavu  START.  V  tomto  stavu  nen� p��pustn� jak�koli zm�na�
parametr� �erp�n�  nebo typu st��ka�ky (vede  na chybu). Procesor�
PRK po��t� po�adovanou polohu  pod p�eru�en�m a prov�d� regulaci.�
Kontroluje chyby  IRC, max rozd�l skute�n�  a po�adovan� polohy a�
pou�itou energii. Z�rove� kontroluje  skute�nou hodnotu IRC �idla�
s hodnotou z kapacitn�ho �idla z procesoru PKK. PKK po��t� naopak�
z absolutn�  polohy  kapacitn�ho  �idla,po��te�n�  polohy  a typu�
st��ka�ky  �as  pot�ebn�  k  vyd�vkov�n�  dan�ho  objemu  zadanou�
rychlost�  a tento  �as porovn�v�  se skute�n�m  �asem (p�i chyb؍
vet�  ne� 4%  vyhl�s� chybu).  Tento postup  zaji�uje i chybu v�
rutin�ch delen�, n�soben� a  ostan�ch operac�ch, proto�e n�soben��
a d�len� je  p�evr�ceno a po��t� se  v zcela odli�n�ch jednotk�ch�
(min,ml <> IRC,1s/450). D�vkov�n� skon�� stisknut�m tla��tka STOP�
nebo  p�echodem  do  chybov�ho  stavu  (i  konec d�vky je chybov�
stav). V obou ��padech je�t�  dojde po zastaven� �erp�n� k v�m�n؍
dat mezi procesory a p�ipo�ten� vyd�vkovan�ho objemu.
  Chyba  je  rozkop�rov�na  do  obou  procesor�  a  blokuje dal硍
�innost.  Zru�ena m��e  b�t  pouze  stiskem klavesy  ALARM, kter�
zp�sob�  p�ed�n�  t�to  informace  mezi  procesory  a po n�sledn��
kontrole i smaz�n� registr� chybov�ho k�du v obou procesorech.

     Popis kontrol po zapnut�

  Po  resetu je  v  obou  procesorech vypo��t�n  kontroln� sou�et�
EPROM.  Prob�hne kontrola  p�ep�n�n� banek  registr� a  funk�nost�
instrukc�  n�soben�  a  d�len�  kontroln�m  v�po�tem  se  zn�m�mi�
v�sledky,  t�m  je  z�rove�   ��ste�n�  ov��eno  adresov�n�  p�es�
registry R0, R1.  Pak prob�hne kontrola RAM z�pisem  n�sobk� 7 do�
cel�  RAM  (v�hoda  -  n�sobky  7  mod  256  daj�  nulu a� po 256�
z�pisech) p�es  ukazatel SP, d�le prob�hne  komparace RAM a z�pis�
inverzn�ch hodnot a dal� komparace.
  V procesoru PRK dojde  ke kontrole funkce p�evodn�ku p�iveden�m�
nap�t� Uref/2  a hodnot analogov�ch sign�l�.  Provede se kontrola�
proudu  diodami LED  a porovn�   se je  jich osazan�  s kontrolou�
budi��  LCD  displej�.  V  sou�asn�  verzi  se zat�m nekontroluj��
kapacity vlastn�ch LCD displej�.
  Procesor PKK �ek� na zpr�vu z PRK a poku� nep�ijde dojde k jeho�
zablokov�n�. PRK  se sna�� ve  stavu SYNC 1  p�edat PKK informaci�
o resetu  a o  servisn�m re�imu.  PKK p�ejde  do stavu  SYNC 2  a�
ozn�m�  to PRK.  Dojde k  testov�n� watchdogu  PRK, kter� mus� do�
ur�it� doby  zp�sobit reset, jinak je  vyhl��ena chyba. PRK znovu�
projde �vodn� sekvenci, ale v SYNC 1 dostane informaci od PKK, �e�
se  nach�z� v  WD.D 1,  proto  p�ejde  do WD.D  2 a  procesor PKK�
provede kontrolu  watchdogu a druh� reset.  P�ijme v�ak informaci�
od PRK, �e se nach�z� ve stavu WD.D  2 a p�ejde do stavu STOP 2 a�
ozn�m� v�sledek sv�ch test� PRK. Tato sekvence zaji�uje kontrolu�
watchdog� a v p��pad� n�hodn�ho  v�padku jednoho procesoru vede k�
chyb�  p�i  startu.  �sp��n�  start  nastane  pouze p�i sou�asn�m�
resetu obou procesor� a �sp�chu jejich kontrol.
  P�i  p�echodu PRK  z WD.D  2  do  STOP 1  dojde k  testu obvodu�
f�zov�ho   diskrimin�toru,  adresov�n�   registr�  LED,  �innosti�
�asov�ho  p�eru�en�, software  sn�m�n� polohy  a funkce  hardware�
��ta�� polohy.  To se provede p�epnut�m  obvodu FD do testovac�ho�
stavu a  vysl�n�m impulz� pro ��t�n�  na jednu stranu, komparac�,�
��t�n�m  na  druhou  stranu   a  dal�  komparac� se  softwarov�m�
registrem okam�it� polohy IRC. Pak  se op�t FD p�ipoj� ke vstup�m�
z �idla.
  Procesor PKK kontroluje odpojen� kladn�ho p�lu od motoru.
