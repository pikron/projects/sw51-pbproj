;********************************************************************
;*                    LCP 4000 - LP_TTY.ASM                         *
;*     Obsluha LCP displaye a klavesnice                            *
;*                  Stav ke dni 18.06.1994                          *
;*                      (C) Pisoft 1994                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

          PUBLIC LCDINST,LCDNBUS,LCDWCOM,LCDWCO1,LCDWR,LCDWR1
          PUBLIC PRINT,PRINTH,xPRINT,cPRINT

	  PUBLIC SCANKEY

          PUBLIC LEDWR,LED_FLG

%DEFINE   (IN_TTY)(1)

TTY___B SEGMENT DATA BITADDRESSABLE
RSEG TTY___B
LED_FLG:DS    1
$INCLUDE(II_TTY.H)

$INCLUDE(IC_ADR.H)

TTY___C SEGMENT CODE

TTY___D SEGMENT DATA

RSEG TTY___D

KBDFL:  DS    1

RSEG TTY___C

LCDINST:MOV   DPTR,#LCD_INST
	MOV   A,#LCD_MOD
	CALL  LCDWAIT
	CALL  LCDWAIT
	MOV   A,#LCD_CLR
	CALL  LCDWAIT
	MOV   DPTR,#LCD_STAT
	MOVX  A,@DPTR
	JNZ   LCDINSR
	MOV   DPTR,#LCD_WDATA
	MOV   A,#055H
	CALL  LCDWAIT
	MOV   A,#LCD_HOM
	MOV   DPTR,#LCD_INST
	CALL  LCDWAIT
	MOV   DPTR,#LCD_RDATA
	MOVX  A,@DPTR
	XRL   A,#055H
	JNZ   LCDINSR
	MOV   A,#LCD_CLR
	CALL  LCDWCOM
	MOV   A,#LCD_NROL
	CALL  LCDWCOM
	MOV   A,#LCD_DON OR LCD_CON
	CALL  LCDWCOM
	MOV   A,#LCD_NSH
	CALL  LCDWCOM
	MOV   A,#LCD_CLR
	CALL  LCDWCOM
	MOV   KBDFL,#0
	CLR   A
LCDINSR:RET

LCDWAIT:MOVX  @DPTR,A
	MOV   R0,#10
LCDWAI1:DJNZ  R1,LCDWAI1
	DJNZ  R0,LCDWAI1
	RET

LCDNBUS:PUSH  DPH
	PUSH  DPL
LCDNBU1:MOV   DPTR,#LCD_STAT
	MOVX  A,@DPTR
	ANL   A,#LCD_BF
	JNZ   LCDNBU1
	MOVX  A,@DPTR
	POP   DPL
	POP   DPH
	RET

LCDWCOM:PUSH  ACC
        CALL  LCDNBUS
        POP   ACC
LCDWCO1:PUSH  DPH
	PUSH  DPL
        MOV   DPTR,#LCD_INST
        MOVX  @DPTR,A
        POP   DPL
        POP   DPH
	RET

LCDWR:  PUSH  ACC
	CALL  LCDNBUS
	POP   ACC
LCDWR1: CJNE  A,#C_LIN2,LCDWR2
	MOV   A,#LCD_HOM+040H
	SJMP  LCDWCO1
LCDWR2: JC    LCDWCO1
	PUSH  DPH
	PUSH  DPL
	MOV   DPTR,#LCD_WDATA
	MOVX  @DPTR,A
	POP   DPL
	POP   DPH
PRINTRE:RET

PRINTH: CALL  LCDNBUS
        MOV   A,#LCD_HOM
	CALL  LCDWCO1
PRINT:  MOV   DPL,R1
	MOV   DPH,R2
	CJNE  R3,#002H,cPRINT  ; NEDORESENE - NAVAZNOST NA C

xPRINT: CALL  LCDNBUS
        MOVX  A,@DPTR
        INC   DPTR
        JZ    PRINTRE
        CALL  LCDWR1
        SJMP  xPRINT

cPRINT: CALL  LCDNBUS
        CLR   A
        MOVC  A,@A+DPTR
        INC   DPTR
	JZ    PRINTRE
        CALL  LCDWR1
	SJMP  cPRINT

; Cteni klavesnice
; ================
; vraci: A .. kod stisknute klavesy nebo 0
; meni : A, DP, R2, R3

TIM_REP EQU   5  ; Pocet taktu repeatu
TIM_PUS EQU   20 ; Cekani po stisku
TIM_OFF EQU   3  ; Delka uvolneni

SCANKEY:MOV   R2,KBDTIMR
        MOV   DPTR,#KBD
        MOV   A,KBDFL
	JZ    SCANKE1
        MOVX  A,@DPTR
	CPL   A
	CJNE  R2,#0,SCANKR1
        XRL   A,KBDFL
        ANL   A,#03FH
        MOV   KBDTIMR,#TIM_REP
        JZ    SCANKE2
SCANKE1:MOV   KBDTIMR,#TIM_PUS ; rychlost repeatu
SCANKE2:MOV   R2,#006H
        MOV   R3,#NOT 040H
SCANKE3:MOV   A,R3
        RR    A
        MOV   R3,A
        MOVX  @DPTR,A
        MOVX  A,@DPTR
	CPL   A
        ANL   A,#03FH
	JNZ   SCANKE4
        DJNZ  R2,SCANKE3
SCANKE4:MOV   KBDFL,A
        JZ    SCANKR3
        MOV   R3,#0FFH
SCANKE5:INC   R3
        RRC   A
        JNC   SCANKE5
        MOV   A,R3
        RL    A
        ADD   A,R3
        RL    A
        ADD   A,R2
        MOV   R2,A
	MOV   DPTR,#LED       ; Pipnuti
        MOV   A,LED_FLG
	XRL   A,#080H
        MOVX  @DPTR,A
        MOV   A,#040H
SCANKE6:DJNZ  ACC,SCANKE6
        MOV   A,LED_FLG
        MOVX  @DPTR,A
        MOV   A,R2
        RET

SCANKR1:ORL   A,#080H
        ANL   A,KBDFL
        MOV   R2,#TIM_OFF     ; doba uvolneni
        JZ    SCANKR2
        XRL   A,#080H
	JZ    SCANKRE
        JB    ACC.7,SCANKRE
	MOV   R2,#TIM_PUS     ; penalta na kartac
SCANKR2:XRL   KBDFL,#080H
        MOV   A,R2
SCANKR3:MOV   KBDTIMR,A
SCANKRE:CLR   A
        RET

; Nastaveni indikacnich LED podle A
; =================================

LEDWR:  MOV   A,LED_FLG
        MOV   DPTR,#LED
        MOVX  @DPTR,A
        RET

END
