;********************************************************************
;*                    LCP 4000 - LP_TTY_C.H                         *
;*     Include file se scankody pro KBD  a Headery                  *
;*                  Stav ke dni 24.03.1991                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************


; Ridici kody pro displej

LCD_BF   EQU   080H  ; busy flag - cteni z LCD_STAT

; Rizeni pres LCD_INST

LCD_HOM  EQU   080H  ; nastaveni cursoru
LCD_CLR  EQU   001H  ; smazani displeje
LCD_HOME EQU   002H  ; cursor na pocatek
LCD_MOD  EQU   038H  ; nastaveni modu dual line, 8 bitu
LCD_NROL EQU   010H  ; stabilni displej
LCD_DON  EQU   00CH  ; displej on
LCD_CON  EQU   00AH  ; cursor on
LCD_BON  EQU   009H  ; blink on
LCD_NSH  EQU   004H  ; pohyb

; Definice ridicich znaku

C_CLR    EQU   LCD_CLR
C_HOME   EQU   LCD_HOME
C_LIN2   EQU   00FH

;********************************************************************

; Scan kody klaves

K_0      EQU   01BH
K_1      EQU   015H
K_2      EQU   018H
K_3      EQU   016H
K_4      EQU   021H
K_5      EQU   024H
K_6      EQU   022H
K_7      EQU   00FH
K_8      EQU   012H
K_9      EQU   010H
K_DP     EQU   01EH
K_PM     EQU   00DH     ; +/- neni definovano
K_ENTER  EQU   01CH

K_PROG   EQU   011H
K_IMPL   EQU   00DH
K_LEFT   EQU   023H
K_RIGHT  EQU   01FH
K_UP     EQU   00EH
K_DOWN   EQU   020H
K_INS    EQU   017H
K_DEL    EQU   014H
K_MODE   EQU   013H
K_HELP   EQU   00BH
K_LIST   EQU   008H
K_CYCLE  EQU   007H
K_RUN    EQU   004H
K_END    EQU   003H

K_HOLD   EQU   006H
K_FLOW   EQU   00AH
K_START  EQU   001H
K_PURGE  EQU   002H
K_STOP   EQU   005H
K_A      EQU   01DH
K_B      EQU   01AH
K_C      EQU   019H
K_CONC   EQU   00CH
K_AUX    EQU   009H

K_H_A    EQU   01DH
K_H_B    EQU   01AH
K_H_C    EQU   019H
K_H_D    EQU   017H
K_H_E    EQU   014H
K_H_F    EQU   013H

%IF (%IN_TTY) THEN (

;********************************************************************

; Kody led diod

BEEP_FL BIT   LED_FLG.7
ALRM_FL BIT   LED_FLG.6
PROG_FL BIT   LED_FLG.5
START_FL BIT  LED_FLG.4
G_BF_FL BIT   LED_FLG.3
PURGE_FL BIT  LED_FLG.2
RUN_FL  BIT   LED_FLG.1
HOLD_FL BIT   LED_FLG.0

PUBLIC  BEEP_FL,ALRM_FL,PROG_FL,START_FL,G_BF_FL,PURGE_FL,RUN_FL
PUBLIC  HOLD_FL

EXTRN   DATA(KBDTIMR)         ; softvareovy timer napojeny na cas

)ELSE(

EXTRN    CODE(LCDINST,LCDNBUS,LCDWCOM,LCDWCO1,LCDWR,LCDWR1)
EXTRN    CODE(PRINT,PRINTH,xPRINT,cPRINT)

EXTRN    CODE(SCANKEY)

EXTRN    CODE(LEDWR)
EXTRN    DATA(LED_FLG)
EXTRN    BIT(BEEP_FL,ALRM_FL,PROG_FL,START_FL,G_BF_FL,PURGE_FL)
EXTRN    BIT(RUN_FL,HOLD_FL)


)FI
