$NOMOD51
;********************************************************************
;*                    ID 2050 - IJ.ASM                              *
;*     Hlavni modul software pro procesor kontroly a mereni polohy  *
;*                  Stav ke dni 04.11.2002                          *
;*                      (C) Pisoft 1994                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$INCLUDE(REG552.H)
$INCLUDE(II_AI.H)
$INCLUDE(II_MSG.H)
$INCLUDE(II_IIC.H)
$INCLUDE(II_PLAN.H)
PUBLIC	uL_SNST,uL_IDB,uL_IDE

%DEFINE  (DEBUG_FL)    (0)    ; Povoleni vlozeni debug rutin
%DEFINE  (IPC_SOFT_FL) (0)    ; Software pro IPC
%DEFINE  (POS_POT_FL)  (1)    ; Absolutni poloha z potenciometru
%DEFINE  (POT_FILT_FL) (1)    ; Plovouci filtr pro potenciometr
%DEFINE  (HALRS_FL)    (1)    ; S kontrolnim halovym snimacem otacek
%DEFINE  (CFG7HRS_FL)  (1)    ; Konfiguracni bit 7 zakazuje HRS

%IF (%IPC_SOFT_FL) THEN (
CMAX_FL   EQU   9999 	      ; Max nastavitelny prutok v 0.1 ml/h
SOFT_VER  EQU	94H           ; Kompatabilita verze pro IPC

)ELSE(
CMAX_FL   EQU   1500 	      ; Max nastavitelny prutok v 0.1 ml/h
SOFT_VER  EQU	14H           ; Kompatabilita verze pro IP
)FI

%DEFINE (WATCHDOG) (
	ORL   PCON,#10H
	MOV   T3,#0E0H
)

%*DEFINE (TOLED (WHAT)) (
	PUSH  ACC
	MOV   A,#%WHAT
	MOV   DPTR,#LED2
	MOVX  @DPTR,A
	POP   ACC
)

%*DEFINE (W (WO)) (
	DB   LOW (%WO),HIGH (%WO)
)

SPRRDY  BIT   P1.2    ; Je otevrena spojka
BATTST	BIT   P4.6    ; Nulovy spina test baterie
SPRTST	BIT   P4.4    ; Testovani zalozene strikacky
MOTOFF	BIT   P4.3    ; Vypnuti motoru
ELED	BIT   P4.2    ; Errorova led
BEEPB   BIT   P4.1    ; Pipani
SERV_SW BIT   P4.5    ; 0 .. servisni switch je sepnuty
P4_RES  EQU   11110001B
P4_MSK  EQU   01011110B

LED_CLK BIT   P3.5    ; Ovladani indikace strikacky
LED_DAT BIT   P3.4

LED0    EQU   0111111111111111B
LED1    EQU   1011111111111111B
LED2    EQU   1101111111111111B

;=============================================================

; Zpravy v OUT_MSG posilane na druhy procesor

OMS_CAP EQU   080H          ; Skupina zprav pro kalibraci CAP cidla
OMS_SCH EQU   090H          ; Zmenil se typ strikacky
OMS_CCH EQU   094H          ; Zmenila se konfigurace
OMS_CER EQU   0AAH          ; Bude se mazat chyba

IC____C SEGMENT CODE
IC____D SEGMENT DATA
IC____I SEGMENT IDATA
IC____B SEGMENT DATA BITADDRESSABLE
STACK   SEGMENT IDATA


RSEG    IC____C
%IF (%IPC_SOFT_FL) THEN (
uL_IDB: DB    '.mt IPC2050 v 1.4 .uP 51i .dy',0
uL_IDE:

)ELSE(
uL_IDB: DB    '.mt IP2050 v 1.4 .uP 51i .dy',0
uL_IDE:
)FI

RSEG	STACK
STACK_S EQU   2DH
	DS    STACK_S

USING   0

CSEG    AT    RESET
	JMP   START

CSEG    AT    TIMER0          ; Realny cas z casovace 0
	JMP   I_TIME

CSEG    AT    T2CAP0          ; Vzestupna hrana z meridla polohy
	JMP   I_CAP0

CSEG    AT    1FF0H
SER_NUM:%W    (7)
	%W    (0)

RSEG IC____B

TIME_CN:DS    1    ; Delici citac pro odvozeni 25 Hz
HW_FLG: DS    1
FL_EREE BIT   HW_FLG.7        ; Chyba EEPROM
FL_CAPA BIT   HW_FLG.6        ; Mazan pri cteni CAP cidla
FL_MOT  BIT   HW_FLG.5        ; Spusteni motoru
FL_DCIN BIT   HW_FLG.4        ; Je vnejsi napajeni
FL_TRER BIT   HW_FLG.3        ; Pro test chyby ERTRHI
FL_MOV  BIT   HW_FLG.2        ; Motor overload
FL_SETL BIT   HW_FLG.1        ; Nastavit limity podle pocitace
FL_NERR BIT   HW_FLG.0        ; Pretizeni motoru


SW_FLG: DS    1               ; Priznaky vyuzite softwarem
FL_BEWA BIT   SW_FLG.7        ; Varovani pipanim
FL_TIME BIT   SW_FLG.6        ; Nastaven casovy limit
FL_PRES BIT   SW_FLG.5        ; Tlakovy limit
FL_VOL  BIT   SW_FLG.4        ; Objemovy limit
FL_BAT  BIT   SW_FLG.3        ; Napajeni z baterie
FL_EDRF BIT   SW_FLG.2        ; Priznak refresovani udaju
FL_SPRR BIT   SW_FLG.1        ; Priznak otevrene spojky
FL_SERV BIT   SW_FLG.0        ; Priznak servisniho rezimu

SM_TIME EQU   40H
SM_PRES EQU   20H
SM_VOL  EQU   10H

SW1_FLG:DS    1
FL_POTC BIT   SW1_FLG.7       ; Potemciometr je v mezich

CFG_FLG:DS    1               ; Priznaky konfigurace davkovace
%IF(%CFG7HRS_FL)THEN(
FL_HRSOFF BIT CFG_FLG.7       ; uLan 4800 / 9600 Bd
)ELSE(
FL_UL48 BIT   CFG_FLG.7       ; uLan 4800 / 9600 Bd
)FI
FL_PARSV BIT  CFG_FLG.6       ; Ukladani posledni hodnoty parametru
FL_P100 BIT   CFG_FLG.5       ; Zmena standartni tlakove meze
			      ; pro tenzometr z 50 na 100 kPa
FL_USCH BIT   CFG_FLG.4       ; Uzivatel smi menit typ strikacky

PPCNT:  DS    1               ; Ping-Pong counter

RSEG IC____D

PPT_MAX EQU   30   ; Maximalni cas mezi prijmy Ping-Pongu
PPT_SND EQU   4    ; Cas po kterem se zacne vysilat
PPTIMR: DS    1    ; Casovac pro Ping-Pong

BPTIMR: DS    1    ; Citac delky pipnuti

M_TIMED EQU   2048; Pocet impulsu na preruseni od motoru
C_TIMED EQU   18   ; Deleni na 25 Hz pro MCB1 8051
C_TIM06 EQU   15   ; Deleni z 25 Hz na 0.6 s (0.01 min)
C_TMIN  EQU   100  ; Deleni na 1 min
N_OF_T  EQU   2    ; Pocet timeru
TIMR1:  DS    1    ; Timery jsou decrementovany
TIMRI:  DS    1    ; Delicka 0.01 min
TIMRJ:  DS    1    ; Delicka na minuty
TIME:   DS    2    ; Cas v min              =====
TIM_DIF:DS    1    ; Rozdil casu mezi procesory

; Delky jednotlivych pipnuti
BP_OK   EQU   3
BP_WA   EQU   20
BP_ERR  EQU   60

; Jednotlive chybove kody
ERINT   EQU   10H  ; Vnitrni chyba
ERINTSW EQU   10H  ;  Vnitrni chyba softu
ERINTSM EQU   11H  ;   Neopravneny rezim
ERINTST EQU   12H  ;   Neopravneny start cerpani
ERINTHW EQU   14H  ;  Vnitrni chyba hardware
ERFLTS  EQU   14H  ;   Rozchazi se davkovani mezi procesory
ERTIME  EQU   15H  ;   Rozchazi se hodiny procesoru
ERTRON  EQU   16H  ;   Chyba spinacich tranzistoru
ERTRHI  EQU   17H  ;   Chyba horniho tranzistoru
ERINTMS EQU   18H  ;   Rychlost motoru neodpovida
ERRCOMM EQU   19H  ;   Chyba komunikace
ERCOMMT EQU   1AH  ;   Chyba komunikace - time out
ERCAPNP EQU   1FH  ;   Neni pripojene CAP cidlo
ERNCMD  EQU   20H  ; Dele nez 2 min bez povelu
ERPRCUR EQU   42H  ; Prekrocen proud motorem => tlak limit
EREEPR  EQU   88H  ;  Chyba pri cteni udaju z EEPROM
ERST    EQU   90H  ; Chyba pri startu
ERSTAD  EQU   91H  ;  Chyby prevodniku
ERSTSYN EQU   99H  ;  Chyba synchronizace s druhym procesorem
ERSTWDT EQU   9AH  ;  Chyba ve watchdogu
ERSTSNU EQU   9BH  ;  Chyba v seriovem cislu
ERSTSNC EQU   9CH  ;  Nekompatabilni procesory

ERHRS   EQU   9DH  ; Chyba ve snimaci HRS

ERRNUM: DS    1    ; Cislo chyby

SPRTYP: DS    1    ; Typ strikacky  S555N222
		   ; S .. select 50/20; 555 .. typ 50 ml;
		   ; N .. zadna       ; 222 .. typ 20 ml;

A_MSK:  DS    2    ; Maska pozadavku na informace
A_PER:  DS    1    ; Perioda vysilani bloku informaci
A_CPOS: DS    1    ; Pozice cteni prikazu
A_OPOS: DS    1    ; Pozice zapisu dat
TIMR_A: DS    1    ; Casovac vysilani A_SYS


MOD_SLV:DS    1
MOD_II: DS    1    	      ; Pracovni mod druheho procesoru
M_SNORM EQU   0C0H            ; Stop
M_COMRD EQU   0C4H            ; Macteni dat z pocitace
M_RNORM EQU   0D0H            ; Cerpani
M_SERV  EQU   0E0H            ; Servisni rezim

SLAV_BL	EQU   8
IIC_INP:DS    SLAV_BL
TMP:
IIC_OUT:DS    SLAV_BL

RSEG    IC____I

MAX_TIM:DS    2    ; Limit casu
MAX_PRE:DS    2    ; Limit tlaku
MAX_VOL:DS    2    ; Limit objemu
II_TIM:	DS    2    ; Aktualni cas
II_PRE:	DS    2    ; Aktualni tlak
II_VOL:	DS    2    ; Aktualni objem

A_BUF:  DS    20H

%IF(%HALRS_FL)THEN(
RSEG    IC____B
HRS_FLG:DS    1	   ; Priznaky pomocneho Halova cidla 
FL_HRLV BIT   HRS_FLG.7	; Uroven vstupu
FL_HRCH BIT   HRS_FLG.6 ; Negativni priznak zmeny pro filtr
FL_HRCP BIT   HRS_FLG.5 ; Ubehla draha pro spusteni korekce
FL_HRCK BIT   HRS_FLG.4 ; Ubehla draha pro spusteni kontroly

HRS_ERC DATA  HRS_FLG   ; Citac chyb 
HRS_ERCM EQU  00FH

RSEG    IC____D
C_I2HRS EQU   3000 ; Pocet IRC na impulz 400*30/4 
HRS_CNT:DS    2    ; Citac impulzu

HRS_COR:DS    1    ; Korekce rozdilu s CAP_IRC

HRS_OFS:DS    2    ; Offset = HRS + IRC_ACT 

%IF(%POS_POT_FL)THEN(
HRS_PIN BIT   P4.0 ; Vstup z HAL senzoru
)ELSE(
HRS_PIN BIT   P1.3 ; Vstup z HAL senzoru
)FI

CAP_IRC:DS    2    ; Poloha v 256*irc vypoctena z potenciometru
		   ; nebo kapacitniho cidla
)FI
;=================================================================

RSEG IC____D

FLOW:   DS    2    ; Prutok

CAP_MAX:DS    2    ; Maximalni hodnota z kapacitniho cidal
CAP_DIV:DS    2    ; Hodnota z kapacitniho cidla na [1024*256 irc]
CAP_ACT:DS    2    ; Aktualni vystup z kapacitniho cidla
IRC_ACT:DS    2    ; Aktualni pozice z IRC cidla druheho procesoru

RSEG    IC____I

CAP_LST:DS    2    ; Minula poloha pro kalibraci a kontrolu behu
VOL_IN: DS    2    ; Aktualni objem do konce strikacky

;=================================================================
; Programovy segment

RSEG IC____C

START:  MOV   IEN0,#00000000B ; zakaz preruseni
	MOV   IEN1,#00000000B
	MOV   IP0, #00000000B ; priority preruseni
	MOV   IP1, #00000000B ;
	%WATCHDOG	      ; Nulovani watch-dogu
	MOV   TMOD,#00010001B ; timer 1 mod 1; timer 0 mod 1
	MOV   TCON,#01010101B ; citac 0 a 1 cita ; interapy hranou
	MOV   TM2CON,#00000000B; timer 2, 16 bit OV
	MOV   SCON,#11011000B ; dva stopbity
	MOV   PCON,#10000000B ; Bd = OSC/12/16/(256-TH1)
	MOV   TH1,#0FAH       ; 9600Bd
	MOV   PSW,#0          ; banka registru 0
	MOV   SP,#STACK       ; inicializace zasobniku
	MOV   P3,#0FFH
	MOV   P1,#0FFH

	MOV   PWMP,#0         ; PWM frekvence 23 kHz
	MOV   PWM0,#0

	MOV   PSW,#AR0
	MOV   B,#093H         ; Test instrukcniho souboru
	MOV   0,#055H
	MOV   A,#0F0H
	XRL   A,R0    ; A5
	MOV   9,#18H
	SETB  PSW.3
	MOV   @R1,A
	INC   R1
	MOV   @R1,#0C3H
	SETB  PSW.4   ; R0=A5, R1=C3
	ADD   A,B     ; A=38, PSW=9F
	SUBB  A,PSW
	RLC   A
	ANL   A,#NOT 4
	CJNE  A,#031H,V1ERR_H
	MUL   AB
	ADDC  A,B
	MOV   B,R0
	XRL   B,#0A0H
	DIV   AB
	SWAP  A
	XRL   A,B
	XRL   A,R1
	JZ    ST1_0
V1ERR_H:MOV   B,#5
V2ERR_H:JMP   ERR_HL1

ST1_0:	MOV   PSW,#AR0
	CLR   A               ; Test pameti eprom
	MOV   R2,#0EEH
	MOV   R3,#020H
	MOV   R4,A
	MOV   R5,A
	MOV   DPTR,#0
ST1_1:  CLR   A
	MOVC  A,@A+DPTR
	INC   DPTR
	ADD   A,R4
	MOV   R4,A
	MOV   A,R5
	ADDC  A,#0
	MOV   R5,A
	DJNZ  R2,ST1_1
	%WATCHDOG
	DJNZ  R3,ST1_1
	CLR   A
	MOVC  A,@A+DPTR
	ADD   A,R4
	MOV   R4,A
	INC   DPTR
	CLR   A
	MOVC  A,@A+DPTR
	ADDC  A,R5
	ORL   A,R4
	MOV   B,#7
	JNZ   V2ERR_H         ; !!!!!!!!!!!!!!!

ST2_0:  CLR   EA              ; Test pameti RAM
	MOV   R0,#0FFH
	MOV   A,@R0
	MOV   B,A
	MOV   DPL,A
	MOV   SP,R0
ST2_1:  ADD   A,#7
	PUSH  ACC
	CJNE  A,B,ST2_1
	%WATCHDOG             ; Nulovani watch-dogu
ST2_2:  INC   SP
	ADD   A,#7
	POP   B
	CJNE  A,B,V1ERR_H
	XRL   B,#0FFH
	PUSH  B
	CJNE  A,DPL,ST2_2
	%WATCHDOG             ; Nulovani watch-dogu
	MOV   DPH,#0
ST2_3:  INC   SP
	ADD   A,#7
	POP   B
	XRL   B,#0FFH
	CJNE  A,B,V1ERR_H
	PUSH  DPH             ; Nulovani pameti ram
	CJNE  A,DPL,ST2_3
	CLR   A
	CJNE  A,0,V1ERR_H
	MOV   SP,#STACK

	MOV   DPH,#HIGH LED1  ; Test AD prevodniku
	MOV   A,#0CFH
	MOVX  @DPTR,A
	CALL  DB_W_10
	MOV   R1,#ST_ADt-ST_D_Sk
	MOV   R7,#ERSTAD-1
ST_AD_1:INC   R7
	MOV   B,#1
	CALL  ST_D_S0
	JZ    ST_SYN
	CALL  GET_ADN
%IF (1) THEN (
	CALL  ST_D_S
	CALL  CMPi
	JC    ST_AD_E
	CALL  ST_D_S
	CALL  CMPi
	JNC   ST_AD_E
)ELSE(
	CALL  ST_D_S
	CALL  ST_D_S
	PUSH  AR1
	SETB  EA
	SETB  TR1
	SETB  ET1
	CLR   EBP
	CALL  PRTR45
ST_AD_3:CALL  ST_WDC          ; Nulovani watch-dogu
	CALL  SCANKEY
	JZ    ST_AD_3
	POP   AR1
)FI
	SJMP  ST_AD_1

ST_AD_E:MOV   ERRNUM,R7
	SJMP  ST_SYN

ST_D_S: MOV   B,#2
ST_D_S0:MOV   R0,#AR2
ST_D_S1:MOV   A,R1
	INC   R1
	MOVC  A,@A+PC
ST_D_Sk:MOV   @R0,A
	INC   R0
	DJNZ  B,ST_D_S1
	RET

ST_ADt: DB    0

;=============================================================
; Provedeni synchronizace s druhym procesorem

PPSYNSN:MOV   IIC_OUT,A
	JNB   FL_SERV,PPSYNS0
	MOV   A,#055H
PPSYNS0:MOV   IIC_OUT+2,A
	MOV   IIC_OUT+1,ERRNUM
	MOV   IIC_OUT+3,#SOFT_VER
PPSYNS1:CALL  SND_OUT
	JNB   ACC.2,PPSYNCR
	CALL  PPSYNW
	SJMP  PPSYNS1

PPSYNRC:MOV   TMP+2,#6H
PPSYNR1:JB    ICF_SRC,PPSYNR3 ; Cekani na druhy procesor
	CALL  PPSYNW
	SJMP  PPSYNR1
PPSYNR3:MOV   R0,#IIC_INP
	CALL  XOR_SU0         ; Kontrola prijateho xorsumu
	XRL   A,@R0
	JNZ   PPSYNER
	MOV   A,IIC_INP+1
	JNZ   PPSYNE1
	MOV   R7,IIC_INP+2
	MOV   A,R7
	XRL   A,#0AAH
	JZ    PPSYNR4
	CLR   FL_SERV
PPSYNR4:MOV   A,IIC_INP+3
	XRL   A,#SOFT_VER
	JZ    PPSYNR5
	MOV   ERRNUM,#ERSTSNC
PPSYNR5:MOV   A,IIC_INP
PPSYNCR:RET

ST_SYN:	JB    SERV_SW,ST_SYN2
	SETB  FL_SERV
ST_SYN2:MOV   S1ADR,#30H      ; Nastaveni adresy stanice
	MOV   R4,#IIC_INP
	MOV   R2,#SLAV_BL OR 80H
	MOV   R6,#0
	MOV   R7,#0
	CALL  IIC_SLI
	SETB  EA
	CALL  PPSYNRC
	CLR   ICF_SRC
	CJNE  A,#0B3H,ST_SYN3
	MOV   A,#0B4H
	CALL  PPSYNSN
	JMP   STARTC

ST_SYN3:CJNE  A,#0B0H,PPSYNER
	MOV   A,#0B1H
	CALL  PPSYNSN
	CALL  PPSYNRC
	CJNE  A,#0B0H,PPSYNER
	MOV   A,#0B2H
	CALL  PPSYNSN
PPSYNT:	CLR   EA
	CALL  ST_WDC
	MOV   TMP+1,#0B0H
PPSYNT0:DJNZ  TMP,PPSYNT0
	DJNZ  TMP+1,PPSYNT0
	CALL  ST_WDC
	MOV   A,#ERSTWDT
	SJMP  PPSYNE1

PPSYNW: DJNZ  TMP,ST_WDC
	DJNZ  TMP+1,ST_WDC
	DJNZ  TMP+2,ST_WDC
PPSYNER:MOV   A,#ERSTSYN
PPSYNE1:MOV   ERRNUM,A
	JMP   ERR_HLT

; Bezpecne nulovani watchdogu se zastavenim cerpani
ST_WDC: %WATCHDOG
	SETB  ELED
	MOV   PWM0,#0
	CLR   FL_MOT
	SETB  MOTOFF
	RET

;=============================================================
; Start komunikace s uzivatelem

STARTC:	MOV   AD_CNT,#80H     ; Zpusteni snimani prevodniku
	MOV   P4,#P4_RES
    %IF(%POS_POT_FL AND %HALRS_FL)THEN(
	MOV   RTE,#0          ; Vstup P4.0 se puziva pro HAL
	MOV   STE,#0
	MOV   CTCON,#000H
    )ELSE(
	MOV   RTE,#1          ; Spusteni mereni kapacitniho cidla
	MOV   CML1,#0
	MOV   CMH1,#0
	MOV   CTCON,#001H
    )FI
	MOV   TM2CON,#10000001B
    %IF(NOT %POS_POT_FL)THEN(
	SETB  ECT0
    )ELSE(
	MOV   CAP_ACT+1,#01H
      %IF(%POT_FILT_FL)THEN(
	MOV   A,#01H	      ; Pocatecni hodnota filtru
	MOV   R0,#FLT_POT+1
	MOV   @R0,A
	MOV   R0,#FLT_POT+3
	MOV   @R0,A
	MOV   R0,#FLT_POT+5
	MOV   @R0,A
      )FI
    )FI
	CLR   TR0             ; Priprava casoveho preruseni
	MOV   TH0,#0FFH
	MOV   TL0,#0FFH
	SETB  TR0
	ORL   IEN0,#10000010B ; Spusteni casoveho preruseni

	MOV   SPRTYP,#80H     ; Typ strikacky 50 ml - 0

	CLR   F0
	MOV   R0,#87H         ; Parametry strikacky
	CALL  EEPA_RD
	JB    F0,STARTC0
	MOV   SPRTYP,IIC_OUT+1
	MOV   CFG_FLG,IIC_OUT+2
STARTC0:
	MOV   CAP_MAX  ,#LOW  51507
	MOV   CAP_MAX+1,#HIGH 51507

	MOV   CAP_DIV  ,#LOW  10651
	MOV   CAP_DIV+1,#HIGH 10651

	CLR   F0
	MOV   R0,#80H         ; Kalibrace kapacitniho cidla
	CALL  EEPA_RD
	JB    F0,STARTC1
	MOV   CAP_MAX,  IIC_OUT+1
	MOV   CAP_MAX+1,IIC_OUT+2
	MOV   CAP_DIV,  IIC_OUT+3
	MOV   CAP_DIV+1,IIC_OUT+4
STARTC1:CLR   F0
	MOV   R0,#95H         ; Kontrola serioveho cisla
	CALL  EEPA_RD
	JB    F0,STARTC2
	MOV   R0,#IIC_OUT+1
	CALL  CMP_SN
	JZ    STARTC4
STARTC2:JB    FL_SERV,STARTC3
	MOV   A,#ERSTSNU
	CALL  SET_ERR
	SJMP  STARTC4
STARTC3:MOV   R0,#IIC_OUT+1
	MOV   DPTR,#SER_NUM
	MOV   R2,#4
	CALL  MOVsci
	CLR   F0
	MOV   R0,#95H         ; Kontrola serioveho cisla
	CALL  EEPA_WR
STARTC4:CLR   A
	MOV   R0,#AD_MCUR
	MOV   @R0,A
	INC   R0
	MOV   @R0,A
	MOV   R0,#AD_BAT
	MOV   @R0,A
	INC   R0
	MOV   @R0,#30H

	MOV   R5,#6           ; Rychlost 9600
%IF(NOT %CFG7HRS_FL)THEN(
	JNB   FL_UL48,STARTC5
	MOV   R5,#12          ; Rychlost 4800
STARTC5:
)FI
	CALL  uL_INIT	      ; Zpusteni uLan komunikace
	MOV   C,FL_EREE
	ANL   C,/FL_SERV
	JNC   C0
	MOV   A,#EREEPR
STARTCE:MOV   ERRNUM,A
C0:	MOV   MOD_SLV,#0C0H
	MOV   PPCNT,#1
C1:	CALL  CREC
CP1:    MOV   R0,MOD_SLV

%IF (%DEBUG_FL) THEN (
	CJNE  R0,#0,CP2       ; MOD_SLV=0 testovani vysilani na
	MOV   R5,TMP          ; slave 10H
	MOV   R4,TMP+1
	MOV   R6,#18H
	CALL  iPRTLhw
	MOV   R6,#18H
	CALL  OUT_BUF
     ;	%WATCHDOG	      ; Nulovani watch-dogu
	JB    ACC.0,C1

	MOV   A,TMP
	ORL   A,TMP+1
	MOV   DPTR,#LED0
	MOVX  @DPTR,A
	MOV   DPTR,#LED1
	MOVX  @DPTR,A
	MOV   DPTR,#LED2
	MOVX  @DPTR,A

	MOV   R1,#0
C2:     NOP
	DJNZ  R0,C2
     ;	%WATCHDOG	      ; Nulovani watch-dogu
	DJNZ  R1,C2
C3:  ;  %WATCHDOG	      ; Nulovani watch-dogu
	MOV   R6,#10H
	MOV   R4,#TMP
	MOV   R2,#2
	MOV   R3,#0
	CALL  IIC_RQI
	JB    ACC.0,C3
C4:     JBC   ICF_MER,C5
	JB    ICF_MRQ,C4
	INC   TMP
	JMP   C1
C5:	INC   TMP+1
C2_ER:	CLR   ICF_MRQ
	SETB  AA
	JMP   C1

CP2:    MOV   A,R0
	ANL   A,#0F0H
	CJNE  A,#010H,CP3     ; SLV_MOD=10 az 17h vystup prevodniku
	MOV   A,R0            ; na LCD
	ANL   A,#07H
	MOV   ADCON,A
	ORL   A,#08H
	MOV   ADCON,A
	MOV   R1,#10H
CP2_1:	NOP
	DJNZ  R2,CP2_1
      ;	%WATCHDOG	      ; Nulovani watch-dogu
	DJNZ  R1,CP2_1
	MOV   R4,ADCON
	MOV   R5,ADCH
	CLR   EA
	MOV   IIC_OUT,R4
	MOV   IIC_OUT+1,R5
	SETB  EA
	MOV   A,R0
	JB    ACC.3,CP2_2
	MOV   R6,#18H
	CALL  iPRTLhw
	MOV   R6,#18H
	CALL  OUT_BUF
	JMP   C1

CP2_2:	JBC   ICF_MER,C2_ER   ; SLV_MOD=18 az 1Fh Posilani dat z ADC na IIC
	MOV   R6,#10H
	MOV   R4,#IIC_OUT
	MOV   R2,#2
	MOV   R3,#0
	CALL  IIC_RQI
	JMP   C1

CP3:    CJNE  R0,#1,CP4       ; SLV_MOD=1 test cteni IIC_OUT
	MOV   IIC_OUT,#012H
	INC   IIC_OUT+1
	JMP   C1

CP4:    CJNE  R0,#2,CP5       ; SLV_MOD=2  vypis CAP_ACT na display

	MOV   R4,CAP_ACT
	MOV   R5,CAP_ACT+1
	MOV   R6,#18H
	CALL  iPRTLhw
	MOV   R7,#0C4H
;	CALL  iPRTLi
	MOV   R6,#18H
	CALL  OUT_BUF
	MOV   R1,#80H
CP4_1:	NOP
	DJNZ  R2,CP4_1
      ;	%WATCHDOG	      ; Nulovani watch-dogu
	DJNZ  R1,CP4_1
	JMP   C1
)FI

CP5:    MOV   A,R0            ; SLV_MOD=C0 pracovni rezim
	ADD   A,#-0C0H
	ADD   A,#-8
	JC    CP6
	CALL  PPSYS
	JNB   FL_EDRF,CP5_50
	CLR   FL_EDRF
	CALL  DI_SPR
CP5_50:	JMP   C1

CP6:
	JMP   C1

URECNO:	RET

CREC:   JB    ICF_SRC,CREC0
	MOV   R0,#BEG_IB
	MOV   A,@R0
	JNB   ACC.7,URECNO
	INC   R0              ; Zpracovani zprav uLan
	MOV   A,@R0
	CJNE  A,#07FH,UREC10
	INC   R0
	MOV   A,@R0
	CLR   C
	SUBB  A,#BEG_IB+3+1+4+1
	JC    URECRV1
	INC   R0
	MOV   A,@R0
	CJNE  A,#0C1H,URECRV1
	INC   R0
	CALL  CMP_SN
	JNZ   URECRV1
	MOV   uL_ADR,@R0
	ANL   uL_DYFL,#NOT 7
URECRV1:JMP   URECR
UREC10: CJNE  A,#024H,UREC20
	MOV   A,MOD_II
	XRL   A,#M_COMRD
	JNZ   URECRV1
	INC   R0
	INC   R0
	MOV   FLOW,@R0
	INC   R0
	MOV   FLOW+1,@R0
	INC   R0
	MOV   A,R0
	MOV   R1,A
	MOV   R0,#MAX_TIM
	MOV   R2,#6
	CALL  MOVsi
	SETB  FL_SETL
	JMP   URECR
UREC20:

URECR:	MOV   R0,#BEG_IB
	MOV   @R0,#0
	RET

CREC0:	MOV   R0,IIC_INP      ; Zpracovani zprav IIC
	CJNE  R0,#1,CREC1     ; Prikaz 1 nastaveni MOD_SLV
	MOV   MOD_SLV,IIC_INP+1
CREC1:	CJNE  R0,#2,CREC2     ; Prikaz 2 nastaveni LED
	MOV   DPTR,#LED0
	MOV   A,IIC_INP+1
	MOVX  @DPTR,A
	MOV   DPTR,#LED1
	MOV   A,IIC_INP+2
	MOVX  @DPTR,A
	MOV   DPTR,#LED2
	MOV   A,IIC_INP+3
	MOVX  @DPTR,A
CREC2:  CJNE  R0,#3,CREC3     ; Prikaz 3 zkouska citace casu
	MOV   TIMR1,#0FFH
CREC2_1:MOV   R4,TIME_CN
	MOV   R5,TIMR1
	MOV   R6,#18H
	CALL  iPRTLhw
	MOV   R6,#18H
	CALL  OUT_BUF
	MOV   A,TIMR1
	JNZ   CREC2_1
	JMP   CRR
CREC3:  CJNE  R0,#4,CREC4     ; Prikaz 4 nulovani citace polohy
	JMP   CRR

CREC4:  CJNE  R0,#80H,CREC5   ; Vstup z TTY
	MOV   R4,IIC_INP+1
	MOV   R5,#0
	MOV   R6,#18H
	CALL  iPRTLhw
	CALL  IIC_WME
	MOV   R6,#18H
	CALL  OUT_BUF
	CALL  IIC_WME
	MOV   TMP,#'A'
	MOV   TMP+1,#'b'
	MOV   R6,#10H
	MOV   R4,#TMP
	MOV   R2,#2
	MOV   R3,#0
	CALL  IIC_RQI
	CALL  IIC_WME
	CALL  DB_WAIT
	JMP   CRR
CREC5:  CJNE  R0,#0BH,CREC6   ; Prikaz B nastaveni brany P4
	MOV   A,IIC_INP+1
	ANL   P4,#NOT P4_MSK
	ANL   A,#P4_MSK
	ORL   P4,A
	JMP   CRR

CREC6:

CREC10: CJNE  R0,#0AH,CREC11  ; Prikaz A nastaveni A_SYS
	CLR   EA
	MOV   A_PER,IIC_INP+1
	MOV   A_MSK,#1
	MOV   A_MSK+1,IIC_INP+2
	MOV   A_CPOS,#0
	MOV   A,A_PER
	JZ    CREC10R
	CLR   ICF_MER
	CLR   ICF_MRQ
	MOV   A_CPOS,#80H
CREC10R:SETB  EA
	JMP   CRR

CREC11: MOV   A,R0            ; Ping-Pong C0-FF
	XRL   A,#0C0H
	ANL   A,#0C0H
	JNZ   CREC12
	CALL  PPREC
	JMP   CRR
CREC12: CJNE  R0,#0BFH,CREC13 ; Nove nahozeni Pig-Pongu
	CLR   ICF_SRC
	MOV   MOD_SLV,#0C0H
	CALL  PPSTART
	RET

CREC13:

CRR:	CLR   ICF_SRC
	RET

DB_W_10:MOV   R0,#10H
	SJMP  DB_WAI1
DB_WAIT:MOV   R0,#0
DB_WAI1:CALL  ST_WDC
	DJNZ  R1,DB_WAI1
	DJNZ  R0,DB_WAI1
	RET

;=================================================================
; Nastaveni chyby

SET_ERR:SETB  FL_NERR
SET_ERRS:
	XCH   A,ERRNUM
	JZ    SET_ER1
	XCH   A,ERRNUM
SET_ER1:SETB  ELED
	CLR   FL_MOT
	SETB  MOTOFF
	RET

;=================================================================
; Indikace typu strikacek

%IF (0) THEN (
DI_SPR: MOV   R0,#0FFH
	MOV   A,@R0
	MOV   R0,#8
DI_SPRT:CLR   LED_CLK
	RLC   A
	MOV   LED_DAT,C
	SETB  LED_CLK
	DJNZ  R0,DI_SPRT
	RET
)ELSE(
DI_SPR: MOV   A,SPRTYP
	SWAP  A
	CALL  DI_SPR3
	MOV   A,SPRTYP
DI_SPR3:ANL   A,#7H
	JNB   ACC.2,DI_SPR4
	MOV   A,#3
DI_SPR4:MOV   R1,A
	INC   R1
	MOV   R0,#4
DI_SPR5:CLR   LED_CLK
	MOV   A,R1
	XRL   A,R0
	SETB  LED_DAT
	JNZ   DI_SPR6
	CLR   LED_DAT
DI_SPR6:SETB  LED_CLK
	DJNZ  R0,DI_SPR5
	RET
)FI

;=================================================================
; Ruzne vypocty

; Prevadi kapacitni cidlo na [256*IRC] podle vztahu
;   (CAP_MAX-CAP_ACT)/CAP_DIV*1024

%IF(NOT %HALRS_FL)THEN(
CAP2IRC:
)FI
CAP2I1:	MOV   R4,CAP_MAX
	MOV   R5,CAP_MAX+1
	MOV   C,EA
	CLR   EA
	MOV   R2,CAP_ACT
	MOV   R3,CAP_ACT+1
	MOV   EA,C
	CALL  SUBi
	JC    CAP2IZ
	MOV   R2,CAP_DIV
	MOV   R3,CAP_DIV+1
	CLR   F0
	MOV   R0,#0
	MOV   R1,#10	      ; 2^10=1024
	CALL  DIVi0
	RET
CAP2IZ: CLR   A
	MOV   R4,A
	MOV   R5,A
	RET

; Reseni prekryvajici vypadky potenciometru z HAL senzoru otacek

%IF(%HALRS_FL)THEN(
CAP2IRC:CALL  CAP2I1
	MOV   CAP_IRC,R4
	MOV   CAP_IRC+1,R5
	JB    FL_MOT,HRS2I04
	JMP   HRS2I60	      ; Motor se netoci
HRS2I04:
    %IF(%CFG7HRS_FL)THEN(
	JNB    FL_HRSOFF,HRS2I05
	RET
HRS2I05:
    )FI
	MOV   A,HRS_COR
	MOV   R0,#0
	JNB   ACC.7,HRS2I06
	DEC   R0
HRS2I06:MOV   C,EA
	CLR   EA
	MOV   A,HRS_CNT
	MOV   R4,HRS_CNT+1    ; R45=HRS_CNT+HRS_COR
	MOV   EA,C
	ADD   A,HRS_COR
	XCH   A,R4
	ADDC  A,R0
	MOV   R5,A
	
	MOV   R2,#LOW C_I2HRS ; Pocet IRC na impulz
        MOV   R3,#HIGH C_I2HRS
	CALL  MULi
	CLR   C
	MOV   A,R5	      ; Delit na 256-tice IRC
	MOV   R4,A
	SUBB  A,CAP_IRC
	MOV   R2,A
	MOV   A,R6
	MOV   R5,A	      ; R45 hodnota HRS prepoctena na IRC*256
	SUBB  A,CAP_IRC+1
	MOV   R3,A	      ; R23 = R45 - CAP_IRC
	MOV   A,MOD_II
	ANL   A,#0F0H	      ; Neprovadet kontrolu a korekce
	CJNE  A,#M_SERV,HRS2I12 ; v servisnim rezimu
	JMP   HRS2I95
HRS2I12:JBC   FL_HRCK,HRS2I30
HRS2I20:JBC   FL_HRCP,HRS2I40	
HRS2I21:MOV   A,R3
	JNB   ACC.7,HRS2I95   ; Minimem je CAP_IRC
	SJMP  HRS2I99	      ; Minimem je informace z HRS 
; Kontrola soubehu
HRS2I30:MOV   A,R2
	ADD   A,#LOW ((C_I2HRS*6)/256)
	MOV   R0,A
	MOV   A,R3
	ADDC  A,#HIGH ((C_I2HRS*6)/256)
	XCH   A,R0
	ADD   A,#LOW (-((C_I2HRS*12)/256))
	MOV   A,R0
	ADDC  A,#HIGH (-((C_I2HRS*12)/256))
	MOV   A,HRS_ERC
	ANL   A,#HRS_ERCM
	JC    HRS2I33
	JZ    HRS2I20
	DEC   HRS_ERC	      ; Rozdil v toleranci
	SJMP  HRS2I20	      ; => snizit citac chyb
HRS2I33:ADD   A,#-9
	JC    HRS2I35         ; Rozdil mimo toleranci
	INC   HRS_ERC	      ; => zvysit citac chyb
	SJMP  HRS2I20
HRS2I35:MOV   A,#ERCAPNP      ; Prekrocen citac => CHYBA
	CALL  SET_ERR
	SJMP  HRS2I95
; Korekce soubehu
HRS2I40:MOV   A,R3
        JNB   ACC.7,HRS2I42
	INC   HRS_COR	      ; Posunout korekci zpet
	SJMP  HRS2I21
HRS2I42:MOV   A,R2
	ADD   A,#LOW (-((C_I2HRS*3)/256/2))
	MOV   A,R3
	ADDC  A,#HIGH (-((C_I2HRS*3)/256/2))
	JNC   HRS2I21
	DEC   HRS_COR	      ; Posunout korekci dopredu 
	SJMP  HRS2I21

HRS2I60:MOV   R2,#LOW C_I2HRS ; Pocet IRC na impulz
        MOV   R3,#HIGH C_I2HRS
	CLR   F0
	MOV   R0,#0
	MOV   R1,#8	      ; 2^8=256 
	CALL  DIVi0	      ; Pprepocet z 256-tic IRC na HRS
	MOV   C,EA
	CLR   EA
	MOV   HRS_CNT,R4
	MOV   HRS_CNT+1,R5
	MOV   EA,C
	MOV   HRS_COR,#0
	ANL   HRS_ERC,#NOT HRS_ERCM
HRS2I95:MOV   R4,CAP_IRC
	MOV   R5,CAP_IRC+1
HRS2I99:RET
)FI

%IF(%HALRS_FL)THEN(
CHK_HRS:
    %IF(%CFG7HRS_FL)THEN(
	JB    FL_HRSOFF,CHK_H90
    )FI
	JNB   FL_SERV,CHK_H10
    	MOV   A,MOD_II
	ANL   A,#0F0H	      ; Neprovadet kontrolu 
	XRL   A,#M_SERV	      ; v servisnim rezimu
	JZ    CHK_H90
CHK_H10:MOV   C,EA
	CLR   EA
	MOV   R4,HRS_CNT
	MOV   R5,HRS_CNT+1
	MOV   EA,C
	MOV   R2,#LOW C_I2HRS ; Pocet IRC na impulz
        MOV   R3,#HIGH C_I2HRS
	CALL  MULi
	MOV   A,R5	      ; Delit na 256-tice IRC
	MOV   R4,A
	ADD   A,IRC_ACT
	MOV   R2,A
	MOV   A,R6
	MOV   R5,A	      ; R45 hodnota HRS prepoctena na IRC*256
	ADDC  A,IRC_ACT+1
	MOV   R3,A	      ; R23 = R45 + IRC_ACT
	MOV   A,HRS_OFS+1
	CJNE  A,#86H,CHK_H22
	DEC   HRS_OFS+1
	SJMP  CHK_H90
CHK_H22:CJNE  A,#85H,CHK_H25
	MOV   HRS_OFS,R2
	MOV   HRS_OFS+1,R3    ; Zkalibrovani offsetu pri prvnim 
	SJMP  CHK_H90	      ; pruchodu
CHK_H25:CLR   C
	MOV   A,R2
	SUBB  A,HRS_OFS
	MOV   R2,A
	MOV   A,R3
	SUBB  A,HRS_OFS+1
	MOV   R3,A	      ; R23=HRS+IRC_ACT-HRS_OFS
	MOV   A,R2
	ADD   A,#LOW  200
	XCH   A,R3
	ADDC  A,#HIGH 200
	XCH   A,R3	      ; HRS a IRC musi sedet s maximalni
	ADD   A,#LOW (-400)   ; odchylkou +/-200 IRC*256
	XCH   A,R3
	ADDC  A,#HIGH (-400)
	JNC   CHK_H90
	MOV   A,#ERCAPNP      ; Rozchazi se IRC a HRC
	MOV   A,#ERHRS
	CALL  SET_ERR
CHK_H90:RET

)FI
;=================================================================
; Ping-Pong mezi procesory

PPRECE: MOV   A,#ERRCOMM
	JMP   SET_ERR

PPREC:  CALL  PPINC
	CJNE  A,IIC_INP,PPRECE
	MOV   A,S_RLEN
	CJNE  A,#SLAV_BL,PPRECE
	MOV   R0,#IIC_INP
	CALL  XOR_SU0
	XRL   A,@R0
	JNZ   PPRECE
	MOV   PPTIMR,#PPT_MAX ; Natazeni watchdogu PP
	CALL  PPSW
	RET

PPSTART:MOV   PPCNT,#0
	MOV   PPTIMR,#PPT_MAX ; Natazeni watchdogu PP
	SJMP  PPSEND
PPSYS:  MOV   A,PPTIMR        ; Test casove prodlevy
	ADD   A,#-PPT_MAX+PPT_SND
	JC    PPSRET          ; Nenadesel jeste cas
PPSEND: JB    PPCNT.0,PPSRET
	CALL  PPINC
	MOV   IIC_OUT,A
	CALL  PPSW
SND_OUT:MOV   R0,#IIC_OUT
	CALL  XOR_SU0
	MOV   @R0,A

%IF (%DEBUG_FL) THEN (
	MOV   A,PPCNT
	SWAP  A
	XRL   A,PPCNT
	ANL   A,#03H
	JNZ   SND_OU1
)FI
SND_OU0:
%IF (%DEBUG_FL) THEN (
	CALL  IIC_CER
	MOV   R6,#10H
	MOV   R4,#IIC_OUT
	MOV   R2,#SLAV_BL
	MOV   R3,#0
	CALL  IIC_RQI
	JNZ   SND_OU0
	CALL  IIC_WME
)FI

SND_OU1:CALL  IIC_CER
	MOV   R6,#20H
	MOV   R4,#IIC_OUT
	MOV   R2,#SLAV_BL
	MOV   R3,#0
	CALL  IIC_RQI
	JNZ   SND_OU1
	CALL  IIC_WME
PPSRET: RET

PPINC:  MOV   A,PPCNT
	INC   A
	ANL   A,#03FH
	ANL   PPCNT,#0C0H
	ORL   PPCNT,A
	ORL   A,#0C0H
	RET

XOR_SU0:MOV   R2,#SLAV_BL-1
XOR_SUM:CLR   A
XOR_SU1:MOV   R3,A
	MOV   A,@R0
	INC   R0
	XRL   A,R3
	INC   A
	DJNZ  R2,XOR_SU1
	RET

PPSW:   MOV   A,PPCNT
	ANL   A,#00FH
	RL    A
	MOV   R0,A
	ADD   A,#PPSWT-PPSWk1
	MOVC  A,@A+PC
PPSWk1: PUSH  ACC
	MOV   A,R0
	ADD   A,#PPSWT-PPSWk2+1
	MOVC  A,@A+PC
PPSWk2: PUSH  ACC

PPSWRET:RET

PPSWT:  %W    (PPSW0)
	%W    (PPSW1)
	%W    (PPSW2)
	%W    (PPSW3)
	%W    (PPSW4)
	%W    (PPSW5)
	%W    (PPSW6)
	%W    (PPSWRET)
	%W    (PPSW8)
	%W    (PPSW9)
	%W    (PPSWA)
	%W    (PPSWB)
	%W    (PPSWRET)
	%W    (PPSWRET)
	%W    (PPSWE)
	%W    (PPSWRET)

PPSW0:  CLR   EA
	MOV   IRC_ACT,IIC_INP+2
	MOV   IRC_ACT+1,IIC_INP+3
	SETB  EA
	MOV   A,ERRNUM
	JNZ   PPSW010
	MOV   A,IIC_INP+6     ; ERRNUM druheho procesoru
	JZ    PPSW010
	CALL  SET_ERRS
PPSW010:JB    FL_MOT,PPSW012
	MOV   A,MOD_II
	XRL   A,#M_COMRD
	JZ    PPSW011
	MOV   FLOW  ,IIC_INP+4; Prutok z druheho procesoru
	MOV   FLOW+1,IIC_INP+5
PPSW011:RET
PPSW012:	
    %IF(%HALRS_FL)THEN(
	CALL  CHK_HRS
    )FI
	RET

PPSW1:  MOV   R4,#LOW  1700H  ; Dostatecna uroven vnejsiho napajeni
	MOV   R5,#HIGH 1700H
	MOV   R0,#AD_DCIN
	CALL  iLDR23i
	CALL  CMPi
	MOV   FL_DCIN,C
	CLR   SPRTST          ; Zjisteni typu strikacky
	MOV   C,SPRRDY
	MOV   FL_SPRR,C
	MOV   IIC_OUT+1,HW_FLG
	MOV   IIC_OUT+2,SW_FLG
	CLR   FL_NERR
	MOV   IIC_OUT+3,ERRNUM
	MOV   A,P5
	ANL   A,#3
	ADD   A,#PPSW1t-PPSW1k
	MOVC  A,@A+PC
PPSW1k:	ANL   SPRTYP,#077H
	ORL   SPRTYP,A
	MOV   IIC_OUT+4,SPRTYP
	SETB  SPRTST
	CALL  CAP2IRC
	MOV   IIC_OUT+5,R4
	MOV   IIC_OUT+6,R5
	RET
PPSW1t: DB    008H
	DB    000H
	DB    080H
	DB    088H

PPSW23: CJNE  R7,#OMS_CER,PPSW24 ; Prikaz nulovani chyby
	MOV   A,ERRNUM
	JB    ACC.7,PPSW24
	MOV   A,IIC_INP+3
	CJNE  A,#055H,PPSW24
	MOV   ERRNUM,IIC_INP+4
	CLR   ELED
	MOV   R0,#AD_BAT
	MOV   @R0,A
	INC   R0
	MOV   @R0,#30H
PPSW24: CJNE  R7,#OMS_SCH,PPSW260
	MOV   C,FL_MOT
	ORL   C,/FL_USCH
	ANL   C,/FL_SERV
	JC    PPSW2E
	MOV   A,MOD_II
	ANL   A,#0F0H
	XRL   A,#M_SNORM
	JZ    PPSW25
	XRL   A,#M_SNORM XOR M_SERV
	JNZ   PPSW2E
PPSW25:	MOV   SPRTYP,IIC_INP+3
PPSW260:CJNE  R7,#OMS_CCH,PPSW270
	JNB   FL_SERV,PPSW2E
	MOV   CFG_FLG,IIC_INP+3
PPSW270:JMP   PPSWPPF

PPSW2E: MOV   A,#ERINTSM
	CALL  SET_ERR
	JMP   PPSWPPF

PPSW2:  MOV   A,IIC_INP+6     ; MOD_SLV druheho procesoru
	MOV   R7,A
	ANL   A,#0F0H
	CJNE  A,#M_SERV,PPSW201
	JNB   FL_SERV,PPSW2E
PPSW201:MOV   MOD_II,IIC_INP+6
	MOV   R7,IIC_INP+2    ; OUT_MSG druheho procesoru
	MOV   A,R7
	ANL   A,#0F0H
	CJNE  A,#080H,PPSW23  ; OMS_CAP
	JNB   FL_SERV,PPSW2E
	CJNE  R7,#81H,PPSW211
PPSW210:CLR   EA
	MOV   R4,CAP_ACT
	MOV   R5,CAP_ACT+1
	SETB  EA
	MOV   R0,#CAP_LST
	CALL  iSVR45i
	SJMP  PPSW2R
PPSW211:CJNE  R7,#83H,PPSW212
	CALL  PPSW21S
	MOV   CAP_DIV,R4
	MOV   CAP_DIV+1,R5
	CALL  PRTR45
	SJMP  PPSW210
PPSW212:CJNE  R7,#85H,PPSW215
	CALL  PPSW21S
	MOV   R2,CAP_DIV
	MOV   R3,CAP_DIV+1
	CALL  SUBi
	CALL  SHR1i
	MOV   A,R4
	MOV   R2,A
	MOV   A,R5
	MOV   C,ACC.6
	MOV   ACC.7,C
	MOV   R3,A
	MOV   R4,CAP_DIV
	MOV   R5,CAP_DIV+1
	CALL  ADDi
	MOV   CAP_DIV,R4
	MOV   CAP_DIV+1,R5
	MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	JNB   ACC.7,PPSW213
	CALL  NEGi
PPSW213:MOV   R2,#LOW  10
	MOV   R3,#HIGH 10
	CALL  CMPi
	;JNC ERROR
	CALL  PRTR45
	SJMP  PPSW2R
PPSW215:CJNE  R7,#87H,PPSW216
	CLR   EA
	MOV   CAP_MAX,CAP_ACT
	MOV   CAP_MAX+1,CAP_ACT+1
	SETB  EA
	MOV   R4,CAP_DIV
	MOV   R5,CAP_DIV+1
	CALL  PRTR45
	MOV   IIC_OUT+1,CAP_MAX
	MOV   IIC_OUT+2,CAP_MAX+1
	MOV   IIC_OUT+3,CAP_DIV
	MOV   IIC_OUT+4,CAP_DIV+1
	CLR   F0
	MOV   R0,#80H
	CALL  EEPA_WR
	SJMP  PPSW2R
PPSW216:
PPSW2R: SJMP  PPSWPPF

PPSW3:  CLR   EA
	MOV   R5,#0FFH
	MOV   A,ERRNUM
	JNZ   PPSW350
	MOV   R0,#AD_BAT
	CALL  iLDR45i
PPSW350:MOV   IIC_OUT+1,R4
	MOV   IIC_OUT+2,R5
	SETB  EA
	RET

PPSW4:  MOV   R0,#II_TIM
	JMP   PPSW899

PPSW5:	CALL  CAP2IRC         ; Soucasna poloha palce
	MOV   A,#O_SPRE       ; Konec strikacky
	CALL  G_SPR
	CALL  SUBi
	JNC   PPSW505
	CLR   A
	MOV   R4,A
	MOV   R5,A
PPSW505:MOV   A,#O_SPRV       ; Objem strikacky
	CALL  G_SPR
	JZ    PPSW508
	CALL  MULi            ; R4567 = V*CAP
	CALL  NORMMI          ; Znormuje R4567 a exp R1 na R45 a R1
	MOV   A,#O_SPRL       ; Delka strikacky
	CALL  G_SPR
	CALL  DIVihf          ; vydavkovany objem = V*CAP/L
	MOV   A,R1
	CPL   A
	INC   A
	CALL  SHRi
PPSW508:MOV   R0,#VOL_IN
	CALL  iSVR45i
	MOV   IIC_OUT+1,R4
	MOV   IIC_OUT+2,R5
	RET

PPSW6:
PPSWPPF:MOV   B,IIC_INP+1     ; Zpracovani PP_FLG z II
	JNB   B.0,PPSW655     ; FL_BEOK
	MOV   BPTIMR,#BP_OK
PPSW655:JNB   B.1,PPSW657     ; FL_BEER
	MOV   BPTIMR,#BP_ERR
PPSW657:JNB   B.5,PPSW658     ; FL_PPTI
	INC   TIM_DIF
PPSW658:MOV   A,B
	ANL   A,#00001100B
	JZ    PPSW659
	SETB  FL_BEWA
PPSW659:MOV   A,MOD_II
	ANL   A,#0F0H
	MOV   R1,A
	CJNE  R1,#M_SERV,PPSW661
PPSW660:SETB  FL_MOT
	SJMP  PPSW699
PPSW661:MOV   A,ERRNUM
	JNZ   PPSW698
	JB    B.6,PPSW662     ; FL_HSPD
	JNB   B.7,PPSW698     ; FL_STRT
PPSW662:JB    FL_MOT,PPSW699
	MOV   A,PPCNT
	ANL   A,#00FH
	CJNE  A,#2,PPSW6ER
	CALL  CAP2IRC
	MOV   R0,#CAP_LST
	CALL  iSVR45i
%IF(%HALRS_FL)THEN(
	MOV   HRS_OFS+1,#86H  ; Zkalibrovani offsetu pri prvnim 
)FI	
	MOV   B,IIC_INP+1     ; Zpracovani PP_FLG z II
	JB    B.6,PPSW660     ; FL_HSPD
	MOV   A,ERRNUM        ; Start cerpani
	JNZ   PPSW698
	SETB  FL_MOT
	CLR   MOTOFF
	CLR   A
	MOV   TIMRJ,A
	MOV   TIME,A
	MOV   TIME+1,A
	SJMP  PPSW699
PPSW698:CLR   FL_MOT
PPSW699:MOV   A,MOD_II
	ANL   A,#0F0H
	CJNE  A,#M_RNORM,PPSW6R1
	JNB   FL_MOT,PPSW6ER
PPSW6R1:RET
PPSW6ER:MOV   A,#ERINTST
	JMP   SET_ERR

PPSW8:  MOV   A,MOD_II
	XRL   A,#M_COMRD
	JZ    PPSW8R
	MOV   R0,#MAX_TIM
PPSW899:MOV   R1,#IIC_INP+1
	MOV   R2,#6
	CLR   EA
	CALL  MOVsi
	SETB  EA
PPSW8R:	RET

PPSW9:  MOV   R1,#MAX_TIM
	MOV   R0,#IIC_OUT+1
	MOV   R2,#6
	JMP   MOVsi

PPSWA:	CALL  G_MCLIM         ; Test proudu motorem
	MOV   A,IIC_INP+1     ; PP_FLG
	JB    ACC.7,PPSWA11   ; FL_STRT
	MOV   A,MOD_II
	ANL   A,#0F0H
	CJNE  A,#M_SNORM,PPSWA11
	MOV   A,SPRTYP
	JB    ACC.7,PPSWA11
	MOV   A,R2            ; Pridani proudu
	ADD   A,#LOW  1000H   ; na rychloposun 20 ml
	MOV   R2,A 	      ; strikacky
	MOV   A,R3
	ADDC  A,#HIGH 1000H
	MOV   R3,A
PPSWA11:JB    FL_DCIN,PPSWA12
	MOV   R4,#LOW  180H
	MOV   R5,#HIGH 180H
	CALL  MULi
	MOV   A,R5
	MOV   R2,A
	MOV   A,R6
	MOV   R3,A
PPSWA12:MOV   R0,#AD_MCUR
	CLR   EA
	CALL  iLDR45i
	SETB  EA
	CALL  CMPi
	CPL   C
	MOV   FL_MOV,C
	JNC   PPSWA15
	MOV   A,#ERPRCUR
	CALL  SET_ERR
PPSWA15:MOV   A,MOD_II
	ANL   A,#0F0H
	CJNE  A,#M_RNORM,PPSWA50
	CALL  CHK_CAP
PPSWA50:JMP   PPSWPPF

PPSWB:  CALL  CAP2IRC
	MOV   IIC_OUT+1,R4
	MOV   IIC_OUT+2,R5
	CLR   A
	JNB   FL_SETL,PPSWB10
	CPL   A
PPSWB10:MOV   IIC_OUT+4,A
	MOV   IIC_OUT+5,FLOW
	MOV   IIC_OUT+6,FLOW+1
	CLR   FL_SETL
	CLR   A
	JNB   FL_NERR,PPSWB20
	CLR   FL_NERR
	MOV   A,ERRNUM
PPSWB20:MOV   IIC_OUT+3,A     ; Zrychleny prenos chyby
	RET

PPSWE:  JNB   uLD_RQA,PPSWE05
	MOV   A,PPCNT
	ANL   A,#03FH
	CJNE  A,#00EH,PPSWE05
	CALL  SNRQ
PPSWE05:MOV   A,IIC_INP+6     ; SW1_FLG z II
	JNB   ACC.4,PPSWE20   ; FL_TRTS
	JNB   FL_MOT,PPSWE20
	MOV   A,#6
	CALL  GET_ADN
	MOV   A,R5
	MOV   R7,A
	ADD   A,#-30H         ; Kontrola funkce spinacu
	JNC   PPSWE10
	MOV   A,#5
	CALL  GET_ADN
	MOV   A,R5
	SUBB  A,R7
	ADD   A,#8
	ADD   A,#-28H
	JNC   PPSWE11
PPSWE10:MOV   A,#ERTRON
	CALL  SET_ERR
PPSWE11:SETB  AD_EN
PPSWE20:JMP   PPSWPPF

PPSW21S:MOV   R0,#CAP_LST
	CALL  iLDR23i
	CLR   EA
	MOV   R4,CAP_ACT
	MOV   R5,CAP_ACT+1
	SETB  EA
	CALL  SUBi
	RET

;=================================================================
; Tabulka proudu pro 20 A 50 ml strikacky

G_MCLIM:MOV   A,SPRTYP
	MOV   C,ACC.7         ; 20 / 50 ml
	MOV   A,CFG_FLG
	ANL   A,#0FH
	RLC   A
	RLC   A
	MOV   R2,A
	ADD   A,#T_MCLIM-G_MCLI2
	MOVC  A,@A+PC
G_MCLI2:XCH   A,R2
	ADD   A,#T_MCLIM-G_MCLI3+1
	MOVC  A,@A+PC
G_MCLI3:MOV   R3,A
	MOV   A,#1
	RET

T_MCLIM:%W    ( 800)          ; 0 - 20
	%W    (1000)          ; 0 - 50
	%W    ( 900)          ; 1 - 20
	%W    (1200)          ; 1 - 50
	%W    (1050)          ; 2 - 20
	%W    (1400)          ; 2 - 50
	%W    (1150)          ; 3 - 20
	%W    (1600)          ; 3 - 50
	%W    (1350)          ; 4 - 20
	%W    (1900)          ; 4 - 50
	%W    (1500)          ; 5 - 20
	%W    (2200)          ; 5 - 50
	%W    (1650)          ; 6 - 20
	%W    (2500)          ; 6 - 50
	%W    (1850)          ; 7 - 20
	%W    (2900)          ; 7 - 50
	%W    (2000)          ; 8 - 20
	%W    (3300)          ; 8 - 50
	%W    (2200)          ; 9 - 20
	%W    (3800)          ; 9 - 50
	%W    (2400)          ; A - 20
	%W    (4300)          ; A - 50
	%W    (2650)          ; B - 20
	%W    (4800)          ; B - 50
	%W    (2950)          ; C - 20
	%W    (5400)          ; C - 50
	%W    (3400)          ; D - 20
	%W    (6000)          ; D - 50
	%W    (3600)          ; E - 20
	%W    (6600)          ; E - 50
	%W    (3800)          ; F - 20
	%W    (7000)          ; F - 50

;=================================================================
; Kontrolni vypocet casu z polohy

CHK_CAP:CALL  CAP2IRC
	CALL  NEGi
	MOV   R0,#CAP_LST
	CALL  iLDR23i
	CALL  ADDi            ; Rozdil v 256*IRC
	JNC   CHK_CAZ
	MOV   A,#O_SPRV
	CALL  G_SPR
	CALL  MULi            ; R4567 = V*CAP
	CALL  NORMMI          ; Znormuje R4567 a exp R1 na R45 a R1
	MOV   A,#O_SPRL
	CALL  G_SPR
	CALL  DIVihf          ; vydavkovany objem = V*CAP/L
			      ; R45 exp R1
	MOV   R2,#LOW  60     ; Prutok FLOW je za hod cas je v min
	MOV   R3,#HIGH 60
	CALL  MULi
	CALL  NORMMI1
	MOV   R2,FLOW
	MOV   R3,FLOW+1
	CALL  DIVihf          ; T_chk v min = V*CAP*60/L/FLOW
	MOV   A,R1
	CPL   A
	INC   A
	CALL  SHRi
CHK_CA1:CLR   EA
	MOV   R2,TIME
	MOV   R3,TIME+1
	SETB  EA
	CALL  SUBi	      ; R45 = T_chk - TIME
	JNC   CHK_CA2
	CALL  NEGi	      ; R45 = abs(R45)
	MOV   A,SPRTYP
	JNB   ACC.7,CHK_CA2
	MOV   A,#1	      ; Pro 50 ml strikacku
	CALL  SHRi	      ; povolit 2x vetsi zpozdeni
CHK_CA2:
    %IF(0)THEN(
	MOV   R1,#NOT 3
    )ELSE(
	MOV   A,#1	      ; Novejsi reseni pro 0.1ml/h
	CALL  SHRi
	MOV   R1,#NOT 1
    )FI
	MOV   A,FLOW+1
	JNZ   CHK_CA4
	MOV   A,FLOW
	JZ    CHK_CA4
CHK_CA3:RLC   A
	JC    CHK_CA4
	XCH   A,R1
	RLC   A
	XCH   A,R1
	SJMP  CHK_CA3
CHK_CA4:MOV   A,R1
	ANL   A,R4
	ORL   A,R5
	JZ    CHK_CAR
    %IF(0)THEN(
	MOV   R1,#5+15-1
    )ELSE(
	MOV   R1,#5+15-1+1    ; Novejsi reseni pro 0.1ml/h
    )FI
	CALL  DIVihf
	JB    F0,CHK_CA5
	MOV   A,R1
	JB    ACC.7,CHK_CAR
    %IF(%POT_FILT_FL)THEN(
	JBC   FL_POTC,CHK_CAE
    )FI
CHK_CA5:MOV   A,#ERFLTS
	CALL  SET_ERR
CHK_CAR:SETB  FL_POTC
CHK_CAE:RET
CHK_CAZ:CLR   A
	MOV   R4,A
	MOV   R5,A
	SJMP  CHK_CA1

; Rotuje R4567 tak ze se cislo zkrati na R45 pocet posunu ulozi do R1

NORMMI: MOV   R1,#0
NORMMI1:MOV   A,R7
	JZ    NORMMI2
	CLR   A
	XCH   A,R7
	XCH   A,R6
	XCH   A,R5
	XCH   A,R4
	MOV   A,R1
	ADD   A,#8
	MOV   R1,A
NORMMI2:MOV   A,R6
	JZ    NORMMIR
	CLR   C
	MOV   A,R6
	RRC   A
	MOV   R6,A
	MOV   A,R5
	RRC   A
	MOV   R5,A
	MOV   A,R4
	RRC   A
	MOV   R4,A
	INC   R1
	SJMP  NORMMI2
NORMMIR:RET

; Naplni R23 konstantou z SPR_PAR podle SPRTYP a offsetu v ACC

G_SPR:  MOV   R2,A
	MOV   A,SPRTYP
	JB    ACC.3,G_SPRE
G_SPR0:	JNB   ACC.7,G_SPR1
	SWAP  A
G_SPR1: ANL   A,#0FH
	RL    A
	RL    A
	RL    A
	ADD   A,R2
	MOV   R2,A
	ADD   A,#SPR_PAR-G_SPR2
	MOVC  A,@A+PC
G_SPR2: XCH   A,R2
	ADD   A,#SPR_PAR-G_SPR3+1
	MOVC  A,@A+PC
G_SPR3: MOV   R3,A
	MOV   A,#1
	RET

G_SPRE: CLR   A
	MOV   R2,A
	MOV   R3,A
	RET

; Tabulka parametru strikacek

O_SPRV  SET   0   ; Objem strikacky v 0.1 ml
O_SPRL  SET   2   ; Delka strikacky v 256 irc
O_SPRE  SET   4   ; Konec davkovani v 256 irc
O_SPRT  SET   6   ; 2 znaky nazvu stikacky

SPR_PAR:%W    (200)           ; 0 - CHIRANA 20
	%W    (2273)
	%W    (140)
	DB    lcda+lcdd+lcde+lcdf
	DB    lcdc+lcde+lcdf+lcdg

	%W    (200)           ; 1 - TERUMO 20
	%W    (2252)
	%W    (204)
	DB    lcdd+lcde+lcdf+lcdg
	DB    lcda+lcdd+lcde+lcdf+lcdg

	%W    (200)           ; 2 - BRAUN 20
	%W    (2273) ; 2248
	%W    (15)
	DB    lcdc+lcdd+lcde+lcdf+lcdg
	DB    lcde+lcdg

	%W    (200)           ; 3
	%W    (3403)
	%W    (50)
	DB    lcda+lcdb+lcdg+lcde+lcdd
	DB    lcda+lcdb+lcdg+lcdc+lcdd

	%W    (200)           ; 4
	%W    (3404)
	%W    (50)
	DB    lcda+lcdb+lcdg+lcde+lcdd
	DB    lcdf+lcdg+lcdb+lcdc

	%W    (200)           ; 5
	%W    (3405)
	%W    (50)
	DB    lcda+lcdb+lcdg+lcde+lcdd
	DB    lcda+lcdf+lcdg+lcdc+lcdd

	%W    (200)           ; 6
	%W    (3406)
	%W    (50)
	DB    lcda+lcdb+lcdg+lcde+lcdd
	DB    lcda+lcdf+lcdg+lcde+lcdd+lcdc

	%W    (200)           ; 7
	%W    (3407)
	%W    (50)
	DB    lcda+lcdb+lcdg+lcde+lcdd
	DB    lcda+lcdb+lcdc

	%W    (500)           ; 8 - CHIRANA 50
	%W    (2894)
	%W    (247)
	DB    lcda+lcdd+lcde+lcdf
	DB    lcdc+lcde+lcdf+lcdg

	%W    (500)           ; 9 - TERUMO 50
	%W    (2669)
	%W    (281)
	DB    lcdd+lcde+lcdf+lcdg
	DB    lcda+lcdd+lcde+lcdf+lcdg

	%W    (500)           ; A - BRAUN 50
	%W    (2894) ; 2966
	%W    (220)
	DB    lcdc+lcdd+lcde+lcdf+lcdg
	DB    lcde+lcdg

	%W    (500)           ; B - BRAUN 50 stary
	%W    (2894)
	%W    (237)
	DB    lcda+lcdf+lcdg+lcdc+lcdd
	DB    lcda+lcdb+lcdg+lcdc+lcdd

	%W    (500)           ; C
	%W    (2894)
	%W    (237)
	DB    lcda+lcdf+lcdg+lcdc+lcdd
	DB    lcdf+lcdg+lcdb+lcdc

	%W    (500)           ; D
	%W    (2894)
	%W    (237)
	DB    lcda+lcdf+lcdg+lcdc+lcdd
	DB    lcda+lcdf+lcdg+lcdc+lcdd

	%W    (500)           ; E
	%W    (2894)
	%W    (237)
	DB    lcda+lcdf+lcdg+lcdc+lcdd
	DB    lcda+lcdf+lcdg+lcde+lcdd+lcdc

	%W    (500)           ; F
	%W    (2894)
	%W    (237)
	DB    lcda+lcdf+lcdg+lcdc+lcdd
	DB    lcda+lcdb+lcdc

;=================================================================
; Preruseni z meridla polohy

I_CAP0: SETB  P4.0            ; Zastaveni delice vstupu polohy
	CLR   CTI0
    %IF(NOT %POS_POT_FL)THEN(
	MOV   CAP_ACT,CTL0
	MOV   CAP_ACT+1,CTH0
	CLR   FL_CAPA
    )FI
	RETI

;=================================================================
; Zborceni programu

ERR_HL1:
ERR_HLT:CLR   EA
	SETB  ELED
	SETB  MOTOFF
	SETB  BATTST
	CLR   AA              ; Neprijima se po IIC
	CLR   SI              ; Uvolneni IIC z prijmu
	SETB  STO             ; Uvolneni IIC z vysilani
	CLR   EA
	%WATCHDOG
	JMP   ERR_HLT

;=================================================================
; Casove preruseni

USING   2

I_TIME: PUSH  ACC
	PUSH  PSW
	PUSH  B
	MOV   PSW,#AR0
	MOV   A,SP            ; Test stavu zasobniku
	CLR   C
	SUBB  A,#STACK
	JC    ERR_HLT
	SUBB  A,#STACK_S-4
	JNC   ERR_HLT
	CLR   TF0
	CLR   C
	CLR   TR0
S_T1_D  SET   7               ; zpozdeni rutiny
	MOV   A,TL0           ; 7 Cyklu
	SUBB  A,#LOW (M_TIMED-S_T1_D)
	MOV   TL0,A
	MOV   A,TH0
	SUBB  A,#HIGH (M_TIMED-S_T1_D)
	MOV   TH0,A
	SETB  TR0
	JMP   A_SYS

MOVsi:  MOV   A,@R1           ; @R0:=@R1 .. delka R2
	MOV   @R0,A
	INC   R0
	INC   R1
	DJNZ  R2,MOVsi
	RET

MOVsci: CLR   A        	      ; @R0:=@DPTR .. delka R2
	MOVC  A,@A+DPTR
	MOV   @R0,A
	INC   R0
	INC   DPTR
	DJNZ  R2,MOVsci
	RET

CMP_SN: MOV   DPTR,#SER_NUM
	MOV   R2,#4
CMPsci: CLR   A        	      ; @R0 ?= @DPTR .. delka R2
	MOVC  A,@A+DPTR       ; vrati ACC=0 pri rovnosti
	XRL   A,@R0
	JNZ   CMPsciR
	INC   R0
	INC   DPTR
	DJNZ  R2,CMPsci
CMPsciR:RET

;=================================================================
; Aquission subsystem

A_SYS:  MOV   A,TIMR_A
	JZ    A_SYS01
	DEC   TIMR_A
A_SYS01:MOV   A,A_CPOS
	JNZ   A_SYS10
A_SYS_R:JMP   I_TIME1
A_SYS10:ANL   A,#07FH
	JNZ   A_SYS20
	MOV   A,TIMR_A
	JNZ   A_SYS_R
	MOV   TIMR_A,A_PER
	MOV   A_CPOS,#80H-1
	MOV   R0,#A_BUF
	MOV   @R0,S1ADR
A_SYS17:INC   R0
A_SYS18:MOV   A_OPOS,R0
A_SYS19:INC   A_CPOS
A_SYS20:MOV   R0,A_OPOS
	MOV   A,A_CPOS
	JNB   ACC.4,A_SYS30
A_SYS21:MOV   R4,#A_BUF       ; Pocatek bufferu
	MOV   R5,#0
	MOV   A,R0
	CLR   C
	SUBB  A,R4
	MOV   R2,A            ; Delka = A_OPOS-A_BUF
	MOV   R3,#0
	MOV   R6,#10H         ; Na slave 10H
	CALL  IIC_RQI         ; Odesli zpravu
	JNZ   A_SYS_R         ; Nepovedlo se
	MOV   A_CPOS,#80H     ; Povedlo se cekej na dalsi TIMR_A=0
	SJMP  A_SYS_R

A_SYS30:MOV   R7,A
	MOV   R4,A_MSK
	MOV   R5,A_MSK+1
	CALL  TSTBR45
	JZ    A_SYS19
	CJNE  R7,#80H,A_SYS31
	MOV   R1,#CAP_ACT     ; Vyslani polohy z kapacitniho cidla
	MOV   R2,#2
	CALL  MOVsi
	CLR   A
	MOV   @R0,A
	INC   R0
	JMP   A_SYS18
A_SYS31:
A_SYS40:MOV   A,R7
	JNB   ACC.3,A_SYS18
	XRL   A,ADCON         ; Vysilnani prevodniku
	ANL   A,#1FH
	CJNE  A,#18H,A_SYS_R  ; Neni ukoncen prevod daneho ADC
	MOV   A,ADCON         ; ADCI=1 a ADCS=0
	CLR   ACC.4
	MOV   @R0,A
	INC   R0
	MOV   ADCON,A
	MOV   @R0,ADCH
	JMP   A_SYS17

TSTBR45:JB    ACC.3,TSTBR5A   ; Testuje bit A registru R45 vysledek v A
TSTBR4A:ANL   A,#7            ; Testuje bit A registru R4 vysledek v A
	ADD   A,#TSTBR_t-TSTBR4t
	MOVC  A,@A+PC
TSTBR4t:ANL   A,R4
	RET
TSTBR5A:ANL   A,#7            ; Testuje bit A registru R5 vysledek v A
	ADD   A,#TSTBR_t-TSTBR5t
	MOVC  A,@A+PC
TSTBR5t:ANL   A,R5
	RET

TSTBR_t:DB    001h
	DB    002h
	DB    004h
	DB    008h
	DB    010h
	DB    020h
	DB    040h
	DB    080h

;=================================================================
; Prime cteni prevodniku pri pocatecnich testech

GET_ADN:MOV   AD_CNT,#0
	MOV   C,EA
	CALL  AD_PREC
	MOV   EA,C
	JB    B.3,GET_ADN
GET_AD1:MOV   B,ADCON
	JB    B.3,GET_AD1
	MOV   R5,ADCH
	ANL   A,#7
	MOV   ADCON,A
	MOV   A,B
	ANL   A,#0C0H
	MOV   R4,A
	RET

;=================================================================
; Prubezne cteni dat z prevodniku
;
RSEG    IC____B
AD_CNT: DS    1
AD_EN   BIT   AD_CNT.7
AD_ST   BIT   AD_CNT.6

RSEG    IC____I
AD_DCIN:DS    2
AD_MCUR:DS    2
AD_BAT: DS    2

RSEG    IC____C

AD_PREP:JNB   AD_EN,AD_PRER   ; Neni povolene prevadeni
	JB    AD_ST,AD_PRER
	MOV   A,AD_CNT
	ANL   A,#0FH
	ADD   A,#AD_TAB-AD_PRE1
	MOVC  A,@A+PC
AD_PRE1:JZ    AD_PRER
	JB    ACC.7,AD_PRE2
AD_PREC:MOV   B,ADCON
	JB    B.3,AD_PRER
	ANL   A,#07H
	MOV   ADCON,A
	SETB  ACC.3
	MOV   ADCON,A
	SETB  AD_ST
AD_PRER:RET

AD_PRE2:MOV   R7,A            ; Specialni funkce
	MOV   A,TIMRI
	DEC   A
	JZ    AD_PRE4
	INC   AD_CNT
	INC   AD_CNT
AD_PRE3:INC   AD_CNT
	INC   AD_CNT
	RET

AD_PRE4:CLR   BATTST
	MOV   A,R7
	JB    ACC.6,AD_PREC
	SJMP  AD_PRE3

AD_RD:  JNB   AD_EN,AD_RDR1
	JNB   AD_ST,AD_RDR1
	MOV   B,ADCON
	JB    B.3,AD_RDR
	MOV   A,AD_CNT
	ANL   A,#0FH
	ADD   A,#AD_TAB-AD_RD1
	MOVC  A,@A+PC
AD_RD1:	MOV   R1,A
	JZ    AD_RDR1
	JNB   ACC.6,AD_RD2
			      ; Specialni funkce
	SETB  BATTST
AD_RD2: XRL   A,B
	ANL   A,#7
	JNZ   AD_RDR1
	INC   AD_CNT
	MOV   A,AD_CNT
	ANL   A,#0FH
	ADD   A,#AD_TAB-AD_RD3
	MOVC  A,@A+PC
AD_RD3: INC   AD_CNT
	MOV   R0,A
	ANL   B,#7
	MOV   ADCON,B
	CJNE  R1,#15H,AD_RD5
	MOV   A,ADCH
	JB    FL_MOT,AD_RDR1
	ANL   A,#0FEH
	MOV   C,FL_TRER
	CLR   FL_TRER
	JZ    AD_RDR1
	SETB  FL_TRER
	JNC   AD_RDR1
	MOV   A,#ERTRHI
	CALL  SET_ERR
	SJMP  AD_RDR1
AD_RD5:	
    %IF(%POS_POT_FL)THEN(
	CJNE  R1,#023H,AD_RD6
	CLR   FL_CAPA
      %IF(%POT_FILT_FL)THEN(
	SJMP  AD_RDPOT
      )FI
      %IF(0)THEN(
	MOV   R3,#0C0H
	SJMP  AD_RD7
      )FI
    )FI
AD_RD6:	MOV   R3,#0E0H
AD_RD7:	MOV   R2,#0
	CALL  iLDR45i
	CALL  MULi
	MOV   A,R6
	MOV   R4,A
	MOV   A,R7
	MOV   R5,A
	MOV   A,ADCON
	RL    A
	RL    A
	RL    A
	ANL   A,#6
	MOV   R2,A
	MOV   A,ADCH
	RL    A
	RL    A
	RL    A
	MOV   R3,A
	ANL   A,#7
	XCH   A,R3
	ANL   A,#NOT 7
	ORL   A,R2
	MOV   R2,A
	CALL  ADDi
	CALL  iSVR45i
AD_RDR1:CLR   AD_ST
AD_RDR:	RET

AD_TAB: DB    007H,AD_DCIN
    %IF(%POS_POT_FL)THEN(
	DB    023H,CAP_ACT
    )FI
	DB    002H,AD_MCUR
	DB    080H,0
	DB    0C4H,AD_BAT
	DB    015H,0
    %IF(%POS_POT_FL)THEN(
	DB    023H,CAP_ACT
    )FI
	DB    0

%IF(%POT_FILT_FL)THEN(
; Mereni potenciomentru s plovoucim prumerem ze 4 vzorku

RSEG    IC____I
FLT_POT:DS    6

RSEG    IC____C

AD_RDPOT:
	MOV   A,ADCON
	SWAP  A
	ANL   A,#0CH
	MOV   R2,A
	MOV   A,ADCH
	SWAP  A
	MOV   R3,A
	ANL   A,#0FH
	MOV   R5,A
	XCH   A,R3
	ANL   A,#NOT 0FH
	ORL   A,R2
	MOV   R4,A
	MOV   R2,A
	MOV   R1,#FLT_POT
	MOV   R7,#3
AD_RDP1:MOV   A,R2
	XCH   A,@R1
	MOV   R2,A
	ADD   A,R4
	MOV   R4,A
	INC   R1
	MOV   A,R3
	XCH   A,@R1
	MOV   R3,A
	ADDC  A,R5
	MOV   R5,A
	INC   R1
	DJNZ  R7,AD_RDP1
	MOV   C,EA
	CLR   EA
	MOV   CAP_ACT,R4
	MOV   CAP_ACT+1,R5
	MOV   EA,C
	CLR   AD_ST
	RET

)FI


;=================================================================
; Casove preruseni

I_TIME1:CALL  AD_PREP         ; Start ADC pro vnitrni pouziti

A_SYSP:	MOV   A,A_CPOS
	JNB   ACC.3,A_SYSP1   ; Neni potreba pripravit data v prevodniku
	MOV   B,ADCON
	JB    B.3,A_SYSP1     ; ADCS=1 .. prevodnik zamestnan
	ANL   A,#07H
	MOV   ADCON,A
	SETB  ACC.3           ; Odstartovani ADC
	MOV   ADCON,A
A_SYSP1:

	CALL  AD_RD
	DJNZ  PPTIMR,I_TIM15  ; Preruseni s frekvenci 450 Hz
	INC   PPTIMR          ; Chyba Ping-Pongu
	MOV   A,#ERCOMMT
	CALL  SET_ERR

I_TIM15:JNB   T2OV,I_TIM21    ; Test behu CAP cidla
	CLR   T2OV
    %IF(%POS_POT_FL)THEN(
      %IF(1)THEN(
	CLR   EA	      ; Chyba pokud <0080H
	MOV   A,CAP_ACT	      ;       nebo >=3F80H
	RLC   A
	MOV   A,CAP_ACT+1
	SETB  EA
	RLC   A
       %IF(NOT %HALRS_FL)THEN(
	JZ    I_TIM19
       )FI
	ADD   A,#-7FH
	JC    I_TIM19
      )ELSE(
	CLR   EA	      ; Chyba pokud <0080H
	MOV   A,CAP_ACT	      ;       nebo >=7F80H
	ADD   A,#080H
	MOV   A,CAP_ACT+1
	SETB  EA
	ADDC  A,#0
	ANL   A,#7FH
	JZ    I_TIM19
      )FI
    )FI
	JNB   FL_CAPA,I_TIM20
I_TIM19:MOV   A,#ERCAPNP
	CALL  SET_ERR
I_TIM20:SETB  FL_CAPA
I_TIM21:MOV   A,ERRNUM        ; Rizeni horniho tanzistoru
	JZ    I_TIM22
	SETB  ELED
	SETB  C
	SETB  MOTOFF
	CLR   FL_MOT
	SJMP  I_TIM23
I_TIM22:MOV   C,FL_MOT
	CPL   C
	MOV   MOTOFF,C
	SETB  C               ; Generovani pipnuti
	DJNZ  BPTIMR,I_TIM23
	CLR   C
	INC   BPTIMR
I_TIM23:MOV   BEEPB,C
    %IF(%HALRS_FL)THEN(       ; Kontrolni HAL senzor
	JB    HRS_PIN,I_TIM26
	JNB   FL_HRLV,I_TIM28
	JBC   FL_HRCH,I_TIM29
	CLR   FL_HRLV         ; Detekovana sestupna hrana
	SJMP  I_TIM28
I_TIM26:JB    FL_HRLV,I_TIM28
	JBC   FL_HRCH,I_TIM29
	SETB  FL_HRLV         ; Detekovana vzestupna hrana
	SETB  FL_HRCK
	MOV   A,HRS_CNT
	DEC   HRS_CNT
	JNZ   I_TIM27
	DEC   HRS_CNT+1
I_TIM27:ANL   A,#01FH	      ; Priznak provedeni korekce
	JNZ   I_TIM28	      ; po 32 impulzech
	SETB  FL_HRCP
I_TIM28:SETB  FL_HRCH
I_TIM29:
    )FI
	DEC   TIME_CN
	JNB   TIME_CN.7,ITIMEQ
	ANL   AD_CNT,#0C0H    ; Nove odstartovani cteni ADC
	MOV   A,#C_TIMED      ; Pruchod s frekvenci 25Hz
	ADD   A,TIME_CN
	MOV   TIME_CN,A

	MOV   PSW,#AR0
	MOV   R0,#TIMR1
	MOV   B,#N_OF_T
I_TIME2:MOV   A,@R0
	JZ    I_TIME3
	DEC   A
	MOV   @R0,A
I_TIME3:INC   R0
	DJNZ  B,I_TIME2
	MOV   A,TIMRI
	JNZ   I_TIMR1
	SETB  FL_EDRF         ; Pruchod s periodou 0.01 min
	CALL  uL_STR
	MOV   TIMRI,#C_TIM06
	DEC   TIM_DIF
	MOV   A,TIM_DIF
	ADD   A,#10
	ADD   A,#-20
	JNC   I_TIM32
	MOV   A,#ERTIME
	CALL  SET_ERR
I_TIM32:JNB   FL_BEWA,I_TIM33
	CLR   FL_BEWA
	MOV   A,TIMRJ         ; Generovani varovnych pipu
	ANL   A,#003H
	JNZ   I_TIM33
	MOV   A,BPTIMR
	ADD   A,#BP_WA
	MOV   BPTIMR,A
I_TIM33:INC   TIMRJ
	MOV   A,TIMRJ
	ADD   A,#-C_TMIN
	JNC   I_TIMR1
	MOV   TIMRJ,#0        ; Pruchod s periodou 1 min
	MOV   A,TIM_DIF
	DEC   A
	JNB   ACC.7,I_TIM51
	INC   A
	INC   A
I_TIM51:MOV   TIM_DIF,A
	INC   TIME
	MOV   A,TIME
	JNZ   I_TIM52
	INC   TIME+1
I_TIM52:

I_TIMR1:MOV   R7,#C_TIMED
ITIMEQ: %WATCHDOG
	POP   B
	POP   PSW
	POP   ACC
	RETI

;=================================================================
; Ovladani displeje pres 8577

DispLen EQU     4
DispAdr EQU     74H

; Definice segmentu
lcda    SET     02h             ;lcd segment a
lcdb    SET     01h             ;lcd segment b
lcdc    SET     04h             ;lcd segment c
lcdd    SET     10h             ;lcd segment d
lcde    SET     40h             ;lcd segment e
lcdf    SET     08h             ;lcd segment f
lcdg    SET     20h             ;lcd segment g
lcdh    SET     80h             ;lcd segment h colon

RSEG    IC____I

WR_BUF: DS    DispLen+1

RSEG    IC____C

; Vypsani WR_BUF po IIC na DISPLAY
OUT_BUF:MOV   A,R6
	ANL   A,#0F8H
	MOV   R0,#WR_BUF
	MOV   @R0,A
        MOV   R6,#DispAdr
	MOV   R4,#WR_BUF
        MOV   R2,#DispLen+1
	MOV   R3,#0
	JMP   IIC_RQI

;Vystup cisla na LCD display pres prevod LCD_TBL a LCD_POS
;Vstup: R45   vypisovane cislo
;       R7    .. format vystupu cisla
;                  SLLLxADD
;            S   - signed
;            A   - znamenko nebo cislice
;                  jinak znamenko nebo blank
;            LLL - delka vypisu > 0
;            DD  - pocet desetinych mist
;       R6    .. pozice vypisu

iPRTLi: MOV   A,R5
	MOV   C,ACC.7
        MOV   A,R7
        JNB   ACC.7,iPRTLi3
	JC    iPRTLi1
	JB    ACC.2,iPRTLi3
	MOV   A,#lcdBlnk
        JNC   iPRTLi2
iPRTLi1:CALL  NEGi
        MOV   A,#lcdSig
iPRTLi2:CALL  iPRTLc
	MOV   A,R7
	ADD   A,#-10H
	MOV   R7,A
iPRTLi3:MOV   A,R7
	SWAP  A
	ANL   A,#7
	MOV   R1,A
iPRTLi9:DEC   R1
	MOV   A,R1
	RL    A
	CPL   A
	ADD   A,#O10E4i-1+5*2
	MOV   R0,A
	CALL  rLDR23i
iPRTL10:MOV   R0,#-1
iPRTL11:CJNE  R0,#9,iPRTL12
iPRTLiE:MOV   A,#lcd_Err
	CALL  iPRTLc
	MOV   A,R7
	ANL   A,#70H
	ADD   A,#-10H
	MOV   R7,A
	JNZ   iPRTLiE
	RET
iPRTL12:CALL  SUBi
	INC   R0
	JNC   iPRTL11
	CALL  ADDi
	MOV   A,R7
	ANL   A,#3
	DEC   A
	XRL   A,R1
	JZ    iPRTL13
	CLR   C
iPRTL13:MOV   A,R0
	MOV   ACC.7,C
	CALL  iPRTLc
	MOV   A,R7
	CLR   ACC.7
	ADD   A,#-10H
	MOV   R7,A
	ANL   A,#70H
	JNZ   iPRTLi9
	RET

; Rotace vlevo o 4 bity

RL4R45: MOV   A,R4
	SWAP  A
	MOV   R4,A
	ANL   A,#0F0H
	XCH   A,R4
	ANL   A,#00FH
	XCH   A,R5
	SWAP  A
	XCH   A,R4
	XRL   A,R4
	XCH   A,R4
	ANL   A,#0F0H
	XCH   A,R5
	ORL   A,R5
	XCH   A,R5
	XRL   A,R4
	MOV   R4,A
	RET

; Vystup hexa
;Vstup: R45   vypisovane cislo
;       R6    .. pozice vypisu
iPRTLhw:MOV   R7,#4
iPRTLh: MOV   A,R5
	SWAP  A
	ANL   A,#00FH
	CALL  iPRTLc
	CALL  RL4R45
	DJNZ  R7,iPRTLh
	RET

iPRTLc: MOV     R0,#0
	JNB     ACC.7,iPRTLc1
	CLR     ACC.7
	MOV     R0,#lcdH
iPRTLc1:ADD     A,#LCD_TBL-iPRTLck
	MOVC    A,@A+PC
iPRTLck:ORL     A,R0
	MOV     R0,A
	MOV     A,R6
	INC     R6
	ANL     A,#07H
	ADD     A,#LCD_POS-iPRTLcl
	MOVC    A,@A+PC
iPRTLcl:ADD     A,#WR_BUF+1
	XCH     A,R0
	MOV     @R0,A
	RET

lcdBlnk SET     10H
lcdSig  SET     17h
lcd_Err SET     17h ; = '-' dalsi moznost 0Eh = 'E'

LCD_POS:DB      3
	DB      2
	DB      0
	DB      1

LCD_TBL:db      lcda+lcdb+lcdc+lcdd+lcde+lcdf           ;0
	db      lcdb+lcdc                               ;1
	db      lcda+lcdb+lcdg+lcde+lcdd                ;2
	db      lcda+lcdb+lcdg+lcdc+lcdd                ;3
	db      lcdf+lcdg+lcdb+lcdc                     ;4
	db      lcda+lcdf+lcdg+lcdc+lcdd                ;5
	db      lcda+lcdf+lcdg+lcde+lcdd+lcdc           ;6
	db      lcda+lcdb+lcdc                          ;7
	db      lcda+lcdb+lcdc+lcdd+lcde+lcdf+lcdg      ;8
	db      lcda+lcdb+lcdf+lcdg+lcdc+lcdd           ;9
	db      lcda+lcdb+lcdf+lcdg+lcdc+lcde           ;A
	db      lcdc+lcdd+lcde+lcdf+lcdg                ;b
	db      lcda+lcdd+lcde+lcdf                     ;C
	db      lcde+lcdg+lcdd+lcdc+lcdb                ;d
	db      lcda+lcdd+lcde+lcdf+lcdg                ;E
	db      lcda+lcde+lcdf+lcdg                     ;F
	db      0                                       ;blank
	db      lcda                                    ;segment a
	db      lcdb                                    ;segment b
	db      lcdc                                    ;segment c
	db      lcdd                                    ;segment d
	db      lcde                                    ;segment e
	db      lcdf                                    ;segment f
	db      lcdg                                    ;segment g
	db      lcdb+lcdc+lcde+lcdf+lcdg                ;H
	db      lcdd+lcde+lcdf                          ;L
	db      lcda+lcdb+lcdf+lcdg                     ;degree
	db      lcdc+lcdd+lcde+lcdg                     ;lower degree
	db      0                                       ;spare
	db      0                                       ;spare
	db      0                                       ;spare
	db      0                                       ;spare
	db      lcdh                                    ;colon

;=================================================================
; Cteni a zapis do pameti EEPROM 8582

EEPRAdr EQU   0A0H ; IIC adresa pameti EEPROM

; Nacte do bufferu IIC_OUT paket dat z adresy R0 v EEPROM

EEPA_RD:JB    F0,EEPA_RR
	MOV   IIC_OUT,R0
EEPA_R1:CALL  IIC_CER
	MOV   R2,#1
	MOV   R3,#7
	MOV   R4,#IIC_OUT
	MOV   R6,#EEPRAdr
	CALL  IIC_RQI
	JNZ   EEPA_R1
	CALL  IIC_WME
	JNZ   EEPA_RE
	MOV   R0,#IIC_OUT
	MOV   R2,#7
	CALL  XOR_SUM
	XRL   A,@R0
	JZ    EEPA_RR
EEPA_RE:SETB  F0
	SETB  FL_EREE
EEPA_RR:RET

; Nacte do bufferu IIC_OUT paket dat z adresy R0 v EEPROM

EEPA_WR:JB    F0,EEPA_WQ
	MOV   IIC_OUT,R0
	MOV   R0,#IIC_OUT
	MOV   R2,#7
	CALL  XOR_SUM
	MOV   @R0,A
EEPA_W1:CALL  IIC_CER
	MOV   R2,#8
	MOV   R3,#0
	MOV   R6,#EEPRAdr
	MOV   R4,#IIC_OUT
	CALL  IIC_RQI
	JNZ   EEPA_W1
	CALL  IIC_WME
	JZ    EEPA_WQ
EEPA_WE:SETB  F0
	SETB  FL_EREE
EEPA_WQ:RET

;=================================================================
; Vypis na LCD pro ladeni

PRTR23:	MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
PRTR45:	MOV   R7,#0C4H
	MOV   R6,#18H
	CALL  iPRTLhw
;	CALL  iPRTLi
	MOV   R6,#18H
	CALL  OUT_BUF
	RET

;=================================================================
; System dynamicke adresace a vysilani statusu

RSEG    IC____B

uL_DYFL:DS    1
uLD_RQA	BIT   uL_DYFL.2       ; Pozadavek na pripojeni do site

RSEG    IC____D

uL_DYSA:DS    1

RSEG    IC____C

; Rutina vyslani zadosti o prideleni dynamicke adresy

SNRQ:   JNB   uLD_RQA,SNRQ01
	MOV   R0,#BEG_OB
	MOV   A,@R0
	JZ    SNRQ02
SNRQ01:	RET
SNRQ02: DEC   uL_DYFL
	MOV   @R0,uL_DYSA
	INC   R0
	MOV   @R0,#07FH
	INC   R0
	INC   R0
	MOV   @R0,#0C0H
	INC   R0
	MOV   @R0,uL_ADR
	MOV   R2,#4
	MOV   DPTR,#SER_NUM
	CALL  MOVsci

CL_OB:  MOV   A,R0
	MOV   R0,#BEG_OB+2
	MOV   @R0,A
	MOV   R0,#BEG_OB
	MOV   A,@R0
	ORL   A,#080H
	MOV   @R0,A
	SETB  uLF_RS
	JNB   uLF_NB,CL_OB9
	JNB   ES,CL_OB9
	SETB  TI
CL_OB9: RET

; Rutina vysilani statusu z davkovace  CMD=0C1H

uL_SNST:MOV   R1,#BEG_PB
	MOV   A,@R1
	JNZ   SNSTA10
	JB    uLD_RQA,SNSTA03 ; Snaha o zviditelneni
	INC   uL_DYFL
	JNB   uLD_RQA,SNSTA03
SNSTA02:ORL   uL_DYFL,#7
	MOV   uL_DYSA,uL_SA   ; Server dynamickych adres
	MOV   uL_ADR,#0
SNSTA03:JMP   SNSTAR

SNSTA10:MOV   R7,A
	ANL   A,#0F0H
	CJNE  A,#010H,SNSTA03
	MOV   A,R6            ; Prikaz cteni udaju
	CLR   C
	SUBB  A,#BEG_PB+1+4   ; Neni test serioveho cisla
	JC    SNSTA13
	INC   R1
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#SER_NUM   ; Kontrola serioveho cisla
	MOV   R2,#4
SNSTA11:CLR   A
	MOVC  A,@A+DPTR
	XRL   A,@R1
	JNZ   SNSTA12
	INC   R1
	INC   DPTR
	DJNZ  R2,SNSTA11
SNSTA12:POP   DPH
	POP   DPL
	JNZ   SNSTA02         ; Nesouhlasi cislo
SNSTA13:CALL  ACK_CMD
	ANL   uL_DYFL,#NOT 7
SNSTA20:CALL  SND_BEB
	PUSH  DPL
	PUSH  DPH
	MOV   R2,#4           ; Vysli seriove cislo
	MOV   DPTR,#SER_NUM
SNSTA21:CLR   A
	MOVC  A,@A+DPTR
	CALL  SND_CHC
	INC   DPTR
	DJNZ  R2,SNSTA21
	POP   DPH
	POP   DPL
	CJNE  R7,#010H,SNSTA30
	MOV   A,MOD_II        ; Vyslani zakladnich udaju
	CALL  SND_CHC
	MOV   A,ERRNUM
	CALL  SND_CHC
	MOV   R0,#FLOW
	CALL  SND_IDi
	MOV   R0,#MAX_TIM
	MOV   R2,#6
SNSTA25:CALL  SND_IDi
	DJNZ  R2,SNSTA25
	MOV   A,SPRTYP
	CALL  SND_CHC
	MOV   R0,#VOL_IN
	CALL  SND_IDi
	SJMP  SNSTA50
SNSTA30:CJNE  R7,#011H,SNSTA50
	MOV   R0,#CAP_ACT     ; Vyslani servisnich udaju
	CALL  SND_IDi
	MOV   R0,#IRC_ACT
	CALL  SND_IDi
	MOV   R2,#3
	MOV   R0,#AD_DCIN
SNSTA35:CALL  SND_IDi
	DJNZ  R2,SNSTA35
	MOV   A,SPRTYP
	CALL  SND_CHC
	MOV   R0,#VOL_IN
	CALL  SND_IDi
%IF(%HALRS_FL)THEN(
	MOV   R0,#HRS_CNT
	CALL  SND_IDi
)FI	
	SJMP  SNSTA50

SNSTA50:CALL  SND_END
SNSTAR: JMP   S_WAITD
	JMP   NAK_CMD

SND_IDi:MOV   A,@R0
	MOV   R4,A
	INC   R0
	MOV   A,@R0
	MOV   R5,A
	INC   R0
SNDR45i:MOV   A,R4
	CALL  SND_CHC
	MOV   A,R5
	JMP   SND_CHC

	END