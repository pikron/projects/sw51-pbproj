$NOMOD51
;********************************************************************
;*                    ID 2050 - II.ASM                              *
;*     Hlavni modul software pro procesor klavesnice a rizeni motoru*
;*                  Stav ke dni 05.01.2001                          *
;*                      (C) Pisoft 1994                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$INCLUDE(REG552.H)
$INCLUDE(II_AI.H)
$INCLUDE(II_MSG.H)
$INCLUDE(II_IIC.H)

%DEFINE  (DEBUG_FL)    (0)    ; Povoleni vlozeni debug rutin
%DEFINE  (IPC_SOFT_FL) (0)    ; Software pro IPC
%DEFINE  (NEW_LED_FL)  (1)    ; Software pro IPC
%DEFINE  (PARSAVE_FL)  (1)    ; Ukladani poslednich parametru

%IF (%IPC_SOFT_FL) THEN (
CMAX_FL   EQU   9999 	      ; Max nastavitelny prutok v 0.1 ml/h
SOFT_VER  EQU	94H           ; Kompatabilita verze pro IPC

)ELSE(
CMAX_FL   EQU   1500 	      ; Max nastavitelny prutok v 0.1 ml/h
SOFT_VER  EQU	14H           ; Kompatabilita verze pro IP
)FI

EXTRN   CODE(SCANKEY,TESTKEY)
EXTRN   DATA(KBDFL)
EXTRN   NUMBER(MAX_KEY,VIR_KEY1,VIR_KEY2,VIR_KEY3)

PUBLIC  INPUTC
PUBLIC  KBDTIMR

; Seznam prikazu modulu SLA=20H
;
; Prikaz 1 nastaveni MOD_SLV
;   MOD_SLV=0 testovani vysilani na LCD a slave 10h
;   SLV_MOD=10 az 17h vystup prevodniku na LCD
;   SLV_MOD=18 az 1Fh Posilani dat z ADC na IIC
;   SLV_MOD=1 test cteni IIC_OUT
;   SLV_MOD=2  vypis POZ_ACT na display
;   SLV_MOD=3  vypis POZ_RQ na display
;   SLV_MOD=4  vypis POZ_DEL na display
;   SLV_MOD=5  vypis M_ENERG na display
;   SLV_MOD=6  vypis M_DFILT na display
;   SLV_MOD=7  vypis SCANKEY na display
;   SLV_MOD=20 interaktivni zadavani konstant
; Prikaz 2 nastaveni LED
; Prikaz 3 zkouska citace casu
; Prikaz 4 nulovani citace polohy
; Prikaz 5 rizeni PWM
; Prikaz 6 motor skok
; Prikaz 7 sputeni motoru rychlosti
; Prikaz 8 nastaveni M_ENERI
; Prikaz 9 nastaveni M_SPDEI
; Prikaz A nastaveni systemu vydavani dat

%DEFINE (WATCHDOG) (
	ORL   PCON,#10H
	MOV   T3,#0E0H
)

%*DEFINE (TOLED (WHAT)) (
	PUSH  ACC
	MOV   A,#%WHAT
	MOV   DPTR,#LED2
	MOVX  @DPTR,A
	POP   ACC
)

%*DEFINE (W (WO)) (
	DB   LOW (%WO),HIGH (%WO)
)

; Definice ridicich signalu
MBRK    BIT   P3.5 ; 0 .. sepnuti brzdy
EBP     BIT   P3.2 ; 0 .. zajistuje spravne rizeni displeje
ELED    BIT   P3.0 ; 1 .. spina led ERROR a vypina motor
TOFF    BIT   P3.1 ; 0 .. spina mereni tenzometru
TS_FDIS BIT   P1.5 ; 1 .. zapoji testovani IRC fazoveho dis.
KLO1    BIT   P4.0 ;      vystup pro testovani FD
LED0    XDATA 0111111111111111B   ; Pravy display
LED1    XDATA 1011111111111111B   ; Levy display
LED2    XDATA 1101111111111111B   ; Errorova skupina

;=============================================================
; Definice pracovnich stavu MOD_SLV

M_SNORM EQU   0C0H          ; Bez 2. displeje
M_STIME EQU   0C1H          ; Zobraz cas
M_SPRES EQU   0C2H          ; Zobraz tlak
M_SVOL  EQU   0C3H          ; Zobraz objem
M_UN_M  EQU   0C4H          ; Maska nestabilnich modu
M_COMRD EQU   0C4H          ; Macteni dat z pocitace
M_LTIME EQU   0C5H          ; Zobraz limit casu
M_LPRES EQU   0C6H          ; Zobraz limit tlaku
M_LVOL  EQU   0C7H          ; Zobraz limit objemu
M_RNORM EQU   0D0H          ; Cerpani bez 2. displeje
M_RTIME EQU   0D1H          ; Cerpani + zobraz cas
M_RPRES EQU   0D2H          ; Cerpani + zobraz tlak
M_RVOL  EQU   0D3H          ; Cerpani + zobraz objem
M_RLTIM EQU   0D5H          ; Cerpani + zobraz limit casu
M_RLPRE EQU   0D6H          ; Cerpani + zobraz limit tlaku
M_RLVOL EQU   0D7H          ; Cerpani + zobraz limit objemu
M_SERV  EQU   0E0H          ; Servisni rezim

NCMXDEL EQU   200           ; Maximalni doba bez prikazu
NCMODEN EQU   10            ; Doba po ktere se vrati z nestabil. mod.
NCLEDOF EQU   200           ; Doba po ktere se zacne blikat LED diod

; Zpravy v OUT_MSG posilane na druhy procesor

OMS_CAP EQU   080H          ; Skupina zprav pro kalibraci CAP cidla
OMS_SCH EQU   090H          ; Zmenil se typ strikacky
OMS_CCH EQU   094H          ; Zmenila se konfigurace
OMS_CER EQU   0AAH          ; Bude se mazat chyba

IC____C SEGMENT CODE
IC____D SEGMENT DATA
IC____I SEGMENT IDATA
IC____B SEGMENT DATA BITADDRESSABLE
STACK   SEGMENT IDATA

RSEG	STACK
STACK_S EQU   40H
	DS    STACK_S

USING   0

CSEG    AT    RESET
	JMP   START

CSEG    AT    TIMER1          ; Realny cas z casovace 1
	JMP   I_TIME

RSEG IC____B

TIME_CN:DS    1    ; Delici citac pro odvozeni 25 Hz

SW_FLG: DS    1               ; Priznaky vyuzite softwarem
FL_EDCH BIT   SW_FLG.7        ; Doslo ke zmene dat editaci
FL_TIME BIT   SW_FLG.6        ; Nastaven casovy limit
FL_PRES BIT   SW_FLG.5        ; Tlakovy limit
FL_VOL  BIT   SW_FLG.4        ; Objemovy limit
FL_BAT  BIT   SW_FLG.3        ; Napajeni z baterie
FL_EDRF BIT   SW_FLG.2        ; Priznak refresovani displeje
FL_VOLA BIT   SW_FLG.1        ; Tato strikacka je posledni
FL_SERV BIT   SW_FLG.0        ; Priznak servisniho rezimu

SM_TIME EQU   40H
SM_PRES EQU   20H
SM_VOL  EQU   10H
SM_LIMS EQU   70H
SM_ATIM EQU   04H ; Pouze pro CL_SPC aktualniho casu
SM_AVOL EQU   01H ; Pouze pro CL_SPC aktualniho objemu

SW1_FLG:DS    1
FL_NRD  BIT   SW1_FLG.7       ; Neni pravy display
FL_SETL	BIT   SW1_FLG.6       ; Prisla data z PC
FL_SERT	BIT   SW1_FLG.5	      ; Pritazeni testu pro FL_SERV
FL_TRTS BIT   SW1_FLG.4       ; Test vykonovych tranzistoru
M_D_256 BIT   SW1_FLG.3       ; Pouzit filtr derivace * 256
FL_EREE BIT   SW1_FLG.2       ; Chyba pri praci s EEPROM
FL_ERRW BIT   SW1_FLG.1       ; Chyba byla jiz zaznamenana do ERR_HIS
FL_HSD  BIT   SW1_FLG.0       ; Podrzeni displeje po urcitou dobu

PP_FLG: DS    1               ; Priznaky pro komunikaci mezi procesory
FL_STRT BIT   PP_FLG.7        ; Probiha davkovani
FL_HSPD BIT   PP_FLG.6        ; Probiha rychloposuv
FL_PPTI BIT   PP_FLG.5        ; Kontrola casove synchronizace
FL_MOT  BIT   PP_FLG.4        ; Spusteni motoru
FL_BALO BIT   PP_FLG.3        ; Blizi se vybiti baterie
FL_NEND BIT   PP_FLG.2        ; Blizi se konec davky
FL_BEER BIT   PP_FLG.1        ; Pipnuti pri chybnem vstupu
FL_BEOK BIT   PP_FLG.0        ; Pipnuti vse OK

CFG_FLG:DS    1               ; Priznaky konfigurace davkovace
FL_PARSV BIT  CFG_FLG.6       ; Ukladani posledni hodnoty parametru
FL_P100 BIT   CFG_FLG.5       ; Zmena standartni tlakove meze
			      ; pro tenzometr z 50 na 100 kPa
FL_USCH BIT   CFG_FLG.4       ; Uzivatel smi menit typ strikacky

PPCNT:  DS    1               ; Ping-Pong counter

RSEG IC____D

PPT_MAX EQU   30   ; Maximalni cas mezi prijmy Ping-Pongu
PPT_SND EQU   3    ; Cas po kterem se zacne vysilat
PPTIMR: DS    1    ; Casovac pro Ping-Pong

M_TIMED EQU   2048; Pocet impulsu na preruseni od motoru
C_TIMED EQU   18   ; Deleni na 25 Hz pro ID s 80552
C_TIM06 EQU   15   ; Deleni z 25 Hz na 0.6 s (0.01 min)
C_TMIN  EQU   100  ; Deleni na 1 min
N_OF_T  EQU   3    ; Pocet timeru
TIMR1:  DS    1    ; Timery jsou decrementovany
KBDTIMR:DS    1    ; s frekvenci 25 Hz
TIMRI:  DS    1    ; Delicka z 25 Hz na 0.01 min
TIMRJ:  DS    1    ; Delicka z 0.01 min na 1 min
TIME:   DS    2    ; Cas cerpani v min

; Jednotlive chybove kody
ERINT   EQU   10H  ; Vnitrni chyba
ERINTSW EQU   10H  ;  Vnitrni chyba softu
ERINTSM EQU   11H  ;   Neopravneny rezim
ERINTST EQU   12H  ;   Neopravneny start cerpani
ERINTHW EQU   13H  ;  Vnitrni chyba hardware
ERVCC   EQU   13H  ;   Chyba napajeni
ERFLTS  EQU   14H  ;   Rozchazi se davkovani mezi procesory
ERTIME  EQU   15H  ;   Rozchazi se hodiny procesoru
ERTRON  EQU   16H  ;   Chyba spinacich tranzistoru
ERTRHI  EQU   17H  ;   Chyba horniho tranzistoru
ERINTMS EQU   18H  ;   Rychlost motoru neodpovida
ERCOMM  EQU   19H  ;   Chyba komunikace
ERCOMMT EQU   1AH  ;   Chyba komunikace - time out
ERIRCFD EQU   1BH  ;   Chyba fazove diskriminace
ERIRCME EQU   1CH  ;   Motor se netoci pri urcite energii
ERIRCAP EQU   1DH  ;   Neodpovida CAP cidlo IRC cidlu
ERTENZ  EQU   1EH  ;   Upadl tenzometr
ERCAPNP EQU   1FH  ;   Neni pripojene CAP cidlo  
ERNCMD  EQU   20H  ; Dele nez 2 min bez povelu
ERRBAT  EQU   30H  ; Vybita baterie
ERPRESS EQU   41H  ; Prekrocen maximalni tlak
ERPRCUR EQU   42H  ; Prekrocen proud motorem => tlak limit
EREND   EQU   50H  ; Konec davky
ERENDTI EQU   51H  ;  Prekrocen casovy limit
ERENDVO EQU   53H  ;  Prekrocen limit objemu
ERNULF  EQU   60H  ; Nastaveno 0.0 ml
ERSPR   EQU   70H  ; Nedovolena manipulace se strikackou
ERSPRGE EQU   71H  ;  Rozpojena spojka
ERSPRBT EQU   72H  ;  Spatny typ
ERST    EQU   80H  ; Chyba behem spusteni
ERSTAD  EQU   81H  ;  Chyba stavu prevodniku pri spusteni
	    ; 81H      Tenzometr vypnut
	    ; 82H      +5V/-5V pomer napajecich napeti
	    ; 83H      Napajeni tenzometru + kontrola ADC
	    ; 84H      Napeti na ERRORove LED
	    ; 85H      Proud 6 LED diodami
ERLED   EQU   86H  ;  Chyba v mereni diod LED
ERSTFD  EQU   87H  ;  Chyba v obvodu PLC18V8 nebo v procesoru
EREEPR  EQU   88H  ;  Chyba pri cteni udaju z EEPROM
ERSTSYN EQU   89H  ;  Chyba synchronizace s druhym procesorem
ERSTWDT EQU   8AH  ;  Chyba ve watchdogu
ERSTKBD EQU   8BH  ;  Chyba v klavesnici
ERSTLCD EQU   8CH  ;  Chyba v kontroleru leveho  LCD displeje
ERSTLCM EQU   8DH  ;  Chyba v kontroleru praveho LCD displeje
ERSTSNU EQU   9BH  ;  Chyba v seriovem cislu
ERSTSNC EQU   9CH  ;  Nekompatabilni procesory

ERRNUM: DS    1    ; Cislo chyby

SPRTYP: DS    1    ; Typ strikacky  S555N222
		   ; S .. select 50/20; 555 .. typ 50 ml;
		   ; N .. zadna       ; 222 .. typ 20 ml;

TMP:    DS    3

MOD_SLV:DS    1

SLAV_BL EQU   8
IIC_INP:DS    SLAV_BL
IIC_OUT:DS    SLAV_BL

;=================================================================

RSEG IC____D

MX_PDEL EQU   400  ; Maximalni odchylka polohy
C_ENERI EQU   000D0H  ; Energie od rozdilu polohy    (P)
C_DPOL  EQU   -25*256 ; C_DPOL casova konstantu filtru derivace
C_SPEDI EQU   00300H  ; Energie od rozdilu rychlosti (D)

M_SPDEI:DS    2 ; *  Inkrement energie od rozdilu ryclosti
MAX_PRE:DS    2    ; Tlakova mez
FLOW:   DS    2    ; Prutok
; Oblast nulovana pri startu
M_ENERI:DS    2    ; Inkrement energie od rozdilu polohy
M_DFILT:DS    2    ; Filtrace derivace
M_DPOL: DS    2    ; Pol filtrace derivace
POZ_INC:DS    4    ; Rychlost v impulsech za 1 cas preruseni *2^24
POZ_ACT:DS    3    ; Aktualni pozice
POZ_RQS:DS    3    ; Dilky 1 kroku
POZ_RQ: DS    3    ; Pozadovana pozice
POZ_DEL:DS    2    ; Rozdil pozic POZ_RQ - POZ_ACT
M_ENERG:DS    2    ; Energie pro motor
POZ_S00:           ; Konec oblasti nulovane pri startu
SPD_ACT:DS    2    ; Skutecny pocet impulsu za cas int
PRE_ACT:DS    2    ; Aktualni tlak
IRC_ERR:DS    1    ; Pocet chyb z IRC
CIRE_DF EQU   0E0H ; Penalizace za chybu diskriminatoru
CIRE_ME EQU   010H ; Penalizace za stojici motor pri energii >0.5

STR_POZ:DS    2    ; Planovana delka pohybu v [256*irc]
LST_VOL:DS    2    ; Objem vydavkovany za minule strikacky
MAX_VOL:DS    2    ; Limit objemu

RSEG IC____I

MAX_TIM:DS    2    ; Limit casu
NEN_IRC:DS    2    ; Pocet 256*IRC kdy je treba zacit varovat
CMAX_PL EQU   150  ; Maximalni dovolena hodnota limitu tlaku
CDEF_PL EQU   50   ; Defaultni hodnota limitu tlaku
CDAL_PL EQU   100  ; Alternativni hodnota limitu tlaku
CINT_PR EQU   500H
INT_PRE:DS    2    ; Posun prevodniku tlaku
CSLP_PR EQU   0834H
SLP_PRE:DS    2    ; Sklon prevodniku tlaku * 8
CINT_BA EQU   493  ; Offset prevodniku baterie
CSLP_BA EQU   138  ; Pocet dilku prevodniku na 0.1 V
ACU_BAT:DS    2    ; Napeti na baterii v 0.1 V
CLO_BA  EQU   68   ; Baterie 6.8 V error (bylo 6.6)
CALR_BA EQU   72   ; Baterie 7.2 V varovani (bylo 6.9)
LST_ERR:DS    1    ; Kod posledni chyby
ST_BAT: DS    1    ; 0 .. bez napeti 1 .. varovat 2..OK
OUT_MSG:DS    3    ; Prikaz pro druhy procesor
CAP_IRC:DS    2    ; Aktualni pozice z kapacitniho cidla
CAP_OFS:DS    2    ; Offset CAP_IRC+POZ_ACT/256
CAP_TMP:DS    1    ; Pomocna promenna pro CHK_IRC
CAP_ERC:DS    1	   ; Promenna pro kontrolu chyb CAP<>IRC

;=================================================================
; Programovy segment

RSEG IC____C

START:  MOV   IEN0,#00000000B ; zakaz preruseni
	MOV   IEN1,#00000000B
	MOV   IP0, #00000000B ; priority preruseni
	MOV   IP1, #00000000B ; priority preruseni
	%WATCHDOG             ; Nulovani watch-dogu
	MOV   TMOD,#00010101B ; timer 1 mod 1; counter 0 mod 1
	MOV   TCON,#01010101B ; citac 0 a 1 cita ; interapy hranou
	MOV   TM2CON,#00100011B; counter 2 from T2, TR2 enabled
	MOV   CTCON,#01000000B; zaznamenat ERR z IRC cidla
	MOV   SCON,#11011000B ; dva stopbity
	MOV   PCON,#10000000B ; Bd = OSC/12/16/(256-TH1)
	MOV   TH1,#0FAH       ; 9600Bd
	MOV   PSW,#0          ; banka registru 0
	MOV   SP,#STACK       ; inicializace zasobniku
	MOV   P3,#0FFH
	MOV   P1,#0FFH
	CLR   EBP

	MOV   PWMP,#0         ; PWM frekvence 23 kHz
	MOV   PWM0,#0

%IF(0)THEN(
	MOV   08EH,#00H	      ; Zakazani nabojove pumpy	
)FI

	MOV   PSW,#AR0
	MOV   B,#093H         ; Test instrukcniho souboru
	MOV   0,#055H
	MOV   A,#0F0H
	XRL   A,R0    ; A5
	MOV   9,#18H
	SETB  PSW.3
	MOV   @R1,A
	INC   R1
	MOV   @R1,#0C3H
	SETB  PSW.4   ; R0=A5, R1=C3
	ADD   A,B     ; A=38, PSW=9F
	SUBB  A,PSW
	RLC   A
	ANL   A,#NOT 4
	CJNE  A,#031H,V1ERR_H
	MUL   AB
	ADDC  A,B
	MOV   B,R0
	XRL   B,#0A0H
	DIV   AB
	SWAP  A
	XRL   A,B
	XRL   A,R1
	JZ    ST1_0
V1ERR_H:MOV   B,#5
V2ERR_H:JMP   ERR_HL1

ST1_0:	MOV   PSW,#AR0
	CLR   A               ; Test pameti eprom
	MOV   R2,#0FEH
	MOV   R3,#020H
	MOV   R4,A
	MOV   R5,A
	MOV   DPTR,#0
ST1_1:  CLR   A
	MOVC  A,@A+DPTR
	INC   DPTR
	ADD   A,R4
	MOV   R4,A
	MOV   A,R5
	ADDC  A,#0
	MOV   R5,A
	DJNZ  R2,ST1_1
	SETB  ELED
	%WATCHDOG
	DJNZ  R3,ST1_1
	CLR   A
	MOVC  A,@A+DPTR
	ADD   A,R4
	MOV   R4,A
	INC   DPTR
	CLR   A
	MOVC  A,@A+DPTR
	ADDC  A,R5
	ORL   A,R4
	MOV   B,#7
	JNZ   V2ERR_H         ; !!!!!!!!!!!!!!!

ST2_0:  CLR   EA              ; Test pameti RAM
	MOV   R0,#0FFH
	MOV   A,@R0
	MOV   B,A
	MOV   DPL,A
	MOV   SP,R0
ST2_1:  ADD   A,#7
	PUSH  ACC
	CJNE  A,B,ST2_1
	%WATCHDOG             ; Nulovani watch-dogu
ST2_2:  INC   SP
	ADD   A,#7
	POP   B
	CJNE  A,B,V1ERR_H
	XRL   B,#0FFH
	PUSH  B
	CJNE  A,DPL,ST2_2
	%WATCHDOG             ; Nulovani watch-dogu
	MOV   DPH,#0
ST2_3:  INC   SP
	ADD   A,#7
	POP   B
	XRL   B,#0FFH
	CJNE  A,B,V1ERR_H
	PUSH  DPH             ; Nulovani pameti ram
	CJNE  A,DPL,ST2_3
	CLR   A
	CJNE  A,0,V1ERR_H
	MOV   SP,#STACK
	CALL  LED_CLR


	MOV   A,#K_STOP
	CALL  TESTKEY
	JNZ   ST_AD_0
	SETB  FL_SERT
ST_AD_0:MOV   DPH,#HIGH LED1  ; Test AD prevodniku
	MOV   A,#0CFH
	MOVX  @DPTR,A
	CALL  DB_W_10
	MOV   R7,#ERSTAD-1
	MOV   R1,#ST_ADt-ST_D_Sk
	JNB   FL_SERT,ST_AD_1
	MOV   R1,#ST_ADtT-ST_D_Sk
ST_AD_1:INC   R7
	MOV   B,#1
	CALL  ST_D_S0
	JZ    ST_D_0
	CALL  GET_ADN
%IF (1) THEN (
	CALL  ST_D_S
	CALL  CMPi
	JC    ST_AD_E
	CALL  ST_D_S
	CALL  CMPi
	JNC   ST_AD_E
)ELSE(
	CALL  ST_D_S
	CALL  ST_D_S
	PUSH  AR1
	SETB  EA
	SETB  TR1
	SETB  ET1
	CLR   EBP
	CALL  PRTR45
ST_AD_3:CALL  ST_WDC          ; Nulovani watch-dogu
	CALL  SCANKEY
	JZ    ST_AD_3
	POP   AR1
)FI
	CLR   TOFF
	SJMP  ST_AD_1

ST_D_E:	POP   AR5
ST_D_E1:MOV   R7,#ERLED
ST_AD_E:MOV   ERRNUM,R7
	JMP   ERR_HLD

ST_D_0: SETB  TOFF            ; Test LED indikaci
    %IF (NOT %NEW_LED_FL) THEN (
	MOV   TMP,R4
	MOV   TMP+1,R5
    )ELSE(
	MOV   A,#0FFH
	MOV   DPH,#HIGH (LED0 AND LED1 AND LED2)
	MOVX  @DPTR,A
	MOV   R0,#80H
	CALL  DB_WAI1
	MOV   TMP,#0
	MOV   TMP+1,#040H
    )FI
	MOV   TMP+2,#0
	MOV   R1,#ST_D_St-ST_D_Sk
ST_D_1: CALL  ST_D_S
	MOV   A,R2
	ANL   A,#0F0H
	JZ    ST_D_9
	ORL   A,#0FH
	MOV   DPH,A
	MOV   A,R3
	MOVX  @DPTR,A
	MOV   A,R2
	ANL   A,#0FH
    %IF (NOT %NEW_LED_FL) THEN (
	ADD   A,TMP+2
    )FI

	MOV   TMP+2,A
	PUSH  AR1
    %IF (NOT %NEW_LED_FL) THEN (
	CALL  DB_W_10
    )ELSE(
	MOV   R0,#08H
	CALL  DB_WAI1
    )FI
	CLR   A
	CALL  GET_ADN
	CALL  ST_WDC          ; Nulovani watch-dogu
	MOV   R2,TMP
	MOV   R3,TMP+1
	CLR   F0
	MOV   R0,#0
    %IF (NOT %NEW_LED_FL) THEN (
	MOV   R1,#6
    )ELSE(
	MOV   R1,#6-3
    )FI
	CALL  DIVi0
	MOV   R0,TMP+2
	MOV   TMP+2,R4
	MOV   R1,#-9
	JNB   FL_SERT,ST_D_2
	INC   R1
	MOV   A,R0
	JZ    ST_D_2
	MOV   R1,#-5
	INC   R0
	INC   R0
ST_D_2: MOV   A,R4
	CLR   C
	SUBB  A,R0
%IF (1) THEN (
    %IF (NOT %NEW_LED_FL) THEN (
	MOV   R4,A
    )FI
	JC    ST_D_8
ST_D_3:	ADD   A,R1
	JC    ST_D_E
)ELSE(
	SETB  EA
	SETB  TR1
	SETB  ET1
	CLR   EBP
	CALL  PRTR45
;	CALL  DB_WAIT
ST_D_5:	CALL  ST_WDC          ; Nulovani watch-dogu
	CALL  SCANKEY
	JZ    ST_D_5
)FI
	POP   AR1
	SJMP  ST_D_1

ST_D_8: POP   ACC
	MOV   R5,A
	ADD   A,#-(ST_D_RD-ST_D_Sk)-2
	JNZ   ST_D_E1
	SETB  FL_NRD
ST_D_9:	JMP   ST_SYN

ST_D_S: MOV   B,#2
ST_D_S0:MOV   R0,#AR2
ST_D_S1:MOV   A,R1
	INC   R1
	MOVC  A,@A+PC
ST_D_Sk:MOV   @R0,A
	INC   R0
	DJNZ  B,ST_D_S1
	RET

%IF (NOT %NEW_LED_FL) THEN (
ST_D_St:DB    00010000B+0,000H
	DB    10110000B+6,080H
	DB    10110000B+6,0C0H
	DB    10110000B+6,0C1H
	DB    10110000B+6,0C3H
	DB    10110000B+6,0C7H
	DB    10110000B+6,0CFH
;	DB    10110000B+0EH,0FFH
	DB    11010000B+4,001H
	DB    11010000B+4,003H
	DB    11010000B+4,007H
	DB    11010000B+4,00FH
	DB    11010000B+4,01FH
	DB    11010000B+4,03FH
	DB    11010000B+4,07FH
ST_D_RD:DB    01110000B+5,002H  ; Prvni led rozsirene skupiny
	DB    01110000B+5,042H
	DB    01110000B+5,043H
	DB    01110000B+5,063H
	DB    01110000B+4,06BH
	DB    01110000B+4,07BH
;	DB    01110000B+8,0FFH
	DB    0
)ELSE(
; Nova verze testu led se spinanim po jedne ledce
ST_D_St:DB    00010000B+0,000H
	DB    10110000B+8,080H
	DB    10110000B+8,040H
	DB    10110000B+8,001H
	DB    10110000B+8,002H
	DB    10110000B+8,004H
	DB    10110000B+8,008H
;	DB    10110000B+0EH,030H
	DB    10110000B+0,000H	; null
	DB    11010000B+6,001H
	DB    11010000B+6,002H
	DB    11010000B+6,004H
	DB    11010000B+6,008H
	DB    11010000B+6,010H
	DB    11010000B+6,020H
	DB    11010000B+6,040H
	DB    11010000B+0,000H	; null
ST_D_RD:DB    01110000B+8,002H  ; Prvni led rozsirene skupiny
	DB    01110000B+8,040H
	DB    01110000B+8,001H
	DB    01110000B+8,020H
	DB    01110000B+8,008H
	DB    01110000B+8,010H
;	DB    01110000B+0EH,084H
	DB    01110000B+0,000H	; null
	DB    0
)FI

ST_ADt: DB    14H             ; Tenzometr
	%W    (00001H)
	%W    (00800H)
	DB    15H             ; +5V/-5V = 0B982H
	%W    (0B600H)
	%W    (0C800H)
	DB    14H             ; Tenzometr
	%W    (07D00H)
	%W    (08300H)
    %IF (NOT %NEW_LED_FL) THEN (
	DB    13H             ; Eled
	%W    (06800H)
	%W    (09400H)
    )ELSE(
	DB    13H             ; Eled
	%W    (07400H)
	%W    (0A800H)
    )FI
    %IF (NOT %NEW_LED_FL) THEN (
	DB    10H             ; Led
	%W    (03800H)
	%W    (05000H)
    )FI
	DB    0

ST_ADtT:DB    14H             ; Tenzometr
	%W    (00001H)
	%W    (00600H)
	DB    15H             ; +5V/-5V = 0B982H
	%W    (0B800H)
	%W    (0C600H)
	DB    14H             ; Tenzometr
	%W    (07E00H)
	%W    (08200H)
    %IF (NOT %NEW_LED_FL) THEN (
	DB    13H             ; Eled
	%W    (06C00H)
	%W    (09000H)
    )ELSE(
	DB    13H             ; Eled
	%W    (07800H)
	%W    (0A400H)
    )FI
    %IF (NOT %NEW_LED_FL) THEN (
	DB    10H             ; Led
	%W    (03C00H)
	%W    (04C00H)
    )FI
	DB    0

ST_FDu: %W    (035AH)
	%W    (035AH)
	%W    (8A78H)
	%W    (035AH-0A78H)
	%W    (0)

ST_FD:	CALL  LED_CLR         ; Test fazoveho diskriminatoru
	MOV   A,#1
	MOV   DPH,#HIGH LED2
	MOVX  @DPTR,A
	SETB  TS_FDIS
	SETB  KLO1
	CALL  M_RESP
	MOV   R1,#ST_FDu-ST_D_Sk
	MOV   R6,#0
ST_FD1: CALL  ST_D_S
	MOV   C,ACC.7
	MOV   F0,C
	ANL   A,#7FH
	MOV   R3,A
	ORL   A,R2
	JNZ   ST_FD2
	MOV   A,POZ_ACT+2
	CJNE  A,#-1,ST_FDE
	CLR   EA
	CLR   CTI3
	CLR   A
	MOV   DPH,#HIGH LED2
	MOVX  @DPTR,A
        MOVX  A,@DPTR
	CALL  LED_CLR
	MOVX  A,@DPTR
	JNB   CTI3,ST_FDE
	CLR   TS_FDIS
	SETB  EA
	RET
ST_FD2: CALL  ST_FDS
	MOV   R0,TIMRI
ST_FD3: MOV   A,TIMRI
	XRL   A,R0
	JZ    ST_FD3
	CALL  ST_D_S
	MOV   R4,POZ_ACT
	MOV   R5,POZ_ACT+1
	CALL  CMPi
	JZ    ST_FD1
ST_FDE: MOV   ERRNUM,#ERSTFD
	JMP   ERR_HLE

ST_FDS: INC   R3
	CJNE  R2,#0,ST_FDS1
	DEC   R3
ST_FDS1:SETB  ELED
	INC   R6
	JB    F0,ST_FDS2
	DEC   R6
	DEC   R6
ST_FDS2:MOV   A,R6
	ANL   A,#3
	ADD   A,#ST_FDt-ST_FDk
	MOVC  A,@A+PC
ST_FDk: MOV   DPH,#HIGH LED0
	MOVX  @DPTR,A
	RR    A
	MOV   DPH,#HIGH LED1
	MOVX  @DPTR,A
	MOV   PWM0,#0
	CLR   FL_MOT
	DJNZ  R2,ST_FDS1
	DJNZ  R3,ST_FDS1
	RET

ST_FDt: DB    00B,01B,11B,10B

ST_LC:  MOV   R2,#3
	MOV   R6,#10H
	CALL  ST_LCP
	MOV   A,#ERSTLCD
	JC    ST_LCE
	MOV   R2,#3
	MOV   R6,#18H
	CALL  ST_LCP
	JNB   FL_NRD,ST_LC0
	CPL   C
ST_LC0:	MOV   A,#ERSTLCM
	JC    ST_LCE
	RET
ST_LCE: MOV   ERRNUM,A
	JMP   ERR_HLD

	MOV   TMP,#0          ; Test LCD indikaci
ST_LC2: SETB  EBP
	MOV   R2,TMP
	MOV   R6,#10H
	CALL  ST_LCP
	CALL  DB_W_10
	MOV   R6,#18H
	CALL  OUT_BUF
	CALL  DB_WAIT
	INC   TMP
	CALL  ST_LCS
	CLR   EBP
	MOV   A,R7
	MOV   R5,A
	MOV   A,R6
	MOV   R4,A
	CALL  PRTR45
ST_LC4: CALL  SCANKEY
	JZ    ST_LC4
	JMP   ST_LC2
	RET

ST_LCP:	MOV   R0,#WR_BUF+1
	MOV   R1,#4
ST_LCP2:MOV   A,R2
	ORL   A,#NOT 3
	ADD   A,R1
	CLR   A
	JNC   ST_LCP3
	CPL   A
ST_LCP3:MOV   @R0,A
	INC   R0
	DJNZ  R1,ST_LCP2
	CALL  OUT_BUF
	JMP   IIC_WME

ST_LCS: MOV   R6,#0FFH
	MOV   R7,#0
	MOV   R0,#0
	MOV   R1,#80H
ST_LCS1:MOV   A,#1
	CALL  GET_ADN
	MOV   A,R5
	XCH   A,R1
	XRL   A,R1
	JNZ   ST_LCS3
	MOV   A,R5
	SUBB  A,R6
	JNC   ST_LCS2
	MOV   A,R5
	MOV   R6,A
ST_LCS2:MOV   A,R7
	SUBB  A,R5
	JNC   ST_LCS3
	MOV   A,R5
	MOV   R7,A
ST_LCS3:DJNZ  R0,ST_LCS1
	MOV   A,R7
	CLR   C
	SUBB  A,R6
	RET

;=============================================================
; Provedeni synchronizace s druhym procesorem

PPSYNC: MOV   IIC_OUT,A
	JNB   FL_SERV,PPSYNC0
	MOV   A,#0AAH
PPSYNC0:MOV   IIC_OUT+2,A
	MOV   IIC_OUT+1,ERRNUM
	MOV   IIC_OUT+3,#SOFT_VER
	MOV   TMP+1,#20
PPSYNC1:CALL  SND_OUT
	JNB   ACC.2,PPSYNC2
	CALL  PPSYNW
	SJMP  PPSYNC1
PPSYNC2:JB    ICF_SRC,PPSYNC3 ; Cekani na druhy procesor
	CALL  PPSYNW
	SJMP  PPSYNC2
PPSYNC3:MOV   R0,#IIC_INP
	CALL  XOR_SU0         ; Kontrola prijateho xorsumu
	XRL   A,@R0
	JNZ   PPSYNER
	MOV   R4,IIC_INP+3
	MOV   R5,#SOFT_VER
	MOV   A,IIC_INP+1
	JNZ   PPSYNE1
	MOV   R7,IIC_INP+2
	MOV   A,R7
	XRL   A,#055H
	JZ    PPSYNC4
	CLR   FL_SERV
PPSYNC4:MOV   A,IIC_INP+3
	XRL   A,#SOFT_VER
	JNZ   PPSYNER
	MOV   A,IIC_INP
	CLR   ICF_SRC
	MOV   DPTR,#LED2
	MOVX  @DPTR,A
	RET

ST_SYN:	CLR   A
	MOV   KBDTIMR,A
	MOV   KBDFL,A
	CALL  SCANKEY
	CJNE  A,#VIR_KEY1+K_STOP,ST_SYN0
	SJMP  ST_SYN1
ST_SYN0:CJNE  A,#K_ALARM,ST_SYN2
ST_SYN1:SETB  FL_SERV
	CLR   A
ST_SYN2:JNZ   ST_KBDE
	MOV   S1ADR,#20H      ; Nastaveni adresy stanice
	MOV   R4,#IIC_INP
	MOV   R2,#SLAV_BL OR 80H
	MOV   R6,#0
	MOV   R7,#0
	CALL  IIC_SLI
	SETB  EA
	CALL  ST_WDC
	CALL  ST_LC           ; Test displayu
	MOV   A,#0B0H
	CALL  PPSYNC          ; Vymena zpravy
	CJNE  A,#0B2H,PPSYNT
	MOV   A,#0B3H
	CALL  PPSYNC
	CJNE  A,#0B4H,PPSYNER
	JMP   STARTC

PPSYNT:	CJNE  A,#0B1H,PPSYNER ; Test spadnuti na watchdog
	CLR   EA
	CALL  ST_WDC
	MOV   TMP+1,#0B0H
PPSYNT0:DJNZ  TMP,PPSYNT0
	DJNZ  TMP+1,PPSYNT0
	CALL  ST_WDC
	MOV   A,#ERSTWDT
	SJMP  PPSYNE1

PPSYNW: DJNZ  TMP,ST_WDC
	DJNZ  TMP+1,ST_WDC
PPSYNER:MOV   A,#ERSTSYN
PPSYNE1:MOV   ERRNUM,A
	JMP   ERR_HLD

ST_KBDE:MOV   A,#ERSTKBD
	SJMP  PPSYNE1

; Bezpecne nulovani watchdogu se zastavenim cerpani
ST_WDC: %WATCHDOG
	SETB  ELED
	MOV   PWM0,#0
	CLR   FL_MOT
	RET

;=============================================================
; Start komunikace s uzivatelem

STARTC:	CLR   TS_FDIS
	CLR   EBP
	CLR   TOFF

	CALL  LED_CLR

	CLR   A
	MOV   R0,#AD_PRE
	MOV   @R0,#10H
	MOV   R0,#AD_VCC+1
	MOV   @R0,#30H

	MOV   STR_POZ,#LOW  3200
	MOV   STR_POZ+1,#HIGH 3200

	CALL  M_RESP

	SETB  FL_BAT

	MOV   M_ENERI,  #LOW  C_ENERI
	MOV   M_ENERI+1,#HIGH C_ENERI

	MOV   M_SPDEI,  #LOW  C_SPEDI
	MOV   M_SPDEI+1,#HIGH C_SPEDI

	MOV   M_DPOL,   #LOW  C_DPOL
	MOV   M_DPOL+1, #HIGH C_DPOL

	CLR   TR1
	MOV   TH1,#0FFH       ; Snulovani casu
	MOV   TL1,#0FFH
	SETB  TR1
	MOV   PPTIMR,#-1
	MOV   NCCNT,#0
	ORL   IEN0,#10001000B ; Povoleni casoveho prerueseni

	CALL  ST_FD
	CALL  CLR_ER2

	SETB  FL_EDCH
			      ; Nacitani udaju z EEPROM
	CALL  ST_WDC          ; Nulovani watch-dogu
	MOV   SPRTYP,#80H     ; Typ strikacky 50 ml - 0
	CLR   F0
	MOV   R0,#87H         ; Parametry strikacky
	CALL  EEPA_RD
	JB    F0,STARTC0
	MOV   SPRTYP,IIC_OUT+1
	MOV   CFG_FLG,IIC_OUT+2
STARTC0:MOV   A,#SM_PRES      ; Defaultni hodnota tlakoveho
	CALL  CL_SPC          ; limitu

	CALL  ST_WDC          ; Nulovani watch-dogu
	MOV   R0,#INT_PRE     ; Posun prevodniku tlaku
	MOV   @R0,#LOW  CINT_PR
	INC   R0
	MOV   @R0,#HIGH CINT_PR
	MOV   R0,#SLP_PRE     ; Sklon prevodniku tlaku * 256
	MOV   @R0,#LOW  CSLP_PR
	INC   R0
	MOV   @R0,#HIGH CSLP_PR
	CLR   F0
	MOV   R0,#8EH
	CALL  EEPA_RD
	JB    F0,STARTC5
	MOV   R0,#INT_PRE
	MOV   R1,#IIC_OUT+1
	MOV   R2,#4
	CALL  MOVsi
STARTC5:
    %IF(%PARSAVE_FL)THEN(
	JNB   FL_PARSV,STARTC6
	CLR   F0
	MOV   R0,#0C6H        ; Posledni navolene limity
	CALL  EEPA_RD
	JB    F0,STARTC6
	MOV   R2,IIC_OUT+1
	MOV   R3,IIC_OUT+2
	MOV   R0,#MAX_TIM
	CALL  iSVR23i
	;MOV   MAX_PRE,IIC_OUT+3
	;MOV   MAX_PRE+1,IIC_OUT+4
	MOV   FLOW,IIC_OUT+3
	MOV   FLOW+1,IIC_OUT+4
	MOV   MAX_VOL,IIC_OUT+5
	MOV   MAX_VOL+1,IIC_OUT+6
STARTC6:
    )FI
	CALL  ST_WDC          ; Nulovani watch-dogu
	CLR   ELED
	CALL  PPSTART
	JNB   FL_SERV,STARTC8
	JMP   ISERV

STARTC8:JNB   FL_EREE,ST_USRM
	MOV   ERRNUM,#EREEPR  ; Chyba pri cteni EEPROM
	JMP   ERR_HLE

ST_USRM:MOV   MOD_SLV,#M_SVOL ; Uzivatelsky rezim
	JNB   FL_NRD,C1
	MOV   MOD_SLV,#M_SNORM

C1:    	CALL  CREC            ; Testovani prijatyc zprav
CP1:    MOV   R0,MOD_SLV

%IF (%DEBUG_FL) THEN (
	CJNE  R0,#0,CP2       ; MOD_SLV=0 testovani vysilani na
	CLR   EBP             ; LCD a slave 10h
	MOV   R5,TMP
	MOV   R4,TMP+1
	MOV   R6,#10H
	CALL  iPRTLhw
	MOV   R6,#10H
	CALL  OUT_BUF
     ;  %WATCHDOG             ; Nulovani watch-dogu
	JB    ACC.0,C1

	MOV   A,TMP
	ORL   A,TMP+1
	MOV   DPTR,#LED0
	MOVX  @DPTR,A
	MOV   DPTR,#LED1
	MOVX  @DPTR,A
	MOV   DPTR,#LED2
	MOVX  @DPTR,A

	MOV   R1,#0
C2:     NOP
	DJNZ  R0,C2
     ;  %WATCHDOG             ; Nulovani watch-dogu
	DJNZ  R1,C2
C3:  ;  %WATCHDOG             ; Nulovani watch-dogu
	MOV   R6,#10H
	MOV   R4,#TMP
	MOV   R2,#2
	MOV   R3,#0
	CALL  IIC_RQI
	JB    ACC.0,C3
C4:     JBC   ICF_MER,C5
	JB    ICF_MRQ,C4
	INC   TMP
	JMP   C1
C5:     INC   TMP+1
C2_ER:  CLR   ICF_MRQ
	SETB  AA
	JMP   C1

CP2:    MOV   A,R0
	ANL   A,#0F0H
	CJNE  A,#010H,CP3     ; SLV_MOD=10 az 17h vystup prevodniku
	MOV   A,R0            ; na LCD
	ANL   A,#07H
	MOV   ADCON,A
	ORL   A,#08H
	MOV   ADCON,A
	MOV   R1,#10H
CP2_1:  NOP
	DJNZ  R2,CP2_1
      ; %WATCHDOG             ; Nulovani watch-dogu
	DJNZ  R1,CP2_1
	MOV   R4,ADCON
	MOV   R5,ADCH
	CLR   EA
	MOV   IIC_OUT,R4
	MOV   IIC_OUT+1,R5
	SETB  EA
	MOV   A,R0
	JB    ACC.3,CP2_2
	MOV   R6,#10H
	CALL  iPRTLhw
	MOV   R6,#10H
	CALL  OUT_BUF
	JMP   C1

CP2_2:  SETB  EBP             ; SLV_MOD=18 az 1Fh Posilani dat z ADC na IIC
	JBC   ICF_MER,C2_ER
	MOV   R6,#10H
	MOV   R4,#IIC_OUT
	MOV   R2,#2
	MOV   R3,#0
	CALL  IIC_RQI
	JMP   C1

CP3:    CJNE  R0,#1,CP4       ; SLV_MOD=1 test cteni IIC_OUT
	MOV   IIC_OUT,#012H
	INC   IIC_OUT+1
	JMP   C1

CP4:    CJNE  R0,#2,CP5       ; SLV_MOD=2  vypis POZ_ACT na display
	MOV   R4,POZ_ACT+1
	MOV   R5,POZ_ACT+2
	MOV   R6,#10H
;       CALL  iPRTLhw
	MOV   R7,#0C4H
	CALL  iPRTLi
CP4_0:	MOV   R6,#10H
	CALL  OUT_BUF
	MOV   R1,#20H
CP4_1:  NOP
	DJNZ  R2,CP4_1
      ; %WATCHDOG             ; Nulovani watch-dogu
	DJNZ  R1,CP4_1
	JMP   C1

CP5:    CJNE  R0,#3,CP6       ; SLV_MOD=3  vypis POZ_RQ na display
	MOV   R4,POZ_RQ+1
	MOV   R5,POZ_RQ+2
	JMP   CP8_0

CP6:    CJNE  R0,#4,CP7       ; SLV_MOD=4  vypis POZ_DEL na display
	MOV   R4,POZ_DEL
	MOV   R5,POZ_DEL+1
	JMP   CP8_0

CP7:    CJNE  R0,#5,CP8       ; SLV_MOD=5  vypis M_ENERG na display
	MOV   R4,M_ENERG
	MOV   R5,M_ENERG+1
	JMP   CP8_0

CP8:    CJNE  R0,#6,CP9       ; SLV_MOD=6  vypis M_DFILT na display
	MOV   R4,M_DFILT
	MOV   R5,M_DFILT+1
CP8_0:	MOV   A,#0FH
	JB    M_D_256,CP8_1
	MOV   A,#0H
CP8_1:  MOV   DPTR,#LED1
	MOVX  @DPTR,A
CP8_2:	MOV   R6,#10H
	CALL  iPRTLhw
	MOV   A,#0FFH
	JMP   CP4_0
CP9:    CJNE  R0,#7,CP10      ; SLV_MOD=7  vypis SCANKEY na display
	CALL  SCANKEY
	JZ    CP9_1
	MOV   TMP,A
	INC   TMP+1
CP9_1:	MOV   R4,TMP
	MOV   R5,TMP+1        ; TMP+1 ; KBDTIMR
	JMP   CP8_2

CP10:   CJNE  R0,#20H,CP11V   ;   SLV_MOD=20 interaktivni zadavani konstant
	JNB   FL_EDCH,CP10_1
	CLR   FL_EDCH
	CALL  DIEDNUM
	CALL  EDDIHEX
CP10_1: MOV   PPTIMR,#-1
	CALL  SCANKEY
	MOV   DPTR,#SFT_TMP
	CALL  KEY_FNC
	JMP   C1
CP11V:  JMP   CP11

SFT_TMP:DB    K_ALARM         ; Pomocny mod
	%W    (0)
	%W    (CHEDNUM)

	DB    K_LU0
	%W    (1)
	%W    (EDADD)

	DB    K_LU1
	%W    (10H)
	%W    (EDADD)

	DB    K_LU2
	%W    (100H)
	%W    (EDADD)

	DB    K_LU3
	%W    (1000H)
	%W    (EDADD)

	DB    K_LD0
	%W    (-1)
	%W    (EDADD)

	DB    K_LD1
	%W    (-10H)
	%W    (EDADD)

	DB    K_LD2
	%W    (-100H)
	%W    (EDADD)

	DB    K_LD3
	%W    (-1000H)
	%W    (EDADD)

	DB    K_START
	%W    (0)
	%W    (M_START)

	DB    K_STOP
	%W    (0)
	%W    (M_STOP)

	DB    K_LPOS
	%W    (0)
	%W    (A_START)

	DB    K_RPOS
	%W    (0)
	%W    (LED_CLR)

	DB    K_NUL
	%W    (0)
	%W    (USR_MOD)

	DB    K_RU0
	%W    (0)
	%W    (CLR_EBP)

	DB    K_RU3
	DB    TMP,1
	%W    (EEPR_RV)

	DB    K_RD3
	DB    TMP,1
	%W    (EEPR_WR)

	DB    0

CLR_EBP:SETB  EBP
	MOV   A,#B_OK
	RET

USR_MOD:CALL  PPSTART
	MOV   R2,#0C0H
	JMP   SV_MOD

A_START:CLR   EA
	CALL  IIC_CER
	MOV   A_MSK,#1
	MOV   A_CPOS,#0
	MOV   A,A_PER
	JZ    A_STARR
	MOV   A_CPOS,#80H
A_STARR:SETB  EA
	MOV   A,#B_OK
	RET

CHEDNUM:MOV   A,EDNUM
	INC   A
	ANL   A,#07H
	MOV   EDNUM,A
	SETB  FL_EDCH
	JMP   CLR_ER1

; Zobrazeni nesmyslu

DIEDNUM:MOV   R0,EDNUM
	INC   R0
	MOV   A,#80H
DIEDNU1:RL    A
	DJNZ  R0,DIEDNU1
	MOV   DPTR,#LED2
	MOVX  @DPTR,A
	RET

; Hexadecimalni zobrazeni cisla podle [EDNUM]

EDDIHEX:CALL  GET_EDN
EDDIH11:MOV   R6,#10H
	CALL  iPRTLhw
EDDIH12:CALL  IIC_CER
	MOV   R6,#10H
	CALL  OUT_BUF
	JNZ   EDDIH12
	MOV   A,#B_OK
	RET

)FI

CP11:   MOV   A,R0            ; SLV_MOD=C0 pracovni rezim
	ADD   A,#-0C0H
	ADD   A,#-16
	JC    CP12
LSTOP:  MOV   C,FL_EDCH
	JNC   CP11_C
	CLR   FL_HSD
	MOV   C,FL_TIME
	ANL   C,FL_VOL
	JNC   CP11_0
	CALL  LIM2FL          ; Dopocitava se prutok
	SJMP  CP11_C
CP11_0: MOV   A,FLOW+1
	ADD   A,#-7FH         ; Kontrola chyboveho prutoku
	JNZ   CP11_C
	MOV   FLOW,A
	MOV   FLOW+1,A
CP11_C:	JB    FL_HSD,CP11_B
	CALL  DILCD           ; Zobrazeni udaju na LCD displejich
CP11_B:	CALL  DILED           ; Zobrazeni led diod
	CALL  SCANKEY         ; Cteni klavesnice
	JZ    CP11_A
	CLR   FL_HSD
CP11_A:	MOV   DPTR,#SFT_D2    ; Tabulka funkci
	JNB   FL_NRD,CP11_1
	MOV   DPTR,#SFT_D3    ; Tabulka funkci bez praveho displaye
CP11_1:	CALL  KEY_FNC         ; Vykonani funkce podle A a DPTR
	ANL   A,#3
	ORL   PP_FLG,A        ; Nastaveni pipani
CP11_2: CALL  PPSYS           ; Volani Ping-Pongu
	MOV   A,NCCNT
	ADD   A,#-NCMODEN
	JNC   CP11_3
	CLR   FL_HSD
	MOV   A,MOD_SLV
	ANL   A,#NOT 3
	XRL   A,#M_UN_M
	JNZ   CP11_3
	ANL   MOD_SLV,#NOT 4
	SETB  FL_EDRF
CP11_3: JB    FL_MOT,CP11_4
	MOV   A,NCCNT
	ADD   A,#-NCMXDEL
	JNC   CP11_4
	MOV   A,#ERNCMD       ; Dele  jak 2 min bez povelu
	CALL  SET_ERR
CP11_4: JMP   C1

CP12:
	JMP   C1

CRNO:   RET

CREC:   JNB   ICF_SRC,CRNO
	MOV   R0,IIC_INP
%IF (%DEBUG_FL) THEN (
	CJNE  R0,#1,CREC1     ; Prikaz 1 nastaveni MOD_SLV
	MOV   MOD_SLV,IIC_INP+1
CREC1:  CJNE  R0,#2,CREC2     ; Prikaz 2 nastaveni LED
	MOV   DPTR,#LED0
	MOV   A,IIC_INP+1
	MOVX  @DPTR,A
	MOV   DPTR,#LED1
	MOV   A,IIC_INP+2
	MOVX  @DPTR,A
	MOV   DPTR,#LED2
	MOV   A,IIC_INP+3
	MOVX  @DPTR,A
CREC2:  CJNE  R0,#3,CREC3     ; Prikaz 3 zkouska citace casu
	MOV   KBDTIMR,#0FFH
CREC2_1:MOV   R4,TIME_CN
	MOV   R5,KBDTIMR
	MOV   R6,#10H
	CALL  iPRTLhw
	MOV   R6,#10H
	CALL  OUT_BUF
	MOV   A,KBDTIMR
	JNZ   CREC2_1
	JMP   CRR
CREC3:  CJNE  R0,#4,CREC4     ; Prikaz 4 nulovani citace polohy
	CLR   A
	MOV   POZ_RQ,A
	MOV   POZ_RQ+1,A
	MOV   POZ_RQ+2,A
	CALL  M_RESP
	JMP   CRR
CREC4:  CJNE  R0,#80H,CREC5   ; Vstup z TTY
	MOV   R4,IIC_INP+1
	MOV   R5,#0
	MOV   R6,#10H
	CALL  iPRTLhw
	CALL  IIC_WME
	MOV   R6,#10H
	CALL  OUT_BUF
	CALL  IIC_WME
	MOV   TMP,#'A'
	MOV   TMP+1,#'b'
	MOV   R6,#10H
	MOV   R4,#TMP
	MOV   R2,#2
	MOV   R3,#0
	CALL  IIC_RQI
	CALL  IIC_WME
	CALL  DB_WAIT
	JMP   CRR
CREC5:  CJNE  R0,#5,CREC6     ; Prikaz 5 rizeni PWM
	CLR   FL_MOT
	SETB  MBRK
	MOV   A,IIC_INP+1
	MOV   PWM0,A
	JMP   CRR
CREC6:  CJNE  R0,#6,CREC7     ; Prikaz 6 motor skok
	CLR   FL_MOT
	SETB  MBRK
	MOV   A,IIC_INP+1
	MOV   PWM0,A
	CALL  DB_WAIT
	CALL  DB_WAIT
	CALL  DB_WAIT
	MOV   PWM0,#0
	CLR   MBRK
	JMP   CRR
CREC7:  CJNE  R0,#7,CREC8     ; Prikaz 7 sputeni motoru rychlosti
	%TOLED(0)
	CLR   FL_MOT
	MOV   POZ_INC,#0
	MOV   POZ_INC+1,#0
	MOV   POZ_INC+2,IIC_INP+1
	MOV   POZ_INC+3,IIC_INP+2
	CALL  M_RESP
	CLR   M_D_256
	SETB  FL_MOT
	JMP   CRR
)FI
CREC8:  CJNE  R0,#8,CREC9     ; Prikaz 8 nastaveni M_ENERI
	CLR   EA
	MOV   M_ENERI,IIC_INP+1
	MOV   M_ENERI+1,IIC_INP+2
	SETB  EA
	JMP   CRR
CREC9:  CJNE  R0,#9,CREC10    ; Prikaz 9 nastaveni M_SPDEI
	CLR   EA
	MOV   M_SPDEI,IIC_INP+1
	MOV   M_SPDEI+1,IIC_INP+2
	SETB  EA
	JMP   CRR
CREC10: CJNE  R0,#10,CREC11   ; Prikaz A nastaveni A_SYS
	CLR   EA
	MOV   A_PER,IIC_INP+1
	MOV   A_MSK,#1
	MOV   A_MSK+1,IIC_INP+2
	MOV   A_CPOS,#0
	MOV   A,A_PER
	JZ    CREC10R
	CALL  IIC_CER
	MOV   A_CPOS,#80H
CREC10R:SETB  EA
	JMP   CRR
CREC11: MOV   A,R0            ; Ping-Pong C0-FF
	XRL   A,#0C0H
	ANL   A,#0C0H
	JNZ   CREC12
	CALL  PPREC
	JMP   CRR
CREC12:

CRR:    CLR   ICF_SRC
	RET

INPUTc: CALL  SCANKEY
	JZ    INPUTc
	RET

DB_W_10:MOV   R0,#10H
	SJMP  DB_WAI1
DB_WAIT:MOV   R0,#0H
DB_WAI1:CALL  ST_WDC
	DJNZ  R1,DB_WAI1
	DJNZ  R0,DB_WAI1
	RET

IICTP1: CALL  DB_WAIT
	%TOLED (01H)
	MOV   P1,#01111111B
	CALL  DB_WAIT
	%TOLED (02H)
	MOV   P1,#00111111B
	CALL  DB_WAIT
	%TOLED (03H)
	MOV   P1,#11111111B
	CALL  DB_WAIT
	JMP   IICTP1

;=================================================================
; Hlavni smycky jednotlivych cinnosti

; Nacita do EEPROM historii chyb

ERR_HIS:CLR   F0
	MOV   R0,#0           ; Historie chyb
	CALL  EEPA_RD
	MOV   R1,#IIC_OUT+1
	MOV   R0,#6
ERR_HI1:XCH   A,@R1
	INC   R1
	DJNZ  R0,ERR_HI1
	MOV   IIC_OUT+1,ERRNUM
	CLR   F0
	JMP   EEPA_WR

; Chyba pri kontrole hardware pri zapnuti

ERR_HLD:MOV   A,#K_ALARM
	CALL  TESTKEY
	JZ    ERR_HLK
ERR_HLE:MOV   R4,ERRNUM
	MOV   R5,#0E0H
ERR_HLK:CLR   EBP
	SETB  ELED
	MOV   IEN0,#80H
	MOV   R6,#10H
	CALL  iPRTLhw
ERR_HLL:SETB  ELED
	MOV   PWM0,#0
	%WATCHDOG
	CALL  IIC_CER
	MOV   R6,#10H
	CALL  OUT_BUF
	JNZ   ERR_HLL
	CALL  DB_W_10
	CALL  ERR_HIS
	CALL  DB_W_10
	MOV   B,#1
	JMP   ERR_HL1

ISTOP2V:JMP   ISTOP2

; Priprava na spusteni cerpani
ISTART:	CLR   FL_HSPD
	CLR   FL_HSD
	CALL  CREC            ; Zpracovani prijatych zprav
	CALL  PPSYS           ; Volani Ping-Pongu
	JNZ   ISTART
	MOV   A,PPCNT
	ANL   A,#00FH
	JNZ   ISTART
	JB    FL_STRT,ISTART1
	CALL  MFSTART
	JB    F0,ISTOP2V      ; Inicializace se nepovedla
	MOV   A,SW_FLG
	SWAP  A
	CPL   A
	ANL   A,#SM_ATIM OR SM_AVOL
	JB    FL_PRES,ISTART0
	ORL   A,#SM_PRES
ISTART0:CALL  CL_SPC
	MOV   A,MOD_SLV
	ANL   A,#003H
	ORL   A,#M_RNORM
	MOV   MOD_SLV,A
	CLR   FL_NEND
	SETB  FL_STRT
	SETB  FL_TRTS
	SJMP  ISTART
ISTART1:
    %IF(%PARSAVE_FL)THEN(
	JNB   FL_PARSV,ISTART2
	MOV   R0,#MAX_TIM
	CALL  iLDR23i
	MOV   IIC_OUT+1,R2
	MOV   IIC_OUT+2,R3
	;MOV   IIC_OUT+3,MAX_PRE
	;MOV   IIC_OUT+4,MAX_PRE+1
	MOV   IIC_OUT+3,FLOW
	MOV   IIC_OUT+4,FLOW+1
	MOV   IIC_OUT+5,MAX_VOL
	MOV   IIC_OUT+6,MAX_VOL+1
	MOV   R0,#0C6H        ; Posledni navolene limity
	CALL  EEPA_WR
ISTART2:
    )FI
	MOV   A,SP            ; Test programoveho zasobniku
	XRL   A,#STACK
	JNZ   ISTOPE
	CALL  M_START         ; Fyzicke spusteni motoru
	SETB  FL_BEOK
	SETB  FL_EDCH

; Programova smycka po dobu cerpani
LSTART: CALL  CREC            ; Zpracovani prijatych zprav
	CALL  DILCDUS         ; Zobrazeni cislnych udaju
	CALL  DILED           ; Zobrazeni led diod
	CALL  SCANKEY         ; Cteni klavesnice
	MOV   DPTR,#SFT_D4    ; Tabulka funkci
	JNB   FL_NRD,LSTART0
	MOV   DPTR,#SFT_D5    ; Tabulka funkci bez praveho displaye
LSTART0:CALL  KEY_FNC         ; Vykonani prikazu podle A a DPTR
	ANL   A,#3
	ORL   PP_FLG,A        ; Nastaveni pipani
	CALL  PPSYS           ; Volani Ping-Pongu
	MOV   A,ERRNUM
	JNZ   ISTOP
	MOV   A,SP            ; Test programoveho zasobniku
	XRL   A,#STACK
	JNZ   ISTOPE
	MOV   A,MOD_SLV       ; Test spravnosti rezimu
	ANL   A,#0F0H
	XRL   A,#M_RNORM
	JNZ   ISTOPE
	JNB   FL_TIME,LSTART1
	MOV   R2,TIME
	MOV   R3,TIME+1
	MOV   R0,#MAX_TIM
	CALL  iLDR45i
	CALL  SUBi
	MOV   R2,#3
	MOV   R3,#0
	CALL  CMPi
	JC    LSTART2         ; Mene jak 3 min do konce
LSTART1:MOV   R0,#NEN_IRC
	CALL  iLDR23i
	MOV   R4,POZ_ACT+1
	MOV   R5,POZ_ACT+2
	CALL  CMPi
	JC    LSTART3
LSTART2:SETB  FL_NEND         ; Za 2 min konci davka
LSTART3:JB    FL_MOT,LSTART   ; Test behu motoru
ISTOPE:	MOV   A,ERRNUM
	JNZ   ISTOP
	MOV   A,#ERINTSW
	CALL  SET_ERR
ISTOP:  CALL  M_STOP
ISTOP1:	CALL  CREC            ; Zpracovani prijatych zprav
	CALL  PPSYS           ; Volani Ping-Pongu
	JNZ   ISTOP1
	MOV   A,PPCNT
	ANL   A,#00FH
	JNZ   ISTOP1
	JNB   FL_STRT,ISTOP2
	MOV   A,MOD_SLV
	ANL   A,#003H
	ORL   A,#M_SNORM
	MOV   MOD_SLV,A
	CALL  IRC2VOL
	CLR   FL_STRT
	MOV   R2,LST_VOL
	MOV   R3,LST_VOL+1
	CALL  ADDi
	MOV   LST_VOL,R4
	MOV   LST_VOL+1,R5
	MOV   A,ERRNUM
	XRL   A,#ERENDVO
	JNZ   ISTOP1
	MOV   LST_VOL,MAX_VOL
	MOV   LST_VOL+1,MAX_VOL+1
	SJMP  ISTOP1
ISTOP2: CALL  M_STOP
	CLR   FL_NEND
	JMP   C1

; Smycka pro rychloposuv

IHSPEED:MOV   KBDFL,#0FFH
	SETB  FL_BEOK
	CLR   FL_HSPD
	CLR   FL_MOT
IHSPEE0:CALL  CREC            ; Zpracovani prijatych zprav
	CALL  PPSYS
	JNZ   IHSPEE0
	MOV   A,PPCNT
	ANL   A,#0FH
	JNZ   IHSPEE0
	JB    FL_HSPD,IHSPEE4
	MOV   POZ_INC+0,#0
	MOV   POZ_INC+1,#0
	MOV   POZ_INC+2,#06FH ; 1000 ml/min pro BRAUN 50
	MOV   POZ_INC+3,#008H
	MOV   A,SPRTYP
	JB    ACC.7,IHSPEE1
	MOV   POZ_INC+2,#0F6H ; 1000 ml/min pro BRAUN 20
	MOV   POZ_INC+3,#011H
IHSPEE1:MOV   R4,#LOW  20     ; !!!!!!!!!!!!!!
	MOV   R5,#HIGH 20
	MOV   A,SPRTYP
	JB    ACC.3,IHSPEE2
	MOV   R4,#LOW  20
	MOV   R5,#HIGH 20
	CALL  VOL2IRC
IHSPEE2:CLR   FL_VOLA
	INC   R5
	MOV   STR_POZ,R4
	MOV   STR_POZ+1,R5
	JB    FL_NRD,IHSPEE3
	SETB  FL_HSD
	MOV   MOD_SLV,#M_SVOL
IHSPEE3:SETB  FL_HSPD
	SETB  FL_TRTS
	SJMP  IHSPEE0
IHSPEE4:CALL  M_START
	MOV   A,ERRNUM
	JNZ   EHSPEED

LHSPEED:JNB   FL_MOT,LHSPEE0
	MOV   A,POZ_ACT+2
	INC   A
	JNB   ACC.7,EHSPEED
LHSPEE0:CALL  PPSYS
	CALL  CREC            ; Zpracovani prijatych zprav
	CALL  DILCDUS         ; Zobrazeni cislnych udaju
	CALL  DILED           ; Zobrazeni led diod
	MOV   A,ERRNUM
	JNZ   EHSPEED
	MOV   R4,#0
	MOV   A,#K_RPOS
	CALL  TESTKEY
	JNZ   LHSPEE2
	INC   R4
LHSPEE2:MOV   A,#K_LPOS
	CALL  TESTKEY
	JNZ   LHSPEE3
	INC   R4
LHSPEE3:MOV   A,R4
	JZ    EHSPEED
	MOV   C,ACC.1
	MOV   FL_MOT,C
	JB    FL_HSPD,LHSPEED
EHSPEED:CALL  DILCD
	CLR   FL_HSPD
	MOV   NCCNT,#0
	MOV   KBDFL,#0FFH
	JMP   M_STOP

DILCDUS:MOV   A,MOD_SLV	      ; Zobrazeni udaju s testem nestabilnich modu
	JB    ACC.2,DILCDU1
DILCD:	JBC   FL_EDRF,DILCD1  ; Zobrazi udaje pro levy a pravy display
	JBC   FL_EDCH,DILCD1
	RET
DILCDU1:MOV   A,NCCNT
	ADD   A,#-NCMODEN
	JNC   DILCD
	ANL   MOD_SLV,#NOT 4
DILCD1: CLR   FL_EDCH
	MOV   R3,#0           ; Leva strana
	CALL  C_M2EDN
	CALL  EDDI
	MOV   R3,#1           ; Prava strana
	CALL  C_M2EDN
	CALL  EDDI
DILCDR: RET

ICOMRD: CALL  M_STOP          ; Prijem dat z pcitace
	MOV   SP,#STACK
	CLR   F0
	MOV   R0,#95H         ; Cteni serioveho cisla
	CALL  EEPA_RD
	MOV   R4,IIC_OUT+1
	MOV   R5,IIC_OUT+2
	MOV   R6,#10H
	CALL  iPRTLhw
	MOV   R6,#10H
	CALL  OUT_BUF
LCOMRD: MOV   MOD_SLV,#M_COMRD
	CALL  CREC            ; Zpracovani prijatych zprav
	CALL  PPSYS           ; Volani Ping-Pongu
	MOV   A,#K_ALARM
	CALL  TESTKEY
	JNZ   ECOMRD

	SJMP  LCOMRD
ECOMRD: SETB  FL_EDCH
	JMP   ST_USRM

; Podmineny vstup
ISERVCI:JB    FL_SERV,ISERV1
	CLR   A
	RET
; Servisni rezim
ISERV:  SETB  FL_SERV
ISERV1: MOV   SP,#STACK
	CALL  M_STOP
	MOV   EDNUM,#O_SMOD
LSERVT:	MOV   MOD_SLV,#M_SERV
LSERV:  CALL  PPSYS
	CALL  CREC            ; Zpracovani prijatych zprav
	CALL  DILEDER
	MOV   A,#K_ALARM
	CALL  TESTKEY
	JNZ   LSERV0
	MOV   EDNUM,#O_SMOD
	MOV   EDPOS,#10H
	CALL  CLR_ER0
	CALL  SCANKEY
	MOV   DPTR,#SFT_S0
	MOV   R7,A
	SJMP  LSERV5
LSERV0:	MOV   A,MOD_SLV
	ANL   A,#00FH
	RL    A
	RL    A
	ADD   A,#O_MSERV
	MOV   R7,A
	CALL  G_EDPA
	MOV   A,R2
	XCH   A,EDNUM
	MOV   EDPOS,#10H
	CJNE  A,#O_SMOD,LSERV1
	MOV   A,#K_V_INI
	SJMP  LSERV2
LSERV1:	CALL  SCANKEY
	JNZ   LSERV2
	MOV   A,#K_V_RUN
LSERV2:	XCH   A,R7
	ADD   A,#2
	CALL  G_EDPA
	MOV   DPL,R2
	MOV   DPH,R3
LSERV5:	CALL  SEL_FNC
	ANL   A,#3
	ORL   PP_FLG,A
	MOV   C,FL_EDRF
	ORL   C,FL_EDCH
	JNC   LSERV6
	CLR   FL_EDRF
	CLR   FL_EDCH
	CALL  EDDI
LSERV6:	JB    FL_SERV,LSERV
ESERV:  MOV   A,#ERINTSM
	CALL  SET_ERR
	SJMP  LSERV

QSERV:  MOV   SP,#STACK
	MOV   MOD_SLV,#M_SNORM
	JMP   C1

LS_FN1: CALL  EDADD           ; Editace EEPROM
LS_FN10:MOV   R2,#TMP
	MOV   R3,#1
	CALL  EEPR_RD
	MOV   A,#B_OK
	RET

LS_FN11:CALL  EEPR_WR
	MOV   A,#B_OK
	RET

LS_FN12:MOV   TMP,#0
	SJMP  LS_FN10

LS_FN20:MOV   TMP,#0          ; Kalibrace CAP cidla
	MOV   A,#B_OK
	RET

LS_FN21:MOV   TMP,#1
	MOV   R4,#LOW  302
	MOV   R5,#HIGH 302
LS_FN25:MOV   POZ_INC+2,#0
	MOV   POZ_INC+3,#18H
LS_FN26:MOV   POZ_INC,#0
	MOV   POZ_INC+1,#0
	CLR   FL_VOLA
	CALL  M_STAR0
	MOV   A,#B_OK
	RET

LS_FN22:MOV   A,TMP
	MOV   R7,A
	JZ    LS_FN29
	MOV   A,POZ_ACT+2
	INC   A
	JNZ   LS_FN29
	MOV   A,POZ_ACT+1
	ADD   A,#20
	JNC   LS_FN29
	MOV   POZ_INC+2,#64
	MOV   POZ_INC+3,#0
	MOV   A,R7
	JNB   ACC.0,LS_FN23
	MOV   A,POZ_ACT+1
	ADD   A,#10
	JNC   LS_FN29
	CLR   FL_MOT
	MOV   A,TMP
	ORL   A,#80H
	INC   TMP
	MOV   R0,#OUT_MSG
	MOV   @R0,A
	SJMP  LS_FN29
LS_FN23:MOV   R0,#OUT_MSG
	MOV   A,@R0
	JNZ   LS_FN29
	MOV   A,PPCNT
	ANL   A,#0FH
	JNZ   LS_FN29
	INC   TMP
	CJNE  R7,#8,LS_FN24
	MOV   TMP,#0
LS_FN29:CLR   A
	RET
LS_FN24:MOV   R4,#LOW  1034
	MOV   R5,#HIGH 1034
	JMP   LS_FN25

LS_FS2: JNB   FL_MOT,LS_FS21
	CALL  M_STOP
LS_FS20:MOV   A,#B_OK
	RET

LS_FS21:CALL  CLR_ER0
	MOV   A,TMP
	CJNE  A,#7,LS_FS20
	MOV   R0,#OUT_MSG
	MOV   @R0,#87H
	MOV   TMP,#0
	MOV   A,#B_ERR
	RET

LS_FS40:MOV   R0,#AD_PRE      ; Kalibrace tenzometru
	CLR   EA
	CALL  iLDR45i
	SETB  EA
	MOV   R0,#INT_PRE
	CALL  iSVR45i
	MOV   A,#B_ERR
	RET

LS_FS42:MOV   R0,#SLP_PRE     ; Odpojeni/pripojeni tenzometru
	CALL  iLDR45i
	ORL   A,R4
	MOV   R4,#01H
	JZ    LS_FS43
	CLR   A
	MOV   R4,A
	MOV   R5,A
LS_FS43:MOV   KBDFL,#0FFH
	SJMP  LS_FS46

LS_FS45:CALL  AD2SUB1
	MOV   R2,#LOW  100
	MOV   R3,#HIGH 100
	CALL  DIVihf
	MOV   A,R1
	CPL   A
	INC   A
	CALL  SHRi
LS_FS46:MOV   R0,#SLP_PRE
	CALL  iSVR45i
	MOV   R0,#IIC_OUT+1
	MOV   R1,#INT_PRE
	MOV   R2,#4
	CALL  MOVsi
	CLR   F0
	MOV   R0,#8EH
	CALL  EEPA_WR
	MOV   A,#B_ERR
	RET

LS_FS48:MOV   R5,#LOW  200    ; Pomale zrychlovani
	MOV   R6,#HIGH 200
	JNB   FL_MOT,LS_FS49
	MOV   R4,POZ_INC+2
	MOV   R5,POZ_INC+3
	MOV   R2,#80H
	MOV   R3,#1
	CALL  MULi
LS_FS49:MOV   POZ_INC+2,R5
	MOV   POZ_INC+3,R6
	JMP   LS_FS71

LS_FS70:MOV   POZ_INC+2,R2    ; Test velmi nizke ryclosti
	MOV   POZ_INC+3,R3
LS_FS71:MOV   R4,#LOW  4000
	MOV   R5,#HIGH 4000
	JMP   LS_FN26

LS_F8I: MOV   TMP,SPRTYP      ; Nastaveni typu strikacky
	CLR   A
	RET

LS_F8A: MOV   A,R2            ; R2 xoruje editovanou polozku
	MOV   R0,A            ; podle tabulky [EDNUM]
	MOV   A,#O_EDPTR
	CALL  G_EDPAR
	MOV   A,R2
	XCH   A,R0
	XRL   A,@R0
	MOV   @R0,A
LS_F8A1:SETB  FL_EDCH
	MOV   A,#B_OK
	RET

LS_F8B: MOV   A,TMP
	MOV   C,ACC.7
	MOV   F0,C
	JNC   LS_F8B1
	SWAP  A
LS_F8B1:MOV   R1,A
	ADD   A,R2
	ANL   A,#7
	XCH   A,R1
	ANL   A,#NOT 7
	ORL   A,R1
	JNB   F0,LS_F8B2
	SWAP  A
LS_F8B2:MOV   TMP,A
	SJMP  LS_F8A1

LS_F8S: CALL  LS_F8S1
	MOV   IIC_OUT+1,TMP
	MOV   IIC_OUT+2,CFG_FLG
	MOV   R1,#OMS_SCH     ; OUT_MSG zmena typu strikacky
	CALL  LS_F8S2
	JB    F0,LS_F8I
	SJMP  LS_F8A1

LS_F8S1:CLR   F0
	MOV   R0,#87H         ; Parametry strikacky a konfigurace
	JMP   EEPA_RD

LS_F8S2:MOV   R0,#OUT_MSG     ; Zprava v R1 OUT_MSG
	MOV   A,@R0
	JZ    LS_F8S3
	SETB  F0
	RET
LS_F8S3:MOV   A,R1
	MOV   @R0,A
	INC   R0
	MOV   @R0,TMP
	CLR   F0
	MOV   R0,#87H         ; Parametry strikacky a konfigurace
	JMP   EEPA_WR

LS_F9I: MOV   TMP,CFG_FLG     ; Nastaveni konfigurace
	CLR   A
	MOV   TMP+1,A
	RET

LS_F9S: CALL  LS_F8S1
	MOV   IIC_OUT+1,SPRTYP
	MOV   IIC_OUT+2,TMP
	MOV   R1,#OMS_CCH     ; OUT_MSG zmena konfigurace
	CALL  LS_F8S2
	JB    F0,LS_F9I
	MOV   CFG_FLG,TMP
	SJMP  LS_F8A1

USR_SCH:MOV   TMP,SPRTYP      ; Uzivatelska zmena strikacky
	JNB   FL_USCH,USR_SC1
	CALL  LS_F8B
	CALL  LS_F8S
USR_SC1:MOV   EDPOS,#10H
	MOV   R4,TMP
	CALL  EDDI_ST
	SETB  FL_HSD
	CLR   FL_EDCH
	MOV   KBDTIMR,#0FFH
	MOV   A,#B_OK
	RET

;=================================================================
; Ping-Pong mezi procesory

PPRECE: MOV   A,#ERCOMM
	CALL  SET_ERR
	RET

; Zpracovani prijmute zpravy Ping-Pongu

PPREC:  CALL  PPINC           ; Zvyseni citace
	CJNE  A,IIC_INP,PPRECE
	MOV   A,S_RLEN        ; Kontrola prijate delky
	CJNE  A,#SLAV_BL,PPRECE
	MOV   R0,#IIC_INP
	CALL  XOR_SU0         ; Kontrola prijateho xorsumu
	XRL   A,@R0
	JNZ   PPRECE
	MOV   PPTIMR,#PPT_MAX ; Natazeni watchdogu PP
	CALL  PPSW
	RET

PPSTART:MOV   PPCNT,#0        ; Spusteni PP
	MOV   PPTIMR,#PPT_MAX ; Natazeni watchdogu PP
	MOV   IIC_OUT,#0BFH
	JMP   SND_OUT

PPSYS:  MOV   A,PPTIMR        ; Test casove prodlevy
	ADD   A,#-PPT_MAX+PPT_SND
	MOV   A,#80H
	JC    PPSRET          ; Nenadesel jeste cas
PPSEND: JNB   PPCNT.0,PPSRET  ; Cekam pouze na odpoved
	CALL  PPINC
	MOV   IIC_OUT,A
	CALL  PPSW
SND_OUT:MOV   R0,#IIC_OUT
	CALL  XOR_SU0         ; Zabezpeceni xorsumem
	MOV   @R0,A

%IF (%DEBUG_FL) THEN (
	MOV   A,PPCNT
	SWAP  A
	XRL   A,PPCNT
	ANL   A,#03H
	JNZ   SND_OU1
)FI
SND_OU0:
%IF (%DEBUG_FL) THEN (
	CALL  IIC_CER
	MOV   R6,#10H
	MOV   R4,#IIC_OUT
	MOV   R2,#SLAV_BL
	MOV   R3,#0
	CALL  IIC_RQI
	JNZ   SND_OU0
	CALL  IIC_WME
)FI
SND_OU1:CALL  IIC_CER
	MOV   R6,#30H
	MOV   R4,#IIC_OUT
	MOV   R2,#SLAV_BL
	MOV   R3,#0
	CALL  IIC_RQI
	JNZ   SND_OU1
	CALL  IIC_WME
PPSRET:	RET

PPINC:  MOV   A,PPCNT
	INC   A
	ANL   A,#03FH
	ANL   PPCNT,#0C0H
	ORL   PPCNT,A
	ORL   A,#0C0H
	RET

XOR_SU0:MOV   R2,#SLAV_BL-1
XOR_SUM:CLR   A
XOR_SU1:MOV   R3,A
	MOV   A,@R0
	INC   R0
	XRL   A,R3
	INC   A
	DJNZ  R2,XOR_SU1
	RET

PPSW:   MOV   A,PPCNT
	ANL   A,#00FH
	RL    A
	MOV   R0,A
	ADD   A,#PPSWT-PPSWk1
	MOVC  A,@A+PC
PPSWk1: PUSH  ACC
	MOV   A,R0
	ADD   A,#PPSWT-PPSWk2+1
	MOVC  A,@A+PC
PPSWk2: PUSH  ACC

PPSWRET:RET

PPSWT:  %W    (PPSW0)
	%W    (PPSW1)
	%W    (PPSW2)
	%W    (PPSW3)
	%W    (PPSW4)
	%W    (PPSW5)
	%W    (PPSW6)
	%W    (PPSWRET)
	%W    (PPSW8)
	%W    (PPSW9)
	%W    (PPSWA)
	%W    (PPSWB)
	%W    (PPSWRET)
	%W    (PPSWRET)
	%W    (PPSWE)
	%W    (PPSWRET)

PPSW0:  SETB  AD_EN           ; Povoleni prevadeni ADC
	CLR   FL_TRTS
	CLR   EA
	MOV   IIC_OUT+1,POZ_ACT
	MOV   IIC_OUT+2,POZ_ACT+1
	MOV   IIC_OUT+3,POZ_ACT+2
	MOV   IIC_OUT+4,FLOW
	MOV   IIC_OUT+5,FLOW+1
	MOV   IIC_OUT+6,ERRNUM
	SETB  EA
	RET

PPSW1: 	MOV   R4,IIC_INP+5    ; Pozice z kapacitniho cidla
	MOV   R5,IIC_INP+6    ; v 256*irc od koncoveho spinace
	MOV   R0,#CAP_IRC
	CALL  iSVR45i
	MOV   C,FL_STRT
	ANL   C,/FL_TRTS
	ORL   C,FL_MOT
	ORL   C,FL_HSPD
	JC    PPSW110
	CLR   FL_VOLA
	MOV   A,#O_SPRE       ; Kde strikacka konci
	CALL  G_SPR
	CALL  SUBi
	JC    PPSW105
	MOV   STR_POZ,R4      ; Pocet 256*irc ktere je mozno
	MOV   STR_POZ+1,R5    ; z dane strikacky vydavkovat
	JNB   FL_VOL,PPSW120  ; Neni zadan limit objemu
	MOV   R4,MAX_VOL
	MOV   R5,MAX_VOL+1
	MOV   R2,LST_VOL
	MOV   R3,LST_VOL+1
	CALL  SUBi
	JC    PPSW105
	CALL  VOL2IRC
	JB    F0,PPSW120      ; Objemovy limit je jeste daleko
	MOV   R2,STR_POZ
	MOV   R3,STR_POZ+1
	CALL  CMPi            ; Objem ve strikacce je mensi
	JNC   PPSW120         ; nez nastaveny limit
	SETB  FL_VOLA
	MOV   STR_POZ,R4
	MOV   STR_POZ+1,R5
	SJMP  PPSW120
PPSW105:CLR   A               ; Davka jiz vydavkovana
	MOV   STR_POZ,A
	MOV   STR_POZ+1,A
	SJMP  PPSW120
PPSW110:MOV   A,IIC_INP+2
	JNB   ACC.1,PPSW112   ; Otevrena spojka
	MOV   A,#ERSPRGE
	CALL  SET_ERR
PPSW112:

PPSW120:CALL  CHK_IRC
	MOV   R0,#OUT_MSG
	MOV   A,@R0
	XRL   A,#OMS_CER
	JZ    PPSW125
	MOV   A,ERRNUM
	JNZ   PPSW125
	MOV   A,IIC_INP+3     ; ERRNUM z druheho procesoru
	JZ    PPSW125
	JB    ACC.7,VER_HLD
	CALL  SET_ERR
PPSW125:MOV   A,IIC_INP+1     ; HW_FLG z druheho procesoru
	MOV   C,ACC.4         ; Priznak vnejsiho napajeni
	CPL   C
	MOV   FL_BAT,C
	MOV   A,IIC_INP+4
	XRL   A,SPRTYP
	JNZ   PPSW150
	RET
PPSW150:JB    FL_MOT,PPSW160  ; Strikacka se nesmi menit
	JB    FL_STRT,PPSW160 ; kdyz se cerpa
	ANL   A,#077H
	JNZ   PPSW160
	MOV   SPRTYP,IIC_INP+4
	RET
PPSW160:MOV   A,#ERSPR
	JMP   SET_ERR
VER_HLD:MOV   ERRNUM,A
	JMP   ERR_HLE

PPSW2:  MOV   IIC_OUT+6,MOD_SLV
	MOV   R0,#IIC_OUT+2
	MOV   R1,#OUT_MSG
	MOV   R2,#3
	CALL  MOVsi
	MOV   R1,#OUT_MSG
	MOV   @R1,#0
	MOV   A,IIC_OUT+2
	XRL   A,#OMS_SCH
	JNZ   PPSW21
	MOV   SPRTYP,IIC_OUT+3
	MOV   A,MOD_SLV
	XRL   A,#M_SERV
	ANL   A,#0F0H
	JZ    PPSW21
	JB    FL_USCH,PPSW21
	MOV   A,#ERINTSM
	CALL  SET_ERR
PPSW21:	JMP   PPSWPPF

PPSW3:  MOV   R4,IIC_INP+1
	MOV   R5,IIC_INP+2
	MOV   A,R5
	CPL   A
	JZ    PPSW356
	MOV   R2,#LOW  CINT_BA
	MOV   R3,#HIGH CINT_BA
	CALL  SUBi
	JNC   PPSW353
	CLR   A
	MOV   R4,A
	MOV   R5,A
PPSW353:MOV   R2,#LOW  CSLP_BA
	MOV   R3,#HIGH CSLP_BA
	CALL  DIVi
	MOV   R0,#ACU_BAT
	CALL  iSVR45i
	CLR   FL_BALO
	MOV   R2,#LOW  CALR_BA
	MOV   R3,#HIGH CALR_BA
	CALL  CMPi
	MOV   A,#2
	JNC   PPSW355
	MOV   C,FL_BAT
	MOV   FL_BALO,C
	MOV   R2,#LOW  CLO_BA
	MOV   R3,#HIGH CLO_BA
	CALL  CMPi
	MOV   A,#1
	JNC   PPSW355
	DEC   A
	JNB   FL_BAT,PPSW355
	MOV   A,#ERRBAT
	CALL  SET_ERR
PPSW355:MOV   R0,#ST_BAT
	MOV   @R0,A
PPSW356:RET

PPSW4:  CALL  AD2PRE
	CLR   EA
	MOV   IIC_OUT+1,TIME
	MOV   IIC_OUT+2,TIME+1
	MOV   IIC_OUT+3,PRE_ACT
	MOV   IIC_OUT+4,PRE_ACT+1
	SETB  EA
	JB    FL_STRT,PPSW450
	MOV   IIC_OUT+5,LST_VOL
	MOV   IIC_OUT+6,LST_VOL+1
	RET
PPSW450:CALL  IRC2VOL
	MOV   R2,LST_VOL
	MOV   R3,LST_VOL+1
	CALL  ADDi
	MOV   IIC_OUT+5,R4
	MOV   IIC_OUT+6,R5
	RET

PPSW5:  MOV   A,ERRNUM
	JZ    PPSW550
	JB    FL_ERRW,PPSW550
	SETB  FL_ERRW
	XRL   A,#ERINT
	ANL   A,#0F0H
	JNZ   PPSW550
	CALL  ERR_HIS
PPSW550:RET

PPSW6:  CLR   EA
	MOV   IIC_OUT+2,M_ENERG
	MOV   IIC_OUT+3,M_ENERG+1
	MOV   IIC_OUT+5,SW_FLG
	MOV   IIC_OUT+6,SW1_FLG
	SETB  EA
PPSWPPF:MOV   IIC_OUT+1,PP_FLG
	ANL   PP_FLG,#NOT 00100011B
	RET

PPSW8:  MOV   R0,#MAX_TIM
	CALL  iLDR23i
	MOV   IIC_OUT+1,R2
	MOV   IIC_OUT+2,R3
	MOV   IIC_OUT+3,MAX_PRE
	MOV   IIC_OUT+4,MAX_PRE+1
	MOV   IIC_OUT+5,MAX_VOL
	MOV   IIC_OUT+6,MAX_VOL+1
	CLR   A
	JB    FL_TIME,PPSW811
	MOV   IIC_OUT+1,A
	MOV   IIC_OUT+2,A
PPSW811:JB    FL_VOL,PPSW812
	MOV   IIC_OUT+5,A
	MOV   IIC_OUT+6,A
PPSW812:RET

PPSW9:  JB    FL_MOT,PPSW9R
	MOV   A,MOD_SLV
	XRL   A,#M_COMRD
	JNZ   PPSW9R
	JNB   FL_SETL,PPSW9R      ; Nastaveni parametru z PC
	MOV   R1,#IIC_INP+1
	MOV   R0,#MAX_TIM
	MOV   R2,#SM_TIME
	CALL  GET_LIM
	MOV   R0,#MAX_PRE
	MOV   R2,#SM_PRES
	CALL  GET_LIM
	MOV   R0,#MAX_VOL
	MOV   R2,#SM_VOL
	CALL  GET_LIM
	SETB  FL_BEER
PPSW9R:	RET

PPSWA:  SJMP  PPSWPPF

PPSWB:	MOV   R4,IIC_INP+1    ; Pozice z kapacitniho cidla
	MOV   R5,IIC_INP+2    ; v 256*irc od koncoveho spinace
	MOV   R0,#CAP_IRC     ; pomocny prenos
	CALL  iSVR45i
	CLR   FL_SETL
	MOV   A,ERRNUM
	JNZ   PPSWB20
	MOV   A,IIC_INP+3     ; Nove ERRNUM z druheho procesoru
	JZ    PPSWB20
	JNB   ACC.7,PPSWB19
	JMP   VER_HLD
PPSWB19:CALL  SET_ERR
PPSWB20:JB    FL_MOT,PPSWB30
	MOV   A,MOD_SLV
	XRL   A,#M_COMRD
	JNZ   PPSWBR
	MOV   A,IIC_INP+4     ; FL_SETL
	JZ    PPSWBR
	SETB  FL_SETL	      ; Nastaveni parametru z PC
	MOV   FLOW,IIC_INP+5
	MOV   FLOW+1,IIC_INP+6
PPSWBR:	RET
PPSWB30:CALL  CHK_IRC
	RET

PPSWE:  MOV   R0,#AD_VCC+1    ; Kontrola napajeciho napeti
	MOV   A,@R0
	ADD   A,#-2DH
	ADD   A,#2DH-34H
	JNC   PPSWE50
	MOV   A,#ERVCC
	CALL  SET_ERR
PPSWE50:MOV   IIC_OUT+6,SW1_FLG
	JMP   PPSWPPF

GET_LIM:MOV   A,@R1
	MOV   R4,A
	INC   R1
	MOV   A,@R1
	MOV   R5,A
	INC   R1
	MOV   A,R2
	CPL   A
	ANL   SW_FLG,A
	MOV   A,R4
	ORL   A,R5
	JZ    GET_LI1
	MOV   A,R2
	ORL   SW_FLG,A
GET_LI1:JMP   iSVR45i

;=================================================================
; Uzivatelske rozhrani

IC_UI_D SEGMENT DATA
IC_UI_C SEGMENT CODE

; Kody ruznych pipnuti
B_NUL   EQU   00H
B_OK    EQU   01H
B_ERR   EQU   02H

; Kody jednotlivych klaves
K_ALARM EQU   04H
K_LU3   EQU   0EH
K_LU2   EQU   12H
K_LU1   EQU   16H
K_LU0   EQU   1AH
K_LD3   EQU   0DH
K_LD2   EQU   11H
K_LD1   EQU   15H
K_LD0   EQU   19H
K_RU3   EQU   02H
K_RU2   EQU   06H
K_RU1   EQU   0AH
K_RU0   EQU   0BH
K_RD3   EQU   01H
K_RD2   EQU   05H
K_RD1   EQU   09H
K_RD0   EQU   07H
K_START EQU   18H
K_STOP  EQU   1CH
K_NUL   EQU   1BH
K_TIME  EQU   08H
K_PRES  EQU   0CH
K_VOL   EQU   03H
K_LTIME EQU   17H
K_LPRES EQU   13H
K_LVOL  EQU   0FH
K_LPOS  EQU   10H
K_RPOS  EQU   14H
; Virtualni klavesy
K_SPEED EQU   1CH*2+K_RPOS
K_NTIME EQU   1CH*3+K_LTIME
K_NPRES EQU   1CH*3+K_LPRES
K_NVOL  EQU   1CH*3+K_LVOL
K_NATIM EQU   1CH*3+K_TIME
K_NAVOL EQU   1CH*3+K_VOL
; Virtualni programove klavesy
K_V_INI EQU   -2
K_V_RUN EQU   -3

RSEG    IC_UI_D

EDNUM:  DS    1               ; Ukazatel do tabulky EDPAR
EDPOS:  DS    1               ; Pozice vypisu
NCCNT:  DS    1               ; Citac doby bez povelu v 0.01 min

RSEG    IC_UI_C

KEY_FNC:JZ    SEL_FNR         ; Zpracovani stisknute klavesy
	MOV   NCCNT,#0        ; Nastaveni citace necinnosti
	MOV   R7,A
SEL_FNC:                      ; Vyber funkce a R23 podle R7
SEL_FN2:CLR   A
	MOVC  A,@A+DPTR
	JZ    SEL_FNR
	INC   DPTR
	INC   A
	JZ    SEL_FN4
	DEC   A
	XRL   A,R7
	JNZ   SEL_FN3
	CALL  cLDR23i
	JMP   cJMPDPP
SEL_FN3:INC   DPTR
	INC   DPTR
	INC   DPTR
	INC   DPTR
	SJMP  SEL_FN2
SEL_FN4:CALL  cLDR23i
	MOV   DPL,R2
	MOV   DPH,R3
	JMP   SEL_FN2
SEL_FNR:RET

cJMPDPP:CLR   A
	MOVC  A,@A+DPTR
	INC   DPTR
	MOV   R1,A
	CLR   A
	MOVC  A,@A+DPTR
	INC   DPTR
	MOV   DPH,A
	MOV   DPL,R1
	CLR   A
	JMP   @A+DPTR

; Zobrazeni LED diod

DILED:  MOV   A,TIMRI
	ANL   A,#0F8H
	CLR   F0
	JZ    DILED1
	SETB  F0
DILED1:	MOV   A,MOD_SLV       ; Zobrazeni stavu druheho displeje
	JNB   ACC.2,DILED10
	CLR   A
	JNB   F0,DILEDk1
	MOV   A,MOD_SLV
DILED10:ANL   A,#3
	ADD   A,#DILEDT1-DILEDk1
	MOVC  A,@A+PC
DILEDk1:MOV   TMP,A
	MOV   TMP+1,#0        ; Zobrazeni stavu limitu
	JNB   FL_TIME,DILED20
	ORL   TMP,#02H
DILED20:JNB   FL_PRES,DILED21
	ORL   TMP,#40H
DILED21:JNB   FL_VOL,DILED22
	ORL   TMP,#01H
DILED22:MOV   A,#08H          ; Stav motoru
	JNB   FL_MOT,DILED23
	MOV   A,#04H
DILED23:ORL   TMP+1,A
	MOV   A,SPRTYP
	JB    ACC.3,DILED25
	MOV   C,ACC.7
	MOV   A,#40H
	JC    DILED24
	MOV   A,#80H
DILED24:ORL   TMP+1,A
DILED25:MOV   A,#002H         ; Stav napajeni
	JNB   FL_BAT,DILED26
	JNB   F0,DILED27      ; !!!
	MOV   A,#001H
DILED26:ORL   TMP+1,A
DILED27:
	JNB   FL_BAT,DILED50
	MOV   A,NCCNT
	ADD   A,#-NCLEDOF
	JNC   DILED50
%IF (0) THEN (
	MOV   A,TIMRI         ; Doom rezim
	DEC   A
	DEC   A
	JZ    DILED52
	DEC   A
	JNZ   DILED51
)ELSE(
	ANL   TMP,#0
	ANL   TMP+1,#0FH
	JMP   DILED52
)FI
DILED50:ORL   TMP,#84H
	ORL   TMP+1,#30H
	SJMP  DILED52

DILED51:DEC   A
	JZ    DILED52
	ANL   TMP,#0
	ANL   TMP+1,#0

DILED52:
	MOV   A,TMP
	MOV   DPTR,#LED0
	MOVX  @DPTR,A
	MOV   A,TMP+1
	MOV   DPTR,#LED1
	MOVX  @DPTR,A

DILEDER:MOV   A,ERRNUM        ; Vyhodnoceni chyb
	MOV   R1,A
	JZ    DILED32
	SETB  ELED
	SWAP  A
	ANL   A,#7
	MOV   R0,A
	MOV   A,#80H
DILED31:RL    A
	DJNZ  R0,DILED31
	MOV   R1,A
DILED32:MOV   A,TIMRI
	ANL   A,#0F8H
	JZ    DILED38
	JNB   FL_NEND,DILED33 ; Blizi se konec davky
	MOV   A,R1
	ORL   A,#10H
	MOV   R1,A
DILED33:JNB   FL_BALO,DILED38 ; Stav baterii
	MOV   A,R1
	ORL   A,#04H
	MOV   R1,A
DILED38:MOV   A,R1
	MOV   DPTR,#LED2
	MOVX  @DPTR,A
	CLR   F0
	RET

DILEDT1:DB    00H
	DB    10H
	DB    08H
	DB    20H

; Nalezeni EDNUM pro levou nebo pravou cast displeje podle R3
; a MOD_SLV

C_M2EDN:MOV   A,MOD_SLV
	ANL   A,#0FH
	RL    A
	ADD   A,R3
	RL    A
	MOV   R0,A
	ADD   A,#C_M2EDt-C_M2EDk
	CJNE  A,#C_M2EDe-C_M2EDk,C_M2ED1
C_M2ED1:JNC   C_M2ED2
	MOVC  A,@A+PC
C_M2EDk:MOV   EDNUM,A
	MOV   A,R0
	ADD   A,#C_M2EDt-C_M2EDl+1
	MOVC  A,@A+PC
C_M2EDl:MOV   EDPOS,A
	RET
C_M2ED2:MOV   EDNUM,#O_EDNIL
	SETB  ELED
	MOV   A,#ERINTSW
	CALL  SET_ERR
	RET

C_M2EDt:DB    O_EDFL,10H,O_EDNIL,0
	DB    O_EDFL,10H,O_DITI,18H
	DB    O_EDFL,10H,O_DIPR,18H
	DB    O_EDFL,10H,O_DIVO,18H
	DB    O_EDFL,10H,O_EDNIL,0
	DB    O_EDFL,10H,O_EDLTI,18H
	DB    O_EDFL,10H,O_EDLPR,18H
	DB    O_EDFL,10H,O_EDLVO,18H
	DB    O_DIFL,10H,O_EDNIL,0
	DB    O_DIFL,10H,O_DITI,18H
	DB    O_DIFL,10H,O_DIPR,18H
	DB    O_DIFL,10H,O_DIVO,18H
	DB    O_DIFL,10H,O_EDNIL,0
	DB    O_DIFL,10H,O_DILTI,18H
	DB    O_DIFL,10H,O_DILPR,18H
	DB    O_DIFL,10H,O_DILVO,18H
C_M2EDe:

; Modifikace cisla pro MOD_SLV registry R23 s kontrolou limitu

ED_LMOD:CALL  C_M2EDN         ; Naplneni EDNUM a EDPOS
	MOV   A,R2
	RL    A
	MOV   R4,A
	MOV   A,#O_EDMDT
	CALL  G_EDPAR         ; Zjisteni adresy modifikacni tabulky
	MOV   A,R2
	JZ    EDADDLR         ; Udaj nelze editovat
	ADD   A,R4
	CALL  G_EDPA

; Modifikace cisla [[EDNUM]] registry R23 s kontrolou limitu

EDADDL: CALL  GET_EDN
	MOV   A,R0
	JZ    EDADDLR         ; Ukazatel na prazdnou pozici
	CALL  ADDi
	JB    OV,EDADDLE
	MOV   A,#O_EDMIN
	CALL  G_EDPAR
	CALL  CMPi
	MOV   C,OV
	XRL   A,PSW
	JB    ACC.7,EDADDLE
	MOV   A,#O_EDMAX
	CALL  G_EDPAR
	CALL  CMPi
	JZ    EDADDS
	MOV   C,OV
	XRL   A,PSW
	JB    ACC.7,EDADDS
EDADDLE:
ERR_KEY:MOV   KBDFL,#0FFH     ; Stisk chybne klavesy
	MOV   A,#B_ERR
EDADDLR:RET

; Modifikace cisla [[EDNUM]] registry R23

EDADD:  CALL  GET_EDN
	CALL  ADDi
EDADDS:	CALL  SET_EDN
	SETB  FL_EDCH
	MOV   A,#B_OK
	RET

; Dekadicke zobrazeni cisla podle [EDNUM]

EDDI:
%IF (0) THEN (
	MOV   R5,EDNUM
	MOV   R4,EDPOS
	MOV   R6,EDPOS
	CALL  iPRTLhw
	JMP   EDDI12
)FI
	CALL  GET_EDN
	MOV   A,#O_EDDF
	CALL  G_EDPAR
	MOV   A,R2
	MOV   R7,A
	ANL   A,#0F0H
	JZ    EDDI20
EDDI11: MOV   R6,EDPOS
	CALL  iPRTLi          ; Zobrazeni dekadickeho cisla
EDDI12: CALL  IIC_CER
	MOV   R6,EDPOS
	CALL  OUT_BUF
	JNZ   EDDI12
	CALL  IIC_WME
	MOV   A,#B_OK
	RET
EDDI20: CJNE  R7,#1,EDDI30
	CALL  GET_EDN         ; Zobrazeni casu HH:MM
	MOV   R2,#LOW  60
	MOV   R3,#HIGH 60
	CALL  DIVi
	MOV   R6,EDPOS
	MOV   R7,#0A4H
	CALL  iPRTLi
	CALL  GET_EDN
	MOV   R2,#LOW  60
	MOV   R3,#HIGH 60
	CALL  MODi
	MOV   R6,EDPOS
	INC   R6
	INC   R6
	MOV   R7,#0A4H
	CALL  iPRTLi
EDDI21:	MOV   R0,#WR_BUF+1
	MOV   A,@R0
	ORL   A,#lcdh
	MOV   @R0,A
	JMP   EDDI12
EDDI30: CJNE  R7,#3,EDDI40    ; Zobrazeni vydavkovaneho objemu
	JB    FL_HSPD,EDDI32
	MOV   R4,LST_VOL
	MOV   R5,LST_VOL+1
	JNB   FL_MOT,EDDI31
	CALL  IRC2VOL
	MOV   R2,LST_VOL
	MOV   R3,LST_VOL+1
	CALL  ADDi
EDDI31:	MOV   R7,#0C5H
	SJMP  EDDI11
EDDI32: CALL  IRC2VOL
	SJMP  EDDI31
EDDI40: CJNE  R7,#4,EDDI50    ; Zobrazeni EEPROM
	XCH   A,R4
	XCH   A,R5
	XCH   A,R4
	MOV   R6,EDPOS
	CALL  iPRTLhw
	SJMP  EDDI21
EDDI50: CJNE  R7,#5,EDDI60    ; Zobrazeni HEXA
EDDI51:	MOV   R6,EDPOS
	CALL  iPRTLhw
	SJMP  EDDI12
EDDI60: CJNE  R7,#6,EDDI70    ; Zobrazi zadane cislo + param R3
	MOV   R5,#0
	MOV   A,R4
	ADD   A,R3
	MOV   R4,A
	MOV   R7,#0C0H
	JMP   EDDI11
EDDI70: CJNE  R7,#7,EDDI80    ; Zobrazi typ strikacky
EDDI_ST:MOV   R6,EDPOS
	MOV   A,R4
	MOV   C,ACC.7
	MOV   A,#2
	JNC   EDDI71
	MOV   A,#5
EDDI71:	CALL  iPRTLc
	MOV   A,#lcdBlnk
	CALL  iPRTLc
	MOV   A,R4
	ANL   A,#0F7H
	MOV   R2,#O_SPRT
	CALL  G_SPR0
	MOV   A,R2
	CALL  iPRTLg
	MOV   A,R3
	CALL  iPRTLg
	JMP   EDDI12
EDDI80: CJNE  R7,#8,EDDI90    ; Zobrazeni HEXA za nenulove podminky
	MOV   A,R3            ; integer cisla [R3]
	MOV   R0,A
	MOV   A,@R0
	INC   R0
	ORL   A,@R0
	JNZ   EDDI51
	MOV   R5,#07FH
	MOV   R7,#0C0H
	JMP   EDDI11
EDDI90:	RET

; Naplni R45 daty na ktera ukazuje [EDNUM]

GET_EDN:MOV   A,EDNUM
	ADD   A,#EDPAR-GET_ED1
	MOVC  A,@A+PC
GET_ED1:MOV   R0,A
	JMP   iLDR45i

; Zapise R45 na adresu [EDNUM]

SET_EDN:MOV   A,EDNUM
	ADD   A,#EDPAR-SET_ED1
	MOVC  A,@A+PC
SET_ED1:MOV   R0,A
	JMP   iSVR45i

;  Nacte do R23 [EDNUM+ACC]

G_EDPAR:ADD   A,EDNUM
G_EDPA: MOV   R2,A
	ADD   A,#EDPAR-G_EDPA1
	MOVC  A,@A+PC
G_EDPA1:XCH   A,R2
	ADD   A,#EDPAR-G_EDPA2+1
	MOVC  A,@A+PC
G_EDPA2:MOV   R3,A
	RET

; Offsety jednotlivych parametru v [EDPAR+EDNUM]
O_EDPTR SET   0              ; Ukazatel na menena data
O_EDMDT SET   1              ; Tabulka prevodu klaves (0 bez editace)
O_EDDF  SET   2              ; Format zobrazeni
O_EDMIN SET   4              ; Minimalni dovolena hodnota pro editaci
O_EDMAX SET   6              ; Maximalni dovolena hodnota pro editaci

EDPAR:  DB    POZ_INC+2
	DB    M_ENERI        ; Inkrement energie od rozdilu polohy
	DB    M_SPDEI        ; Inkrement energie od rozdilu ryclosti
	DB    TIME           ; Korekcni faktor energie
	DB    S_RLEN
	DB    TMP
	DB    M_DPOL         ; Pol derivace
	DB    A_MSK+1        ; Maska vysilanych dat

O_MSERV SET   $-EDPAR        ; Servisni rezim
	DB    O_SE1,0
	%W    (SFT_S1)
	DB    O_SE2,0
	%W    (SFT_S2)
	DB    O_SE3,0
	%W    (SFT_S0)
	DB    O_SE4,0
	%W    (SFT_S4)
	DB    O_SE5,0
	%W    (SFT_S0)
	DB    O_SE6,0
	%W    (SFT_S0)
	DB    O_SE7,0
	%W    (SFT_S7)
	DB    O_SE8,0
	%W    (SFT_S8)
	DB    O_SE9,0
	%W    (SFT_S9)
	DB    0

O_SMOD  SET   $-EDPAR        ; Nastavovani servisniho modu
	DB    MOD_SLV
	DB    0
	DB    6,-M_SERV

O_SE1   SET   $-EDPAR        ; Parametry editace EEPROM
	DB    TMP
	DB    0
	DB    4,0

O_SE2   SET   $-EDPAR        ; Kalibrace kapacit. cidla
	DB    POZ_ACT+1
	DB    0
	DB    5,0

O_SE3   SET   $-EDPAR        ; Vypis stavu baterie
	DB    ACU_BAT
	DB    0
	DB    0C1H,0

O_SE4   SET   $-EDPAR        ; Vypis prevodniku tlaku
	DB    AD_PRE
	DB    0
	DB    8,SLP_PRE

O_SE5   SET   $-EDPAR        ; Vypis polohy z kapacitniho cidla
	DB    CAP_IRC
	DB    0
	DB    0C4H,0

O_SE6   SET   $-EDPAR        ; Vypis podrobne chyby
	DB    LST_ERR
	DB    0
	DB    5,0

O_SE7   SET   $-EDPAR        ; Vypis pozice IRC
	DB    POZ_ACT
	DB    0
	DB    5,0

O_SE8   SET   $-EDPAR        ; Nastaveni typu strikacek
	DB    TMP
	DB    0
	DB    7,0

O_SE9   SET   $-EDPAR        ; Nastaveni konfigurace
	DB    TMP
	DB    0
	DB    5,0

O_EDFL  SET   $-EDPAR        ; Parametry pro nastaveni prutoku
	DB    FLOW
	DB    O_ED_MS
	DB    0C5H,0
	%W    (0)
	%W    (CMAX_FL)

O_DITI  SET   $-EDPAR        ; Parametry pro vypsani casu
	DB    TIME
	DB    0
	DB    1,0

O_DIPR  SET   $-EDPAR        ; Parametry pro vypsani tlaku
	DB    PRE_ACT
	DB    0
	DB    0C0H,0

O_DIVO  SET   $-EDPAR        ; Parametry pro vypsani objemu
	DB    TIME
	DB    0
	DB    3,0

O_EDLTI SET   $-EDPAR        ; Parametry pro nastaveni limitu casu
	DB    MAX_TIM
	DB    O_ED_MT
	DB    1,0
	%W    (0)
	%W    (5999)

O_EDLPR SET   $-EDPAR        ; Parametry pro nastaveni limitu tlaku
	DB    MAX_PRE
	DB    O_ED_MS
	DB    0C0H,0
	%W    (30)
	%W    (CMAX_PL)

O_EDLVO SET   $-EDPAR        ; Parametry pro nastaveni limitu objemu
	DB    MAX_VOL
	DB    O_ED_MS
	DB    0C5H,0
	%W    (0)
	%W    (9999)

O_DIFL  SET   $-EDPAR        ; Parametry pro vypis prutoku
	DB    FLOW
	DB    O_ED_MS
	DB    0C5H,0

O_DILTI SET   $-EDPAR        ; Parametry pro vypis limitu casu
	DB    MAX_TIM
	DB    0
	DB    1,0

O_DILPR SET   $-EDPAR        ; Parametry pro vypis limitu tlaku
	DB    MAX_PRE
	DB    0
	DB    0C0H,0

O_DILVO SET   $-EDPAR        ; Parametry pro vypis limitu objemu
	DB    MAX_VOL
	DB    0
	DB    0C5H,0

O_EDNIL SET   $-EDPAR        ; Nulova editace
	DB    0
	DB    0
	DB    0,0

O_ED_MS SET   $-EDPAR        ; Standartni modifikace
	%W    (1)
	%W    (10)
	%W    (100)
	%W    (1000)
	%W    (-1)
	%W    (-10)
	%W    (-100)
	%W    (-1000)

O_ED_MT SET   $-EDPAR        ; Modifikace casu
	%W    (1)
	%W    (10)
	%W    (60)
	%W    (600)
	%W    (-1)
	%W    (-10)
	%W    (-60)
	%W    (-600)

SFT_D2: DB    K_TIME          ; Necerpa a je pravy display
	%W    (M_STIME)
	%W    (SV_MOD)

	DB    K_PRES
	%W    (M_SPRES)
	%W    (SV_MOD)

	DB    K_VOL
	%W    (M_SVOL)
	%W    (SV_MOD)

	DB    K_LTIME
	DB    M_LTIME,SM_TIME
	%W    (SET_LIM)

	DB    K_LPRES
	DB    M_LPRES,SM_PRES
	%W    (SET_LIM)

	DB    K_LVOL
	DB    M_LVOL,SM_VOL
	%W    (SET_LIM)

	DB    K_NATIM
	DB    M_STIME,SM_ATIM
	%W    (CLR_LIM)

	DB    K_NAVOL
	DB    M_SVOL,SM_AVOL
	%W    (CLR_LIM)

	DB    K_NTIME
	DB    M_LTIME,SM_TIME
	%W    (CLR_LIM)

	DB    K_NPRES
	DB    M_LPRES,SM_PRES
	%W    (CLR_LIM)

	DB    K_NVOL
	DB    M_LVOL,SM_VOL
	%W    (CLR_LIM)

	DB    K_RU0
	DB    0,1
	%W    (ED_LMOD)

	DB    K_RU1
	DB    1,1
	%W    (ED_LMOD)

	DB    K_RU2
	DB    2,1
	%W    (ED_LMOD)

	DB    K_RU3
	DB    3,1
	%W    (ED_LMOD)

	DB    K_RD0
	DB    4,1
	%W    (ED_LMOD)

	DB    K_RD1
	DB    5,1
	%W    (ED_LMOD)

	DB    K_RD2
	DB    6,1
	%W    (ED_LMOD)

	DB    K_RD3
	DB    7,1
	%W    (ED_LMOD)

SFT_D3: DB    K_START         ; Necerpa a je jen jeden display
	%W    (0)
	%W    (FSTART)

	DB    K_ALARM
	%W    (20H)
	%W    (CLR_ERR)

	DB    K_STOP
	%W    (0)
	%W    (M_STOP)

	DB    VIR_KEY1+K_LU0
	%W    (1)
	%W    (USR_SCH)

	DB    VIR_KEY1+K_LD0
	%W    (-1)
	%W    (USR_SCH)

	DB    K_LU0
	DB    0,0
	%W    (ED_LMOD)

	DB    K_LU1
	DB    1,0
	%W    (ED_LMOD)

	DB    K_LU2
	DB    2,0
	%W    (ED_LMOD)

	DB    K_LU3
	DB    3,0
	%W    (ED_LMOD)

	DB    K_LD0
	DB    4,0
	%W    (ED_LMOD)

	DB    K_LD1
	DB    5,0
	%W    (ED_LMOD)

	DB    K_LD2
	DB    6,0
	%W    (ED_LMOD)

	DB    K_LD3
	DB    7,0
	%W    (ED_LMOD)

	DB    K_SPEED
	DB    0,0
	%W    (IHSPEED)

	DB    VIR_KEY1+K_START
	DB    0,0
	%W    (ISERVCI)

	DB    VIR_KEY1+K_STOP
	DB    0,0
	%W    (ICOMRD)

	DB    0

SFT_D4: DB    K_TIME          ; Cerpa a je pravy display
	%W    (M_RTIME)
	%W    (SV_MOD)

	DB    K_PRES
	%W    (M_RPRES)
	%W    (SV_MOD)

	DB    K_VOL
	%W    (M_RVOL)
	%W    (SV_MOD)

	DB    K_LTIME
	%W    (M_RLTIM)
	%W    (SV_MOD)

	DB    K_LPRES
	%W    (M_RLPRE)
	%W    (SV_MOD)

	DB    K_LVOL
	%W    (M_RLVOL)
	%W    (SV_MOD)

SFT_D5:	DB    K_STOP          ; Cerpa a neni pravy display
	%W    (0)
	%W    (FSTOP)

	DB    0

SFT_S9: DB    K_V_INI
	%W    (0)
	%W    (LS_F9I)

	DB    K_STOP
	DB    0,0
	%W    (LS_F9S)

	DB    K_LU0
	%W    (1H)
	%W    (EDADD)

	DB    K_LU1
	%W    (10H)
	%W    (EDADD)

	DB    K_LD0
	%W    (-1H)
	%W    (EDADD)

	DB    K_LD1
	%W    (-10H)
	%W    (EDADD)

	DB    -1
	%W    (SFT_S0)

SFT_S8: DB    K_V_INI
	%W    (0)
	%W    (LS_F8I)

	DB    K_LU0
	%W    (1)
	%W    (LS_F8B)

	DB    K_LD0
	%W    (-1)
	%W    (LS_F8B)

	DB    K_LU3
	DB    080H,0
	%W    (LS_F8A)

	DB    K_LD3
	DB    080H,0
	%W    (LS_F8A)

	DB    K_STOP
	DB    0,0
	%W    (LS_F8S)

	DB    -1
	%W    (SFT_S0)

SFT_S7: DB    K_START
	%W    (1)
	%W    (LS_FS70)

	DB    -1
	%W    (SFT_S0)

SFT_S4: DB    K_START         ; Kalibrace tenzometru
	%W    (0)
	%W    (LS_FS48)

	DB    K_LPOS
	%W    (0)
	%W    (LS_FS40)

	DB    K_RPOS
	%W    (0)
	%W    (LS_FS45)

	DB    K_SPEED
	%W    (0)
	%W    (LS_FS42)

	DB    -1
	%W    (SFT_S0)

SFT_S2: DB    K_V_INI         ; Rezim kalibrace irc/cap
	%W    (0)
	%W    (LS_FN20)

	DB    K_V_RUN
	%W    (0)
	%W    (LS_FN22)

	DB    K_START
	%W    (0)
	%W    (LS_FN21)

	DB    K_STOP
	%W    (0)
	%W    (LS_FS21)

	DB    -1
	%W    (SFT_S0)

SFT_S1: DB    K_V_INI         ; Rezim editace EEPROM
	%W    (0)
	%W    (LS_FN12)

	DB    K_LU0
	%W    (100H)
	%W    (EDADD)

	DB    K_LU1
	%W    (1000H)
	%W    (EDADD)

	DB    K_LU2
	%W    (1H)
	%W    (LS_FN1)

	DB    K_LU3
	%W    (10H)
	%W    (LS_FN1)

	DB    K_LD0
	%W    (-100H)
	%W    (EDADD)

	DB    K_LD1
	%W    (-1000H)
	%W    (EDADD)

	DB    K_LD2
	%W    (-1H)
	%W    (LS_FN1)

	DB    K_LD3
	%W    (-10H)
	%W    (LS_FN1)

	DB    K_STOP
	DB    TMP,1
	%W    (LS_FN11)

SFT_S0: DB    VIR_KEY1+K_LU0
	DB    1,M_SERV+8
	%W    (ED_BADD)

	DB    VIR_KEY1+K_LD0
	DB    -1,M_SERV
	%W    (ED_BADD)

	DB    VIR_KEY1+K_START
	DB    0,0
	%W    (QSERV)

	DB    K_STOP
	%W    (0)
	%W    (M_STOP)

	DB    0

LED_CLR:CLR   A
	MOV   DPTR,#LED0 AND LED1 AND LED2
	MOVX  @DPTR,A
	MOV   A,#B_OK
	RET

CLR_ERR:CLR   FL_NEND
	MOV   A,MOD_SLV
	ANL   A,#00FH
	ORL   A,#0C0H
	MOV   MOD_SLV,A
CLR_ER0:CLR   FL_MOT
CLR_ER1:SETB  FL_EDCH
	MOV   PWM0,#0
	MOV   A,ERRNUM
	JZ    CLR_ER3
	MOV   ERRNUM,#0
	SETB  ELED
	CLR   MBRK
	CLR   ELED
	MOV   A,#B_OK
CLR_ER2:MOV   R0,#OUT_MSG
	MOV   @R0,#OMS_CER
	INC   R0
	MOV   @R0,#055H
	INC   R0
	MOV   @R0,#000H
CLR_ER3:CLR   FL_ERRW
	RET

CLR_LIM:MOV   A,R3
	CALL  CL_SPC
	MOV   A,R3
	ANL   A,#SM_LIMS
	CPL   A
	ANL   SW_FLG,A
	JMP   SV_MOD

; Prednastavuje udaje podle masky v A

CL_SPC: MOV   R7,#CL_SPCt-CL_SPCk
CL_SPC1:CLR   C
	RRC   A
	MOV   R1,A
	JNC   CL_SPC4
	MOV   A,R7
	MOVC  A,@A+PC
CL_SPCk:JZ    CL_SPC4
	MOV   R0,A
	CJNE  A,#MAX_PRE,CL_SPC2
	MOV   @R0,#LOW  CDEF_PL
	INC   R0
	MOV   @R0,#HIGH CDEF_PL
	JNB   FL_P100,CL_SPC4
	DEC   R0
	MOV   @R0,#LOW  CDAL_PL
	INC   R0
	MOV   @R0,#HIGH CDAL_PL
	SJMP  CL_SPC4
CL_SPC2:CJNE  A,#TIME,CL_SPC3
	MOV   TIMRJ,#0
CL_SPC3:CLR   A
	MOV   @R0,A
	INC   R0
	MOV   @R0,A
CL_SPC4:INC   R7
	MOV   A,R1
	JNZ   CL_SPC1
	RET

; Hodnoty prednastaveni

CL_SPCt:DB    LST_VOL         ; Objem
	DB    0               ; Tlak
	DB    TIME            ; Cas
	DB    0
	DB    MAX_VOL         ; Limit objemu
	DB    MAX_PRE         ; Limit tlaku
	DB    MAX_TIM         ; Limit casu
	DB    0

SET_LIM:MOV   A,R2
	XRL   A,MOD_SLV
	JNZ   SV_MOD
	MOV   A,R3
	XRL   SW_FLG,A

SV_MOD: MOV   MOD_SLV,R2
	MOV   EDNUM,#0
	SETB  FL_EDCH
	MOV   A,#B_OK
	MOV   KBDFL,#0FFH
	RET

SET_ERR:MOV   R0,#LST_ERR
	MOV   @R0,A
	XCH   A,ERRNUM
	JZ    SET_ER1
	XCH   A,ERRNUM
SET_ER1:SETB  ELED
	CLR   FL_MOT
	MOV   PWM0,#0
	CLR   MBRK
	RET

FSTOP:  SETB  FL_BEOK
	CALL  M_STOP
	POP   ACC
	POP   ACC
	JMP   ISTOP

FSTART:	POP   ACC
	POP   ACC
	JMP   ISTART

ED_BADD:CALL  GET_EDN
	MOV   A,R4
	XRL   A,R3
	JZ    ED_BADR
	XRL   A,R3
	ADD   A,R2
	MOV   @R0,A
	MOV   A,#B_OK
	RET
ED_BADR:MOV   A,#B_ERR
	RET

;=================================================================
; Rizeni motoru

RSEG    IC____C

; Prevod FLOW na pocet irc pulsu za 1 interrupt
;   Prutok             F  [0.1 ml/h] vstup z FLOW
;   Zdvih strikacky    L  [irc]
;   Zdvih strikacky    L1 [256*irc] podle typu strikacky SPRTYP
;   Objem strikacky    V  [0.1 ml]             podle SPRTYP
;   Frekvence intr.    N  [intr/h] = 60*60*450 = 50625*2^5
;   Rychlost           S  [irc/int]
;   Zpresnena rychlost S1 [1/2^24 irc/intr]
;
;                          24  8
;       F*L          F*L1*2  *2
;   S = ---       S1=----------
;       V*N                   5
;                    V*50625*2
;
;  vystup : R4567 pocet pulsu za interrupt
;           R23 totez s exponentem v R1

FL_CONV:MOV   R4,FLOW         ; Nacteni F
	MOV   R5,FLOW+1
FL_CON0:MOV   A,#O_SPRL
	CALL  G_SPR           ; Nacteni L
	CALL  MULi            ; R4567 = L*F
	MOV   R1,#24+8-5      ; Zpusobi *2^24*2^8/2^5
	CALL  NORMMI1         ; Znormuje R4567 a exp R1 na R45 a R1
	MOV   A,#O_SPRV
	CALL  G_SPR           ; Nacteni V
	CALL  DIVihf          ; R45 exp R1 = L*F*2^27/V
	JB    F0,FL_CONE
	MOV   R2,#LOW  50625
	MOV   R3,#HIGH 50625
	CALL  DIVihf          ; R45 exp R1 = L*F*2^27/(V*50625)
	MOV   A,R4
	MOV   R2,A
	MOV   A,R5
	MOV   R3,A
	CLR   A
	MOV   R6,A
	MOV   R7,A
	MOV   A,R1
	JB    ACC.7,FL_CON5   ; Exponent je zaporny
	ANL   A,#7            ; Posun R45 vlevo do R4567
	JZ    FL_CON2
	MOV   R0,A            ; Posun o R1 mod 8
FL_CON1:CLR   C
	MOV   A,R4
	RLC   A
	MOV   R4,A
	MOV   A,R5
	RLC   A
	MOV   R5,A
	MOV   A,R6
	RLC   A
	MOV   R6,A
	DJNZ  R0,FL_CON1
FL_CON2:MOV   A,R1
	ANL   A,#NOT 7
	RR    A
	RR    A
	RR    A
	MOV   R0,A            ; Posun o (R1 div 8)*8
	JZ    FL_CONR
FL_CON3:CLR   A
	XCH   A,R4
	XCH   A,R5
	XCH   A,R6
	XCH   A,R7
	DJNZ  R0,FL_CON3
	SJMP  FL_CONR
FL_CON5:CPL   A               ; Posum R45 vpravo a R67=0
	INC   A
	CALL  SHRi
FL_CONR:CLR   F0
FL_CONE:RET


; Rotuje R4567 tak ze se cislo zkrati na R45 pocet posunu ulozi do R1

NORMMI: MOV   R1,#0
NORMMI1:MOV   A,R7
	JZ    NORMMI2
	CLR   A
	XCH   A,R7
	XCH   A,R6
	XCH   A,R5
	XCH   A,R4
	MOV   A,R1
	ADD   A,#8
	MOV   R1,A
NORMMI2:MOV   A,R6
	JZ    NORMMIR
	CLR   C
	MOV   A,R6
	RRC   A
	MOV   R6,A
	MOV   A,R5
	RRC   A
	MOV   R5,A
	MOV   A,R4
	RRC   A
	MOV   R4,A
	INC   R1
	SJMP  NORMMI2
NORMMIR:RET

; Naplni R23 konstantou z SPR_PAR podle SPRTYP a offsetu v ACC

G_SPR:  MOV   R2,A
	MOV   A,SPRTYP
	JB    ACC.3,G_SPRE
G_SPR0:	JNB   ACC.7,G_SPR1
	SWAP  A
G_SPR1: ANL   A,#0FH
	RL    A
	RL    A
	RL    A
	ADD   A,R2
	MOV   R2,A
	ADD   A,#SPR_PAR-G_SPR2
	MOVC  A,@A+PC
G_SPR2: XCH   A,R2
	ADD   A,#SPR_PAR-G_SPR3+1
	MOVC  A,@A+PC
G_SPR3: MOV   R3,A
	MOV   A,#1
	RET

G_SPRE: CLR   A
	MOV   R2,A
	MOV   R3,A
	RET

; Tabulka parametru strikacek

O_SPRV  SET   0   ; Objem strikacky v 0.1 ml
O_SPRL  SET   2   ; Delka strikacky v 256 irc
O_SPRE  SET   4   ; Konec davkovani v 256 irc
O_SPRT  SET   6   ; 2 znaky nazvu stikacky

SPR_PAR:%W    (200)           ; 0 - CHIRANA 20
	%W    (2273)
	%W    (140)
	DB    lcda+lcdd+lcde+lcdf
	DB    lcdc+lcde+lcdf+lcdg

	%W    (200)           ; 1 - TERUMO 20
	%W    (2252)
	%W    (204)
	DB    lcdd+lcde+lcdf+lcdg
	DB    lcda+lcdd+lcde+lcdf+lcdg

	%W    (200)           ; 2 - BRAUN 20
	%W    (2273) ; 2248
	%W    (15)
	DB    lcdc+lcdd+lcde+lcdf+lcdg
	DB    lcde+lcdg

	%W    (200)           ; 3
	%W    (3403)
	%W    (50)
	DB    lcda+lcdb+lcdg+lcde+lcdd
	DB    lcda+lcdb+lcdg+lcdc+lcdd

	%W    (200)           ; 4
	%W    (3404)
	%W    (50)
	DB    lcda+lcdb+lcdg+lcde+lcdd
	DB    lcdf+lcdg+lcdb+lcdc

	%W    (200)           ; 5
	%W    (3405)
	%W    (50)
	DB    lcda+lcdb+lcdg+lcde+lcdd
	DB    lcda+lcdf+lcdg+lcdc+lcdd

	%W    (200)           ; 6
	%W    (3406)
	%W    (50)
	DB    lcda+lcdb+lcdg+lcde+lcdd
	DB    lcda+lcdf+lcdg+lcde+lcdd+lcdc

	%W    (200)           ; 7
	%W    (3407)
	%W    (50)
	DB    lcda+lcdb+lcdg+lcde+lcdd
	DB    lcda+lcdb+lcdc

	%W    (500)           ; 8 - CHIRANA 50
	%W    (2894)
	%W    (247)
	DB    lcda+lcdd+lcde+lcdf
	DB    lcdc+lcde+lcdf+lcdg

	%W    (500)           ; 9 - TERUMO 50
	%W    (2669)
	%W    (281)
	DB    lcdd+lcde+lcdf+lcdg
	DB    lcda+lcdd+lcde+lcdf+lcdg

	%W    (500)           ; A - BRAUN 50
	%W    (2894) ; 2966
	%W    (220)
	DB    lcdc+lcdd+lcde+lcdf+lcdg
	DB    lcde+lcdg

	%W    (500)           ; B - BRAUN 50 stary
	%W    (2894)
	%W    (237)
	DB    lcda+lcdf+lcdg+lcdc+lcdd
	DB    lcda+lcdb+lcdg+lcdc+lcdd

	%W    (500)           ; C
	%W    (2894)
	%W    (237)
	DB    lcda+lcdf+lcdg+lcdc+lcdd
	DB    lcdf+lcdg+lcdb+lcdc

	%W    (500)           ; D
	%W    (2894)
	%W    (237)
	DB    lcda+lcdf+lcdg+lcdc+lcdd
	DB    lcda+lcdf+lcdg+lcdc+lcdd

	%W    (500)           ; E
	%W    (2894)
	%W    (237)
	DB    lcda+lcdf+lcdg+lcdc+lcdd
	DB    lcda+lcdf+lcdg+lcde+lcdd+lcdc

	%W    (500)           ; F
	%W    (2894)
	%W    (237)
	DB    lcda+lcdf+lcdg+lcdc+lcdd
	DB    lcda+lcdb+lcdc

MFST_ES:MOV   A,#ERSPRBT
	SJMP  MFST_E8
MFST_ER:MOV   A,#ERNULF
MFST_E8:CALL  SET_ERR
MFST_E9:SETB  F0
	RET

MFSTART:MOV   A,SPRTYP
	JB    ACC.3,MFST_ES
	MOV   A,#K_START
	CALL  TESTKEY
	JNZ   MFST_E9
	MOV   A,ERRNUM
	JNZ   MFST_E9
	MOV   R4,FLOW
	MOV   R5,FLOW+1
	MOV   A,R4
	ORL   A,R5
	JZ    MFST_ER
	MOV   R2,#LOW  (CMAX_FL+1)
	MOV   R3,#HIGH (CMAX_FL+1)
	CALL  CMPi
	JNC   MFST_ER
	CALL  FL_CONV
	JB    F0,MFST_ER
	MOV   POZ_INC,R4
	MOV   POZ_INC+1,R5
	MOV   POZ_INC+2,R6
	MOV   POZ_INC+3,R7
	MOV   R4,#LOW  54000  ; Pocet int za 1 min
	MOV   R5,#HIGH 54000
	MOV   A,#-4*8         ; Posun z POZ_INC na IRC*256
	ADD   A,R1
	MOV   R1,A
	CALL  MULi
	CALL  NORMMi1
	MOV   A,R1
	JB    ACC.7,MFSTAR1
	MOV   R4,#0FFH
	MOV   R5,#0FFH
	SJMP  MFSTAR2
MFSTAR1:CPL   A
	INC   A
	CALL  SHRi
MFSTAR2:MOV   R0,#NEN_IRC
	MOV   A,R4
	CPL   A
	MOV   R4,A
	MOV   A,R5
	CPL   A
	MOV   R5,A
	CALL  iSVR45i
	CLR   F0
	RET

; Spusteni cerpani

M_START:MOV   R4,STR_POZ      ; Objem davkovani v 256*IRC
	MOV   R5,STR_POZ+1
M_STAR0:CLR   FL_MOT
	MOV   PWM0,#0
	CLR   A
	MOV   R0,#CAP_ERC
	MOV   @R0,A
	MOV   POZ_RQ,A
	CALL  NEGi
	MOV   POZ_RQ+1,R4
	MOV   POZ_RQ+2,R5
	MOV   R0,#CAP_IRC
	CALL  iLDR23i
	CALL  ADDi
	MOV   R0,#CAP_OFS
	CALL  iSVR45i
	CALL  M_RESP
	CLR   M_D_256
	MOV   A,ERRNUM        ; Test chyby
	JNZ   M_STAR2
	MOV   IRC_ERR,#0
	SETB  FL_MOT
M_STAR2:MOV   A,#B_OK
	RET

M_STOP: CLR   FL_MOT
	MOV   PWM0,#0
	MOV   A,#B_OK
	RET

M_RESP: PUSH  IE
	CLR   EA
	MOV   PWM0,#0
	MOV   R4,POZ_RQ
	MOV   R5,POZ_RQ+1
	MOV   R6,POZ_RQ+2
	MOV   POZ_ACT,R4
	MOV   POZ_ACT+1,R5
	MOV   POZ_ACT+2,R6
	CLR   TR0
	MOV   R3,TMH2
	MOV   R2,TML2
	MOV   A,TMH2
	XRL   A,R3
	JZ    M_RESP1
	MOV   A,R2
	JB    ACC.7,M_RESP1
	INC   R3
M_RESP1:CALL  NEGi
	CALL  ADDi
	MOV   TL0,R4
	MOV   TH0,R5
	SETB  TR0
	CLR   FL_MOT
	MOV   M_DFILT,#0
	MOV   M_DFILT+1,#0
	POP   IE
	RET

S_ENERG:JNB   FL_MOT,S_ENER2
	MOV   A,M_ENERG+1
	JB    ACC.7,S_ENER2
S_ENER1:MOV   A,M_ENERG
	MOV   C,ACC.7
	MOV   A,M_ENERG+1
	RLC   A
	SETB  MBRK
	MOV   PWM0,A
	RET
S_ENER2:MOV   PWM0,#0
	CLR   MBRK
	RET

; Prevadi R45 [objem 0.1*ml] na pocet irc*256 v R45 pri preteceni F0

VOL2IRC:MOV   A,#O_SPRL       ; Delka strikacky v 256*irc
	CALL  G_SPR
	CALL  MULi            ; R4567 = vstupR45*L
	CALL  NORMMI          ; Znormuje R4567 a exp R1 na R45 a R1
	MOV   A,#O_SPRV
	CALL  G_SPR           ; Objem strikacky v 0.1*ml
	CALL  DIVihf          ; R45*2^R1=vstupR45*L/V
	MOV   A,R1
	CPL   A
	INC   A               ; Vysledek nejde prevest na integer
	JB    ACC.7,VOL2IR1   ; v R45
	JMP   SHRi
VOL2IR1:SETB  F0
	RET

; Prevadi POZ_ACT na vydavkovany objem pop poslednim stisku start

IRC2VOL:MOV   C,EA
	CLR   EA
	MOV   R4,POZ_ACT+1
	MOV   R5,POZ_ACT+2
	MOV   R2,STR_POZ
	MOV   R3,STR_POZ+1
	MOV   EA,C
	CALL  ADDi
IRC2VO0:MOV   A,#O_SPRV
	CALL  G_SPR
	CALL  MULi            ; R4567 = V*POZ_ACT/256
	CALL  NORMMI          ; Znormuje R4567 a exp R1 na R45 a R1
	CLR   F0
	MOV   R0,#0
	MOV   A,#O_SPRL
	CALL  G_SPR
	CALL  DIVi0
	JB    F0,IRC2VOE
	RET

IRC2VOE:MOV   R4,#0FFH
	MOV   R5,#07FH
	RET

; Prevod udaje z tenzometru v AD_PRE na skutecny tlak v PRE_ACT
;
;            AD_PRE - INT_PRE     O_SPRL
;  PRE_ACT = ----------------- * ---------
;               SLP_PRE           O_SPRV

AD2PRE: CALL  AD2SUB1
	MOV   R0,#SLP_PRE
	CALL  iLDR23i
	ORL   A,R2
	JZ    AD2PREN
	JB    F0,AD2PREE      ; Chyba v tenzometru
	JC    AD2PREZ         ; Tlak mensi nez nula
	CALL  DIVihf
	MOV   A,R1
	CPL   A
	INC   A
	CALL  SHRi
	MOV   R0,#PRE_ACT
	CALL  iSVR45i
	MOV   C,FL_STRT
	ORL   C,FL_HSPD
	JNC   AD2PRE9
	MOV   R2,#LOW  CDEF_PL
	MOV   R3,#HIGH CDEF_PL
	JNB   FL_P100,AD2PRE7
	MOV   R2,#LOW  CDAL_PL
	MOV   R3,#HIGH CDAL_PL
AD2PRE7:JNB   FL_PRES,AD2PRE8
	MOV   R0,#MAX_PRE
	CALL  iLDR23i
AD2PRE8:CALL  CMPi
	JC    AD2PRE9
	MOV   A,#ERPRESS
	CALL  SET_ERR
AD2PRE9:RET

AD2PREZ:MOV   R0,#PRE_ACT
	CLR   A
	MOV   @R0,A
	INC   R0
	MOV   @R0,A
	RET

AD2PREE:MOV   A,#ERTENZ
	JMP   SET_ERR

AD2PREN:MOV   R0,#PRE_ACT+1
	MOV   @R0,#07FH
	RET

AD2SUB1:MOV   R0,#AD_PRE
	CLR   EA
	CALL  iLDR45i
	SETB  EA
	ORL   A,R4
	SETB  F0
	JZ    AD2SUBR
	CLR   F0
AD2SUB2:MOV   R0,#INT_PRE     ; Odecteni posunu
	CALL  iLDR23i
	CALL  SUBi
	JC    AD2SUBR         ; Mensi nez nula
	MOV   R1,#3H
	MOV   A,#O_SPRL
	CALL  G_SPR
	CALL  MULi
	CALL  NORMMI1         ; Znormuje R4567 a exp R1 na R45 a R1
	MOV   A,#O_SPRV
	CALL  G_SPR
	CALL  DIVihf
	CLR   C
AD2SUBR:RET

; Pri limituobjemu a casu vypocita prutok
;
;  FLOW = MAX_VOL / MAX_TIM * 60

LIM2FL: MOV   R4,MAX_VOL
	MOV   R5,MAX_VOL+1
	MOV   R2,#60
	MOV   R3,#0
	CALL  MULi
	CALL  NORMMI
	MOV   R0,#MAX_TIM
	CALL  iLDR23i
	CALL  DIVihf
	MOV   A,R1
	JNB   ACC.7,LIM2FLE
	CPL   A
	INC   A
	CALL  SHRi
	MOV   R2,#LOW  (CMAX_FL+1)
	MOV   R3,#HIGH (CMAX_FL+1)
	CALL  CMPi
	JNC   LIM2FLE
	SJMP  LIM2FL9
LIM2FLE:MOV   R4,#LOW  7FFFH
	MOV   R5,#HIGH 7FFFH
LIM2FL9:MOV   FLOW,R4
	MOV   FLOW+1,R5
	SETB  FL_EDCH
	RET

;=================================================================
; Porovnani CAP cidla s IRC

CHK_IRC:JNB   FL_MOT,CHK_IRR
	MOV   A,MOD_SLV
	XRL   A,#M_SERV
	ANL   A,#0F0H
	JZ    CHK_IRR
	MOV   R0,#CAP_IRC
	CALL  iLDR45i
	MOV   A,#O_SPRE
	CALL  G_SPR
	CALL  CMPi
	MOV   A,#EREND
	JC    CHK_IRE
	CLR   EA
	MOV   R2,POZ_ACT+1
	MOV   R3,POZ_ACT+2
	SETB  EA
	CALL  ADDi
	MOV   R0,#CAP_OFS
	CALL  iLDR23i
	CALL  SUBi
	MOV   C,ACC.7
	MOV   F0,C
	JC    CHK_IR0
	MOV   A,#1	      ; Dovolit 2x vetsi zpozdeni nez 
	CALL  SHRi	      ; predbehnuti potenciomentru
	SJMP  CHK_IR1	      ; proti motorku
CHK_IR0:CALL  NEGi
CHK_IR1:MOV   R2,#LOW  28H    ; Maximalni rozdil mezi cidly
	MOV   R3,#HIGH 28H    ; bylo 20; bylo 28H ; bylo 2DH
	CALL  CMPi
	JC    CHK_IR2
	MOV   R0,#CAP_ERC
	MOV   A,@R0
	ADD   A,#40H	      ; 100H/<pocet chyb> bylo 60H
	MOV   @R0,A
	JNC   CHK_IRR
	MOV   A,#ERIRCAP
CHK_IRE:JMP   SET_ERR
CHK_IR2:MOV   R0,#CAP_TMP
	MOV   A,POZ_ACT+1
	XCH   A,@R0
	XRL   A,@R0           ; !!!!!!!!!!!!!!!
	ANL   A,#0E0H         ; Maska zmeny POZ_ACT+1
	JZ    CHK_IR4         ; pri ktere se koriguje CAP_OFS
	MOV   R0,#CAP_OFS
	CALL  iLDR45i
	CLR   A
	JNB   F0,CHK_IR3
	CPL   A
CHK_IR3:MOV   R3,A
	XRL   A,#1
	MOV   R2,A
	CALL  ADDi
	CALL  iSVR45i
CHK_IR4:MOV   R0,#CAP_ERC
    %IF(0)THEN(
	MOV   A,@R0
	JZ    CHK_IR5
	DEC   @R0
CHK_IR5:
    )ELSE(
	MOV   A,@R0	      ; Nizsi pozadovany pomer
	ADD   A,#-4           ; spravnych proti chybnym mereni
	JC    CHK_IR5
	CLR   A
CHK_IR5:MOV   @R0,A
    )FI
;	CALL  PRTR45          ; !!!!!!!!!!!!!!!
CHK_IRR:RET

;=================================================================
; Zborceni programu

ERR_HLT:MOV   B,#3
ERR_HL1:CLR   EA
	SETB  ELED
	SETB  TOFF
	MOV   PWM0,#0
	MOV   DPTR,#LED2
	MOV   A,B
	MOVX  @DPTR,A
	CLR   EA
	%WATCHDOG
	JMP   ERR_HL1

;=================================================================
; Casove preruseni

USING   2

I_TIME: PUSH  ACC
	PUSH  PSW
	PUSH  B

	MOV   PSW,#AR0

	MOV   A,SP            ; Test stavu zasobniku
	CLR   C
	SUBB  A,#STACK
	JC    ERR_HLT
	SUBB  A,#STACK_S-4
	JNC   ERR_HLT

	CLR   TF1
	CLR   C
	CLR   TR1
S_T1_D  SET   7               ; zpozdeni rutiny
	MOV   A,TL1           ; 7 Cyklu
	SUBB  A,#LOW (M_TIMED-S_T1_D)
	MOV   TL1,A
	MOV   A,TH1
	SUBB  A,#HIGH (M_TIMED-S_T1_D)
	MOV   TH1,A
	SETB  TR1
	CALL  AD_PREP         ; Odstartovani prevodu ADC pro vnitrni ucely

A_SYSP:	MOV   A,A_CPOS        ; Priprava prevodu ADC pro A_SYS
	JNB   ACC.3,A_SYSP1   ; Neni potreba pripravit data v prevodniku
	MOV   B,ADCON
	JB    B.3,A_SYSP1     ; ADCS=1 .. prevodnik zamestnan
	ANL   A,#07H
	MOV   ADCON,A
	SETB  ACC.3           ; Odstartovani ADC
	MOV   ADCON,A
A_SYSP1:
	MOV   R5,TMH2         ; Vypocet aktualni pozice motoru
	MOV   R4,TML2
	MOV   A,TMH2
	XRL   A,R5
	JZ    M_TIME1
	MOV   A,R4
	JB    ACC.7,M_TIME1
	INC   R5
M_TIME1:MOV   R3,TH0
	MOV   R2,TL0
	MOV   A,TH0
	XRL   A,R3
	JZ    M_TIME2
	MOV   A,R2
	JB    ACC.7,M_TIME2
	INC   R3
M_TIME2:CLR   C
	MOV   A,R4
	SUBB  A,R2
	MOV   R4,A
	MOV   A,R5
	SUBB  A,R3
	MOV   R5,A

	CLR   C               ; Generovani SPD_ACT = COUNTER-POZ_ACT
	MOV   A,R4
	SUBB  A,POZ_ACT
	MOV   SPD_ACT,A
	MOV   POZ_ACT,R4
	MOV   A,R5
	SUBB  A,POZ_ACT+1
	MOV   SPD_ACT+1,A
	MOV   POZ_ACT+1,R5
	JNC   M_TIM20
	CPL   A
M_TIM20:JNB   ACC.7,M_TIME3   ; Prodlouzeni POZ_ACT
	INC   POZ_ACT+2
	JC    M_TIME3
	DEC   POZ_ACT+2
	DEC   POZ_ACT+2
M_TIME3:JB    FL_MOT,M_TIME4  ; Cerpa se ?
	JMP   M_TIMOF

M_TIME4:JNB   CTI3,M_TIM40
	CLR   CTI3            ; Chyba z inkrementalniho cidla
	MOV   A,#CIRE_DF
	ADD   A,IRC_ERR
	MOV   IRC_ERR,A
	JNC   M_TIM40
	MOV   A,#ERIRCFD
	JMP   M_ERR1

M_TIM40:MOV   A,SPD_ACT
	ORL   A,SPD_ACT+1
	JNZ   M_TIM42
	MOV   R0,#-40H
	JNB   FL_BAT,M_TIM41
	MOV   R0,#-60H
M_TIM41:MOV   A,M_ENERG+1
	ADD   A,R0
	MOV   C,OV
	XRL   A,PSW
	JB    ACC.7,M_TIM42
	MOV   A,#CIRE_ME
	ADD   A,IRC_ERR
	MOV   IRC_ERR,A
	JNC   M_TIM42
	MOV   A,#ERIRCME
	JMP   M_ERR1

M_TIM42:MOV   R0,#POZ_RQS     ; Generovani POZ_RQS += POZ_INC
	MOV   R1,#POZ_INC
	MOV   R2,#04H
	CALL  ADDsi
	JNC   M_TIME5
	INC   POZ_RQS+4
	MOV   A,POZ_RQS+4
	CJNE  A,#0,M_TIME5
	INC   POZ_RQS+5
M_TIME5:MOV   A,POZ_INC+3
	JNB   ACC.7,M_TIME6
	DEC   POZ_RQS+4
	MOV   A,POZ_RQS+4
	CJNE  A,#-1,M_TIME6
	DEC   POZ_RQS+5
M_TIME6:CLR   C               ; Generovani POZ_DEL = POZ_RQ - POZ_ACT
	MOV   A,POZ_RQ
	SUBB  A,POZ_ACT
	MOV   POZ_DEL,A
	MOV   R2,A
	MOV   A,POZ_RQ+1
	SUBB  A,POZ_ACT+1
	MOV   POZ_DEL+1,A
	MOV   R3,A
	ANL   A,#80H
	MOV   R1,A
	MOV   A,POZ_RQ+2
	SUBB  A,POZ_ACT+2
	CJNE  R1,#80H,M_TIME7
	CPL   A
	XCH   A,R2
	CPL   A
	ADD   A,#1
	XCH   A,R2
	XCH   A,R3
	CPL   A
	ADDC  A,#0	      ; !!!!!!!!!!!!!
	XCH   A,R3
M_TIME7:JNZ   M_ERR
	MOV   R4,#LOW  MX_PDEL+1
	MOV   R5,#HIGH MX_PDEL+1
	CALL  CMPi
	JNC   M_TIM70

M_ERR:	MOV   A,#ERINTMS
M_ERR1:	SETB  ELED
	CALL  SET_ERR
M_TIMOF:CLR   FL_MOT
	MOV   PWM0,#0
	MOV   C,FL_TRTS       ; Pokud neni test tak zapne brzdu
	MOV   MBRK,C
	JMP   A_SYS

M_TIM70:MOV   A,POZ_ACT+2     ; Test znamenka polohy
	JB    ACC.7,M_TIM75   ; Strikacka nedojela na konec
	MOV   A,#EREND        ; Konec davky - strikacky
	JNB   FL_VOLA,M_ERR1
	MOV   A,#ERENDVO      ; Konec davky - objemovy limit
	SJMP  M_ERR1

M_TIM75:MOV   R4,M_ENERI      ; Generovani M_ENERG = POZ_DEL*M_ENERI
	MOV   R5,M_ENERI+1
	CALL  MULi
	CJNE  R7,#0,M_TIME8
%IF (0) THEN (                ; Mala energie z odchylky polohy
	MOV   A,R5
	MOV   R4,A
	MOV   A,R6
	MOV   R5,A
)ELSE(                        ; Velka energie z odchylky polohy
	CJNE  R6,#0,M_TIME8
	MOV   A,R5
)FI
	JNB   ACC.7,M_TIME9
M_TIME8:MOV   R4,#0FFH
	MOV   R5,#07FH
M_TIME9:CJNE  R1,#80H,M_TIMF0
	CALL  NEGi
M_TIMF0:MOV   M_ENERG,R4
	MOV   M_ENERG+1,R5

	MOV   R4,M_DFILT      ; R45 = M_DFILT * M_DPOL/65536
	MOV   R5,M_DFILT+1    ;       znamenko v F0
	MOV   A,R5
	MOV   C,ACC.7
	MOV   F0,C
	JNC   M_TIMF1
	CALL  NEGi
M_TIMF1:MOV   R2,M_DPOL
	MOV   R3,M_DPOL+1
	CALL  MULi
	JNB   M_D_256,M_TIMF2 ; Posun pri presnosti M_D_256
	MOV   A,R6            ; znamenko v F0 jednotky R67 zlomky v R5
	MOV   R5,A
	MOV   A,R7
	MOV   R6,A
	MOV   R7,#0
M_TIMF2:CLR   A               ; R23 = SPD_ACT-POZ_INC/2^24
	CLR   C               ;       zlomky v R1
	SUBB  A,POZ_INC+2
	MOV   R1,A
	MOV   A,SPD_ACT
	SUBB  A,POZ_INC+3
	MOV   R2,A
	MOV   A,SPD_ACT+1     ; Znamenkove rozsirit !!!!!!!!!!!!!!!
	SUBB  A,#0
	MOV   R3,A
	CLR   C
	MOV   A,R1
	JB    F0,M_TIMF3      ; R56 =  SPD_ACT-POZ_INC/2^24 +
	ADDC  A,R5            ;        + M_DFILT * M_DPOL/2^24
	MOV   R4,A            ;        zlomky v R4
	MOV   A,R2
	ADDC  A,R6
	MOV   R5,A
	MOV   A,R3
	ADDC  A,R7
	MOV   R6,A
	SJMP   M_TIMF4
M_TIMF3:SUBB  A,R5
	MOV   R4,A
	MOV   A,R2
	SUBB  A,R6
	MOV   R5,A
	MOV   A,R3
	SUBB  A,R7
	MOV   R6,A
M_TIMF4:MOV   A,R5            ; Zjisteni radu M_DFILT
	RLC   A
	CLR   A
	ADDC  A,R6
	JZ    M_TIMF5         ; Ulozeni M_DFILT a radu
	CLR   M_D_256
	MOV   M_DFILT,R5
	MOV   M_DFILT+1,R6
	SJMP  M_TIMF6
M_TIMF5:SETB  M_D_256
	MOV   M_DFILT,R4
	MOV   M_DFILT+1,R5
M_TIMF6:MOV   R4,M_DFILT
	MOV   R5,M_DFILT+1

	MOV   R2,M_SPDEI
	MOV   R3,M_SPDEI+1
	MOV   A,R5
	MOV   C,ACC.7
	MOV   F0,C
	JNC   M_TIMF7
	CALL  NEGi
M_TIMF7:CALL  MULi            ; R45 = -R45*M_SPDEI

%IF (0) THEN (
	JB    M_D_256,M_TIMF8 ; Mala energie z derivace
	MOV   A,R5
	MOV   R4,A
	MOV   A,R6
	MOV   R5,A
	SJMP  M_TIMF9
M_TIMF8:MOV   A,R6
	MOV   R4,A
	MOV   A,R7
	MOV   R5,A
	MOV   R7,#0
M_TIMF9:ANL   A,#080H
	ORL   A,R7
)ELSE(
	JNB   M_D_256,M_TIMF9 ; Velka energie z derivace
M_TIMF8:MOV   A,R5
	MOV   R4,A
	MOV   A,R6
	MOV   R5,A
	MOV   R6,#0
M_TIMF9:MOV   A,R5
	ANL   A,#080H
	ORL   A,R7
	ORL   A,R6
)FI
	MOV   C,F0
	JZ    M_TIMG1
	MOV   R4,#0FFH
	MOV   R5,#07FH
M_TIMG1:JC    M_TIMG2
	CALL  NEGi
M_TIMG2:MOV   R2,M_ENERG
	MOV   R3,M_ENERG+1
	CALL  ADDi
	JNB   OV,M_TIMG5
	MOV   C,ACC.7
M_TIMG3:JNC   M_TIMG4         ; Zajisteni max +- energie pri OV
	MOV   R4,#0FFH        ; v CY non SGN
	MOV   R5,#07FH
	SJMP  M_TIMG5
M_TIMG4:MOV   R4,#001H
	MOV   R5,#080H
M_TIMG5:MOV   M_ENERG,R4
	MOV   M_ENERG+1,R5

	CALL  S_ENERG	      ; Nastaveni energie

	JMP   A_SYS

ADDsi:  CLR   C               ; @R0:=@R0+@R1 .. delka R2
	MOV   A,@R0
	ADDC  A,@R1
	MOV   @R0,A
	INC   R0
	INC   R1
	DJNZ  R2,ADDsi+1
	RET

SUBsi:  CLR   C               ; @R0:=@R0-@R1 .. delka R2
	MOV   A,@R0
	SUBB  A,@R1
	MOV   @R0,A
	INC   R0
	INC   R1
	DJNZ  R2,SUBsi+1
	RET

MOVsi:  MOV   A,@R1           ; @R0:=@R1 .. delka R2
	MOV   @R0,A
	INC   R0
	INC   R1
	DJNZ  R2,MOVsi
	RET

;=================================================================
; Aquission subsystem

IC_AS_D SEGMENT DATA
IC_AS_I SEGMENT IDATA

RSEG    IC_AS_D
A_MSK:  DS    2    ; Maska pozadavku na informace
A_PER:  DS    1    ; Perioda vysilani bloku informaci
A_CPOS: DS    1    ; Pozice cteni prikazu
A_OPOS: DS    1    ; Pozice zapisu dat
TIMR_A: DS    1    ; Casovac vysilani A_SYS

RSEG    IC_AS_I
A_BUF:  DS    20H

RSEG    IC____C

A_SYS:  MOV   A,TIMR_A
	JZ    A_SYS01
	DEC   TIMR_A
A_SYS01:MOV   A,A_CPOS
	JNZ   A_SYS10
A_SYS_R:JMP   I_TIME1
A_SYS10:ANL   A,#07FH
	JNZ   A_SYS20
	MOV   A,TIMR_A
	JNZ   A_SYS_R
	MOV   TIMR_A,A_PER
	MOV   A_CPOS,#80H-1
	MOV   R0,#A_BUF
	MOV   @R0,S1ADR
A_SYS17:INC   R0
A_SYS18:MOV   A_OPOS,R0
A_SYS19:INC   A_CPOS
A_SYS20:MOV   R0,A_OPOS
	MOV   A,A_CPOS
	JNB   ACC.4,A_SYS30
A_SYS21:MOV   R4,#A_BUF       ; Pocatek bufferu
	MOV   R5,#0
	MOV   A,R0
	CLR   C
	SUBB  A,R4
	MOV   R2,A            ; Delka = A_OPOS-A_BUF
	MOV   R3,#0
	MOV   R6,#10H         ; Na slave 10H
	CALL  IIC_RQI         ; Odesli zpravu
	JNZ   A_SYS_R         ; Nepovedlo se
	MOV   A_CPOS,#80H     ; Povedlo se cekej na dalsi TIMR_A=0
	SJMP  A_SYS_R

A_SYS30:MOV   R7,A
	MOV   R4,A_MSK
	MOV   R5,A_MSK+1
	CALL  TSTBR45
	JZ    A_SYS19
	CJNE  R7,#80H,A_SYS31
	MOV   R1,#POZ_ACT     ; Vyslani polohy motoru
	MOV   R2,#3
	CALL  MOVsi
	JMP   A_SYS18
A_SYS31:
A_SYS40:MOV   A,R7
	JNB   ACC.3,A_SYS18
	XRL   A,ADCON         ; Vysilnani prevodniku
	ANL   A,#1FH
	CJNE  A,#18H,A_SYS_R  ; Neni ukoncen prevod daneho ADC
	MOV   A,ADCON         ; ADCI=1 a ADCS=0
	CLR   ACC.4
	MOV   @R0,A
	INC   R0
	MOV   ADCON,A
	MOV   @R0,ADCH
	JMP   A_SYS17

TSTBR45:JB    ACC.3,TSTBR5A   ; Testuje bit A registru R45 vysledek v A
TSTBR4A:ANL   A,#7            ; Testuje bit A registru R4 vysledek v A
	ADD   A,#TSTBR_t-TSTBR4t
	MOVC  A,@A+PC
TSTBR4t:ANL   A,R4
	RET
TSTBR5A:ANL   A,#7            ; Testuje bit A registru R5 vysledek v A
	ADD   A,#TSTBR_t-TSTBR5t
	MOVC  A,@A+PC
TSTBR5t:ANL   A,R5
	RET

TSTBR_t:DB    001h
	DB    002h
	DB    004h
	DB    008h
	DB    010h
	DB    020h
	DB    040h
	DB    080h


;=================================================================
; Prime cteni prevodniku pri pocatecnich testech

GET_ADN:MOV   AD_CNT,#0
	MOV   C,EA
	CALL  AD_PREC
	MOV   EA,C
	JB    B.3,GET_ADN
GET_AD1:MOV   B,ADCON
	JB    B.3,GET_AD1
	MOV   R5,ADCH
	ANL   A,#7
	MOV   ADCON,A
	MOV   A,B
	ANL   A,#0C0H
	MOV   R4,A
	RET

;=================================================================
; Prubezne cteni dat z prevodniku
;
RSEG    IC____B
AD_CNT: DS    1
AD_EN   BIT   AD_CNT.7
AD_ST   BIT   AD_CNT.6

RSEG    IC____I
AD_PRE: DS    2
AD_VCC: DS    2

RSEG    IC____C

AD_PREP:JNB   AD_EN,AD_PRER   ; Neni povolene prevadeni
	JB    AD_ST,AD_PRER
	MOV   A,AD_CNT
	ANL   A,#0FH
	ADD   A,#AD_TAB-AD_PRE1
	MOVC  A,@A+PC
AD_PRE1:JZ    AD_PRER
	JB    ACC.7,AD_PRE2
AD_PREC:MOV   B,ADCON
	JB    B.3,AD_PRER
	ANL   A,#07H
	MOV   ADCON,A
	SETB  ACC.3
	MOV   ADCON,A
	SETB  AD_ST
AD_PRER:RET

AD_PRE2:CLR   TOFF
	INC   AD_CNT
	INC   AD_CNT
	RET

AD_RD:  JNB   AD_EN,AD_RDR1
	JNB   AD_ST,AD_RDR1
	MOV   B,ADCON
	JB    B.3,AD_RDR
	MOV   A,AD_CNT
	ANL   A,#0FH
	ADD   A,#AD_TAB-AD_RD1
	MOVC  A,@A+PC
AD_RD1:	JZ    AD_RDR1
	JNB   ACC.6,AD_RD2
	SETB  TOFF
AD_RD2: XRL   A,B
	ANL   A,#7
	JNZ   AD_RDR1
	INC   AD_CNT
	MOV   A,AD_CNT
	ANL   A,#0FH
	ADD   A,#AD_TAB-AD_RD3
	MOVC  A,@A+PC
AD_RD3: INC   AD_CNT
	MOV   R0,A
	ANL   B,#7
	MOV   ADCON,B
	CALL  iLDR45i
	MOV   R2,#0
	MOV   R3,#0E0H
	CALL  MULi
	MOV   A,R6
	MOV   R4,A
	MOV   A,R7
	MOV   R5,A
	MOV   A,ADCON
	RL    A
	RL    A
	RL    A
	ANL   A,#6
	MOV   R2,A
	MOV   A,ADCH
	RL    A
	RL    A
	RL    A
	MOV   R3,A
	ANL   A,#7
	XCH   A,R3
	ANL   A,#NOT 7
	ORL   A,R2
	MOV   R2,A
	CALL  ADDi
	CALL  iSVR45i
AD_RDR1:CLR   AD_ST
AD_RDR:	RET

AD_TAB: DB    80H,0
	DB    05H,AD_VCC
	DB    42H,AD_PRE	; posunuto kvuli ustaleni
	DB    0

;=================================================================
; Casove preruseni
; predpoklada na stacku ACC PSW B

I_TIME1:CALL  AD_RD           ; Cteni prevodniku
	DJNZ  PPTIMR,I_TIM20  ; Preruseni s frekvenci 450 Hz
	INC   PPTIMR          ; Chyba Ping-Pongu
	MOV   A,#ERCOMMT
	CALL  SET_ERR

I_TIM20:DEC   TIME_CN
	JNB   TIME_CN.7,ITIMEQ
	MOV   A,#C_TIMED
	ADD   A,TIME_CN
	MOV   TIME_CN,A
	ANL   AD_CNT,#0C0H    ; Nove odstartovani cteni ADC

	MOV   PSW,#AR0        ; Pruchod s frekvenci 25 Hz
	PUSH  DPL
	PUSH  DPH
	JNB   FL_HSPD,I_TIM21
	SETB  FL_EDRF
I_TIM21:MOV   R0,#TIMR1
	MOV   B,#N_OF_T
I_TIME2:MOV   A,@R0
	JZ    I_TIME3
	DEC   A
	MOV   @R0,A
I_TIME3:INC   R0              ; Snizovani hodnoty monostabilnich citacu
	DJNZ  B,I_TIME2
	JNZ   I_TIMR1
	SETB  FL_EDRF         ; Pruchod s periodou 0.01 min
	SETB  FL_PPTI
	INC   NCCNT
	MOV   A,NCCNT
	JNZ   I_TIME4
	DEC   NCCNT
I_TIME4:MOV   A,IRC_ERR
	JZ    I_TIM42
	JNB   FL_STRT,I_TIM41
	MOV   A,TIMRJ
	JNZ   I_TIM42
I_TIM41:DEC   IRC_ERR
I_TIM42:MOV   TIMRI,#C_TIM06
	JNB   FL_STRT,I_TIM55
	JNB   FL_MOT,I_TIM55  ; Necerpa se
	INC   TIMRJ
	MOV   A,TIMRJ
	ADD   A,#-C_TMIN
	JNC   I_TIM51
	MOV   TIMRJ,#0
	INC   TIME
	MOV   A,TIME          ; Cas cerpani
	JNZ   I_TIM51
	INC   TIME+1
I_TIM51:JNB   FL_TIME,I_TIM55 ; Neni casovy limit
	MOV   R0,#MAX_TIM
	CALL  iLDR23i
	MOV   R4,TIME
	MOV   R5,TIME+1
	CALL  CMPi
	JC    I_TIM55         ; Neni prekrocen casovy limit
	MOV   A,#ERENDTI
	CALL  SET_ERR
I_TIM55:

I_TIMR1:POP   DPH
	POP   DPL
	MOV   R7,#C_TIMED
ITIMEQ: %WATCHDOG
	POP   B
	POP   PSW
	POP   ACC
	RETI


;=================================================================
; Ovladani displeje pres 8577

DispLen EQU     4
DispAdr EQU     74H

; Definice segmentu
lcda    SET     02h             ;lcd segment a
lcdb    SET     01h             ;lcd segment b
lcdc    SET     04h             ;lcd segment c
lcdd    SET     10h             ;lcd segment d
lcde    SET     40h             ;lcd segment e
lcdf    SET     08h             ;lcd segment f
lcdg    SET     20h             ;lcd segment g
lcdh    SET     80h             ;lcd segment h colon

RSEG    IC____I

WR_BUF: DS    DispLen+1

RSEG    IC____C

; Vypsani WR_BUF po IIC na DISPLAY
OUT_BUF:MOV   A,R6
	ANL   A,#0F8H
	MOV   R0,#WR_BUF
	MOV   @R0,A
	MOV   R6,#DispAdr
	MOV   R4,#WR_BUF
	MOV   R2,#DispLen+1
	MOV   R3,#0
	JMP   IIC_RQI

;Vystup cisla na LCD display pres prevod LCD_TBL a LCD_POS
;Vstup: R45   vypisovane cislo
;       R7    .. format vystupu cisla
;                  SLLLxADD
;            S   - signed
;            A   - znamenko nebo cislice
;                  jinak znamenko nebo blank
;            LLL - delka vypisu > 0
;            DD  - pocet desetinych mist
;       R6    .. pozice vypisu

iPRTLi: MOV   A,R5
	MOV   C,ACC.7
	MOV   A,R7
	JNB   ACC.7,iPRTLi3
	JC    iPRTLi1
	JB    ACC.2,iPRTLi3
	MOV   A,#lcdBlnk
	JNC   iPRTLi2
iPRTLi1:CALL  NEGi
        MOV   A,#lcdSig
iPRTLi2:CALL  iPRTLc
	MOV   A,R7
	ADD   A,#-10H
	MOV   R7,A
iPRTLi3:MOV   A,R7
	SWAP  A
	ANL   A,#7
	MOV   R1,A
iPRTLi9:DEC   R1
	MOV   A,R1
	RL    A
	CPL   A
	ADD   A,#O10E4i-1+5*2
	MOV   R0,A
	CALL  rLDR23i
iPRTL10:MOV   R0,#-1
iPRTL11:CJNE  R0,#9,iPRTL12
iPRTLiE:MOV   A,#lcd_Err
	CALL  iPRTLc
	MOV   A,R7
	ANL   A,#70H
	ADD   A,#-10H
	MOV   R7,A
	JNZ   iPRTLiE
	RET
iPRTL12:CALL  SUBi
	INC   R0
	JNC   iPRTL11
	CALL  ADDi
	MOV   A,R7
	ANL   A,#3
	DEC   A
	XRL   A,R1
	JZ    iPRTL13
	CLR   C
iPRTL13:MOV   A,R0
	MOV   ACC.7,C
	CALL  iPRTLc
	MOV   A,R7
	CLR   ACC.7
	ADD   A,#-10H
	MOV   R7,A
	ANL   A,#70H
	JNZ   iPRTLi9
	RET

; Rotace vlevo o 4 bity

RL4R45: MOV   A,R4
	SWAP  A
	MOV   R4,A
	ANL   A,#0F0H
	XCH   A,R4
	ANL   A,#00FH
	XCH   A,R5
	SWAP  A
	XCH   A,R4
	XRL   A,R4
	XCH   A,R4
	ANL   A,#0F0H
	XCH   A,R5
	ORL   A,R5
	XCH   A,R5
	XRL   A,R4
	MOV   R4,A
	RET

; Vystup hexa
;Vstup: R45   vypisovane cislo
;       R6    .. pozice vypisu
iPRTLhw:MOV   R7,#4
iPRTLh: MOV   A,R5
	SWAP  A
	ANL   A,#00FH
	CALL  iPRTLc
	CALL  RL4R45
	DJNZ  R7,iPRTLh
	RET

iPRTLc: MOV     R0,#0
	JNB     ACC.7,iPRTLc1
	CLR     ACC.7
	MOV     R0,#lcdH
iPRTLc1:ADD     A,#LCD_TBL-iPRTLck
	MOVC    A,@A+PC
iPRTLck:ORL     A,R0
iPRTLg: MOV     R0,A
	MOV     A,R6
	INC     R6
	ANL     A,#07H
	ADD     A,#LCD_POS-iPRTLcl
	MOVC    A,@A+PC
iPRTLcl:ADD     A,#WR_BUF+1
	XCH     A,R0
	MOV     @R0,A
	RET

lcdBlnk SET     10H
lcdSig  SET     17h
lcd_Err SET     17h ; = '-' dalsi moznost 0Eh = 'E'

LCD_POS:DB      3
	DB      2
	DB      0
	DB      1

LCD_TBL:db      lcda+lcdb+lcdc+lcdd+lcde+lcdf           ;0
	db      lcdb+lcdc                               ;1
	db      lcda+lcdb+lcdg+lcde+lcdd                ;2
	db      lcda+lcdb+lcdg+lcdc+lcdd                ;3
	db      lcdf+lcdg+lcdb+lcdc                     ;4
	db      lcda+lcdf+lcdg+lcdc+lcdd                ;5
	db      lcda+lcdf+lcdg+lcde+lcdd+lcdc           ;6
	db      lcda+lcdb+lcdc                          ;7
	db      lcda+lcdb+lcdc+lcdd+lcde+lcdf+lcdg      ;8
	db      lcda+lcdb+lcdf+lcdg+lcdc+lcdd           ;9
	db      lcda+lcdb+lcdf+lcdg+lcdc+lcde           ;A
	db      lcdc+lcdd+lcde+lcdf+lcdg                ;b
	db      lcda+lcdd+lcde+lcdf                     ;C
	db      lcde+lcdg+lcdd+lcdc+lcdb                ;d
	db      lcda+lcdd+lcde+lcdf+lcdg                ;E
	db      lcda+lcde+lcdf+lcdg                     ;F
	db      0                                       ;blank
	db      lcda                                    ;segment a
	db      lcdb                                    ;segment b
	db      lcdc                                    ;segment c
	db      lcdd                                    ;segment d
	db      lcde                                    ;segment e
	db      lcdf                                    ;segment f
	db      lcdg                                    ;segment g
	db      lcdb+lcdc+lcde+lcdf+lcdg                ;H
	db      lcdd+lcde+lcdf                          ;L
	db      lcda+lcdb+lcdf+lcdg                     ;degree
	db      lcdc+lcdd+lcde+lcdg                     ;lower degree
	db      0                                       ;spare
	db      0                                       ;spare
	db      0                                       ;spare
	db      0                                       ;spare
	db      lcdh                                    ;colon

;=================================================================
; Cteni a zapis do pameti EEPROM 8582

EEPRAdr EQU   0A0H ; IIC adresa pameti EEPROM

EEPR_RV:SETB  FL_EDCH

; Cteni z EEPROM[[R2]] R3 bytu na adresu R2+1
EEPR_RD:MOV   A,R2
	MOV   R4,A
	MOV   R2,#1
EEPR1:	MOV   R6,#EEPRAdr
EEPR_R1:CALL  IIC_RQI
	CALL  IIC_WME
	JNZ   EEPR_R1
	RET

; Zapis do EEPROM[[R2]] R3 bytu z adresy R2+1
EEPR_WR:MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	INC   A
	MOV   R2,A
	MOV   R3,#0
	SJMP  EEPR1

; Nacte do bufferu IIC_OUT paket dat z adresy R0 v EEPROM

EEPA_RD:JB    F0,EEPA_RR
	MOV   IIC_OUT,R0
EEPA_R1:CALL  IIC_CER
	MOV   R2,#1
	MOV   R3,#7
	MOV   R4,#IIC_OUT
	MOV   R6,#EEPRAdr
	CALL  IIC_RQI
	JNZ   EEPA_R1
	CALL  IIC_WME
	JNZ   EEPA_RE
	MOV   R0,#IIC_OUT
	MOV   R2,#7
	CALL  XOR_SUM
	XRL   A,@R0
	JZ    EEPA_RR
EEPA_RE:SETB  F0
	SETB  FL_EREE
EEPA_RR:RET

; Ulozi buffer IIC_OUT na adresu R0 v EEPROM

EEPA_WR:JB    F0,EEPA_WQ
	MOV   IIC_OUT,R0
	MOV   R0,#IIC_OUT
	MOV   R2,#7
	CALL  XOR_SUM
	MOV   @R0,A
EEPA_W1:CALL  IIC_CER
	MOV   R2,#8
	MOV   R3,#0
	MOV   R6,#EEPRAdr
	MOV   R4,#IIC_OUT
	CALL  IIC_RQI
	JNZ   EEPA_W1
	CALL  IIC_WME
	JZ    EEPA_WQ
EEPA_WE:SETB  F0
	SETB  FL_EREE
EEPA_WQ:RET

;=================================================================
; Vypis na LCD pro ladeni

PRTR23:	MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
PRTR45:	MOV   R7,#0C4H
	MOV   R6,#18H
;	CALL  iPRTLi
	CALL  iPRTLhw
	MOV   R6,#18H
	CALL  OUT_BUF
	RET

	END