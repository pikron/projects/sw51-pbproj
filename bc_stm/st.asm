$NOMOD51
;********************************************************************
;*         Spektrofotometricky detektor - DS.ASM                    *
;*                       Hlavni modul                               *
;*                  Stav ke dni 10.10.1997                          *
;*                      (C) Pisoft 1997                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_ADR)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_AL)
;$INCLUDE(%INCH_AF)
$INCLUDE(%INCH_UI)
$INCLUDE(%INCH_ULAN)
$INCLUDE(%INCH_UL_OI)
;$INCLUDE(%INCH_UL_OC)
$INCLUDE(%INCH_REGS)
$INCLUDE(%INCH_RS232)
$LIST

%DEFINE (WITH_UL_DY) (1)
%DEFINE (WITH_UL_DY_EEP) (1)
%DEFINE (WITH_ALLEGRO) (1)

EXTRN	CODE(PRINThb,PRINThw,INPUThw,SEL_FNC,PRINTli)
EXTRN	CODE(MONITOR)
EXTRN	CODE(xMDPDP,ADDATDP)

%IF(%WITH_UL_DY) THEN (
EXTRN	CODE(UD_INIT,UD_RQ)
)FI

EXTRN   CODE(IHEXLD)

PUBLIC	INPUTc,KBDBEEP,ERRBEEP,RES_STAR,DPLOCK

BACK_LIGHT BIT P3.4	; Podsvetleni


STM___C SEGMENT CODE
STM___D SEGMENT DATA
STM___B SEGMENT DATA BITADDRESSABLE
STM___X SEGMENT XDATA

STACK	DATA  80H

; More stack for uLan
SER_STACK SEGMENT DATA
RSEG	SER_STACK
	DS    7

%IF(%WITH_UL_DY_EEP) THEN (
PUBLIC	SER_NUM
CSEG AT	80A0H
SER_NUM:DS    8		; Instrument unigue serial number
XSEG AT	80A0H
	DS    8		; XDATA overlay
)FI

RSEG	STM___B

HW_FLG: DS    1

LEB_FLG:DS    1			; Blikani ledek

ITIM_RF BIT   HW_FLG.7
FL_DIPR	BIT   HW_FLG.6		; Displej pripojen
LEB_PHA	BIT   HW_FLG.5		; Pro blikani ledek

RSEG	STM___D

DPLOCK: DS    1			; Maximalni pouzivane DPTR

RSEG	STM___X

C_R_PER	EQU   5
REF_PER:DS    1

TMP:	DS    16
TMP1:	DS    16

RSEG	STM___C

RES_STAR:
	MOV   IEN0,#0
	MOV   IEN1,#0
	MOV   IEN2,#0
	MOV   IP0,#00000110B  ; S0INT pri 2,TF0 pri 1,IEX3/CC0 pri 3
	MOV   IP1,#00010100B  ; 0 .. S1/IE0/IADC, 1 .. TF0/IEX2
			      ; 2 .. IE1/IEX3, 3 .. TF1/CTF/IEX4
			      ; 4 .. S0/IEX5, 5 .. TF2/EXTF2/IEX6
	MOV   SP,#STACK
	MOV   DPSEL,#0
	MOV   DPLOCK,#0
	MOV   PSW,#0
	MOV   PCON,#10000000B ; Bd = OSC/12/16/(256-TH1)
	MOV   TMOD,#00100001B ; 16 bit timer 0;
	MOV   TCON,#01010101B ; citac 0 a 1 cita ; interapy hranou
	MOV   CMEN,#0	      ; Disable comparators
	%VECTOR(TIMER0,I_TIME); Realny cas z citace 0
	MOV   CINT25,#1
	SETB  ET0
	MOV   HW_FLG,#080H
	MOV   LEB_FLG,#0
	CLR   %BEEP_FL
	CLR   BACK_LIGHT
	MOV   STP_FLG,#0

	CLR   A
	MOV   DPTR,#STATUS
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#TIME
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A

	CALL  I_TIMRI
	CALL  I_TIMRI

	CALL  LCDINST
	JNZ   STRT30
	SETB  FL_DIPR
	CALL  LEDWR
STRT30:
	MOV   DPTR,#REF_PER
	MOV   A,#C_R_PER
	MOVX  @DPTR,A

	CALL  ADC_INI

	JMP   L0

INPUTc:	CALL  SCANKEY
	JZ    INPUTc
	RET

; Pipnuti na klavese klavesnice

;KBDBEEP:JMP   KBDSTDB
KBDBEEP:MOV   A,#2
BEEP:	MOV   DPTR,#BEEPTIM
	MOVX  @DPTR,A
	SETB  %BEEP_FL       ; Pipnuti
%IF(1) THEN (
	MOV   DPTR,#LED
	MOV   A,LED_FLG
	MOVX  @DPTR,A
)FI
	MOV   A,R2
	RET
ERRBEEP:MOV   A,#8
	JMP   BEEP

; *******************************************************************
; Konfigurace komunikaci RS232 a uLAN

RSEG	STM___X

COM_ADR:DS    2		; Adresa jednotky
COM_SPD:DS    2		; Rychlost komunikace
COM_GRP:DS    2		; Skupina pristroje

RSEG	STM___C

BAUDDIV_9600 EQU  36	; new baudgen; 52 pro 9600 a 16 MHz
			; new baudgen; 36 pro 9600 a 11.0592 MHz

COM_DIVT:
	DB    BAUDDIV_9600*4	;  2400
	DB    BAUDDIV_9600*2	;  4800
	DB    BAUDDIV_9600	;  9600
	DB    BAUDDIV_9600/2	; 19200
	DB    0

; Vraci v ACC divisor pro danou rychlost
COM_DIV:MOV   DPTR,#COM_SPD
	MOVX  A,@DPTR
	MOV   DPTR,#COM_DIVT
	MOVC  A,@A+DPTR
	RET

; Nastavi promennou a restartuje komunikaci
UW_COMi:CALL  UW_Mi
	JMP   I_U_LAN

COM_WR23:
	MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	JMP   UB_A_WR

; Nastaveni komunikace uLan
I_U_LAN:CLR   ES
%IF (0) THEN (
;	CLR   A
;	MOV   A,#10	; 9600 pro krystal 18432 kHz
	MOV   A,#3	; 19200 pro krystal 110592
;	MOV   A,#5	; 19200 pro krystal 18432 kHz
	MOV   R0,#1
	CALL  uL_FNC	; Rychlost
)FI
	MOV   DPTR,#COM_ADR
	MOVX  A,@DPTR
	MOV   R0,#2
	CALL  uL_FNC	; Adresa
	MOV   R2,#0
	MOV   R0,#3
	CALL  uL_FNC	; Delka IB OB
	MOV   R2,#0
	MOV   R0,#4
	CALL  uL_FNC	; Rychle bloky
%IF (1) THEN (
	SETB  BD	; new baudgen; -26 pro 19200 a 16 MHz
	ORL   PCON,#80H	; new baudgen; -18 pro 19200 a 11.0592 MHz
	CALL  COM_DIV
	CPL   A
	INC   A
	MOV   S0RELL,A
	MOV   S0RELH,#0FFH
	MOV   R0,#6
	CALL  uL_FNC	; Start s uzivatelskym baud
)ELSE(
	MOV   R0,#0
	CALL  uL_FNC	; Start
)FI
%IF(%WITH_UL_DY) THEN (
	%LDR45i(STATUS)
	MOV   R6,#2
	CALL  UD_INIT
)FI
	MOV   DPTR,#COM_GRP
	MOVX  A,@DPTR
	MOV   R0,#5
	CALL  uL_FNC	; Nastavit uL_GRP
	MOV   A,#1
	RET

%IF(%WITH_UL_DY_EEP) THEN (
INI_SERNUM:
	MOV   R2,#8	; pocet prenasenych byte
	MOV   R4,#0F0H	; pocatecni adresa v EEPROM
	MOV   DPTR,#SER_NUM
	CALL  EE_RD
	JZ    INI_SERNUM9
INI_SERNUM7:
	MOV   R2,#8
	MOV   DPTR,#SER_NUM
	MOV   A,#0FFH
INI_SERNUM8:
	MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R2,INI_SERNUM8
INI_SERNUM9:
	RET
WR_SERNUM:
	MOV   R2,#8	; pocet prenasenych byte
	MOV   R4,#0F0H	; pocatecni adresa v EEPROM
	MOV   DPTR,#SER_NUM
	CALL  EE_WR
	RET
)FI

; *******************************************************************
;
; Casove preruseni

PUBLIC  KBDTIMR

RSEG	STM___D

DTINT	EQU   2048 ; Prevod XTAL/12 na 450 Hz pro X 11.0592 MHz
DINT25  EQU   18   ; Delitel na 25 Hz
CINT25: DS    1

RSEG	STM___X

BEEPTIM:DS    1	   ; Timer delky pipani

N_OF_T  EQU   6    ; Pocet timeru
TIMR1:  DS    1    ; Timery jsou decrementovany
TIMR2:  DS    1    ; s frekvenci 25 Hz
TIMR_WAIT:
TIMR3:  DS    1
REF_TIM:DS    1	   ; Refresh timr
KBDTIMR:DS    1
TIMRI:  DS    1
TIME:   DS    2    ; Cas v 0.01 min              =====

DELAY_WDG EQU 100  ; Cas V 0.01 min po kterych dojde k vypnti
TIMR_WDG:DS   2	   ; Watchdog kontrolujici cinnost PC
EV_WDGT:DS    1	   ; Typ global event vyslany watchdogem

RSEG	STM___C

USING   3
I_TIME:	PUSH  ACC		; Cast s pruchodem 675 Hz
	PUSH  PSW
	MOV   PSW,#AR0
	PUSH  B
	PUSH  DPL
	PUSH  DPH

	CLR   TF0
	CLR   C
	CLR   TR0
S_T1_D  SET   7               ; zpozdeni rutiny
	MOV   A,TL0           ; 7 Cyklu
	SUBB  A,#LOW  (DTINT-S_T1_D)
	MOV   TL0,A
	MOV   A,TH0
	SUBB  A,#HIGH (DTINT-S_T1_D)
	MOV   TH0,A
	SETB  TR0
	CALL  ADC_D		; Cteni AD prevodniku

	DJNZ  CINT25,I_TIMR1	; Konec casti spruchodem 450 Hz
	MOV   CINT25,#DINT25	; Pruchod s frekvenci 25 Hz

	CALL  ADC_S		; Spusteni cyklu prevodu ADC
	MOV   DPTR,#BEEPTIM
	MOVX  A,@DPTR
	JZ    I_TIM80
	DEC   A
	MOVX  @DPTR,A
	JNZ   I_TIM80
	CLR   %BEEP_FL
%IF(1) THEN (
	MOV   DPTR,#LED       ; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
)FI
I_TIM80:%WATCHDOG

	CALL  uL_STR		; Vybuzeni uLan komunikace

	MOV   DPTR,#TIMR1
	MOV   B,#N_OF_T-1
I_TIME2:MOVX  A,@DPTR
	JZ    I_TIME3
	DEC   A
	MOVX  @DPTR,A
I_TIME3:INC   DPTR
	DJNZ  B,I_TIME2
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	JNB   ITIM_RF,I_TIMR1
	JB    ACC.7,I_TIME4
I_TIMR1:POP   DPH
	POP   DPL
	POP   B
	POP   PSW
	POP   ACC
	SETB  EA
I_TIMRI:RETI

I_TIME4:CLR   ITIM_RF	      ; Pruchod 0.6 sec
;	CALL  I_TIMRI
;	MOV   PSW,#AR0		; Banka 2
	ADD   A,#15
	MOVX  @DPTR,A

	MOV   A,LEB_FLG
	JBC   LEB_PHA,I_TIM42
	ORL   LED_FLG,A
	SETB  LEB_PHA
	SJMP  I_TIM43
I_TIM42:CPL   A
	ANL   LED_FLG,A
I_TIM43:SETB  ITIM_RF

	MOV   DPTR,#TIMR_WDG	; Watchdog nadrizeneho systemu
	MOVX  A,@DPTR
	ADD   A,#-1
	MOV   R1,A
	MOV   A,#1
	MOVC  A,@A+DPTR
	ADDC  A,#-1
	JNC   I_TIM45
	XCH   A,R1
	MOVX  @DPTR,A
	INC   DPTR
	XCH   A,R1
	MOVX  @DPTR,A
I_TIM45:

	JMP   I_TIMR1

; *******************************************************************
; Interni ADC v 80C517A

RSEG	STM___X

ADC_CNT	EQU   8		; Pocet pouzivanych prevodniku

ADC0:	DS    2
ADC1:	DS    2
ADC2:	DS    2
ADC3:	DS    2
ADC4:	DS    2
ADC5:	DS    2
ADC6:	DS    2
ADC7:	DS    2

LS_ADRF	SET   ADC2	; Prime mereni reference

RSEG	STM___C

; Cteci cyklus ADC
ADC_D:	MOV   B,ADCON0
	JB    B.4,ADC_DR	; BSY
	MOV   A,ADCON1
	ANL   A,#0FH
	ADD   A,#-ADC_CNT
	JC    ADC_DR		; Neni co dal prevadet
	ADD   A,#ADC_CNT
	RL    A
	ADD   A,#LOW  ADC0
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH ADC0
	MOV   DPH,A
	MOV   A,ADDATL
	ANL   A,#0C0H
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,ADDATH
	MOVX  @DPTR,A
	MOV   A,B
	ANL   A,#0FH
	INC   A
	SJMP  ADC_S2

; Odstartovani dalsiho cykly
ADC_S:  CLR   A
	MOV   B,ADCON0
	JB    B.4,ADC_DR
ADC_S2:	ANL   A,#0FH
	MOV   ADCON1,A
	MOV   DAPR,#0F0H
ADC_DR:	RET

; Inicializace ADC
ADC_INI:ANL   ADCON0,#NOT 03FH
	MOV   ADCON1,#ADC_CNT
	MOV   DAPR,#0F0H
	RET

; *******************************************************************
; Emulace SPI

SPI_PORT DATA P6	; Brana, ke ktere je pripojena pamet
SPI_DO	EQU   010H	; DI periferie
SPI_DI	EQU   080H	; DO periferie
SPI_CL	EQU   040H	; CL

; Nacte 8 bitu z periferie do ACC
SPI8IN:	MOV   A,#1
SPI8IN1:ORL   SPI_PORT,#SPI_CL		; Clock
	NOP
	ANL   SPI_PORT,#NOT SPI_CL
	PUSH  ACC
	MOV   A,SPI_PORT
	ANL   A,#SPI_DI
	ADD   A,#-1
	POP   ACC
	RLC   A
	JNC   SPI8IN1
	RET

; Vysle 8 bitu v ACC do periferie
SPI8OUT:SETB  C
SPI8OU0:RLC   A
SPI8OU1:JNC   SPI8OU2
	ORL   SPI_PORT,#SPI_DO
	SJMP  SPI8OU3
SPI8OU2:ANL   SPI_PORT,#NOT SPI_DO
SPI8OU3:ORL   SPI_PORT,#SPI_CL		; Clock
	NOP
	ANL   SPI_PORT,#NOT SPI_CL
	CLR   C
	RLC   A
	JNZ   SPI8OU1
	RET

SPI3OUT:ANL   A,#7
	SWAP  A
	RL    A
	ORL   A,#10H
	CLR   C
	SJMP  SPI8OU0

SPI7OUT:RL    A
	ORL   A,#1
	CLR   C
	SJMP  SPI8OU0

SPI6OUT:ANL   A,#03FH
	RL    A
	RL    A
	ORL   A,#2
	CLR   C
	SJMP  SPI8OU0

; *******************************************************************
; Prace s pameti EEPROM 9346

EEA_RD	EQU   1		; akce cteni
EEA_WR	EQU   2		; akce zapisu

EE_SEL	EQU   020H	; 1 .. select pameti

EE_WR_ATO EQU 1		; Pocet bytu zapisovanych naraz
EE_RD_ATO EQU 1		; Pocet bytu ctenych naraz
%DEFINE (EE_ORG16) (0)  ; Organizace po 16 bitech, jinak 8

; Prikazy v 1. trech bitech
EE_CMRD	EQU   110B	; read
EE_CMWR	EQU   101B	; write
EE_CMER	EQU   111B	; erase one byte
EE_CMEW	EQU   100B	; write/erase enable  .. ADR=11xxxxx
			; write/erase disable .. ADR=00xxxxx
			; erase all ..           ADR=10xxxxx
			; write all by data ..   ADR=01xxxxx

RSEG	STM___X

EE_PTR:	DS    2		; ukazatel do MEM_BUF na data
EEP_TAB:DS    2		; popis dat ulozenych v bloku EEPROM

MEM_BUF:DS    40H	; Buffer pameti EEPROM

RSEG	STM___C

; Vysle prikaz v ACC do pameti
EE_SCMD:ANL   SPI_PORT,#NOT EE_SEL
	ANL   SPI_PORT,#NOT SPI_CL
	NOP
	ORL   SPI_PORT,#SPI_CL
	NOP
	ANL   SPI_PORT,#NOT SPI_CL
	NOP
	ORL   SPI_PORT,#EE_SEL
	JMP   SPI3OUT


; Cteni pameti EEPROM
;	DPTR .. pointer na buffer
;	R2   .. pocet ctenych byte
;	R4   .. adresa, od ktere se cte

EE_RD:  MOV   R1,#0		; Inicializace XOR sum
EE_RD10:MOV   A,R2		; Priprava bloku 1-3 byte
	JZ    EE_RD40
	ADD   A,#-EE_RD_ATO
	MOV   R0,#EE_RD_ATO
	MOV   R2,A
	JC    EE_RD12
	ADD   A,#EE_RD_ATO
	MOV   R0,A
	MOV   R2,#0
EE_RD12:MOV   A,#EE_CMRD
	CALL  EE_SCMD
	MOV   A,R4
%IF(%EE_ORG16)THEN(
	RR    A
	CALL  SPI6OUT		; Organizace EEPROM 64*16
)ELSE(
	CALL  SPI7OUT		; Organizace EEPROM 128*8
)FI
EE_RD30:CALL  SPI8IN
	MOVX  @DPTR,A
	XRL   A,R1
	INC   A
	MOV   R1,A
	INC   DPTR
	INC   R4
	DJNZ  R0,EE_RD30
	SJMP  EE_RD10
EE_RD40:MOV   A,#EE_CMRD
	CALL  EE_SCMD
	MOV   A,R4
%IF(%EE_ORG16)THEN(
	RR    A
	CALL  SPI6OUT		; Organizace EEPROM 64*16
)ELSE(
	CALL  SPI7OUT		; Organizace EEPROM 128*8
)FI
	CALL  SPI8IN
	ANL   SPI_PORT,#NOT EE_SEL
	XRL   A,R1
	RET

EE_ERR:	SETB  F0
	ORL   A,#07H
	RET

; Zapis do pameti EEPROM
;	DPTR .. pointer na buffer
;	R2   .. pocet zapisovanych byte
;	R4   .. adresa, od ktere se zapisuje

EE_WR:	MOV   R1,#0		; Povoleni zapisu
	MOV   A,#EE_CMEW
	CALL  EE_SCMD
	MOV   A,#0FFH
	CALL  SPI7OUT
EE_WR10:MOV   A,R2		; Priprava bloku 1-3 byte
	JZ    EE_WR40
	ADD   A,#-EE_WR_ATO
	MOV   R0,#EE_WR_ATO
	MOV   R2,A
	JC    EE_WR12
	ADD   A,#EE_WR_ATO
	MOV   R0,A
	MOV   R2,#0
EE_WR12:MOV   A,#EE_CMWR	; Vyslani prikazu a adresy
	CALL  EE_SCMD
	MOV   A,R4
%IF(%EE_ORG16)THEN(
	RR    A
	CALL  SPI6OUT		; Organizace EEPROM 64*16
)ELSE(
	CALL  SPI7OUT		; Organizace EEPROM 128*8
)FI
EE_WR22:MOVX  A,@DPTR		; Vysilani bytu
	INC   DPTR
	PUSH  ACC
	XRL   A,R1
	INC   A
	MOV   R1,A
	POP   ACC
	CALL  SPI8OUT
	INC   R4
	DJNZ  R0,EE_WR22
	ANL   SPI_PORT,#NOT EE_SEL	;  Konec zapisu
	NOP
	ORL   SPI_PORT,#EE_SEL
EE_WR26:ORL   SPI_PORT,#SPI_CL
	NOP
	ANL   SPI_PORT,#NOT SPI_CL
	MOV   A,SPI_PORT
	ANL   A,#SPI_DI
	JZ    EE_WR26		; Cekat na SPI_CL=1
	SJMP  EE_WR10
EE_WR40:MOV   A,#EE_CMWR	; Vyslani prikazu a adresy
	CALL  EE_SCMD
	MOV   A,R4
%IF(%EE_ORG16)THEN(
	RR    A
	CALL  SPI6OUT		; Organizace EEPROM 64*16
)ELSE(
	CALL  SPI7OUT		; Organizace EEPROM 128*8
)FI
	MOV   A,R1		; Kontrolni byte
	CALL  SPI8OUT
	MOV   A,#EE_CMEW
	CALL  EE_SCMD
	MOV   A,#000H
	CALL  SPI7OUT
	ANL   SPI_PORT,#NOT EE_SEL	;  Konec zapisu
	CLR   A
	RET

EE_TSRD:MOV   R4,#10H
	MOV   R2,#22H
	MOV   DPTR,#MEM_BUF
	JMP   EE_RD

EE_TSWR:MOV   R4,#10H
	MOV   R2,#22H
	MOV   DPTR,#MEM_BUF
	JMP   EE_WR

; R45 := [EE_PTR++]
EEA_RDi:MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOV   R0,DPH
	MOV   A,DPL
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	RET

; [EE_PTR++] := R45
EEA_WRi:MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   R0,DPH
	MOV   A,DPL
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	RET

; provadi EEA_RD, EEA_WR pro iteger cislo
EEA_Mi:	CJNE  R0,#EEA_RD,EEA_Mi5
	CALL  EEA_RDi
	MOV   DPL,R2
	MOV   DPH,R3
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	RET
EEA_Mi5:CJNE  R0,#EEA_WR,EEA_Mi9
	MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  EEA_WRi
EEA_Mi9:RET

EEA_PRO:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#EE_PTR
	MOV   A,#LOW MEM_BUF
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH MEM_BUF
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
EEA_PR2:MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	ORL   A,R4
	JZ    EEA_Mi9
	PUSH  DPL
	PUSH  DPH
	MOV   A,R0
	PUSH  ACC
	CALL  JMPR45
	POP   ACC
	MOV   R0,A
	POP   DPH
	POP   DPL
	JMP   EEA_PR2

JMPR45:	MOV   A,R4
	PUSH  ACC
	MOV   A,R5
	PUSH  ACC
	RET

; Nastavi EEP_TAB a DPTR na R23
EEP_PTS:MOV   DPTR,#EEP_TAB
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	MOV   DPL,R2
	MOV   DPH,R3
	RET

; Ulozit data podle tabulky R23
EEP_WRS:CALL  EEP_PTS
	INC   DPTR
	INC   DPTR	; popis akci
	MOV   R0,#EEA_WR
	CALL  EEA_PRO
	MOV   DPTR,#EEP_TAB
	CALL  xMDPDP
	MOVX  A,@DPTR
	MOV   R2,A	; pocet prenasenych byte
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A	; pocatecni adresa v EEPROM
	MOV   DPTR,#MEM_BUF
	JMP   EE_WR

; Nacist data podle tabulky R23
EEP_RDS:CALL  EEP_PTS
	MOVX  A,@DPTR
	MOV   R2,A	; pocet prenasenych byte
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A	; pocatecni adresa v EEPROM
	MOV   DPTR,#MEM_BUF
	CALL  EE_RD
	JNZ   EEP_RD9
	MOV   DPTR,#EEP_TAB
	CALL  xMDPDP
	INC   DPTR
	INC   DPTR	; popis akci
	MOV   R0,#EEA_RD
	JMP   EEA_PRO
EEP_RD9:RET

; *******************************************************************
; Ukladani a cteni nastaveni z EEPROM

; Tabulky popisu akci pro ulozeni a cteni dat

; Tabulka pro SERVICE
EEC_SER:DB    020H	; pocet byte ukladanych dat
	DB    010H	; pocatecni adresa v EEPROM

	%W    (COM_ADR)
	%W    (EEA_Mi)

	%W    (COM_SPD)
	%W    (EEA_Mi)

	%W    (COM_GRP)
	%W    (EEA_Mi)

	%W    (STC_POS1)
	%W    (EEA_Mi)

	%W    (STC_SPD1)
	%W    (EEA_Mi)

	%W    (STC_POS2)
	%W    (EEA_Mi)

	%W    (STC_SPD2)
	%W    (EEA_Mi)

	%W    (0)
	%W    (0)

; *******************************************************************
; Rizeni krokace

; bipolarni motor pripojen
; 1 vinuti   proud P1.2, P1.3 znameko P1.4
; 2 vinuti   proud P1.5, P1.6 znameko P1.7
; Celkova uroven proudu nasobena CM7 na P4.7

; Povolovani fazi (CM0) P4.2 P4.3 az (CM1) P4.4 P4.5
; PWM do fazi druhe konfigurace z CM0 a CM1

; Casovani z Compare Timeru a CM6

%DEFINE	(WITH_SMOOT_PROF) (0)
%DEFINE	(WITH_NOPOSDIV)   (1)
%DEFINE	(WITH_LOGPROF)    (1)

M_STP	EQU   11111100B

RSEG	STM___B
STP_FLG:DS    1		; Priznaky
FL_BSY	BIT   STP_FLG.7	; Motor v pohybu
FL_ERR	BIT   STP_FLG.6	; Chyba
FL_BSY1	BIT   STP_FLG.5	; Konec pohybu pro kalibraci
FL_STCR	BIT   STP_FLG.4	; Spusteni behu cyklu
FL_STCF	BIT   STP_FLG.3	; Faze cyklu
FL_SWOL	BIT   STP_FLG.2	; Filtr koncaku

RSEG	STM___D

STP_DEL:DS    2
STP_FRC SET   STP_DEL   ; delicka frekvence

STP_RP:	DS    2		; pozadovana poloha
STP_AP:	DS    2		; aktualni poloha
STP_SPD:DS    1		; rychlost
STP_SPM:DS    1		; maximalni rychlost
STP_PHA:DS    1		; faze krokace
%IF(%WITH_LOGPROF)THEN(
STP_PHAST:DS  1		; inkrement mikrokroku
)FI
STP_DAT_END:

STP_CURM:DS   1		; Nasobitel proudu PWM7

RSEG	STM___C


; Casovani krokovani
; =================================
USING 2
STP_INT:PUSH  ACC
	PUSH  PSW
	CLR   P1.1		; !!!!!!!!!!!!!
	MOV   PSW,#AR0
	PUSH  B
	CALL  STP_GS
	JZ    STP_I25		; Pohyb dokoncen
	MOV   A,CRCL
	ADD   A,STP_DEL
   %IF(0)THEN(
	MOV   CRCL,A
	MOV   A,CRCH		; CRC+=STP_DEL
	ADDC  A,STP_DEL+1
	MOV   CRCH,A
   )ELSE(
	MOV   R0,A
	MOV   A,CRCH		; CRC+=STP_DEL
	ADDC  A,STP_DEL+1
	MOV   CRCH,A
	MOV   CRCL,R0
   )FI
	CLR   IEX3
	SJMP  STP_I29
STP_I25:CLR   FL_BSY
	CLR   EX3
STP_I29:POP   B
	SETB   P1.1		; !!!!!!!!!!!!!
	POP   PSW
	POP   ACC
	RETI


; Krokuje dokud STP_AP rovno STP_RP
; =================================
;stara se o rychlosti rozjezdu a brzdeni STP_SPD a STP_FRC
;vraci:	ACC = 0 .. konec pohybu
;rusi:	R1
STP_GS:
    %IF(%WITH_NOPOSDIV)THEN(
	MOV   A,STP_PHA
	ANL   A,#(STP_TUNN/4*4)-1
	JZ    STP_G08
	MOV   A,STP_SPD
	JNB   ACC.7,STP_G04
	JMP   STP_G47		; decrement ST_PHA
STP_G04:JMP   STP_G42		; increment ST_PHA
    )FI
STP_G08:CLR   C
	MOV   A,STP_RP
	SUBB  A,STP_AP
	MOV   R1,A
	MOV   A,STP_RP+1
	SUBB  A,STP_AP+1	; STP_AP-STP_RP ?
	JB    ACC.7,STP_G16
STP_G10:JNZ   STP_G12
	MOV   A,R1
	JNZ   STP_G14
	MOV   A,STP_SPD		; Chceme zastavit
	JB    ACC.7,STP_G1I
	JNZ   STP_G1D
	RET
; potreba pohybu nahoru
STP_G12:MOV   R1,#0FFH
STP_G14:MOV   A,STP_SPD
	JB    ACC.7,STP_G1I	; STP_SPD<0?  STP_SPD++
	CLR   C
	SUBB  A,R1
	JZ    STP_G20		; STP_SPD=R1?  STP_SPD
	JNC   STP_G1D		; STP_SPD>R1?  STP_SPD--
	MOV   A,STP_SPD
	CLR   C
	SUBB  A,STP_SPM
	JNC   STP_G20		; STP_SPD>=STP_SPM? STP_SPD
STP_G1I:INC   STP_SPD		; STP_SPD++
	SJMP  STP_G20
; potreba pohybu dolu
STP_G16:INC   A
	JZ    STP_G18
	MOV   R1,#000H
STP_G18:MOV   A,STP_SPD
	JNB   ACC.7,STP_G1D	; STP_SPD>=0?  STP_SPD--
	CLR   C
	SUBB  A,R1
	JZ    STP_G20		; STP_SPD=R1?  STP_SPD
	JC    STP_G1I		; STP_SPD<R1?  STP_SPD++
	MOV   A,STP_SPD
	DEC   A
	ADD   A,STP_SPM
	JNC   STP_G20		; STP_SPD<=-STP_SPM?  STP_SPD
STP_G1D:DEC   STP_SPD		; STP_SPD--
STP_G20:
; Nastaveni rychlosti
STP_G30:MOV   A,STP_SPD
	JNB   ACC.7,STP_G33
	CPL   A
	INC   A
STP_G33:			; A=abs(STP_SPD)
    %IF(NOT %WITH_SMOOT_PROF)THEN(
	RL    A
    )ELSE(
	CLR   ACC.0
    )FI
	MOV   R1,A
	ADD   A,#STP_PRT-STP_Gb1
	MOVC  A,@A+PC
STP_Gb1:MOV   STP_FRC,A
	MOV   A,R1
	ADD   A,#STP_PRT-STP_Gb2+1
	MOVC  A,@A+PC
STP_Gb2:JMP   STP_G39
%IF(NOT %WITH_LOGPROF)THEN(
STP_PRT:			; Normalni profil
$INCLUDE(ST_PRF0.ASM)
%IF(NOT %WITH_SMOOT_PROF)THEN(
	STP_PRN	SET   ($-STP_PRT)/2
)ELSE(
	STP_PRN	SET   ($-STP_PRT)
)FI
;-------------------------------------------
)ELSE(
STP_PRT:			; Nasobkovy profil
$INCLUDE(ST_PRF1.ASM)
%IF(NOT %WITH_SMOOT_PROF)THEN(
	STP_PRN	SET   ($-STP_PRT)/2
)ELSE(
	STP_PRN	SET   ($-STP_PRT)
)FI
)FI
STP_G39:
    %IF(%WITH_LOGPROF)THEN(
	MOV   R1,A
	ANL   A,#1FH
	MOV   STP_FRC+1,A	; nove zpozdeni mezi subkroky
	XRL   A,R1
	SWAP  A
	RR    A
	INC   A
	MOV   STP_PHAST,A	; novy inkrement mikrokroku
    )ELSE(
	MOV   STP_FRC+1,A
    )FI
; Generovani polohy
; a zmena faze STP_PHA podle STP_SPD a STP_PHAST
STP_G40:MOV   A,STP_SPD
	JZ    STP_G49
	JB    ACC.7,STP_G45
; Pohyb nahoru
	INC   STP_AP		; STP_AP+=1
	MOV   A,STP_AP
	JNZ   STP_G42
	INC   STP_AP+1
STP_G42:
    %IF(NOT %WITH_LOGPROF)THEN(
	INC   STP_PHA		; STP_PHA++
	MOV   A,STP_PHA
    )ELSE(
	MOV   A,STP_PHA		; STP_PHA+=STP_PHAST
	ADD   A,STP_PHAST
	MOV   STP_PHA,A
    )FI
	CJNE  A,#STP_TAN,STP_G49
	MOV   STP_PHA,#0
	SJMP  STP_G49
; Pohyb dolu
STP_G45:MOV   A,STP_AP		; STP_AP-=1
	DEC   STP_AP
	JNZ   STP_G47
	DEC   STP_AP+1
STP_G47:MOV   A,STP_PHA		; decrease STP_PHA
	JNZ   STP_G48
    %IF(NOT %WITH_LOGPROF)THEN(
	MOV   STP_PHA,#STP_TAN
STP_G48:DEC   STP_PHA		; STP_PHA--
    )ELSE(
	MOV   A,#STP_TAN
STP_G48:CLR   C
	SUBB  A,STP_PHAST
	MOV   STP_PHA,A		; STP_PHA+=STP_PHAST
    )FI
STP_G49:
STP_G90:JMP   STP_GEN

; Generuje vystup na motor podle STP_PHA
; ======================================
; musi vracet ACC != 0
STP_GEN:
%IF(NOT %WITH_ALLEGRO)THEN(	; Bipolarni rizeni
	MOV   A,STP_PHA		; pro UDN2916
	ADD   A,#STP_TAB-STP_Gb3
	MOVC  A,@A+PC
STP_Gb3:ORL   P1,#M_STP
	XRL   P1,A
	JMP   STP_GUN

; Tabulka fazi pro bipolarni rizeni s UDN2916 a CM7
;	      PIIPII
;	      H10H10
;             222111
STP_TAB:DB    11110000B
	DB    11100000B
	DB    11100100B
	DB    11100100B
	DB    11100100B
	DB    11100100B
	DB    11001000B
	DB    11001000B
	DB    11001000B
	DB    11001000B
	DB    11001000B
	DB    10101100B
	DB    10101100B
	DB    10101100B
	DB    10101100B
	DB    10001100B
	DB    10001100B
	DB    00001100B
	DB    00101100B
	DB    00101100B
	DB    00101100B
	DB    00101100B
	DB    01001000B
	DB    01001000B
	DB    01001000B
	DB    01001000B
	DB    01001000B
	DB    01100100B
	DB    01100100B
	DB    01100100B
	DB    01100100B
	DB    01100000B
	DB    01100000B
	DB    01110000B
	DB    01110100B
	DB    01110100B
	DB    01110100B
	DB    01110100B
	DB    01011000B
	DB    01011000B
	DB    01011000B
	DB    01011000B
	DB    01011000B
	DB    00111100B
	DB    00111100B
	DB    00111100B
	DB    00111100B
	DB    00011100B
	DB    00011100B
	DB    10011100B
	DB    10111100B
	DB    10111100B
	DB    10111100B
	DB    10111100B
	DB    11011000B
	DB    11011000B
	DB    11011000B
	DB    11011000B
	DB    11011000B
	DB    11110100B
	DB    11110100B
	DB    11110100B
	DB    11110100B
	DB    11110000B
STP_TAN	SET   $-STP_TAB
)FI

STP_GUN:
%IF(NOT %WITH_ALLEGRO)THEN(	; Uipolarni rizeni
	MOV   A,STP_PHA		; s pomoci PWM
	ADD   A,STP_PHA
	ADD   A,STP_PHA
	MOV   R1,A
	ADD   A,#STP_TUN-STP_GU1
	MOVC  A,@A+PC
STP_GU1:ANL   P4,A
	ANL   A,#03CH
	ORL   P4,A

	MOV   A,R1
	ADD   A,#STP_TUN-STP_GU2+1
	MOVC  A,@A+PC
STP_GU2:MOV   B,STP_CURM
	MUL   AB
	ORL   B,#0C0H
	MOV   CML0,B

	MOV   A,R1
	ADD   A,#STP_TUN-STP_GU3+2
	MOVC  A,@A+PC
STP_GU3:MOV   B,STP_CURM
	MUL   AB
	ORL   B,#0C0H
	MOV   CML1,B
	MOV   A,#1
	RET

; Tabulka fazi pro unipolarni rizeni P4,CM0,CM1
STP_TUN:DB    NOT 20H,  0, 64
	DB    NOT 28H,  1, 63
	DB    NOT 28H,  2, 62
	DB    NOT 28H,  5, 59
        DB    NOT 28H,  9, 55
	DB    NOT 28H, 14, 50
	DB    NOT 28H, 20, 44
        DB    NOT 28H, 26, 38
        DB    NOT 28H, 32, 32
	DB    NOT 28H, 38, 26
	DB    NOT 28H, 44, 20
	DB    NOT 28H, 50, 14
	DB    NOT 28H, 55,  9
	DB    NOT 28H, 59,  5
	DB    NOT 28H, 62,  2
        DB    NOT 28H, 63,  1
        DB    NOT 08H, 64,  0
        DB    NOT 18H, 63,  1
        DB    NOT 18H, 62,  2
        DB    NOT 18H, 59,  5
        DB    NOT 18H, 55,  9
	DB    NOT 18H, 50, 14
	DB    NOT 18H, 44, 20
        DB    NOT 18H, 38, 26
        DB    NOT 18H, 32, 32
	DB    NOT 18H, 26, 38
	DB    NOT 18H, 20, 44
        DB    NOT 18H, 14, 50
        DB    NOT 18H,  9, 55
	DB    NOT 18H,  5, 59
	DB    NOT 18H,  2, 62
	DB    NOT 18H,  1, 63
	DB    NOT 10H,  0, 64
	DB    NOT 14H,  1, 63
	DB    NOT 14H,  2, 62
        DB    NOT 14H,  5, 59
        DB    NOT 14H,  9, 55
        DB    NOT 14H, 14, 50
        DB    NOT 14H, 20, 44
        DB    NOT 14H, 26, 38
        DB    NOT 14H, 32, 32
	DB    NOT 14H, 38, 26
	DB    NOT 14H, 44, 20
        DB    NOT 14H, 50, 14
        DB    NOT 14H, 55,  9
	DB    NOT 14H, 59,  5
	DB    NOT 14H, 62,  2
        DB    NOT 14H, 63,  1
        DB    NOT 04H, 64,  0
	DB    NOT 24H, 63,  1
	DB    NOT 24H, 62,  2
	DB    NOT 24H, 59,  5
	DB    NOT 24H, 55,  9
	DB    NOT 24H, 50, 14
	DB    NOT 24H, 44, 20
	DB    NOT 24H, 38, 26
	DB    NOT 24H, 32, 32
	DB    NOT 24H, 26, 38
	DB    NOT 24H, 20, 44
	DB    NOT 24H, 14, 50
	DB    NOT 24H,  9, 55
	DB    NOT 24H,  5, 59
	DB    NOT 24H,  2, 62
	DB    NOT 24H,  1, 63
STP_TUNN SET  ($-STP_TUN)/3
)FI

STP_GALG:
%IF(%WITH_ALLEGRO)THEN(		; Bipolarni rizeni
	MOV   A,STP_PHA		; s obvodem A3957S
	RL    A
	MOV   R1,A
	ADD   A,#STP_TALG-STP_GA1
	MOVC  A,@A+PC
STP_GA1:ORL   P4,A
	ORL   A,#083H
	ANL   P4,A

	MOV   A,R1
	ADD   A,#STP_TALG-STP_GA2+1
	MOVC  A,@A+PC
STP_GA2:ORL   P1,A
	ORL   A,#083H
	ANL   P1,A

	MOV   A,#1
	RET

; Tabulka fazi pro rizeni s A3957S na P4,P1
STP_TALG:DB   1111100B, 000000B
	DB    1111100B,1001000B
	DB    1111100B,1001100B
	DB    1111000B,1010000B
	DB    1110100B,1010100B
	DB    1110000B,1011000B
        DB    1101100B,1011100B
        DB    1101000B,1100000B
        DB    1100100B,1100100B
        DB    1100000B,1101000B
        DB    1011100B,1101100B
        DB    1011000B,1110000B
        DB    1010100B,1110100B
        DB    1010000B,1111000B
        DB    1001100B,1111100B
        DB    1001000B,1111100B
        DB     000000B,1111100B
        DB    0001000B,1111100B
        DB    0001100B,1111100B
        DB    0010000B,1111000B
	DB    0010100B,1110100B
        DB    0011000B,1110000B
        DB    0011100B,1101100B
        DB    0100000B,1101000B
	DB    0100100B,1100100B
	DB    0101000B,1100000B
        DB    0101100B,1011100B
        DB    0110000B,1011000B
        DB    0110100B,1010100B
        DB    0111000B,1010000B
        DB    0111100B,1001100B
        DB    0111100B,1001000B
        DB    0111100B, 000000B
        DB    0111100B,0001000B
        DB    0111100B,0001100B
        DB    0111000B,0010000B
        DB    0110100B,0010100B
        DB    0110000B,0011000B
        DB    0101100B,0011100B
        DB    0101000B,0100000B
	DB    0100100B,0100100B
        DB    0100000B,0101000B
        DB    0011100B,0101100B
        DB    0011000B,0110000B
	DB    0010100B,0110100B
	DB    0010000B,0111000B
	DB    0001100B,0111100B
	DB    0001000B,0111100B
	DB     000000B,0111100B
	DB    1001000B,0111100B
	DB    1001100B,0111100B
	DB    1010000B,0111000B
	DB    1010100B,0110100B
	DB    1011000B,0110000B
	DB    1011100B,0101100B
	DB    1100000B,0101000B
	DB    1100100B,0100100B
	DB    1101000B,0100000B
	DB    1101100B,0011100B
	DB    1110000B,0011000B
	DB    1110100B,0010100B
	DB    1111000B,0010000B
	DB    1111100B,0001100B
	DB    1111100B,0001000B
STP_TUNN SET  ($-STP_TALG)/2
STP_TAN  SET  ($-STP_TALG)/2
)FI
	RET


; Inicializace krokace
STP_INI:CLR   EX3		; No interrupt from IEX3/CRC/CC0
	MOV   R0,#STP_RP	; Nulovani dat
	MOV   R2,#STP_DAT_END-STP_RP
	CLR   A
	MOV   STP_FLG,A
STP_IN4:MOV   @R0,A
	INC   R0
	DJNZ  R2,STP_IN4
	MOV   STP_SPM,#STP_PRN/4; Pocet kroku profilu
	; ---------------------	  Compare timer
	ANL   CTCON,#NOT 0FH	; input fosc/2
	MOV   CTRELH,#0FFH	; Base pro PWM 6 bit
	MOV   CTRELL,#0C0H
	ORL   CMSEL,#10000011B	; CM7,CM1,CM0 to CT, CM6 to T2
    %IF(NOT %WITH_ALLEGRO)THEN(
	ORL   CMEN, #11000011B	; Enable function
    )ELSE(
	ORL   CMEN, #10000011B	; Enable function
    )FI
	MOV   CMH0,#0FFH
	MOV   CML0,#0E0H
	MOV   CMH1,#0FFH
	MOV   CML1,#0F0H
	MOV   CMH7,#0FFH
	MOV   CML7,#0FFH
	;CLR   P4.7		; Povolit nasobici PWM
	; ---------------------	  Timer 2
	ANL   CTCON,#NOT 80H	; T2 clk=fosc
	ANL   T2CON,#NOT 82H	; Timer Function
	ORL   T2CON,#    01H
	ANL   T2CON,#NOT 18H	; No reload T2
	; ---------------------	  CRC/CC0
	SETB  T2CM		; Mode 1 for CRC a CC1 az CC3
	ANL   CCEN,#NOT 03H	; No function,
				; if CCEN|=2 then IEX3 on next match
	ORL   CCEN,#    02H	; Compare function CRC/CC0
	%VECTOR(053H,STP_INT)	; IEX2/CC0 enabled by EX3
	; ---------------------   dalsi parametry
	MOV   STP_CURM,#080H	; PWM7 nasobitel proudu
	CALL  UW_CURM1
   %IF(%WITH_LOGPROF)THEN(
	MOV   STP_PHAST,#1		; inkrement mikrokroku
   )FI
	RET

STP_SUP:CLR   EA
	MOV   A,STP_AP
	MOV   R0,STP_AP+1
	SETB  EA
	INC   A
	JNZ   STP_SX5
	INC   R0
STP_SX5:CLR   EA
	MOV   STP_RP,A
	MOV   STP_RP+1,R0
	SETB  EA
	JB    FL_BSY,STP_SX9
   %IF(%WITH_LOGPROF)THEN(
	MOV   STP_PHAST,#1		; inkrement mikrokroku
   )FI
	JMP   STP_GS
STP_SX9:RET

STP_SDO:CLR   EA
	MOV   A,STP_AP
	MOV   R0,STP_AP+1
	SETB  EA
	JNZ   STP_SDO1
	DEC   R0
STP_SDO1:DEC  A
	SJMP  STP_SX5

; Start pohybu na polohu v R45
STP_GO:	MOV   C,EA
	CLR   EA
	MOV   STP_RP,R4
	MOV   STP_RP+1,R5
	MOV   EA,C
STP_GO1:SETB  EX3
	SETB  FL_BSY
	MOV   A,#1
	RET

; Cteni aktualni pozice
STP_GAP:MOV   C,EA
	CLR   EA
	MOV   R4,STP_AP
	MOV   R5,STP_AP+1
	MOV   EA,C
	RET

; Nastaveni nasobitele proudu a PWM7
UW_CURM:CALL  UW_Mb
UW_CURM1:MOV  A,STP_CURM
	RR    A
	RR    A
	ORL   A,#0C0H
	MOV   CML7,A
	MOV   A,#1
	CLR   EA
	JB    FL_BSY,UW_CURM8
	CALL  STP_GUN
UW_CURM8:SETB EA
	RET

; *******************************************************************
; Rizeni cyklickeho behu

; FL_STCR FL_STCF

; Koncove spinace P7.2(ADC2) P7.3(ADC3)

RSEG	STM___X

STC_POS1:DS   2		; Position 1
STC_SPD1:DS   2		; Speed 1
STC_POS2:DS   2		; Position 1
STC_SPD2:DS   2		; Speed 1

RSEG	STM___C

STC_RUN:CLR   FL_STCF
	SETB  FL_STCR
	RET

STC_END:MOV   DPTR,#STATUS
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
STC_END2:CLR  FL_STCR
	JNB   FL_BSY,STC_END9
	CALL  STP_GAP
	CALL  STP_GO
STC_END9:RET


STC_DO: MOV   A,P7		; Test koncovych spinacu
	CPL   A
	ANL   A,#0CH
	MOV   C,FL_SWOL
	CLR   FL_SWOL
	JZ    STC_DO2
	SETB  FL_SWOL
	JNC   STC_DO2
	MOV   R4,A
	MOV   R5,#0FFH
	MOV   DPTR,#STATUS+1
	MOVX  A,@DPTR
	JB    ACC.7,STC_DO2
	MOV   DPTR,#STATUS
	CALL  xSVR45i
	CALL  STC_END2
STC_DO2:JNB   FL_STCR,STC_DO9
	JB    FL_BSY,STC_DO9
	MOV   C,FL_STCF
	CPL   C
	MOV   FL_STCF,C
	MOV   DPTR,#STC_POS1
	JC    STC_DO5
	MOV   DPTR,#STC_POS2
STC_DO5:MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   STP_SPM,A
	CALL  STP_GO
STC_DO9:RET

; *******************************************************************
;

DB_W_10:MOV   R0,#10H
	SJMP  DB_WAI1

DB_WAIT:MOV   R0,#0H
DB_WAI1:%WATCHDOG
	DJNZ  R1,DB_WAI1
	DJNZ  R0,DB_WAI1
	RET

RAMT:	MOV   DPTR,#08000H

RAMT10:	MOV   C,EA
	PUSH  PSW
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#80H
RAMT11:	MOV   A,R5
	RL    A
	MOV   R5,A
	MOVX  @DPTR,A
	CLR   A
	MOVC  A,@A+DPTR
	XRL   A,R5
	JNZ   RAMTERR
	CJNE  R5,#80H,RAMT11
	MOV   A,R4
	MOVX  @DPTR,A
	POP   PSW
	MOV   EA,C

;	MOV   A,#LCD_HOM
;	CALL  LCDWCOM
;	MOV   R4,DPL
;	MOV   R5,DPH
;	CALL  PRINThw

	INC   DPTR
	MOV   A,DPH
	CJNE  A,#HIGH RAMT,RAMT14
	INC   DPH
	INC   DPH
RAMT14:	CJNE  A,#0FFH,RAMT10

	MOV   DPTR,#08000H

RAMT20:	MOV   C,EA
	PUSH  PSW
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#7FH
RAMT21:	MOV   A,R5
	RL    A
	MOV   R5,A
	MOVX  @DPTR,A
	CLR   A
	MOVC  A,@A+DPTR
	XRL   A,R5
	JNZ   RAMTERR
	CJNE  R5,#7FH,RAMT21
	MOV   A,R4
	MOVX  @DPTR,A
	POP   PSW
	MOV   EA,C

;	MOV   A,#LCD_HOM
;	CALL  LCDWCOM
;	MOV   R4,DPL
;	MOV   R5,DPH
;	CALL  PRINThw

	INC   DPTR
	MOV   A,DPH
	CJNE  A,#HIGH RAMT,RAMT24
	INC   DPH
	INC   DPH
RAMT24:	CJNE  A,#0FFH,RAMT20

	RET

RAMTERR:PUSH  ACC
	MOV   A,#LCD_HOM+8
	CALL  LCDWCOM
	POP   ACC
	CALL  PRINThb
	MOV   A,#':'
	CALL  LCDWR
	MOV   R4,DPL
	MOV   R5,DPH
	CALL  PRINThw
RAMEHLD:SJMP  RAMEHLD

L0:	MOV   SP,#STACK
	SETB  EA
	JNB   FL_DIPR,L007
	MOV   DPTR,#DEVER_T
	CALL  cPRINT
L007:
	%LDMXi(COM_ADR,3)	; Adresa 3
	%LDMXi(COM_SPD,2)	; Rychlost 9600
	%LDMXi(COM_GRP,2)	; Skupina 2

	%LDR23i(EEC_SER)	; Cteni parametru z EEPROM
	CALL  EEP_RDS

	CALL  STP_INI

    %IF(%WITH_UL_DY_EEP) THEN (
	CALL  INI_SERNUM	; Initialize serial number
    )FI
	CALL  I_U_LAN		; Start komunikace uLan
	CALL  uL_OINI

	;MOV   R7,#0FFH		; Start komunikace RS232
	;MOV   R6,#-26*2
	;CALL  RS232_INI

	;CALL  DB_WAIT
	CALL  RAMT		; Kontrola pameti RAM

	JB    FL_DIPR,L090
				; Bez displeje
L080:
L081:	;CALL  UC_OI
	CALL  UI_PR		; uL slave OI
	;CALL  WL_DO		; rizeni vlnove delky
    %IF(%WITH_UL_DY) THEN (
	CALL  UD_RQ		; dynamicaka adresace
    )FI
	JMP   L081

L090:	MOV   A,#LCD_CLR
	CALL  LCDWCOM

	MOV   A,#K_PROG
	CALL  TESTKEY
	JZ    L_IHEX

L092:	JMP   UT

L_IHEX:	CALL  IHEXLD
	SJMP  L_IHEX

DEVER_T:DB    LCD_CLR,'%VERSION'
	DB    C_LIN2 ,' (c) PiKRON 2000',0

; *******************************************************************
; Komunikace s ostatnimi jednotkami pres uLan

; Definice jmenprikazu

%OID_ADES(AI_STATUS,STATUS,u2)
%OID_ADES(AI_ERRCLR,ERRCLR,e)
I_OFF	  EQU   250
%OID_ADES(AI_OFF,OFF,e)
I_ON	  EQU   251
%OID_ADES(AI_ON,ON,e)

; *******************************************************************
; Komunikace pres uLan - cast slave

RSEG	STM___X

TMP_U:	DS    16

RSEG	STM___C

; inicializace objektovych komunikaci
uL_OINI:
%IF(0) THEN (
	CALL  NUL_IP
	%LDR45i (UPP_0)
	MOV   DPTR,#UC_AUPP	; seznam parametru ostatnich jednotek
	CALL  xSVR45i
)FI
	%LDR45i (OID_1IN)	; seznam prijimanych prikazu
	%LDR67i (OID_1OUT)	; seznam vysilanych prikazu
	JMP    US_INIT

; Identifikace typu pristroje
PUBLIC	uL_IDB,uL_IDE
uL_IDB: DB    '.mt %VERSION .uP 51x'
    %IF(%WITH_UL_DY) THEN (
	DB    ' .dy'
    )FI
	DB    0
uL_IDE:

; Subsystem komunikace

; Status

G_STATUS:
	MOV   DPTR,#STATUS
	CALL  xLDR45i
	RET

ERRCLR_U:
	MOV   DPTR,#STATUS
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	RET

ON_U:   RET
	;LDR23i(GL_PWRON)
	;JMP   GLOB_RQ23

OFF_U:	RET
	;LDR23i(GL_STBY)
	;JMP   GLOB_RQ23

; Prijimane prikazy

OID_T	SET   $
	%W    (I_ERRCLR)
	%W    (OID_ISTD)
	%W    (AI_ERRCLR)
	%W    (ERRCLR_U)

%OID_NEW(I_ON,AI_ON)
	%W    (ON_U)

%OID_NEW(I_OFF,AI_OFF)
	%W    (OFF_U)

OID_1IN SET   OID_T

; Vysilane hodnoty

OID_T	SET   0

%OID_NEW(I_STATUS,AI_STATUS)
	%W    (UO_INT)
	%W    (0)
	%W    (G_STATUS)

OID_1OUT SET  OID_T

; *******************************************************************
; Promenne

RSEG	STM___X

STATUS:	DS    2

; *******************************************************************
; Globalni udalosti

RSEG	STM___C

; Posle globalni udalost
GLOB_RQ23:MOV A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	MOV   R7,#ET_GLOB
	JMP   EV_POST

; Zpracovani globalnich udalosti
GLOB_DO:MOV   A,R4
	MOV   R7,A
	MOV   A,R5
	MOV   R4,A
	MOV   DPTR,#GLOB_SF1
	JMP   SEL_FNC

; Globalni udalosti

; Tabulka globalnich udalosti

GLOB_SF1:

	DB    0

; *******************************************************************
; User interface

RSEG	STM___X

UT_UIAD:DS    40
UT_DATA:DS    40

RSEG	STM___C

UT_INIT:SETB  D4LINE
	SETB  FL_CMAV
	MOV   DPTR,#UI_MV_SX
	MOV   A,#20
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#4
	MOVX  @DPTR,A
	MOV   DPTR,#UT_UIAD
	MOV   UI_AD,DPL
	MOV   UI_AD+1,DPH
	CLR   A
	MOV   DPTR,#EV_BUF
	MOVX  @DPTR,A
	MOV   DPTR,#GR_ACT
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#REF_TIM
	MOVX  @DPTR,A
	RET

UT_TREF:MOV   DPTR,#REF_TIM
	MOVX  A,@DPTR
	JNZ   UT_TRE1
	MOV   DPTR,#REF_PER
	MOVX  A,@DPTR
	MOV   DPTR,#REF_TIM
	MOVX  @DPTR,A
	MOV   A,#1
	RET
UT_TRE1:CLR   A
	RET

UT:     CALL  UT_INIT
	MOV   R7,#ET_RQGR
	%LDR45i(UT_GR10)
	CALL  EV_POST

UT_ML:  CALL  EV_GET
	JZ    UT_ML50
UT_ML44:CJNE  R7,#ET_GLOB,UT_ML45
	CALL  GLOB_DO		; Globalni udalosti
	SJMP  UT_ML
UT_ML45:CALL  EV_DO
	JMP   UT_ML
UT_ML50:;CALL  WL_DO		; rizeni vlnove delky
	;CALL  GS_DO		; vyvola handler signalu

	;JNB   FL_ECRS,UT_ML53
	;CALL  RS_POOL		; Komunikace RS232
	;JNZ   UT_ML53
	;MOV   DPTR,#STATUS+1
	;MOVX  A,@DPTR
	;CALL  RS_RDYSND
    %IF(0)THEN(
	CALL   RS_RD
	JZ     UT_ML53
	MOV    A,R0
	CALL   RS_WR
    )FI

UT_ML53:;JNB   FL_ECUL,UT_ML55
	;CALL  UC_OI		; uL master OC
	CALL  UI_PR		; uL slave OI
	JB    uLF_INE,UT_ML55
	;JB    UCF_RDP,UT_ML57
UT_ML55:CALL  UT_TREF		; Urceni okamziku refrese od casu
	JZ    UT_ML60
	;JNB   FL_ECUL,UT_ML57
	;CALL  UC_REFR		; uL master OC
UT_ML57:;CLR   UCF_RDP
	SETB  FL_REFR
	SJMP  UT_ML65
UT_ML60:CALL  UT_ULED
    %IF(%WITH_UL_DY) THEN (
	CALL  UD_RQ		; uLan dynamic addressing
    )FI
	CALL  STC_DO		; Rizeni cyklickeho pohybu
UT_ML65:JMP   UT_ML

UT_ULED:CALL  UT_USLS
	JMP   LEDWR

UT_USLS:MOV   DPTR,#STATUS
	MOV   R2,#1 SHL LFB_PUMP2
	MOV   R2,#1 SHL LFB_END
	CALL  UT_USTS
	MOV   C,FL_BSY
	MOV   %PUMP1_FL,C
	MOV   C,FL_STCR
	MOV   %RUN_FL,C
	RET

; Stav na LED podle x[DPTR]
UT_USTS:MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
; Stav R45 na ledku s maskou R2
UT_UST1:MOV   A,R5
	JB    ACC.7,UT_UST7
	ORL   A,R4
UT_UST2:JZ    UT_UST5
UT_UST3:MOV   A,R2		; On
	CPL   A
	ANL   LEB_FLG,A
	CPL   A
	ORL   LED_FLG,A
	RET
UT_UST5:MOV   A,R2		; Off
	CPL   A
	ANL   LEB_FLG,A
	ANL   LED_FLG,A
	RET
UT_UST7:MOV   A,R2		; Error
	ORL   LEB_FLG,A
UT_UST9:RET

; *******************************************************************
; Podminene metody objektu a rozsireni UI

EXTRN	DATA(EV_TYPE)
EXTRN	CODE(FND_MINT,UB_GFLG,UB_DRAW1,UB_A_RD,UB_WRDPN)
EXTRN	CODE(BFL_EV,BFL_XOR,BFL_3ST,UB_A_WR)

OU_IMUT_T SET OU_I_E+3

; UIN nebo MUT
UIN_MUT_EV:
	MOV   A,EV_TYPE
	CJNE  A,#ET_DRAW,UIN_MUT_EV1
	SJMP  UIN_MUT_EV2
UIN_MUT_EV1:
	CJNE  A,#ET_REFR,UIN_MUT_EV8
UIN_MUT_EV2:
	CALL  UB_GFLG
	JB    ACC.UFB_EDV,UIN_MUT_EV8
	CALL  GOTO_A
	JZ    UIN_MUT_EV9

	MOV   R1,EV_TYPE
	CALL  UB_A_RD
	JB    F0,UIN_MUT_EV6
	JZ    UIN_MUT_EV5
	MOV   DPL,UI_AU
	MOV   DPH,UI_AU+1
	MOV   A,#OU_SX
	MOVC  A,@A+DPTR		; OU_SX
	MOV   R6,A
	%UI_OU2DP(OU_IMUT_T)
	CALL  FND_MINT
	JZ    UIN_MUT_EV7
	JMP   UB_WRDPN
UIN_MUT_EV5:
	MOV   A,EV_TYPE
	XRL   A,#ET_REFR
	JZ    UIN_MUT_EV9
UIN_MUT_EV6:
	MOV   A,#'-'
	JMP   UB_DRAW1
UIN_MUT_EV7:
	MOV   DPL,UI_AU
	MOV   DPH,UI_AU+1
	MOV   A,#OU_I_F
	MOVC  A,@A+DPTR
	MOV   R3,A
	MOV   A,#OU_SX
	MOVC  A,@A+DPTR
	MOV   R2,A
	JMP   PRINTli
UIN_MUT_EV8:
	JMP   UIN_EV
UIN_MUT_EV9:
	RET

; AUX nastavit jen mimo program
BFL_EV_NR:
	MOV   A,EV_TYPE
	CJNE  A,#ET_GETF,BFL_EV_NR1
	JB    %RUN_FL,BFL_EV_NR9
BFL_EV_NR1:
	JMP   BFL_EV
BFL_EV_NR9:
	RET

; INT nastavit jen mimo program
UIN_EV_NR:
	MOV   A,EV_TYPE
	CJNE  A,#ET_GETF,UIN_EV_NR1
	JB    %RUN_FL,UIN_EV_NR9
UIN_EV_NR1:
	JMP   UIN_EV
UIN_EV_NR9:
	RET

; Nacte hodnotu, pricte R2, a kontroluje max R3
MUT_AD23:
	MOV   A,R2
	PUSH  ACC
	MOV   A,R3
	PUSH  ACC
	CALL  UB_A_RD
	POP   B
	POP   ACC
	ADD   A,R4
	JB    ACC.7,MUT_AD23N
	MOV   R4,A
	CLR   C
	JBC   B.7,MUT_AD23K
	SUBB  A,B
	JC    MUT_AD23W
	MOV   R4,B
	DEC   R4
	SJMP  MUT_AD23W
MUT_AD23K:
	SUBB  A,B
	JC    MUT_AD23W
	MOV   R4,A
	SJMP  MUT_AD23W
MUT_AD23N:
	JNB   B.7,MUT_AD23M
	ADD   A,B
	MOV   R4,A
	DB    074H	; MOV  A,#n
MUT_AD23M:
	MOV   R4,#0
MUT_AD23W:
	MOV   R5,#0
	JMP   UB_A_WR

UR_Mb:	MOV   A,#OU_DPSI+1
	MOVC  A,@A+DPTR
	MOV   R5,A		; Znamenkove pri DPSI=0FF00H
	MOV   A,#OU_DP
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OU_DP+1
	MOVC  A,@A+DPTR
	JZ    UR_Mb1
	MOV   DPL,R0
	MOV   DPH,A
	MOVX  A,@DPTR
	MOV   R4,A
	SJMP  UR_Mb2
UR_Mb1:	MOV   A,@R0
	MOV   R4,A
UR_Mb2:	CJNE  R5,#0FFH,UR_Mb3
	JB    ACC.7,UR_Mb4
UR_Mb3: MOV   R5,#0
UR_Mb4:	MOV   A,#1
	RET


UW_Mb:	MOV   A,#OU_DP
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OU_DP+1
	MOVC  A,@A+DPTR
	JZ    UW_Mb1
	MOV   DPL,R0
	MOV   DPH,A
	MOV   A,R4
	MOVX  @DPTR,A
	MOV   A,#1
	RET
UW_Mb1:	MOV   A,R4
	MOV   @R0,A
	MOV   A,#1
	RET

; *******************************************************************

UT_SF1:	DB    K_MENU
	%W    (UT_GR70)
	%W    (GR_RQ23)

	DB    K_PROG
	%W    (UT_GR70)
	%W    (GR_RQ23)

	DB    K_PUMP1
	%W    (UT_GR10)
	%W    (GR_RQ23)

	DB    K_PUMP2
	%W    (UT_GR20)
	%W    (GR_RQ23)

	DB    K_RUN
	%W    (0)
	%W    (STC_RUN)

	DB    K_END
	%W    (0)
	%W    (STC_END)

UT_SFTN:DB    K_RIGHT
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    K_LEFT
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    K_UP
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    K_DOWN
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

UT_SF0:	DB    0

; *******************************************************************
; Manualni rezim rezim

; ---------------------------------
; Zakladni display
UT_GR10:DS    UT_GR10+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR10+OGR_BTXT-$
	%W    (UT_GT10)
	DS    UT_GR10+OGR_STXT-$
	%W    (UT_US10)
	DS    UT_GR10+OGR_HLP-$
	%W    (0)
	DS    UT_GR10+OGR_SFT-$
	%W    (UT_SF10)
	DS    UT_GR10+OGR_PU-$
	%W    (UT_U1001)
	%W    (UT_U1002)
	%W    (UT_U1003)
	%W    (UT_U1004)
	%W    (UT_U1005)
	%W    (UT_U1006)
	%W    (UT_U1007)
	%W    (UT_U1008)
	%W    (UT_U1050)
	%W    (0)

UT_GT10:DB    'STAT',C_NL
	DB    'AP',C_NL
	DB    'RP',C_NL
	DB    'SPD',C_NL
	DB    'SPM',C_NL
	DB    'CURM',C_NL
	DB    'VIN',0

UT_US10:DB    'ST- ST+ =GS= === ===',0

UT_SF10:
	DB    K_F1
	%W    (0)
	%W    (STP_SDO)

	DB    K_F2
	%W    (0)
	%W    (STP_SUP)

	DB    K_F3
	%W    (0)
	%W    (STP_GS)

	DB    -1
	%W    (UT_SF1)

UT_U1001: ; Status
	DS    UT_U1001+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1001+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1001+OU_X-$
	DB    6,0,6,1
	DS    UT_U1001+OU_HLP-$
	%W    (0)
	DS    UT_U1001+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1001+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1001+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (STATUS)		; DP
	DS    UT_U1001+OU_I_F-$
	DB    80H		; format I_F
	%W    (-200)		; I_L
	%W    (200)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1002: ; Actual pos
	DS    UT_U1002+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1002+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1002+OU_X-$
	DB    6,1,6,1
	DS    UT_U1002+OU_HLP-$
	%W    (0)
	DS    UT_U1002+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1002+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1002+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (STP_AP)		; DP
	DS    UT_U1002+OU_I_F-$
	DB    80H		; format I_F
	%W    (-30000)		; I_L
	%W    ( 30000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1003: ; Requested pos
	DS    UT_U1003+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1003+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1003+OU_X-$
	DB    6,2,6,1
	DS    UT_U1003+OU_HLP-$
	%W    (0)
	DS    UT_U1003+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1003+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (STP_GO)		; A_WR
	DS    UT_U1003+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (STP_RP)		; DP
	DS    UT_U1003+OU_I_F-$
	DB    80H		; format I_F
	%W    (-30000)		; I_L
	%W    ( 30000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1004: ; Actual speed
	DS    UT_U1004+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1004+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1004+OU_X-$
	DB    6,3,6,1
	DS    UT_U1004+OU_HLP-$
	%W    (0)
	DS    UT_U1004+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1004+OU_A_RD-$
	%W    (UR_Mb)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1004+OU_DPSI-$
	%W    (0FF00H)		; DPSI
	%W    (STP_SPD)		; DP
	DS    UT_U1004+OU_I_F-$
	DB    80H		; format I_F
	%W    (-127)		; I_L
	%W    ( 127)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1005: ; Maximal speed
	DS    UT_U1005+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1005+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1005+OU_X-$
	DB    6,4,6,1
	DS    UT_U1005+OU_HLP-$
	%W    (0)
	DS    UT_U1005+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1005+OU_A_RD-$
	%W    (UR_Mb)		; A_RD
	%W    (UW_Mb)		; A_WR
	DS    UT_U1005+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (STP_SPM)		; DP
	DS    UT_U1005+OU_I_F-$
	DB    0H		; format I_F
	%W    (0)		; I_L
	%W    (STP_PRN-1)	; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1006: ; Central PWM7 current multiplier
	DS    UT_U1006+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1006+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1006+OU_X-$
	DB    6,5,6,1
	DS    UT_U1006+OU_HLP-$
	%W    (0)
	DS    UT_U1006+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1006+OU_A_RD-$
	%W    (UR_Mb)		; A_RD
	%W    (UW_CURM)		; A_WR
	DS    UT_U1006+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (STP_CURM)	; DP
	DS    UT_U1006+OU_I_F-$
	DB    0H		; format I_F
	%W    (0)		; I_L
	%W    (255)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1007: ; Faze krokace STP_PHA
	DS    UT_U1007+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1007+OU_MSK-$
	DB    0
	DS    UT_U1007+OU_X-$
	DB    13,1,5,1
	DS    UT_U1007+OU_HLP-$
	%W    (0)
	DS    UT_U1007+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1007+OU_A_RD-$
	%W    (UR_Mb)		; A_RD
	%W    (UW_CURM)		; A_WR
	DS    UT_U1007+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (STP_PHA)		; DP
	DS    UT_U1007+OU_I_F-$
	DB    0H		; format I_F
	%W    (0)		; I_L
	%W    (255)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1008: ; Inkrement mikrokroku STP_PHAST
	DS    UT_U1008+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1008+OU_MSK-$
	DB    0
	DS    UT_U1008+OU_X-$
	DB    13,3,5,1
	DS    UT_U1008+OU_HLP-$
	%W    (0)
	DS    UT_U1008+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1008+OU_A_RD-$
	%W    (UR_Mb)		; A_RD
	%W    (UW_CURM)		; A_WR
	DS    UT_U1008+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (STP_PHAST)	; DP
	DS    UT_U1008+OU_I_F-$
	DB    0H		; format I_F
	%W    (0)		; I_L
	%W    (255)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1050: ; Napeti vnejsiho napajeni ADC7
	DS    UT_U1050+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1050+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1050+OU_X-$
	DB    6,6,6,1
	DS    UT_U1050+OU_HLP-$
	%W    (0)
	DS    UT_U1050+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1050+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1050+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (ADC7)		; DP
	DS    UT_U1050+OU_I_F-$
	DB    03H		; format I_F
	%W    (0)		; I_L
	%W    (255)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Zakladni display
UT_GR20:DS    UT_GR20+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR20+OGR_BTXT-$
	%W    (UT_GT20)
	DS    UT_GR20+OGR_STXT-$
	%W    (0)
	DS    UT_GR20+OGR_HLP-$
	%W    (0)
	DS    UT_GR20+OGR_SFT-$
	%W    (UT_SF20)
	DS    UT_GR20+OGR_PU-$
	%W    (UT_U2001)
	%W    (UT_U2002)
	%W    (UT_U2003)
	%W    (UT_U2004)
	%W    (UT_U2050)
	%W    (0)

UT_GT20:DB    'POS1',C_NL
	DB    'SPD1',C_NL
	DB    'POS2',C_NL
	DB    'SPD2',0

UT_SF20:DB    -1
	%W    (UT_SF1)

UT_U2001: ; Cyclus position 1
	DS    UT_U2001+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2001+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2001+OU_X-$
	DB    6,0,6,1
	DS    UT_U2001+OU_HLP-$
	%W    (0)
	DS    UT_U2001+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2001+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U2001+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (STC_POS1)	; DP
	DS    UT_U2001+OU_I_F-$
	DB    80H		; format I_F
	%W    (-20000)		; I_L
	%W    (20000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2002: ; Cyclus speed 1
	DS    UT_U2002+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2002+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2002+OU_X-$
	DB    6,1,6,1
	DS    UT_U2002+OU_HLP-$
	%W    (0)
	DS    UT_U2002+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2002+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U2002+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (STC_SPD1)	; DP
	DS    UT_U2002+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (STP_PRN-1)	; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2003: ; Cyclus position 2
	DS    UT_U2003+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2003+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2003+OU_X-$
	DB    6,2,6,1
	DS    UT_U2003+OU_HLP-$
	%W    (0)
	DS    UT_U2003+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2003+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U2003+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (STC_POS2)	; DP
	DS    UT_U2003+OU_I_F-$
	DB    80H		; format I_F
	%W    (-20000)		; I_L
	%W    (20000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2004: ; Cyclus speed 2
	DS    UT_U2004+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2004+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2004+OU_X-$
	DB    6,3,6,1
	DS    UT_U2004+OU_HLP-$
	%W    (0)
	DS    UT_U2004+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2004+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U2004+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (STC_SPD2)	; DP
	DS    UT_U2004+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (STP_PRN-1)	; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2050:
	DS    UT_U2050+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U2050+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2050+OU_X-$
	DB    0,4,13,1
	DS    UT_U2050+OU_HLP-$
	%W    (0)
	DS    UT_U2050+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U2050+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U2050+OU_B_P-$
	%W    (EEC_SER)		; Uzivatelske nastaveni
	DS    UT_U2050+OU_B_F-$
	%W    (EEP_WRS)		; Zapis do EEPROM
	DS    UT_U2050+OU_B_T-$
	DB    'Save settings',0

; *******************************************************************
; Mode menu

UT_GR70:DS    UT_GR70+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR70+OGR_BTXT-$
	%W    (UT_GT70)
	DS    UT_GR70+OGR_STXT-$
	%W    (0)		; UT_GS70
	DS    UT_GR70+OGR_HLP-$
	%W    (0)
	DS    UT_GR70+OGR_SFT-$
	%W    (UT_SF70)
	DS    UT_GR70+OGR_PU-$
	%W    (UT_U7001)
	%W    (UT_U7002)
	%W    (0)

UT_GT70:DB    '  Mode menu',0

UT_SF70:DB    -1
	%W    (UT_SF1)

UT_U7001:
	DS    UT_U7001+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U7001+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7001+OU_X-$
	DB    0,1,13,1
	DS    UT_U7001+OU_HLP-$
	%W    (0)
	DS    UT_U7001+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U7001+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U7001+OU_B_P-$
	%W    (EEC_SER)		; Uzivatelske nastaveni
	DS    UT_U7001+OU_B_F-$
	%W    (EEP_WRS)		; Zapis do EEPROM
	DS    UT_U7001+OU_B_T-$
	DB    'Save settings',0

UT_U7002:
	DS    UT_U7002+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U7002+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7002+OU_X-$
	DB    0,2,12,1
	DS    UT_U7002+OU_HLP-$
	%W    (0)
	DS    UT_U7002+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U7002+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U7002+OU_B_P-$
	%W    (UT_GR72)
	DS    UT_U7002+OU_B_F-$
	%W    (GR_RQ23)
	DS    UT_U7002+OU_B_T-$
	DB    'Comunication',0

; ---------------------------------
; Nastaveni parametru komunikace
UT_GR72:DS    UT_GR72+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR72+OGR_BTXT-$
	%W    (UT_GT72)
	DS    UT_GR72+OGR_STXT-$
	%W    (0)		; UT_GS72
	DS    UT_GR72+OGR_HLP-$
	%W    (0)
	DS    UT_GR72+OGR_SFT-$
	%W    (UT_SF72)
	DS    UT_GR72+OGR_PU-$
	%W    (UT_U7202)
	%W    (UT_U7203)
	%W    (UT_U7204)
	%W    (0)

UT_GT72:DB    'Adr kBd Mode Grp',0

UT_SF72:DB    -1
	%W    (UT_SF1)

UT_U7202:
	DS    UT_U7202+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U7202+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7202+OU_X-$
	DB    0,1,2,1
	DS    UT_U7202+OU_HLP-$
	%W    (0)
	DS    UT_U7202+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U7202+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_COMi)	; A_WR
	DS    UT_U7202+OU_DPSI-$
	%W    (0)		; DPSI
	%W    (COM_ADR)		; DP
	DS    UT_U7202+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (99)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U7203:
	DS    UT_U7203+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U7203+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7203+OU_X-$
	DB    3,1,5,1
	DS    UT_U7203+OU_HLP-$
	%W    (0)
	DS    UT_U7203+OU_SFT-$
	%W    (UT_SF7203)
	DS    UT_U7203+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_COMi)		; A_WR
	DS    UT_U7203+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (COM_SPD)		; DP
	DS    UT_U7203+OU_M_S-$
	%W    (0)
	DS    UT_U7203+OU_M_P-$
	DB    1,3+080H
	DS    UT_U7203+OU_M_F-$
	%W    (MUT_AD23)
	DS    UT_U7203+OU_M_T-$
	%W    (0FFFFH)
	%W    (00000H)
	%W    (UT_U7203T0)
	%W    (0FFFFH)
	%W    (00001H)
	%W    (UT_U7203T1)
	%W    (0FFFFH)
	%W    (00002H)
	%W    (UT_U7203T2)
	%W    (0FFFFH)
	%W    (00003H)
	%W    (UT_U7203T3)
	%W    (0)
UT_U7203T0:DB    ' 2400',0
UT_U7203T1:DB    ' 4800',0
UT_U7203T2:DB    ' 9600',0
UT_U7203T3:DB    '19200',0

UT_SF7203:
	DB    K_9
	%W    (2)
	%W    (COM_WR23)

	DB    K_1
	%W    (3)
	%W    (COM_WR23)

	DB    0

UT_U7204:
	DS    UT_U7204+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U7204+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7204+OU_X-$
	DB    13,1,2,1
	DS    UT_U7204+OU_HLP-$
	%W    (0)
	DS    UT_U7204+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U7204+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_COMi)	; A_WR
	DS    UT_U7204+OU_DPSI-$
	%W    (0)		; DPSI
	%W    (COM_GRP)		; DP
	DS    UT_U7204+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (99)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru


	END