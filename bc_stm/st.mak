#   Project file pro detektor AAA
#         (C) Pisoft 1996

st.obj    : st.asm st_prf1.asm config.h
	a51 st.asm $(par) debug

st_vec.obj: st_vec.asm config.h
	a51 st_vec.asm $(par)

rs232_1.obj: rs232_1.asm config.h
	a51 rs232_1.asm $(par) debug

ik_tty.obj: ik_tty.asm
	a51 ik_tty.asm $(par) debug

st.	  : st.obj st_vec.obj ik_tty.obj rs232_1.obj ..\pblib\pb.lib
	l51 st.obj,st_vec.obj,ik_tty.obj,rs232_1.obj,..\pblib\pb.lib code(09000H) xdata(8000H) ramsize(100h) ixref
#						      		     code(09000H)

st.hex    : st.
	ohs51 st

	  : st.hex
#	sendhex st.hex /p4 /m3 /t2 /g40960 /b11520
#	sendhex st.hex /p4 /m3 /t2 /g40960 /b16666
#	sendhex st.hex /p4 /m3 /t2 /g36864 /b9600
##	sendhex st.hex /p3 /m3 /t2 /g36864 /b19200
#	sendhex st.hex /p3 /m3 /t2 /g36864 /b9600
	unixcmd -d ul_sendhex -g 0
	pause
	unixcmd -d ul_sendhex st.hex -m 3 -g 0x9000

