% Unipolar stepper motor PWM computation

stpdiv=16
%stpdiv=4

pwm_lev=64

EN_TAB=[32,16,8,4];

X=[0:1/stpdiv:4-1e-9]';

if 0	% Sin
  A=sin(X*pi/2);
  B=cos(X*pi/2);
end
if 0	% Sqrt Sin
  A=sin(X*pi/2);
  B=cos(X*pi/2);
  A=sign(A).*sqrt(abs(A));
  B=sign(B).*sqrt(abs(B));
end
if 1	% Sin^2
  A=sin(X*pi/2);
  B=cos(X*pi/2);
  A=sign(A).*abs(A).^2;
  B=sign(B).*abs(B).^2;
end


figure(1);
plot(X,A,'+b',X,B,'+g');

eps=1/pwm_lev/10;
PWM0=round(abs(A*pwm_lev));
PWM1=round(abs(B*pwm_lev));
PEN=(B>eps)*EN_TAB(1)+(B<-eps)*EN_TAB(2)+...
    (A>eps)*EN_TAB(3)+(A<-eps)*EN_TAB(4);
    


figure(2);
plot(X,PWM0,'+b',X,PWM1,'+g',X,PEN,':r');

for i=1:length(X)
  fprintf(1,'	DB    NOT %02XH,%3d,%3d\n',PEN(i),PWM0(i),PWM1(i));
end;


return
% Bipolar stepper motor with UDN2916

UDN= bitshift(B>0,7)+...
     bitshift(round(3*abs(B)),5)+...
     bitshift(A<=0,4)+...
     bitshift(round(3*abs(A)),2);

fprintf(1,'\nBipolar motor with UDN2916\n\n');
for i=1:length(X)
  fprintf(1,'	DB    ');
  for j=7:-1:0
    fprintf(1,'%d',bitand(UDN(i),bitshift(1,j))~=0);
  end;
  fprintf(1,'B\n');
end;

