;********************************************************************
;*                    PB_TTY_C.H                                    *
;*     Include file se scankody a ovladanim displaye                *
;*                  Stav ke dni 24.03.1991                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

$INCLUDE(%INCH_LCD)

;********************************************************************

; Scan kody klaves

K_0      EQU   01BH
K_1      EQU   015H
K_2      EQU   018H
K_3      EQU   016H
K_4      EQU   021H
K_5      EQU   024H
K_6      EQU   022H
K_7      EQU   00FH
K_8      EQU   012H
K_9      EQU   010H
K_DP     EQU   01EH
K_PM     EQU   00DH ; 0FFH  ; 00DH ; +/- neni definovano
K_ENTER  EQU   01CH

K_PROG   EQU   011H
K_LEFT   EQU   023H
K_RIGHT  EQU   01FH
K_UP     EQU   00EH
K_DOWN   EQU   020H
K_INS    EQU   017H
K_DEL    EQU   014H
K_RUN    EQU   00AH
K_END    EQU   00CH
K_HELP   EQU   013H

K_MENU   EQU   00DH
K_F1     EQU   004H
K_F2     EQU   006H
K_F3     EQU   003H
K_F4     EQU   001H
K_F5     EQU   002H
K_ESC    EQU   005H

K_PUMP1  EQU   009H
K_PUMP2  EQU   007H
K_SAMPL  EQU   008H
K_DETEC  EQU   00BH
K_TEMPER EQU   01DH
K_VALVES EQU   01AH
K_STBY   EQU   019H

K_H_A    EQU   01DH
K_H_B    EQU   01AH
K_H_C    EQU   019H
K_H_D    EQU   017H
K_H_E    EQU   014H
K_H_F    EQU   013H

; Virtualni scankody

KV_V_OK	EQU    071H

;********************************************************************

; Kody led diod

LFB_RUN    EQU  7
LFB_END    EQU  6
LFB_PUMP1  EQU  5
LFB_PUMP2  EQU  4
LFB_SAMPL  EQU  3
LFB_DETEC  EQU  2
LFB_TEMPER EQU  1
LFB_VALVES EQU  0

%DEFINE (SAMPL_FL)  (LED_FLG.LFB_SAMPL)
%DEFINE (DETEC_FL)  (LED_FLG.LFB_DETEC)
%DEFINE (PUMP1_FL)  (LED_FLG.LFB_PUMP1)
%DEFINE (PUMP2_FL)  (LED_FLG.LFB_PUMP2)
%DEFINE (TEMPER_FL) (LED_FLG.LFB_TEMPER)
%DEFINE (RUN_FL)    (LED_FLG.LFB_RUN)
%DEFINE (VALVES_FL) (LED_FLG.LFB_VALVES)
%DEFINE (BEEP_FL)   (0F9H)

%DEFINE (PROG_FL)  (LED_FLH.1)
%DEFINE (STBY_FL)  (LED_FLH.0)

EXTRN    CODE(LCDINST,LCDNBUS,LCDWCOM,LCDWCO1,LCDWR,LCDWR1)
EXTRN    CODE(LCDINSM,PRINT,PRINTH,xPRINT,cPRINT)

EXTRN    CODE(SCANKEY,TESTKEY)
EXTRN    CODE(KBDSTDB)

EXTRN    CODE(LEDWR)
EXTRN    DATA(LED_FLG,LED_FLH,KBD_FLG)
