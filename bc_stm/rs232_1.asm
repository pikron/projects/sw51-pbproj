$NOMOD51
;********************************************************************
;*              Seriova komunikace - RS232.ASM                      *
;*                       Hlavni modul                               *
;*                  Stav ke dni 10.11.1996                          *
;*                      (C) Pisoft 1996                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_REGS)
$LIST

%DEFINE (DEBUG_FL)  (0)       ; Povoleni vlozeni debug rutin
%DEFINE (VECTOR_FL) (1)       ; Vyuzivaji se vektory preruseni v RAM
%DEFINE (INT_DI_EA) (1)	      ; Pouziva se EA misto ES pro lockovani
%DEFINE	(SPN)	    (1)	      ; Cislo serioveho portu

%IF(NOT %SPN)THEN(

%DEFINE  (SCON)		(SCON)
%DEFINE  (SBUF)		(SBUF)
%*DEFINE (JBC_TI(ADR))	(JBC  TI,%ADR)
%*DEFINE (JB_RI(ADR))	(JB   RI,%ADR)
%DEFINE  (INP_S)	(MOV  A,SBUF
			 CLR  RI)
%DEFINE  (OUT_S)	(MOV  SBUF,A)
%DEFINE	 (SETB_TI)	(SETB TI)
%DEFINE  (RTS_ON)	(SETB P3.5)
%DEFINE  (RTS_OFF)	(CLR  P3.5)
%*DEFINE (JNB_CTS(ADR))	(JB   P3.3,%ADR)
%DEFINE  (SINTADR)	(SINT)
%DEFINE  (INT_SEN)	(SETB ES)
%DEFINE  (INT_SDI)	(CLR  ES)

)ELSE(

%DEFINE  (SCON)		(S1CON)
%DEFINE  (SBUF)		(S1BUF)
%*DEFINE (JBC_TI(ADR))	(MOV  A,%SCON
			 JNB  ACC.1,$+8
			 ANL  %SCON,#NOT 2
			 SJMP %ADR)
%*DEFINE (JB_RI(ADR))	(MOV  A,%SCON
			 JB   ACC.0,%ADR)
%DEFINE  (INP_S)	(MOV  A,%SBUF
			 ANL  %SCON,#NOT 1)
%DEFINE  (OUT_S)	(MOV  %SBUF,A)
%DEFINE	 (SETB_TI)	(ORL  %SCON,#2)
%DEFINE  (RTS_ON)	(ORL  P6,#8)
%DEFINE  (RTS_OFF)	(ANL  P6,#NOT 8)
%*DEFINE (JNB_CTS(ADR))	(MOV  A,P6
			 JB   ACC.0,%ADR)
%DEFINE  (SINTADR)	(S1INT)
%DEFINE  (INT_SEN)	(ORL  IEN2,#1)
%DEFINE  (INT_SDI)	(ANL  IEN2,#NOT 1)

)FI


%IF(%INT_DI_EA)THEN(
%DEFINE  (INT_EN)	(SETB EA)
%DEFINE  (INT_DI)	(CLR  EA)
)ELSE(
%DEFINE  (INT_EN)	(%INT_SEN)
%DEFINE  (INT_DI)	(%INT_SDI)
)FI

%IF (%VECTOR_FL) THEN (
;  EXTRN    CODE(VEC_SET)
)ELSE(
CSEG AT %SINTADR
	JMP   S_INT
)FI

PUBLIC  SQ_INI,SQ_PUT,SQ_GET,SQ_FLUS
PUBLIC  RS232_INI,RS_RD,RS_WR,RS_RDFL,RS_WRFL
PUBLIC  RSF_LNT,RSF_RPLY,RSF_BSYO,RSF_RDYR

RS232_B SEGMENT DATA BITADDRESSABLE
RS232_C SEGMENT CODE
RS232_X SEGMENT XDATA

OSQ_LEN	SET	0
%STRUCTM(OSQ,IP,2)
%STRUCTM(OSQ,OP,2)
%STRUCTM(OSQ,BB,1)
%STRUCTM(OSQ,BE,1)

%IF(NOT %SPN)THEN(

XSEG	AT    0E800H
RS232_IBL EQU 4
RS232_OBL EQU 4
RS232_BUF:DS  (RS232_IBL+RS232_OBL)*256

)ELSE(

XSEG	AT    0F800H
RS232_IBL EQU 3
RS232_OBL EQU 4
RS232_BUF:DS  (RS232_IBL+RS232_OBL)*256

)FI

RSEG	RS232_B
RS_FLG:	DS    1
RSF_TIP BIT   RS_FLG.7  ; Probiha vysilani
RSF_TMW BIT   RS_FLG.6  ; Cekani na CTS
RSF_INE BIT   RS_FLG.5  ; Vstupni fronta neni prazdna
RSF_OVR BIT   RS_FLG.4  ; Vstupni fronta preplnena
; pro ucely aplikace
RSF_BSYO BIT  RS_FLG.3  ; Minuly stav MR_FLG.BMR_BSY
RSF_RDYR BIT  RS_FLG.2  ; Povoleni informovani o READY
RSF_RPLY BIT  RS_FLG.1  ; Potvrzovani radek
RSF_LNT BIT   RS_FLG.0  ; Vysli radku


RSEG	RS232_X
RS232_ISQ:DS  OSQ_LEN
RS232_OSQ:DS  OSQ_LEN

RSEG	RS232_C

; Ulozi znak v R0 do bufferu
; ret:	A=0   buffer full
;	R45=OP-IP-1 free space in queue
SQ_PUT:	MOV   A,#OSQ_IP		; R23 = IP
	MOVC  A,@A+DPTR
	MOV   R2,A
	MOV   A,#OSQ_IP+1
	MOVC  A,@A+DPTR
	MOV   R3,A
	MOV   A,#OSQ_BB		; R6 = BB
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   A,#OSQ_BE		; R7 = BE
	MOVC  A,@A+DPTR
	MOV   R7,A
	SETB  C			; R45 = OP-IP-1
	MOV   A,#OSQ_OP
%IF(%INT_DI_EA)THEN(%INT_DI)FI
	MOVC  A,@A+DPTR
	SUBB  A,R2
	MOV   R4,A
	MOV   A,#OSQ_OP+1
	MOVC  A,@A+DPTR
%IF(%INT_DI_EA)THEN(%INT_EN)FI
	SUBB  A,R3
	JNB   ACC.7,SQ_PUT2
	CLR   C
	SUBB  A,R6
	ADD   A,R7
SQ_PUT2:MOV   R5,A
	ORL   A,R4
	JZ    SQ_PUT9
	MOV   A,R2   		; IP = IP+1
	INC   A
%IF(%INT_DI_EA)THEN(%INT_DI)FI
	MOVX  @DPTR,A
	JNZ   SQ_PUT7
	INC   DPTR
	MOV   A,R3
	INC   A
	MOVX  @DPTR,A
	XRL   A,R7
	JNZ   SQ_PUT7
	MOV   A,R6
	MOVX  @DPTR,A
SQ_PUT7:MOV   DPL,R2
	MOV   DPH,R3
	MOV   A,R0
%IF(%INT_DI_EA)THEN(%INT_EN)FI
	MOVX  @DPTR,A
	MOV   A,#1
SQ_PUT9:RET

; Nacte znak  z bufferu do R0
; ret:	A=0   buffer empty
;	R45=IP-OP bytes in buffer
SQ_GET:	MOV   A,#OSQ_OP		; R23 = OP
	MOVC  A,@A+DPTR
	MOV   R2,A
	MOV   A,#OSQ_OP+1
	MOVC  A,@A+DPTR
	MOV   R3,A
	MOV   A,#OSQ_BB		; R6 = BB
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   A,#OSQ_BE		; R7 = BE
	MOVC  A,@A+DPTR
	MOV   R7,A
	CLR   C			; R45 = IP-OP
	MOV   A,#OSQ_IP
%IF(%INT_DI_EA)THEN(%INT_DI)FI
	MOVC  A,@A+DPTR
	SUBB  A,R2
	MOV   R4,A
	MOV   A,#OSQ_IP+1
	MOVC  A,@A+DPTR
%IF(%INT_DI_EA)THEN(%INT_EN)FI
	SUBB  A,R3
	JNB   ACC.7,SQ_GET2
	CLR   C
	SUBB  A,R6
	ADD   A,R7
SQ_GET2:MOV   R5,A
	ORL   A,R4
	JZ    SQ_GET9
	INC   DPTR
	INC   DPTR
	MOV   A,R2   		; OP = OP+1
	INC   A
%IF(%INT_DI_EA)THEN(%INT_DI)FI
	MOVX  @DPTR,A
	JNZ   SQ_GET7
	INC   DPTR
	MOV   A,R3
	INC   A
	MOVX  @DPTR,A
	XRL   A,R7
	JNZ   SQ_GET7
	MOV   A,R6
	MOVX  @DPTR,A
SQ_GET7:MOV   DPL,R2
	MOV   DPH,R3
%IF(%INT_DI_EA)THEN(%INT_EN)FI
	MOVX  A,@DPTR
	MOV   R0,A
	MOV   A,#1
SQ_GET9:RET

; vymaze data z bufferu OP:=IP
SQ_FLUS:MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R0
	INC   DPTR
	MOVX  @DPTR,A
	XCH   A,R0
	INC   DPTR
	MOVX  @DPTR,A
	RET

; Inicializuje buffer
; call: R45 .. buffer begin
;       R6  .. buffer length in 256 bytes
SQ_INI:	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	ADD   A,R5
	MOV   R5,A
	MOVX  @DPTR,A
	RET

;********************************************************************

USING   1
S_INT:  PUSH  PSW
	PUSH  ACC
	MOV   PSW,#AR0
	PUSH  DPL
	PUSH  DPH
	%JB_RI (S_REC)
	%JBC_TI(S_TRAN)
S_INT_R:POP   DPH
	POP   DPL
	POP   ACC
	POP   PSW
	RETI

S_REC:	%INP_S
	MOV   R0,A
	MOV   DPTR,#RS232_ISQ
	CALL  SQ_PUT
	SETB  RSF_INE		; Imput buffer not empty
	JZ    S_RECE
	CJNE  R5,#0,S_REC9
	CJNE  R4,#32,S_REC9
	%RTS_OFF
S_REC9:	JMP   S_INT_R
S_RECE: SETB  RSF_OVR
	SJMP  S_REC9

S_TRAN:	%JNB_CTS(S_TRAN4)
	CLR   RSF_TMW
	MOV   DPTR,#RS232_OSQ
	CALL  SQ_GET
	JZ    S_TRAN6
	MOV   A,R0
	%OUT_S
	SJMP  S_TRAN8
S_TRAN4:SETB  RSF_TMW
S_TRAN6:CLR   RSF_TIP		; Neni co vysilat
S_TRAN8:JMP   S_INT_R


; Nacte do R0 znak
; ret:  A = 0 imput empty
;	F0    overrun
RS_RD:  JNB   RSF_TMW,RS_RD2
	%JNB_CTS(RS_RD2)	; Aktivuje vysilani
	%INT_DI
	JB    RSF_TIP,RS_RD1
	SETB  RSF_TIP
	%SETB_TI
RS_RD1:	%INT_EN
RS_RD2:	CLR   A
	JNB   RSF_INE,RS_RD9
	CLR   RSF_INE
	MOV   DPTR,#RS232_ISQ
%IF(NOT %INT_DI_EA)THEN(%INT_DI)FI
	CALL  SQ_GET
%IF(NOT %INT_DI_EA)THEN(%INT_EN)FI
	JZ    RS_RD9
	SETB  RSF_INE
	CJNE  R4,#80H,RS_RD9
	CJNE  R5,#0,RS_RD9
	%RTS_ON
RS_RD9: JBC   RSF_OVR,RS_RDE
	RET
RS_RDE:	SETB  F0
	RET

; Zapise znak v R0
; ret:	F0    output full
RS_WR:	MOV   DPTR,#RS232_OSQ
%IF(NOT %INT_DI_EA)THEN(%INT_DI)FI
	CALL  SQ_PUT
%IF(NOT %INT_DI_EA)THEN(%INT_EN)FI
	JNZ   RS_WR1
	SETB  F0
RS_WR1:	%INT_DI
	JB    RSF_TIP,RS_WR4
	%JNB_CTS(RS_WR2)
	SETB  RSF_TIP
	%SETB_TI
	SJMP  RS_WR4
RS_WR2: SETB  RSF_TMW
RS_WR4:	%INT_EN
	RET

; Vymaze vse ze vstupni fronty
RS_RDFL:MOV   DPTR,#RS232_ISQ
	%INT_DI
	CALL  SQ_FLUS
	%INT_EN
	RET

; Vymaze vse z vystupni fronty
RS_WRFL:MOV   DPTR,#RS232_OSQ
	%INT_DI
	CALL  SQ_FLUS
	%INT_EN
	RET


RS232_INI:
%IF(NOT %SPN)THEN(
	%INT_DI
	ANL   TMOD,#00FH	; 8 bit reloadable timer
	ORL   TMOD,#020H
	SETB  TR1
	MOV   A,R7
	CPL   A
	INC   A
	MOV   TH1,A
)ELSE(
	MOV   A,R7
	CJNE  A,#0FFH,RS232_INI3
	MOV   A,R6
	MOV   S1REL,A
	SJMP  RS232_INI4
RS232_INI3:
	RL    A
	ADD   A,R7
	RL    A
	CPL   A
	INC   A
	MOV   S1REL,A
RS232_INI4:
)FI
RS232_INIB:
	%INT_DI
	MOV   %SCON,#11011000B
	MOV   RS_FLG,#0
%IF (%VECTOR_FL)THEN(
	MOV   R4,#%SINTADR
	MOV   DPTR,#S_INT
	CALL  VEC_SET
)FI
	%LDR45i(RS232_BUF)
	MOV   R6,#RS232_IBL
	MOV   DPTR,#RS232_ISQ
	CALL  SQ_INI
	MOV   R6,#RS232_OBL
	MOV   DPTR,#RS232_OSQ
	CALL  SQ_INI
	%RTS_ON
	%INT_EN
	%INT_SEN

	MOV   DPTR,#RS_ILBL
	CLR   A
	MOVX  @DPTR,A
	RET

;********************************************************************

PUBLIC	CPC2PS,SKIPSPC,ISALPHANUM,ISNUM,ISALPHA
PUBLIC	RS_RDLN,RS_WRLN,RS_RDLF,RS_WRL1,RS_WRLI
PUBLIC  RS_ILB,RS_OLB

LIBL_MAX EQU  80

RSEG	RS232_X
RS_ILBL:DS    1		; pocet znaku v bufferu radky
RS_ILB:	DS    LIBL_MAX	; buffer ctene radky pro textovy rezim
RS_OLBL:DS    1
RS_OLB:	DS    LIBL_MAX	; vystupni buffer

RSEG	RS232_C

; Prida znak v R0 do pstringu v DPTR
CPC2PS:	MOVX  A,@DPTR
CPC2PS1:INC   A
	MOVX  @DPTR,A
	ADD   A,DPL
	MOV   DPL,A
	CLR   A
	ADDC  A,DPH
	MOV   DPH,A
	MOV   A,R0
	MOVX  @DPTR,A
	RET

; Preskoci mezery ve stringu na DPTR, v ACC konec
SKIPSPC:MOVX  A,@DPTR
	CJNE  A,#' ',SKIPSPR
	INC   DPTR
	SJMP  SKIPSPC
SKIPSPR:RET

; Vraci ACC=0 pokud v ACC znak nebo cislo
ISALPHANUM:
	JB    ACC.6,ISALPHA
ISNUM:  ADD   A,#-'9'-1
	ADD   A,#'9'-'0'+1
	JC    IS000
	RET
ISALPHA:CLR   ACC.5
	ADD   A,#-'Z'-1
	ADD   A,#'Z'-'A'+1
	JC    IS000
	RET
IS000:  CLR   A
	RET

; Nacita radku ukoncenou 0D 0A
; nenacte-li celou radku vraci ACC=0
; je-li nactena vraci ACC=poctu znaku a DPTR na data

RS_RDLN:
RS_RDL1:CALL  RS_RD
	JZ    RS_RDL5
	MOV   DPTR,#RS_ILBL
	MOV   A,R0
	JZ    RS_RDL4
	CJNE  A,#0DH,RS_RDL3
	SJMP  RS_RDL4
RS_RDL3:CJNE  A,#0AH,RS_RDL6
RS_RDL4:MOVX  A,@DPTR
	JZ    RS_RDL1
	MOV   R0,A
	CLR   A
	MOVX  @DPTR,A
	MOV   A,R0
	INC   A
	ADD   A,DPL
	MOV   DPL,A
	CLR   A
	ADDC  A,DPH
	MOV   DPH,A
	CLR   A
	MOVX  @DPTR,A
	MOV   A,R0
	MOV   DPTR,#RS_ILB
RS_RDL5:CLR   C
	RET
RS_RDL6:CJNE  A,#1BH,RS_RDL7
	CLR   A
	SETB  C
	RET
RS_RDL7:MOVX  A,@DPTR		; jiz prijato znaku
	CJNE  A,#LIBL_MAX-1,RS_RDL8
	SETB  F0
	SJMP  RS_RDL1
RS_RDL8:CALL  CPC2PS1
	SJMP  RS_RDL1

; Vymaze vse z bufferu radky
RS_RDLF:MOV   DPTR,#RS_ILBL
	CLR   A
	MOVX  @DPTR,A
	RET

RS_WRLI:MOV   DPTR,#RS_ILB
	SJMP  RS_WRL1

; Vysle null terminated retezec plus 0D 0A
RS_WRLN:MOV   DPTR,#RS_OLB
RS_WRL1:MOVX  A,@DPTR
	JZ    RS_WRL8
	PUSH  DPL
	PUSH  DPH
	MOV   R0,A
	CALL  RS_WR
	POP   DPH
	POP   DPL
	JB    F0,RS_WRL9
	INC   DPTR
	SJMP  RS_WRL1
RS_WRL8:MOV   R0,#0DH
	CALL  RS_WR
	MOV   R0,#0AH
	CALL  RS_WR
RS_WRL9:RET

END