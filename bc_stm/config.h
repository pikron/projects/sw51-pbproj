;********************************************************************
;*                     CONFIG.ASM                                   *
;*     Konfiguracni soubor pro kompilaci klavesnice AA_IKB          *
;*                  Stav ke dni 12.02.1997                          *
;*                      (C) Pisoft 1997                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

%DEFINE (STM_VERSION) (v0.10)
%DEFINE (VERSION) (STM %STM_VERSION)
%DEFINE (HW_VERSION) (IK)
%DEFINE (USE_WR_MASK) (1)
%DEFINE (REG_STRUCT_TYPE) (PD)
%DEFINE (PBLIBDIR) (..\PBLIB\)
%DEFINE (PROCESSOR_TYPE) (517)

%DEFINE (INCH_NULL)(%PBLIBDIR%()PB_NULL.H)
%DEFINE (INCH_TTY) (IK_TTY.H)
%DEFINE (INCH_LCD) (%PBLIBDIR%()PB_LCD.H)
%DEFINE (INCH_AI)  (%PBLIBDIR%()PB_AI.H)
%DEFINE (INCH_AL)  (%PBLIBDIR%()PB_AL.H)
%DEFINE (INCH_AF)  (%PBLIBDIR%()PB_AF.H)
%DEFINE (INCH_ADR) (IK_ADR.H)
%DEFINE (INCH_ULAN)(%PBLIBDIR%()ULAN.H)
%DEFINE (INCH_UL_OI)(%PBLIBDIR%()UL_OI.H)
%DEFINE (INCH_UL_OC)(%PBLIBDIR%()UL_OC.H)
%DEFINE (INCH_MMAC)(%PBLIBDIR%()PB_MMAC.H)
%DEFINE (INCH_MR_DEFS)(%PBLIBDIR%()MR_DEFS.H)
%DEFINE (INCH_UI_DEFS)(%PBLIBDIR%()UI_DEFS.H)
%DEFINE (INCH_UI)  (%PBLIBDIR%()PB_UI.H)
%DEFINE (INCH_RS232)(%PBLIBDIR%()PB_RS232.H)
%DEFINE (INCH_BREAK)(%PBLIBDIR%()BREAK.H)
%DEFINE (VECTOR_TYPE) (DYNAMIC)
%DEFINE (WATCHDOG) (

)

; Processor depended features

%DEFINE (INCH_REGS) (%PBLIBDIR%()REG%(%PROCESSOR_TYPE).H)

; Zpusob definice vektoru preruseni a sluzeb

%IF (%EQS(%VECTOR_TYPE,STATIC)) THEN (
%*DEFINE (VECTOR  (VECNUM,VECADR)) ()
%*DEFINE (SVECTOR (VECNUM,VECADR)) (
CSEG    AT    %VECNUM
	JMP   %VECADR
)
)FI

%IF (%EQS(%VECTOR_TYPE,DYNAMIC)) THEN (
	EXTRN CODE(VEC_SET)
%*DEFINE (SVECTOR (VECNUM,VECADR)) ()
%*DEFINE (VECTOR  (VECNUM,VECADR)) (
	MOV   R4,#%VECNUM
	MOV   DPTR,#%VECADR
	CALL  VEC_SET
)
)FI

