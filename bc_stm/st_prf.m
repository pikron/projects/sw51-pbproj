
% vypocet profilu zrychleni


tend=0.7	% koncovy cas zrychleni v [S]

v_tend=2500	% koncova rychlost v [step/S]

% zrychleni klesa ke koncove hodnote a_tend=0

% derivace zrychleni v case 

% solve('v_tend=tend*(tend*da_t)-tend^2/2*da_t','da_t')

da_t=2*v_tend/tend^2;

% overeni vypoctu

t=[0:1e-4:tend]';

a_t0=tend*da_t;

a_t=a_t0-t*da_t;

v_t=t*a_t0-t.^2/2*da_t;

s_t=t.^2/2*a_t0-t.^3/3/2*da_t;

s_tend=s_t(end)

figure(1);
plot(t,a_t,'b',t,v_t,'g',t,s_t,'r')

% vypocet casu k poloze

% syms s_t t a_t0 t da_t
% solve(s_t=t^2/2*a_t0-t^3/3/2*da_t,'t')

s_t_x=[0:420]';

t_x= -1./2./da_t.*(-3.*s_t_x.*da_t.^2+a_t0.^3+3.^(1./2).*(s_t_x.*(3.*s_t_x.*da_t.^2-2.*a_t0.^3)).^(1./2).*da_t).^(1./3)-1./2.*a_t0.^2./da_t./(-3.*s_t_x.*da_t.^2+a_t0.^3+3.^(1./2).*(s_t_x.*(3.*s_t_x.*da_t.^2-2.*a_t0.^3)).^(1./2).*da_t).^(1./3)+a_t0./da_t-1./2.*i.*3.^(1./2).*(1./da_t.*(-3.*s_t_x.*da_t.^2+a_t0.^3+3.^(1./2).*(s_t_x.*(3.*s_t_x.*da_t.^2-2.*a_t0.^3)).^(1./2).*da_t).^(1./3)-a_t0.^2./da_t./(-3.*s_t_x.*da_t.^2+a_t0.^3+3.^(1./2).*(s_t_x.*(3.*s_t_x.*da_t.^2-2.*a_t0.^3)).^(1./2).*da_t).^(1./3));
t_x= real(t_x);

figure(2);
plot(t,s_t,'r',t_x,s_t_x,'b')

dt_x=diff(t_x);

v_max=1/dt_x(end)
t_max=t_x(end)

fd=fopen('st_prf1.asm','w');
for k=1:4:length(dt_x)
  ssinc=1;
  ssdel=dt_x(k)/16*1e6;
  while (ssinc<8)&(ssdel<400)
    ssinc=ssinc*2;
    ssdel=ssdel*2;
  end
  ssdel=round(ssdel);
  fprintf(1,'	%%W    ((%d)OR(%d SHL 13))\n',ssdel,ssinc-1);
  fprintf(fd,'	%%W    ((%d)OR(%d SHL 13))\r\n',ssdel,ssinc-1);
end;

fclose(fd)
