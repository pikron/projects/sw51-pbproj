; *******************************************************************
; Preddefinovane hodnoty parametru

STDR_A:	DB    0, 0,0,0, 0,0
	DB    0
	DB    0,0   ; 	(CASH_ZP)	; RP
	DB    0
	DB    0,0,0			; RS
	DB    15,  0,  2,  0, 40,  0	; P I D
	DB    000H,0, 000H,0, 0,07DH	; 1 2 ME
	%W    (5333)			; MS
	%W    (10)			; MA
	DB    0,0,0,0,0,0,0
	DB    LOW (AIR_CF0+6),LOW APWM0
	%W    (0168H)			; CFG
	DB    2				; JMP
	DW    %MR_REG_TYPE		; vektor na REGULATOR
	DB    2				; JMP
	DW    VR_REG			; skok na GENERATOR
	DS    STDR_A+OREG_A+OMR_SCM-$
	%W    (1)			; Meritko, nasobeni
	%W    (25)			; Meritko, deleni
	DS    STDR_A+OREG_A+OMR_JCA-$
	%W    (ADC7)			; OMR_JCA
	%W    (8000H)			; OMR_JCO
	%W    (-625)			; OMR_JCR
	%W    (200H)			; OMR_JCH
    %IF(%WITH_IICCM)THEN(
	DS    STDR_A+OREG_A+OMR_CMF-$
	DB    0
    )FI

%IF(%REG_COUNT GE 2) THEN(
	DS    STDR_A+OREG_B-$
	DB    0, 0,0,0, 0,0, 0,0,0,0, 0,0,0
	DB    7,  0,  4,  0, 20,  0	; P I D
	DB    000H,0, 000H,0, 0,027H	; 1 2 ME
	%W    (2000)			; MS
	%W    (4)			; MA
	DB    0,0,0,0,0,0,0
	DB    LOW (AIR_CF0+4),LOW APWM1
	%W    (0168H)			; CFG
	DB    2				; JMP
	DW    %MR_REG_TYPE		; vektor na REGULATOR
	DB    2				; JMP
	DW    VR_REG			; skok na GENERATOR
	DS    STDR_A+OREG_B+OMR_SCM-$
	%W    (0)			; Meritko, nasobeni
	%W    (0)			; Meritko, deleni
	DS    STDR_A+OREG_B+OMR_JCA-$
	%W    (ADC6)			; OMR_JCA
	%W    (8000H)			; OMR_JCO
	%W    (-625)			; OMR_JCR
	%W    (200H)			; OMR_JCH
    %IF(%WITH_IICCM)THEN(
	DS    STDR_A+OREG_B+OMR_CMF-$
	DB    0
    )FI
)FI

%IF(%REG_COUNT GE 3) THEN(
	DS    STDR_A+OREG_C-$
	DB    0, 0,0,0, 0,0, 0,0,0,0, 0,0,0
	DB    7,  0,  4,  0, 20,  0	; P I D
	DB    000H,0, 000H,0, 0,027H	; 1 2 ME
	%W    (2000)			; MS
	%W    (4)			; MA
	DB    0,0,0,0,0,0,0
	DB    LOW (AIR_CF0+2),LOW APWM2
	%W    (0168H)			; CFG
	DB    2				; JMP
	DW    %MR_REG_TYPE ;MR_RPULSEP	; vektor na REGULATOR
	DB    2				; JMP
	DW    VR_REG			; skok na GENERATOR
	DS    STDR_A+OREG_C+OMR_SCM-$
	%W    (0)			; Meritko, nasobeni
	%W    (0)			; Meritko, deleni
	DS    STDR_A+OREG_C+OMR_JCA-$
	%W    (ADC5)			; OMR_JCA
	%W    (8000H)			; OMR_JCO
	%W    (-30)			; OMR_JCR
	%W    (200H)			; OMR_JCH
    %IF(%WITH_IICCM)THEN(
	DS    STDR_A+OREG_C+OMR_CMF-$
	DB    0
    )FI
)FI

%IF(%REG_COUNT GE 4) THEN(
	DS    STDR_A+OREG_D-$
	DB    0, 0,0,0, 0,0, 0,0,0,0, 0,0,0
	DB    7,  0,  4,  0, 20,  0	; P I D
	DB    000H,0, 000H,0, 0,027H	; 1 2 ME
	%W    (2000)			; MS
	%W    (4)			; MA
	DB    0,0,0,0,0,0,0
	DB    LOW (AIR_CF1+6),LOW APWM2
	%W    (0168H)			; CFG
	DB    2				; JMP
	DW    %MR_REG_TYPE ;MR_RPULSEP	; vektor na REGULATOR
	DB    2				; JMP
	DW    VR_REG			; skok na GENERATOR
	DS    STDR_A+OREG_D+OMR_SCM-$
	%W    (0)			; Meritko, nasobeni
	%W    (0)			; Meritko, deleni
	DS    STDR_A+OREG_D+OMR_JCA-$
	%W    (ADC4)			; OMR_JCA
	%W    (8000H)			; OMR_JCO
	%W    (-30)			; OMR_JCR
	%W    (200H)			; OMR_JCH
    %IF(%WITH_IICCM)THEN(
	DS    STDR_A+OREG_D+OMR_CMF-$
	DB    0
    )FI
)FI

%IF(%REG_COUNT GE 5) THEN(
	DS    STDR_A+OREG_E-$
	DB    0, 0,0,0, 0,0, 0,0,0,0, 0,0,0
	DB    7,  0,  4,  0, 20,  0	; P I D
	DB    000H,0, 000H,0, 0,027H	; 1 2 ME
	%W    (2000)			; MS
	%W    (4)			; MA
	DB    0,0,0,0,0,0,0
	DB    LOW (AIR_CF1+4),LOW APWM2
	%W    (0168H)			; CFG
	DB    2				; JMP
	DW    %MR_REG_TYPE ;MR_RPULSEP	; vektor na REGULATOR
	DB    2				; JMP
	DW    VR_REG			; skok na GENERATOR
	DS    STDR_A+OREG_E+OMR_SCM-$
	%W    (0)			; Meritko, nasobeni
	%W    (0)			; Meritko, deleni
	DS    STDR_A+OREG_E+OMR_JCA-$
	%W    (ADC3)			; OMR_JCA
	%W    (8000H)			; OMR_JCO
	%W    (-30)			; OMR_JCR
	%W    (200H)			; OMR_JCH
    %IF(%WITH_IICCM)THEN(
	DS    STDR_A+OREG_E+OMR_CMF-$
	DB    0
    )FI
)FI

%IF(%REG_COUNT GE 6) THEN(
	DS    STDR_A+OREG_F-$
	DB    0, 0,0,0, 0,0, 0,0,0,0, 0,0,0
	DB    7,  0,  4,  0, 20,  0	; P I D
	DB    000H,0, 000H,0, 0,027H	; 1 2 ME
	%W    (2000)			; MS
	%W    (4)			; MA
	DB    0,0,0,0,0,0,0
	DB    LOW (AIR_CF1+2),LOW APWM2
	%W    (0168H)			; CFG
	DB    2				; JMP
	DW    %MR_REG_TYPE ;MR_RPULSEP	; vektor na REGULATOR
	DB    2				; JMP
	DW    VR_REG			; skok na GENERATOR
	DS    STDR_A+OREG_F+OMR_SCM-$
	%W    (0)			; Meritko, nasobeni
	%W    (0)			; Meritko, deleni
	DS    STDR_A+OREG_F+OMR_JCA-$
	%W    (ADC2)			; OMR_JCA
	%W    (8000H)			; OMR_JCO
	%W    (-30)			; OMR_JCR
	%W    (200H)			; OMR_JCH
    %IF(%WITH_IICCM)THEN(
	DS    STDR_A+OREG_F+OMR_CMF-$
	DB    0
    )FI
)FI

STDR_AE:

; *******************************************************************
; Specificky kod

RSEG	MARS__X

TEST_LN:DS    2		; Delka testu v mm
TEST_SP:DS    2		; Rychlost pro test v mm/min
RET_SP:	DS    2		; Rychlost pro navrat v mm/min
REP_MOTX:DS   1		; Pro urceni rychlosti pro UP/DOWN

RSEG	MARS__C

SP2INC:	%LDR23i(C_SP2INC)
	CALL  MULi
	MOV   A,#C_SP2INC_SR
	CALL  SHRl		; R45=(R45*C_SP2INC)>>C_SP2INC_SR
	MOV   A,#OMR_MS
	CALL  MR_GPA1
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A		; OMR_MS=R45
	RET

TEST_START:
	MOV   R1,#0		; motor A
	MOV   DPTR,#TEST_SP
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A		; R45=TEST_SP
	CALL  SP2INC		; set speed
	MOV   DPTR,#TEST_LN
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A		; R45=TEST_LN
	%LDR23i(1000)
	CALL  MULi
	MOV   R0,#1		; meritko SCL_MR z logickych na motor
	MOV   R3,#0		; absolutni pohyb
	JMP   UW_MRPl3		; start relativniho pohybu

RET_START:
	MOV   R1,#0		; motor A
	MOV   DPTR,#RET_SP
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A		; R45=RET_SP
	CALL  SP2INC		; set speed
	CLR   A
        MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R7,A
	MOV   R0,#1		; meritko SCL_MR z logickych na motor
	MOV   R3,#0		; absolutni pohyb
	JMP   UW_MRPl3		; start relativniho pohybu

CS_MOTUP:
	%LDR45i(-106*10)
	SJMP  CS_MOTX
CS_MOTDOWN:
	%LDR45i(106*10)
CS_MOTX:MOV   DPTR,#KBDTIMR
	MOVX  A,@DPTR
	ADD   A,#-10
	JNC   CS_MOTX10
	%LDR23i(1)		; poprve
	CLR   A
	MOV   DPTR,#REP_MOTX
	MOVX  @DPTR,A
	SJMP  CS_MOTX20
CS_MOTX10:
	MOV   A,#1
	MOV   DPTR,#BEEPTIM	; jiz v repeatu
	MOVX  @DPTR,A
	MOV   DPTR,#KBDTIMR
	MOVX  @DPTR,A
	MOV   DPTR,#REP_MOTX
	MOVX  A,@DPTR
	INC   A
	JZ    CS_MOTX15
	MOVX  @DPTR,A
CS_MOTX15:
	DEC   A
	MOV   B,#25
	DIV   AB
	INC   A
	MOV   R2,A
	MOV   R3,#0
CS_MOTX20:
	CALL  MULsi
	%LDR67i(45)
	MOV   R1,#0		; motor A
	MOV   R2,#3		; jed rychlosti R45
	JMP   GO_GSPT		; po R67 ms zastavuj


; Inicializovat pohyb s definovanou rychlosti
UW_MRPl_RS:
	PUSH  DPL
	PUSH  DPH
	MOV   A,#OU_DP
	MOVC  A,@A+DPTR
	CJNE  A,#-1,UW_MRPl_RS1
	MOV   DPTR,#MR_UIAM
	MOVX  A,@DPTR
UW_MRPl_RS1:
	MOV   R1,A		; cislo motoru
UW_MRPl_RS2:
	MOV   DPTR,#TMP
	CALL  xSVl
	MOV   DPTR,#RET_SP
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A		; R45=RET_SP
	CALL  SP2INC		; set speed
	MOV   DPTR,#TMP
	CALL  xLDl
	POP   DPH
	POP   DPL
	JMP   UW_MRPl

DISP_SCROL:
	MOV   DPTR,#UI_AV_OY
	MOVX  A,@DPTR
	ADD   A,R2
	MOV   R2,A
	DEC   A
	ADD   A,R3
	JNC   DISP_SCROL9
	MOV   A,R2
	MOVX  @DPTR,A
;	MOV   DPTR,#UI_CP_Y
;	MOVX  @DPTR,A
	SETB  FL_DRAW
DISP_SCROL9:
	RET


EEC_PAR:DB    020H	; pocet byte ukladanych dat
	DB    080H	; pocatecni adresa v EEPROM

	%W    (TEST_LN)
	%W    (EEA_Mi)

	%W    (TEST_SP)
	%W    (EEA_Mi)

	%W    (RET_SP)
	%W    (EEA_Mi)

	%W    (0)
	%W    (0)
	
CUSTHOOK_UT:
	%LDR23i(EEC_PAR)
	CALL  EEP_RDS		; nacteni user parametru
	JMP   UT		; start user interface

CS_GR23_ZERO:
	MOV   A,R2
	PUSH  ACC
	MOV   A,R3
	PUSH  ACC
	CALL  CLR_ALL
	POP   ACC
	MOV   R3,A
	POP   ACC
	MOV   R2,A
	JMP   GR_RQ23

; *******************************************************************
; Definice user interface

RSEG	MARS__C

; ---------------------------------
; Definice klaves

UT_SF1:	DB    K_H_A
	DB    0,0
	%W    (UT_SAMG)

%IF(%REG_COUNT GE 2) THEN(
	DB    K_H_B
	DB    1,0
	%W    (UT_SAMG)

%IF(%REG_COUNT GE 3) THEN(
	DB    K_H_C
	DB    2,0
	%W    (UT_SAMG)

%IF(%REG_COUNT GE 4) THEN(
	DB    K_H_D
	DB    3,0
	%W    (UT_SAMG)

%IF(%REG_COUNT GE 5) THEN(
	DB    K_H_E
	DB    4,0
	%W    (UT_SAMG)

%IF(%REG_COUNT GE 6) THEN(
	DB    K_H_F
	DB    5,0
	%W    (UT_SAMG)
)FI )FI )FI )FI )FI

	DB    004H	; K_LIST
	%W    (UT_GR10)
	%W    (GR_RQ23)

	DB    017H	; K_MENU
	%W    (UT_GR60)
	%W    (GR_RQ23)

	DB    K_MODE
	%W    (UT_GR10)
	%W    (GR_RQ23)

	DB    008H	; K_ZERO
	%W    (UT_GR15)
	%W    (GR_RQ23)

	DB    K_RUN
	%W    (0)
	%W    (TEST_START)	;(GO_JCTT)

	DB    007H	; K_CLRERR
	%W    (0)
	%W    (CER_ALL)

	DB    009H	; K_STOP
	%W    (0)
	%W    (STP_ALL)

	DB    K_HOLD
	%W    (0)
	%W    (RET_START)	;(HH_ALL)

	DB    005H	; K_MOTUP
	%W    (0)
	%W    (CS_MOTUP)

	DB    00BH	; K_MOTDOWN
	%W    (0)
	%W    (CS_MOTDOWN)

   %IF(0)THEN(
	DB    K_PROG
	%W    (0)
	%W    (IHEXLD)
   )FI

UT_SFTN:

%IF(%WITH_IICKB)THEN(
	DB    043H
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    041H
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    042H
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    046H
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)
)FI

	DB    K_RIGHT
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    K_LEFT
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    K_DOWN
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    K_UP
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

UT_SF0:	DB    0

; *******************************************************************
; Zakladni display

UT_GR10:DS    UT_GR10+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR10+OGR_BTXT-$
	%W    (UT_GT10)
	DS    UT_GR10+OGR_STXT-$
	%W    (0)
	DS    UT_GR10+OGR_HLP-$
	%W    (0)
	DS    UT_GR10+OGR_SFT-$
	%W    (UT_SF10)
	DS    UT_GR10+OGR_PU-$
	%W    (UT_U1001)
	%W    (UT_U1002)
	%W    (UT_U1003)
	%W    (UT_U1004)
%IF(%WITH_IIC)THEN(
	%W    (UT_U1031)
	%W    (UT_U1032)
)FI
	%W    (UT_U1040)
%IF (%WITH_F_SEL) THEN (
	%W    (UT_U1050)
)FI
%IF(0) THEN (
	%W    (UT_U1081)
	%W    (UT_U1082)
	%W    (UT_U1083)
)FI
	%W    (0)

	      ;0123456789012345
UT_GT10:DB    'AP          mm',C_NL
	DB    'TL      mm',C_NL
	DB    'TS      mm/min',C_NL
	DB    'RS      mm/min',C_NL
	DB    C_NL
	DB    C_NL
	DB    C_NL
	DB    'Reg frq',C_NL
    %IF(0)THEN(
	DB    'AE      BE',C_NL
	DB    'CE'
    )FI
	DB    0

UT_SF10:
	DB    -1
	%W    (UT_SF1)

; Aktualni poloha
UT_U1001:
	DS    UT_U1001+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1001+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1001+OU_X-$
	DB    3,0,8,1
	DS    UT_U1001+OU_HLP-$
	%W    (0)
	DS    UT_U1001+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1001+OU_A_RD-$
	%W    (UR_MRPl)		; A_RD
	%W    (UW_MRPl_RS)	; A_WR
	DS    UT_U1001+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    0,0		; DP
	DS    UT_U1001+OU_I_F-$
	DB    0C3H		; format I_F
	%W    (8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; Delka testu
UT_U1002:
	DS    UT_U1002+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1002+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1002+OU_X-$
	DB    3,1,4,1
	DS    UT_U1002+OU_HLP-$
	%W    (0)
	DS    UT_U1002+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1002+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1002+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TEST_LN)		; DP
	DS    UT_U1002+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (220)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; Rychlost testu
UT_U1003:
	DS    UT_U1003+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1003+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1003+OU_X-$
	DB    3,2,4,1
	DS    UT_U1003+OU_HLP-$
	%W    (0)
	DS    UT_U1003+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1003+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1003+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TEST_SP)		; DP
	DS    UT_U1003+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (200)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; Rychlost navratu
UT_U1004:
	DS    UT_U1004+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1004+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1004+OU_X-$
	DB    3,3,4,1
	DS    UT_U1004+OU_HLP-$
	%W    (0)
	DS    UT_U1004+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1004+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1004+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (RET_SP)		; DP
	DS    UT_U1004+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (200)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Parametry do EEPROM a komunikace

%IF(%WITH_IIC)THEN(
; Ulozeni do EEPROM
UT_U1031:
	DS    UT_U1031+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U1031+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1031+OU_X-$
	DB    0,4,12,1
	DS    UT_U1031+OU_HLP-$
	%W    (0)
	DS    UT_U1031+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U1031+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U1031+OU_B_P-$
	%W    (EEC_PAR)		; R23
	DS    UT_U1031+OU_B_F-$
	%W    (EEP_WRS)		; Zapis do EEPROM
	DS    UT_U1031+OU_B_T-$
	DB    'Save setting',0

; Cteni z EEPROM
UT_U1032:
	DS    UT_U1032+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U1032+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1032+OU_X-$
	DB    0,5,15,1
	DS    UT_U1032+OU_HLP-$
	%W    (0)
	DS    UT_U1032+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U1032+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U1032+OU_B_P-$
	%W    (EEC_PAR)		; R23
	DS    UT_U1032+OU_B_F-$
	%W    (EEP_RDS)		; Cteni z EEPROM
	DS    UT_U1032+OU_B_T-$
	DB    'Restore setting',0
)FI

; Komunikace
UT_U1040:
	DS    UT_U1040+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U1040+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1040+OU_X-$
	DB    0,6,12,1
	DS    UT_U1040+OU_HLP-$
	%W    (0)
	DS    UT_U1040+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U1040+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U1040+OU_B_P-$
	%W    (UT_GR72)
	DS    UT_U1040+OU_B_F-$
	%W    (GR_RQ23)
	DS    UT_U1040+OU_B_T-$
	DB    'Comunication',0

%IF (%WITH_F_SEL) THEN (
UT_U1050:
	DS    UT_U1050+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1050+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1050+OU_X-$
	DB    8,7,4,1
	DS    UT_U1050+OU_HLP-$
	%W    (0)
	DS    UT_U1050+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1050+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (TI_FSLR)		; A_WR
	DS    UT_U1050+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TI_SFRQ)		; DP
	DS    UT_U1050+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (20)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru
)FI

; ---------------------------------
; Vystup PWM na motory

UT_U1081:
	DS    UT_U1081+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1081+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1081+OU_X-$
	DB    3,8,4,1
	DS    UT_U1081+OU_HLP-$
	%W    (0)
	DS    UT_U1081+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1081+OU_A_RD-$
	%W    (NULL_A)		; A_RD
	%W    (UW_Mb)		; A_WR
	DS    UT_U1081+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (APWM0)		; DP
	DS    UT_U1081+OU_I_F-$
	DB    80H		; format I_F
	%W    (-128)		; I_L
	%W    (127)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1082:
	DS    UT_U1082+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1082+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1082+OU_X-$
	DB    11,8,4,1
	DS    UT_U1082+OU_HLP-$
	%W    (0)
	DS    UT_U1082+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1082+OU_A_RD-$
	%W    (NULL_A)		; A_RD
	%W    (UW_Mb)		; A_WR
	DS    UT_U1082+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (APWM1)		; DP
	DS    UT_U1082+OU_I_F-$
	DB    80H		; format I_F
	%W    (-128)		; I_L
	%W    (127)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1083:
	DS    UT_U1083+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1083+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1083+OU_X-$
	DB    3,9,4,1
	DS    UT_U1083+OU_HLP-$
	%W    (0)
	DS    UT_U1083+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1083+OU_A_RD-$
	%W    (NULL_A)		; A_RD
	%W    (UW_Mb)		; A_WR
	DS    UT_U1083+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (APWM2)		; DP
	DS    UT_U1083+OU_I_F-$
	DB    80H		; format I_F
	%W    (-128)		; I_L
	%W    (127)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Nulovani polohy

UT_GR15:DS    UT_GR15+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR15+OGR_BTXT-$
	%W    (UT_GT15)
	DS    UT_GR15+OGR_STXT-$
	%W    (0)
	DS    UT_GR15+OGR_HLP-$
	%W    (0)
	DS    UT_GR15+OGR_SFT-$
	%W    (UT_SF15)
	DS    UT_GR15+OGR_PU-$
	%W    (UT_U1501)
	%W    (UT_U1502)
	%W    (0)

UT_GT15:DB    'Set zero pos. ?',0

UT_SF15:DB    K_0
	%W    (UT_GR10)
	%W    (GR_RQ23)

	DB    K_1
	%W    (UT_GR10)
	%W    (CS_GR23_ZERO)

	DB    -1
	%W    (UT_SF1)

UT_U1501:
	DS    UT_U1501+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U1501+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1501+OU_X-$
	DB    3,1,2,1
	DS    UT_U1501+OU_HLP-$
	%W    (0)
	DS    UT_U1501+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U1501+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U1501+OU_B_P-$
	%W    (UT_GR10)
	DS    UT_U1501+OU_B_F-$
	%W    (GR_RQ23)
	DS    UT_U1501+OU_B_T-$
	DB    'No',0

UT_U1502:
	DS    UT_U1502+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U1502+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1502+OU_X-$
	DB    9,1,3,1
	DS    UT_U1502+OU_HLP-$
	%W    (0)
	DS    UT_U1502+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U1502+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U1502+OU_B_P-$
	%W    (UT_GR10)
	DS    UT_U1502+OU_B_F-$
	%W    (CS_GR23_ZERO)
	DS    UT_U1502+OU_B_T-$
	DB    'Yes',0

; *******************************************************************
; Zadavani konstant
UT_GR20:DS    UT_GR20+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR20+OGR_BTXT-$
	%W    (UT_GT20)
	DS    UT_GR20+OGR_STXT-$
	%W    (0)
	DS    UT_GR20+OGR_HLP-$
	%W    (0)
	DS    UT_GR20+OGR_SFT-$
	%W    (UT_SF20)
	DS    UT_GR20+OGR_PU-$
	%W    (UT_U2000)
	%W    (UT_U2001)
	%W    (UT_U2002)
	%W    (UT_U2003)
	%W    (UT_U2004)
	%W    (UT_U2005)
	%W    (UT_U2006)
	%W    (UT_U2007)
	%W    (UT_U2008)
	%W    (UT_U2009)
	%W    (UT_U2010)
	%W    (UT_U2011)
	%W    (UT_U2012)
	%W    (UT_U2013)
	%W    (UT_U2014)
	%W    (UT_U2015)
	%W    (UT_U2016)
    %IF (%MR_REG_SEL) THEN (
	%W    (UT_U2017)
    )FI
    %IF(%WITH_IIC)THEN(
	%W    (UT_U2031)
	%W    (UT_U2032)
	%W    (UT_U2033)
    )FI
	%W    (0)

UT_GT20:DB    'Motor',C_NL
	DB    'POS',C_NL
	DB    'MS',C_NL
	DB    'MA',C_NL
	DB    'ME',C_NL
	DB    'P',C_NL
	DB    'I',C_NL
	DB    'D',C_NL
	DB    'S1',C_NL
	DB    'S2',C_NL
	DB    'FLG',C_NL
	DB    'CFG',C_NL
	DB    'S MUL',C_NL
	DB    'S DIV',C_NL
	DB    'JOFFS',C_NL
	DB    'JRES',C_NL
	DB    'JHYS',C_NL
    %IF (%MR_REG_SEL) THEN (
	DB    'REG TYPE',C_NL
    )FI
	DB    0

UT_SF20:DB    -1
	%W    (UT_SF1)

UT_U2000:
	DS    UT_U2000+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U2000+OU_MSK-$
	DB    0 ; UFM_FOC
	DS    UT_U2000+OU_X-$
	DB    8,0,2,1
	DS    UT_U2000+OU_HLP-$
	%W    (0)
	DS    UT_U2000+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2000+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U2000+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (MR_UIAM)		; DP
	DS    UT_U2000+OU_M_S-$
	%W    (0)
	DS    UT_U2000+OU_M_P-$
	%W    (0)
	DS    UT_U2000+OU_M_F-$
	%W    (0)
	DS    UT_U2000+OU_M_T-$
	%W    (0FFH)
	%W    (000H)
	%W    (UT_U2000TA)
	%W    (0FFH)
	%W    (001H)
	%W    (UT_U2000TB)
	%W    (0FFH)
	%W    (002H)
	%W    (UT_U2000TC)
	%W    (0FFH)
	%W    (003H)
	%W    (UT_U2000TD)
	%W    (0FFH)
	%W    (004H)
	%W    (UT_U2000TE)
	%W    (0FFH)
	%W    (005H)
	%W    (UT_U2000TF)
	%W    (0)
UT_U2000TA:DB 'A',0
UT_U2000TB:DB 'B',0
UT_U2000TC:DB 'C',0
UT_U2000TD:DB 'D',0
UT_U2000TE:DB 'E',0
UT_U2000TF:DB 'F',0

UT_U2001:
	DS    UT_U2001+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2001+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2001+OU_X-$
	DB    3,1,8,1
	DS    UT_U2001+OU_HLP-$
	%W    (0)
	DS    UT_U2001+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2001+OU_A_RD-$
	%W    (UR_MRPl)		; A_RD
	%W    (UW_MRPl)		; A_WR
	DS    UT_U2001+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,0		; DP
	DS    UT_U2001+OU_I_F-$
	DB    0C3H		; format I_F
	%W    (8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2002:
	DS    UT_U2002+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2002+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2002+OU_X-$
	DB    5,2,6,1
	DS    UT_U2002+OU_HLP-$
	%W    (0)
	DS    UT_U2002+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2002+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2002+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_MS		; DP
	DS    UT_U2002+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2003:
	DS    UT_U2003+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2003+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2003+OU_X-$
	DB    5,3,6,1
	DS    UT_U2003+OU_HLP-$
	%W    (0)
	DS    UT_U2003+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2003+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2003+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_ACC	; DP
	DS    UT_U2003+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2004:
	DS    UT_U2004+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2004+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2004+OU_X-$
	DB    5,4,6,1
	DS    UT_U2004+OU_HLP-$
	%W    (0)
	DS    UT_U2004+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2004+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2004+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_ME		; DP
	DS    UT_U2004+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2005:
	DS    UT_U2005+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2005+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2005+OU_X-$
	DB    5,5,6,1
	DS    UT_U2005+OU_HLP-$
	%W    (0)
	DS    UT_U2005+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2005+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2005+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_P		; DP
	DS    UT_U2005+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2006:
	DS    UT_U2006+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2006+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2006+OU_X-$
	DB    5,6,6,1
	DS    UT_U2006+OU_HLP-$
	%W    (0)
	DS    UT_U2006+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2006+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2006+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_I		; DP
	DS    UT_U2006+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2007:
	DS    UT_U2007+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2007+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2007+OU_X-$
	DB    5,7,6,1
	DS    UT_U2007+OU_HLP-$
	%W    (0)
	DS    UT_U2007+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2007+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2007+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_D		; DP
	DS    UT_U2007+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2008:
	DS    UT_U2008+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2008+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2008+OU_X-$
	DB    5,8,6,1
	DS    UT_U2008+OU_HLP-$
	%W    (0)
	DS    UT_U2008+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2008+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2008+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_S1		; DP
	DS    UT_U2008+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2009:
	DS    UT_U2009+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2009+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2009+OU_X-$
	DB    5,9,6,1
	DS    UT_U2009+OU_HLP-$
	%W    (0)
	DS    UT_U2009+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2009+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2009+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_S2		; DP
	DS    UT_U2009+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2010:
	DS    UT_U2010+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2010+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2010+OU_X-$
	DB    5,10,6,1
	DS    UT_U2010+OU_HLP-$
	%W    (0)
	DS    UT_U2010+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2010+OU_A_RD-$
	%W    (UR_MRb)		; A_RD
	%W    (UW_MRb)		; A_WR
	DS    UT_U2010+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_FLG	; DP
	DS    UT_U2010+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2011:
	DS    UT_U2011+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2011+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2011+OU_X-$
	DB    5,11,6,1
	DS    UT_U2011+OU_HLP-$
	%W    (0)
	DS    UT_U2011+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2011+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2011+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_CFG	; DP
	DS    UT_U2011+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (0FFFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2012:
	DS    UT_U2012+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2012+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2012+OU_X-$
	DB    5,12,6,1
	DS    UT_U2012+OU_HLP-$
	%W    (0)
	DS    UT_U2012+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2012+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2012+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_SCM	; DP
	DS    UT_U2012+OU_I_F-$
	DB    80H		; format I_F
	%W    (-0FFH)		; I_L
	%W    ( 0FFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2013:
	DS    UT_U2013+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2013+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2013+OU_X-$
	DB    5,13,6,1
	DS    UT_U2013+OU_HLP-$
	%W    (0)
	DS    UT_U2013+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2013+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2013+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_SCD	; DP
	DS    UT_U2013+OU_I_F-$
	DB    80H		; format I_F
	%W    (-0FFH)		; I_L
	%W    ( 0FFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2014:
	DS    UT_U2014+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2014+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2014+OU_X-$
	DB    5,14,6,1
	DS    UT_U2014+OU_HLP-$
	%W    (0)
	DS    UT_U2014+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2014+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2014+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_JCO	; DP
	DS    UT_U2014+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (0FFFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2015:
	DS    UT_U2015+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2015+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2015+OU_X-$
	DB    5,15,6,1
	DS    UT_U2015+OU_HLP-$
	%W    (0)
	DS    UT_U2015+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2015+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2015+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_JCR	; DP
	DS    UT_U2015+OU_I_F-$
	DB    80H		; format I_F
	%W    (-7FFFH)		; I_L
	%W    ( 7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2016:
	DS    UT_U2016+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2016+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2016+OU_X-$
	DB    5,16,6,1
	DS    UT_U2016+OU_HLP-$
	%W    (0)
	DS    UT_U2016+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2016+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2016+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_JCH	; DP
	DS    UT_U2016+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (0FFFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

%IF (%MR_REG_SEL) THEN (
UT_U2017:
	DS    UT_U2017+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2017+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2017+OU_X-$
	DB    10,17,1,1
	DS    UT_U2017+OU_HLP-$
	%W    (0)
	DS    UT_U2017+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2017+OU_A_RD-$
	%W    (NULL_A)		; A_RD
	%W    (UW_REGTYPE)	; A_WR
	DS    UT_U2017+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_JCH	; DP
	DS    UT_U2017+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (0FFFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru
)FI

; ---------------------------------
; Konfigurace motoru do EEPROM

%IF(%WITH_IIC)THEN(
; Ulozeni do EEPROM
UT_U2031:
	DS    UT_U2031+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U2031+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2031+OU_X-$
	DB    0,18,12,1
	DS    UT_U2031+OU_HLP-$
	%W    (0)
	DS    UT_U2031+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U2031+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U2031+OU_B_P-$
	%W    (0)		; R23
	DS    UT_U2031+OU_B_F-$
	%W    (MR_EEWR)		; Zapis do EEPROM
	DS    UT_U2031+OU_B_T-$
	DB    'Save setting',0

; Cteni z EEPROM
UT_U2032:
	DS    UT_U2032+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U2032+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2032+OU_X-$
	DB    0,19,15,1
	DS    UT_U2032+OU_HLP-$
	%W    (0)
	DS    UT_U2032+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U2032+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U2032+OU_B_P-$
	%W    (0)		; R23
	DS    UT_U2032+OU_B_F-$
	%W    (MR_EERD)		; Cteni z EEPROM
	DS    UT_U2032+OU_B_T-$
	DB    'Restore setting',0

; Prednastavene konstanty
UT_U2033:
	DS    UT_U2033+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U2033+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2033+OU_X-$
	DB    0,20,15,1
	DS    UT_U2033+OU_HLP-$
	%W    (0)
	DS    UT_U2033+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U2033+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U2033+OU_B_P-$
	%W    (0)		; R23
	DS    UT_U2033+OU_B_F-$
	%W    (MR_INIS)		; Defaultni konstanty
	DS    UT_U2033+OU_B_T-$
	DB    'Factory presset',0
)FI

; *******************************************************************
; Lockovany display
UT_GR30:DS    UT_GR30+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR30+OGR_BTXT-$
	%W    (UT_GT30)
	DS    UT_GR30+OGR_STXT-$
	%W    (0)
	DS    UT_GR30+OGR_HLP-$
	%W    (0)
	DS    UT_GR30+OGR_SFT-$
	%W    (UT_SF30)
	DS    UT_GR30+OGR_PU-$
	%W    (UT_U3001)
	%W    (UT_U3002)
	%W    (0)

UT_GT30:DB    'AP          KEY',C_NL
	DB    'TL          LOCK',C_NL
	DB    0

UT_SF30:DB    K_STOP
	%W    (0)
	%W    (STP_ALL)

	DB    K_END
	%W    (0)
	%W    (CLR_ALL)

	DB    0

UT_U3001:
	DS    UT_U3001+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U3001+OU_MSK-$
	DB    0
	DS    UT_U3001+OU_X-$
	DB    3,0,8,1
	DS    UT_U3001+OU_HLP-$
	%W    (0)
	DS    UT_U3001+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U3001+OU_A_RD-$
	%W    (UR_MRPl)		; A_RD
	%W    (UW_MRPl)		; A_WR
	DS    UT_U3001+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    0,0		; DP
	DS    UT_U3001+OU_I_F-$
	DB    0C3H		; format I_F
	%W    (8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; Delka testu
UT_U3002:
	DS    UT_U3002+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U3002+OU_MSK-$
	DB    0
	DS    UT_U3002+OU_X-$
	DB    3,1,4,1
	DS    UT_U3002+OU_HLP-$
	%W    (0)
	DS    UT_U3002+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U3002+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U3002+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TEST_LN)		; DP
	DS    UT_U3002+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (250)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; *******************************************************************
; Menu

UT_GR60:DS    UT_GR60+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR60+OGR_BTXT-$
	%W    (UT_GT60)
	DS    UT_GR60+OGR_STXT-$
	%W    (0)		; UT_GS60
	DS    UT_GR60+OGR_HLP-$
	%W    (0)
	DS    UT_GR60+OGR_SFT-$
	%W    (UT_SF60)
	DS    UT_GR60+OGR_PU-$
	%W    (UT_U6001)
	%W    (0)

UT_GT60:DB    ' Main menu',0

UT_SF60:DB    -1
	%W    (UT_SF1)

; About
UT_U6001:
	DS    UT_U6001+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U6001+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U6001+OU_X-$
	DB    0,1,5,1
	DS    UT_U6001+OU_HLP-$
	%W    (0)
	DS    UT_U6001+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U6001+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U6001+OU_B_P-$
	%W    (UT_GR61)
	DS    UT_U6001+OU_B_F-$
	%W    (GR_RQ23)
	DS    UT_U6001+OU_B_T-$
	DB    'About',0

; *******************************************************************
; About

UT_GR61:DS    UT_GR61+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR61+OGR_BTXT-$
	%W    (UT_GT61)
	DS    UT_GR61+OGR_STXT-$
	%W    (0)		; UT_GS60
	DS    UT_GR61+OGR_HLP-$
	%W    (0)
	DS    UT_GR61+OGR_SFT-$
	%W    (UT_SF61)
	DS    UT_GR61+OGR_PU-$
	%W    (0)

UT_GT61:DB    '** Mechanic **',C_NL
	DB    'Strojni dilna',C_NL
	DB    'Tuma a Synove',C_NL
	DB    'Tachlovice 171',C_NL
	DB    '252 17 Praha Z',C_NL
	DB    'Tel/Fax :',C_NL
	DB    '+420 311670972',C_NL
	DB    '** Electronic **',C_NL
	DB    'PiKRON s.r.o',C_NL
	DB    'Kankovskeho 1235',C_NL
	DB    '182 00 Praha 8',C_NL
	DB    'Tel/Fax :',C_NL
	DB    '  +420 2 6884676',C_NL
	DB    'Tel :    4097671',C_NL
	DB    0

UT_SF61:
	DB    K_DOWN
	DB    -1,14
	%W    (DISP_SCROL)

	DB    K_UP
	DB    1,14
	%W    (DISP_SCROL)

	DB    -1
	%W    (UT_SF1)

; *******************************************************************
; Nastaveni parametru komunikace
UT_GR72:DS    UT_GR72+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR72+OGR_BTXT-$
	%W    (UT_GT72)
	DS    UT_GR72+OGR_STXT-$
	%W    (0)		; UT_GS72
	DS    UT_GR72+OGR_HLP-$
	%W    (0)
	DS    UT_GR72+OGR_SFT-$
	%W    (UT_SF72)
	DS    UT_GR72+OGR_PU-$
	%W    (UT_U7201)
	%W    (UT_U7202)
	%W    (UT_U7203)
	%W    (0)

UT_GT72:DB    'Type  Adr  Spd',0

UT_SF72:DB    -1
	%W    (UT_SF1)

UT_U7201:
	DS    UT_U7201+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U7201+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7201+OU_X-$
	DB    0,1,5,1
	DS    UT_U7201+OU_HLP-$
	%W    (0)
	DS    UT_U7201+OU_SFT-$
	%W    (UT_SF7201)
	DS    UT_U7201+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_COMi)		; A_WR
	DS    UT_U7201+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (COM_TYP)		; DP
	DS    UT_U7201+OU_M_S-$
	%W    (0)
	DS    UT_U7201+OU_M_P-$
	%W    (0)
	DS    UT_U7201+OU_M_F-$
	%W    (COM_TYPCH)
	DS    UT_U7201+OU_M_T-$
	%W    (0FFFFH)
	%W    (00000H)
	%W    (UT_U7201T0)
	%W    (0FFFFH)
	%W    (00001H)
	%W    (UT_U7201T1)
	%W    (0FFFFH)
	%W    (00002H)
	%W    (UT_U7201T2)
	%W    (0)
UT_U7201T0:DB    'None',0
UT_U7201T1:DB    'uLAN',0
UT_U7201T2:DB    'RS232',0

UT_SF7201:
	DB    K_2
	%W    (2)
	%W    (COM_WR23)

	DB    K_1
	%W    (1)
	%W    (COM_WR23)

	DB    K_0
	%W    (0)
	%W    (COM_WR23)

	DB    0

UT_U7202:
	DS    UT_U7202+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U7202+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7202+OU_X-$
	DB    6,1,2,1
	DS    UT_U7202+OU_HLP-$
	%W    (0)
	DS    UT_U7202+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U7202+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_COMi)	; A_WR
	DS    UT_U7202+OU_DPSI-$
	%W    (0)		; DPSI
	%W    (COM_ADR)		; DP
	DS    UT_U7202+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (99)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U7203:
	DS    UT_U7203+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U7203+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7203+OU_X-$
	DB    10,1,5,1
	DS    UT_U7203+OU_HLP-$
	%W    (0)
	DS    UT_U7203+OU_SFT-$
	%W    (UT_SF7203)
	DS    UT_U7203+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_COMi)		; A_WR
	DS    UT_U7203+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (COM_SPD)		; DP
	DS    UT_U7203+OU_M_S-$
	%W    (0)
	DS    UT_U7203+OU_M_P-$
	%W    (0)
	DS    UT_U7203+OU_M_F-$
	%W    (COM_SPDCH)
	DS    UT_U7203+OU_M_T-$
	%W    (0FFFFH)
	%W    (00000H)
	%W    (UT_U7203T0)
	%W    (0FFFFH)
	%W    (00001H)
	%W    (UT_U7203T1)
	%W    (0FFFFH)
	%W    (00002H)
	%W    (UT_U7203T2)
	%W    (0FFFFH)
	%W    (00003H)
	%W    (UT_U7203T3)
	%W    (0FFFFH)
	%W    (00004H)
	%W    (UT_U7203T4)
	%W    (0FFFFH)
	%W    (00005H)
	%W    (UT_U7203T5)
	%W    (0)
UT_U7203T0:DB    ' 1200',0
UT_U7203T1:DB    ' 2400',0
UT_U7203T2:DB    ' 4800',0
UT_U7203T3:DB    ' 9600',0
UT_U7203T4:DB    '19200',0
UT_U7203T5:DB    '28800',0

UT_SF7203:
	DB    K_9
	%W    (3)
	%W    (COM_WR23)

	DB    K_4
	%W    (2)
	%W    (COM_WR23)

	DB    K_2
	%W    (1)
	%W    (COM_WR23)

	DB    K_1
	%W    (4)
	%W    (COM_WR23)

	DB    K_0
	%W    (0)
	%W    (COM_WR23)

	DB    0

