;********************************************************************
;*                     CONFIG.ASM                                   *
;*     Konfiguracni soubor pro kompilaci MARS                       *
;*                  Stav ke dni 07.05.2003                          *
;*                      (C) Pisoft 1996                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

%DEFINE (VERSION) (MARS v2.2a)
%DEFINE (HW_VERSION) (PB)
%DEFINE (USE_WR_MASK) (1)
%DEFINE (REG_STRUCT_TYPE) (MR_PID)
%DEFINE (PBLIBDIR) (..\PBLIB\)

%IF (%EQS(%HW_VERSION,PB)) THEN (
; Nova verze
%DEFINE (INCH_NULL)(%PBLIBDIR%()PB_NULL.H)
%DEFINE (INCH_TTY) (%PBLIBDIR%()PB_TTY.H)
%DEFINE (INCH_LCD) (%PBLIBDIR%()PB_LCD.H)
%DEFINE (INCH_AI)  (%PBLIBDIR%()PB_AI.H)
%DEFINE (INCH_AL)  (%PBLIBDIR%()PB_AL.H)
%DEFINE (INCH_AF)  (%PBLIBDIR%()PB_AF.H)
%DEFINE (INCH_ADR) (%PBLIBDIR%()PB_ADR.H)
%DEFINE (INCH_ULAN)(%PBLIBDIR%()ULAN.H)
%DEFINE (INCH_UL_OI)(%PBLIBDIR%()UL_OI.H)
%DEFINE (INCH_MMAC)(%PBLIBDIR%()PB_MMAC.H)
%DEFINE (INCH_MR_DEFS)(%PBLIBDIR%()MR_DEFS.H)
%DEFINE (INCH_MR_PDEFS)(%PBLIBDIR%()MR_PDEFS.H)
%DEFINE (INCH_MR_PSYS)(%PBLIBDIR%()MR_PSYS.H)
%DEFINE (INCH_UI_DEFS)(%PBLIBDIR%()UI_DEFS.H)
%DEFINE (INCH_UI)  (%PBLIBDIR%()PB_UI.H)
%DEFINE (INCH_RS232)(%PBLIBDIR%()PB_RS232.H)
%DEFINE (INCH_RSOI)(%PBLIBDIR%()PB_RSOI.H)
%DEFINE (INCH_BREAK)(%PBLIBDIR%()BREAK.H)
%DEFINE (INCH_IIC) (%PBLIBDIR%()PB_IIC.H)
%DEFINE (PROCESSOR_TYPE) (552)
%DEFINE (VECTOR_TYPE) (DYNAMIC)
%DEFINE (REG_OUT_TYPE) (PWM_MR)
%DEFINE (WATCHDOG) (
	ORL   PCON,#10H
	MOV   T3,#080H
)
)FI

; Processor depended features

%IF (%EQS(%PROCESSOR_TYPE,552)) THEN (
%DEFINE (INCH_REGS) (%PBLIBDIR%()REG552.H)
)FI

%IF (%EQS(%PROCESSOR_TYPE,31)) THEN (
%DEFINE (INCH_REGS) (%INCH_NULL)
)FI

; Zpusob definice vektoru preruseni a sluzeb

%IF (%EQS(%VECTOR_TYPE,STATIC)) THEN (
%*DEFINE (VECTOR  (VECNUM,VECADR)) ()
%*DEFINE (SVECTOR (VECNUM,VECADR)) (
CSEG    AT    %VECNUM
	JMP   %VECADR
)
)FI

%IF (%EQS(%VECTOR_TYPE,DYNAMIC)) THEN (
	EXTRN CODE(VEC_SET)
%*DEFINE (SVECTOR (VECNUM,VECADR)) ()
%*DEFINE (VECTOR  (VECNUM,VECADR)) (
	MOV   R4,#%VECNUM
	MOV   DPTR,#%VECADR
	CALL  VEC_SET
)
)FI

