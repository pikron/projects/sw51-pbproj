#   Project file pro viceosou regulaci
#         (C) Pisoft 1996

mars.obj  : mars.asm mars_lim.asm mars_std.asm config.h
	a51 mars.asm $(par) debug

mr_psys.obj : mr_psys.asm config.h
	a51   mr_psys.asm $(par)

mr_pshw.obj : mr_pshw.asm config.h
	a51   mr_pshw.asm $(par)

pb_uihw.obj : pb_uihw.asm config.h
	a51   pb_uihw.asm $(par) debug

	  : mars.obj
	del mars.

mars.     : mars.obj mr_psys.obj mr_pshw.obj pb_uihw.obj ..\pblib\pb.lib
	l51 mars.obj,mr_psys.obj,mr_pshw.obj,pb_uihw.obj,..\pblib\pb.lib xdata(8000H) ramsize(100h) ixref

mars.hex  : mars.
	ohs51 mars
	del mars.
#       eprem mars.hex -s80 -1

