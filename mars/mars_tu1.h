; *******************************************************************
; Nastaveni pozadovanych vlastnosti

%DEFINE	(CUST_APPL_CODE) (MARS_TU1.ASM)	; Specificky kod pro aplikaci

%DEFINE (WITH_ULAN)	(1*1)	; s komunikaci uLan
%DEFINE (WITH_IIC)	(1)	; s komunikaci IIC
%DEFINE (WITH_IICKB)	(1)	; se vzalenou IIC klavesnici
%DEFINE (WITH_IICCM)	(1*0)	; s prijmem prikazu pres IIC
%DEFINE (WITH_IO_CM)	(1*1)	; s prikazy pro IO
%DEFINE (WITH_F_SEL)	(0)	; s promennou vzorkovaci frekvenci
%DEFINE (WITH_MRDY)	(1*1)	; s ohlasem Rm! pro jednotlive osy
%DEFINE (WITH_TRIG)	(1*1)	; s triggry
%DEFINE (WITH_AM_CM)	(1)	; s prikazy pro prime PWM a rychlost
%DEFINE (WITH_CUSTHOOK)	(1)	; s kodem pro aplikaci

%DEFINE (MR_REG_TYPE) (MR_PIDNLP); Prednastaveny typ regulatoru
%DEFINE (MR_REG_SEL)    (1)	; Moznost vyberu regulatoru
%DEFINE (REG_COUNT)	(3)	; Pocet pouzitych regulatoru
%DEFINE (TIME_INT_CM)   (1)	; Ktery z komparatoru pro preruseni
				; CMx & ECMx -> int T2CMPx, CMIx

;DINT600 EQU   1536 ; Deleni CLK/12 na 600 Hz pro X 11.0592 MHz
;DINT600 EQU   2560 ; Deleni CLK/12 na 600 Hz pro X 18.432 MHz
;DINT600 EQU   1280 ; Deleni CLK/12 na 1200 Hz pro X 18.432 MHz
;DINT600 EQU   4583 ; Deleni CLK/12 na 600 Hz pro X 33.000 MHz
DINT600 EQU   2750 ; Deleni CLK/12 na 1000 Hz pro X 33.000 MHz
;DINT600 EQU   3333 ; Deleni CLK/12 na 600 Hz pro X 24.000 MHz
;DINT25  EQU   24   ; Delitel EXINT1 na 25 Hz
DINT25  EQU   40   ; Delitel EXINT1 na 25 Hz

;BAUDDIV_9600	EQU	06	; pro 11.0592 MHz
;BAUDDIV_9600	EQU	10	; pro 18.432 MHz
BAUDDIV_9600	EQU	18	; pro 33.000 MHz
;BAUDDIV_9600	EQU	13	; pro 24.000 MHz

; stoupani sroubu 8mm/otacku
; 100*2000 IRC na otacku sroubu
;  => 25000 IRC na 1 mm
; pozadovana rychlost 50 a 200 mm / min
;  1 mm/min odpovida  106.666 IRC/256/ms
; 50 mm/min odpovida 5333.333 IRC/256/ms

C_SP2INC	EQU	27307
C_SP2INC_SR	EQU	8