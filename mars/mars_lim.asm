; *******************************************************************
; Preddefinovane hodnoty parametru

STDR_A:	DB    0, 0,0,0, 0,0
	DB    0
	DB    0,0   ; 	(CASH_ZP)	; RP
	DB    0
	DB    0,0,0			; RS
	DB    11,  0,  8,  0, 20,  0	; P I D
	DB    001H,0, 001H,0, 0,060H	; 1 2 ME
	%W    (8000)			; MS
	%W    (010H)			; MA
	DB    0,0,0,0,0,0,0
	DB    LOW (AIR_CF0+6),LOW APWM0
	%W    (26+256+1024)		; CFG
	DB    2				; JMP
	DW    %MR_REG_TYPE		; vektor na REGULATOR
	DB    2				; JMP
	DW    VR_REG			; skok na GENERATOR
	DS    STDR_A+OREG_A+OMR_SCM-$
	%W    (3)			; Meritko, nasobeni
	%W    (20)			; Meritko, deleni
	DS    STDR_A+OREG_A+OMR_JCA-$
	%W    (ADC7)			; OMR_JCA
	%W    (8000H)			; OMR_JCO
	%W    (-625)			; OMR_JCR
	%W    (200H)			; OMR_JCH

	DS    STDR_A+OREG_B-$
	DB    0, 0,0,0, 0,0, 0,0,0,0, 0,0,0
	DB    10,  0,  7,  0, 20,  0	; P I D
	DB    001H,0, 001H,0, 0,060H	; 1 2 ME
	%W    (8000)			; MS
	%W    (010H)			; MA
	DB    0,0,0,0,0,0,0
	DB    LOW (AIR_CF0+4),LOW APWM1
	%W    (26+256+1024)		; CFG
	DB    2				; JMP
	DW    %MR_REG_TYPE		; vektor na REGULATOR
	DB    2				; JMP
	DW    VR_REG			; skok na GENERATOR
	DS    STDR_A+OREG_B+OMR_SCM-$
	%W    (3)			; Meritko, nasobeni
	%W    (20)			; Meritko, deleni
	DS    STDR_A+OREG_B+OMR_JCA-$
	%W    (ADC6)			; OMR_JCA
	%W    (8000H)			; OMR_JCO
	%W    (-625)			; OMR_JCR
	%W    (200H)			; OMR_JCH

	DS    STDR_A+OREG_C-$
	DB    0, 0,0,0, 0,0, 0,0,0,0, 0,0,0
	DB    8,  0,  5,  0, 20,  0	; P I D
	DB    001H,0, 001H,0, 0,060H	; 1 2 ME
	%W    (800)			; MS
	%W    (010H)			; MA
	DB    0,0,0,0,0,0,0
	DB    LOW (AIR_CF0+2),LOW APWM2
	%W    (0)			; CFG
	DB    2				; JMP
	DW    %MR_REG_TYPE ;MR_RPULSEP	; vektor na REGULATOR
	DB    2				; JMP
	DW    VR_REG			; skok na GENERATOR
	DS    STDR_A+OREG_C+OMR_SCM-$
	%W    (0)			; Meritko, nasobeni
	%W    (0)			; Meritko, deleni
	DS    STDR_A+OREG_C+OMR_JCA-$
	%W    (ADC5)			; OMR_JCA
	%W    (8000H)			; OMR_JCO
	%W    (-30)			; OMR_JCR
	%W    (200H)			; OMR_JCH
STDR_AE:

; *******************************************************************
; Definice user interface

RSEG	MARS__C

LIM_STOP:
	JNB   MR_FLG.BMR_BSY,LIM_STOP1
	JMP   STP_ALL
LIM_STOP1:
	CALL  REL_ALL
	JMP   CER_ALL

LIM_RESET:
	CALL  CER_ALL
	JMP   GO_HHT

; ---------------------------------
; Definice klaves

UT_SF1:	DB    K_H_A
	DB    0,0
	%W    (UT_SAMG)

%IF(%REG_COUNT GE 2) THEN(
	DB    K_H_B
	DB    1,0
	%W    (UT_SAMG)

%IF(%REG_COUNT GE 3) THEN(
	DB    K_H_C
	DB    2,0
	%W    (UT_SAMG)

%IF(%REG_COUNT GE 4) THEN(
	DB    K_H_D
	DB    3,0
	%W    (UT_SAMG)

%IF(%REG_COUNT GE 5) THEN(
	DB    K_H_E
	DB    4,0
	%W    (UT_SAMG)

%IF(%REG_COUNT GE 6) THEN(
	DB    K_H_F
	DB    5,0
	%W    (UT_SAMG)
)FI )FI )FI )FI )FI

	DB    K_LIST
	%W    (UT_GR10)
	%W    (GR_RQ23)

	DB    K_RUN
	%W    (0)
	%W    (GO_JCTT)

	DB    K_PURGE
	%W    (0)
	%W    (CER_ALL)

	DB    K_STOP
	%W    (0)
	%W    (STP_ALL)

	DB    K_END
	%W    (0)
	%W    (CLR_ALL)

	DB    K_HOLD
	%W    (0)
	%W    (GO_HHT)

	DB    K_PROG
	%W    (0)
	%W    (IHEXLD)

UT_SFTN:

%IF(%WITH_IICKB)THEN(
	DB    68		; Reset
	%W    (0)
	%W    (LIM_RESET)

	DB    69		; Joy
	%W    (0)
	%W    (GO_JCTT)

	DB    71		; Stop
	%W    (0)
	%W    (LIM_STOP)
)FI

	DB    K_RIGHT
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    K_LEFT
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    K_DOWN
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    K_UP
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

UT_SF0:	DB    0

; *******************************************************************
; Zakladni display

UT_GR10:DS    UT_GR10+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR10+OGR_BTXT-$
	%W    (UT_GT10)
	DS    UT_GR10+OGR_STXT-$
	%W    (0)
	DS    UT_GR10+OGR_HLP-$
	%W    (0)
	DS    UT_GR10+OGR_SFT-$
	%W    (UT_SF10)
	DS    UT_GR10+OGR_PU-$
	%W    (UT_U1001)
%IF(%REG_COUNT GE 2) THEN(
	%W    (UT_U1002)
%IF(%REG_COUNT GE 3) THEN(
	%W    (UT_U1003)
)FI )FI
	%W    (UT_U1010)
	%W    (UT_U1021)
%IF(%REG_COUNT GE 2) THEN(
	%W    (UT_U1022)
)FI
%IF(%WITH_IIC)THEN(
	%W    (UT_U1031)
	%W    (UT_U1032)
	%W    (UT_U1033)
)FI
	%W    (UT_U1040)
%IF (%WITH_F_SEL) THEN (
	%W    (UT_U1050)
)FI
	%W    (0)

UT_GT10:DB    'X',C_NL
	DB    'Y         Z',C_NL
	DB    C_NL
	DB    C_NL
	DB    C_NL
	DB    C_NL
	DB    C_NL
	DB    C_NL
	DB    C_NL
	DB    'Reg frq',C_NL
	DB    0

UT_SF10:
	DB    -1
	%W    (UT_SF1)

; ---------------------------------
; poloha X

UT_U1001:
	DS    UT_U1001+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1001+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1001+OU_X-$
	DB    1,0,7,1
	DS    UT_U1001+OU_HLP-$
	%W    (0)
	DS    UT_U1001+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1001+OU_A_RD-$
	%W    (UR_MRPl)		; A_RD
	%W    (UW_MRPl)		; A_WR
	DS    UT_U1001+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    0,0		; DP
	DS    UT_U1001+OU_I_F-$
	DB    0C2H		; format I_F
	%W    (8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; poloha Y

UT_U1002:
	DS    UT_U1002+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1002+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1002+OU_X-$
	DB    1,1,7,1
	DS    UT_U1002+OU_HLP-$
	%W    (0)
	DS    UT_U1002+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1002+OU_A_RD-$
	%W    (UR_MRPl)		; A_RD
	%W    (UW_MRPl)		; A_WR
	DS    UT_U1002+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    1,0		; DP
	DS    UT_U1002+OU_I_F-$
	DB    0C2H		; format I_F
	%W    (8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; poloha Z

UT_U1003:
	DS    UT_U1003+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1003+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1003+OU_X-$
	DB    11,1,5,1
	DS    UT_U1003+OU_HLP-$
	%W    (0)
	DS    UT_U1003+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1003+OU_A_RD-$
	%W    (UR_MRPl)		; A_RD
	%W    (UW_MRPl)		; A_WR
	DS    UT_U1003+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    2,0		; DP
	DS    UT_U1003+OU_I_F-$
	DB    0C0H		; format I_F
	%W    (8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Stav systemu

UT_U1010:
	DS    UT_U1010+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1010+OU_MSK-$
	DB    0
	DS    UT_U1010+OU_X-$
	DB    12,0,4,1
	DS    UT_U1010+OU_HLP-$
	%W    (0)
	DS    UT_U1010+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1010+OU_A_RD-$
	%W    (UR_STALL)	; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1010+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    0,0		; DP
	DS    UT_U1010+OU_M_S-$
	%W    (0)
	DS    UT_U1010+OU_M_P-$
	%W    (0)
	DS    UT_U1010+OU_M_F-$
	%W    (0)
	DS    UT_U1010+OU_M_T-$
	%W    (MMR_ERR)
	%W    (MMR_ERR)
	%W    (UT_U1010TE)
	%W    (0200H OR MMR_ENG)
	%W    (0200H OR MMR_ENG)
	%W    (UT_U1010T2)
	%W    (MMR_BSY)
	%W    (MMR_BSY)
	%W    (UT_U1010T3)
	%W    (0100H)
	%W    (0100H)
	%W    (UT_U1010T1)
	%W    (0100H)
	%W    (0000H)
	%W    (UT_U1010T0)
	%W    (0)
UT_U1010T0:DB    ' Ok',0
UT_U1010T1:DB    ',',0
UT_U1010T2:DB    'Man',0
UT_U1010T3:DB    'Run',0
UT_U1010TE:DB    'ERR',0

; ---------------------------------
; Priznaky X

UT_U1021:
	DS    UT_U1021+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1021+OU_MSK-$
	DB    0
	DS    UT_U1021+OU_X-$
	DB    8,0,1,1
	DS    UT_U1021+OU_HLP-$
	%W    (0)
	DS    UT_U1021+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1021+OU_A_RD-$
	%W    (UR_STEX)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1021+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    0,0		; DP
	DS    UT_U1021+OU_M_S-$
	%W    (0)
	DS    UT_U1021+OU_M_P-$
	%W    (0)
	DS    UT_U1021+OU_M_F-$
	%W    (0)
	DS    UT_U1021+OU_M_T-$
	%W    (MMR_ERR)
	%W    (MMR_ERR)
	%W    (UT_U1020TE)
	%W    (0200H OR MMR_ENG)
	%W    (0200H OR MMR_ENG)
	%W    (UT_U1020T2)
	%W    (MMR_BSY)
	%W    (MMR_BSY)
	%W    (UT_U1020T3)
	%W    (0100H)
	%W    (0100H)
	%W    (UT_U1020T1)
	%W    (0100H)
	%W    (0000H)
	%W    (UT_U1020T0)
	%W    (0)
UT_U1020T0:DB    '',0
UT_U1020T1:DB    ',',0
UT_U1020T2:DB    'j',0
UT_U1020T3:DB    'r',0
UT_U1020TE:DB    'e',0

; ---------------------------------
; Priznaky Y

UT_U1022:
	DS    UT_U1022+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1022+OU_MSK-$
	DB    0
	DS    UT_U1022+OU_X-$
	DB    8,1,1,1
	DS    UT_U1022+OU_HLP-$
	%W    (0)
	DS    UT_U1022+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1022+OU_A_RD-$
	%W    (UR_STEX)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1022+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    1,0		; DP
	DS    UT_U1022+OU_M_S-$
	%W    (0)
	DS    UT_U1022+OU_M_P-$
	%W    (0)
	DS    UT_U1022+OU_M_F-$
	%W    (0)
	DS    UT_U1022+OU_M_T-$
	%W    (MMR_ERR)
	%W    (MMR_ERR)
	%W    (UT_U1020TE)
	%W    (0200H OR MMR_ENG)
	%W    (0200H OR MMR_ENG)
	%W    (UT_U1020T2)
	%W    (MMR_BSY)
	%W    (MMR_BSY)
	%W    (UT_U1020T3)
	%W    (0100H)
	%W    (0100H)
	%W    (UT_U1020T1)
	%W    (0100H)
	%W    (0000H)
	%W    (UT_U1020T0)
	%W    (0)

; ---------------------------------
; Konfigurace do EEPROM a komunikace

%IF(%WITH_IIC)THEN(
; Ulozeni do EEPROM
UT_U1031:
	DS    UT_U1031+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U1031+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1031+OU_X-$
	DB    0,5,12,1
	DS    UT_U1031+OU_HLP-$
	%W    (0)
	DS    UT_U1031+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U1031+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U1031+OU_B_P-$
	%W    (0)		; R23
	DS    UT_U1031+OU_B_F-$
	%W    (MR_EEWR)		; Zapis do EEPROM
	DS    UT_U1031+OU_B_T-$
	DB    'Save setting',0

; Cteni z EEPROM
UT_U1032:
	DS    UT_U1032+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U1032+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1032+OU_X-$
	DB    0,6,15,1
	DS    UT_U1032+OU_HLP-$
	%W    (0)
	DS    UT_U1032+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U1032+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U1032+OU_B_P-$
	%W    (0)		; R23
	DS    UT_U1032+OU_B_F-$
	%W    (MR_EERD)		; Zapis do EEPROM
	DS    UT_U1032+OU_B_T-$
	DB    'Restore setting',0

; Prednastavene konstanty
UT_U1033:
	DS    UT_U1033+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U1033+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1033+OU_X-$
	DB    0,7,15,1
	DS    UT_U1033+OU_HLP-$
	%W    (0)
	DS    UT_U1033+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U1033+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U1033+OU_B_P-$
	%W    (0)		; R23
	DS    UT_U1033+OU_B_F-$
	%W    (MR_INIS)		; Zapis do EEPROM
	DS    UT_U1033+OU_B_T-$
	DB    'Factory presset',0
)FI

; Komunikace
UT_U1040:
	DS    UT_U1040+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U1040+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1040+OU_X-$
	DB    0,8,12,1
	DS    UT_U1040+OU_HLP-$
	%W    (0)
	DS    UT_U1040+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U1040+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U1040+OU_B_P-$
	%W    (UT_GR72)
	DS    UT_U1040+OU_B_F-$
	%W    (GR_RQ23)
	DS    UT_U1040+OU_B_T-$
	DB    'Comunication',0

%IF (%WITH_F_SEL) THEN (
UT_U1050:
	DS    UT_U1050+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1050+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1050+OU_X-$
	DB    8,9,4,1
	DS    UT_U1050+OU_HLP-$
	%W    (0)
	DS    UT_U1050+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1050+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (TI_FSLR)		; A_WR
	DS    UT_U1050+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TI_SFRQ)		; DP
	DS    UT_U1050+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (20)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru
)FI

; *******************************************************************
; Zadavani konstant
UT_GR20:DS    UT_GR20+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR20+OGR_BTXT-$
	%W    (UT_GT20)
	DS    UT_GR20+OGR_STXT-$
	%W    (0)
	DS    UT_GR20+OGR_HLP-$
	%W    (0)
	DS    UT_GR20+OGR_SFT-$
	%W    (UT_SF20)
	DS    UT_GR20+OGR_PU-$
	%W    (UT_U2000)
	%W    (UT_U2001)
	%W    (UT_U2002)
	%W    (UT_U2003)
	%W    (UT_U2004)
	%W    (UT_U2005)
	%W    (UT_U2006)
	%W    (UT_U2007)
	%W    (UT_U2008)
	%W    (UT_U2009)
	%W    (UT_U2010)
	%W    (UT_U2011)
	%W    (UT_U2012)
	%W    (UT_U2013)
	%W    (UT_U2014)
	%W    (UT_U2015)
	%W    (UT_U2016)
    %IF (%MR_REG_SEL) THEN (
	%W    (UT_U2017)
    )FI
	%W    (0)

UT_GT20:DB    'Motor',C_NL
	DB    'POS',C_NL
	DB    'MS',C_NL
	DB    'MA',C_NL
	DB    'ME',C_NL
	DB    'P',C_NL
	DB    'I',C_NL
	DB    'D',C_NL
	DB    'S1',C_NL
	DB    'S2',C_NL
	DB    'FLG',C_NL
	DB    'CFG',C_NL
	DB    'S MUL',C_NL
	DB    'S DIV',C_NL
	DB    'JOFFS',C_NL
	DB    'JRES',C_NL
	DB    'JHYS',C_NL
    %IF (%MR_REG_SEL) THEN (
	DB    'REG TYPE',C_NL
    )FI
	DB    0

UT_SF20:DB    -1
	%W    (UT_SF1)

UT_U2000:
	DS    UT_U2000+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U2000+OU_MSK-$
	DB    0 ; UFM_FOC
	DS    UT_U2000+OU_X-$
	DB    8,0,2,1
	DS    UT_U2000+OU_HLP-$
	%W    (0)
	DS    UT_U2000+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2000+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U2000+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (MR_UIAM)		; DP
	DS    UT_U2000+OU_M_S-$
	%W    (0)
	DS    UT_U2000+OU_M_P-$
	%W    (0)
	DS    UT_U2000+OU_M_F-$
	%W    (0)
	DS    UT_U2000+OU_M_T-$
	%W    (0FFH)
	%W    (000H)
	%W    (UT_U2000TA)
	%W    (0FFH)
	%W    (001H)
	%W    (UT_U2000TB)
	%W    (0FFH)
	%W    (002H)
	%W    (UT_U2000TC)
	%W    (0FFH)
	%W    (003H)
	%W    (UT_U2000TD)
	%W    (0FFH)
	%W    (004H)
	%W    (UT_U2000TE)
	%W    (0FFH)
	%W    (005H)
	%W    (UT_U2000TF)
	%W    (0)
UT_U2000TA:DB 'X',0
UT_U2000TB:DB 'Y',0
UT_U2000TC:DB 'Z',0
UT_U2000TD:DB 'D',0
UT_U2000TE:DB 'E',0
UT_U2000TF:DB 'F',0

UT_U2001:
	DS    UT_U2001+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2001+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2001+OU_X-$
	DB    3,1,8,1
	DS    UT_U2001+OU_HLP-$
	%W    (0)
	DS    UT_U2001+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2001+OU_A_RD-$
	%W    (UR_MRPDl)	; A_RD UR_MRPl
	%W    (UW_MRPDl)	; A_WR UW_MRPl
	DS    UT_U2001+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,0		; DP
	DS    UT_U2001+OU_I_F-$
	DB    0C3H		; format I_F
	%W    (8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2002:
	DS    UT_U2002+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2002+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2002+OU_X-$
	DB    5,2,6,1
	DS    UT_U2002+OU_HLP-$
	%W    (0)
	DS    UT_U2002+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2002+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2002+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_MS		; DP
	DS    UT_U2002+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2003:
	DS    UT_U2003+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2003+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2003+OU_X-$
	DB    5,3,6,1
	DS    UT_U2003+OU_HLP-$
	%W    (0)
	DS    UT_U2003+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2003+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2003+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_ACC	; DP
	DS    UT_U2003+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2004:
	DS    UT_U2004+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2004+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2004+OU_X-$
	DB    5,4,6,1
	DS    UT_U2004+OU_HLP-$
	%W    (0)
	DS    UT_U2004+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2004+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2004+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_ME		; DP
	DS    UT_U2004+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2005:
	DS    UT_U2005+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2005+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2005+OU_X-$
	DB    5,5,6,1
	DS    UT_U2005+OU_HLP-$
	%W    (0)
	DS    UT_U2005+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2005+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2005+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_P		; DP
	DS    UT_U2005+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2006:
	DS    UT_U2006+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2006+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2006+OU_X-$
	DB    5,6,6,1
	DS    UT_U2006+OU_HLP-$
	%W    (0)
	DS    UT_U2006+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2006+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2006+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_I		; DP
	DS    UT_U2006+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2007:
	DS    UT_U2007+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2007+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2007+OU_X-$
	DB    5,7,6,1
	DS    UT_U2007+OU_HLP-$
	%W    (0)
	DS    UT_U2007+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2007+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2007+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_D		; DP
	DS    UT_U2007+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2008:
	DS    UT_U2008+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2008+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2008+OU_X-$
	DB    5,8,6,1
	DS    UT_U2008+OU_HLP-$
	%W    (0)
	DS    UT_U2008+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2008+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2008+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_S1		; DP
	DS    UT_U2008+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2009:
	DS    UT_U2009+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2009+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2009+OU_X-$
	DB    5,9,6,1
	DS    UT_U2009+OU_HLP-$
	%W    (0)
	DS    UT_U2009+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2009+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2009+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_S2		; DP
	DS    UT_U2009+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2010:
	DS    UT_U2010+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2010+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2010+OU_X-$
	DB    5,10,6,1
	DS    UT_U2010+OU_HLP-$
	%W    (0)
	DS    UT_U2010+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2010+OU_A_RD-$
	%W    (UR_MRb)		; A_RD
	%W    (UW_MRb)		; A_WR
	DS    UT_U2010+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_FLG	; DP
	DS    UT_U2010+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2011:
	DS    UT_U2011+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2011+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2011+OU_X-$
	DB    5,11,6,1
	DS    UT_U2011+OU_HLP-$
	%W    (0)
	DS    UT_U2011+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2011+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2011+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_CFG	; DP
	DS    UT_U2011+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (0FFFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2012:
	DS    UT_U2012+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2012+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2012+OU_X-$
	DB    5,12,6,1
	DS    UT_U2012+OU_HLP-$
	%W    (0)
	DS    UT_U2012+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2012+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2012+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_SCM	; DP
	DS    UT_U2012+OU_I_F-$
	DB    80H		; format I_F
	%W    (-0FFH)		; I_L
	%W    ( 0FFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2013:
	DS    UT_U2013+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2013+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2013+OU_X-$
	DB    5,13,6,1
	DS    UT_U2013+OU_HLP-$
	%W    (0)
	DS    UT_U2013+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2013+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2013+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_SCD	; DP
	DS    UT_U2013+OU_I_F-$
	DB    80H		; format I_F
	%W    (-0FFH)		; I_L
	%W    ( 0FFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2014:
	DS    UT_U2014+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2014+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2014+OU_X-$
	DB    5,14,6,1
	DS    UT_U2014+OU_HLP-$
	%W    (0)
	DS    UT_U2014+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2014+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2014+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_JCO	; DP
	DS    UT_U2014+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (0FFFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2015:
	DS    UT_U2015+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2015+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2015+OU_X-$
	DB    5,15,6,1
	DS    UT_U2015+OU_HLP-$
	%W    (0)
	DS    UT_U2015+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2015+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2015+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_JCR	; DP
	DS    UT_U2015+OU_I_F-$
	DB    80H		; format I_F
	%W    (-7FFFH)		; I_L
	%W    ( 7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2016:
	DS    UT_U2016+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2016+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2016+OU_X-$
	DB    5,16,6,1
	DS    UT_U2016+OU_HLP-$
	%W    (0)
	DS    UT_U2016+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2016+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2016+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_JCH	; DP
	DS    UT_U2016+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (0FFFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

%IF (%MR_REG_SEL) THEN (
UT_U2017:
	DS    UT_U2017+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2017+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2017+OU_X-$
	DB    10,17,1,1
	DS    UT_U2017+OU_HLP-$
	%W    (0)
	DS    UT_U2017+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2017+OU_A_RD-$
	%W    (NULL_A)		; A_RD
	%W    (UW_REGTYPE)	; A_WR
	DS    UT_U2017+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_JCH	; DP
	DS    UT_U2017+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (0FFFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru
)FI

; *******************************************************************
; Lockovany display

UT_GR30:DS    UT_GR30+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR30+OGR_BTXT-$
	%W    (UT_GT30)
	DS    UT_GR30+OGR_STXT-$
	%W    (0)
	DS    UT_GR30+OGR_HLP-$
	%W    (0)
	DS    UT_GR30+OGR_SFT-$
	%W    (UT_SF30)
	DS    UT_GR30+OGR_PU-$
	%W    (UT_U3001)
%IF(%REG_COUNT GE 2) THEN(
	%W    (UT_U3002)
%IF(%REG_COUNT GE 3) THEN(
	%W    (UT_U3003)
)FI )FI
	%W    (UT_U1021)
%IF(%REG_COUNT GE 2) THEN(
	%W    (UT_U1022)
)FI
	%W    (0)

UT_GT30:DB    'X           LOCK',C_NL
	DB    'Y         Z',C_NL
	DB    0

UT_SF30:DB    K_STOP
	%W    (0)
	%W    (STP_ALL)

	DB    K_END
	%W    (0)
	%W    (CLR_ALL)

%IF(%WITH_IICKB)THEN(
	DB    68		; Reset
	%W    (0)
	%W    (LIM_RESET)

	DB    71		; Stop
	%W    (0)
	%W    (LIM_STOP)
)FI
	DB    0

UT_U3001:
	DS    UT_U3001+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U3001+OU_MSK-$
	DB    0
	DS    UT_U3001+OU_X-$
	DB    1,0,7,1
	DS    UT_U3001+OU_HLP-$
	%W    (0)
	DS    UT_U3001+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U3001+OU_A_RD-$
	%W    (UR_MRPl)		; A_RD
	%W    (UW_MRPl)		; A_WR
	DS    UT_U3001+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    0,0		; DP
	DS    UT_U3001+OU_I_F-$
	DB    0C2H		; format I_F
	%W    (8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; poloha Y

UT_U3002:
	DS    UT_U3002+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U3002+OU_MSK-$
	DB    0
	DS    UT_U3002+OU_X-$
	DB    1,1,7,1
	DS    UT_U3002+OU_HLP-$
	%W    (0)
	DS    UT_U3002+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U3002+OU_A_RD-$
	%W    (UR_MRPl)		; A_RD
	%W    (UW_MRPl)		; A_WR
	DS    UT_U3002+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    1,0		; DP
	DS    UT_U3002+OU_I_F-$
	DB    0C2H		; format I_F
	%W    (8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; poloha Z

UT_U3003:
	DS    UT_U3003+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U3003+OU_MSK-$
	DB    0
	DS    UT_U3003+OU_X-$
	DB    11,1,5,1
	DS    UT_U3003+OU_HLP-$
	%W    (0)
	DS    UT_U3003+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U3003+OU_A_RD-$
	%W    (UR_MRPl)		; A_RD
	%W    (UW_MRPl)		; A_WR
	DS    UT_U3003+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    2,0		; DP
	DS    UT_U3003+OU_I_F-$
	DB    0C0H		; format I_F
	%W    (8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; *******************************************************************
; Nastaveni parametru komunikace
UT_GR72:DS    UT_GR72+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR72+OGR_BTXT-$
	%W    (UT_GT72)
	DS    UT_GR72+OGR_STXT-$
	%W    (0)		; UT_GS72
	DS    UT_GR72+OGR_HLP-$
	%W    (0)
	DS    UT_GR72+OGR_SFT-$
	%W    (UT_SF72)
	DS    UT_GR72+OGR_PU-$
	%W    (UT_U7201)
	%W    (UT_U7202)
	%W    (UT_U7203)
	%W    (0)

UT_GT72:DB    'Type  Adr  Spd',0

UT_SF72:DB    -1
	%W    (UT_SF1)

UT_U7201:
	DS    UT_U7201+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U7201+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7201+OU_X-$
	DB    0,1,5,1
	DS    UT_U7201+OU_HLP-$
	%W    (0)
	DS    UT_U7201+OU_SFT-$
	%W    (UT_SF7201)
	DS    UT_U7201+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_COMi)		; A_WR
	DS    UT_U7201+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (COM_TYP)		; DP
	DS    UT_U7201+OU_M_S-$
	%W    (0)
	DS    UT_U7201+OU_M_P-$
	%W    (0)
	DS    UT_U7201+OU_M_F-$
	%W    (COM_TYPCH)
	DS    UT_U7201+OU_M_T-$
	%W    (0FFFFH)
	%W    (00000H)
	%W    (UT_U7201T0)
	%W    (0FFFFH)
	%W    (00001H)
	%W    (UT_U7201T1)
	%W    (0FFFFH)
	%W    (00002H)
	%W    (UT_U7201T2)
	%W    (0)
UT_U7201T0:DB    'None',0
UT_U7201T1:DB    'uLAN',0
UT_U7201T2:DB    'RS232',0

UT_SF7201:
	DB    K_2
	%W    (2)
	%W    (COM_WR23)

	DB    K_1
	%W    (1)
	%W    (COM_WR23)

	DB    K_0
	%W    (0)
	%W    (COM_WR23)

	DB    0

UT_U7202:
	DS    UT_U7202+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U7202+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7202+OU_X-$
	DB    6,1,2,1
	DS    UT_U7202+OU_HLP-$
	%W    (0)
	DS    UT_U7202+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U7202+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_COMi)	; A_WR
	DS    UT_U7202+OU_DPSI-$
	%W    (0)		; DPSI
	%W    (COM_ADR)		; DP
	DS    UT_U7202+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (99)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U7203:
	DS    UT_U7203+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U7203+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7203+OU_X-$
	DB    10,1,5,1
	DS    UT_U7203+OU_HLP-$
	%W    (0)
	DS    UT_U7203+OU_SFT-$
	%W    (UT_SF7203)
	DS    UT_U7203+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_COMi)		; A_WR
	DS    UT_U7203+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (COM_SPD)		; DP
	DS    UT_U7203+OU_M_S-$
	%W    (0)
	DS    UT_U7203+OU_M_P-$
	%W    (0)
	DS    UT_U7203+OU_M_F-$
	%W    (COM_SPDCH)
	DS    UT_U7203+OU_M_T-$
	%W    (0FFFFH)
	%W    (00000H)
	%W    (UT_U7203T0)
	%W    (0FFFFH)
	%W    (00001H)
	%W    (UT_U7203T1)
	%W    (0FFFFH)
	%W    (00002H)
	%W    (UT_U7203T2)
	%W    (0FFFFH)
	%W    (00003H)
	%W    (UT_U7203T3)
	%W    (0FFFFH)
	%W    (00004H)
	%W    (UT_U7203T4)
	%W    (0FFFFH)
	%W    (00005H)
	%W    (UT_U7203T5)
	%W    (0)
UT_U7203T0:DB    ' 1200',0
UT_U7203T1:DB    ' 2400',0
UT_U7203T2:DB    ' 4800',0
UT_U7203T3:DB    ' 9600',0
UT_U7203T4:DB    '19200',0
UT_U7203T5:DB    '28800',0

UT_SF7203:
	DB    K_9
	%W    (3)
	%W    (COM_WR23)

	DB    K_4
	%W    (2)
	%W    (COM_WR23)

	DB    K_2
	%W    (1)
	%W    (COM_WR23)

	DB    K_1
	%W    (4)
	%W    (COM_WR23)

	DB    K_0
	%W    (0)
	%W    (COM_WR23)

	DB    0

