#   Project file pro viceosou regulaci
#         (C) Pisoft 1996

mars.obj  : mars.asm mars_lim.asm mars_std.asm mars_std.h config.h
	a51 mars.asm $(par) debug

mr_psys.obj : mr_psys.asm config.h
	a51   mr_psys.asm $(par)

mr_pshw.obj : mr_pshw.asm config.h
	a51   mr_pshw.asm $(par)

pb_uihw.obj : pb_uihw.asm config.h
	a51   pb_uihw.asm $(par) debug

mars.     : mars.obj mr_psys.obj mr_pshw.obj pb_uihw.obj ..\pblib\pb.lib
	l51 mars.obj,mr_psys.obj,mr_pshw.obj,pb_uihw.obj,..\pblib\pb.lib code(08800H) xdata(8000H) ramsize(100h) ixref

mars.hex  : mars.
	ohs51 mars

	  : mars.hex
#	sendhex mars.hex /p4 /m3 /t2 /g40960 /b16000
#	sendhex mars.hex /p4 /m3 /t2 /g36864 /b9600
	sendhex mars.hex /p3 /m3 /t2 /b9600
#	sendhex mars.hex /p4 /m3 /t2 /b9600
