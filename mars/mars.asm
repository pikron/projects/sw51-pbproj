$NOMOD51
;********************************************************************
;*              Multiosa regulace  - MARS.ASM                       *
;*                       Hlavni modul                               *
;*                  Stav ke dni 10. 1.1998                          *
;*                      (C) Pisoft 1996                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_AL)
$INCLUDE(%INCH_ADR)
$INCLUDE(%INCH_REGS)
$INCLUDE(%INCH_MR_DEFS)
$INCLUDE(%INCH_MR_PDEFS)
$INCLUDE(%INCH_MR_PSYS)
$INCLUDE(%INCH_UI)
$INCLUDE(%INCH_RS232)
$INCLUDE(%INCH_RSOI)
$LIST

MARS__C SEGMENT CODE
MARS__D SEGMENT DATA
MARS__B SEGMENT DATA BITADDRESSABLE
MARS__X SEGMENT XDATA

; Vyuziti bank registru (USING)
;  0 .. beh v popredi
;  1 .. komunikace uLAN
;  2 .. casove preruseni
;  3 .. komunikace IIC

RSEG	MARS__C
	JMP   RES_STAR		; Skok na zacatek programu

; *******************************************************************
; Nastaveni konkretni konfigurace

;$INCLUDE(MARS_6AX.H)
;$INCLUDE(MARS_STD.H)
$INCLUDE(MARS_TAB.H)
;$INCLUDE(MARS_LIM.H)
;$INCLUDE(MARS_PRF.H)
;$INCLUDE(MARS_TU1.H)

; *******************************************************************

%IF (%WITH_ULAN) THEN (
EXTRN	CODE(uL_FNC,uL_STR)
EXTRN	BIT(uLF_INE)
) FI

%IF(%WITH_IIC)THEN(
$INCLUDE(%INCH_IIC)
)FI

PUBLIC	LEB_FLG

%IF(%WITH_IICKB)THEN(
EXTRN	CODE(UI_INIHW,UI_BEEP,KB_KPUSH,KB_IICWRLN,IHEXLDND)
EXTRN	BIT(FL_DIPR,FL_IICKB)
)FI

EXTRN	CODE(cxMOVE,xxMOVE,xMDPDP,SEL_FNC,ADDATDP)
EXTRN	CODE(UB_A_WR)
EXTRN	CODE(PRINThb,PRINThw,INPUThw)
EXTRN	CODE(MONITOR)
EXTRN   CODE(IHEXLD)

PUBLIC	INPUTc,BEEP,KBDBEEP,ERRBEEP,RES_STAR

RSEG	MARS__B

LEB_FLG:DS    1		; Blikani ledek

HW_FLG: DS    1
ITIM_RF BIT   HW_FLG.7	; Kontrola reentrance preruseni
%IF (%WITH_CMPP) THEN (
FL_CPAC	BIT   HW_FLG.6	; Komparator v cinnosti
FL_CPEV	BIT   HW_FLG.5	; Doslo k udalosti na komparatoru
)FI
FL_25Hz	BIT   HW_FLG.4	; Nastaven pri preruseni
LEB_PHA	BIT   HW_FLG.3	; Pro blikani led
FL_MRMC	BIT   HW_FLG.2	; Povoleni sberu udaju
FL_MRCE	BIT   HW_FLG.1	; Zadost o vynulovani chyby MR_FLG.BMR_ERR
%IF(%WITH_MRDY)THEN(
FL_MRDYT BIT  HW_FLG.0	; Pro vysilani Rm!
)FI

HW_FLG1: DS   1
FL_RDYR1 BIT  HW_FLG1.7 ; Vysli pouze jedno READY
FL_ECUL	 BIT  HW_FLG1.6	; Povolit komunikaci uLAN
FL_ECRS	 BIT  HW_FLG1.5	; Povolit komunikaci RS232
%IF(%WITH_IICCM)THEN(
CMF_IPR	BIT   HW_FLG1.4	; Jsou nevyrizene pozadavky
CMF_NR	BIT   HW_FLG1.3	; Nove pozadavky
)FI
%IF (%WITH_TRIG) THEN (
FL_TRIG2 BIT  HW_FLG1.1	; Doslo k udalosti trigru 2
FL_TRIG1 BIT  HW_FLG1.0	; Doslo k udalosti trigru 1
)FI

RSEG	MARS__X

TMP:	DS    16

C_R_PER	EQU   2
REF_PER:DS    1		; Perioda refrese displeje

RSEG	MARS__C

RES_STAR:
	MOV   IEN0,#0
	MOV   IEN1,#0
	MOV   IP0,#0
	MOV   IP1,#0
	MOV   SP,#80H
	MOV   PCON,#10000000B	; Bd = OSC/12/16/(256-TH1)
	MOV   TM2CON,#10000001B; timer 2 CLK, TR2 disabled, 16 bit OV
	%VECTOR(T2CMP%TIME_INT_CM,I_TIME1); Realny cas z komparatoru
	SETB  ECM%TIME_INT_CM	; povoleni casu od CMx

	MOV   P4,#0FFH

	CALL  I_TIMRI
	CALL  I_TIMRI
	MOV   CINT25,#10
	CLR   A
	MOV   DPTR,#TIMRI
	MOVX  @DPTR,A
	MOV   DPTR,#TIME
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A

	CLR    A
	MOV    HW_FLG,A
	MOV    HW_FLG1,A
	MOV    LEB_FLG,A
	SETB   ITIM_RF

	CLR    A
	MOV    DPTR,#APWM0
	MOVX   @DPTR,A
	MOV    DPTR,#APWM1
	MOVX   @DPTR,A
	MOV    DPTR,#APWM2
	MOVX   @DPTR,A
	MOV    DPTR,#MR_UIAM
	MOVX   @DPTR,A
	INC    DPTR
	MOVX   @DPTR,A
%IF(%WITH_MRDY)THEN(
	MOV    DPTR,#RS_MRDYM
	MOVX   @DPTR,A
)FI
%IF (%WITH_TRIG) THEN (
	MOV    DPTR,#TRIG1
	MOVX   @DPTR,A
	MOV    DPTR,#TRIG2
	MOVX   @DPTR,A
	MOV    CTCON,#01010101B ; Vzestupne hrany
)FI
%IF (%WITH_F_SEL) THEN (
	MOV   DPTR,#TI_SFRQ
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	CALL  TI_FSL
)FI

	MOV   DPTR,#REG_A+OMR_FLG
	CLR   A
	MOVX  @DPTR,A
%IF (%WITH_CMPP) THEN (
	MOV   DPTR,#CP_ARR
	MOV   R2,#CP_NUM*OCP_LEN
	CALL  xNULs
)FI

	CALL  LCDINST
    %IF(%WITH_IICKB)THEN(
	CLR   FL_DIPR
	JNZ   RES_ST2		; Test pritomnosti LCD displaye
	SETB  FL_DIPR
    )FI
	CALL  LEDWR
RES_ST2:

    %IF(%WITH_IIC)THEN(
	CALL  INI_IIC		; Inicializace IIC komunikace
    )FI
    %IF(%WITH_IICCM)THEN(
	CALL  CM_INI
    )FI

	MOV   DPTR,#REF_PER
	MOV   A,#C_R_PER
	MOVX  @DPTR,A

	JMP   L0

INPUTc:	CALL  SCANKEY
	JZ    INPUTc
	RET

; Pipnuti na klavese klavesnice
;KBDBEEP:JMP   KBDSTDB
KBDBEEP:MOV   A,#2
BEEP:	MOV   DPTR,#BEEPTIM
	MOVX  @DPTR,A
	SETB  %BEEP_FL
	MOV   DPTR,#LED       ; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
	MOV   A,R2
	RET
ERRBEEP:MOV   A,#8
    %IF(%WITH_IICKB)THEN(
	JMP   UI_BEEP
    )ELSE(
	JMP   BEEP
    )FI

; *******************************************************************
;
; Casove preruseni

PUBLIC  KBDTIMR

RSEG	MARS__D

CINT25: DS    1

RSEG	MARS__X

BEEPTIM:DS    1	   ; Timer delky pipani

N_OF_T  EQU   6    ; Pocet timeru
TIMR1:  DS    1    ; Timery jsou decrementovany
TIMR2:  DS    1    ; s frekvenci 25 Hz
TIMR_WAIT:
TIMR3:  DS    1
REF_TIM:DS    1	   ; Refresh timr
KBDTIMR:DS    1
TIMRI:  DS    1
TIME:   DS    2    ; Cas v 0.01 min              =====

%IF (%WITH_F_SEL) THEN (
TI_SFRQ:DS    2	   ; Zvolena kombinace frekvenci
TI_DINT:DS    2    ; Frekvence IRQ = XTAL/12/TI_DINT
TI_DI25:DS    1	   ; Delic frekvence IRQ na 25 Hz
)FI

RSEG	MARS__C

USING   2
I_TIME1:PUSH  ACC		; Cast s pruchodem 600 Hz
	PUSH  PSW
	MOV   PSW,#AR0		; Prepnuti banky registru
	PUSH  B
	PUSH  DPL
	PUSH  DPH
%IF (%WITH_F_SEL) THEN (
	MOV   DPTR,#TI_DINT
	MOVX  A,@DPTR
	ADD   A,CML%TIME_INT_CM
	MOV   CML%TIME_INT_CM,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,CMH%TIME_INT_CM
	MOV   CMH%TIME_INT_CM,A
)ELSE(
	MOV   A,CML%TIME_INT_CM
	ADD   A,#LOW  DINT600
	MOV   CML%TIME_INT_CM,A
	MOV   A,CMH%TIME_INT_CM
	ADDC  A,#HIGH DINT600
	MOV   CMH%TIME_INT_CM,A
)FI
	CLR   CMI%TIME_INT_CM

	CLR   FL_25Hz
	DJNZ  CINT25,I_TIM10
	SETB  FL_25Hz
I_TIM10:
	CALL  DO_REG		; Regulace DC motoru
      %IF(%WITH_MRDY)THEN(
	SETB  FL_MRDYT
      )FI
	CALL  ADC_D		; Cteni AD prevodniku
      %IF (%WITH_TRIG) THEN (
	MOV   DPTR,#TRIG1	; Test trigru
	MOVX  A,@DPTR
	ANL   A,TM2IR
	JZ    I_TIM21
	CPL   A
	ANL   TM2IR,A
	CALL  TG_OCC		; Ulozit a vykonat
	SETB  FL_TRIG1
I_TIM21:MOV   DPTR,#TRIG2	; Test trigru
	MOVX  A,@DPTR
	ANL   A,TM2IR
	JZ    I_TIM22
	CPL   A
	ANL   TM2IR,A
	CALL  TG_OCC		; Ulozit a vykonat
	SETB  FL_TRIG2
I_TIM22:
      )FI
      %IF (%WITH_CMPP) THEN (
	JNB   FL_CPAC,I_TIM23
	CALL  CP_DO		; Zpracovani komparatoru
I_TIM23:
      )FI


	JNB   FL_25Hz,I_TIMR1	; Konec casti spruchodem 600 Hz
%IF (%WITH_F_SEL) THEN (	; Pruchod s frekvenci 25 Hz
	MOV   DPTR,#TI_DI25
	MOVX  A,@DPTR
	MOV   CINT25,A
)ELSE(
	MOV   CINT25,#DINT25
)FI
	CALL  ADC_S		; Spusteni cyklu prevodu ADC
    %IF(%WITH_ULAN)THEN(
	JNB   FL_ECUL,I_TIM70
	CALL  uL_STR
    )FI
I_TIM70:%WATCHDOG
	MOV   DPTR,#BEEPTIM
	MOVX  A,@DPTR
	JZ    I_TIM80
	DEC   A
	MOVX  @DPTR,A
	JNZ   I_TIM80
	CLR   %BEEP_FL
	MOV   DPTR,#LED		; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
I_TIM80:MOV   DPTR,#TIMR1
	MOV   B,#N_OF_T-1
I_TIME2:MOVX  A,@DPTR
	JZ    I_TIME3
	DEC   A
	MOVX  @DPTR,A
I_TIME3:INC   DPTR
	DJNZ  B,I_TIME2
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	JNB   ITIM_RF,I_TIMR1
	JB    ACC.7,I_TIME4
I_TIMR1:POP   DPH
	POP   DPL
	POP   B
	POP   PSW
	POP   ACC
	SETB  EA
I_TIMRI:RETI

I_TIME4:CLR   ITIM_RF		; Pruchod 0.6 sec
;	CALL  I_TIMRI
;	MOV   PSW,#AR0		; Banka 2
	ADD   A,#15
	MOVX  @DPTR,A

	MOV   A,LEB_FLG
	JBC   LEB_PHA,I_TIM42
	ORL   LED_FLG,A
	SETB  LEB_PHA
	SJMP  I_TIM43
I_TIM42:CPL   A
	ANL   LED_FLG,A
I_TIM43:SETB  ITIM_RF
	JMP   I_TIMR1

WAIT_T:	MOV   DPTR,#TIMR_WAIT
	MOVX  @DPTR,A
WAIT_T1:MOV   DPTR,#TIMR_WAIT
	MOVX  A,@DPTR
	JNZ   WAIT_T1
	RET

%IF (%WITH_F_SEL) THEN (
TI_FSLR:MOV   DPTR,#TI_SFRQ
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A
TI_FSL:	MOV   DPTR,#TI_SFRQ
	MOVX  A,@DPTR
	CJNE  A,#(TI_FSLte-TI_FSLt)/3,TI_FSL1
TI_FSL1:JNC   TI_FSL9
	MOV   R0,A
	RL    A
	ADD   A,R0
	ADD   A,#LOW TI_FSLt
	MOV   R2,A
	CLR   A
	ADDC  A,#HIGH TI_FSLt
	MOV   R3,A
	%LDR45i(TI_DINT)
	%LDR01i(3)
	CLR   ECM%TIME_INT_CM
	CALL  xxMOVE
	SETB  ECM%TIME_INT_CM	; povoleni casu od CMx
	MOV   A,#1
	RET
TI_FSL9:SETB  F0
	RET

TI_FSLt:%W    (DINT600)		; 600 Hz
	DB    (DINT25)
	%W    (DINT600)		; 600 Hz
	DB    (DINT25)
	%W    ((DINT600*3+2)/4)	; 800 Hz
	DB    (DINT25*4)/3
	%W    ((DINT600*3+2)/5)	; 1000 Hz
	DB    (DINT25*5)/3
	%W    (DINT600/2)	; 1200 Hz
	DB    (DINT25*2)
TI_FSLte:
)FI

; *******************************************************************
; Promenne multiregulatoru

%IF (%MR_REG_SEL) THEN (
  EXTRN	CODE(MR_PIDNLP,MR_PZP,MR_PIDLP)
  MR_REG_ARR:
	%W    (%MR_REG_TYPE)
	%W    (MR_PIDNLP)
	%W    (MR_PIDLP)
	%W    (MR_PZP)
	%W    (MR_RELCHP)
	%W    (MR_RPULSEP)
	%W    (MR_R_DIRC)
	%W    (0)
)ELSE(
  EXTRN	CODE(%MR_REG_TYPE)
)FI

RSEG	MARS__B

MR_FLG:	DS    1
MR_FLGA:DS    1

RSEG	MARS__D

MR_BAS:	DS    2		; ukazatel na struktury regulatoru

%STRUCTM(OMR,SCM,2)	; zmena meritka - nasobeni
%STRUCTM(OMR,SCD,2)	; zmena meritka - deleni
%STRUCTM(OMR,JCA,2)	; joy ctrl - adresa ridiciho registru
%STRUCTM(OMR,JCO,2)	; joy ctrl - offset joysticku
%STRUCTM(OMR,JCR,2)	; joy ctrl - rozliseni joysticku
%STRUCTM(OMR,JCH,2)	; joy ctrl - necitlivost joysticku

%IF(%WITH_IICCM)THEN(
%STRUCTM(OMR,LAP,4)	; iic cm - aktualni logicka pozice
%STRUCTM(OMR,LRP,4)	; iic cm - pozadovana logicka pozice
%STRUCTM(OMR,CMF,1)	; iic cm - priznaky a pozadavky

CMB_GEP	EQU   0		; najeti na polohu
CMB_HH	EQU   1		; kalibrace nulove polohy
CMB_RPR	EQU   2		; parametry regulatoru
CMB_CFG	EQU   3		; parametry konfigurace a LS prepoctu
CMM_GEP	EQU   1 SHL CMB_GEP
CMM_HH	EQU   1 SHL CMB_HH
CMM_RPR	EQU   1 SHL CMB_RPR
CMM_CFG	EQU   1 SHL CMB_CFG
)FI

RSEG	MARS__X

REG_N	EQU   %REG_COUNT
REG_LEN	EQU   OMR_LEN
REG_A:	DS    REG_N*REG_LEN+1

OREG_A	EQU   0
OREG_B	EQU   REG_LEN
OREG_C	EQU   REG_LEN*2
%IF(%REG_COUNT GE 4) THEN(
OREG_D	EQU   REG_LEN*3
OREG_E	EQU   REG_LEN*4
OREG_F	EQU   REG_LEN*5
)FI

PUBLIC	MR_BAS,MR_FLG,MR_FLGA
PUBLIC	REG_A,REG_LEN,REG_N
PUBLIC	GO_NOTIFY,DO_REGDBG
PUBLIC  FL_MRCE

RSEG	MARS__C

GO_NOTIFY:
      %IF(%WITH_MRDY)THEN(
	CLR   FL_MRDYT
      )FI
	SETB  RSF_BSYO
	JNB   ACC.BMR_DBG,GO_NTF1
	JMP   MRMC_I 		; Start ukladani udaju
GO_NTF1:RET

DO_REGDBG:
	JMP   DO_MRMC

; *******************************************************************
; Cteni polohy z CF32006

AIR_CF0	XDATA 0FF20H	; Baze prvniho CF32006
AIR_CF1	XDATA 0FF40H	; Baze druheho CF32006

; *******************************************************************
; Vystup energii na motory

APWM0	XDATA 0FFE0H
APWM1	XDATA 0FFE1H
APWM2	XDATA 0FFE2H
APWM3	XDATA 0FFE4H
APWM4	XDATA 0FFE5H
APWM5	XDATA 0FFE6H

; *******************************************************************
; Manualni ovladani joystickem

; OMR_GST ... stav
; OMR_JCA ... joy ctrl - adresa ridiciho registru
; OMR_JCO ... joy ctrl - offset joysticku
; OMR_JCR ... joy ctrl - rozliseni joysticku
; OMR_JCH ... joy ctrl - necitlivost joysticku

CI_JCT:	%LDR45i(CD_JCT)
	CALL  CI_VG
	JMP   CD_JCTZ

CD_JCT: JB    FL_25Hz,CD_JCTA
CD_JCTSC:CALL MR_GPSC
	CALL  MR_WRRP
	JMP   VR_REG
CD_JCTA:MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,#OMR_JCO	; Offset hodnoty
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_JCO+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   A,#OMR_JCA	; Adresa vstupu
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OMR_JCA+1
	MOVC  A,@A+DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOVX  A,@DPTR		; Nacteni hodnoty
	CLR   C
	SUBB  A,R4
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	JZ    CD_JCT0		; Eliminace limitnich hodnot
	CJNE  A,#0FFH,CD_JCT1
CD_JCT0:CALL  MR_GPSC
	JMP   CD_JC65
CD_JCT1:SUBB  A,R5
	MOV   R5,A
	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	JB    ACC.7,CD_JCT2
	MOV   A,#OMR_JCH	; Necitlivost pro +
	MOVC  A,@A+DPTR
	CPL   A
	SETB  C
	ADDC  A,R4
	MOV   R4,A
	MOV   A,#OMR_JCH+1
	MOVC  A,@A+DPTR
	CPL   A
	ADDC  A,R5
	MOV   R5,A
	JNC   CD_JCTZ
	MOV   A,#OMR_GST	; Stav - priznak pohybu
	MOVC  A,@A+DPTR
	JB    ACC.7,CD_JCTZ
	JB    ACC.6,CD_JCT3
	SETB  ACC.6
	MOV   R1,A
	JMP   CD_JCT8
CD_JCT2:MOV   A,#OMR_JCH	; Necitlivost pro -
	MOVC  A,@A+DPTR
	ADD   A,R4
	MOV   R4,A
	MOV   A,#OMR_JCH+1
	MOVC  A,@A+DPTR
	ADDC  A,R5
	MOV   R5,A
	JC    CD_JCTZ
	MOV   A,#OMR_GST	; Stav - priznak pohybu
	MOVC  A,@A+DPTR
	JB    ACC.6,CD_JCTZ
	JB    ACC.7,CD_JCT3
	SETB  ACC.7
	MOV   R1,A
	SJMP  CD_JCT8
CD_JCT3:MOV   R1,A		; Potvrzeni pocatku pohybu
	INC   R1		; ctyrmi vzorky
	JNB   ACC.2,CD_JCT8
	MOV   A,#OMR_JCR	; Nasobeni hodnoty
	MOVC  A,@A+DPTR
	MOV   R2,A
	MOV   A,#OMR_JCR+1
	MOVC  A,@A+DPTR
	MOV   R3,A
	CALL  MULsi		; R567 pozadovana rychlost
	MOV   A,#OMR_MS
	MOVC  A,@A+DPTR
	MOV   R2,A
	MOV   A,#OMR_MS+1
	MOVC  A,@A+DPTR
	MOV   R3,A		; R23 .. OMR_MS
	MOV   A,R5
	MOV   R4,A
	MOV   A,R6
	MOV   R5,A
	RLC   A
	MOV   A,R7
	JB    ACC.7,CD_JCT5
	ADDC  A,#0
	JNZ   CD_JCT4
	SETB  C			; ? R45 <= OMR_MS
	MOV   A,R4
	SUBB  A,R2
	MOV   A,R5
	SUBB  A,R3
	JC    CD_JCT6
CD_JCT4:MOV   A,R2		; R45 = OMR_MS
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	SJMP  CD_JCT6
CD_JCT5:ADDC  A,#0
	JNZ   CD_JC55
	MOV   A,R2		; ? R45 >= -OMR_MS
	ADD   A,R4
	MOV   A,R3
	ADDC  A,R5
	JC    CD_JCT6
CD_JC55:CLR   C			; R45 = -OMR_MS
	CLR   A
	SUBB  A,R2
	MOV   R4,A
	CLR   A
	SUBB  A,R3
	MOV   R5,A
CD_JCT6:CALL  MR_GPSV		; Pozadovana poloha
CD_JC65:CALL  MR_WRRP
	SJMP  CD_JCT9
CD_JCTZ:JNB   MR_FLGA.BMR_ERR,CD_JC79
	CLR   MR_FLGA.BMR_ERR	; Joystick v klidu a je chyba
	SETB  FL_MRCE		; Pokusi se vynulovat MR_FLG.BMR_ERR
	CALL  MR_RDAP
	CALL  MR_WRRP
	%MR_OFS2DP(OMR_FOI)
	MOV   R2,#OMR_ERC-OMR_FOI+1
	CLR   A
CD_JC73:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R2,CD_JC73
CD_JC79:%MR_OFS2DP(OMR_RS)	; Pozadovana rychlost
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   R1,A
CD_JCT8:%MR_OFS2DP(OMR_GST)
	MOV   A,R1
	MOVX  @DPTR,A
CD_JCT9:JMP   VR_REG

; *******************************************************************
; Neregulator, energie R45 = OMR_RS

MR_RPULSEP:
	MOV   A,#OMR_RS
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_RS+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	RET

; *******************************************************************
; Releova charakteristika pro identifikaci
;
; parametry  

MR_RELCHP:
	CLR   C
	MOV   A,#OMR_AP		; R45=OMR_RQI-OMR_ACT
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_RPI
	MOVC  A,@A+DPTR
	SUBB  A,R4
	MOV   R4,A
	MOV   A,#OMR_AP+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   A,#OMR_RPI+1
	MOVC  A,@A+DPTR
	SUBB  A,R5
	MOV   R5,A
	MOV   B,A
	MOV   A,#OMR_AP+2
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   A,#OMR_RPI+2
	MOVC  A,@A+DPTR
	SUBB  A,R6
	MOV   C,B.7
	JB    ACC.7,MR_RCH1
	ADDC  A,#0
	JZ    MR_RCH2
	MOV   R4,#0FFH
	MOV   R5,#07FH
	SJMP  MR_RCH2
MR_RCH1:ADDC  A,#0
	JZ    MR_RCH2
	MOV   R4,#000H
	MOV   R5,#080H		; _ -> R45 ; R6
MR_RCH2:

	MOV   A,#OMR_FOI	; minuly stav
	MOVC  A,@A+DPTR
	JNZ   MR_RCH5
	MOV   A,R5		; minula energie kladna
	JNB   ACC.7,MR_RCH4
	MOV   A,#OMR_P
	MOVC  A,@A+DPTR
	ADD   A,R4
	MOV   A,#OMR_P+1
	MOVC  A,@A+DPTR
	ADDC  A,R5
	JNC   MR_RCH6
MR_RCH4:MOV   A,#OMR_I		; pristi kladna
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_I+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   R1,#0
	SJMP  MR_RCH7
MR_RCH5:MOV   A,R5		; minula energie kladna
	JB    ACC.7,MR_RCH6
	CLR   C
	MOV   A,#OMR_P
	MOVC  A,@A+DPTR
	SUBB  A,R4
	MOV   A,#OMR_P+1
	MOVC  A,@A+DPTR
	SUBB  A,R5
	JC    MR_RCH4
MR_RCH6:MOV   A,#OMR_I		; pristi zaporna
	MOVC  A,@A+DPTR
	CPL   A
	ADD   A,#1
	MOV   R4,A
	MOV   A,#OMR_I+1
	MOVC  A,@A+DPTR
	CPL   A
	ADDC  A,#0
	MOV   R5,A
	MOV   R1,#1
MR_RCH7:MOV   A,#OMR_FOI
	ADD   A,DPL
	MOV   DPL,A
	CLR   A
	ADDC  A,DPH
	MOV   DPH,A
	MOV   A,R1
	MOVX  @DPTR,A
	CLR   F0
	RET

; *******************************************************************
; Regulator kopirujici rychlost z predchoziho kanalu

MR_R_DIRC:
	%ADDDPci (OMR_AS-REG_LEN)
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	%ADDDPci (REG_LEN-1)
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	%ADDDPci (-OMR_AS-1)
	JMP   %MR_REG_TYPE

; *******************************************************************
; Nastaveni pro MARS

MR_INIS:CALL  MR_ZER
	%LDR45i (REG_A)
	%LDR23i (STDR_A)
	%LDR01i (STDR_AE-STDR_A)
	CALL  cxMOVE
	MOV   A,#MMR_ENI 		; OR MMR_ENR
	MOV   DPTR,#REG_A+OREG_A+OMR_FLG
	MOVX  @DPTR,A
%IF(%REG_COUNT GE 2) THEN(
	MOV   DPTR,#REG_A+OREG_B+OMR_FLG
	MOVX  @DPTR,A
%IF(%REG_COUNT GE 3) THEN(
	MOV   DPTR,#REG_A+OREG_C+OMR_FLG
	MOVX  @DPTR,A
%IF(%REG_COUNT GE 4) THEN(
	MOV   DPTR,#REG_A+OREG_D+OMR_FLG
	MOVX  @DPTR,A
%IF(%REG_COUNT GE 5) THEN(
	MOV   DPTR,#REG_A+OREG_E+OMR_FLG
	MOVX  @DPTR,A
%IF(%REG_COUNT GE 6) THEN(
	MOV   DPTR,#REG_A+OREG_F+OMR_FLG
	MOVX  @DPTR,A
)FI )FI )FI )FI )FI
%IF (%WITH_F_SEL) THEN (
	MOV   DPTR,#TI_SFRQ
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	JMP   TI_FSL
)ELSE(
	RET
)FI

TM_GEPT:DB    LCD_CLR,'Kam :',0

TM_GEPR:RET

TM_GEP: MOV   DPTR,#TM_GEPT
	CALL  cPRINT
	MOV   R6,#8
	MOV   R7,#0C0H
	CALL  INPUTi
	JB    F0,TM_GEPR
	MOV   A,R5
	MOV   R6,A
	MOV   A,R4
	MOV   R5,A
	CLR   A
	MOV   R4,A
	MOV   R7,A
	MOV   R1,#0		; motor A
	JMP   GO_GEP

; Spusti rizeni motoru R1 od vstupu [OMR_JCA]
GO_JCTS:%LDR23i(0)
; Spusti rizeni motoru R1 od vstupu [R23]
GO_JCT:	CALL  GO_PRPC		; odpojit generator
	JNB   F0,GO_JCT3
	RET
GO_JCT3:MOV   R0,A
	MOV   A,R2
	ORL   A,R3		; Pouzit prednastavene parametry
	JZ    GO_JCT4
	MOV   A,#OMR_JCA	; Ridici adresa
	CALL  MR_GPA1
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
GO_JCT4:MOV   A,R0
	JB    ACC.BMR_ENR,GO_JCT7
	CALL  GO_RSFT		; beznarazove pripojeni regulatoru
GO_JCT7:%LDR45i(CI_JCT)
	JMP   GO_VG

GO_JCTT:MOV   R1,#0
	CALL  GO_JCTS
	MOV   R1,#1
	CALL  GO_JCTS
	MOV   R1,#2
	CALL  GO_JCTS
	RET

; Kalibrace nulove polohy joysticku
JCT_CAL:MOV   A,#OMR_JCA	; Ridici adresa
	CALL  MR_GPA1
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   EA,C
	MOV   DPL,R0
	MOV   DPH,A
	MOV   C,EA		; Nacteni hodnoty
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   EA,C
	MOV   R5,A
	MOV   A,#OMR_JCO	; Nulova poloha
	CALL  MR_GPA1
	MOV   A,R4
	MOV   C,EA
	CLR   EA
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   EA,C
	RET

GO_HHT: MOV   R1,#0
	CALL  GO_HH
	MOV   R1,#1
	CALL  GO_HH
	RET

%IF (%MR_REG_SEL) THEN (

; Nastaveni typu regulatoru pro R1 na typ R4
REG_SEL:MOV   DPTR,#MR_REG_ARR
	INC   R4
REG_SEL2:CALL cLDR23i
	ORL   A,R2
	JZ    REG_SEL9
	DJNZ  R4,REG_SEL2
	CALL  GO_PRPC		; odpojit generator
	JNB   F0,REG_SEL3
	RET
REG_SEL3:ANL  A,#NOT (MMR_ENR OR MMR_BSY)
	MOVX  @DPTR,A
	MOV   A,#OMR_VRJ	; JMP na regulator
	CALL  MR_GPA1
	MOV   C,EA
	CLR   EA
	MOV   A,#2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R2
	MOVX  @DPTR,A
	MOV   EA,C
	CLR   A			; nulova vystupni energie
	MOV   R4,A
	MOV   R5,A
	CALL  MR_GPA1
	CALL  MR_SENEP
	MOV   A,#OMR_FLG	; uvolneni zamku
	CALL  MR_GPA1
	MOVX  A,@DPTR
	ANL   A,#7FH
	MOVX  @DPTR,A
	RET
REG_SEL9:SETB F0
	RET
)FI

; *******************************************************************
; Zaznam deje do pameti ram

RSEG	MARS__X

MRMC_BAS:
MRMC_AP:DS    2		; Pozice kam ukladat
MRMC_EP:DS    2		; Koncova pozice

MRMC_BMB XDATA 08800H	; Pocatek oblasti pro ulozeni dat
MRMC_EMB XDATA 0B000H	; Konec oblasti pro ulozeni dat
MRMC_MAXL SET (MRMC_EMB-MRMC_BMB)/2

RSEG	MARS__C

; Uklada udaje v R4567
DO_MRMC:JNB   FL_MRMC,MRMC_R
	MOV   DPTR,#MRMC_BAS
	MOV   A,#MRMC_AP-MRMC_BAS
	MOVC  A,@A+DPTR
	MOV   R2,A
	ADD   A,#4
	MOV   R0,A
	MOV   A,#MRMC_AP-MRMC_BAS+1
	MOVC  A,@A+DPTR
	MOV   R3,A
	ADDC  A,#0
	MOV   R1,A
	CLR   C
	MOV   A,#MRMC_EP-MRMC_BAS
	MOVC  A,@A+DPTR
	SUBB  A,R0
	MOV   A,#MRMC_EP-MRMC_BAS+1
	MOVC  A,@A+DPTR
	SUBB  A,R1
	JC    MRMC_R1		; Pamet zaplnena
	MOV   A,R0
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A
	MOV   DPL,R2		; Ulozit data
	MOV   DPH,R3
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
MRMC_R:	RET
MRMC_R1:CLR   FL_MRMC
	RET

; Inicializace subsystemu
MRMC_I:	CLR   FL_MRMC
	%LDMXi(MRMC_AP,MRMC_BMB)
	%LDMXi(MRMC_EP,MRMC_EMB)
	SETB  FL_MRMC
	MOV   A,#1
	RET

; *******************************************************************
; Generator sumu pripraveneho v bufferu historie

; DPTR = MRMC_AP
; pokud MRMC_AP>=MRMC_EP pak CY=1
CD_GNMRA:MOV  DPTR,#MRMC_AP
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R1,A
	INC   DPTR
	SETB  C
	MOVX  A,@DPTR
	SUBB  A,R0
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,R1
	MOV   DPL,R0
	MOV   DPH,R1
	RET

; inicializace a beh
CI_GNS:	JNB   ACC.BMR_DBG,CD_GNS8
	SETB  MR_FLGA.BMR_BSY	; Busy
	%LDR45i(CD_GNS)
	CALL  CI_VG
	CLR   MR_FLGA.BMR_ENR
CD_GNS:	JNB   FL_MRMC,CD_GNS8
	CALL  CD_GNMRA		; DPTR=MRMC_AP
	JC    CD_GNS8
	MOV   A,#2
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#3
	MOVC  A,@A+DPTR
	MOV   R5,A
	JMP   VR_REG1
CD_GNS8:CLR   MR_FLGA.BMR_BSY
	%LDR45i(VR_REG)
CI_GNS9:CLR   MR_FLGA.BMR_ENR
	CALL  CI_VG
	CLR   A
	MOV   R4,A
	MOV   R5,A
	JMP   VR_REG1

; start
GO_GNS:	CALL  GO_PRPC		; odpojit generator
	JB    F0,GO_GNS1
	JB    ACC.BMR_DBG,GO_GNS3
	ANL   A,#7FH
	MOVX  @DPTR,A
GO_GNS1:RET
GO_GNS3:%LDR45i(CI_GNS)
	JMP   GO_VG

GO_GNSALL:
	MOV   R1,#0
GO_GNSA1:CALL GO_GNS
	INC   R1
	MOV   A,R1
	CJNE  A,#REG_N,GO_GNSA1
	RET

; *******************************************************************
; Generator pripraveneho sumu do regulatoru

; inicializace a beh
CI_GNR:	JNB   ACC.BMR_DBG,CD_GNR8
	SETB  MR_FLGA.BMR_BSY	; Busy
	%LDR45i(CD_GNR)
	CALL  CI_VG
CD_GNR:	JNB   FL_MRMC,CD_GNR8
	CALL  CD_GNMRA		; DPTR=MRMC_AP
	JC    CD_GNR8
	MOV   A,#2
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#3
	MOVC  A,@A+DPTR
	MOV   R5,A              ; R45 rychlost z bufferu
	MOV   R6,#0
	JNB   ACC.7,CD_GNR6	; rozsirit na R456
	DEC   R6
CD_GNR6:%MR_OFS2DP(OMR_RPI)	; OMR_RPI=OMR_RPI+R456
	MOVX  A,@DPTR
	ADD   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R6
	MOVX  @DPTR,A
	%MR_OFS2DP(OMR_RSI)	; OMR_RSI=R45
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	JMP   VR_REG
CD_GNR8:CLR   MR_FLGA.BMR_BSY
	%LDR45i(VR_REG)
CI_GNR9:CALL  CI_VG
	JMP   VR_REG

; start
GO_GNR:	CALL  GO_PRPC		; odpojit generator
	JB    F0,GO_GNR1
	JB    ACC.BMR_DBG,GO_GNR3
	ANL   A,#7FH
	MOVX  @DPTR,A
GO_GNR1:RET
GO_GNR3:JB    ACC.BMR_ENR,GO_GNR7
	CALL  GO_RSFT		; beznarazove pripojeni regulatoru
GO_GNR7:%LDR45i(CI_GNR)
	JMP   GO_VG

GO_GNRALL:
	MOV   R1,#0
GO_GNRA1:CALL GO_GNR
	INC   R1
	MOV   A,R1
	CJNE  A,#REG_N,GO_GNRA1
	RET

; *******************************************************************
; System prevodniku

RSEG	MARS__X

ADC0:	DS    2
ADC1:	DS    2
ADC2:	DS    2
ADC3:	DS    2
ADC4:	DS    2
ADC5:	DS    2
ADC6:	DS    2
ADC7:	DS    2

RSEG	MARS__C

ADC_D:	MOV   B,ADCON
	JNB   B.4,ADC_DR
	MOV   A,B
	ANL   A,#7
	RL    A
	ADD   A,#LOW  ADC0
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH ADC0
	MOV   DPH,A
	MOV   A,B
	ANL   A,#0C0H
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,ADCH
	MOVX  @DPTR,A
	MOV   A,B
	ANL   A,#7
	INC   A
	JNB   ACC.3,ADC_S2
	ANL   ADCON,#NOT 10H
ADC_DR:	RET

ADC_S:  CLR   A
	MOV   B,ADCON
	JB    B.3,ADC_DR
ADC_S2:	ANL   A,#07H
	MOV   ADCON,A
	SETB  ACC.3
	MOV   ADCON,A
	RET

; *******************************************************************
; Komunikace IIC

%IF(%WITH_IIC)THEN(

; Typ v 1. byte IIC zpravy
; zpravy pro motory
IIC_CMD	EQU   40H	; Vnejsi prikazy
IIC_CMM	EQU   41H	; Prikaz pro motor
IIC_STM	EQU   42H	; Status motoru
; zpravy pro terminal
IIC_TEC	EQU   51H	; Rizeni IIC klavesnice a displaye
IIC_TEK	EQU   52H	; Informace o stisnutych klavesach
IIC_TED	EQU   53H	; Zobrazovani dat na display

; Adresa jednotky
IIC_ADR	EQU   10H

RSEG	MARS__X

SLAV_BL EQU   100
IIC_INP:DS    SLAV_BL
IIC_OUT:DS    SLAV_BL
IIC_BUF:DS    SLAV_BL

RSEG	MARS__C

INI_IIC:MOV   S1ADR,#IIC_ADR
	CALL  IIC_PRE
	MOV   S1CON,#01000000B	; S1CON pro X 24.000 MHz
	%LDR45i(IIC_INP)
	%LDR67i(SL_CMIC)
	MOV   R2,#SLAV_BL OR 080H
	CALL  IIC_SLX
	RET

SL_CM_R:JMP   SL_JRET

; Zpracovavani prikazu z IIC pod prerusenim
; registry  R0  .. funkce SJ_TSTA,SJ_TEND,SJ_RSTA,SJ_REND
;           R12 .. ukazatel na data
;           R3  .. pocet byte do konce S_BLEN
;	    S_DP   .. ukazatel na zacatek bufferu
;	    S_RLEN .. pro SJ_REND jiz naplnen poctem prijatych byte

SL_CMIC:CJNE  R0,#SJ_REND,SL_CM_R
	PUSH  DPL
	PUSH  DPH
	MOV   DPL,S_DP		; Bufer s prijmutymi daty
	MOV   DPH,S_DP+1
	MOVX  A,@DPTR		; Prijmuty prikaz
	INC   DPTR
   %IF(%WITH_IICKB)THEN(
	CJNE  A,#IIC_TEK,SL_CM20
	MOV   A,S_RLEN		; Kody stisknutych klaves
	DEC   A			; Delka dat prikazu
	MOV   R3,A
	JZ    SL_CM19
SL_CM15:MOVX  A,@DPTR
	INC   DPTR
	MOV   R4,DPL
	MOV   R5,DPH
	CALL  KB_KPUSH		; Ulozit kod klavesy
	MOV   DPL,R4
	MOV   DPH,R5
	DJNZ  R3,SL_CM15
SL_CM19:JMP   SL_CM_A
   )FI
SL_CM20:
   %IF(%WITH_IICCM)THEN(
	CJNE  A,#IIC_CMD,SL_CM30
	JMP   CM_SL		; Rizeni motoru pres IIC
   )FI
SL_CM30:
	JMP   SL_CM_N

SL_CM_A:CLR   ICF_SRC
	SETB  AA
SL_CM_N:MOV   R0,#SJ_REND
	POP   DPH
	POP   DPL
	JMP   SL_JRET

)FI

; *******************************************************************
; Rizeni motoru pres IIC

%IF(%WITH_IICCM)THEN(

RSEG	MARS__X

CM_RTD:	DS    1		; Pocet motoru, ktere je treba zkontrolovat
CM_ACR:	DS    1		; Aktualni motor

RSEG	MARS__C

; K prikazu v ACC nastavi MOT_UCM a ofset na data v R2

SET_CMF:CJNE  A,#(SET_CMTE-SET_CMT)/2,SET_CM1
SET_CM1:JNC   SET_CM9
SET_CM2:RL    A
	MOV   R2,A
	ADD   A,#SET_CMT-SET_CM3
	MOVC  A,@A+PC
SET_CM3:XCH   A,R2
	ADD   A,#SET_CMT-SET_CM4+1
	MOVC  A,@A+PC
SET_CM4:MOV   R3,A
	CLR   C
	RET
SET_CM9:SETB  C
	RET

SET_CMT:DB    OMR_LAP+80H,0	  ; 0 .. Read actual position v LS
	DB    OMR_LRP+80H,0	  ; 1 .. Pozadovana pozice v LS
	DB    OMR_LRP+80H,CMM_GEP ; 2 .. Pozadovana pozice v LS + MI
	DB    OMR_FLG	 ,CMM_HH  ; 3 .. Hard home
	DB    OMR_P	 ,CMM_RPR ; 4 .. Regulator P
	DB    OMR_I	 ,CMM_RPR ; 5 .. Regulator I
	DB    OMR_D	 ,CMM_RPR ; 6 .. Regulator D
	DB    OMR_ME	 ,CMM_RPR ; 7 .. Max energie
	DB    OMR_MS	 ,CMM_RPR ; 8 .. Max rychlost
	DB    OMR_ACC	 ,CMM_RPR ; 9 .. Max zrychleni
	DB    OMR_SCM	 ,CMM_CFG ; A .. Multiplikacni konstanta
	DB    OMR_SCD	 ,CMM_CFG ; B .. Delici konstanta
	DB    0		 ,CMM_CFG ; C .. Minimalni poloha v LS
	DB    0		 ,CMM_CFG ; D .. Maximalni poloha v LS
	DB    OMR_CFG	 ,CMM_CFG ; E .. Priznaky
	DB    OMR_FLG	 ,0	  ; F .. Status
SET_CMTE:

)FI

%IF(%WITH_IICCM)THEN(

; Zpracovavani prijate zpravy rizeni motoru
; IIC SJ_REND IIC_CMD
; vyuziti registru : R1 .. motor, R2 .. offset, R3 .. maska

CM_SL:  PUSH  B
	MOV   A,S_RLEN		; Delka dat prikazu
	DEC   A
	JZ    CM_SL70v		; Delka 1 -> pouze centralni status
	MOVX  A,@DPTR	; prikaz
	INC   DPTR
	CALL  SET_CMF
	JC    CM_SL75v		; Chybny prikaz
	MOV   A,S_RLEN
	ADD   A,#-2
	JNZ   CM_SL20		; Delka >2 -> cislo motoru
CM_SL10:MOV   A,R3
	JZ    CM_SL70v
	MOV   R0,#REG_N		; Prikaz na vsechny motory
	MOV   DPTR,#REG_A+OMR_CMF
CM_SL12:MOVX  A,@DPTR
	ORL   A,R3
	MOVX  @DPTR,A
	MOV   A,DPL
	ADD   A,#REG_LEN
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#0
	MOV   DPH,A
	DJNZ  R0,CM_SL12
	SETB  CMF_NR
	SETB  CMF_IPR
CM_SL70v:JMP  CM_SL70
CM_SL75v:JMP  CM_SL75
CM_SL20:MOVX  A,@DPTR		; cislo motoru
	INC   DPTR
	MOV   R1,A
	ADD   A,#-REG_N
	JC    CM_SL75v
	CJNE  R2,#OMR_FLG,CM_SL30
	MOV   A,R2		; Status
	CALL  MR_GPA1
	MOVX  A,@DPTR
	MOV   DPTR,#IIC_OUT+2
	CALL  CM_F2STAT
	SJMP  CM_SL70v
CM_SL30:MOV   A,S_RLEN
	ADD   A,#-3
	JNZ   CM_SL40		; Delka >3 -> prijem hodnoty
	MOV   A,R2
	ANL   A,#07FH		; Pouze vyslani hodnoty
	CALL  MR_GPA1
	MOV   B,R2
	SJMP  CM_SL50
CM_SL40:MOVX  A,@DPTR		; Nacteni hodnoty z IIC_INP
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   A,S_RLEN
	ADD   A,#-6
	JC    CM_SL42		; Delka >=6 -> long cislo
	MOV   A,R5
	MOV   C,ACC.7
	CLR   A
	SUBB  A,#0
	MOV   R6,A
	MOV   R7,A
	SJMP  CM_SL43
CM_SL42:INC   DPTR
	MOVX  A,@DPTR
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
CM_SL43:MOV   A,R3
	JZ    CM_SL44
	MOV   A,#OMR_CMF	; Nastaveni pozadavku
	CALL  MR_GPA1
	MOVX  A,@DPTR
	ORL   A,R3
	MOVX  @DPTR,A
	SETB  CMF_NR
	SETB  CMF_IPR
CM_SL44:MOV   A,R2
	ANL   A,#07FH		; Adresa do MR
	CALL  MR_GPA1
	MOV   B,R2
	MOV   A,R4		; Ulozeni hodnoty do MR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   A,#-1
	JNB   B.7,CM_SL45
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	MOV   A,#-3
CM_SL45:ADD   A,DPL
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#-1
	MOV   DPH,A
CM_SL50:MOVX  A,@DPTR		; Nacteni hodnoty z MR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	JNB   B.7,CM_SL55
	MOVX  A,@DPTR
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
	SJMP  CM_SL56
CM_SL55:MOV   C,ACC.7
	CLR   A
	SUBB  A,#0
	MOV   R6,A
	MOV   R7,A
CM_SL56:MOV   DPTR,#IIC_OUT+2	; Ulozeni hodnoty do IIC_OUT
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
CM_SL70:MOV   A,MR_FLG		; Centralni status
	MOV   DPTR,#IIC_OUT
	CALL  CM_F2STAT
CM_SL72:POP   B
	JMP   SL_CM_A
CM_SL75:MOV   DPTR,#IIC_OUT	; Chybny prikaz nebo motor
	MOV   A,#0FFH
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	SJMP  CM_SL72

CM_F2STAT:
	MOVX  @DPTR,A
	INC   DPTR
	MOV   B,A
	CLR   A
	MOV   C,B.BMR_BSY
	MOV   ACC.0,C		; Busy
	ORL   C,CMF_IPR
	MOV   ACC.7,C		; Busy systemu
	MOV   C,B.BMR_ERR
	MOV   ACC.3,C		; Error
	MOVX  @DPTR,A
	RET

)FI

%IF(%WITH_IICCM)THEN(

CM_LOOP:JNB   CMF_NR,CM_LP05
	CLR   CMF_NR
	MOV   A,#REG_N
	MOV   DPTR,#CM_RTD
	MOVX  @DPTR,A
CM_LP05:MOV   DPTR,#CM_ACR
	MOVX  A,@DPTR
	MOV   R1,A
	MOV   A,#OMR_CMF
	CALL  MR_GPA1
	MOVX  A,@DPTR
	JZ    CM_LP60
	MOV   R0,A
	MOV   DPTR,#CM_RQT
	CALL  CM_RQFN
	JZ    CM_LP60
	MOV   A,#REG_N
	MOV   DPTR,#CM_RTD
	MOVX  @DPTR,A
	RET
; Pouze zobrazit
CM_LP60:CALL  CMR_MRPl		; Prepocet aktualni polohy
	MOV   A,#OMR_LAP
	CALL  MR_GPA1
	MOV   C,ES1
	CLR   ES1
	CALL  xSVl
	MOV   ES1,C
CM_LP80:MOV   DPTR,#CM_ACR	; Prechod na nasledujici motor
	MOVX  A,@DPTR
	INC   A
	CJNE  A,#REG_N,CM_LP82
	CLR   A
CM_LP82:MOVX  @DPTR,A
	MOV   DPTR,#CM_RTD	; Vyhodnoceni CMF_IPR
	MOVX  A,@DPTR
	JZ    CM_LP89
	DEC   A
	MOVX  @DPTR,A
	JNZ   CM_LP89
	CLR   EA
	JB    CMF_NR,CM_LP88
	CLR   CMF_IPR
CM_LP88:SETB  EA
CM_LP89:RET

; Hledani funkce pro pozadavek
CM_RQF1:INC   DPTR
	INC   DPTR
CM_RQFN:MOVX  A,@DPTR
	JZ    CM_RQFR
	INC   DPTR
	ANL   A,R0
	JZ    CM_RQF1
	CPL   A			; Nalezena funkce
	MOV   R0,A
	MOVX  A,@DPTR
	PUSH  ACC
	INC   DPTR
	MOVX  A,@DPTR
	PUSH  ACC
	MOV   A,#OMR_CMF
	CALL  MR_GPA1
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	ANL   A,R0		; Zruzeni priznaku
	MOVX  @DPTR,A
	MOV   EA,C
CM_RQFR:RET

CM_RQNUL:
	MOV  A,#1
	RET

CM_RQT:	DB    CMM_GEP
	%W    (CM_GEP)
	DB    CMM_HH
	%W    (GO_HH)
	DB    CMM_RPR
	%W    (CM_RQNUL)
	DB    CMM_CFG
	%W    (CM_RQNUL)
	DB    0

CM_GEP:	MOV   A,#OMR_LRP
	CALL  MR_GPA1
	MOV   C,ES1
	CLR   ES1
	CALL  xLDl
	MOV   ES1,C
	MOV   R3,#0
	JMP   CMW_MRPl

CM_INI:	CLR   CMF_IPR
	CLR   CMF_NR
	CLR   A
	MOV   DPTR,#CM_RTD
	MOVX  @DPTR,A
	MOV   DPTR,#CM_ACR
	MOVX  @DPTR,A
	RET
)FI

; *******************************************************************
; Prace s pameti EEPROM 8582

%IF(%WITH_IIC)THEN(

EEP_ADR	EQU   0A0H	; Adresa EEPROM na IIC sbernici
C_S1CON EQU   11000000B ; 11000001B ; Pocatecni stav S1CON

EEA_RD	EQU   1		; akce cteni
EEA_WR	EQU   2		; akce zapisu

RSEG	MARS__D

RSEG	MARS__X

EE_CNT:	DS    1		; citac byte pro prenos z/do EEPROM
EE_PTR:	DS    2		; ukazatel do EE_MEM na data
EEP_TAB:DS    2		; popis dat ulozenych v bloku EEPROM

EE_MEM:	DS    100H	; Buffer pameti EEPROM

RSEG	MARS__C

XOR_SUM:CLR   A
XOR_SU1:MOV   R3,A
	MOVX  A,@DPTR
	INC   DPTR
	XRL   A,R3
	INC   A
	DJNZ  R2,XOR_SU1
	RET

; Cteni pameti EEPROM
;	DPTR .. pointer na buffer
;	R2   .. pocet ctenych byte
;	R4   .. adresa, od ktere se cte

EE_RD:	MOV   A,DPL
	MOV   R0,DPH
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A		; cilova adresa
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	MOV   DPTR,#IIC_BUF
	MOV   A,R4		; adresa v pameti EEPROM
	MOVX  @DPTR,A
	MOV   DPTR,#EE_CNT
	MOV   A,R2		; pocet prenasenych byte
	MOVX  @DPTR,A
	CALL  IIC_WME		; cekat na ukonceni prenosu
EE_RD1:	%LDR45i(IIC_BUF)
	MOV   R2,#1
	MOV   R3,#010H
	MOV   R6,#EEP_ADR
	CALL  IIC_RQX		; pozadavek na IIC
	JNZ   EE_RD1
	CALL  IIC_WME		; cekat na ukonceni prenosu
	JNZ   EE_RDR
	MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR		; cilova adresa
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	%LDR23i(IIC_BUF+1)
	%LDR01i(10H)
	CALL  xxMOVE
	MOV   DPTR,#EE_PTR
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   DPTR,#IIC_BUF
	MOVX  A,@DPTR		; nova adresa v EEPROM
	ADD   A,#10H
	MOVX  @DPTR,A
	MOV   DPTR,#EE_CNT
	MOVX  A,@DPTR
	ADD   A,#-10H-1
	INC   A
	MOVX  @DPTR,A
	JC    EE_RD1
EE_RDR:	RET

; Zapis do pameti EEPROM
;	DPTR .. pointer na buffer
;	R2   .. pocet zapisovanych byte
;	R4   .. adresa, od ktere se zapisuje

EE_WRAO EQU   4

EE_WR:	MOV   A,DPL
	MOV   R0,DPH
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A		; zdrojova adresa
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	MOV   DPTR,#IIC_BUF
	MOV   A,R4		; adresa v pameti EEPROM
	MOVX  @DPTR,A
	MOV   DPTR,#EE_CNT
	MOV   A,R2		; pocet prenasenych byte
	MOVX  @DPTR,A
	CALL  IIC_WME		; cekat na ukonceni prenosu
EE_WR1:	MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR		; zdrojova adresa
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	%LDR45i(IIC_BUF+1)
	%LDR01i(EE_WRAO)
	CALL  xxMOVE
	MOV   DPTR,#EE_PTR
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
EE_WR2:	%LDR45i(IIC_BUF)
	MOV   R2,#EE_WRAO+1
	MOV   R3,#0
	MOV   R6,#EEP_ADR
	CALL  IIC_RQX
	JNZ   EE_WR2
	CALL  IIC_WME
	JNZ   EE_WR2
	MOV   DPTR,#IIC_BUF
	MOVX  A,@DPTR		; adresa v EEPROM
	ADD   A,#EE_WRAO
	MOVX  @DPTR,A
	MOV   DPTR,#EE_CNT
	MOVX  A,@DPTR		; pocitani prenasenych byte
	ADD   A,#-EE_WRAO-1
	INC   A
	MOVX  @DPTR,A
	JC    EE_WR1
EE_WRR:	RET

)FI

%IF(%WITH_IIC)THEN(

; R45 := [EE_PTR++]
EEA_RDi:MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOV   R0,DPH
	MOV   A,DPL
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	RET

; [EE_PTR++] := R45
EEA_WRi:MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   R0,DPH
	MOV   A,DPL
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	RET

; provadi EEA_RD, EEA_WR pro iteger cislo
EEA_Mi:	CJNE  R0,#EEA_RD,EEA_Mi5
	CALL  EEA_RDi
	MOV   DPL,R2
	MOV   DPH,R3
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	RET
EEA_Mi5:CJNE  R0,#EEA_WR,EEA_Mi9
	MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  EEA_WRi
EEA_Mi9:RET

EEA_PRO:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#EE_PTR
	MOV   A,#LOW EE_MEM
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH EE_MEM
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
EEA_PR2:MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	ORL   A,R4
	JZ    EEA_Mi9
	PUSH  DPL
	PUSH  DPH
	MOV   A,R0
	PUSH  ACC
	CALL  JMPR45
	POP   ACC
	MOV   R0,A
	POP   DPH
	POP   DPL
	JMP   EEA_PR2

JMPR45:	MOV   A,R4
	PUSH  ACC
	MOV   A,R5
	PUSH  ACC
	RET

; Nastavi EEP_TAB a DPTR na R23
EEP_PTS:MOV   DPTR,#EEP_TAB
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	MOV   DPL,R2
	MOV   DPH,R3
	RET

; Nastavi
; R2=[EEP_TAB]   .. delka prenasenych dat
; R4=[EEP_TAB+1] .. pocatecni adresa EEPROM
; DPTR=EE_MEM    .. adresa bufferu pameti
EEP_GM:	MOV   DPTR,#EEP_TAB
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R2
	MOV   DPH,A
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   DPTR,#EE_MEM
	RET

; Ulozit data podle tabulky R23
EEP_WRS:CALL  EEP_PTS
	INC   DPTR
	INC   DPTR	; popis akci
	MOV   R0,#EEA_WR
	CALL  EEA_PRO
	CALL  EEP_GM
	DEC   R2
	CALL  XOR_SUM
	MOVX  @DPTR,A		; kontrolni byte
	CALL  EEP_GM
	JMP   EE_WR

; Nacist data podle tabulky R23
EEP_RDS:CALL  EEP_PTS
	CALL  EEP_GM
	CALL  EE_RD
	JNZ   EEP_RD9
	CALL  EEP_GM
	DEC   R2
	CALL  XOR_SUM
	MOV   R3,A
	MOVX  A,@DPTR		; kontrolni byte
	XRL   A,R3
	JNZ   EEP_RD9
	MOV   DPTR,#EEP_TAB
	CALL  xMDPDP
	INC   DPTR
	INC   DPTR	; popis akci
	MOV   R0,#EEA_RD
	JMP   EEA_PRO
EEP_RD9:SETB  F0
	RET

)FI

; *******************************************************************
; Ukladani a cteni nastaveni z EEPROM

%IF(%WITH_IIC)THEN(

; Nacti parametry regulatoru
; R2 cislo regulatoru
EEA_MR:	MOV   A,R2	; cislo regulatoru
	MOV   R1,A
	CJNE  R0,#EEA_RD,EEA_MR5
	MOV   R2,#EEA_MRT-EEA_MR3
EEA_MR1:MOV   A,R2
	INC   R2
	MOVC  A,@A+PC
EEA_MR3:JZ    EEA_MR9
	MOV   R3,A
	CALL  EEA_RDi
	MOV   A,R3
	CALL  MR_GPA1
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	JMP   EEA_MR1
EEA_MR5:CJNE  R0,#EEA_WR,EEA_MR9
	MOV   R2,#EEA_MRT-EEA_MR8
EEA_MR6:MOV   A,R2
	INC   R2
	MOVC  A,@A+PC
EEA_MR8:JZ    EEA_MR9
	CALL  MR_GPA1
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  EEA_WRi
	JMP   EEA_MR6
EEA_MR9:RET

EEA_MRT:; parametry regulatoru RP_NUM*2B
	DB    OMR_P, OMR_I, OMR_D
	DB    OMR_S1, OMR_S2, OMR_ME, OMR_MS, OMR_ACC
	; dalsi parametry
	DB    OMR_CFG, OMR_SCM, OMR_SCD
	; joystick
	DB    OMR_JCO, OMR_JCR, OMR_JCH
	DB    0

; Tabulky popisu akci pro ulozeni a cteni dat

; Tabulka ukladanych parametru
EEC_MR:	DB    060H	; pocet byte ukladanych dat
	DB    010H	; pocatecni adresa v EEPROM

	%W    (COM_TYP)
	%W    (EEA_Mi)

	%W    (COM_ADR)
	%W    (EEA_Mi)

	%W    (COM_SPD)
	%W    (EEA_Mi)

	%W    (0)
	%W    (EEA_MR)

    %IF(%REG_COUNT GE 2) THEN(
	%W    (1)
	%W    (EEA_MR)

    %IF(%REG_COUNT GE 3) THEN(
	%W    (2)
	%W    (EEA_MR)
    )FI )FI

    %IF (%WITH_F_SEL) THEN (
	%W    (TI_SFRQ)
	%W    (EEA_Mi)
    )FI

	%W    (0)
	%W    (0)

MR_EERD:%LDR23i(EEC_MR)
	JMP   EEP_RDS

MR_EEWR:%LDR23i(EEC_MR)
	JMP   EEP_WRS

)FI

; *******************************************************************
; Konfigurace komunikaci RS232 a uLAN

;FL_ECUL	Povolit komunikaci uLAN
;FL_ECRS	Povolit komunikaci RS232

RSEG	MARS__X

COM_TYP:DS    2		; Typ komunikace
COM_ADR:DS    2		; Adresa jednotky
COM_SPD:DS    2		; Rychlost komunikace

RSEG	MARS__C

COM_INI:CLR   ES
	CLR   FL_ECUL
	CLR   FL_ECRS
	MOV   DPTR,#COM_TYP
	MOVX  A,@DPTR
	DJNZ  ACC,COM_I50
	; Bude se pouzivat uLAN
%IF(%WITH_ULAN)THEN(
	SETB  FL_ECUL
	CALL  I_U_LAN
	CALL  uL_OINI
)FI
	RET
COM_I50:DJNZ  ACC,COM_I60
	; Bude se pouzivat RS232
COM_I51:SETB  FL_ECRS
	CALL  COM_DIV
	MOV   R7,A
	CALL  RS232_INI
	SETB  PS
COM_RS_OPL:
	%LDMXi(RS_OPL,RS_OPL1)
	RET
COM_I60:DJNZ  ACC,COM_I99
	; RS232 s jednim stopbitem
	CALL  COM_I51
	ANL   SCON,#07FH
	RET
COM_I99:RET

COM_DIVT:	; Tabulka delitelu frekvence
	DB    BAUDDIV_9600*8	;  1200
	DB    BAUDDIV_9600*4	;  2400
	DB    BAUDDIV_9600*2	;  4800
	DB    BAUDDIV_9600	;  9600
	DB    BAUDDIV_9600/2	; 19200
	DB    BAUDDIV_9600/3	; 28800
	DB    0

; Vraci v ACC divisor pro danou rychlost
COM_DIV:MOV   DPTR,#COM_SPD
	MOVX  A,@DPTR
	MOV   DPTR,#COM_DIVT
	MOVC  A,@A+DPTR
	RET

; Zmeni rychlost komunikace
COM_SPDCH:
	MOV   DPTR,#COM_SPD
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	MOV   DPTR,#COM_DIVT
	MOVC  A,@A+DPTR
	MOV   DPTR,#COM_SPD
	JNZ   COM_SC1
	MOVX  @DPTR,A
COM_SC1:INC   DPTR
	CLR   A
	MOVX  @DPTR,A
	JMP   COM_RQINI

UW_COMi:
	CALL  UW_Mi
	JMP   COM_RQINI

COM_WR23:
	MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	JMP   UB_A_WR

COM_TYPCH:
	MOV   DPTR,#COM_TYP
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	ADD   A,#-4
	JNC   COM_TC1
	CLR   A
	MOVX  @DPTR,A
COM_TC1:INC   DPTR
	CLR   A
	MOVX  @DPTR,A
COM_RQINI:
	SETB  FL_REFR
%IF (0) THEN (
	MOV   R2,#GL_COMCH
	MOV   R3,#0
	JMP   GLOB_RQ23
)ELSE(
	JMP   COM_INI
)FI

%IF(%WITH_ULAN)THEN(
I_U_LAN:CALL  COM_DIV
	MOV   R0,#1
	CALL  uL_FNC	; Rychlost
	MOV   DPTR,#COM_ADR
	MOVX  A,@DPTR
	MOV   R0,#2
	CALL  uL_FNC	; Adresa
	MOV   R2,#0
	MOV   R0,#3
	CALL  uL_FNC	; Delka IB OB
	MOV   R2,#0
	MOV   R0,#4
	CALL  uL_FNC	; Rychle bloky
	MOV   R0,#0
	CALL  uL_FNC	; Start
	RET
)FI

; *******************************************************************

xPR_AD: MOV   A,#' '
	CALL  LCDWR
	MOV   R1,#3
	SJMP  xPRThl1

xPRThl:	MOV   R1,#4
xPRThl1:MOV   A,R1
	DEC   A
	MOVC  A,@A+DPTR
	CALL  PRINThb
	DJNZ  R1,xPRThl1
	RET

DB_W_10:MOV   R0,#10H
	SJMP  DB_WAI1

DB_WAIT:MOV   R0,#0H
DB_WAI1:%WATCHDOG
	DJNZ  R1,DB_WAI1
	DJNZ  R0,DB_WAI1
	RET

L0:	MOV   SP,#80H
	SETB  EA
    %IF(%WITH_IICKB)THEN(
	JNB   FL_DIPR,L002
    )FI
	MOV   DPTR,#DEVER_T
	CALL  cPRINT
	CALL  DB_WAIT
L002:
	%LDMXi(COM_TYP,2)	; Nastaveni parametru komunikace
	%LDMXi(COM_ADR,3)	; Adresa 3
	%LDMXi(COM_SPD,3)	; Rychlost 9600

	CALL  IRC_INIT		; Inicializace CF32006
	CALL  MR_INIS		; Inicializace multi regu

    %IF(%WITH_IIC)THEN(
	CALL  MR_EERD		; Nacteni parametru z EEPROM
      %IF (%WITH_F_SEL) THEN (
	CALL  TI_FSL		; Nastaveni vzorkovaci frekvence
      )FI
    )FI
	CALL  COM_INI		; Odstartovani komunikace

	JNB   FL_ECRS,L010
    %IF(%WITH_IICKB)THEN(
	JNB   FL_DIPR,L010
    )FI
	MOV   A,#K_PROG
	CALL  TESTKEY
	JZ    L_IHEX
L010:
    %IF(%WITH_IICKB)THEN(
	JNB   FL_DIPR,L012
    )FI
	MOV   A,#K_DP
	CALL  TESTKEY
	JZ    L1
L012:
    %IF(NOT %WITH_CUSTHOOK)THEN(
	JMP   UT
    )ELSE(
	JMP   CUSTHOOK_UT
    )FI

L_IHEX:	CALL  IHEXLD
	SJMP  L_IHEX

L1:
	MOV   A,#LCD_CLR
	CALL  LCDWCOM
	MOV   A,#LCD_HOM
	CALL  LCDWCOM

	MOV   DPTR,#REG_A+OREG_A+OMR_AP
	CALL  xPR_AD

	MOV   DPTR,#REG_A+OREG_B+OMR_AP
	MOV   DPTR,#REG_A+OREG_A+OMR_RS
	CALL  xPR_AD

	MOV   A,#LCD_HOM+40H
	CALL  LCDWCOM

	MOV   DPTR,#ADC0		; Prevodniky
	CALL  xLDR45i
	CALL  PRINThw

	MOV   DPTR,#ADC6
	CALL  xLDR45i
	CALL  PRINThw

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#ADC7
	CALL  xLDR45i
	CALL  PRINThw
%IF (0) THEN (
	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#REG_A+OREG_A+OMR_FOD
	CALL  xLDR45i
	CALL  PRINThw

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#REG_A+OREG_A+OMR_FOI
	CALL  xLDR45i
	CALL  PRINThw

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#REG_A+OREG_A+OMR_RP
	CALL  xLDR45i
	CALL  PRINThw
) FI

L2:	CALL  SCANKEY
	JZ    L3
	MOV   R7,A
	MOV   DPTR,#SFT_D1
	CALL  SEL_FNC
L3:

	JMP   L1


T_RDVAL:MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR
	PUSH  ACC
	INC   DPTR
	MOVX  A,@DPTR
	PUSH  ACC
	INC   DPTR
	CALL  cPRINT
	MOV   R6,#8
	MOV   R7,#40H
	CALL  INPUTi
	POP   DPH
	POP   DPL
	JB    F0,T_RDV99
	CALL  xSVR45i
T_RDV99:RET

SFT_D1:	DB    K_END
	%W    (0)
	%W    (L0)

	DB    K_H_C
	%W    (0)
	%W    (GO_JCTT)

	DB    K_H_C
	%W    (0)
	%W    (TM_GEP)

	DB    K_H_A
	%W    (T_PWM0)
	%W    (T_RDVAL)

	DB    K_H_B
	%W    (T_PWM1)
	%W    (T_RDVAL)

	DB    K_6
	%W    (0)
	%W    (UT)

	DB    K_DP
	%W    (0)
	%W    (MONITOR)

	DB    0

T_PWM0:	%W    (0FFE0H)
	DB    LCD_CLR,'PWM0 :',0

T_PWM1:	%W    (0FFE1H)
	DB    LCD_CLR,'PWM1 :',0

DEVER_T:DB    LCD_CLR,'%VERSION'
	DB    C_LIN2 ,' (c) PiKRON 1997',0

; *******************************************************************
; Logika trigru

%IF (%WITH_TRIG) THEN (

; CTCON		N3,P3,N2,P2,N1,P1,N0,P0
; TM2IR		T2OV, CM 2,1,0, CTI 3,2,1,0

OTG_LEN	SET   0
%STRUCTM(OTG,MSK,1)	; Maska zdroje drigru
%STRUCTM(OTG,CFG,1)	; Konfigurace trigru
%STRUCTM(OTG,DIGI,2)	; Stav digitalnich vstupu
%STRUCTM(OTG,DIGO,2)	; Co nastavit na vystupy
%STRUCTM(OTG,SAS,1)	; Ktere motory zastavit a zobrazit
%STRUCTM(OTG,AP,3*REG_N); Stav snimacu IRC

RSEG	MARS__X

TRIG1:	DS    OTG_LEN
TRIG2:	DS    OTG_LEN
TRIGSBU:DS    OTG_LEN	; Kopie trigru pro vyslani

RSEG	MARS__C

; Ulozi data a provede akce odpovidajici
; triggeru, na ktery ukazuje DPTR
; Smi byt volano pouze z urovne preruseni DO_REG
TG_OCC:	INC   DPTR
	MOVX  A,@DPTR
	MOV   B,A
	INC   DPTR
    %IF (%WITH_IO_CM) THEN (
	CALL  G_DIGI	; stav vstupu do OTG_DIGI
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR	; OTG_DIGO pozadovany stav vystupu
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	JNB   B.7,TG_OCC2
	MOV   R2,DPL
	MOV   R3,DPH
	CALL  S_DIGO
	MOV   DPL,R2
	MOV   DPH,R3
    )ELSE(
	INC   DPTR
	INC   DPTR
	INC   DPTR
	INC   DPTR
    )FI
TG_OCC2:MOV   R1,#0
	MOV   MR_BAS,#LOW REG_A
	MOV   MR_BAS+1,#HIGH REG_A
	MOVX  A,@DPTR	; zastavit motory podle OTG_SAS
	ANL   A,#0FH
	INC   DPTR
TG_OCC4:RRC   A
	PUSH  ACC
	MOV   R2,DPL
	MOV   R3,DPH
	JNC   TG_OCC6
	MOV   DPL,MR_BAS; Zastavit generator pohybu
	MOV   DPH,MR_BAS+1
	MOVX  A,@DPTR
	JB    ACC.7,TG_OCC6 ; Prave probiha zmena odjinud
	ANL   A,#NOT (MMR_ENG OR MMR_BSY)
	MOVX  @DPTR,A
	%MR_OFS2DP(OMR_RS)
	CLR   A
	MOV   R0,#OMR_P-OMR_RS
TG_OCC5:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,TG_OCC5
TG_OCC6:CALL  MR_RDAP	; R567=IRC, R4=0
	MOV   DPL,R2
	MOV   DPH,R3
	MOV   A,R5
	MOVX  @DPTR,A	; Ulozit aktualni polohu
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,MR_BAS
	ADD   A,#REG_LEN
	MOV   MR_BAS,A
	JNC   TG_OCC8
	INC   MR_BAS+1
TG_OCC8:INC   R1
	POP   ACC
	CJNE  R1,#REG_N,TG_OCC4
	RET

)FI
%IF (%WITH_TRIG) THEN (

; Zkontroluje triggery
TG_CHCK:JBC  FL_TRIG1,TG_CHC1
	JBC  FL_TRIG2,TG_CHC2
	CLR  A
	RET
TG_CHC1:%LDR23i(TRIG1)
	CALL  TG_CHCS
	SETB  EA
	CLR   FL_TRIG1
	MOV   R1,#0
	SJMP  TG_SEND
TG_CHC2:%LDR23i(TRIG2)
	CALL  TG_CHCS
	SETB  EA
	CLR   FL_TRIG2
	MOV   R1,#1
	SJMP  TG_SEND
TG_CHCS:%LDR45i(TRIGSBU)
	%LDR01i(OTG_LEN)
	CLR   EA
	JMP   xxMOVE

; Vysle informace o trigru z TRIGSBU
; R1 cislo trigru
TG_SEND:MOV   DPTR,#TRIGSBU
	CALL  S_APTR
	MOV   DPTR,#RS_TMP
	MOV   A,#'T'
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#'G'
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	ADD   A,#'0'
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#'!'
	MOVX  @DPTR,A
	INC   DPTR
    %IF(1)THEN(
	CALL  TG_SNU4
	MOV   A,R5
	JZ    TG_SND6
	MOV   DPTR,#RS_TMP
    )FI
	CALL  X_APTR		; Vypsat digitalni vstupy
	MOV   A,#OTG_CFG
	MOVC  A,@A+DPTR
	MOV   B,A		; B=OTG_CFG
	MOV   A,#OTG_DIGI
	CALL  ADDATDP
	CALL  xLDR45i
	MOV   A,#OTG_SAS-OTG_DIGI
	CALL  ADDATDP
	MOVX  A,@DPTR
	SWAP  A
	ANL   A,#0FH
	PUSH  ACC
	INC   DPTR
	CALL  X_APTR
	JB    B.6,TG_SND3
	MOV   A,#'N'
	MOVX  @DPTR,A
	INC   DPTR
	CALL  TG_SNU4
	SJMP  TG_SND4
TG_SND3:MOV   R2,#0	; delka vypisu
	MOV   R3,#000H	; SLZxxDDD
	CALL  TG_SNU3
TG_SND4:MOV   B,#REG_N		; Vypsat polohy
	POP   ACC
TG_SND5:PUSH  B
	RRC   A
	PUSH  ACC
	CALL  TG_SNU1
	POP   ACC
	POP   B
	DJNZ  B,TG_SND5
TG_SND6:MOV   DPTR,#RS_TMP
	CLR   A
	MOVX  @DPTR,A
	CALL  RS_WRL1
	RET

TG_SNU1:CALL  L_APTR
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R6,A
	INC   DPTR
	MOV   R7,#0
	JNB   ACC.7,TG_SNU2
	DEC   R7
TG_SNU2:CALL  S_APTR
	JNC   TG_SNU9
	MOV   DPTR,#RS_TMP
	MOV   A,#','
	MOVX  @DPTR,A
	INC   DPTR
	MOV   R2,#0	; delka vypisu
	MOV   R3,#0C3H	; SLZxxDDD
TG_SNU3:CALL  xITOAF
TG_SNU4:CLR   F0
	CLR   A
	MOVX  @DPTR,A
	MOV   DPTR,#RS_TMP
TG_SNU5:MOVX  A,@DPTR
	JZ    TG_SNU9
	PUSH  DPL
	PUSH  DPH
	MOV   R0,A
	CALL  RS_WR
	POP   DPH
	POP   DPL
	JB    F0,TG_SNU9
	INC   DPTR
	SJMP  TG_SNU5
TG_SNU9:RET

)FI
%IF (%WITH_TRIG) THEN (

; Nastavi TRIG[MR_RSNU] podle RS_TMP
RS_STG:	MOV   DPTR,#RS_TMP
	CALL  xLDl		; R45 - CFG, R67 - SAS
	CALL  xLDR23i		; R23 - DIGO
	MOV   DPTR,#MR_RSNU
	MOVX  A,@DPTR
	MOV   B,#OTG_LEN
	MUL   AB
	ADD   A,#LOW (TRIG1+OTG_MSK)
	MOV   DPL,A
	MOV   A,B
	ADDC  A,#HIGH (TRIG1+OTG_MSK)
	MOV   DPH,A
	CLR   A
	MOVX  @DPTR,A		; OTG_MSK = 0
	MOV   A,R5
	JB    ACC.7,RS_STG9
	PUSH  DPL
	PUSH  DPH
    %IF(0)THEN(
	MOV   A,#OTG_CFG-OTG_MSK
	CALL  ADDATDP
    )ELSE(
	INC   DPTR
    )FI
	MOV   A,R4
	MOVX  @DPTR,A		; OTG_CFG = R4
	MOV   A,#OTG_DIGO-OTG_CFG
	CALL  ADDATDP
	MOV   A,R2
	MOVX  @DPTR,A		; OTG_DIGO = R23
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	INC   DPTR
    %IF(0)THEN(
	MOV   A,#OTG_SAS-OTG_DIGO-2
	CALL  ADDATDP
    )FI
	MOV   A,R6
	MOVX  @DPTR,A		; OTG_SAS = R6
	POP   DPH
	POP   DPL
	MOV   A,R4
	ANL   A,#0FH
	RL    A
	MOV   R2,A
	ADD   A,#RS_STGt-RS_STGk
	MOVC  A,@A+PC
RS_STGk:XCH   A,R2
	ADD   A,#RS_STGt-RS_STGl+1
	MOVC  A,@A+PC
RS_STGl:MOV   B,R4
	JNB   B.4,RS_STG4
	ORL   CTCON,A
	RL    A
	JB    B.5,RS_STG5
	CPL   A
	ANL   CTCON,A
	SJMP  RS_STG6
RS_STG4:JNB   B.5,RS_STG6
	CPL   A
	ANL   CTCON,A
	CPL   A
	RL    A
RS_STG5:ORL   CTCON,A
RS_STG6:MOV   A,R2
	CPL   A
	ANL   TM2IR,A		; Vymazat priznak trigu
	MOV   A,R2
	MOVX  @DPTR,A		; Spustit trig
RS_STG9:RET

RS_STGt:DB    1,001H	; CTI
	DB    2,004H
	DB    4,010H
	DB    8,040H
	DB    10H,0	; CM
	DB    20H,0
	DB    40H,0
	DB    80H,0	; T2OV
)FI

; *******************************************************************
; Komparatory polohy

%IF (%WITH_CMPP) THEN (

CP_CCNT	DATA  MR_BAS	; Pro pruchody smyckou komparatoru
CP_HELP	DATA  MR_BAS+1	; Kontrola aktivity

; Jednotlive masky bitu v OCP_FLG

BCP_NHI	EQU   0		; Jeste nebylo OMR_AP>OCP_POS
BCP_NLO	EQU   1		; Jeste nebylo OMR_AP<=OCP_POS
BCP_EVE	EQU   2		; Doslo k prejeti OCP_POS
BCP_LST EQU   3		; Blokovat dalsi zbyvajici komparatory
BCP_SDO EQU   4		; Set digital out
BCP_REP EQU   5		; Opakovane spoustet komparator

MCP_NHI	EQU   1 SHL BCP_NHI
MCP_NLO	EQU   1 SHL BCP_NLO
MCP_EVE	EQU   1 SHL BCP_EVE
MCP_LST	EQU   1 SHL BCP_LST
MCP_REP	EQU   1 SHL BCP_REP

OCP_LEN	SET   0
%STRUCTM(OCP,FLG,1)	; Konfigurace a priznaky komparatoru
%STRUCTM(OCP,REG,1)	; Cislo komparovane osy
%STRUCTM(OCP,POS,4)	; Komparovana poloha
%STRUCTM(OCP,DIGI,2)	; Stav digitalnich vstupu
%STRUCTM(OCP,DIGO,2)	; Co nastavit na vystupy
%STRUCTM(OCP,REPO,4)	; Repeat offset

CP_NUM	SET   4

)FI
%IF (%WITH_CMPP) THEN (

RSEG	MARS__X

; Pole komparatoru
CP_ARR:	DS    CP_NUM*OCP_LEN

RSEG	MARS__C

; Kontroluje podminky komparatoru
; Smi byt volano pouze z urovne preruseni DO_REG
CP_DO:	MOV   CP_CCNT,#CP_NUM
	MOV   CP_HELP,#0
	MOV   DPTR,#CP_ARR

CP_DO10:MOVX  A,@DPTR	; OCP_FLG
	ANL   A,#MCP_NHI OR MCP_NLO
	JZ    CP_DO30		; Neceka se na nic
	ORL   CP_HELP,#1
	MOV   A,#OCP_REG
	MOVC  A,@A+DPTR
	PUSH  DPL		; Komparace polohy
	PUSH  DPH
	MOV   R1,A
	MOV   A,#OMR_AP
	CALL  MR_GPA1
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A	; R567 = OMR_AP
	POP   DPH
	POP   DPL
	MOVX  A,@DPTR	; OCP_FLG
	MOV   B,A
	CLR   C
	MOV   A,#OCP_POS+0
	MOVC  A,@A+DPTR
	SUBB  A,R5
	MOV   A,#OCP_POS+1
	MOVC  A,@A+DPTR
	SUBB  A,R6
	MOV   A,#OCP_POS+2
	MOVC  A,@A+DPTR
	SUBB  A,R7
	MOV   C,OV
	XRL   A,PSW		; ACC.7=1 <=> OMR_AP>OCP_POS
	JB    ACC.7,CP_DO22
	MOV   C,B.BCP_NHI
	JBC   B.BCP_NLO,CP_DO24
	SJMP  CP_DO28
CP_DO22:MOV   C,B.BCP_NLO
	JBC   B.BCP_NHI,CP_DO24
	SJMP  CP_DO28
CP_DO24:JNC   CP_DO50		; Nastala udalost
	MOV   A,B
	MOVX  @DPTR,A
CP_DO28:JB    B.BCP_LST,CP_DO33
CP_DO30:MOV   A,#OCP_LEN
	ADD   A,DPL
	MOV   DPL,A
	CLR   A
	ADDC  A,DPH
	MOV   DPH,A
	DJNZ  CP_CCNT,CP_DO10
CP_DO33:MOV   A,CP_HELP
	JNZ   CP_DO39
	CLR   FL_CPAC		; Zadny aktivni komparator
CP_DO39:RET

CP_DO50:SETB  B.BCP_EVE		; Nastala udalost
	MOV   A,B
	MOVX  @DPTR,A
	SETB  FL_CPEV
    %IF (%WITH_IO_CM) THEN (
	MOV   A,#OCP_DIGI
	CALL  ADDATDP
	CALL  G_DIGI
	MOV   A,R4
	MOVX  @DPTR,A		; OCP_DIGI
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR		; OCP_DIGO
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	JNB   B.BCP_REP,CP_DO70
	INC   DPTR		; Priprava opakovani udalosti
	MOVX  A,@DPTR		; OCP_REPO
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R1,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   A,DPL
	ADD   A,#-(OCP_REPO+3)
	MOV   DPL,A
	JC    CP_DO67
	DEC   DPH
CP_DO67:MOV   A,B
	ORL   A,#MCP_NHI OR MCP_NLO
	CLR   ACC.BCP_LST
	MOVX  @DPTR,A		; OCP_FLG
	INC   DPTR
	INC   DPTR
	MOVX  A,@DPTR		; OCP_POS += OCP_REPO
	ADD   A,R0
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R1
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R3
	MOVX  @DPTR,A
CP_DO70:JNB   B.BCP_SDO,CP_DO75
	CALL  S_DIGO		; Rusi DPTR
CP_DO75:
    )FI
	RET

)FI
%IF (%WITH_CMPP) THEN (

; Vraci v DPTR pocatek OCP struktury podle MR_RSNU
; Rusi ACC, B
RS_SCN2DP:
	MOV   DPTR,#MR_RSNU
	MOVX  A,@DPTR
	MOV   B,#OCP_LEN
	MUL   AB
	ADD   A,#LOW CP_ARR
	MOV   DPL,A
	MOV   A,B
	ADDC  A,#HIGH CP_ARR
	MOV   DPH,A
	RET

; Nastavi CMP_ARR[MR_RSNU] podle RS_TMP
; RS_TMP+0  Konfigurace
; RS_TMP+2  Porovnavana osa
; RS_TMP+4  Poloha
; RS_TMP+8  Digo

RS_SCP:	MOV   DPTR,#RS_TMP+2
	MOVX  A,@DPTR
	ADD   A,#-'A'
	MOV   R1,A
	ADD   A,#-REG_N
	JC    RS_SCPE
	MOV   DPTR,#RS_TMP+4	; Poloha
	CALL  xLDl
	MOV   R0,#3		; meritko SCL_MR z logickych na motor
	CALL  SCL_MR
	MOV   DPTR,#RS_TMP+8	; Digitalni vystupy
	CALL  xLDR23i
	MOV   DPTR,#RS_TMP+0	; Konfigurace
	MOVX  A,@DPTR
	MOV   R0,A
	CALL  RS_SCN2DP		; DPTR podle MR_RSNU
	CLR   FL_CPAC
	MOV   A,R0
	MOVX  @DPTR,A		; OCP_FLG
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A		; OCP_REG
	INC   DPTR
	MOV   A,R2
	PUSH  ACC
	CALL  xSVl		; OCP_POS
	POP   ACC
	INC   DPTR
	INC   DPTR
	MOVX  @DPTR,A		; OCP_DIGO
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	SETB  FL_CPAC
	MOV   A,#1
	RET
RS_SCPE:SETB  F0
	RET

; Nastavi CMP_ARR[MR_RSNU].OCP_REPO podle RS_TMP
; RS_TMP+4  Poloha

RS_SCRP:MOV   DPTR,#RS_TMP+4	; Poloha
	CALL  xLDl
	CALL  RS_SCN2DP		; DPTR podle MR_RSNU
	MOV   A,#OCP_REG
	MOVC  A,@A+DPTR		
	MOV   R1,A
	MOV   R0,#3		; meritko SCL_MR z logickych na motor
	CALL  SCL_MR
	CALL  RS_SCN2DP		; DPTR podle MR_RSNU
	MOV   A,#OCP_REPO
	CALL  ADDATDP
	CLR   FL_CPAC
	CALL  xSVl
	SETB  FL_CPAC
	MOV   A,#1
	RET

RSI_CHAR:MOVX A,@DPTR
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
	INC   DPTR
	PUSH  DPL
	PUSH  DPH
	CALL  L_APTR
	CALL  SKIPSPC
	MOV   R4,A
	INC   DPTR
	CALL  S_APTR
	MOV   A,R6
	ORL   A,R7
	JZ    RSI_CH2
        MOV   DPL,R6
	MOV   DPH,R7
	MOV   A,R4
	MOVX  @DPTR,A
RSI_CH2:POP   DPH
	POP   DPL
	MOV   A,R4
	JZ    RS_SCPE
	JMP   RS_DOCM
)FI
%IF (%WITH_CMPP) THEN (

CP_CHCK:MOV   DPTR,#CP_ARR
	MOV   R1,#0
	MOV   R2,#CP_NUM
	CLR   FL_CPEV
CP_CHC1:CLR   EA
	MOVX  A,@DPTR		; OCP_FLG
	JBC   ACC.BCP_EVE,CP_CHC2
	SETB  EA
	INC   R1
	MOV   A,#OCP_LEN
	CALL  ADDATDP
	DJNZ  R2,CP_CHC1
	CLR   A
	RET

CP_CHC2:MOVX  @DPTR,A
	SETB  EA
	SETB  FL_CPEV	; R1 cislo komparatoru
	MOV   A,#OCP_DIGI
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OCP_DIGI+1
	MOVC  A,@A+DPTR
	MOV   R5,A	; R45 hodnota vstupu
	MOV   DPTR,#RS_TMP
	MOV   A,#'C'
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#'M'
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#'P'
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	ADD   A,#'0'	; Cislo komparatoru
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#'!'
	MOVX  @DPTR,A
	INC   DPTR
    %IF (%WITH_IO_CM) THEN (
	MOV   R2,#0	; delka vypisu
	MOV   R3,#000H	; SLZxxDDD
	CALL  xITOAF
    )ELSE(
	MOV   A,#'N'
	MOVX  @DPTR,A
	INC   DPTR
    )FI
	CLR   F0
	CLR   A
	MOVX  @DPTR,A
	MOV   DPTR,#RS_TMP
	CALL  RS_WRL1
	MOV   A,#1
	RET
)FI

; *******************************************************************
; Prikazy pro DIG a ADC vstupy/vystupy

; Digitalni vystupy
;   P4.0 az 3
;   AUX_POR.4 az 7
;   CS7-C3.0 az 6

%IF (%WITH_IO_CM) THEN (
AUX_POR	XDATA 0FFA0H	; adresa portu AUX bity 4 az 7
			; bity 0 az 3 musi byt 101
DIGO2_A	XDATA 0FFE3H	; na desce AA_PPW

RSEG	MARS__X

AUX_BUF:DS    2

RSEG	MARS__C

; Nacte do R45 stav digitalnich vstupu
G_DIGI: MOV   A,P1	; znacky motoru A (P1.0) a B (P1.1)
	MOV   C,P1.5	; P1.2 a P1.3
	MOV   ACC.4,C	; AUX input 1
	MOV   C,P4.6
	MOV   ACC.5,C	; AUX input 2
	MOV   C,P3.4
	MOV   ACC.6,C	; INV_P1
	MOV   C,P1.4
	MOV   ACC.7,C	; INV_P2
	MOV   R4,A
	MOV   R5,P5	; ADC
	RET

; Nastavi digitalni vystupy podle R45
S_DIGO:
    %IF(%REG_COUNT LE 3) THEN(
	MOV   A,R4
	SWAP  A
	ANL   A,#0F0H
	ORL   A,#1101B	; !!!!! konfigurace CF32006
	MOV   DPTR,#AUX_POR
	MOVX  @DPTR,A	; AUX
    )ELSE(
      %IF(%REG_COUNT EQ 6) THEN(
	MOV   A,R4	; !!!!! Pro MARS_6AX
	MOV   C,ACC.0
	MOV   P3.4,C	; INV_P1
	MOV   C,ACC.1
	MOV   P1.4,C	; INV_P2
      )FI
    )FI
	MOV   A,R4
	SWAP  A
	ANL   A,#00FH
	ORL   P4,A
	ORL   A,#0F0H	; Vystupy na krokac
	ANL   P4,A
	MOV   A,R5
	MOV   DPTR,#DIGO2_A
	MOVX  @DPTR,A
	MOV   C,ACC.6
	MOV   P4.4,C	; INV_E
	RET
)FI

; *******************************************************************
; Hardwarove delici a casovaci funkce

%IF (%WITH_HW_DTFN) THEN (

RSEG	MARS__D

HWDIV_P:DS    2		; Perioda hardwarove delicky

RSEG	MARS__C

S_HWDIV:MOV   A,R4
	ANL   A,#NOT 15
	ORL   A,R5
	JNZ   S_HWDI5
	CLR   ET0
	CLR   TR0
	MOV   HWDIV_P,#0
	MOV   HWDIV_P+1,#0
	RET
S_HWDI5:CLR   EA
	MOV   HWDIV_P,R4
	MOV   HWDIV_P+1,R5
	SETB  EA
	SETB  PT0
	%VECTOR(TIMER0,I_HWDIV)
	MOV   A,#05H		; 16 bit counter 0
	ORL   TMOD,A
	ORL   A,#NOT 0FH
	ANL   TMOD,A
	CLR   TR0
	CLR   C
	CLR   A
	SUBB  A,HWDIV_P+0
	MOV   TL0,A
	CLR   A
	SUBB  A,HWDIV_P+1
	MOV   TH0,A
	SETB  TR0
	SETB  ET0
	RET

I_HWDIV:PUSH  PSW
	PUSH  ACC
	CLR   C
	CLR   TR0
	MOV   A,TL0
	SUBB  A,HWDIV_P+0
	MOV   TL0,A
	MOV   A,TH0
	SUBB  A,HWDIV_P+1
	MOV   TH0,A
	SETB  TR0
	XRL   P4,#10H		; P4.4
	POP   ACC
	POP   PSW
	RETI

)FI

; *******************************************************************
; RS232 pro MARS

%*DEFINE (RS_TID(NAME)) (
RST_%NAME:DB '%NAME',0
)

%*DEFINE (RS_TIDSUF(NAME)) (
RST_%(%NAME)_SUF:DB '%NAME#',0
)

%*DEFINE (RSD_NEW (NAME)) (
RSD_P	SET   RSD_T
RSD_T	SET   $
	DB   LOW (RSD_P),HIGH (RSD_P)
	DB   LOW (RST_%NAME),HIGH (RST_%NAME)
)

RSEG	MARS__X
MR_RSNU:
MR_RSAM:DS    1

TMP_MSPC:
RS_TMP:	DS    20

%IF(%WITH_MRDY)THEN(
RS_MRDYM:DS   1		; Maska cekani na Rm!
)FI

RSEG	MARS__C

; Nastavi MR_RSAM podle posledniho
; znaku prikazu 'A','B', ...
RS_SAMA:CALL  RS_ACKL
RS_SAM:	MOV   A,R6
	ADD   A,#-'A'
	CJNE  A,#REG_N,RS_SAM1
RS_SAM1:JC    RS_SAM3
RS_SAM2:SETB  F0
	RET
; Nastavi MR_RSAM podle posledniho
; znaku prikazu '0','1' az uvedene max
RS_SNUA:CALL  RS_ACKL
RS_SNU: MOVX  A,@DPTR
	INC   DPTR
	XCH   A,R6
	ADD   A,#-'0'
	SUBB  A,R6
	JNC   RS_SAM2
	ADDC  A,R6
RS_SAM3:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#MR_RSAM
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
	JMP   RS_DOCM

; Skip to delimitter character
RS_SDLM:MOVX  A,@DPTR
	MOV   R3,A
	INC   DPTR
	CALL  X_APTR
	CALL  SKIPSPC
	XRL   A,R3
	JNZ   RS_SRDYE
	INC   DPTR
	CALL  X_APTR
	JMP   RS_DOCM

RSO_VER_T:DB  'VER=%VERSION',0

RSO_VER:MOV   DPTR,#RSO_VER_T
	JMP   RS_WRL1

RSO_TEST_T:DB 'TEST OK',0
RSO_TEST:MOV  DPTR,#RSO_TEST_T
	JMP   RS_WRL1

; Zapina/vypina ohlas READY/FAIL
RS_SRDY:MOV   A,R4
	ADD   A,#-'1'-1
	JNC   RS_SRDY1
RS_SRDYE:SETB F0
	RET
RS_SRDY1:ADD  A,#1
	MOV   RSF_RDYR,C
	SETB  RSF_BSYO
	CLR   FL_RDYR1
	RET

; Vysli pouze jedno ready
RS_RDYR1:SETB RSF_RDYR
	SETB  RSF_BSYO
	SETB  FL_RDYR1
	RET

%IF(%WITH_MRDY)THEN(
; Vylat jedno Rm! az bude motor v klidu
RS_MRDY:MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R4,#0FFH
	CALL  TSTBR4A
	MOV   R4,A	; 1 << MR_RSAM
	MOV   DPTR,#RS_MRDYM
	MOVX  A,@DPTR
	ORL   A,R4
	MOVX  @DPTR,A
	CLR   FL_MRDYT	; S vyslanim pockat do
	RET		; nasledujiciho DO_REG

RS_MRDYC:CLR  A
	JNB   FL_MRDYT,RS_MRC9
	CLR   FL_MRDYT
	MOV   DPTR,#RS_MRDYM
	MOVX  A,@DPTR
	JZ    RS_MRC9
	MOV   R4,A
	MOV   R5,#001H
	MOV   R6,#'A'
	MOV   DPTR,#REG_A+OMR_FLG
	SJMP  RS_MRC2
RS_MRC1:MOV   A,R4
	JZ    RS_MRC9
	MOV   A,#REG_LEN
	CALL  ADDATDP
	MOV   A,R5
	RL    A
	MOV   R5,A
	INC   R6
RS_MRC2:MOV   A,R4
	ANL   A,R5
	JZ    RS_MRC1
	XRL   A,R4
	MOV   R4,A
	MOVX  A,@DPTR
	JB    ACC.BMR_BSY,RS_MRC1
	MOV   C,ACC.BMR_ERR
	MOV   DPTR,#RS_MRDYM
	MOVX  A,@DPTR
	XRL   A,R5
	MOVX  @DPTR,A
	MOV   DPTR,#RS_TMP
	MOV   A,#'R'		; Rm!
	JNC   RS_MRC3
	MOV   A,#'F'		; FAILm!
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#'A'
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#'I'
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#'L'
RS_MRC3:MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#'!'
	MOVX  @DPTR,A
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A
	MOV   DPTR,#RS_TMP
	CALL  RS_WRL1
	SETB  FL_MRDYT
	MOV   A,#1
RS_MRC9:RET
)FI

; Zapina/vypina ohlas REPLY
RS_SRPLY:MOV  A,R4
	ADD   A,#-'1'-1
	JNC   RS_SRPL1
	SETB  F0
	RET
RS_SRPL1:ADD  A,#1
	MOV   RSF_RPLY,C
	MOV   RSF_LNT,C
	RET

; Blokovani klavesnice
RS_KEYLOCK:
	MOV   A,R4
	ADD   A,#-'1'-1
	JNC   RS_KEYL1
	SETB  F0
	RET
RS_KEYL1:ADD  A,#1
	MOV   R2,#GL_KEYLOCK
	JC    RS_KEYL2
	MOV   R2,#GL_KEYEN
RS_KEYL2:MOV  R3,#0
	JMP   GLOB_RQ23

; Nastaveni debug modu pro regulator
RS_REGDBG:
	MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	MOV   A,#OMR_FLG
	CALL  MR_GPA1
	MOV   A,R4
	ADD   A,#-'1'
	CLR   EA
	MOVX  A,@DPTR
	MOV   ACC.BMR_DBG,C
	MOVX  @DPTR,A
	SETB  EA
	RET

; Cteni historie
RS_REGDBGH:
	%LDMXi(RS_TMP+2,MRMC_BMB)
RS_DBG2:MOV   DPTR,#RS_TMP
	MOVX  A,@DPTR
	ADD   A,#-1
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#-1
	MOVX  @DPTR,A
	JNC   RS_DBG9
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R2,A
	ADD   A,#2
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	ADDC  A,#0
	MOVX  @DPTR,A
	MOV   DPL,R2
	MOV   DPH,R3
	CALL  xLDR45i
	CALL  L_APTR
	MOV   R2,#0
	MOV   R3,#80H
	CALL  xITOAF
	MOV   A,#0DH
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#0AH
	MOVX  @DPTR,A
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A
	CALL  L_APTR
RS_DBG6:MOVX  A,@DPTR
	JZ    RS_DBG2
	MOV   R0,A
	PUSH  DPL
	PUSH  DPH
	CLR   F0
	CALL  RS_WR
	POP   DPH
	POP   DPL
	JB    F0,RS_DBG6
	INC   DPTR
	SJMP  RS_DBG6
RS_DBG9:CLR   RSF_LNT
	CLR   F0
	RET

; Priprava dat pro inedtifikaci
RS_REGDBGP:
	%LDMXi(RS_TMP+2,MRMC_BMB)
RS_DBP2:MOV   DPTR,#RS_TMP
	MOVX  A,@DPTR
	ADD   A,#-1
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#-1
	MOVX  @DPTR,A
	JNC   RS_DBP5
	CLR   C
RS_DBP3:JC    RS_DBP9
	CALL  RS_RDLN
	JZ    RS_DBP3
	MOV   R2,#0
	MOV   R3,#80H
	CALL  xATOIF
	JB    F0,RS_DBP9
	MOV   DPTR,#RS_TMP+2
	MOVX  A,@DPTR
	MOV   R2,A
	ADD   A,#2
	MOV   R0,A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	ADDC  A,#0
	MOVX  @DPTR,A
	XCH   A,R0
	ADD   A,#LOW (-MRMC_EMB)
	MOV   A,R0
	ADDC  A,#HIGH (-MRMC_EMB)
	JC    RS_DBP5
	MOV   DPL,R2
	MOV   DPH,R3
	CALL  xSVR45i
	SJMP  RS_DBP2
RS_DBP5:MOV   DPTR,#RS_TMP+2
	CALL  xMDPDP
	SETB  C
	MOV   A,#LOW MRMC_EMB
	SUBB  A,DPL
	MOV   R2,A
	MOV   A,#HIGH MRMC_EMB
	SUBB  A,DPH
	MOV   R3,A
	JC    RS_DBP8
	INC   R2
	INC   R3
	CLR   A
RS_DBP6:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R2,RS_DBP6
	DJNZ  R3,RS_DBP6
RS_DBP8:CLR   RSF_LNT
	CLR   F0
RS_DBP9:RET

RS_HH:	MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	JMP   GO_HH

RS_CLEAR:MOV  DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	JMP   CLR_GEP

RS_STALL:MOV  R4,MR_FLG
	MOV   R5,#0
	RET

RS_JCT:	MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	JMP   GO_JCTS

RS_RELEASE_ALL:
	JMP   REL_ALL

RS_RELEASE:
	MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	JMP   REL_GEP

RS_STOP:
	MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	JMP   STP_GEP

RS_REBOOT:
	JMP   RESET

%IF (%MR_REG_SEL) THEN (
RS_REGTYPE:
	MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	JMP   REG_SEL
)FI

%IF (%WITH_IO_CM) THEN (
RS_GADC:MOV   DPTR,#MR_RSNU
	MOVX  A,@DPTR
	RL    A
	ADD   A,#LOW  ADC0
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH ADC0
	MOV   DPH,A
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   C,EA
	MOV   R5,A
	RET
)FI

%IF (%WITH_AM_CM) THEN (
EXTRN	CODE(GO_GSP,GO_GSPT)

RSW_SPD:MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	JMP   GO_GSP

RSW_SPDT:MOV  DPTR,#RS_TMP
	CALL  xLDl
	MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	MOV   R2,#3
	JMP   GO_GSPT

RSW_PWM:MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	CLR   A
	CALL  MR_GPA1
	JMP   MR_SENEP
)FI

; -----------------------------------
; Listy prikazu

RS_OPL1:DB    ':'
	%W    (RS_CML1)
	DB    '?'
	%W    (RS_CML2)
	DB    0


; -----------------------------------
; Jmena prikazu

%RS_TIDSUF(G)
%RS_TIDSUF(GR)
%RS_TIDSUF(AP)
%RS_TIDSUF(HH)
%RS_TIDSUF(ST)
%RS_TIDSUF(REGP)
%RS_TIDSUF(REGI)
%RS_TIDSUF(REGD)
%RS_TIDSUF(REGS1)
%RS_TIDSUF(REGS2)
%RS_TIDSUF(REGMS)
%RS_TIDSUF(REGACC)
%RS_TIDSUF(REGME)
%RS_TIDSUF(REGCFG)
%RS_TIDSUF(REGDBG)
%RS_TIDSUF(REGTYPE)
%RS_TIDSUF(CLEAR)
%RS_TIDSUF(STOP)
%RS_TIDSUF(RELEASE)
%RS_TIDSUF(JOYSTICK)
%RS_TIDSUF(JOYOFFS)
%RS_TIDSUF(JOYRES)
%RS_TIDSUF(JOYHYS)
%RS_TID(STOP)
%RS_TID(HH)
%RS_TID(ST)
%RS_TID(PURGE)
%RS_TID(CLEAR)
%RS_TID(RELEASE)
%RS_TID(READY)
%RS_TID(R)
%RS_TID(REPLY)
%RS_TID(KEYLOCK)
%RS_TID(REGDBGHIS)
%RS_TID(REGDBGPRE)
%RS_TID(REGDBGGNS)
%RS_TID(REGDBGGNR)
%RS_TID(JOYSTICK)
%RS_TID(JOYCAL)
%RS_TID(VER)
%RS_TID(IHEXLD)
%RS_TID(REBOOT)
%RS_TID(TEST)

%IF (%WITH_IO_CM) THEN (
%RS_TIDSUF(ADC)
%RS_TID(DIGI)
%RS_TID(DIGO)
)FI

%IF (%WITH_F_SEL) THEN (
%RS_TID(REGSFRQ)
)FI

%IF(%WITH_MRDY)THEN(
%RS_TIDSUF(R)
)FI

%IF (%WITH_TRIG) THEN (
%RS_TIDSUF(TRIG)
)FI

%IF (%WITH_CMPP) THEN (
%RS_TIDSUF(CMP)
%RS_TIDSUF(CMPREPO)
)FI

%IF (%WITH_HW_DTFN) THEN (
%RS_TID(HWDIV)
)FI

%IF (%WITH_AM_CM) THEN (
%RS_TIDSUF(SPD)
%RS_TIDSUF(SPDT)
%RS_TIDSUF(PWM)
)FI

%IF(%WITH_IIC)THEN(
%RS_TID(CFGNVSAVE)
%RS_TID(CFGDEFAULT)
)FI

; -----------------------------------
; Vstupni prikazy

RSD_T	SET   0

%RSD_NEW(TEST)
	%W    (RS_CMDA)
	%W    (RSO_TEST)

%RSD_NEW(REBOOT)
	%W    (RS_REBOOT)

%RSD_NEW(IHEXLD)
	%W    (RS_CMDA)
%IF(%WITH_IICKB)THEN(
	%W    (IHEXLDND)
)ELSE(
	%W    (IHEXLD)
)FI

%IF(%WITH_IIC)THEN(
%RSD_NEW(CFGNVSAVE)
	%W    (RS_CMDA)
	%W    (MR_EEWR)	; EEC_MR

%RSD_NEW(CFGDEFAULT)
	%W    (RS_CMDA)
	%W    (MR_INIS)	; default
)FI

%IF (%WITH_F_SEL) THEN (
%RSD_NEW(REGSFRQ)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (TI_FSLR)	; Funkce
)FI

%IF (%MR_REG_SEL) THEN (
%RSD_NEW(REGTYPE_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RS_REGTYPE); Funkce
)FI

%IF (%WITH_AM_CM) THEN (
%RSD_NEW(SPD_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,080H
	%W    (0)	; Adresa
	%W    (RSW_SPD)	; Funkce

%RSD_NEW(SPDT_SUF)
	%W    (RS_SAMA)
	%W    (RSI_INT)
	DB    0,080H
	%W    (RS_TMP)	; Adresa
	%W    (RS_SDLM)	; Funkce
	DB    ','
	%W    (RSI_INT)
	DB    0,000H
	%W    (RS_TMP+2); Adresa
	%W    (RSW_SPDT); Funkce

%RSD_NEW(PWM_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,080H
	%W    (0)	; Adresa
	%W    (RSW_PWM)	; Funkce
)FI

%IF (%WITH_HW_DTFN) THEN (
%RSD_NEW(HWDIV)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (S_HWDIV); Funkce
)FI

%IF (%WITH_TRIG) THEN (
%RSD_NEW(TRIG_SUF)
	%W    (RS_SNUA)
	DB    1
	%W    (RSI_INT)
	DB    0,080H
	%W    (RS_TMP)	; Adresa
	%W    (RS_SDLM)	; Funkce
	DB    ','
	%W    (RSI_INT)
	DB    0,000H
	%W    (RS_TMP+2); Adresa
	%W    (RS_SDLM)	; Funkce
	DB    ','
	%W    (RSI_INT)
	DB    0,000H
	%W    (RS_TMP+4); Adresa
	%W    (RS_STG)	; Funkce
)FI

%IF (%WITH_CMPP) THEN (
%RSD_NEW(CMP_SUF)
	%W    (RS_SNUA)	; Cislo CMPx
	DB    CP_NUM-1
	%W    (RSI_INT)	; Konfigurace
	DB    0,000H
	%W    (RS_TMP)	; Adresa
	%W    (RS_SDLM)	; Funkce
	DB    ','
	%W    (RSI_CHAR); Porovnavana osa
	%W    (RS_TMP+2); Adresa
	%W    (RS_SDLM)	; Funkce
	DB    ','
	%W    (RSI_INT)	; Poloha
	DB    0,0C3H
	%W    (RS_TMP+4); Adresa
	%W    (RS_SDLM)	; Funkce
	DB    ','
	%W    (RSI_INT)	; Digo
	DB    0,000H
	%W    (RS_TMP+8); Adresa
	%W    (RS_SCP)	; Funkce

%RSD_NEW(CMPREPO_SUF)
	%W    (RS_SNUA)	; Cislo CMPx
	DB    CP_NUM-1
	%W    (RSI_INT)	; Poloha
	DB    0,0C3H
	%W    (RS_TMP+4); Adresa
	%W    (RS_SCRP)	; Funkce
)FI

%RSD_NEW(REGDBGHIS)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (RS_TMP)	; Adresa
	%W    (RS_REGDBGH)

%RSD_NEW(REGDBGPRE)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (RS_TMP)	; Adresa
	%W    (RS_REGDBGP)

%RSD_NEW(REGDBGGNS)
	%W    (RS_CMDA)
	%W    (GO_GNSALL)

%RSD_NEW(REGDBGGNR)
	%W    (RS_CMDA)
	%W    (GO_GNRALL)

%RSD_NEW(JOYCAL)
	%W    (RS_CMDA)
	%W    (JOYCAL_ALL)

%RSD_NEW(JOYOFFS_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    OMR_JCO

%RSD_NEW(JOYRES_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,080H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    OMR_JCR

%RSD_NEW(JOYHYS_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    OMR_JCH

%RSD_NEW(JOYSTICK)
	%W    (RS_CMDA)
	%W    (GO_JCTT)

%RSD_NEW(KEYLOCK)
	%W    (RSI_ONOFA)
	%W    (RS_KEYLOCK)

%IF (%WITH_IO_CM) THEN (
%RSD_NEW(DIGO)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (S_DIGO)	; Funkce
)FI

%RSD_NEW(REPLY)
	%W    (RSI_ONOFA)
	%W    (RS_SRPLY)

%RSD_NEW(READY)
	%W    (RSI_ONOFA)
	%W    (RS_SRDY)

%RSD_NEW(R)
	%W    (RS_CMDA)
	%W    (RS_RDYR1)

%RSD_NEW(HH)
	%W    (RS_CMDA)
	%W    (GO_HHT)

%RSD_NEW(PURGE)
	%W    (RS_CMDA)
	%W    (CER_ALL)

%RSD_NEW(STOP)
	%W    (RS_CMDA)
	%W    (STP_ALL)

%RSD_NEW(CLEAR)
	%W    (RS_CMDA)
	%W    (CLR_ALL)

%RSD_NEW(RELEASE)
	%W    (RS_CMDA)
	%W    (RS_RELEASE_ALL)

%IF(%WITH_MRDY)THEN(
%RSD_NEW(R_SUF)
	%W    (RS_SAMA)
	%W    (RS_MRDY)
)FI

%RSD_NEW(JOYSTICK_SUF)
	%W    (RS_SAMA)
	%W    (RS_JCT)

%RSD_NEW(CLEAR_SUF)
	%W    (RS_SAMA)
	%W    (RS_CLEAR)

%RSD_NEW(RELEASE_SUF)
	%W    (RS_SAMA)
	%W    (RS_RELEASE)

%RSD_NEW(STOP_SUF)
	%W    (RS_SAMA)
	%W    (RS_STOP)

%RSD_NEW(REGDBG_SUF)
	%W    (RS_SAM)
	%W    (RSI_ONOFA)
	%W    (RS_REGDBG)

%RSD_NEW(REGP_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_P)

%RSD_NEW(REGI_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_I)

%RSD_NEW(REGD_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_D)

%RSD_NEW(REGS1_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_S1)

%RSD_NEW(REGS2_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_S2)

%RSD_NEW(REGMS_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_MS)

%RSD_NEW(REGACC_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_ACC)

%RSD_NEW(REGME_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_ME)

%RSD_NEW(REGCFG_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_CFG)

%RSD_NEW(HH_SUF)
	%W    (RS_SAMA)
	%W    (RS_HH)

%RSD_NEW(GR_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,0C3H
	%W    (0)	; Adresa
	%W    (RSW_MRPl); Funkce
	DB    1

%RSD_NEW(G_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,0C3H
	%W    (0)	; Adresa
	%W    (RSW_MRPl); Funkce
	DB    0

RS_CML1	SET   RSD_T

; -----------------------------------
; Vystupni prikazy
RSD_T	SET   0

%RSD_NEW(TEST)
	%W    (RSO_TEST)

%RSD_NEW(VER)
	%W    (RSO_VER)

%IF (%WITH_F_SEL) THEN (
%RSD_NEW(REGSFRQ)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (TI_SFRQ)	; Adresa
	%W    (0);	Funkce
)FI

%IF (%WITH_IO_CM) THEN (
%RSD_NEW(DIGI)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (G_DIGI)	; Funkce

%RSD_NEW(ADC_SUF)
	%W    (RS_SNU)
	DB    7
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RS_GADC)	; Funkce
)FI

%RSD_NEW(JOYOFFS_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_JCO

%RSD_NEW(JOYRES_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,080H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_JCR

%RSD_NEW(JOYHYS_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_JCH

%RSD_NEW(REGP_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_P

%RSD_NEW(REGI_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_I

%RSD_NEW(REGD_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_D

%RSD_NEW(REGS1_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_S1

%RSD_NEW(REGS2_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_S2

%RSD_NEW(REGMS_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_MS

%RSD_NEW(REGACC_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_ACC

%RSD_NEW(REGME_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_ME

%RSD_NEW(REGCFG_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_CFG

%RSD_NEW(ST_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRb)	; Funkce
	DB    OMR_FLG

%RSD_NEW(AP_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,0C3H
	%W    (0)	; Adresa
	%W    (RSR_MRPl); Funkce

%RSD_NEW(ST)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RS_STALL); Funkce

RS_CML2	SET   RSD_T

; *******************************************************************
; Vyblokovano

%IF(%WITH_ULAN)THEN(

RSEG	MARS__B
UD_BBBB:DS    1
UDF_RDP	BIT   UD_BBBB.7

RSEG	MARS__C

uL_OINI:RET
UD_OI:	RET
UD_REFR:RET

; Identifikace typu pristroje
PUBLIC	uL_IDB,uL_IDE
uL_IDB: DB    '.mt %VERSION .uP 51x',0
uL_IDE:

)FI

; *******************************************************************
; Propojeni s regulatory

RSEG	MARS__X

MR_UIAM:DS    2

RSEG	MARS__C

; Deli R7B registrem R3, pak posune R4567B
SCL_S:	MOV   R0,#1
	CLR   A
	XCH   A,R3
	MOV   R2,A
	CLR   C
SCL_S1:	MOV   A,R2	; R32 >>= 1
	RRC   A
	MOV   R2,A
	MOV   A,R3
	RRC   A
	MOV   R3,A
	MOV   A,R7	; R7B - R32 ?
	SUBB  A,R3
	PUSH  ACC
	MOV   A,B
	SUBB  A,R2
	JC    SCL_S3
	MOV   B,A	; R7B > R32
	POP   ACC
	MOV   R7,A
	MOV   A,R0	; R0 = (R0 << 1) | \0
	RLC   A
	MOV   R0,A
	JNC   SCL_S1
	SJMP  SCL_S4
SCL_S3: DEC   SP	; R7B < R32
	MOV   A,R0	; R0 = (R0 << 1) | \1
	RLC   A
	MOV   R0,A
	JNC   SCL_S1
SCL_S4: CPL   A
	XCH   A,R4
	XCH   A,R5
	XCH   A,R6
	XCH   A,R7
	XCH   A,B
SCL_S9:	RET

; Prevod meritka souradnic pro R1
; pro R0.0=0 .. z motoru, R0.0=1 .. na motor
;     R0.1=1 .. zkontrolovat OMR_CFG.10
SCL_MR:	CLR   A
	CALL  MR_GPA1
	MOV   A,R0
	JNB   ACC.1,SCL_MR0
	DEC   R0
	DEC   R0
	MOV   A,#OMR_CFG+1
	MOVC  A,@A+DPTR
	JNB   ACC.2,SCL_MR0
	RET			; OMR_CFG.10 => neprovadet prepocet
SCL_MR0:MOV   A,#OMR_SCM+1	; multiply
	MOVC  A,@A+DPTR
	MOV   B,A
	MOV   A,#OMR_SCM
	MOVC  A,@A+DPTR
	JNB   B.7,SCL_MR1
	CPL   A
	INC   A
SCL_MR1:JZ    SCL_S9
	MOV   R2,A
	MOV   A,#OMR_SCD+1	; divission
	MOVC  A,@A+DPTR
	XRL   B,A
	MOV   C,ACC.7
	MOV   A,#OMR_SCD
	MOVC  A,@A+DPTR
	JNC   SCL_MR2
	CPL   A
	INC   A
SCL_MR2:JZ    SCL_S9
	MOV   R3,A
	DJNZ  R0,SCL_MR3
	XCH   A,R2
	MOV   R3,A
SCL_MR3:JNB   B.7,SCL_MR4
	CLR   A
	CLR   C
	SUBB  A,R4
	MOV   R4,A
        CLR   A
	SUBB  A,R5
	MOV   R5,A
	CLR   A
	SUBB  A,R6
	MOV   R6,A
	CLR   A
	SUBB  A,R7
	MOV   R7,A
; operace R4567*R2/R3
SCL_MR4:MOV   B,R2
	MOV   A,R4
	MUL   AB
	MOV   R4,A
	MOV   A,R2
	XCH   A,B
	XCH   A,R5
	MUL   AB
	ADD   A,R5
	MOV   R5,A
	MOV   A,R2
	XCH   A,B
	ADDC  A,#0
	XCH   A,R6
	MUL   AB
	ADD   A,R6
	MOV   R6,A
	MOV   A,R2
	XCH   A,B
	ADDC  A,#0
	XCH   A,R7
	JB    ACC.7,SCL_MR5
	MOV   R2,#0	; Bez korekce pro kladne cislo
SCL_MR5:MUL   AB
	ADD   A,R7
	MOV   R7,A
	MOV   A,B
	ADDC  A,#0
	SUBB  A,R2
	JNC   SCL_MR61
	ADD   A,R3	; Zaporne cislo
	MOV   B,A
	CALL  SCL_S
	JNZ   SCL_MR8
	MOV   A,R4
	JB    ACC.7,SCL_MR63
	SJMP  SCL_MR8
SCL_MR61:MOV  B,A	; Kladne cislo
	CALL  SCL_S
	JNZ   SCL_MR8
	MOV   A,R4
	JB    ACC.7,SCL_MR8
SCL_MR63:CALL SCL_S
	CALL  SCL_S
	CALL  SCL_S
	MOV   A,B
	CLR   C
	RLC   A
	JC    SCL_MR65
	SUBB  A,R3
	JC    SCL_MR9
SCL_MR65:INC  R4
	MOV   A,R4
	JNZ   SCL_MR9
	INC   R5
	MOV   A,R5
	JNZ   SCL_MR9
	INC   R6
	MOV   A,R6
	JNZ   SCL_MR9
	INC   R7
	MOV   A,R7
	CJNE  A,#80H,SCL_MR9
SCL_MR8:SETB  F0
SCL_MR9:RET


RSR_MRPl:MOV  DPTR,#MR_RSAM
	MOV   R0,#2		; meritko SCL_MR z motoru na logicke
	SJMP  UR_MRPl1		; ridit se i podle OMR_CFG.10

UR_MRPDl:MOV  R0,#2
	SJMP  UR_MRPl0

; Pro komunikaci pres IIC
CMR_MRPl:MOV  R0,#2
	SJMP  UR_MRPl3

; Nacte do R4567 aktualni polohu z regulatoru [DPTR+OU_DP]
UR_MRPl:MOV   R0,#0		; meritko SCL_MR z motoru na logicke
UR_MRPl0:MOV  A,#OU_DP
	MOVC  A,@A+DPTR
	CJNE  A,#-1,UR_MRPl2
	MOV   DPTR,#MR_UIAM
UR_MRPl1:MOVX A,@DPTR
UR_MRPl2:MOV  R1,A
UR_MRPl3:MOV  A,#OMR_AP
	CALL  MR_GPA1
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	SETB  EA
	MOVX  A,@DPTR
	MOV   R6,A
	MOV   R7,#0
	JNB   ACC.7,UR_MRPl5
	DEC   R7
UR_MRPl5:CLR  F0		; R0.0=0 z motoru na logicke
	CALL  SCL_MR		; prevod meritka
	MOV   A,#1
	RET

RSW_MRPl:MOVX A,@DPTR
	MOV   R3,A		; typ pohybu (absolutne/relativne)
	MOV   DPTR,#MR_RSAM
	MOV   R0,#3		; meritko SCL_MR z logickych na motor
	SJMP  UW_MRPl1		; ridit se i podle OMR_CFG.10

; Pro komunikaci pres IIC
CMW_MRPl:MOV  R0,#3
	SJMP  UW_MRPl3

UW_MRPDl:MOV  R0,#3
	SJMP  UW_MRPl0

; Spusti pohyb regulatoru [DPTR+OU_DP] na polohu R4567
UW_MRPl:MOV   R0,#1		; meritko SCL_MR z logickych na motor
UW_MRPl0:MOV  A,#OU_DP+1
	MOVC  A,@A+DPTR         ; typ pohybu (absolutne/relativne)
	MOV   R3,A
	MOV   A,#OU_DP
	MOVC  A,@A+DPTR         ; cislo regulatoru
	CJNE  A,#-1,UW_MRPl2
	MOV   DPTR,#MR_UIAM
UW_MRPl1:MOVX A,@DPTR
UW_MRPl2:MOV  R1,A
UW_MRPl3:
%IF(1) THEN (
	CLR   F0
	MOV   A,R3
	PUSH  ACC		; R0.0=1 z logickych na motor
	CALL  SCL_MR		; prevod meritka
	POP   ACC
	MOV   R3,A
	JB    F0,UW_MRPl5
	MOV   A,R6
	MOV   C,ACC.7
	XCH   A,R7
	ADDC  A,#0
	JNZ   UW_MRPl5
	MOV   A,R5
	MOV   R6,A
	MOV   A,R4
	MOV   R5,A
	MOV   R4,#0
	CALL  GO_GEPT
	RET
UW_MRPl5:SETB F0
	RET
)ELSE(
	MOV   A,R5
	MOV   R7,A
	MOV   A,R4
	MOV   R6,A
	CLR   A
	MOV   R4,A
	MOV   R5,A
	CLR   F0
	CALL  GO_GEPT
	RET
)FI

; Vypocte adresu parametru regulatoru
; [DPTR]    .. offset parametru
; [MR_RSAM] .. urceni regulatoru
MR_RSCA:MOVX  A,@DPTR
	MOV   R0,A
	MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	MOV   A,R0
	JMP   MR_GPA1

; Vypocte adresu parametru regulatoru
; [DPTR+OU_DP+0] .. cislo regulatoru nebo -1 pro reg [MR_UIAM]
; [DPTR+OU_DP+1] .. offset parametru regulatoru
MR_UICA:MOV   A,#OU_DP+1
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OU_DP
	MOVC  A,@A+DPTR
	CJNE  A,#-1,MR_UIC4
	MOV   DPTR,#MR_UIAM
	MOVX  A,@DPTR
MR_UIC4:MOV   R1,A
	MOV   A,R0
	JMP   MR_GPA1

RSR_MRCi:CALL MR_RSCA
	SJMP  UR_MRCi1

UR_MRCi:CALL  MR_UICA
UR_MRCi1:CALL xLDR45i
	MOV   A,#1
	CLR   F0
	RET

RSW_MRCi:CALL MR_RSCA
	SJMP  UW_MRCi1

UW_MRCi:CALL  MR_UICA
UW_MRCi1:CALL xSVR45i
	CLR   F0
	RET

RSR_MRb:CALL  MR_RSCA
	SJMP  UR_MRb1

UR_MRb:	CALL  MR_UICA
UR_MRb1:MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#0
	RET

RSW_MRb:CALL  MR_RSCA
	SJMP  UW_MRb1

UW_MRb:	CALL MR_UICA
UW_MRb1:MOV   A,R4
	MOVX  @DPTR,A
	RET

UW_Mb:	MOV   A,#OU_DP
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OU_DP+1
	MOVC  A,@A+DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOV   A,R4
	MOVX  @DPTR,A
	CLR   F0
	RET

HH_ALL:	MOV   R1,#0
HH_ALL1:CALL  GO_HH
	INC   R1
	MOV   A,R1
	CJNE  A,#REG_N,HH_ALL1
	RET

STP_ALL:MOV   R1,#0
STP_ALL1:CALL STP_GEP
	INC   R1
	MOV   A,R1
	CJNE  A,#REG_N,STP_ALL1
	RET

CLR_ALL:MOV   R1,#0
CLR_ALL1:CALL CLR_GEP
	INC   R1
	MOV   A,R1
	CJNE  A,#REG_N,CLR_ALL1
	CLR   MR_FLG.BMR_ERR
	RET

CER_ALL:MOV   R1,#0
CER_ALL1:CALL CER_GEP
	INC   R1
	MOV   A,R1
	CJNE  A,#REG_N,CER_ALL1
	CLR   MR_FLG.BMR_ERR
	RET

; Odpojeni generatoru a regulatoru
REL_ALL:MOV   R1,#0
REL_ALL1:CALL REL_GEP
	INC   R1
	MOV   A,R1
	CJNE  A,#REG_N,REL_ALL1
	CLR   MR_FLG.BMR_ERR
	RET

; Kalibrace nulove polohy joysticku
JOYCAL_ALL:
	MOV   R1,#0
JOYCAL_ALL1:
	CALL  JCT_CAL
	INC   R1
	MOV   A,R1
	CJNE  A,#REG_N,JOYCAL_ALL1
	RET

; Cte hodnotu ze SDATA
UR_MSPCb:MOV  A,#OU_DP
	MOVC  A,@A+DPTR
	MOV   R1,A
	MOV   DPTR,#TMP_MSPC
	MOV   A,#0E5H         ; MOV  A,dir
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#022H         ; RET
	MOVX  @DPTR,A
	DB    12H	      ; CALL TMP_MSPC
	DW    TMP_MSPC
	MOV   R4,A
	MOV   R5,#0
	CLR   F0
	MOV   A,#1
	RET

%IF (%MR_REG_SEL) THEN (
UW_REGTYPE:
	CALL  MR_UICA
	JMP   REG_SEL
)FI


; *******************************************************************
; Globalni udalosti

; Posle globalni udalost
GLOB_RQ23:MOV A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	MOV   R7,#ET_GLOB
	JMP   EV_POST

; Zpracovani globalnich udalosti
GLOB_DO:MOV   A,R4
	MOV   R7,A
	MOV   A,R5
	MOV   R4,A
	MOV   DPTR,#GLOB_SF1
	JMP   SEL_FNC

; Globalni udalosti

GL_KEYLOCK  SET  1
GL_KEYEN    SET  2

; Tabulka globalnich udalosti

GLOB_SF1:DB   GL_KEYLOCK
	%W    (UT_GR30)
	%W    (GR_RQ23)

	DB    GL_KEYEN
	%W    (UT_GR10)
	%W    (GR_RQ23)

	DB    0

; *******************************************************************
; Komunikace s uzivatelem

RSEG	MARS__X

UT_UIAD:DS    40
UT_DATA:DS    40

RSEG	MARS__C

UT_INIT:; Dvouradkovy display
	CLR   D4LINE
	SETB  FL_CMAV
	MOV   DPTR,#UI_MV_SX
	MOV   A,#20
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#2
	MOVX  @DPTR,A
	; Kontrola pro ctyrradkovy display
    %IF(%WITH_IICKB)THEN(
	JNB   FL_DIPR,UT_INI1
    )FI
	MOV   DPTR,#KBDRD
	MOVX  A,@DPTR
	JB    ACC.7,UT_INI1
	SETB  D4LINE
	MOV   DPTR,#UI_MV_SX
	MOV   A,#20
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#4
	MOVX  @DPTR,A
	; Rozhodnuto
UT_INI1:MOV   DPTR,#UT_UIAD
	MOV   UI_AD,DPL
	MOV   UI_AD+1,DPH
	CLR   A
	MOV   DPTR,#EV_BUF
	MOVX  @DPTR,A
	MOV   DPTR,#GR_ACT
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#REF_TIM
	MOVX  @DPTR,A
%IF(%WITH_IICKB)THEN(
	MOV   A,#75
	CALL  WAIT_T
	MOV   R6,#07CH	; Adresa vetsi IIC klavesnice
	CALL  UI_INIHW	; Inicializace hardware
	JB    FL_IICKB,UT_INI5

	MOV   R6,#07AH	; Adresa IIC klavesnice
	CALL  UI_INIHW	; Inicializace hardware
	JNB   FL_IICKB,UT_INI6

UT_INI5:
	MOV   DPTR,#DEVER_IT	; text
	MOV   R6,#1		; 1. radka
	MOV   R7,#DEVER_IE-DEVER_IT ; pocet znaku
	CALL  KB_IICWRLN
	MOV   A,#75
	CALL  WAIT_T

UT_INI6:
)FI
	RET

%IF(%WITH_IICKB)THEN(
DEVER_IT:DB   LCD_CLR,'%VERSION'
	DB    C_LIN2 ,'PiKRON'
DEVER_IE:
)FI

UT_TREF:MOV   DPTR,#REF_TIM
	MOVX  A,@DPTR
	JNZ   UT_TRE1
	MOV   DPTR,#REF_PER
	MOVX  A,@DPTR
	MOV   DPTR,#REF_TIM
	MOVX  @DPTR,A
	MOV   A,#1
	RET
UT_TRE1:CLR   A
	RET

UT:     CALL  UT_INIT
	MOV   R7,#ET_RQGR
	%LDR45i(UT_GR10)
	CALL  EV_POST

UT_ML:  CALL  EV_GET
	JZ    UT_ML50
	CJNE  R7,#ET_GLOB,UT_ML45
	CALL  GLOB_DO
	SJMP  UT_ML
UT_ML45:CALL  EV_DO
	JMP   UT_ML
UT_ML50:JNB   FL_ECRS,UT_ML53
	CALL  RS_POOL		; Smycka zpracovani prikazu RS232
	JNZ   UT_ML55
    %IF (%WITH_TRIG) THEN (
	CALL  TG_CHCK		; Kontrola trigru
	JNZ   UT_ML55
    )FI
    %IF (%WITH_CMPP) THEN (
	JNB   FL_CPEV,UT_ML51
	CALL  CP_CHCK		; Doslo k udalosti na komparatoru
	JNZ   UT_ML55
UT_ML51:
)FI
    %IF(%WITH_MRDY)THEN(
	CALL  RS_MRDYC		; Pro vysilani Rm!
	JNZ   UT_ML55
    )FI
	MOV   A,MR_FLG
	MOV   C,ACC.BMR_ERR
	ANL   A,#MMR_BSY
	MOV   ACC.7,C
	CALL  RS_RDYSND		; Vysila R! a FAIL! po RS232
	JZ    UT_ML55
	JNB   FL_RDYR1,UT_ML55
	CLR   RSF_RDYR		; Nepokracuj v informaci o stavu
	SJMP  UT_ML55
UT_ML53:
    %IF(%WITH_ULAN)THEN(
	JNB   FL_ECUL,UT_ML55
	CALL  UD_OI
	JB    uLF_INE,UT_ML55
	JB    UDF_RDP,UT_ML57
    )FI
UT_ML55:CALL  UT_TREF
	JZ    UT_ML60
    %IF(%WITH_ULAN)THEN(
	JNB   FL_ECUL,UT_ML57
	CALL  UD_REFR
UT_ML57:CLR   UDF_RDP
    )FI
	SETB  FL_REFR
	SJMP  UT_ML65
UT_ML60:
    %IF(%WITH_IICCM)THEN(
	CALL  CM_LOOP
    )FI
	CALL  UT_ULED
UT_ML65:JMP   UT_ML

UT_ULED:
;       MOV   DPTR,#IP1_ST+1
;	MOV   R2,#1 SHL LFB_PUMP1
;	CALL  UT_USTS	; CALL  UT_UST1

	MOV   A,MR_FLG
	MOV   R2,#10H
	CALL  UT_MRST
	MOV   DPTR,#REG_A+OREG_A+OMR_FLG
	MOVX  A,@DPTR
	MOV   R2,#G_A_LMSK
	CALL  UT_MRST
%IF(%REG_COUNT GE 2) THEN(
	MOV   DPTR,#REG_A+OREG_B+OMR_FLG
	MOVX  A,@DPTR
	MOV   R2,#G_B_LMSK
	CALL  UT_MRST
%IF(%REG_COUNT GE 3) THEN(
	MOV   DPTR,#REG_A+OREG_C+OMR_FLG
	MOVX  A,@DPTR
	MOV   R2,#G_C_LMSK
	CALL  UT_MRST
)FI )FI
	JMP   LEDWR

UT_MRST:JB    ACC.BMR_ERR,UT_UST7
	JB    ACC.BMR_BSY,UT_UST3
	SJMP  UT_UST5

UT_USTS:MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
UT_UST1:MOV   A,R5
	JB    ACC.7,UT_UST7
	ORL   A,R4
UT_UST2:JZ    UT_UST5
UT_UST3:MOV   A,R2		; On
	CPL   A
	ANL   LEB_FLG,A
	CPL   A
	ORL   LED_FLG,A
	RET
UT_UST5:MOV   A,R2		; Off
	CPL   A
	ANL   LEB_FLG,A
	ANL   LED_FLG,A
	RET
UT_UST7:MOV   A,R2		; Error
	ORL   LEB_FLG,A
	RET

; Nastavi MR_UIAM na hodnotu v R2
UT_SAMG:%LDR45i(UT_GR20)
	MOV   R7,#ET_RQGR
	CALL  EV_POST
UT_SAM:	MOV   A,R2
	MOV   DPTR,#MR_UIAM
	MOVX  @DPTR,A
	RET

; Cte rozsireny stav motoru
; vraci: R4   priznaky
;        R5   .0 .. mark  .1 joystick
UR_STEX:CALL  MR_UICA
	MOV   A,#OMR_FLG
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_VG
	MOVC  A,@A+DPTR
	XRL   A,#HIGH CD_JCT
	MOV   R0,A
	MOV   A,#OMR_VG+1
	MOVC  A,@A+DPTR
	XRL   A,#LOW  CD_JCT
	ORL   A,R0
	MOV   R5,#0
	JNZ   UR_STE1
	MOV   R5,#2
UR_STE1:MOV   A,R1
	MOV   R0,A
	INC   R0
	MOV   A,P1
UR_STE2:RRC   A
	DJNZ  R0,UR_STE2
	JNC   UR_STE3
	INC   R5
UR_STE3:CLR   F0
	RET

; Souhrny stav
UR_STALL:
	MOV   R4,MR_FLG
	MOV   R5,#0
	CLR   F0
	RET

; *******************************************************************
; Konkretni zakaznicka aplikace

$INCLUDE(%CUST_APPL_CODE)

END
