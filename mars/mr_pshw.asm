$NOMOD51
;********************************************************************
;*              Multiosa regulace  - MR_PSHW.ASM                    *
;*                    Modul rizeni polohy - casti zavisle na HW     *
;*                  Stav ke dni 10. 8.1997                          *
;*                      (C) Pisoft 1996                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_AL)
$INCLUDE(%INCH_REGS)
$INCLUDE(%INCH_MR_DEFS)
$INCLUDE(%INCH_MR_PDEFS)
$LIST

%DEFINE (USE_KLIKLO_FL)	(1)	; Pouzit presne vzorkovani rychlosti
%DEFINE (USE_MARK_CTI_FL)(1)	; Pouzit zachyty znacky otacky
%DEFINE (WITH_MARK_NEG) (0)	; Negovana polarita znacek
%DEFINE (USE_DEBUG_OUT)	(0)	; Po dobu preruseni P4.6=1
%DEFINE (WITH_HH_LIMSW)	(1)	; S informaci o dojeti do kraje

EXTRN	NUMBER(REG_LEN,REG_A,REG_N)
EXTRN	BIT(FL_MRCE)
EXTRN	DATA(MR_BAS,MR_FLG,MR_FLGA)
EXTRN	CODE(MR_REG,DO_REGDBG)

PUBLIC	IRC_INIT,IRC_RD,IRC_RDP,IRC_WR,IRC_WRP,HH_MARK
PUBLIC	MR_SENE,MR_SENEP
PUBLIC	DO_REG,VR_REG,VR_REG1,VR_RET

MR____C SEGMENT CODE

; *******************************************************************
; Cteni polohy z CF32006

AIR_CF0	XDATA 0FF20H	; Baze prvniho CF32006
AIR_CF1	XDATA 0FF40H	; Baze druheho CF32006

			;	       7   6   5   4   3   2   1   0
AIR_CC0	XDATA 0FF80H    ; Konfigurace KL0 R0  M22 M12 M02 M21 M11 M01
AIR_CC1	XDATA 0FFA0H	; Konfigurace KL1 R1  0   0   0   M23 M13 M03
; mody:	16 bit counter	- 0
;	single IRC	- 1 on raisign Ua1n, 2 on raising Ua2n
;	double IRC	- 3 on Ua1n edges , 4 on Ua2n edges
;	quadruple IRC	- 5 an all edges
;	PWM measure	- 6 Ua1n gate, Ua2n=1 count up on CLK, 0=down
;	frequncy measure- 7 Ua1n frequncy signal, Ua2n gate

CIR_C_N	EQU   11101101B ; zakladni konfigurace
CIR_RES EQU   10111111B ; reset

%IF (%USE_KLIKLO_FL) THEN (
BKLIKLO	BIT   P4.5
MKLIKLO	SET   020H
)FI

RSEG	MR____C

IRC_INIT:
CF_INIT:MOV   DPTR,#AIR_CC0
	MOV   A,#CIR_C_N AND CIR_RES
	MOVX  @DPTR,A
	MOV   A,#CIR_C_N
	MOVX  @DPTR,A
	MOV   DPTR,#AIR_CC1
	MOV   A,#CIR_C_N AND CIR_RES
	MOVX  @DPTR,A
	MOV   A,#CIR_C_N
	MOVX  @DPTR,A
%IF (%USE_KLIKLO_FL) THEN (
	CLR   BKLIKLO
	NOP
	SETB  BKLIKLO
	ORL   RTE,#MKLIKLO
)FI
%IF (%USE_MARK_CTI_FL) THEN (
	ANL   CTCON,#11110000B	; Zachyt znacky otacky  P1.0 a P1.1
    %IF (%WITH_MARK_NEG) THEN ( ; v bitech TM2IR CTI0 a CTI1
	ORL   CTCON,#00001010B  ; cekat na setupnou hranu
    )ELSE(
	ORL   CTCON,#00000101B  ; cekat na vzestupnou hranu
    )FI
)FI
	RET

; Nacte polohu do OMR_AP (3B) a vypocita OMR_AS (2B)
IRC_RD:
CF_RD:  MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
IRC_RDP:
CF_RDP:	MOV   A,#OMR_AIR
	MOVC  A,@A+DPTR
	MOV   DPL,A
	MOV   DPH,#HIGH AIR_CF0
CF_RDDP:MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A	; R45 = IRC z CF32006
	%MR_OFS2DP(OMR_AP)	; OMR_AP
	MOVX  A,@DPTR
	MOV   R2,A
	MOV   A,R4
	MOVX  @DPTR,A
	CLR   C
	SUBB  A,R2
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   A,R5
	MOVX  @DPTR,A
	SUBB  A,R3
	MOV   R3,A
	INC   DPTR
	JNC   CF_RD5
	JB    ACC.7,CF_RD6	; Prodlouzeni OMR_AP
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	SJMP  CF_RD7
CF_RD5:	JNB   ACC.7,CF_RD6
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	SJMP  CF_RD7
CF_RD6: MOVX  A,@DPTR
CF_RD7:	MOV   R7,A
	INC   DPTR
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	RET

; Pozadavek na kalibraci polohy na R567
IRC_WR:
CF_WR:	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
IRC_WRP:
CF_WRP: MOV   A,#OMR_AIR
	MOVC  A,@A+DPTR		; OMR_AIR
	MOV   DPL,A
	MOV   DPH,#HIGH AIR_CF0
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	RET


; Oznami najeti na znacku  nastavenim ACC != 0
HH_MARK:
CF_MARK:MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,#OMR_AIR
	MOVC  A,@A+DPTR
	MOV   R0,A
	CLR   A
%IF (%USE_MARK_CTI_FL) THEN (
	CJNE  R0,#AIR_CF0+6,CF_MAR1	; 6
	MOV   C,P1.0
    %IF (%WITH_MARK_NEG) THEN (
	CPL   C
    )FI
	JBC   CTI0,CF_MAR9
	RET
CF_MAR1:CJNE  R0,#AIR_CF0+4,CF_MAR2	; 4
	MOV   C,P1.1
    %IF (%WITH_MARK_NEG) THEN (
	CPL   C
    )FI
	JBC   CTI1,CF_MAR9
	RET
CF_MAR2:
)ELSE(
	CJNE  R0,#AIR_CF0+6,CF_MAR1
	MOV   C,P1.0
    %IF (%WITH_MARK_NEG) THEN (
	CPL   C
    )FI
	JC    CF_MAR9
	RET
CF_MAR1:CJNE  R0,#AIR_CF0+4,CF_MAR2
	MOV   C,P1.1
    %IF (%WITH_MARK_NEG) THEN (
	CPL   C
    )FI
	JC    CF_MAR9
	RET
XF_MAR2:
)FI
	CJNE  R0,#AIR_CF0+2,CF_MAR8
	MOV   C,P4.6
    %IF (%WITH_MARK_NEG) THEN (
	CPL   C
    )FI
	JC    CF_MAR9
CF_MAR8:CLR   A
	RET
CF_MAR9:MOV   A,#1
	RET

%IF (%WITH_HH_LIMSW) THEN (

PUBLIC	HH_LIMSW

HH_LIMSW:
	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,#OMR_AIR
	MOVC  A,@A+DPTR
	MOV   R0,A
	CLR   A
	CJNE  R0,#AIR_CF0+6,HH_LSW1	; 6
	MOV   A,P5
	ANL   A,#08H
	RET
HH_LSW1:CJNE  R0,#AIR_CF0+4,HH_LSW2	; 4
	MOV   A,P5
	ANL   A,#10H
	RET
HH_LSW2:CJNE  R0,#AIR_CF0+2,HH_LSW8	; 2
	MOV   A,P3
	ANL   A,#10H
	RET
HH_LSW8:CLR   A
	RET
)FI

; *******************************************************************
; Vystup energii na motory

APWM0	XDATA 0FFE0H
APWM1	XDATA 0FFE1H
APWM2	XDATA 0FFE2H

RSEG	MR____C

; Nastavi energii na R45, nesmi menit R1
MR_SENE:MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
MR_SENEP:MOV  A,#OMR_APW
	MOVC  A,@A+DPTR
	MOV   DPL,A
	MOV   DPH,#HIGH APWM0
	MOV   A,R4
	RLC   A
	MOV   A,R5
	ADDC  A,#0	; Zaokrouhleni podle R4.7
	JNB   OV,MR_SENE5
	DEC   A
MR_SENE5:MOVX @DPTR,A
	RET

; *******************************************************************
; Zakladni smycka multi regulatoru

RSEG	MR____C

DO_REGR:
%IF(%USE_DEBUG_OUT)THEN(
	CLR   P4.6
)FI
	RET

; Prochazi vsechny regulatory az do OMR_FLG=0

DO_REG:
%IF(%USE_DEBUG_OUT)THEN(
	SETB  P4.6
)FI
%IF (%USE_KLIKLO_FL) THEN (
	SETB  BKLIKLO
)FI
	JNB   FL_MRCE,DO_REG0
	CLR   FL_MRCE		; smazani chyby pred vyhodnocenim
	CLR   MR_FLG.BMR_ERR
DO_REG0:ANL   MR_FLG,#MMR_ERR
	MOV   MR_BAS,#LOW REG_A
	MOV   MR_BAS+1,#HIGH REG_A
DO_REG1:CLR   F0
	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOVX  A,@DPTR		; OMR_FLG
	JZ    DO_REGR
	MOV   MR_FLGA,A
	JNB   MR_FLGA.BMR_ENI,DO_REG5
	CALL  IRC_RD
DO_REG5:JNB   MR_FLGA.BMR_ENG,DO_REG6
	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,#OMR_VGJ
	JMP   @A+DPTR  		; skok na vektor generatoru
DO_REG6:
VR_REG: JNB   MR_FLGA.BMR_ENR,DO_REG9
	CALL  MR_REG
DO_REG7:
VR_REG1:JB    MR_FLGA.BMR_ERR,VR_REG2
	JNB   F0,DO_REG8
	ORL   MR_FLGA,#MMR_ERR
VR_REG2:CLR   A
	MOV   R5,A
	MOV   R4,A
DO_REG8:CALL  MR_SENE
	JNB   MR_FLGA.BMR_DBG,DO_REG9
	%MR_OFS2DP(OMR_ENE)	; Pro debug ucely ulada energii
	MOV   A,R4
	MOVX  @DPTR,A
	MOV   R6,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   R7,A
	%MR_OFS2DP(OMR_AS)
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  DO_REGDBG
DO_REG9:
VR_RET:	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,MR_FLGA
	ORL   MR_FLG,A
	MOVX  @DPTR,A		; OMR_FLG
	MOV   A,MR_BAS
	ADD   A,#REG_LEN
	MOV   MR_BAS,A
	JNC   DO_REG1
	INC   MR_BAS+1
	JMP   DO_REG1


	END