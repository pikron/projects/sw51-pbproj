$NOMOD51
;********************************************************************
;*              Jednoosa regulace  - KZ.ASM                         *
;*                       Hlavni modul                               *
;*                  Stav ke dni 10.10.1997                          *
;*                      (C) Pisoft 1996                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_AL)
$INCLUDE(%INCH_ADR)
$INCLUDE(%INCH_REGS)
$INCLUDE(%INCH_MR_DEFS)
$INCLUDE(%INCH_MR_PDEFS)
$INCLUDE(%INCH_MR_PSYS)
$INCLUDE(%INCH_UI)
$INCLUDE(%INCH_RS232)
$INCLUDE(%INCH_RSOI)
$LIST

%DEFINE (WITH_ULAN) (1)

%IF (%WITH_ULAN) THEN (
;$INCLUDE(%INCH_ULAN)
;$INCLUDE(%INCH_UL_OI)
;$INCLUDE(%INCH_BREAK)
EXTRN	CODE(uL_FNC,uL_STR)
EXTRN	BIT(uLF_INE)
) FI

%DEFINE (MR_REG_TYPE) (MR_PIDLP); Prednastaveny typ regulatoru
%DEFINE (MR_REG_SEL)    (1)	; Moznost vyberu regulatoru
%DEFINE (REG_COUNT)	(1)	; Pocet pouzitych regulatoru
%DEFINE (TIME_INT_EX)   (0)	; Ktere z preruseni na casovani
				; 1 je pomalejsi, 0 rychlejsi


;BAUDDIV_9600	EQU	6	; pro 11.0592 MHz
BAUDDIV_9600	EQU	10	; pro 18.4321 MHz
;BAUDDIV_9600	EQU	13	; pro 24 MHz

EXTRN	CODE(cxMOVE,xMDPDP,SEL_FNC)
EXTRN	CODE(PRINThb,PRINThw,INPUThw)
EXTRN	CODE(MONITOR)
EXTRN   CODE(IHEXLD)
PUBLIC	INPUTc,KBDBEEP,ERRBEEP,RES_STAR

KZ____C SEGMENT CODE
KZ____D SEGMENT DATA
KZ____B SEGMENT DATA BITADDRESSABLE
KZ____X SEGMENT XDATA

RSEG	KZ____B

LEB_FLG:DS    1		; Blikani ledek

HW_FLG: DS    1
ITIM_RF BIT   HW_FLG.7	; Kontrola reentrance preruseni
FL_25Hz	BIT   HW_FLG.4	; Nastaven pri preruseni
LEB_PHA	BIT   HW_FLG.3	; Pro blikani led
FL_MRMC	BIT   HW_FLG.2	; Povoleni sberu udaju
FL_MRCE	BIT   HW_FLG.1	; Zadost o vynulovani chyby MR_FLG.BMR_ERR
FL_RS232 BIT  HW_FLG.0	; Pouziva se RS232

HW_FLG1: DS   1
FL_RDYR1 BIT  HW_FLG1.7 ; Vysli pouze jedno READY
FL_DOAMP BIT  HW_FLG1.6 ; Zapnuty generator kmitani
FL_CLAMP BIT  HW_FLG1.5 ; Ukonci kmitani motorem

RSEG	KZ____X

TMP:	DS    16

C_R_PER	EQU   2
REF_PER:DS    1		; Perioda refrese displeje

RSEG	KZ____C

RES_STAR:
	MOV   IEN0,#00000000B ; zakaz preruseni
	MOV   IEN1,#00000000B
	MOV   IP0, #00000000B ; priority preruseni
	%WATCHDOG	      ; Nulovani watch-dogu
	MOV   TMOD,#00100101B ; timer 1 mod 2; counter 0 mod 1
	MOV   TCON,#01010101B ; citac 0 a 1 cita ; interapy hranou
	MOV   TM2CON,#00100011B; counter 2 from T2, TR2 enabled
	MOV   SCON,#11011000B ; dva stopbity
	MOV   PCON,#10000000B ; Bd = OSC/12/16/(256-TH1)
	MOV   SP,#80H
	MOV   P1,#0FFH
	MOV   P3,#0FFH
	MOV   P4,#0FFH
	MOV   PWMP,#1         ; PWM frekvence 18 kHz
	MOV   PWM0,#0
	%VECTOR(EXTI%TIME_INT_EX,I_TIME1) ; EXTINT0/1
	SETB  EX%TIME_INT_EX

	CALL  I_TIMRI
	CALL  I_TIMRI

	CLR    A
	MOV    HW_FLG,A
	MOV    HW_FLG1,A
	MOV    LEB_FLG,A
	SETB   ITIM_RF

	CLR    A
	MOV    DPTR,#MR_UIAM
	MOVX   @DPTR,A
	INC    DPTR
	MOVX   @DPTR,A

	MOV   DPTR,#REG_A+OMR_FLG
	CLR   A
	MOVX  @DPTR,A

	CALL  LCDINST
	CALL  LEDWR

    %IF(NOT %WITH_ULAN)THEN(
	SETB   FL_RS232		; Inicializovat RS 232
    )FI

    %IF(%WITH_ULAN)THEN(
	MOV   A,#3
	CALL  I_U_LAN
	CALL  uL_OINI
    )FI

	MOV   DPTR,#REF_PER
	MOV   A,#C_R_PER
	MOVX  @DPTR,A

	JMP   L0

INPUTc:	CALL  SCANKEY
	JZ    INPUTc
	RET

; Pipnuti na klavese klavesnice
;KBDBEEP:JMP   KBDSTDB
KBDBEEP:MOV   A,#2
BEEP:	MOV   DPTR,#BEEPTIM
	MOVX  @DPTR,A
	SETB  %BEEP_FL
	MOV   DPTR,#LED       ; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
	MOV   A,R2
	RET
ERRBEEP:MOV   A,#8
	JMP   BEEP

%IF(%WITH_ULAN)THEN(
I_U_LAN:PUSH  ACC
	CLR   A
	MOV   A,#BAUDDIV_9600
	MOV   R0,#1
	CALL  uL_FNC	; Rychlost
	POP   ACC
	MOV   R0,#2
	CALL  uL_FNC	; Adresa
	MOV   R2,#0
	MOV   R0,#3
	CALL  uL_FNC	; Delka IB OB
	MOV   R2,#0
	MOV   R0,#4
	CALL  uL_FNC	; Rychle bloky
	MOV   R0,#0
	CALL  uL_FNC	; Start
	RET
)FI

; *******************************************************************
;
; Casove preruseni

PUBLIC  KBDTIMR

RSEG	KZ____D

%IF (%TIME_INT_EX) THEN (
DINT25  EQU   45   ; Delitel EXTINT1 (18.432MHz/2^14) na 25 Hz
)ELSE(
DINT25  EQU   90   ; Delitel EXTINT1 (18.432MHz/2^13) na 25 Hz
)FI
CINT25: DS    1

RSEG	KZ____X

BEEPTIM:DS    1	   ; Timer delky pipani

N_OF_T  EQU   6    ; Pocet timeru
TIMR1:  DS    1    ; Timery jsou decrementovany
TIMR2:  DS    1    ; s frekvenci 25 Hz
TIMR_WAIT:
TIMR3:  DS    1
REF_TIM:DS    1	   ; Refresh timr
KBDTIMR:DS    1
TIMRI:  DS    1
TIME:   DS    2    ; Cas v 0.01 min              =====

RSEG	KZ____C

USING   3
I_TIME1:PUSH  ACC	; Cast s pruchodem 2250 Hz / 1125 Hz
	PUSH  PSW
	CLR   IE%TIME_INT_EX
	MOV   PSW,#AR0
	PUSH  B
	PUSH  DPL
	PUSH  DPH

	CLR   FL_25Hz
	DJNZ  CINT25,I_TIM10
	SETB  FL_25Hz
I_TIM10:
	CALL  DO_REG		; Regulace DC motoru
	JNB   FL_DOAMP,I_TIM20
	MOV   R1,#0		; Generator am_plitudy
	CALL  DO_AMP
I_TIM20:
	;CALL  ADC_D		; Cteni AD prevodniku

	JNB   FL_25Hz,I_TIMR1	; Konec casti spruchodem 600 Hz
	MOV   CINT25,#DINT25	; Pruchod s frekvenci 25 Hz

	;CALL  ADC_S		; Spusteni cyklu prevodu ADC
	JB    FL_RS232,I_TIM70
    %IF(%WITH_ULAN)THEN(
	CALL  uL_STR
    )FI
I_TIM70:%WATCHDOG
	MOV   DPTR,#BEEPTIM
	MOVX  A,@DPTR
	JZ    I_TIM80
	DEC   A
	MOVX  @DPTR,A
	JNZ   I_TIM80
	CLR   %BEEP_FL
	MOV   DPTR,#LED       ; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
I_TIM80:MOV   DPTR,#TIMR1
	MOV   B,#N_OF_T-1
I_TIME2:MOVX  A,@DPTR
	JZ    I_TIME3
	DEC   A
	MOVX  @DPTR,A
I_TIME3:INC   DPTR
	DJNZ  B,I_TIME2
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	JNB   ITIM_RF,I_TIMR1
	JB    ACC.7,I_TIME4
I_TIMR1:POP   DPH
	POP   DPL
	POP   B
	POP   PSW
	POP   ACC
	SETB  EA
I_TIMRI:RETI

I_TIME4:CLR   ITIM_RF	      ; Pruchod 0.6 sec
;	CALL  I_TIMRI
;	MOV   PSW,#AR0        ; Banka 2
	ADD   A,#15
	MOVX  @DPTR,A

	MOV   A,LEB_FLG
	JBC   LEB_PHA,I_TIM42
	ORL   LED_FLG,A
	SETB  LEB_PHA
	SJMP  I_TIM43
I_TIM42:CPL   A
	ANL   LED_FLG,A
I_TIM43:SETB  ITIM_RF
	JMP   I_TIMR1

WAIT_T:	MOV   DPTR,#TIMR_WAIT
	MOVX  @DPTR,A
WAIT_T1:MOV   DPTR,#TIMR_WAIT
	MOVX  A,@DPTR
	JNZ   WAIT_T1
	RET

; *******************************************************************
; Promenne multiregulatoru

%IF (%MR_REG_SEL) THEN (
  EXTRN	CODE(MR_PIDNLP,MR_PZP,MR_PIDLP)
  MR_REG_ARR:
	%W    (MR_PIDNLP)
	%W    (MR_PIDNLP)
	%W    (MR_PIDLP)
	%W    (MR_PZP)
	%W    (MR_RELCHP)
	%W    (0)
)ELSE(
  EXTRN	CODE(%MR_REG_TYPE)
)FI

RSEG	KZ____B

MR_FLG:	DS    1
MR_FLGA:DS    1

RSEG	KZ____D

MR_BAS:	DS    2		; ukazatel na struktury regulatoru

%STRUCTM(OMR,SCM,2)	; zmena meritka - nasobeni
%STRUCTM(OMR,SCD,2)	; zmena meritka - deleni
%STRUCTM(OMR,AMP,2)	; amplituda kmitu

RSEG	KZ____X

REG_N	EQU   %REG_COUNT
REG_LEN	EQU   OMR_LEN
REG_A:	DS    REG_N*REG_LEN+1

OREG_A	EQU   0
OREG_B	EQU   REG_LEN
OREG_C	EQU   REG_LEN*2

PUBLIC	MR_BAS,MR_FLG,MR_FLGA
PUBLIC	REG_A,REG_LEN,REG_N
PUBLIC	GO_NOTIFY,DO_REGDBG
PUBLIC  FL_MRCE

RSEG	KZ____C

GO_NOTIFY:
	SETB  RSF_BSYO
	JNB   ACC.BMR_DBG,GO_NTF1
	JMP   MRMC_I 		; Start ukladani udaju
GO_NTF1:RET

DO_REGDBG:
	JMP   DO_MRMC

; *******************************************************************
; Vystup energii na motory

APWM0	XDATA 0FFE0H
APWM1	XDATA 0FFE1H
APWM2	XDATA 0FFE2H

; *******************************************************************
; Neregulator, energie R45 = OMR_RS

MR_RPULSEP:
	MOV   A,#OMR_RS
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_RS+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	RET

; *******************************************************************
; Releova charakteristika pro identifikaci
;
; parametry  

MR_RELCHP:
	CLR   C
	MOV   A,#OMR_AP		; R45=OMR_RQI-OMR_ACT
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_RPI
	MOVC  A,@A+DPTR
	SUBB  A,R4
	MOV   R4,A
	MOV   A,#OMR_AP+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   A,#OMR_RPI+1
	MOVC  A,@A+DPTR
	SUBB  A,R5
	MOV   R5,A
	MOV   B,A
	MOV   A,#OMR_AP+2
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   A,#OMR_RPI+2
	MOVC  A,@A+DPTR
	SUBB  A,R6
	MOV   C,B.7
	JB    ACC.7,MR_RCH1
	ADDC  A,#0
	JZ    MR_RCH2
	MOV   R4,#0FFH
	MOV   R5,#07FH
	SJMP  MR_RCH2
MR_RCH1:ADDC  A,#0
	JZ    MR_RCH2
	MOV   R4,#000H
	MOV   R5,#080H		; _ -> R45 ; R6
MR_RCH2:

	MOV   A,#OMR_FOI	; minuly stav
	MOVC  A,@A+DPTR
	JNZ   MR_RCH5
	MOV   A,R5		; minula energie kladna
	JNB   ACC.7,MR_RCH4
	MOV   A,#OMR_P
	MOVC  A,@A+DPTR
	ADD   A,R4
	MOV   A,#OMR_P+1
	MOVC  A,@A+DPTR
	ADDC  A,R5
	JNC   MR_RCH6
MR_RCH4:MOV   A,#OMR_I		; pristi kladna
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_I+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   R1,#0
	SJMP  MR_RCH7
MR_RCH5:MOV   A,R5		; minula energie kladna
	JB    ACC.7,MR_RCH6
	CLR   C
	MOV   A,#OMR_P
	MOVC  A,@A+DPTR
	SUBB  A,R4
	MOV   A,#OMR_P+1
	MOVC  A,@A+DPTR
	SUBB  A,R5
	JC    MR_RCH4
MR_RCH6:MOV   A,#OMR_I		; pristi zaporna
	MOVC  A,@A+DPTR
	CPL   A
	ADD   A,#1
	MOV   R4,A
	MOV   A,#OMR_I+1
	MOVC  A,@A+DPTR
	CPL   A
	ADDC  A,#0
	MOV   R5,A
	MOV   R1,#1
MR_RCH7:MOV   A,#OMR_FOI
	ADD   A,DPL
	MOV   DPL,A
	CLR   A
	ADDC  A,DPH
	MOV   DPH,A
	MOV   A,R1
	MOVX  @DPTR,A
	CLR   F0
	RET

; *******************************************************************
; Nastaveni pro MARS

MR_INIS:CALL  MR_ZER
	%LDR45i (REG_A)
	%LDR23i (STDR_A)
	%LDR01i (STDR_AE-STDR_A)
	CALL  cxMOVE
	MOV   A,#MMR_ENI 		; OR MMR_ENR
	MOV   DPTR,#REG_A+OREG_A+OMR_FLG
	MOVX  @DPTR,A
	RET

STDR_A:	DB    0, 0,0,0, 0,0
	DB    0
	DB    0,0   ; 	(CASH_ZP)	; RP
	DB    0
	DB    0,0,0			; RS
	DB    6,  0,  2,  0, 25,  0	; P I D
	DB    003H,0, 007H,0, 0,060H	; 1 2 ME
	%W    (8000)			; MS
	%W    (80)			; MA
	DB    0,0,0,0,0,0,0
	DB    1 ,1			; OMR_AIR, OMR_APW
	%W    (26+256)			; CFG
	DB    2				; JMP
	DW    %MR_REG_TYPE		; vektor na REGULATOR
	DB    2				; JMP
	DW    VR_REG			; skok na GENERATOR
	DS    STDR_A+OREG_A+OMR_SCM-$
	%W    (3*0)			; Meritko, nasobeni
	%W    (2)			; Meritko, deleni
	DS    STDR_A+OREG_A+OMR_AMP-$
	%W    (300)			; OMR_AMP
STDR_AE:

TM_GEPT:DB    LCD_CLR,'Kam :',0

TM_GEPR:RET

TM_GEP: MOV   DPTR,#TM_GEPT
	CALL  cPRINT
	MOV   R6,#8
	MOV   R7,#0C0H
	CALL  INPUTi
	JB    F0,TM_GEPR
	MOV   A,R5
	MOV   R6,A
	MOV   A,R4
	MOV   R5,A
	CLR   A
	MOV   R4,A
	MOV   R7,A
	MOV   R1,#0		; motor A
	JMP   GO_GEP

GO_HHT: MOV   R1,#0
	CALL  GO_HH
	MOV   R1,#1
	CALL  GO_HH
	RET

%IF (%MR_REG_SEL) THEN (

; Nastaveni typu regulatoru pro R1 na typ R4
REG_SEL:MOV   DPTR,#MR_REG_ARR
	INC   R4
REG_SEL2:CALL cLDR23i
	ORL   A,R2
	JZ    REG_SEL9
	DJNZ  R4,REG_SEL2
	CALL  GO_PRPC		; odpojit generator
	JNB   F0,REG_SEL3
	RET
REG_SEL3:ANL  A,#NOT (MMR_ENR OR MMR_BSY)
	MOVX  @DPTR,A
	MOV   A,#OMR_VRJ	; JMP na regulator
	CALL  MR_GPA1
	MOV   C,EA
	CLR   EA
	MOV   A,#2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R2
	MOVX  @DPTR,A
	MOV   EA,C
	CLR   A			; nulova vystupni energie
	MOV   R4,A
	MOV   R5,A
	CALL  MR_GPA1
	CALL  MR_SENEP
	MOV   A,#OMR_FLG	; uvolneni zamku
	CALL  MR_GPA1
	MOVX  A,@DPTR
	ANL   A,#7FH
	MOVX  @DPTR,A
	RET
REG_SEL9:SETB F0
	RET
)FI

; *******************************************************************
; Zaznam deje do pameti ram

RSEG	KZ____X

MRMC_BAS:
MRMC_AP:DS    2		; Pozice kam ukladat
MRMC_EP:DS    2		; Koncova pozice

MRMC_BMB XDATA 08800H	; Pocatek oblasti pro ulozeni dat
MRMC_EMB XDATA 0A000H	; Konec oblasti pro ulozeni dat
MRMC_MAXL SET (MRMC_EMB-MRMC_BMB)/2

RSEG	KZ____C

; Uklada udaje v R4567
DO_MRMC:JNB   FL_MRMC,MRMC_R
	MOV   DPTR,#MRMC_BAS
	MOV   A,#MRMC_AP-MRMC_BAS
	MOVC  A,@A+DPTR
	MOV   R2,A
	ADD   A,#4
	MOV   R0,A
	MOV   A,#MRMC_AP-MRMC_BAS+1
	MOVC  A,@A+DPTR
	MOV   R3,A
	ADDC  A,#0
	MOV   R1,A
	CLR   C
	MOV   A,#MRMC_EP-MRMC_BAS
	MOVC  A,@A+DPTR
	SUBB  A,R0
	MOV   A,#MRMC_EP-MRMC_BAS+1
	MOVC  A,@A+DPTR
	SUBB  A,R1
	JC    MRMC_R1		; Pamet zaplnena
	MOV   A,R0
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A
	MOV   DPL,R2		; Ulozit data
	MOV   DPH,R3
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
MRMC_R:	RET
MRMC_R1:CLR   FL_MRMC
	RET

; Inicializace subsystemu
MRMC_I:	CLR   FL_MRMC
	%LDMXi(MRMC_AP,MRMC_BMB)
	%LDMXi(MRMC_EP,MRMC_EMB)
	SETB  FL_MRMC
	RET

; *******************************************************************
; Generator sumu pripraveneho v bufferu historie

; DPTR = MRMC_AP
; pokud MRMC_AP>=MRMC_EP pak CY=1
CD_GNMRA:MOV  DPTR,#MRMC_AP
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R1,A
	INC   DPTR
	SETB  C
	MOVX  A,@DPTR
	SUBB  A,R0
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,R1
	MOV   DPL,R0
	MOV   DPH,R1
	RET

; inicializace a beh
CI_GNS:	JNB   ACC.BMR_DBG,CD_GNS8
	SETB  MR_FLGA.BMR_BSY	; Busy
	%LDR45i(CD_GNS)
	CALL  CI_VG
	CLR   MR_FLGA.BMR_ENR
CD_GNS:	JNB   FL_MRMC,CD_GNS8
	CALL  CD_GNMRA		; DPTR=MRMC_AP
	JC    CD_GNS8
	MOV   A,#2
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#3
	MOVC  A,@A+DPTR
	MOV   R5,A
	JMP   VR_REG1
CD_GNS8:CLR   MR_FLGA.BMR_BSY
	%LDR45i(VR_REG)
CI_GNS9:CLR   MR_FLGA.BMR_ENR
	CALL  CI_VG
	CLR   A
	MOV   R4,A
	MOV   R5,A
	JMP   VR_REG1

; start
GO_GNS:	CALL  GO_PRPC		; odpojit generator
	JB    F0,GO_GNS1
	JB    ACC.BMR_DBG,GO_GNS3
	ANL   A,#7FH
	MOVX  @DPTR,A
GO_GNS1:RET
GO_GNS3:%LDR45i(CI_GNS)
	JMP   GO_VG

GO_GNSALL:
	MOV   R1,#0
GO_GNSA1:CALL GO_GNS
	INC   R1
	MOV   A,R1
	CJNE  A,#REG_N,GO_GNSA1
	RET

; *******************************************************************
; Generator pripraveneho sumu do regulatoru

; inicializace a beh
CI_GNR:	JNB   ACC.BMR_DBG,CD_GNR8
	SETB  MR_FLGA.BMR_BSY	; Busy
	%LDR45i(CD_GNR)
	CALL  CI_VG
CD_GNR:	JNB   FL_MRMC,CD_GNR8
	CALL  CD_GNMRA		; DPTR=MRMC_AP
	JC    CD_GNR8
	MOV   A,#2
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#3
	MOVC  A,@A+DPTR
	MOV   R5,A              ; R45 rychlost z bufferu
	MOV   R6,#0
	JNB   ACC.7,CD_GNR6	; rozsirit na R456
	DEC   R6
CD_GNR6:%MR_OFS2DP(OMR_RPI)	; OMR_RPI=OMR_RPI+R456
	MOVX  A,@DPTR
	ADD   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R6
	MOVX  @DPTR,A
	%MR_OFS2DP(OMR_RSI)	; OMR_RSI=R45
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	JMP   VR_REG
CD_GNR8:CLR   MR_FLGA.BMR_BSY
	%LDR45i(VR_REG)
CI_GNR9:CALL  CI_VG
	JMP   VR_REG

; start
GO_GNR:	CALL  GO_PRPC		; odpojit generator
	JB    F0,GO_GNR1
	JB    ACC.BMR_DBG,GO_GNR3
	ANL   A,#7FH
	MOVX  @DPTR,A
GO_GNR1:RET
GO_GNR3:JB    ACC.BMR_ENR,GO_GNR7
	CALL  GO_RSFT		; beznarazove pripojeni regulatoru
GO_GNR7:%LDR45i(CI_GNR)
	JMP   GO_VG

GO_GNRALL:
	MOV   R1,#0
GO_GNRA1:CALL GO_GNR
	INC   R1
	MOV   A,R1
	CJNE  A,#REG_N,GO_GNRA1
	RET

; *******************************************************************
; Nalezeni stredu mezi dorazy

EXTRN	CODE(CD_HHCL,CD_HHST)

; Priprava hledani
CI_FC:	SETB  MR_FLGA.BMR_BSY
	CALL  CD_HHCL

	%LDR45i(CD_FC10)
	JMP   CD_FC90

; Jede se dolu
CD_FC10:%LDR45i(-100)
	CALL  MR_GPSV		; R4567=OMR_RP+R45; R3=dir
	CALL  MR_WRRP		; OMR_RP=R456
	JNB   MR_FLGA.BMR_ENR,CD_FC80
	CALL  MR_REG
	JNB   F0,CD_FC99
	; Dojeti na doraz
	CALL  CD_HHCL
	%LDR45i(CD_FC20)
	JMP   CD_FC90

; Jede se dolu
CD_FC20:%LDR45i(100)
	CALL  MR_GPSV		; R4567=OMR_RP+R45; R3=dir
	CALL  MR_WRRP		; OMR_RP=R456
	JNB   MR_FLGA.BMR_ENR,CD_FC80
	CALL  MR_REG
	JNB   F0,CD_FC99
	; Dojeti na doraz
	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	CLR   C
	MOV   A,#OMR_AP+2
	MOVC  A,@A+DPTR
	RRC   A
	MOV   R7,A
	MOV   A,#OMR_AP+1
	MOVC  A,@A+DPTR
	RRC   A
	MOV   R6,A
	MOV   A,#OMR_AP
	MOVC  A,@A+DPTR
	RRC   A
	MOV   R5,A
	MOV   R4,#0
	CALL  CD_HHST
	%LDR45i(CD_FC30)
	JMP   CD_FC90

; Chyba
CD_FC80:CLR   MR_FLGA.BMR_ENR
	CLR   MR_FLGA.BMR_BSY
	%LDR45i(VR_REG)
CD_FC90:CALL  CI_VG
CD_FC95:MOV   R4,#0
	MOV   R5,#0
	CLR   F0
CD_FC99:JMP   VR_REG1

; Najeti na stred
CD_FC30:%LDR45i(-100)
	CALL  MR_GPSV		; R4567=OMR_RP+R45; R3=dir
	MOV   A,R7
	JB    ACC.7,CD_FC32
	CALL  MR_WRRP		; OMR_RP=R456
	JMP   VR_REG
CD_FC32:%MR_OFS2DP(OMR_RP)
	MOV   R0,#7
	CLR   A
CD_FC35:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,CD_FC35
	CLR   MR_FLGA.BMR_BSY
	%LDR45i(VR_REG)
	CALL  CI_VG
	JMP   VR_REG

; Spusteni hledani
GO_FC_A:MOV   R1,#0		; pro regulator A
	CLR   FL_DOAMP
GO_FC:	CALL  GO_PRPC		; odpojit generator
	JNB   F0,GO_FC3
	RET
GO_FC3:	JB    ACC.BMR_ENR,GO_FC7
	CALL  GO_RSFT		; beznarazove pripojeni regulatoru
GO_FC7:	%LDR45i(CI_FC)
	JMP   GO_VG

; *******************************************************************
; Kmitani motorem

; Stara se o kmitani o +/- OMR_AMP regulatorem [R1]
DO_AMP: MOV   A,R1
	MOV   B,#REG_LEN
	MUL   AB
	ADD   A,#LOW  REG_A
	MOV   DPL,A
	MOV   A,B
	ADDC  A,#HIGH REG_A
	MOV   DPH,A
	MOV   A,#OMR_FLG
	MOVC  A,@A+DPTR
	JB    ACC.BMR_BSY,DO_AMP9
	JB    ACC.BMR_ERR,DO_AMP9
	JBC   FL_CLAMP,DO_AMP6
	MOV   A,#OMR_AP+2
	MOVC  A,@A+DPTR
	JNB   ACC.7,DO_AMP3
	MOV   R4,#0		; Pristi pohyb +OMR_AMP
	MOV   A,#OMR_AMP
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   A,#OMR_AMP+1
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   R7,#0
	JMP   DO_AMP8
DO_AMP3:MOV   R4,#0		; Pristi pohyb -OMR_AMP
	MOV   A,#OMR_AMP
	MOVC  A,@A+DPTR
	CPL   A
	ADD   A,#1
	MOV   R5,A
	MOV   A,#OMR_AMP+1
	MOVC  A,@A+DPTR
	CPL   A
	ADDC  A,#0
	MOV   R6,A
	MOV   A,#0FFH
	ADDC  A,#0
	MOV   R7,A
	JMP   DO_AMP8
DO_AMP6:CLR   FL_DOAMP		; Zastaveni kmitani
	CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R7,A
DO_AMP8:CLR   F0
	JMP   GO_GEP		; Novy pohyb
DO_AMP9:RET

; *******************************************************************
; System prevodniku

RSEG	KZ____X

ADC0:	DS    2
ADC1:	DS    2
ADC2:	DS    2
ADC3:	DS    2
ADC4:	DS    2
ADC5:	DS    2
ADC6:	DS    2
ADC7:	DS    2

RSEG	KZ____C

ADC_D:	MOV   B,ADCON
	JNB   B.4,ADC_DR
	MOV   A,B
	ANL   A,#7
	RL    A
	ADD   A,#LOW  ADC0
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH ADC0
	MOV   DPH,A
	MOV   A,B
	ANL   A,#0C0H
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,ADCH
	MOVX  @DPTR,A
	MOV   A,B
	ANL   A,#7
	INC   A
	JNB   ACC.3,ADC_S2
	ANL   ADCON,#NOT 10H
ADC_DR:	RET

ADC_S:  CLR   A
	MOV   B,ADCON
	JB    B.3,ADC_DR
ADC_S2:	ANL   A,#07H
	MOV   ADCON,A
	SETB  ACC.3
	MOV   ADCON,A
	RET

; *******************************************************************

xPR_AD: MOV   A,#' '
	CALL  LCDWR
	MOV   R1,#3
	SJMP  xPRThl1

xPRThl:	MOV   R1,#4
xPRThl1:MOV   A,R1
	DEC   A
	MOVC  A,@A+DPTR
	CALL  PRINThb
	DJNZ  R1,xPRThl1
	RET

DB_W_10:MOV   R0,#10H
	SJMP  DB_WAI1

DB_WAIT:MOV   R0,#0H
DB_WAI1:%WATCHDOG
	DJNZ  R1,DB_WAI1
	DJNZ  R0,DB_WAI1
	RET

FCSTRT_T:DB   C_CLR, 'Press key to'
	DB    C_LIN2,'start calibration',0

PR_PIKT:DB    'PiKRON ltd - precission motion control '
	DB    '+420 02 6884676 Cz '
	DB    0

PR_PIK1:MOV   DPTR,#PR_PIKT
PR_PIK3:MOVX  A,@DPTR
	JZ    PR_PIK9
	PUSH  DPL
	PUSH  DPH
	MOV   A,#LCD_HOM+40H
	CALL  LCDWCOM
	MOV   R0,#16
PR_PIK4:MOVX  A,@DPTR
	CALL  LCDWR
	INC   DPTR
	DJNZ  R0,PR_PIK4
	MOV   A,#10
	CALL  WAIT_T
	POP   DPH
	POP   DPL
	MOV   A,#16
	MOVC  A,@A+DPTR
	INC   DPTR
	JNZ   PR_PIK3
PR_PIK9:RET

L0:	MOV   SP,#80H
	SETB  EA
	MOV   DPTR,#DEVER_T
	CALL  cPRINT
	MOV   A,#4*25
	CALL  WAIT_T
	;CALL  PR_PIK1

	MOV   A,#LCD_CLR
	CALL  LCDWCOM

	JNB   FL_RS232,L010
	MOV   R7,#BAUDDIV_9600
	CALL  RS232_INI
	SETB  PS
	%LDMXi(RS_OPL,RS_OPL1)
	MOV   A,#K_PROG
	CALL  TESTKEY
	JZ    L_IHEX
L010:

	CALL  IRC_INIT		; Inicializace CF32006
	CALL  MR_INIS		; Inicializace multi regu

	MOV   A,#K_DP
	CALL  TESTKEY
	JZ    L1

	MOV   DPTR,#FCSTRT_T
	CALL  cPRINT
	CALL  INPUTc
	CALL  GO_FC_A

	JMP   UT

L_IHEX:	CALL  IHEXLD
	SJMP  L_IHEX

L1:
	MOV   A,#LCD_HOM
	CALL  LCDWCOM

	MOV   DPTR,#REG_A+OREG_A+OMR_AP
	CALL  xPR_AD

	MOV   DPTR,#REG_A+OREG_B+OMR_AP
	MOV   DPTR,#REG_A+OREG_A+OMR_RS
	CALL  xPR_AD

	MOV   A,#LCD_HOM+40H
	CALL  LCDWCOM

	MOV   DPTR,#ADC0		; Prevodniky
	CALL  xLDR45i
	CALL  PRINThw

	MOV   DPTR,#ADC6
	CALL  xLDR45i
	CALL  PRINThw

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#ADC7
	CALL  xLDR45i
	CALL  PRINThw
%IF (0) THEN (
	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#REG_A+OREG_A+OMR_FOD
	CALL  xLDR45i
	CALL  PRINThw

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#REG_A+OREG_A+OMR_FOI
	CALL  xLDR45i
	CALL  PRINThw

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#REG_A+OREG_A+OMR_RP
	CALL  xLDR45i
	CALL  PRINThw
) FI

L2:	CALL  SCANKEY
	JZ    L3
	MOV   R7,A
	MOV   DPTR,#SFT_D1
	CALL  SEL_FNC
L3:

	JMP   L1


T_RDVAL:MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR
	PUSH  ACC
	INC   DPTR
	MOVX  A,@DPTR
	PUSH  ACC
	INC   DPTR
	CALL  cPRINT
	MOV   R6,#8
	MOV   R7,#40H
	CALL  INPUTi
	POP   DPH
	POP   DPL
	JB    F0,T_RDV99
	CALL  xSVR45i
T_RDV99:RET

SFT_D1:	DB    K_END
	%W    (0)
	%W    (L0)

	DB    K_H_C
	%W    (0)
	%W    (TM_GEP)

	DB    K_H_A
	%W    (T_PWM0)
	%W    (T_RDVAL)

	DB    K_H_B
	%W    (T_PWM1)
	%W    (T_RDVAL)

	DB    K_6
	%W    (0)
	%W    (UT)

	DB    K_DP
	%W    (0)
	%W    (MONITOR)

	DB    0

T_PWM0:	%W    (0FFE0H)
	DB    LCD_CLR,'PWM0 :',0

T_PWM1:	%W    (0FFE1H)
	DB    LCD_CLR,'PWM1 :',0

DEVER_T:DB    LCD_CLR,'%VERSION'
	DB    C_LIN2 ,' (c) PiKRON 1997',0

; *******************************************************************
; R232 pro MARS

%*DEFINE (RS_TID(NAME)) (
RST_%NAME:DB '%NAME',0
)

%*DEFINE (RS_TIDSUF(NAME)) (
RST_%(%NAME)_SUF:DB '%NAME#',0
)

%*DEFINE (RSD_NEW (NAME)) (
RSD_P	SET   RSD_T
RSD_T	SET   $
	DB   LOW (RSD_P),HIGH (RSD_P)
	DB   LOW (RST_%NAME),HIGH (RST_%NAME)
)

RSEG	KZ____X
MR_RSAM:DS    1

TMP_MSPC:
RS_TMP:	DS    4

RSEG	KZ____C

RS_SAMA:CALL  RS_ACKL
RS_SAM:	MOV   A,R6
	ADD   A,#-'A'
	CJNE  A,#REG_N,RS_SAM1
RS_SAM1:JC    RS_SAM2
	SETB  F0
	RET
RS_SAM2:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#MR_RSAM
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
	JMP   RS_DOCM

RSO_VER_T:DB  'VER=%VERSION',0

RSO_VER:MOV   DPTR,#RSO_VER_T
	JMP   RS_WRL1

RSO_TEST_T:DB 'TEST OK',0
RSO_TEST:MOV  DPTR,#RSO_TEST_T
	JMP   RS_WRL1

; Zapina/vypina ohlas READY/FAILED
RS_SRDY:MOV   A,R4
	ADD   A,#-'1'-1
	JNC   RS_SRDY1
	SETB  F0
	RET
RS_SRDY1:ADD  A,#1
	MOV   RSF_RDYR,C
	SETB  RSF_BSYO
	CLR   FL_RDYR1
	RET

; Vysli pouze jedno ready
RS_RDYR1:SETB RSF_RDYR
	SETB  RSF_BSYO
	SETB  FL_RDYR1
	RET

; Zapina/vypina ohlas REPLY
RS_SRPLY:MOV  A,R4
	ADD   A,#-'1'-1
	JNC   RS_SRPL1
	SETB  F0
	RET
RS_SRPL1:ADD  A,#1
	MOV   RSF_RPLY,C
	MOV   RSF_LNT,C
	RET

; Blokovani klavesnice
RS_KEYLOCK:
	MOV   A,R4
	ADD   A,#-'1'-1
	JNC   RS_KEYL1
	SETB  F0
	RET
RS_KEYL1:ADD  A,#1
	MOV   R2,#GL_KEYLOCK
	JC    RS_KEYL2
	MOV   R2,#GL_KEYEN
RS_KEYL2:MOV  R3,#0
	JMP   GLOB_RQ23

; Nastaveni debug modu pro regulator
RS_REGDBG:
	MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	MOV   A,#OMR_FLG
	CALL  MR_GPA1
	MOV   A,R4
	ADD   A,#-'1'
	CLR   EA
	MOVX  A,@DPTR
	MOV   ACC.BMR_DBG,C
	MOVX  @DPTR,A
	SETB  EA
	RET

; Cteni historie
RS_REGDBGH:
	%LDMXi(RS_TMP+2,MRMC_BMB)
RS_DBG2:MOV   DPTR,#RS_TMP
	MOVX  A,@DPTR
	ADD   A,#-1
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#-1
	MOVX  @DPTR,A
	JNC   RS_DBG9
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R2,A
	ADD   A,#2
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	ADDC  A,#0
	MOVX  @DPTR,A
	MOV   DPL,R2
	MOV   DPH,R3
	CALL  xLDR45i
	CALL  L_APTR
	MOV   R2,#0
	MOV   R3,#80H
	CALL  xITOAF
	MOV   A,#0DH
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#0AH
	MOVX  @DPTR,A
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A
	CALL  L_APTR
RS_DBG6:MOVX  A,@DPTR
	JZ    RS_DBG2
	MOV   R0,A
	PUSH  DPL
	PUSH  DPH
	CLR   F0
	CALL  RS_WR
	POP   DPH
	POP   DPL
	JB    F0,RS_DBG6
	INC   DPTR
	SJMP  RS_DBG6
RS_DBG9:CLR   RSF_LNT
	CLR   F0
	RET

; Priprava dat pro inedtifikaci
RS_REGDBGP:
	%LDMXi(RS_TMP+2,MRMC_BMB)
RS_DBP2:MOV   DPTR,#RS_TMP
	MOVX  A,@DPTR
	ADD   A,#-1
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#-1
	MOVX  @DPTR,A
	JNC   RS_DBP5
	CLR   C
RS_DBP3:JC    RS_DBP9
	CALL  RS_RDLN
	JZ    RS_DBP3
	MOV   R2,#0
	MOV   R3,#80H
	CALL  xATOIF
	JB    F0,RS_DBP9
	MOV   DPTR,#RS_TMP+2
	MOVX  A,@DPTR
	MOV   R2,A
	ADD   A,#2
	MOV   R0,A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	ADDC  A,#0
	MOVX  @DPTR,A
	XCH   A,R0
	ADD   A,#LOW (-MRMC_EMB)
	MOV   A,R0
	ADDC  A,#HIGH (-MRMC_EMB)
	JC    RS_DBP5
	MOV   DPL,R2
	MOV   DPH,R3
	CALL  xSVR45i
	SJMP  RS_DBP2
RS_DBP5:MOV   DPTR,#RS_TMP+2
	CALL  xMDPDP
	SETB  C
	MOV   A,#LOW MRMC_EMB
	SUBB  A,DPL
	MOV   R2,A
	MOV   A,#HIGH MRMC_EMB
	SUBB  A,DPH
	MOV   R3,A
	JC    RS_DBP8
	INC   R2
	INC   R3
	CLR   A
RS_DBP6:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R2,RS_DBP6
	DJNZ  R3,RS_DBP6
RS_DBP8:CLR   RSF_LNT
	CLR   F0
RS_DBP9:RET

RS_HH:	MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	JMP   GO_HH

RS_CLEAR:MOV  DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	JMP   CLR_GEP

RS_STALL:MOV  R4,MR_FLG
	MOV   R5,#0
	RET

RS_RELEASE_ALL:
	JMP   REL_ALL

RS_RELEASE:
	MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	JMP   REL_GEP

%IF (%MR_REG_SEL) THEN (
RS_REGTYPE:
	MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	JMP   REG_SEL
)FI

; -----------------------------------
; Listy prikazu

RS_OPL1:DB    ':'
	%W    (RS_CML1)
	DB    '?'
	%W    (RS_CML2)
	DB    0


; -----------------------------------
; Jmena prikazu

%RS_TIDSUF(G)
%RS_TIDSUF(GR)
%RS_TIDSUF(AP)
%RS_TIDSUF(HH)
%RS_TIDSUF(ST)
%RS_TIDSUF(REGP)
%RS_TIDSUF(REGI)
%RS_TIDSUF(REGD)
%RS_TIDSUF(REGS1)
%RS_TIDSUF(REGS2)
%RS_TIDSUF(REGMS)
%RS_TIDSUF(REGACC)
%RS_TIDSUF(REGME)
%RS_TIDSUF(REGCFG)
%RS_TIDSUF(REGDBG)
%RS_TIDSUF(REGTYPE)
%RS_TIDSUF(CLEAR)
%RS_TIDSUF(RELEASE)
%RS_TID(STOP)
%RS_TID(HH)
%RS_TID(ST)
%RS_TID(PURGE)
%RS_TID(CLEAR)
%RS_TID(RELEASE)
%RS_TID(READY)
%RS_TID(R)
%RS_TID(REPLY)
%RS_TID(KEYLOCK)
%RS_TID(REGDBGHIS)
%RS_TID(REGDBGPRE)
%RS_TID(REGDBGGNS)
%RS_TID(REGDBGGNR)
%RS_TID(VER)
%RS_TID(IHEXLD)
%RS_TID(TEST)

; -----------------------------------
; Vstupni prikazy

RSD_T	SET   0

%RSD_NEW(TEST)
	%W    (RS_CMDA)
	%W    (RSO_TEST)

%RSD_NEW(IHEXLD)
	%W    (RS_CMDA)
	%W    (IHEXLD)

%IF (%MR_REG_SEL) THEN (
%RSD_NEW(REGTYPE_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RS_REGTYPE); Funkce
)FI

%RSD_NEW(REGDBGHIS)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (RS_TMP)	; Adresa
	%W    (RS_REGDBGH)

%RSD_NEW(REGDBGPRE)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (RS_TMP)	; Adresa
	%W    (RS_REGDBGP)

%RSD_NEW(REGDBGGNS)
	%W    (RS_CMDA)
	%W    (GO_GNSALL)

%RSD_NEW(REGDBGGNR)
	%W    (RS_CMDA)
	%W    (GO_GNRALL)

%RSD_NEW(KEYLOCK)
	%W    (RSI_ONOFA)
	%W    (RS_KEYLOCK)

%RSD_NEW(REPLY)
	%W    (RSI_ONOFA)
	%W    (RS_SRPLY)

%RSD_NEW(READY)
	%W    (RSI_ONOFA)
	%W    (RS_SRDY)

%RSD_NEW(R)
	%W    (RS_CMDA)
	%W    (RS_RDYR1)

%RSD_NEW(HH)
	%W    (RS_CMDA)
	%W    (GO_HHT)

%RSD_NEW(PURGE)
	%W    (RS_CMDA)
	%W    (CER_ALL)

%RSD_NEW(STOP)
	%W    (RS_CMDA)
	%W    (STP_ALL)

%RSD_NEW(CLEAR)
	%W    (RS_CMDA)
	%W    (CLR_ALL)

%RSD_NEW(RELEASE)
	%W    (RS_CMDA)
	%W    (RS_RELEASE_ALL)

%RSD_NEW(CLEAR_SUF)
	%W    (RS_SAMA)
	%W    (RS_CLEAR)

%RSD_NEW(RELEASE_SUF)
	%W    (RS_SAMA)
	%W    (RS_RELEASE)

%RSD_NEW(REGDBG_SUF)
	%W    (RS_SAM)
	%W    (RSI_ONOFA)
	%W    (RS_REGDBG)

%RSD_NEW(REGP_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_P)

%RSD_NEW(REGI_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_I)

%RSD_NEW(REGD_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_D)

%RSD_NEW(REGS1_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_S1)

%RSD_NEW(REGS2_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_S2)

%RSD_NEW(REGMS_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_MS)

%RSD_NEW(REGACC_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_ACC)

%RSD_NEW(REGME_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_ME)

%RSD_NEW(REGCFG_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_CFG)

%RSD_NEW(HH_SUF)
	%W    (RS_SAMA)
	%W    (RS_HH)

%RSD_NEW(GR_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,0C3H
	%W    (0)	; Adresa
	%W    (RSW_MRPl); Funkce
	DB    1

%RSD_NEW(G_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,0C3H
	%W    (0)	; Adresa
	%W    (RSW_MRPl); Funkce
	DB    0

RS_CML1	SET   RSD_T

; -----------------------------------
; Vystupni prikazy
RSD_T	SET   0

%RSD_NEW(TEST)
	%W    (RSO_TEST)

%RSD_NEW(VER)
	%W    (RSO_VER)

%RSD_NEW(REGP_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_P

%RSD_NEW(REGI_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_I

%RSD_NEW(REGD_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_D

%RSD_NEW(REGS1_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_S1

%RSD_NEW(REGS2_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_S2

%RSD_NEW(REGMS_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_MS

%RSD_NEW(REGACC_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_ACC

%RSD_NEW(REGME_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_ME

%RSD_NEW(REGCFG_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_CFG

%RSD_NEW(ST_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRb)	; Funkce
	DB    OMR_FLG

%RSD_NEW(AP_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,0C3H
	%W    (0)	; Adresa
	%W    (RSR_MRPl); Funkce

%RSD_NEW(ST)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RS_STALL); Funkce

RS_CML2	SET   RSD_T

; *******************************************************************
; Vyblokovano

RSEG	KZ____B
UD_BBBB:DS    1
UDF_RDP	BIT   UD_BBBB.7

RSEG	KZ____C

uL_OINI:RET
UD_OI:	RET
UD_REFR:RET

; *******************************************************************
; Propojeni s regulatory

RSEG	KZ____X

MR_UIAM:DS    2

AMP_SWD:DS    1

RSEG	KZ____C

; Deli R7B registrem R3, pak posune R4567B
SCL_S:	MOV   R0,#1
	CLR   A
	XCH   A,R3
	MOV   R2,A
	CLR   C
SCL_S1:	MOV   A,R2	; R32 >>= 1
	RRC   A
	MOV   R2,A
	MOV   A,R3
	RRC   A
	MOV   R3,A
	MOV   A,R7	; R7B - R32 ?
	SUBB  A,R3
	PUSH  ACC
	MOV   A,B
	SUBB  A,R2
	JC    SCL_S3
	MOV   B,A	; R7B > R32
	POP   ACC
	MOV   R7,A
	MOV   A,R0	; R0 = (R0 << 1) | \0
	RLC   A
	MOV   R0,A
	JNC   SCL_S1
	SJMP  SCL_S4
SCL_S3: DEC   SP	; R7B < R32
	MOV   A,R0	; R0 = (R0 << 1) | \1
	RLC   A
	MOV   R0,A
	JNC   SCL_S1
SCL_S4: CPL   A
	XCH   A,R4
	XCH   A,R5
	XCH   A,R6
	XCH   A,R7
	XCH   A,B
SCL_S9:	RET

; Prevod meritka souradnic pro R1
; pro R0=0 .. z motoru, R0=1 .. na motor
SCL_MR:	CLR   A
	CALL  MR_GPA1
	MOV   A,#OMR_SCM+1
	MOVC  A,@A+DPTR
	MOV   B,A
	MOV   A,#OMR_SCM
	MOVC  A,@A+DPTR
	JNB   B.7,SCL_MR1
	CPL   A
	INC   A
SCL_MR1:JZ    SCL_S9
	MOV   R2,A
	MOV   A,#OMR_SCD+1
	MOVC  A,@A+DPTR
	XRL   B,A
	MOV   C,ACC.7
	MOV   A,#OMR_SCD
	MOVC  A,@A+DPTR
	JNC   SCL_MR2
	CPL   A
	INC   A
SCL_MR2:JZ    SCL_S9
	MOV   R3,A
	DJNZ  R0,SCL_MR3
	XCH   A,R2
	MOV   R3,A
SCL_MR3:JNB   B.7,SCL_MR4
	CLR   A
	CLR   C
	SUBB  A,R4
	MOV   R4,A
        CLR   A
	SUBB  A,R5
	MOV   R5,A
	CLR   A
	SUBB  A,R6
	MOV   R6,A
	CLR   A
	SUBB  A,R7
	MOV   R7,A
; operace R4567*R2/R3
SCL_MR4:MOV   B,R2
	MOV   A,R4
	MUL   AB
	MOV   R4,A
	MOV   A,R2
	XCH   A,B
	XCH   A,R5
	MUL   AB
	ADD   A,R5
	MOV   R5,A
	MOV   A,R2
	XCH   A,B
	ADDC  A,#0
	XCH   A,R6
	MUL   AB
	ADD   A,R6
	MOV   R6,A
	MOV   A,R2
	XCH   A,B
	ADDC  A,#0
	XCH   A,R7
	JB    ACC.7,SCL_MR5
	MOV   R2,#0	; Bez korekce pro kladne cislo
SCL_MR5:MUL   AB
	ADD   A,R7
	MOV   R7,A
	MOV   A,B
	ADDC  A,#0
	SUBB  A,R2
	JNC   SCL_MR61
	ADD   A,R3	; Zaporne cislo
	MOV   B,A
	CALL  SCL_S
	JNZ   SCL_MR8
	MOV   A,R4
	JB    ACC.7,SCL_MR63
	SJMP  SCL_MR8
SCL_MR61:MOV  B,A	; Kladne cislo
	CALL  SCL_S
	JNZ   SCL_MR8
	MOV   A,R4
	JB    ACC.7,SCL_MR8
SCL_MR63:CALL SCL_S
	CALL  SCL_S
	CALL  SCL_S
	MOV   A,B
	CLR   C
	RLC   A
	JC    SCL_MR65
	SUBB  A,R3
	JC    SCL_MR9
SCL_MR65:INC  R4
	MOV   A,R4
	JNZ   SCL_MR9
	INC   R5
	MOV   A,R5
	JNZ   SCL_MR9
	INC   R6
	MOV   A,R6
	JNZ   SCL_MR9
	INC   R7
	MOV   A,R7
	CJNE  A,#80H,SCL_MR9
SCL_MR8:SETB  F0
SCL_MR9:RET


RSR_MRPl:MOV  DPTR,#MR_RSAM
	SJMP  UR_MRPl1

; Nacte do R4567 aktualni polohu z regulatoru [DPTR+OU_DP]
UR_MRPl:MOV   A,#OU_DP
	MOVC  A,@A+DPTR
	CJNE  A,#-1,UR_MRPl2
	MOV   DPTR,#MR_UIAM
UR_MRPl1:MOVX A,@DPTR
UR_MRPl2:MOV  R1,A
	MOV   A,#OMR_AP
	CALL  MR_GPA1
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	SETB  EA
	MOVX  A,@DPTR
	MOV   R6,A
	MOV   R7,#0
	JNB   ACC.7,UR_MRPl5
	DEC   R7
UR_MRPl5:CLR  F0
	MOV   R0,#0		; z motoru na logicke souradnice
	CALL  SCL_MR		; prevod meritka
	MOV   A,#1
	RET

RSW_MRPl:MOVX A,@DPTR
	MOV   R3,A		; typ pohybu (absolutne/relativne)
	MOV   DPTR,#MR_RSAM
	SJMP  UW_MRPl1

; Spusti pohyb regulatoru [DPTR+OU_DP] na polohu R4567
UW_MRPl:MOV   A,#OU_DP+1
	MOVC  A,@A+DPTR         ; typ pohybu (absolutne/relativne)
	MOV   R3,A
	MOV   A,#OU_DP
	MOVC  A,@A+DPTR         ; cislo regulatoru
	CJNE  A,#-1,UW_MRPl2
	MOV   DPTR,#MR_UIAM
UW_MRPl1:MOVX A,@DPTR
UW_MRPl2:MOV  R1,A
%IF(1) THEN (
	CLR   F0
	MOV   R0,#1		; z logickych na motor souradnice
	MOV   A,R3
	PUSH  ACC
	CALL  SCL_MR		; prevod meritka
	POP   ACC
	MOV   R3,A
	JB    F0,UW_MRPl5
	MOV   A,R6
	MOV   C,ACC.7
	XCH   A,R7
	ADDC  A,#0
	JNZ   UW_MRPl5
	MOV   A,R5
	MOV   R6,A
	MOV   A,R4
	MOV   R5,A
	MOV   R4,#0
	CALL  GO_GEPT
	RET
UW_MRPl5:SETB F0
	RET
)ELSE(
	MOV   A,R5
	MOV   R7,A
	MOV   A,R4
	MOV   R6,A
	CLR   A
	MOV   R4,A
	MOV   R5,A
	CLR   F0
	CALL  GO_GEPT
	RET
)FI

; Vypocte adresu parametru regulatoru
; [DPTR]    .. offset parametru
; [MR_RSAM] .. urceni regulatoru
MR_RSCA:MOVX  A,@DPTR
	MOV   R0,A
	MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	MOV   A,R0
	JMP   MR_GPA1

; Vypocte adresu parametru regulatoru
; [DPTR+OU_DP+0] .. cislo regulatoru nebo -1 pro reg [MR_UIAM]
; [DPTR+OU_DP+1] .. offset parametru regulatoru
MR_UICA:MOV   A,#OU_DP+1
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OU_DP
	MOVC  A,@A+DPTR
	CJNE  A,#-1,MR_UIC4
	MOV   DPTR,#MR_UIAM
	MOVX  A,@DPTR
MR_UIC4:MOV   R1,A
	MOV   A,R0
	JMP   MR_GPA1

RSR_MRCi:CALL MR_RSCA
	SJMP  UR_MRCi1

UR_MRCi:CALL  MR_UICA
UR_MRCi1:MOVX A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   A,#1
	CLR   F0
	RET

RSW_MRCi:CALL MR_RSCA
	SJMP  UW_MRCi1

UW_MRCi:CALL  MR_UICA
UW_MRCi1:MOV  C,EA
	MOV   A,R4
	CLR   EA
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOV   EA,C
	MOVX  @DPTR,A
	CLR   F0
	RET

RSR_MRb:CALL  MR_RSCA
	SJMP  UR_MRb1

UR_MRb:	CALL  MR_UICA
UR_MRb1:MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#0
	RET

RSW_MRb:CALL  MR_RSCA
	SJMP  UW_MRb1

UW_MRb:	CALL MR_UICA
UW_MRb1:MOV   A,R4
	MOVX  @DPTR,A
	RET

UW_Mb:	MOV   A,#OU_DP
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OU_DP+1
	MOVC  A,@A+DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOV   A,R4
	MOVX  @DPTR,A
	CLR   F0
	RET

STP_ALL:CLR   FL_DOAMP
	MOV   R1,#0
STP_ALL1:CALL STP_GEP
	INC   R1
	MOV   A,R1
	CJNE  A,#REG_N,STP_ALL1
	RET

AMP_SW	BIT   P4.0

AMP_SWT:MOV   R4,#0
	MOV   DPTR,#AMP_SWD
	MOVX  A,@DPTR
	MOV   R0,A
	RL    A
	RL    A
	MOV   C,AMP_SW
	CPL   C
	MOV   ACC.2,C
	ANL   A,#7
	XCH   A,R0
	; R0 .. SW X Y
	; A  .. X Y counter
	; R4 .. akce
AMP_SW1:DJNZ  R0,AMP_SW2	; 0 0 1
	DEC   A			; Casovani do 0
	JB    ACC.6,AMP_SW2
	MOV   R4,#1
AMP_SW2:DJNZ  R0,AMP_SW3	; 0 1 0
	MOV   A,#000H		; Byl zakmit do 1
AMP_SW3:DJNZ  R0,AMP_SW4	; 0 1 1
	MOV   A,#07FH		; Pocatek do 0
AMP_SW4:DJNZ  R0,AMP_SW5	; 1 0 0
	MOV   A,#080H		; Pocatek do 1
AMP_SW5:DJNZ  R0,AMP_SW6	; 1 0 1
	MOV   A,#0C0H		; Byl zakmit do 0
AMP_SW6:DJNZ  R0,AMP_SW7	; 1 1 0
	INC   A			; Casovani do 1
	JNB   ACC.6,AMP_SW7
	MOV   R4,#2
AMP_SW7:MOVX  @DPTR,A
	MOV   A,R4
	JB    ACC.0,STOP_AMP
	JB    ACC.1,START_AMP
AMP_SW9:RET

START_AMP:
	CLR   FL_CLAMP
	SETB  FL_DOAMP
	RET

STOP_AMP:
	CALL  CER_ALL
	SETB  FL_CLAMP
	RET

AMP_SPS:MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR
	INC   DPTR
	MOV   R0,A
	MOVX  A,@DPTR
	INC   DPTR
	XCH   A,R0
	MOV   R2,DPL
	MOV   R3,DPH
	RET

AMP_SPP:CLR   EA
	CALL  AMP_SPS
	MOV   DPTR,#REG_A+OREG_A+OMR_AMP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	CALL  AMP_SPS
	MOV   DPTR,#REG_A+OREG_A+OMR_ACC
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	CALL  AMP_SPS
	MOV   DPTR,#REG_A+OREG_A+OMR_MS
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	SETB  EA
	CALL  AMP_SPS
	ANL   LED_FLG,A
	MOV   A,R0
	ORL   LED_FLG,A
	RET

AMP_PP0:%W    (300)	; AMP max 83.0
	%W    (80)	; ACC max 100
	%W    (8000)	; SPD max 100.00
	DB    NOT GRAD_LMSK,0

AMP_PP1:%W    (200)	; AMP max 83.0
	%W    (90)	; ACC max 100
	%W    (9000)	; SPD max 100.00
	DB    NOT GRAD_LMSK,G_A_LMSK

AMP_PP2:%W    (300)	; AMP max 83.0
	%W    (90)	; ACC max 100
	%W    (9000)	; SPD max 100.00
	DB    NOT GRAD_LMSK,G_B_LMSK

AMP_PP3:%W    (500)	; AMP max 83.0
	%W    (50)	; ACC max 100
	%W    (5000)	; SPD max 100.00
	DB    NOT GRAD_LMSK,G_C_LMSK

CLR_ALL:CLR   FL_DOAMP
	MOV   R1,#0
CLR_ALL1:CALL CLR_GEP
	INC   R1
	MOV   A,R1
	CJNE  A,#REG_N,CLR_ALL1
	CLR   MR_FLG.BMR_ERR
	RET

CER_ALL:MOV   R1,#0
CER_ALL1:CALL CER_GEP
	INC   R1
	MOV   A,R1
	CJNE  A,#REG_N,CER_ALL1
	CLR   MR_FLG.BMR_ERR
	RET

; Odpojeni generatoru a regulatoru
REL_ALL:CLR   FL_DOAMP
	MOV   R1,#0
REL_ALL1:CALL REL_GEP
	INC   R1
	MOV   A,R1
	CJNE  A,#REG_N,REL_ALL1
	CLR   MR_FLG.BMR_ERR
	RET

; Cte hodnotu ze SDATA
UR_MSPCb:MOV  A,#OU_DP
	MOVC  A,@A+DPTR
	MOV   R1,A
	MOV   DPTR,#TMP_MSPC
	MOV   A,#0E5H         ; MOV  A,dir
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#022H         ; RET
	MOVX  @DPTR,A
	DB    12H	      ; CALL TMP_MSPC
	DW    TMP_MSPC
	MOV   R4,A
	MOV   R5,#0
	CLR   F0
	MOV   A,#1
	RET

; Primy zapis PWM
UW_PWMi:CALL  MR_UICA
	JMP   MR_SENEP

; *******************************************************************
; Globalni udalosti

; Posle globalni udalost
GLOB_RQ23:MOV A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	MOV   R7,#ET_GLOB
	JMP   EV_POST

; Zpracovani globalnich udalosti
GLOB_DO:MOV   A,R4
	MOV   R7,A
	MOV   A,R5
	MOV   R4,A
	MOV   DPTR,#GLOB_SF1
	JMP   SEL_FNC

; Globalni udalosti

GL_KEYLOCK  SET  1
GL_KEYEN    SET  2

; Tabulka globalnich udalosti

GLOB_SF1:DB   GL_KEYLOCK
	%W    (UT_GR30)
	%W    (GR_RQ23)

	DB    GL_KEYEN
	%W    (UT_GR10)
	%W    (GR_RQ23)

	DB    0

; *******************************************************************
; Komunikace s uzivatelem

RSEG	KZ____X

UT_UIAD:DS    40
UT_DATA:DS    40

RSEG	KZ____C

UT_INIT:CLR   D4LINE
	SETB  FL_CMAV
	MOV   DPTR,#UI_MV_SX
	MOV   A,#20
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#2
	MOVX  @DPTR,A
	MOV   DPTR,#UT_UIAD
	MOV   UI_AD,DPL
	MOV   UI_AD+1,DPH
	CLR   A
	MOV   DPTR,#EV_BUF
	MOVX  @DPTR,A
	MOV   DPTR,#GR_ACT
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#REF_TIM
	MOVX  @DPTR,A
	RET

UT_TREF:MOV   DPTR,#REF_TIM
	MOVX  A,@DPTR
	JNZ   UT_TRE1
	MOV   DPTR,#REF_PER
	MOVX  A,@DPTR
	MOV   DPTR,#REF_TIM
	MOVX  @DPTR,A
	MOV   A,#1
	RET
UT_TRE1:CLR   A
	RET

UT:     CALL  UT_INIT
	MOV   R7,#ET_RQGR
	%LDR45i(UT_GR10)
	CALL  EV_POST

UT_ML:  CALL  AMP_SWT
	CALL  EV_GET
	JZ    UT_ML50
	CJNE  R7,#ET_GLOB,UT_ML45
	CALL  GLOB_DO
	SJMP  UT_ML
UT_ML45:CALL  EV_DO
	JMP   UT_ML
UT_ML50:JNB   FL_RS232,UT_ML53
	CALL  RS_POOL		; Smycka zpracovani prikazu RS232
	JNZ   UT_ML55
	MOV   A,MR_FLG
	MOV   C,ACC.BMR_ERR
	ANL   A,#MMR_BSY
	MOV   ACC.7,C
	CALL  RS_RDYSND		; Vysila R! a FAILED! po RS232
	JZ    UT_ML55
	JNB   FL_RDYR1,UT_ML55
	CLR   RSF_RDYR		; Nepokracuj v informaci o stavu
	SJMP  UT_ML55
UT_ML53:CALL  UD_OI
    %IF(%WITH_ULAN)THEN(
	JB    uLF_INE,UT_ML55
    )FI
	JB    UDF_RDP,UT_ML57
UT_ML55:CALL  UT_TREF
	JZ    UT_ML60
	JNB   FL_RS232,UT_ML57
	CALL  UD_REFR
UT_ML57:CLR   UDF_RDP
	SETB  FL_REFR
	SJMP  UT_ML65
UT_ML60:CALL  UT_ULED
UT_ML65:JMP   UT_ML

UT_ULED:
;       MOV   DPTR,#IP1_ST+1
;	MOV   R2,#1 SHL LFB_PUMP1
;	CALL  UT_USTS	; CALL  UT_UST1

	MOV   A,MR_FLG
	MOV   R2,#20H
	CALL  UT_MRST
%IF (0) THEN (
	MOV   DPTR,#REG_A+OREG_A+OMR_FLG
	MOVX  A,@DPTR
	MOV   R2,#G_A_LMSK
	CALL  UT_MRST
%IF(%REG_COUNT GE 2) THEN(
	MOV   DPTR,#REG_A+OREG_B+OMR_FLG
	MOVX  A,@DPTR
	MOV   R2,#G_B_LMSK
	CALL  UT_MRST
%IF(%REG_COUNT GE 3) THEN(
	MOV   DPTR,#REG_A+OREG_C+OMR_FLG
	MOVX  A,@DPTR
	MOV   R2,#G_C_LMSK
	CALL  UT_MRST
)FI )FI )FI
	JMP   LEDWR

UT_MRST:JB    ACC.BMR_ERR,UT_UST7
	JB    ACC.BMR_BSY,UT_UST3
	SJMP  UT_UST5

UT_USTS:MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
UT_UST1:MOV   A,R5
	JB    ACC.7,UT_UST7
	ORL   A,R4
UT_UST2:JZ    UT_UST5
UT_UST3:MOV   A,R2		; On
	CPL   A
	ANL   LEB_FLG,A
	CPL   A
	ORL   LED_FLG,A
	RET
UT_UST5:MOV   A,R2		; Off
	CPL   A
	ANL   LEB_FLG,A
	ANL   LED_FLG,A
	RET
UT_UST7:MOV   A,R2		; Error
	ORL   LEB_FLG,A
	RET

; Nastavi MR_UIAM na hodnotu v R2
UT_SAMG:%LDR45i(UT_GR20)
	MOV   R7,#ET_RQGR
	CALL  EV_POST
UT_SAM:	MOV   A,R2
	MOV   DPTR,#MR_UIAM
	MOVX  @DPTR,A
	RET

; ---------------------------------
; Definice klaves

UT_SF1:	DB    K_H_A
	DB    0,0
	%W    (UT_SAMG)

%IF(%REG_COUNT GE 2) THEN(
	DB    K_H_B
	DB    1,0
	%W    (UT_SAMG)

%IF(%REG_COUNT GE 3) THEN(
	DB    K_H_C
	DB    2,0
	%W    (UT_SAMG)

%IF(%REG_COUNT GE 4) THEN(
	DB    K_H_D
	DB    3,0
	%W    (UT_SAMG)

%IF(%REG_COUNT GE 5) THEN(
	DB    K_H_E
	DB    4,0
	%W    (UT_SAMG)

%IF(%REG_COUNT GE 6) THEN(
	DB    K_H_F
	DB    5,0
	%W    (UT_SAMG)
)FI )FI )FI )FI )FI

	DB    K_LIST
	%W    (UT_GR10)
	%W    (GR_RQ23)

	DB    K_PURGE
	%W    (0)
	%W    (CER_ALL)

	DB    K_RUN
	%W    (0)
	%W    (START_AMP)

	DB    K_STOP
	%W    (0)
	%W    (STP_ALL)

	DB    K_END
	%W    (0)
	%W    (CLR_ALL)

	DB    K_HOLD
	%W    (0)
	%W    (GO_FC_A)

	DB    K_HOLD
	%W    (0)
	%W    (GO_HHT)

	DB    K_PROG
	%W    (0)
	%W    (IHEXLD)

UT_SFTN:DB    K_RIGHT
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    K_LEFT
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    K_DOWN
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    K_UP
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

UT_SF0:	DB    0

; ---------------------------------
; Zakladni display
UT_GR10:DS    UT_GR10+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR10+OGR_BTXT-$
	%W    (UT_GT10)
	DS    UT_GR10+OGR_STXT-$
	%W    (0)
	DS    UT_GR10+OGR_HLP-$
	%W    (0)
	DS    UT_GR10+OGR_SFT-$
	%W    (UT_SF10)
	DS    UT_GR10+OGR_PU-$
	%W    (UT_U1001)
	%W    (UT_U1002)
	%W    (UT_U1003)
	%W    (0)

UT_GT10:DB    'Amp',C_NL
	DB    'Acc',C_NL
	DB    'Spd',0

UT_SF10:
	DB    K_RUN
	%W    (0)
	%W    (START_AMP)

	DB    K_START
	%W    (0)
	%W    (START_AMP)

	DB    K_STOP
	%W    (0)
	%W    (STOP_AMP)

	DB    K_HOLD
	%W    (0)
	%W    (GO_FC_A)

	DB    K_PURGE
	%W    (AMP_PP0)
	%W    (AMP_SPP)

	DB    K_H_A
	%W    (AMP_PP1)
	%W    (AMP_SPP)

	DB    K_H_B
	%W    (AMP_PP2)
	%W    (AMP_SPP)

	DB    K_H_C
	%W    (AMP_PP3)
	%W    (AMP_SPP)

	DB    K_LIST
	DB    0,0
	%W    (UT_SAMG)

	DB    -1
	%W    (UT_SFTN)

UT_U1001:
	DS    UT_U1001+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1001+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1001+OU_X-$
	DB    4,0,6,1
	DS    UT_U1001+OU_HLP-$
	%W    (0)
	DS    UT_U1001+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1001+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U1001+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    0,OMR_AMP		; DP
	DS    UT_U1001+OU_I_F-$
	DB    001H		; format I_F
	%W    (0)		; I_L
	%W    (830)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1002:
	DS    UT_U1002+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1002+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1002+OU_X-$
	DB    4,1,6,1
	DS    UT_U1002+OU_HLP-$
	%W    (0)
	DS    UT_U1002+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1002+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U1002+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    0,OMR_ACC		; DP
	DS    UT_U1002+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (100)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1003:
	DS    UT_U1003+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1003+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1003+OU_X-$
	DB    4,2,6,1
	DS    UT_U1003+OU_HLP-$
	%W    (0)
	DS    UT_U1003+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1003+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U1003+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    0,OMR_MS		; DP
	DS    UT_U1003+OU_I_F-$
	DB    002H		; format I_F
	%W    (0)		; I_L
	%W    (10000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1021:
	DS    UT_U1021+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1021+OU_MSK-$
	DB    0
	DS    UT_U1021+OU_X-$
	DB    10,0,3,1
	DS    UT_U1021+OU_HLP-$
	%W    (0)
	DS    UT_U1021+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1021+OU_A_RD-$
	%W    (UR_MSPCb)	; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1021+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    P1,0		; DP
	DS    UT_U1021+OU_M_S-$
	%W    (0)
	DS    UT_U1021+OU_M_P-$
	%W    (0)
	DS    UT_U1021+OU_M_F-$
	%W    (0)
	DS    UT_U1021+OU_M_T-$
	%W    (001H)
	%W    (000H)
	%W    (UT_U1020T0)
	%W    (001H)
	%W    (001H)
	%W    (UT_U1020T1)
	%W    (0)
UT_U1020T0:DB    '',0
UT_U1020T1:DB    'MRK',0

; ---------------------------------
; Zadavani konstant
UT_GR20:DS    UT_GR20+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR20+OGR_BTXT-$
	%W    (UT_GT20)
	DS    UT_GR20+OGR_STXT-$
	%W    (0)
	DS    UT_GR20+OGR_HLP-$
	%W    (0)
	DS    UT_GR20+OGR_SFT-$
	%W    (UT_SF20)
	DS    UT_GR20+OGR_PU-$
	%W    (UT_U2000)
	%W    (UT_U2001)
	%W    (UT_U2002)
	%W    (UT_U2003)
	%W    (UT_U2004)
	%W    (UT_U2005)
	%W    (UT_U2006)
	%W    (UT_U2007)
	%W    (UT_U2008)
	%W    (UT_U2009)
	%W    (UT_U2010)
	%W    (UT_U2011)
	%W    (UT_U2012)
	%W    (UT_U2013)
	%W    (UT_U2014)
	%W    (UT_U2015)
	%W    (0)

UT_GT20:DB    'Motor',C_NL
	DB    'POS',C_NL
	DB    'MS',C_NL
	DB    'MA',C_NL
	DB    'ME',C_NL
	DB    'P',C_NL
	DB    'I',C_NL
	DB    'D',C_NL
	DB    'S1',C_NL
	DB    'S2',C_NL
	DB    'FLG',C_NL
	DB    'CFG',C_NL
	DB    'S MUL',C_NL
	DB    'S DIV',C_NL
	DB    'AMP',C_NL
	DB    'PWM',0

UT_SF20:DB    -1
	%W    (UT_SF1)

UT_U2000:
	DS    UT_U2000+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U2000+OU_MSK-$
	DB    0 ; UFM_FOC
	DS    UT_U2000+OU_X-$
	DB    8,0,2,1
	DS    UT_U2000+OU_HLP-$
	%W    (0)
	DS    UT_U2000+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2000+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U2000+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (MR_UIAM)		; DP
	DS    UT_U2000+OU_M_S-$
	%W    (0)
	DS    UT_U2000+OU_M_P-$
	%W    (0)
	DS    UT_U2000+OU_M_F-$
	%W    (0)
	DS    UT_U2000+OU_M_T-$
	%W    (0FFH)
	%W    (000H)
	%W    (UT_U2000TA)
	%W    (0FFH)
	%W    (001H)
	%W    (UT_U2000TB)
	%W    (0FFH)
	%W    (002H)
	%W    (UT_U2000TC)
	%W    (0FFH)
	%W    (003H)
	%W    (UT_U2000TD)
	%W    (0FFH)
	%W    (004H)
	%W    (UT_U2000TE)
	%W    (0FFH)
	%W    (005H)
	%W    (UT_U2000TF)
	%W    (0)
UT_U2000TA:DB 'A',0
UT_U2000TB:DB 'B',0
UT_U2000TC:DB 'C',0
UT_U2000TD:DB 'D',0
UT_U2000TE:DB 'E',0
UT_U2000TF:DB 'F',0

UT_U2001:
	DS    UT_U2001+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2001+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2001+OU_X-$
	DB    3,1,8,1
	DS    UT_U2001+OU_HLP-$
	%W    (0)
	DS    UT_U2001+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2001+OU_A_RD-$
	%W    (UR_MRPl)		; A_RD
	%W    (UW_MRPl)		; A_WR
	DS    UT_U2001+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,0		; DP
	DS    UT_U2001+OU_I_F-$
	DB    0C3H		; format I_F
	%W    (8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2002:
	DS    UT_U2002+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2002+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2002+OU_X-$
	DB    5,2,6,1
	DS    UT_U2002+OU_HLP-$
	%W    (0)
	DS    UT_U2002+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2002+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2002+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_MS		; DP
	DS    UT_U2002+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2003:
	DS    UT_U2003+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2003+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2003+OU_X-$
	DB    5,3,6,1
	DS    UT_U2003+OU_HLP-$
	%W    (0)
	DS    UT_U2003+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2003+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2003+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_ACC	; DP
	DS    UT_U2003+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2004:
	DS    UT_U2004+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2004+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2004+OU_X-$
	DB    5,4,6,1
	DS    UT_U2004+OU_HLP-$
	%W    (0)
	DS    UT_U2004+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2004+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2004+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_ME		; DP
	DS    UT_U2004+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2005:
	DS    UT_U2005+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2005+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2005+OU_X-$
	DB    5,5,6,1
	DS    UT_U2005+OU_HLP-$
	%W    (0)
	DS    UT_U2005+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2005+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2005+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_P		; DP
	DS    UT_U2005+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2006:
	DS    UT_U2006+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2006+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2006+OU_X-$
	DB    5,6,6,1
	DS    UT_U2006+OU_HLP-$
	%W    (0)
	DS    UT_U2006+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2006+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2006+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_I		; DP
	DS    UT_U2006+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2007:
	DS    UT_U2007+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2007+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2007+OU_X-$
	DB    5,7,6,1
	DS    UT_U2007+OU_HLP-$
	%W    (0)
	DS    UT_U2007+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2007+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2007+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_D		; DP
	DS    UT_U2007+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2008:
	DS    UT_U2008+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2008+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2008+OU_X-$
	DB    5,8,6,1
	DS    UT_U2008+OU_HLP-$
	%W    (0)
	DS    UT_U2008+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2008+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2008+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_S1		; DP
	DS    UT_U2008+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2009:
	DS    UT_U2009+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2009+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2009+OU_X-$
	DB    5,9,6,1
	DS    UT_U2009+OU_HLP-$
	%W    (0)
	DS    UT_U2009+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2009+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2009+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_S2		; DP
	DS    UT_U2009+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2010:
	DS    UT_U2010+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2010+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2010+OU_X-$
	DB    5,10,6,1
	DS    UT_U2010+OU_HLP-$
	%W    (0)
	DS    UT_U2010+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2010+OU_A_RD-$
	%W    (UR_MRb)		; A_RD
	%W    (UW_MRb)		; A_WR
	DS    UT_U2010+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_FLG	; DP
	DS    UT_U2010+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2011:
	DS    UT_U2011+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2011+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2011+OU_X-$
	DB    5,11,6,1
	DS    UT_U2011+OU_HLP-$
	%W    (0)
	DS    UT_U2011+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2011+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2011+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_CFG	; DP
	DS    UT_U2011+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (0FFFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2012:
	DS    UT_U2012+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2012+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2012+OU_X-$
	DB    5,12,6,1
	DS    UT_U2012+OU_HLP-$
	%W    (0)
	DS    UT_U2012+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2012+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2012+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_SCM	; DP
	DS    UT_U2012+OU_I_F-$
	DB    80H		; format I_F
	%W    (-0FFH)		; I_L
	%W    ( 0FFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2013:
	DS    UT_U2013+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2013+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2013+OU_X-$
	DB    5,13,6,1
	DS    UT_U2013+OU_HLP-$
	%W    (0)
	DS    UT_U2013+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2013+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2013+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_SCD	; DP
	DS    UT_U2013+OU_I_F-$
	DB    80H		; format I_F
	%W    (-0FFH)		; I_L
	%W    ( 0FFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2014:
	DS    UT_U2014+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2014+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2014+OU_X-$
	DB    5,14,6,1
	DS    UT_U2014+OU_HLP-$
	%W    (0)
	DS    UT_U2014+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2014+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2014+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_AMP	; DP
	DS    UT_U2014+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (840)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2015:
	DS    UT_U2015+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2015+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2015+OU_X-$
	DB    5,15,6,1
	DS    UT_U2015+OU_HLP-$
	%W    (0)
	DS    UT_U2015+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2015+OU_A_RD-$
	%W    (NULL_A)		; A_RD
	%W    (UW_PWMi)		; A_WR
	DS    UT_U2015+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,0		; DP
	DS    UT_U2015+OU_I_F-$
	DB    82H		; format I_F
	%W    (-7FFFH)		; I_L
	%W    ( 7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Lockovany display
UT_GR30:DS    UT_GR30+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR30+OGR_BTXT-$
	%W    (UT_GT30)
	DS    UT_GR30+OGR_STXT-$
	%W    (0)
	DS    UT_GR30+OGR_HLP-$
	%W    (0)
	DS    UT_GR30+OGR_SFT-$
	%W    (UT_SF30)
	DS    UT_GR30+OGR_PU-$
	%W    (UT_U3001)
	%W    (UT_U3002)
	%W    (0)

UT_GT30:DB    'A            KEY',C_NL
	DB    'B           LOCK',C_NL
	DB    0

UT_SF30:DB    K_STOP
	%W    (0)
	%W    (STP_ALL)

	DB    K_END
	%W    (0)
	%W    (CLR_ALL)

	DB    0

UT_U3001:
	DS    UT_U3001+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U3001+OU_MSK-$
	DB    0
	DS    UT_U3001+OU_X-$
	DB    1,0,8,1
	DS    UT_U3001+OU_HLP-$
	%W    (0)
	DS    UT_U3001+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U3001+OU_A_RD-$
	%W    (UR_MRPl)		; A_RD
	%W    (UW_MRPl)		; A_WR
	DS    UT_U3001+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    0,0		; DP
	DS    UT_U3001+OU_I_F-$
	DB    0C3H		; format I_F
	%W    (8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U3002:
	DS    UT_U3002+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U3002+OU_MSK-$
	DB    0
	DS    UT_U3002+OU_X-$
	DB    1,1,8,1
	DS    UT_U3002+OU_HLP-$
	%W    (0)
	DS    UT_U3002+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U3002+OU_A_RD-$
	%W    (UR_MRPl)		; A_RD
	%W    (UW_MRPl)		; A_WR
	DS    UT_U3002+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    1,0		; DP
	DS    UT_U3002+OU_I_F-$
	DB    0C3H		; format I_F
	%W    (8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

END