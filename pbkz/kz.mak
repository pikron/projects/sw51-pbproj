#   Project file pro jednoosou regulaci
#         (C) Pisoft 1997

kz.obj	  : kz.asm config.h
	a51 kz.asm $(par) debug

mr_psys.obj : mr_psys.asm config.h
	a51   mr_psys.asm $(par)

pb_pshw.obj : pb_pshw.asm config.h
	a51   pb_pshw.asm $(par)

kz.	  : kz.obj mr_psys.obj pb_pshw.obj ..\pblib\pb.lib
	l51 kz.obj,mr_psys.obj,pb_pshw.obj,..\pblib\pb.lib code(09800H) xdata(8000H) ramsize(100h) ixref

kz.hex	    : kz.
	ohs51 kz

	  : kz.hex
#	sendhex kz.hex /p3 /m3 /t2 /g40960 /b16000
#	sendhex kz.hex /p4 /m3 /t2 /g36864 /b16000
	unixcmd -d ul_sendhex -m 4 -g 0
	pause
	unixcmd -d ul_sendhex kz.hex -m 4 -g 0x9800
