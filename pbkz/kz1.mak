#   Project file pro jednoosou regulaci
#         (C) Pisoft 1997

kz.obj	  : kz.asm config.h
	a51 kz.asm $(par) debug

mr_psys.obj : mr_psys.asm config.h
	a51   mr_psys.asm $(par)

pb_pshw.obj : pb_pshw.asm config.h
	a51   pb_pshw.asm $(par)


#	  : mars.obj
#	del mars.

kz.	  : kz.obj mr_psys.obj pb_pshw.obj ..\pblib\pb.lib
	l51 kz.obj,mr_psys.obj,pb_pshw.obj,..\pblib\pb.lib code(0A000H) xdata(8000H) ramsize(100h) ixref

kz.hex	    : kz.
	ohs51 kz
#	del kz.
#       eprem kz.hex -s80 -1

	  : kz.hex
	com_shex 2 mars.hex A000