;********************************************************************
;*              Multiosa regulace  - MR_PSYS.ASM                    *
;*                Modul rizeni a generovani polohy                  *
;*                  Stav ke dni 10. 8.1997                          *
;*                      (C) Pisoft 1996                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_AL)
$INCLUDE(%INCH_MR_DEFS)
$INCLUDE(%INCH_MR_PDEFS)
$LIST

EXTRN	NUMBER(REG_LEN,REG_A,REG_N)
EXTRN	DATA(MR_BAS,MR_FLG,MR_FLGA)

EXTRN	CODE(VR_REG,VR_REG1,HH_MARK,MR_SENEP,IRC_WRP,GO_NOTIFY)

PUBLIC	MR_REG,MR_RNUL,MR_ZER,MR_GPSC,MR_GPSV,MR_CMEP
PUBLIC	MR_RDRP,MR_WRRP,MR_RDAP,CI_VG
PUBLIC	MR_GPA1,GO_GEP,GO_GEPT,GO_VG,GO_PRPC,GO_RSFT
PUBLIC	CLR_GEP,REL_GEP,CER_GEP,STP_GEP
PUBLIC  GO_HH,CD_HHCL,CD_HHST

MR____C SEGMENT CODE

; *******************************************************************

RSEG	MR____C

; Obecna rutina regulatoru
MR_REG:	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,#OMR_VRJ
	JMP   @A+DPTR

; Prazdny regulator
MR_RNUL:CLR   A
	MOV   R4,A
	MOV   R5,A
	RET

; Vynuluje pamet regulatoru
MR_ZER:	MOV   DPTR,#REG_A
	MOV   R3,#REG_N
MR_ZER1:MOV   R2,#REG_LEN
	CLR   A
MR_ZER2:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R2,MR_ZER2
	DJNZ  R3,MR_ZER1
	MOVX  @DPTR,A
	MOV   MR_FLG,#0
	MOV   R2,#REG_N		; Neskodne nastaveni vektoru
	MOV   DPTR,#REG_A+OMR_VRJ
MR_ZER3:MOV   A,#2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH MR_RNUL
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW  MR_RNUL
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH VR_REG
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW  VR_REG
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,DPL
	ADD   A,#REG_LEN-6
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#0
	MOV   DPH,A
	DJNZ  R2,MR_ZER3
	RET

; *******************************************************************
; Generatory pozadovane polohy

; R4567=OMR_RS+OMR_RP , meni R3

MR_GPSC:%MR_OFS2DP(OMR_RS)
	MOVX  A,@DPTR	; OMR_RS
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR	; OMR_RS+1
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR	; OMR_RS+2
	MOV   R3,A
	SJMP  MR_GPS5

; R4567=R45+OMR_RP , meni R3

MR_GPSV:%MR_OFS2DP(OMR_RS)
	MOV   A,R4
	MOVX  @DPTR,A	; OMR_RS
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A	; OMR_RS+1
	INC   DPTR
	MOV   R3,#0
	JNB   ACC.7,MR_GPS2
	DEC   R3
MR_GPS2:MOV   A,R3
	MOVX  @DPTR,A	; OMR_RS+2
MR_GPS5:MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,#OMR_RP
	MOVC  A,@A+DPTR	; OMR_RP
	ADD   A,R4
	MOV   R4,A
	MOV   A,#OMR_RP+1
	MOVC  A,@A+DPTR
	ADDC  A,R5
	MOV   R5,A
	MOV   A,#OMR_RP+2
	MOVC  A,@A+DPTR
	ADDC  A,R3
	MOV   R6,A
	MOV   A,#OMR_RP+3
	MOVC  A,@A+DPTR
	ADDC  A,R3
	MOV   R7,A
	RET

; Provede komparaci R4567 a OMR_GEP

MR_CMEP:MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	CLR   C
	MOV   A,#OMR_GEP
	MOVC  A,@A+DPTR
	SUBB  A,R4
	MOV   B,A
	MOV   A,#OMR_GEP+1
	MOVC  A,@A+DPTR
	SUBB  A,R5
	ORL   B,A
	MOV   A,#OMR_GEP+2
	MOVC  A,@A+DPTR
	SUBB  A,R6
	ORL   B,A
	MOV   A,#OMR_GEP+3
	MOVC  A,@A+DPTR
	SUBB  A,R7
	ORL   B,A
	INC   B
	DJNZ  B,MR_CME3
	RET
MR_CME3:CPL   C
	CPL   A
	ORL   A,#1
	RET

; Nacte do R4567 OMR_RP

MR_RDRP:MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,#OMR_RP
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_RP+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   A,#OMR_RP+2
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   A,#OMR_RP+3
	MOVC  A,@A+DPTR
	MOV   R7,A
	RET

; Ulozi R4567 do OMR_RP

MR_WRRP:%MR_OFS2DP(OMR_RP)
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	RET

; Nacte do R4567 OMR_AP

MR_RDAP:MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   R4,#0
	MOV   A,#OMR_AP+0
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   A,#OMR_AP+1
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   A,#OMR_AP+2
	MOVC  A,@A+DPTR
	MOV   R7,A
	RET

; Inicializuje pohyb konstantni rychlosti na polohu OMR_GEP

CI_GEP:	CALL  MR_RDRP
	CALL  MR_CMEP
	JZ    CI_GEP8
	MOV   C,OV
	XRL   A,PSW
	MOV   C,ACC.7
	SETB  MR_FLGA.BMR_BSY
	MOV   A,#OMR_MS
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_MS+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	JC    CI_GEP4
	CALL  NEGi
CI_GEP4:CALL  MR_GPSV
	%MR_OFS2DP(OMR_VG)	; dale pobezi CD_GEP
	MOV   A,#HIGH CD_GEP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW  CD_GEP
	MOVX  @DPTR,A
	JMP   CD_GEP2
CI_GEP8:JMP   CD_GEPE

; Pohyb konstantni rychlosti

CD_GEP:	JNB   MR_FLGA.BMR_BSY,CD_GEP9
	CALL  MR_GPSC
CD_GEP2:CALL  MR_CMEP
	JZ    CD_GEPE
	MOV   C,OV
	XRL   A,PSW
	XRL   A,R3
	JB    ACC.7,CD_GEP8
CD_GEPE:CLR   MR_FLGA.BMR_BSY	;
	MOV   A,#OMR_GEP
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_GEP+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   A,#OMR_GEP+2
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   A,#OMR_GEP+3
	MOVC  A,@A+DPTR
	MOV   R7,A
	%MR_OFS2DP(OMR_RS)
	CLR   A
	MOVX  @DPTR,A	; OMR_RS
	INC   DPTR
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	%MR_OFS2DP(OMR_VG)	; dale pobezi jen regulator
	MOV   A,#HIGH VR_REG
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW  VR_REG
	MOVX  @DPTR,A
CD_GEP8:%MR_OFS2DP(OMR_RP)
	MOV   A,R4	; OMR_RP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
CD_GEP9:JMP   VR_REG

; Nastavi vektor na generator R45
CI_VG:	%MR_OFS2DP(OMR_VG)
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R4
	MOVX  @DPTR,A
	RET

; *******************************************************************
; Najizdeni na polohu s trapezoidnim profilem rychlosti

OMR_TEP	SET   OMR_GEP+4
OMR_TSP	SET   OMR_GEP+8
OMR_TAC	SET   OMR_GEP+10

%DEFINE (TRDEB) (
  %IF(0) THEN (
	JBC   MR_FLGA.BMR_DBG,$+6
	JMP   VR_REG
  ) FI
)

; Porovna R4567 s OMR_TEP
; ======================
TR_BC1:	SETB  C         ; TEP-R4567-1 ?
	DB    74H	; MOV  A,n
TR_BC0: CLR   C		; TEP-R4567 ?
	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,#OMR_TEP
	MOVC  A,@A+DPTR
	SUBB  A,R4
	MOV   B,A
	MOV   A,#OMR_TEP+1
	MOVC  A,@A+DPTR
	SUBB  A,R5
	ORL   B,A
	MOV   A,#OMR_TEP+2
	MOVC  A,@A+DPTR
	SUBB  A,R6
	MOV   A,#OMR_TEP+3
	MOVC  A,@A+DPTR
	SUBB  A,R7
	RET

; TEP=R0123+R4567
; ===============
TR_RADB:%MR_OFS2DP(OMR_TEP)
	MOV   A,R4
	ADD   A,R0
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	ADDC  A,R1
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	ADDC  A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	ADDC  A,R3
	MOVX  @DPTR,A
	RET

; Priprava jizdy trapezoidem
; ==========================
CI_TR:  %TRDEB
	SETB  MR_FLGA.BMR_BSY	; Busy
	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,#OMR_ACC
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OMR_ACC+1
	MOVC  A,@A+DPTR		; R01=ACC
	MOV   R1,A
	%MR_OFS2DP(OMR_TAC)	; TAC = ACC
	MOV   A,R0
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A
	CALL  MR_RDRP
	CLR   C
	MOV   A,#OMR_GEP
	MOVC  A,@A+DPTR
	SUBB  A,R4
	MOV   R0,A
	MOV   B,A
	MOV   A,#OMR_GEP+1
	MOVC  A,@A+DPTR
	SUBB  A,R5
	MOV   R1,A
	ORL   B,A
	MOV   A,#OMR_GEP+2
	MOVC  A,@A+DPTR
	SUBB  A,R6
	MOV   R2,A
	ORL   B,A
	MOV   A,#OMR_GEP+3
	MOVC  A,@A+DPTR
	SUBB  A,R7
	MOV   R3,A
	ORL   B,A	; R4567=RP
	MOV   C,OV 	; R0123=GEP-RP
	XRL   A,PSW	; ACC.7 .. jede se dolu
	JB    ACC.7,CI_TR40
	MOV   A,B
	JNZ   CI_TR20
; nejede se nikam
	JMP   CD_TEND
; jede se nahoru
CI_TR20:CLR   C
	MOV   A,R3
	RRC   A
	MOV   R3,A
	MOV   A,R2
	RRC   A
	MOV   R2,A
	MOV   A,R1
	RRC   A
	MOV   R1,A
	MOV   A,R0
	RRC   A
	MOV   R0,A
	MOV   F0,C
	CALL  TR_RADB
	INC   DPTR
	MOV   C,F0
	MOV   ACC.7,C
	MOVX  @DPTR,A		; TSP = fract TEP
	%LDR45i(CD_TU10)
	SJMP  CI_TR90

; jede se dolu
CI_TR40:SETB  C
	MOV   A,R3
	RRC   A
	MOV   R3,A
	MOV   A,R2
	RRC   A
	MOV   R2,A
	MOV   A,R1
	RRC   A
	MOV   R1,A
	MOV   A,R0
	RRC   A
	MOV   R0,A
	MOV   F0,C
	CALL  TR_RADB
	INC   DPTR
	MOV   C,F0
	MOV   ACC.7,C
	MOVX  @DPTR,A		; TSP = fract TEP
	%LDR45i(CD_TD10)
CI_TR90:CALL  CI_VG
	%MR_OFS2DP(OMR_RS)	; RS = 0
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	JMP   VR_REG
CD_TEND_V1:JMP CD_TEND

; ----- Pohyb nahoru ------

; Usek po ktery dochazi k zrychlovani
CD_TU10:%TRDEB
	JNB   MR_FLGA.BMR_BSY,CD_TEND_V1
	JB    MR_FLGA.BMR_ERR,CD_TEND_V1
	CALL  MR_RDRP		; R4567 = RP
	MOV   A,#OMR_MS
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OMR_MS+1
	MOVC  A,@A+DPTR
	MOV   R1,A		; R01 = MS
	MOV   A,#OMR_TAC
	MOVC  A,@A+DPTR
	MOV   R2,A
	MOV   A,#OMR_TAC+1
	MOVC  A,@A+DPTR
	MOV   R3,A		; R23 = TAC
	MOV   A,#OMR_RS
	MOVC  A,@A+DPTR
	ADD   A,R2
	MOV   R2,A
	MOV   A,#OMR_RS+1
	MOVC  A,@A+DPTR
	ADDC  A,R3
	MOV   R3,A		; R23 = RS + TAC
	CLR   C
	MOV   A,R2
	SUBB  A,R0
	MOV   A,R3
	SUBB  A,R1		; (RS+TAC) - MS ?
	JNC   CD_TU12		; na max rychlosti
	MOV   A,R2
	ADD   A,R4
	MOV   R4,A
	MOV   A,R3
	ADDC  A,R5
	MOV   R5,A
	CLR   A
	ADDC  A,R6
	MOV   R6,A
	CLR   A
	ADDC  A,R7
	MOV   R7,A		; R4567 = RP + R23
	CALL  TR_BC1		; TEP-R4567-1 ?
	MOV   C,OV
	XRL   A,PSW
	JNB   ACC.7,CD_TWPS
	JMP   CD_TU15		; na pulce vzdalenosti
CD_TWPS:%MR_OFS2DP(OMR_RP)
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A		; RP = R4567
	INC   DPTR
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	INC   DPTR		; RS = R23
	MOV   R0,#0
	JNB   ACC.7,CD_TWS3
	DEC   R0
CD_TWS3:MOV   A,R0
	MOVX  @DPTR,A
	JMP   VR_REG
; Jsme na max rychlosti, R4567=RP, R23=RS+TAC, R01 = MS
CD_TU12:MOV   A,#OMR_RS
	MOVC  A,@A+DPTR		; RS
	MOV   R2,A
	MOV   A,#OMR_RS+1
	MOVC  A,@A+DPTR		; RS+1
	MOV   R3,A
	MOV   A,#OMR_TSP	; fract TEP v TSP.7
	MOVC  A,@A+DPTR
	RLC   A			; TEP=2*TEP-R4567
	%MR_OFS2DP(OMR_TEP)
	MOVX  A,@DPTR		; TEP
	RLC   A
	MOV   B.0,C
	CLR   C
	SUBB  A,R4
	MOV   B.1,C
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR		; TEP + 1
	MOV   C,B.0
	RLC   A
	MOV   B.0,C
	MOV   C,B.1
	SUBB  A,R5
	MOV   B.1,C
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR		; TEP + 2
	MOV   C,B.0
	RLC   A
	MOV   B.0,C
	MOV   C,B.1
	SUBB  A,R6
	MOV   B.1,C
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR		; TEP + 3
	MOV   C,B.0
	RLC   A
	MOV   B.0,C
	MOV   C,B.1
	SUBB  A,R7
	MOV   B.1,C
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	XCH   A,R2
	MOVX  @DPTR,A		; TSP = R23
	INC   DPTR
	MOV   A,R1              ; R23 = MS
	XCH   A,R3
	MOVX  @DPTR,A
	%MR_OFS2DP(OMR_VG)
	MOV   A,#HIGH CD_TU30
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW  CD_TU30
	MOVX  @DPTR,A
	JMP   CD_TAPS
; Dojeti do poloviny
CD_TU15:MOV   A,#OMR_TSP	; RP = 2*TEP-RP
	MOVC  A,@A+DPTR		; nejnizsi bit TEP
	RLC   A
	MOV   A,#OMR_TEP
	MOVC  A,@A+DPTR		; TEP
	RLC   A
	MOV   R4,A
	MOV   A,#OMR_TEP+1
	MOVC  A,@A+DPTR		; TEP+1
	RLC   A
	MOV   R5,A
	MOV   A,#OMR_TEP+2
	MOVC  A,@A+DPTR		; TEP+2
	RLC   A
	MOV   R6,A
	MOV   A,#OMR_TEP+3
	MOVC  A,@A+DPTR		; TEP+3
	RLC   A
	MOV   R7,A
	CLR   C
	%MR_OFS2DP(OMR_RP)
	MOVX  A,@DPTR
	XCH   A,R4
	SUBB  A,R4
	MOVX  @DPTR,A		; RP
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R5
	SUBB  A,R5
	MOVX  @DPTR,A		; RP+1
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R6
	SUBB  A,R6
	MOVX  @DPTR,A		; RP+2
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R7
	SUBB  A,R7
	MOVX  @DPTR,A		; RP+3
	INC   DPTR
	MOV   A,R2
	MOVX  @DPTR,A		; RS
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A		; RS+1
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A		; RS+2
	%LDR45i(CD_TU60)
	CALL  CI_VG
	JMP   VR_REG

; Usek po ktery se jede konstantni rychlosti
CD_TU30:%TRDEB
	JNB   MR_FLGA.BMR_BSY,CD_TEND_V2
	JB    MR_FLGA.BMR_ERR,CD_TEND_V2
	CALL  MR_RDRP		; R4567 = RP
	CALL  MR_GPSC		; R4567 = RP + RS
	CALL  TR_BC1		; TEP-R4567-1 ?
	MOV   C,OV
	XRL   A,PSW
	JB    ACC.7,CD_TU35	; na zacatku zpomaleni
	CALL  MR_WRRP
	JMP   VR_REG
; Prechod na zpomaleni
CD_TU35:MOV   A,#OMR_TEP
	MOVC  A,@A+DPTR		; TEP
	MOV   R4,A
	MOV   A,#OMR_TEP+1
	MOVC  A,@A+DPTR		; TEP+1
	MOV   R5,A
	MOV   A,#OMR_TEP+2
	MOVC  A,@A+DPTR		; TEP+2
	MOV   R6,A
	MOV   A,#OMR_TEP+3
	MOVC  A,@A+DPTR		; TEP+3
	MOV   R7,A
	MOV   A,#OMR_TSP
	MOVC  A,@A+DPTR		; TSP
	MOV   R2,A
	MOV   A,#OMR_TSP+1
	MOVC  A,@A+DPTR		; TSP+1
	MOV   R3,A
	%MR_OFS2DP(OMR_VG)
	MOV   A,#HIGH CD_TU50
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW  CD_TU50
	MOVX  @DPTR,A
	JMP   CD_TWPS
CD_TEND_V2:JMP CD_TEND

; Prvni krok brzdeni
CD_TU50:%TRDEB
	JNB   MR_FLGA.BMR_BSY,CD_TEND_V2
	JB    MR_FLGA.BMR_ERR,CD_TEND_V2
	%LDR45i(CD_TU60)
	CALL  CI_VG
	CALL  MR_RDRP
	MOV   A,#OMR_TSP
	MOVC  A,@A+DPTR
	MOV   R2,A
	MOV   A,#OMR_TSP+1
	MOVC  A,@A+DPTR
	MOV   R3,A
	ORL   A,R2
	JZ    CD_TEND_V2	; jiz jsme v cili
CD_TAPS:%MR_OFS2DP(OMR_RP)
	MOV   A,R4
	ADD   A,R2
	MOVX  @DPTR,A		; RP
	INC   DPTR
	MOV   A,R5
	ADDC  A,R3
	MOVX  @DPTR,A		; RP+1
	INC   DPTR
	MOV   A,R3
	MOV   R0,#0
	JNB   ACC.7,CD_TAP1
	DEC   R0
CD_TAP1:MOV   A,R6
	ADDC  A,R0
	MOVX  @DPTR,A		; RP+2
	INC   DPTR
	MOV   A,R7
	ADDC  A,R0
	MOVX  @DPTR,A		; RP+3
	INC   DPTR
	MOV   A,R2
	MOVX  @DPTR,A		; RS
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A		; RS+1
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A		; RS+2
	JMP   VR_REG

; Usek po ktery dochazi k zpomalovani
CD_TU60:%TRDEB
	JNB   MR_FLGA.BMR_BSY,CD_TEND_V2
	JB    MR_FLGA.BMR_ERR,CD_TEND_V2
	CALL  MR_RDRP
	MOV   A,#OMR_TAC
	MOVC  A,@A+DPTR
	MOV   R2,A
	MOV   A,#OMR_TAC+1
	MOVC  A,@A+DPTR
	MOV   R3,A
	MOV   A,#OMR_RS
	MOVC  A,@A+DPTR
	CLR   C
	SUBB  A,R2
	MOV   R2,A
	MOV   A,#OMR_RS+1
	MOVC  A,@A+DPTR
	SUBB  A,R3
	MOV   R3,A
	ORL   A,R2
	JNZ   CD_TU63
	JMP   CD_TEND
CD_TU63:JMP   CD_TAPS

; ----- Pohyb dolu --------

; Usek po ktery dochazi k zrychlovani
CD_TD10:%TRDEB
	JNB   MR_FLGA.BMR_BSY,CD_TEND_V2
	JB    MR_FLGA.BMR_ERR,CD_TEND_V2
	CALL  MR_RDRP		; R4567 = RP
	MOV   A,#OMR_MS
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OMR_MS+1
	MOVC  A,@A+DPTR
	MOV   R1,A		; R01 = MS
	MOV   A,#OMR_TAC
	MOVC  A,@A+DPTR
	MOV   R2,A
	MOV   A,#OMR_TAC+1
	MOVC  A,@A+DPTR
	MOV   R3,A		; R23 = TAC
	MOV   A,#OMR_RS
	MOVC  A,@A+DPTR
	CLR   C
	SUBB  A,R2
	MOV   R2,A
	MOV   A,#OMR_RS+1
	MOVC  A,@A+DPTR
	SUBB  A,R3
	MOV   R3,A		; R23 = RS - TAC
	MOV   A,R2
	ADD   A,R0
	MOV   A,R3
	ADDC  A,R1		; (RS-TAC) + MS ?
	JNC   CD_TD12		; na max rychlosti
	MOV   A,R2
	ADD   A,R4
	MOV   R4,A
	MOV   A,R3
	ADDC  A,R5
	MOV   R5,A
	MOV   A,#-1
	ADDC  A,R6
	MOV   R6,A
	MOV   A,#-1
	ADDC  A,R7
	MOV   R7,A		; R4567 = RP + R23
	CALL  TR_BC0		; TEP-R4567 ?
	MOV   C,OV
	XRL   A,PSW
	JNB   ACC.7,CD_TD15	; na pulce vzdalenosti
	JMP   CD_TWPS
; Jsme na max rychlosti, R4567=RP, R23=RS+TAC, R01 = MS
CD_TD12:MOV   A,#OMR_RS
	MOVC  A,@A+DPTR		; RS
	MOV   R2,A
	MOV   A,#OMR_RS+1
	MOVC  A,@A+DPTR		; RS+1
	MOV   R3,A
	MOV   A,#OMR_TSP	; fract TEP v TSP.7
	MOVC  A,@A+DPTR
	RLC   A			; TEP=2*TEP-R4567
	%MR_OFS2DP(OMR_TEP)
	MOVX  A,@DPTR		; TEP
	RLC   A
	MOV   B.0,C
	CLR   C
	SUBB  A,R4
	MOV   B.1,C
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR		; TEP + 1
	MOV   C,B.0
	RLC   A
	MOV   B.0,C
	MOV   C,B.1
	SUBB  A,R5
	MOV   B.1,C
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR		; TEP + 2
	MOV   C,B.0
	RLC   A
	MOV   B.0,C
	MOV   C,B.1
	SUBB  A,R6
	MOV   B.1,C
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR		; TEP + 3
	MOV   C,B.0
	RLC   A
	MOV   B.0,C
	MOV   C,B.1
	SUBB  A,R7
	MOV   B.1,C
	MOVX  @DPTR,A
	INC   DPTR
	CLR   C
	CLR   A
	SUBB  A,R0
	XCH   A,R2
	MOVX  @DPTR,A		; TSP = R23
	INC   DPTR
	CLR   A
	SUBB  A,R1              ; R23 = -MS
	XCH   A,R3
	MOVX  @DPTR,A
	%MR_OFS2DP(OMR_VG)
	MOV   A,#HIGH CD_TD30
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW  CD_TD30
	MOVX  @DPTR,A
	JMP   CD_TAPS
; Dojeti do poloviny
CD_TD15:MOV   A,#OMR_TSP	; RP = 2*TEP-RP
	MOVC  A,@A+DPTR		; nejnizsi bit TEP
	RLC   A
	MOV   A,#OMR_TEP
	MOVC  A,@A+DPTR		; TEP
	RLC   A
	MOV   R4,A
	MOV   A,#OMR_TEP+1
	MOVC  A,@A+DPTR		; TEP+1
	RLC   A
	MOV   R5,A
	MOV   A,#OMR_TEP+2
	MOVC  A,@A+DPTR		; TEP+2
	RLC   A
	MOV   R6,A
	MOV   A,#OMR_TEP+3
	MOVC  A,@A+DPTR		; TEP+3
	RLC   A
	MOV   R7,A
	CLR   C
	%MR_OFS2DP(OMR_RP)
	MOVX  A,@DPTR
	XCH   A,R4
	SUBB  A,R4
	MOVX  @DPTR,A		; RP
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R5
	SUBB  A,R5
	MOVX  @DPTR,A		; RP+1
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R6
	SUBB  A,R6
	MOVX  @DPTR,A		; RP+2
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R7
	SUBB  A,R7
	MOVX  @DPTR,A		; RP+3
	INC   DPTR
	MOV   A,R2
	MOVX  @DPTR,A		; RS
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A		; RS+1
	INC   DPTR
	MOV   A,#-1
	MOVX  @DPTR,A		; RS+2
	%LDR45i(CD_TD60)
	CALL  CI_VG
	JMP   VR_REG

; Usek po ktery se jede konstantni rychlosti
CD_TD30:%TRDEB
	JNB   MR_FLGA.BMR_BSY,CD_TEND_V3
	JB    MR_FLGA.BMR_ERR,CD_TEND_V3
	CALL  MR_RDRP		; R4567 = RP
	CALL  MR_GPSC		; R4567 = RP + RS
	CALL  TR_BC0		; TEP-R4567 ?
	MOV   C,OV
	XRL   A,PSW
	JNB   ACC.7,CD_TD35	; na zacatku zpomaleni
	CALL  MR_WRRP
	JMP   VR_REG
; Prechod na zpomaleni
CD_TD35:MOV   A,#OMR_TEP
	MOVC  A,@A+DPTR		; TEP
	MOV   R4,A
	MOV   A,#OMR_TEP+1
	MOVC  A,@A+DPTR		; TEP+1
	MOV   R5,A
	MOV   A,#OMR_TEP+2
	MOVC  A,@A+DPTR		; TEP+2
	MOV   R6,A
	MOV   A,#OMR_TEP+3
	MOVC  A,@A+DPTR		; TEP+3
	MOV   R7,A
	MOV   A,#OMR_TSP
	MOVC  A,@A+DPTR		; TSP
	MOV   R2,A
	MOV   A,#OMR_TSP+1
	MOVC  A,@A+DPTR		; TSP+1
	MOV   R3,A
	%MR_OFS2DP(OMR_VG)
	MOV   A,#HIGH CD_TD50
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW  CD_TD50
	MOVX  @DPTR,A
	JMP   CD_TWPS
CD_TEND_V3:JMP CD_TEND

; Prvni krok brzdeni
CD_TD50:%TRDEB
	JNB   MR_FLGA.BMR_BSY,CD_TEND_V3
	JB    MR_FLGA.BMR_ERR,CD_TEND_V3
	%LDR45i(CD_TD60)
	CALL  CI_VG
	CALL  MR_RDRP
	MOV   A,#OMR_TSP
	MOVC  A,@A+DPTR
	MOV   R2,A
	MOV   A,#OMR_TSP+1
	MOVC  A,@A+DPTR
	MOV   R3,A
	ORL   A,R2
	JZ    CD_TEND_V3	; jiz jsme v cili
	JMP   CD_TAPS

; Usek po ktery dochazi k zpomalovani
CD_TD60:%TRDEB
	JNB   MR_FLGA.BMR_BSY,CD_TEND_V3
	JB    MR_FLGA.BMR_ERR,CD_TEND_V3
	CALL  MR_RDRP
	MOV   A,#OMR_TAC
	MOVC  A,@A+DPTR
	MOV   R2,A
	MOV   A,#OMR_TAC+1
	MOVC  A,@A+DPTR
	MOV   R3,A
	MOV   A,#OMR_RS
	MOVC  A,@A+DPTR
	ADD   A,R2
	MOV   R2,A
	MOV   A,#OMR_RS+1
	MOVC  A,@A+DPTR
	ADDC  A,R3
	MOV   R3,A
	ORL   A,R2
	JNZ   CD_TD63
	JMP   CD_TEND
CD_TD63:JMP   CD_TAPS

; ----- Zastaveni pohybu --

CD_TEND:%MR_OFS2DP(OMR_RS)
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	CLR   MR_FLGA.BMR_BSY
	%LDR45i(VR_REG)
	CALL  CI_VG
	JMP   VR_REG

; *******************************************************************
; Nalezeni nulove polohy

; Paramet OMR_CFG
; xxxxxxxT xxxRDSSS
;
;   SSS	.. rychlost hledani dorazu OMR_MS>>SSS
;   D	.. pocatecni smer
;   R	.. pouziva se znacka otacky z HP HEDS

; Struktura OMR_GST je vyuzita nasledovne
; +0 ... stav
; +6 ... citac prodlevy

; Ulozi do OMR_RS zmodifikovane OMR_MS podle R2
CI_HHS:	%MR_OFS2DP(OMR_MS)
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   A,R2
	ANL   A,#07H
	CJNE  A,#7,CI_HHS3
	MOV   A,R4		; zpomaleni pro ucely hledani znacky
	ANL   A,#0C0H
	ORL   A,R5
	JZ    CI_HHS4
	MOV   R4,#040H
	MOV   R5,#0
	SJMP  CI_HHS4
CI_HHS3:CALL  SHRi
CI_HHS4:MOV   A,R2
	MOV   R3,#0
	JNB   ACC.3,CI_HHS9
	DEC   R3
	CALL  NEGi
CI_HHS9:%MR_OFS2DP(OMR_RS)
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	RET

CI_HH:  SETB  MR_FLGA.BMR_BSY
	CALL  CD_HHCL
	%MR_OFS2DP(OMR_CFG)
	MOVX  A,@DPTR
	MOV   R2,A
	%MR_OFS2DP(OMR_GST)
	MOV   A,R2
	MOVX  @DPTR,A
	CALL  CI_HHS		; Start pohybu
	%LDR45i(CD_HH)
	SJMP  CI_HH20
CI_HH10:CALL  CI_HHS		; Start hledani znacky otacky
	CALL  HH_MARK
	%LDR45i(CD_HHM)
CI_HH20:CALL  CI_VG
CI_HH30:MOV   R4,#0
	MOV   R5,#0
	CLR   F0
CI_HH35:JMP   VR_REG1


CD_HH:
CD_HHSC:CALL  MR_GPSC		; R4567=OMR_RP+OMR_RS; R3=dir
	CALL  MR_WRRP		; OMR_RP=R456
CD_HH09:JNB   MR_FLGA.BMR_ENR,CI_HH30
	CALL  MR_REG
	JNB   F0,CI_HH35
CD_HH10:CLR   MR_FLGA.BMR_ERR	; dojeti na doraz
	CALL  CD_HHCL
	%MR_OFS2DP(OMR_GST)
	MOVX  A,@DPTR
	JNB   ACC.4,CD_HH20
	XRL   A,#8		; hledani znacky otacky
	MOVX  @DPTR,A
	MOV   R2,A
	SJMP  CI_HH10
CD_HH20:SJMP  CD_HHE

; Hleda se znacka otacky
CD_HHM:	CALL  HH_MARK		; je znacka
	JB    MR_FLGA.BMR_ERR,CD_HHE4
	JZ    CD_HHSC
	JC    CD_HHE
CD_HHM5:%MR_OFS2DP(OMR_GST+6)
	MOV   A,#20
	MOVX  @DPTR,A
	%LDR45i(CD_HHMA)
	JMP   CI_HH20
CD_HHE:	%MR_OFS2DP(OMR_GST)
	MOVX  A,@DPTR
	MOV   R2,A
	CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R7,A
CD_HHE2:CALL  CD_HHST
	MOV   A,R2
	JNB   ACC.4,CD_HHE4
	CALL  HH_MARK
	JC    CD_HHE4
	CALL  CI_HHS
	JMP   CD_HHM5
CD_HHE4:CLR   MR_FLGA.BMR_BSY
	%LDR45i(VR_REG)
	JMP   CI_HH20

CD_HHMA:%MR_OFS2DP(OMR_GST+6)
	MOVX  A,@DPTR
	DJNZ  ACC,CD_HHMB
	CALL  HH_MARK
	%MR_OFS2DP(OMR_GST)
	MOVX  A,@DPTR
	ORL   A,#7
	XRL   A,#8
	MOVX  @DPTR,A
	MOV   R2,A
	CALL  CI_HHS
	%LDR45i(CD_HHM)
	JMP   CI_HH20
CD_HHMB:MOVX  @DPTR,A
	JMP   CD_HHSC

; Kompletni vynulovani regulatoru
CD_HHCL:CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R7,A
CD_HHST:MOV   DPL,MR_BAS	; vstupni bod
	MOV   DPH,MR_BAS+1
CD_HHC1:INC   DPTR		; vstupni bod
	MOV   R0,#1		; DPTR = OMR_AP
CD_HHC2:MOV   A,R5 		; OMR_AP, OMR_RPI
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A		; OMR_AS, OMR_RS
	INC   DPTR
	MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,CD_HHC3
	MOV   A,R4		; OMR_RP
	MOVX  @DPTR,A
	INC   DPTR
	SJMP  CD_HHC2
CD_HHC3:MOVX  @DPTR,A		; OMR_RS+1
	INC   DPTR
	MOV   A,DPL
	ADD   A,#OMR_FOI-OMR_P
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#0
	MOV   DPH,A
	MOV   R0,#4
	CLR   A
CD_HHC4:MOVX  @DPTR,A		; OMR_FOI, OMR_FOD
	INC   DPTR
	DJNZ  R0,CD_HHC4
	MOV   A,DPL
	ADD   A,#OMR_ERC-OMR_FOI-4
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#0
	MOV   DPH,A
	CLR   A
	MOVX  @DPTR,A		; OMR_ERC
	MOV   A,DPL
	ADD   A,#-OMR_ERC
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#-1
	MOV   DPH,A
	JMP   IRC_WRP

; *******************************************************************
; Programovy interface k subsystemu motoru

; pro regulator R1 vypocte adresu parametru v ACC

MR_GPA1:ADD   A,#LOW  REG_A
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH REG_A
	MOV   DPH,A
	MOV   A,R1
	MOV   B,#REG_LEN
	MUL   AB
	ADD   A,DPL
	MOV   DPL,A
	MOV   A,B
	ADDC  A,DPH
	MOV   DPH,A
	RET

GO_GEPEV1:JMP GO_GEPE

; Najede motorem R1 na polohu R4567
GO_GEP: MOV   R3,#0
GO_GEPT:MOV   A,#OMR_FLG	; odpojit generator
	CALL  MR_GPA1
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	JB    ACC.7,GO_GEPEV1
	ANL   A,#NOT MMR_ENG
	ORL   A,#080H
	MOVX  @DPTR,A
	MOV   EA,C
	XCH   A,R3
	JZ    GO_GEP4		; vyber moznosti typu pohybu
	CJNE  A,#1,GO_GEP1
	MOV   A,#OMR_GEP	; relativni pohyb proti OMR_GPE
	CALL  MR_GPA1
	CALL  xADDl
	JNB   OV,GO_GEP4
GO_GEP1:SETB  F0
	SJMP  GO_GEP5
GO_GEP4:MOV   A,#OMR_GEP	; koncova poloha OMR_GPE
	CALL  MR_GPA1
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
GO_GEP5:MOV   A,R3
	JB    ACC.BMR_ENR,GO_GEP7
	CALL  GO_RSFT		; beznarazove pripojeni regulatoru
GO_GEP7:MOV   A,#OMR_CFG+1	; jaky typ generatoru
	CALL  MR_GPA1
	MOVX  A,@DPTR
	JB    ACC.0,GO_GEP8
	%LDR45i(CI_GEP)		; vektor na CI_GEP .. konstanta
	SJMP  GO_GEP9
GO_GEP8:%LDR45i(CI_TR)		; vektor na CI_TR .. trapezoid
GO_GEP9:
; Nastavi vektor a spusti regulator R1
GO_VG:	MOV   A,#OMR_VGJ
	CALL  MR_GPA1
	MOV   A,#2
	MOVX  @DPTR,A		; instrukce LJMP
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A		; OMR_VG
	INC   DPTR
	MOV   A,R4
	MOVX  @DPTR,A
	MOV   A,#OMR_FLG	; spusteni rizeni
	CALL  MR_GPA1
	MOVX  A,@DPTR
	ANL   A,#MMR_DBG
	ORL   A,#MMR_ENI OR MMR_ENR OR MMR_ENG
	MOVX  @DPTR,A
	SETB  MR_FLG.BMR_BSY
	CALL  GO_NOTIFY		; upozorneni na start pohybu
GO_GEPR:RET
GO_GEPE:MOV   EA,C
	SETB  F0
	RET

; Priprava na zmenu generatoru regulatoru R1
; vraci:  ACC = [DPTR]	.. OMR_FLG
;	  F0=1		.. pokud je reentrance zmeny
GO_PRPC:MOV   A,R1		; odpojit generator
	MOV   B,#REG_LEN
	MUL   AB
	ADD   A,#LOW  REG_A
	MOV   DPL,A
	MOV   A,B
	ADDC  A,#HIGH REG_A
	MOV   DPH,A
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	JNB   ACC.7,GO_PRP1
	MOV   EA,C
	SETB  F0
	RET
GO_PRP1:ANL   A,#NOT MMR_ENG
	ORL   A,#080H
	MOVX  @DPTR,A
	MOV   EA,C
	CLR   F0
	RET

; OMR_RP = OMR_AP a vynulovani chyb regulatoru R1
GO_RSFT:MOV   A,#OMR_AP-1	; pri vypnutem regulatoru
	CALL  MR_GPA1
	MOV   C,EA
	CLR   EA
	CALL  xLDl 		; beznarazove pripojeni
	MOV   EA,C
	MOV   R4,#0
	MOV   A,#OMR_RP
	CALL  MR_GPA1
	CALL  xSVl
	MOV   A,#OMR_FOI
	CALL  MR_GPA1
	MOV   R2,#OMR_ERC-OMR_FOI+1
	CLR   A
GO_RSFT1:MOVX @DPTR,A		; vynuluje OMR_FOI a OMR_FOD
	INC   DPTR
	DJNZ  R2,GO_RSFT1
	RET

; Vynuluje polohu regulatoru R1
CLR_GEP:MOV   A,#OMR_FLG	; odpojit generator
	CALL  MR_GPA1
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	JB    ACC.7,GO_GEPE
	ANL   A,#NOT (MMR_ENG OR MMR_ENI OR MMR_ENR)
	ORL   A,#080H
	MOVX  @DPTR,A
	MOV   EA,C
	CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R7,A
	MOV   A,#0
	CALL  MR_GPA1
	CALL  CD_HHC1		; nulovani polohy
	CLR   A			; nulova vystupni energie
	MOV   R4,A
	MOV   R5,A
	CALL  MR_GPA1
	CALL  MR_SENEP
	MOV   A,#OMR_FLG	; spusteni odmeru IRC
	CALL  MR_GPA1
	MOV   A,#MMR_ENI
	MOVX  @DPTR,A
	RET

; Zastavi a odpoji regulator R1
REL_GEP:MOV   A,#OMR_FLG
	CALL  MR_GPA1
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	SJMP  CER_GEP1

; Zastavi a odpoji regulator R1 pouze kdyz hlasi chybu
CER_GEP:MOV   A,#OMR_FLG
	CALL  MR_GPA1
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	JNB   ACC.BMR_ERR,CER_GEP9
CER_GEP1:JB   ACC.7,CER_GEP8
	ANL   A,#NOT (MMR_ENG OR MMR_ENR)
	ORL   A,#080H
	MOVX  @DPTR,A
	MOV   EA,C
	CLR   A			; nulova vystupni energie
	MOV   R4,A
	MOV   R5,A
	CALL  MR_GPA1
	CALL  MR_SENEP
	MOV   A,#OMR_FLG
	CALL  MR_GPA1
	MOV   C,EA		; smazani chyby
	MOVX  A,@DPTR
	ANL   A,#NOT (MMR_ERR OR MMR_BSY OR 080H)
	MOVX  @DPTR,A
	MOV   EA,C
	RET
CER_GEP8:SETB F0
CER_GEP9:MOV  EA,C
	RET

; Zastavi regulator R1
STP_GEP:CALL  GO_PRPC	; odpojit generator
	JB    F0,STP_GEPR
	MOV   A,#OMR_RS
	CALL  MR_GPA1
	CLR   A
	MOV   R0,#OMR_P-OMR_RS
STP_GE1:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,STP_GE1
	MOV   A,#OMR_FLG
	CALL  MR_GPA1
	MOVX  A,@DPTR
	ANL   A,#NOT (080H OR MMR_BSY)
	MOVX  @DPTR,A
STP_GEPR:RET

; Spusti hard mome motoru R1
GO_HH:  CALL  GO_PRPC
	JB    F0,GO_HH9
	%LDR45i(CI_HH)
	JMP   GO_VG
GO_HH9:	RET

	END