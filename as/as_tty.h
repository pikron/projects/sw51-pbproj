;********************************************************************
;*                    PB_TTY_C.H                                    *
;*     Include file se scankody a ovladanim displaye                *
;*                  Stav ke dni 24.03.1991                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

$INCLUDE(%INCH_LCD)

;********************************************************************

; Scan kody klaves

K_0      EQU   01BH
K_1      EQU   015H
K_2      EQU   018H
K_3      EQU   016H
K_4      EQU   021H
K_5      EQU   024H
K_6      EQU   022H
K_7      EQU   00FH
K_8      EQU   012H
K_9      EQU   010H
K_DP     EQU   01EH
K_PM     EQU   00DH ; 0FFH  ; 00DH ; +/- neni definovano
K_ENTER  EQU   01CH

K_LEFT   EQU   023H
K_RIGHT  EQU   01FH
K_UP     EQU   00EH
K_DOWN   EQU   020H
K_RUN    EQU   00AH
K_END    EQU   00CH
K_HELP   EQU   011H

K_ESC    EQU   00DH
K_SAMPL  EQU   004H
K_SET    EQU   006H
K_F1     EQU   006H
K_AUX    EQU   003H
K_F2     EQU   003H
K_CYCLE  EQU   001H
K_F3     EQU   001H
K_TEMPER EQU   002H
K_F4     EQU   002H
K_MODE   EQU   00DH

; K_DEL    EQU   014H
; K_INS    EQU   017H
; K_PROG   EQU   011H
; 013H 005H 017H 014H

K_CAL    EQU   009H
K_PREP   EQU   007H
K_LOAD   EQU   008H
K_MAN    EQU   00BH
K_STOP   EQU   01DH
K_FIX    EQU   01AH
K_STBY   EQU   019H

K_H_A    EQU   01DH
K_H_B    EQU   01AH
K_H_C    EQU   019H
K_H_D    EQU   017H
K_H_E    EQU   014H
K_H_F    EQU   013H

; Virtualni scankody

KV_V_OK	EQU    071H

;********************************************************************

; Kody led diod

LFB_BEEP   EQU  7
LFB_CAL	   EQU  6
LFB_PREP   EQU  5
LFB_LOAD   EQU  4
LFB_RUN	   EQU  3
LFB_STBY   EQU  2
LFB_INJ    EQU  1
LFB_FIX	   EQU  0

%DEFINE (BEEP_FL)   (LED_FLG.LFB_BEEP)
%DEFINE (CAL_FL)    (LED_FLG.LFB_CAL)
%DEFINE (PREP_FL)   (LED_FLG.LFB_PREP)
%DEFINE (LOAD_FL)   (LED_FLG.LFB_LOAD)
%DEFINE (RUN_FL)    (LED_FLG.LFB_RUN)
%DEFINE (STBY_FL)   (LED_FLG.LFB_STBY)
%DEFINE (INJ_FL)    (LED_FLG.LFB_INJ)
%DEFINE (FIX_FL)    (LED_FLG.LFB_FIX)

%DEFINE (PROG_FL)   (LED_FLH.7)
%DEFINE (END_FL)    (LED_FLH.6)

EXTRN    CODE(LCDINST,LCDNBUS,LCDWCOM,LCDWCO1,LCDWR,LCDWR1)
EXTRN    CODE(LCDINSM,PRINT,PRINTH,xPRINT,cPRINT)

EXTRN    CODE(SCANKEY,TESTKEY)
EXTRN    CODE(KBDSTDB)

EXTRN    CODE(LEDWR)
EXTRN    DATA(LED_FLG,LED_FLH,KBD_FLG)
