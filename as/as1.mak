#   Project file pro automaticky davkovac AS
#         (C) Pisoft 1996

as.obj:	as.asm
	a51 as.asm $(par)

	: as.obj
	del as.

as.	: as.obj ..\pblib\pb.lib
	l51 as.obj,..\pblib\pb.lib xdata(8000H) ramsize(100h) ixref

as.hex	: as.
	ohs51 as
	del as.
