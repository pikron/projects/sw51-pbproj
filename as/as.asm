$NOMOD51
;********************************************************************
;*                    DAVKOVAC - AS.ASM                             *
;*                       Hlavni modul                               *
;*                  Stav ke dni 24.04.2012                          *
;*                      (C) Pisoft 1996-2012                        *
;*                      (C) PiKRON 1996-2012                        *
;*                          Pavel Pisa Praha                        *
;********************************************************************

%DEFINE (WITH_UL_DY)	(1)	; s dynamickou adresaci
%DEFINE (WITH_UL_DY_EEP) (1)	; vyrobnim cislem v EEPROM
%DEFINE (WITH_RS232)    (1)     ; s komunikaci RS232

$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_AL)
$INCLUDE(%INCH_ADR)
$INCLUDE(%INCH_ULAN)
$INCLUDE(%INCH_UL_OI)
$INCLUDE(%INCH_REGS)
$INCLUDE(%INCH_MR_DEFS)
$INCLUDE(%INCH_UI)
$INCLUDE(%INCH_BREAK)
%IF (%WITH_RS232) THEN (
$INCLUDE(%INCH_RS232)
$INCLUDE(%INCH_RSOI)
)FI
$LIST

EXTRN	CODE(SEL_FNC)
EXTRN	CODE(cxMOVE,xxMOVE,xMDPDP,ADDATDP)

EXTRN	CODE(BFL_EV,BFL_XOR,BFL_3ST,UB_A_RD,UB_A_WR)

%IF(%WITH_UL_DY) THEN (
EXTRN	CODE(UD_INIT,UD_RQ)
)FI

PUBLIC	INPUTc,KBDBEEP,ERRBEEP,RES_STAR

%DEFINE (DEBUG_FL)    (1)	; Vlozeni debugrutin
%DEFINE (OLD_AUX_IN1) (0)	; Vstup AUX_IN1 na P1.1/P1.5
%IF (0) THEN (
  %DEFINE (IRC_WHEEL) (10)	; Pocet znacek na otacku motoru
)ELSE(
  %DEFINE (IRC_WHEEL) (16)	; Pocet znacek na otacku motoru
)FI

%DEFINE (STP_8_PHASE) (1)	; Osmitaktni rizeni krokace
%DEFINE (WITH_ASH_VIP) (0)	; Kontrolovat odkloneni ramenka
%DEFINE (WITH_WHEEL80) (1)	; Pro 80 kotouc

%IF (%DEBUG_FL) THEN (
  EXTRN	CODE(MONITOR)
  EXTRN	CODE(IHEXLD)
  EXTRN	CODE(PRINThb,PRINThw,INPUThw)
)FI

AS____C SEGMENT CODE
AS____D SEGMENT DATA
AS____B SEGMENT DATA BITADDRESSABLE
AS____X SEGMENT XDATA

STACK	DATA  80H

%IF(%WITH_UL_DY_EEP) THEN (
PUBLIC	SER_NUM
CSEG AT	8080H
SER_NUM:DS    10H	; Instrument unigue serial number
XSEG AT	8080H
	DS    10H	; XDATA overlay
)FI

RSEG	AS____B

LEB_FLG:DS    1		; Blikani ledek

HW_FLG: DS    1
ITIM_RF BIT   HW_FLG.7
FL_ASH0	BIT   HW_FLG.6
STP_ERR	BIT   HW_FLG.5
SEK_ERR	BIT   HW_FLG.4
STP_WCH	BIT   HW_FLG.3
LEB_PHA	BIT   HW_FLG.2
FL_WINJ	BIT   HW_FLG.1	; Pri davkovani pockej na I_INJECT
FL_DINJ	BIT   HW_FLG.0	; Prisel I_INJECT, bude se davkovat

HW_FLG1:DS    1
FL_LPS1	BIT   HW_FLG1.7	; Kapalina v cidle 1
FL_LPS2	BIT   HW_FLG1.6	; Kapalina v cidle 2
FL_25Hz	BIT   HW_FLG1.5
FL_ENLPS BIT  HW_FLG1.4	; Povoleni rizeni podle cidel
FL_DIPR	BIT   HW_FLG1.3	; Displej pripojen
FL_RTIM	BIT   HW_FLG1.2	; Zobrazovat bezici cas
; Bity 0 a 1 jsou pouzity pro LPS

HW_FLG2:DS    1
FL_ECUL	BIT   HW_FLG2.7	; Povolit komunikaci uLAN
FL_ECRS	BIT   HW_FLG2.6	; Povolit komunikaci RS232

RSEG	AS____X

C_R_PER	EQU   30
REF_PER:DS    1
STATUS:	DS    2

RSEG	AS____C

RES_STAR:
	CLR   EA
	MOV   IEN0,#0
	MOV   IEN1,#0
	MOV   IP0,#0
	MOV   IP1,#0
	MOV   SP,#STACK
	MOV   PCON,#10000000B ; Bd = OSC/12/16/(256-TH1)
	MOV   TM2CON,#10000001B; timer 2 CLK, TR2 disabled, 16 bit OV
	%VECTOR(T2CMP2,I_TIME1); Realny cas z komparatoru CM2
	SETB  ECM2	       ; povoleni casu od preruseni od CM2
	MOV   P4,#0FFH

%IF(1)THEN(
	MOV   DPTR,#8080H
STRT10:	MOV   R0,#80H
STRT11:	CLR   A
STRT12:	MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,STRT12
	%WATCHDOG
	MOV   A,DPH
	XRL   A,#HIGH STRT10
	JZ    STRT13
	MOV   A,DPH
	CJNE  A,#0F0H,STRT11
STRT13:
)FI

	CALL  I_TIMRI
	CALL  I_TIMRI
	MOV   CINT25,#10
	CLR   A
	MOV   DPTR,#TIMRI
	MOVX  @DPTR,A
	MOV   DPTR,#TIME
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A

	MOV   HW_FLG,#080H
	MOV   HW_FLG1,#0
	MOV   HW_FLG2,#0
	MOV   LEB_FLG,#0

	MOV   A,#018H
	MOV   DPTR,#APWM0
	MOVX  @DPTR,A
	CLR   A
	MOV   DPTR,#TC_ACL
	MOVX  @DPTR,A
	MOV   DPTR,#TMC1_RT
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#TMC1_FLG
	MOVX  @DPTR,A
	MOV   DPTR,#APWM1
	MOVX  @DPTR,A
	MOV   DPTR,#GS_MSK
	MOVX  @DPTR,A
	MOV   DPTR,#STP_ST
	MOVX  @DPTR,A
	MOV   DPTR,#INV_ST
	MOVX  @DPTR,A
	MOV   DPTR,#SEK_ST
	MOVX  @DPTR,A
	MOV   A,#MSEK_LPS	; Musi se pouzivat cidla
	MOV   DPTR,#SEK_CFG
	MOVX  @DPTR,A
	%LDMXi(SAMPNUM,1)
	%LDMXi(CC_REP,1)
	%LDMXi(CC_FRST,1)
	%LDMXi(CC_LAST,C_SAMPLAST)

	%LDMXi(P_VOL1,60)	; Objem sani za 1 cidlo
	%LDMXi(P_INJT,200)	; Doba otoceni ventilu
	%LDMXi(P_IRC_W,%IRC_WHEEL); Pocet impulsu na IRC kolecku

	%LDMXi(LPS_TD1,5)	; Doba pro rozhodnuti o zmene LPS
	%LDMXi(LPS_TD2,5)	; Doba pro rozhodnuti o zmene LPS

	%LDMXi(INV_AUX,0)	; Rizeni AUX od INV pri manualu
	%LDMXi(CU_PROT,0)	; Nevysilat znacku

	%LDMXi(ASH1_BOT1,9000)	; Hloubka zajeti do 25
	%LDMXi(ASH1_BOT2,5700)	; Hloubka zajeti do 80

    %IF(%WITH_WHEEL80)THEN(
	%LDMXi(ASH_RP1,2550)	; Pozice pro 25 kotouc
	%LDMXi(ASH_RP2,2300)	; Pozice pro 80 kotouc, 1-40
	%LDMXi(ASH_RP3,3800)	; Pozice pro 80 kotouc, 41-80
	%LDMXi(ASH_WASH,100)	; Hloubka sjeti pro cisteni
    )FI


	MOV   DPTR,#REG_A+OMR_FLG
	CLR   A
	MOVX  @DPTR,A

	CALL  LCDINST
	JNZ   STRT30
	SETB  FL_DIPR
	CALL  LEDWR
STRT30:

	MOV   DPTR,#REF_PER
	MOV   A,#C_R_PER
	MOVX  @DPTR,A

	JMP   L0

INPUTc:	CALL  SCANKEY
	JZ    INPUTc
	RET

; Pipnuti na klavese klavesnice
;KBDBEEP:JMP   KBDSTDB
KBDBEEP:MOV   A,#2
BEEP:	MOV   DPTR,#BEEPTIM
	MOVX  @DPTR,A
	SETB  %BEEP_FL
	MOV   DPTR,#LED       ; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
	MOV   A,R2
	RET
ERRBEEP:MOV   A,#8
	JMP   BEEP

; *******************************************************************
;
; Casove preruseni

PUBLIC  KBDTIMR

RSEG	AS____D

;DINT600 EQU   1536 ; Deleni CLK/12 na 600 Hz pro X 11.0592 MHz
DINT600 EQU   2560 ; Deleni CLK/12 na 600 Hz pro X 18.432 MHz
DINT25  EQU   24   ; Delitel EXINT1 na 25 Hz
CINT25: DS    1

RSEG	AS____X

BEEPTIM:DS    1	   ; Timer delky pipani

N_OF_T  EQU   6    ; Pocet timeru
TIMR1:  DS    1    ; Timery jsou decrementovany
TIMR2:  DS    1    ; s frekvenci 25 Hz
TIMR_WAIT:
TIMR3:  DS    1
REF_TIM:DS    1	   ; Refresh timr
KBDTIMR:DS    1
TIMRI:  DS    1
TIME:   DS    2    ; Cas v 0.01 min              =====

RSEG	AS____C

USING   3
I_TIME1:PUSH  ACC	; Cast s pruchodem 600 Hz
	PUSH  PSW
	;SETB  P4.7	;*!!!!!!!!!!!!!!!
	MOV   A,CML2
	ADD   A,#LOW  DINT600
	MOV   CML2,A
	MOV   A,CMH2
	ADDC  A,#HIGH DINT600
	MOV   CMH2,A
	CLR   CMI2
	MOV   PSW,#AR0
	PUSH  B
	PUSH  DPL
	PUSH  DPH

	CLR   FL_25Hz
	DJNZ  CINT25,I_TIM10
	SETB  FL_25Hz
I_TIM10:CALL  DO_REG		; Regulace DC motoru
	CALL  ADC_D		; Cteni AD prevodniku
	CALL  SEK		; Sekvencer davkovani

	JNB   FL_25Hz,I_TIMR1	; Konec casti spruchodem 600 Hz
	MOV   CINT25,#DINT25	; Pruchod s frekvenci 25 Hz

	CALL  ADC_S		; Spusteni cyklu prevodu ADC
	JNB   FL_ECUL,I_TIM24
	CALL  uL_STR		; Oziveni uLAN
I_TIM24:CALL  TMC_REG		; Regulace teploty
	CALL  AUX_ICH		; Zmena na vtupech AUX
	JZ    I_TIM25
	SETB  GS_BUF.BGS_AUX	; Doslo ke zmene
I_TIM25:
	%WATCHDOG
	MOV   DPTR,#BEEPTIM
	MOVX  A,@DPTR
	JZ    I_TIM80
	DEC   A
	MOVX  @DPTR,A
	JNZ   I_TIM80
	CLR   %BEEP_FL
	MOV   DPTR,#LED		; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
I_TIM80:MOV   DPTR,#TIMR1	; Casovace
	MOV   B,#N_OF_T-1
I_TIME2:MOVX  A,@DPTR
	JZ    I_TIME3
	DEC   A
	MOVX  @DPTR,A
I_TIME3:INC   DPTR
	DJNZ  B,I_TIME2
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	JNB   ITIM_RF,I_TIMR1
	JB    ACC.7,I_TIME4
I_TIMR1:POP   DPH
	POP   DPL
	POP   B
	POP   PSW
	POP   ACC
	SETB  EA
I_TIMRI:
	;CLR   P4.7	;*!!!!!!!!!!!!!!!
	RETI

I_TIME4:CLR   ITIM_RF		; Pruchod 0.6 sec
;	CALL  I_TIMRI
;	MOV   PSW,#AR0		; Banka 2
	ADD   A,#15
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR		; TIME
	INC   A
	MOVX  @DPTR,A
	JNZ   I_TIM41
	INC   DPTR
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
I_TIM41:

	MOV   A,LEB_FLG		; blikani ledek
	JBC   LEB_PHA,I_TIM42
	ORL   LED_FLG,A
	SETB  LEB_PHA
	SJMP  I_TIM43
I_TIM42:CPL   A
	ANL   LED_FLG,A
I_TIM43:

	MOV   DPTR,#TIME	; rizeni casove posloupnosti
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	SETB  C
	MOV   DPTR,#AL_TIME	; cas na ktery se ceka
	MOVX  A,@DPTR
	SUBB  A,R4
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,R5
	JNC   I_TIM61
	SETB  GS_BUF.BGS_TIM
I_TIM61:

	SETB  ITIM_RF
	JMP   I_TIMR1

; *******************************************************************
; Promenne multiregulatoru

%DEFINE (MR_REG_TYPE) (MR_PID)

EXTRN	CODE(%MR_REG_TYPE)
PUBLIC	MR_BAS

RSEG	AS____B

MR_FLG:	DS    1
MR_FLGA:DS    1

RSEG	AS____D

MR_BAS:	DS    2		; ukazatel na struktury regulatoru

OMR_AIR	EQU   OMR_LEN	;1B ; spodni cast adresy IRC snimace
OMR_APW	EQU   OMR_AIR+1	;1B ; spodni cast adresy PWM vystupu
OMR_VGJ	EQU   OMR_APW+1	;1B ; instrukce JMP
OMR_VG	EQU   OMR_VGJ+1	;2B ; adresa rutiny generatoru, koncit VR_RET
OMR_GST	EQU   OMR_VG+2	;1B ; soucasny stav generovani polohy
OMR_GEP	EQU   OMR_GST+1	;4B ; koncova poloha
OMR_LEN	SET   OMR_GEP+4

RSEG	AS____X

REG_N	EQU   3
REG_LEN	EQU   OMR_LEN
REG_A:	DS    REG_N*REG_LEN+1

OREG_A	EQU   0
OREG_B	EQU   REG_LEN
OREG_C	EQU   2*REG_LEN

REG_BAR:DS   10

; *******************************************************************
; Cteni polohy z CF32006

AIR_CF0	XDATA 0FF20H	; Baze prvniho CF32006
AIR_CF1	XDATA 0FF40H	; Baze druheho CF32006

			;	       7   6   5   4   3   2   1   0
AIR_CC0	XDATA 0FF80H    ; Konfigurace KL0 R0  M22 M12 M02 M21 M11 M01
AIR_CC1	XDATA 0FFA0H	; Konfigurace KL1 R1  0   0   0   M23 M13 M03
; mody:	16 bit counter	- 0
;	single IRC	- 1 on raisign Ua1n, 2 on raising Ua2n
;	double IRC	- 3 on Ua1n edges , 4 on Ua2n edges
;	quadruple IRC	- 5 an all edges
;	PWM measure	- 6 Ua1n gate, Ua2n=1 count up on CLK, 0=down
;	frequncy measure- 7 Ua1n frequncy signal, Ua2n gate

CIR_C_N	EQU   11101101B ; zakladni konfigurace
CIR_RES EQU   10111111B ; reset

AIR_CC1_M EQU 00000111B ; No KL1 and R1, only one CF32006 chip 

RSEG	AS____X

AIR_CC1_BUF:DS 2	; Register is shared with AUX on AS

RSEG	AS____C

CF_INIT:MOV   DPTR,#AIR_CC0
	MOV   A,#CIR_C_N AND CIR_RES
	MOVX  @DPTR,A
	MOV   A,#CIR_C_N
	MOVX  @DPTR,A

	MOV   C,EA
	CLR   EA
	MOV   DPTR,#AIR_CC1_BUF
	MOVX  A,@DPTR
	MOV   DPTR,#AIR_CC1
	ANL   A,#NOT AIR_CC1_M
	ORL   A,#CIR_C_N AND CIR_RES AND AIR_CC1_M
	MOVX  @DPTR,A
	ANL   A,#NOT AIR_CC1_M
	ORL   A,#CIR_C_N AND AIR_CC1_M
	MOVX  @DPTR,A
	MOV   DPTR,#AIR_CC1_BUF
	MOVX  @DPTR,A
	MOV   EA,C
	RET

; Nacte polohu do OMR_AP (3B) a vypocita OMR_AS (2B)
IRC_RD:
CF_RD:  MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
IRC_RDP:
CF_RDP:	MOV   A,#OMR_AIR
	MOVC  A,@A+DPTR
	MOV   DPL,A
	MOV   DPH,#HIGH AIR_CF0
CF_RDDP:MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A	; R45 = IRC z CF32006
	%MR_OFS2DP(OMR_AP)	; OMR_AP
	MOVX  A,@DPTR
	MOV   R2,A
	MOV   A,R4
	MOVX  @DPTR,A
	CLR   C
	SUBB  A,R2
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   A,R5
	MOVX  @DPTR,A
	SUBB  A,R3
	MOV   R3,A
	INC   DPTR
	JNC   CF_RD4
	CPL   A
CF_RD4:	JNB   ACC.7,CF_RD6	; Prodlouzeni OMR_AP
	JNC   CF_RD5
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	SJMP  CF_RD7
CF_RD5: MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	SJMP  CF_RD7
CF_RD6: MOVX  A,@DPTR
CF_RD7:	MOV   R7,A
	INC   DPTR
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	RET

; Pozadavek na kalibraci polohy na R567
IRC_WR:
CF_WR:	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
IRC_WRP:
CF_WRP: MOV   A,#OMR_AIR
	MOVC  A,@A+DPTR		; OMR_AIR
	MOV   DPL,A
	MOV   DPH,#HIGH AIR_CF0
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	RET

; *******************************************************************
; Vystup energii na motory

APWM0	XDATA 0FFE0H
APWM1	XDATA 0FFE1H
APWM2	XDATA 0FFE2H

RSEG	AS____C

MR_SENE:MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
MR_SENEP:MOV  A,#OMR_APW
	MOVC  A,@A+DPTR
	MOV   DPL,A
	MOV   DPH,#HIGH APWM0
	MOV   A,R5
	MOVX  @DPTR,A
	RET

; *******************************************************************
; Zakladni smycka multi regulatoru

RSEG	AS____C

DO_REGR:;CLR   P4.7	;*!!!!!!!!!!!!!!!
	RET

; Prochazi vsechny regulatory az do OMR_FLG=0

DO_REG: ;SETB  P4.7	;*!!!!!!!!!!!!!!!
	ANL   MR_FLG,#MMR_ERR
	MOV   MR_BAS,#LOW REG_A
	MOV   MR_BAS+1,#HIGH REG_A
DO_REG1:CLR   F0
	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOVX  A,@DPTR		; OMR_FLG
	JZ    DO_REGR
	MOV   MR_FLGA,A
	JNB   MR_FLGA.BMR_ENI,DO_REG5
	CALL  CF_RD
DO_REG5:JNB   MR_FLGA.BMR_ENG,DO_REG6
	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,#OMR_VGJ
	JMP   @A+DPTR  		; skok na vektor generatoru
DO_REG6:
VR_REG: JNB   MR_FLGA.BMR_ENR,DO_REG9
	CALL  %MR_REG_TYPE
DO_REG7:
VR_REG1:JNB   F0,DO_REG8
	ORL   MR_FLGA,#MMR_ERR
	CLR   A
	MOV   R5,A
	MOV   R4,A
DO_REG8:CALL  MR_SENE
DO_REG9:
VR_RET:	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,MR_FLGA
	ORL   MR_FLG,A
	MOVX  @DPTR,A		; OMR_FLG
	MOV   A,MR_BAS
	ADD   A,#OMR_LEN
	MOV   MR_BAS,A
	JNC   DO_REG1
	INC   MR_BAS+1
	JMP   DO_REG1

MR_ZER:	MOV   DPTR,#REG_A
	%LDR23i (REG_N*REG_LEN+1)
MR_ZER1:CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R2
	DEC   R2
	JNZ   MR_ZER1
	MOV   A,R3
	DEC   R3
	JNZ   MR_ZER1
	MOV   A,#LOW (AIR_CF0+6)
	MOV   DPTR,#REG_A+OREG_A+OMR_AIR
	MOVX  @DPTR,A
	MOV   A,#LOW APWM0
	MOV   DPTR,#REG_A+OREG_A+OMR_APW
	MOVX  @DPTR,A
	MOV   A,#LOW (AIR_CF0+4)
	MOV   DPTR,#REG_A+OREG_B+OMR_AIR
	MOVX  @DPTR,A
	MOV   A,#LOW APWM1
	MOV   DPTR,#REG_A+OREG_B+OMR_APW
	MOVX  @DPTR,A
	MOV   A,#LOW (AIR_CF0+2)
	MOV   DPTR,#REG_A+OREG_C+OMR_AIR
	MOVX  @DPTR,A
	MOV   A,#LOW APWM2
	MOV   DPTR,#REG_A+OREG_C+OMR_APW
	MOVX  @DPTR,A
	MOV   MR_FLG,#0
	RET

; *******************************************************************
; Generatory pozadovane polohy

; R4567=OMR_RS+OMR_RP , meni R3

MR_GPSC:%MR_OFS2DP(OMR_RS)
	MOVX  A,@DPTR	; OMR_RS
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR	; OMR_RS+1
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR	; OMR_RS+2
	MOV   R3,A
	SJMP  MR_GPS5

; R4567=R45+OMR_RP , meni R3

MR_GPSV:%MR_OFS2DP(OMR_RS)
	MOV   A,R4
	MOVX  @DPTR,A	; OMR_RS
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A	; OMR_RS+1
	INC   DPTR
	MOV   R3,#0
	JNB   ACC.7,MR_GPS2
	DEC   R3
MR_GPS2:MOV   A,R3
	MOVX  @DPTR,A	; OMR_RS+2
MR_GPS5:MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,#OMR_RP
	MOVC  A,@A+DPTR	; OMR_RP
	ADD   A,R4
	MOV   R4,A
	MOV   A,#OMR_RP+1
	MOVC  A,@A+DPTR
	ADDC  A,R5
	MOV   R5,A
	MOV   A,#OMR_RP+2
	MOVC  A,@A+DPTR
	ADDC  A,R3
	MOV   R6,A
	MOV   A,#OMR_RP+3
	MOVC  A,@A+DPTR
	ADDC  A,R3
	MOV   R7,A
	RET

; Provede komparaci R4567 a OMR_GEP

MR_CMEP:MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	CLR   C
	MOV   A,#OMR_GEP
	MOVC  A,@A+DPTR
	SUBB  A,R4
	MOV   B,A
	MOV   A,#OMR_GEP+1
	MOVC  A,@A+DPTR
	SUBB  A,R5
	ORL   B,A
	MOV   A,#OMR_GEP+2
	MOVC  A,@A+DPTR
	SUBB  A,R6
	ORL   B,A
	MOV   A,#OMR_GEP+3
	MOVC  A,@A+DPTR
	SUBB  A,R7
	ORL   B,A
	INC   B
	DJNZ  B,MR_CME3
	RET
MR_CME3:CPL   C
	CPL   A
	ORL   A,#1
	RET

; Nacte do R4567 OMR_RP

MR_RDRP:MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,#OMR_RP
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_RP+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   A,#OMR_RP+2
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   A,#OMR_RP+3
	MOVC  A,@A+DPTR
	MOV   R7,A
	RET

; Ulozi R4567 do OMR_RP

MR_WRRP:%MR_OFS2DP(OMR_RP)
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	RET

; Nacte do R4567 OMR_AP

MR_RDAP:MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   R4,#0
	MOV   A,#OMR_AP+0
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   A,#OMR_AP+1
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   A,#OMR_AP+2
	MOVC  A,@A+DPTR
	MOV   R7,A
	RET

; Inicializuje pohyb konstantni rychlosti na polohu OMR_GEP

CI_GEP:	CALL  MR_RDRP
	CALL  MR_CMEP
	JZ    CI_GEP8
	MOV   C,OV
	XRL   A,PSW
	MOV   C,ACC.7
	SETB  MR_FLGA.BMR_BSY
	MOV   A,#OMR_MS
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_MS+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	JC    CI_GEP4
	CALL  NEGi
CI_GEP4:CALL  MR_GPSV
	%MR_OFS2DP(OMR_VG)	; dale pobezi CD_GEP
	MOV   A,#HIGH CD_GEP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW  CD_GEP
	MOVX  @DPTR,A
	JMP   CD_GEP2
CI_GEP8:JMP   CD_GEPE

; Pohyb konstantni rychlosti

CD_GEP:	JNB   MR_FLGA.BMR_BSY,CD_GEP9
	CALL  MR_GPSC
CD_GEP2:CALL  MR_CMEP
	JZ    CD_GEPE
	MOV   C,OV
	XRL   A,PSW
	XRL   A,R3
	JB    ACC.7,CD_GEP8
CD_GEPE:CLR   MR_FLGA.BMR_BSY
	MOV   A,#OMR_GEP
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_GEP+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   A,#OMR_GEP+2
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   A,#OMR_GEP+3
	MOVC  A,@A+DPTR
	MOV   R7,A
	%MR_OFS2DP(OMR_RS)
	CLR   A
	MOVX  @DPTR,A	; OMR_RS
	INC   DPTR
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	%MR_OFS2DP(OMR_VG); dale pobezi jen regulator
	MOV   A,#HIGH VR_REG
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW  VR_REG
	MOVX  @DPTR,A
CD_GEP8:%MR_OFS2DP(OMR_RP)
	MOV   A,R4	; OMR_RP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
CD_GEP9:JMP   VR_REG

; Kompletni vynulovani regulatoru
CD_HHCL:CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R7,A
CD_HHC0:MOV   DPL,MR_BAS	; vstupni bod
	MOV   DPH,MR_BAS+1
CD_HHC1:INC   DPTR		; vstupni bod
	MOV   R0,#1		; DPTR = OMR_AP
CD_HHC2:MOV   A,R5 		; OMR_AP, OMR_RPI
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A		; OMR_AS, OMR_RS
	INC   DPTR
	MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,CD_HHC3
	MOV   A,R4		; OMR_RP
	MOVX  @DPTR,A
	INC   DPTR
	SJMP  CD_HHC2
CD_HHC3:MOVX  @DPTR,A		; OMR_RS+1
	INC   DPTR
	MOV   A,DPL
	ADD   A,#OMR_FOI-OMR_P
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#0
	MOV   DPH,A
	MOV   R0,#4
	CLR   A
CD_HHC4:MOVX  @DPTR,A		; OMR_FOI, OMR_FOD
	INC   DPTR
	DJNZ  R0,CD_HHC4
	MOV   A,DPL
	ADD   A,#OMR_ERC-OMR_FOI-4
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#0
	MOV   DPH,A
	CLR   A
	MOVX  @DPTR,A		; OMR_ERC
	MOV   A,DPL
	ADD   A,#-OMR_ERC
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#-1
	MOV   DPH,A
	JMP   IRC_WRP

; *******************************************************************
; Rutiny pro ramenko autosampleru

ASH_ZF	BIT   P1.0	; Zakladni poloha ramenka nahore
ASH_VIP	BIT   P1.1	; Poloha pro propichnuti (1=ramenko v poloze)
ASH_RZF	BIT   P4.6	; Priznak nuly otaceni (osa C)

CASH_ZP	EQU   820*%IRC_WHEEL	; Poloha koncove znacky
%IF(%WITH_WHEEL80)THEN(
CASH_HP	EQU   CASH_ZP+4		; Poloha pro vyplach jehly
CASH_HR	EQU   8*%IRC_WHEEL ;(6)	; Pocet IRC otaceni ramenka
CASH_SL	EQU   15*%IRC_WHEEL	; Pomala rychlost
CASH_SH	EQU   45*%IRC_WHEEL	; Vysoka rychlost
CASH_MT	EQU   100*%IRC_WHEEL	; Minimalni poloha pri testu
CASH_MR	EQU   6000		; Pocet IRC pri rotaci
)ELSE(
CASH_HP	EQU   CASH_ZP+4		; Poloha pro vyplach jehly
CASH_HR	EQU   6*%IRC_WHEEL ;(6)	; Pocet IRC otaceni ramenka
CASH_SL	EQU   3*%IRC_WHEEL	; Pomala rychlost
CASH_SH	EQU   45*%IRC_WHEEL	; Vysoka rychlost
CASH_MT	EQU   10*%IRC_WHEEL	; Minimalni poloha pri testu
)FI

	; Pozadovana poloha pro dojeti na dno zkumavky
ASH1_BOT SET  REG_A+OREG_A+OMR_GEP+1
ASH1_GST SET  REG_A+OREG_A+OMR_GST

RSEG	AS____X

ASH1_BOT1:DS 2			; Hloubka zajeti do 25
ASH1_BOT2:DS 2			; Hloubka zajeti do 80

%IF(%WITH_WHEEL80)THEN(
ASH_RP1:DS   2			; Pozice pro 25 kotouc
ASH_RP2:DS   2			; Pozice pro 80 kotouc, 1-40
ASH_RP3:DS   2			; Pozice pro 80 kotouc, 41-80
ASH_WASH:DS  2			; Hloubka sjeti pro cisteni

C_SAMPLAST EQU 80
)ELSE(
C_SAMPLAST EQU 40
)FI

RSEG	AS____C

; Vraci CY=1 pokud doslo k prejeti hrany

ASH_TZ:	MOV   C,FL_ASH0
	MOV   B.7,C
	CLR   A
	MOV   C,ASH_ZF
	ADDC  A,#0
	MOV   C,ASH_ZF
	ADDC  A,#0
	MOV   C,ASH_ZF
	ADDC  A,#-2
	MOV   FL_ASH0,C
	XRL   A,B
	MOV   C,ACC.7
	CPL   C
	RET

; Nastavi polohu odpovidajici hrane

ASH_ZER:MOV   DPTR,#AIR_CF0+6
	MOV   A,#HIGH CASH_ZP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW CASH_ZP
	MOVX  @DPTR,A
	MOV   DPTR,#AIR_CF0+6
	MOV   A,#HIGH CASH_ZP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW CASH_ZP
	MOVX  @DPTR,A
	%MR_OFS2DP(OMR_AP)	; OMR_AP
	MOV   A,#LOW CASH_ZP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH CASH_ZP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#0
	MOVX  @DPTR,A
	INC   DPTR
	INC   DPTR
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW CASH_ZP	; OMR_RP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH CASH_ZP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#0
	MOVX  @DPTR,A
	RET

%IF(%WITH_ASH_VIP)THEN(
; Nastavi CY=1, pokud je aktivni vstup ASH_VIP
ASH_TVIP:
	CLR   A
	MOV   C,ASH_VIP
	ADDC  A,#0
	MOV   C,ASH_VIP
	ADDC  A,#0
	MOV   C,ASH_VIP
	ADDC  A,#-2
	RET
)FI

; Generovani polohy a regulace ramenka autosampleru

VG_ASH: JB    MR_FLGA.BMR_ERR,ASH_090
	CLR   F0
	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,#OMR_GST
	MOVC  A,@A+DPTR
	MOV   R0,A
	ANL   A,#0F0H
	JNZ   ASH_100
ASH_090:CLR   MR_FLGA.BMR_BSY
	MOV   R4,#00H
%IF(%WITH_WHEEL80)THEN(
	MOV   R5,#0		;Pro 80 se nepouziva
)ELSE(
	MOV   R5,#1CH		; Stabilni pridrzna energie
)FI
	JMP   ASH_990

; Kalibrace nulove polohy ramenka
ASH_100:CJNE  A,#10H,ASH_200V
	XRL   A,R0
	JNZ   ASH_110
	SETB  MR_FLGA.BMR_BSY	; Spustit inicializaci
	CALL  ASH_ZER
	SETB  FL_ASH0
	MOV   R0,#11H
	%LDR45i(-CASH_SL)
	JMP   ASH_880
ASH_200V:JMP  ASH_200
ASH_110:DJNZ  ACC,ASH_120
	CALL  ASH_TZ
	JNC   ASH_111
 	MOV   R0,#12H		; Pripravit vyjeti ze znacky
	%LDR45i(CASH_SL*2)
	JMP   ASH_880
ASH_111:CALL  MR_GPSC
	MOV   A,R5
	SUBB  A,#LOW  (CASH_ZP-CASH_MT)
	MOV   A,R6
	SUBB  A,#HIGH (CASH_ZP-CASH_MT)
	JNC   ASH_127
ASH_10E:SETB  MR_FLGA.BMR_ERR
	JMP   ASH_090
ASH_120:DJNZ  ACC,ASH_130
	CALL  ASH_TZ
	JNC   ASH_122
	CALL  ASH_ZER
ASH_10S:
    %IF(%WITH_WHEEL80)THEN(
	MOV   R0,#13H
    )ELSE(
	MOV   R0,#0H
    )FI
	%LDR45i(0)
	JMP   ASH_880
ASH_122:CALL  MR_GPSC
	MOV   A,R5
	SUBB  A,#LOW  (2*CASH_ZP)
	MOV   A,R6
	SUBB  A,#HIGH (2*CASH_ZP)
	JNC   ASH_10E
ASH_127:JMP   ASH_870
ASH_130:
    %IF(%WITH_WHEEL80)THEN(
	DJNZ  ACC,ASH_140
	MOV   DPTR,#REG_A+OREG_C+OMR_VG
	MOV   A,#HIGH CI_ASHR_HH
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW CI_ASHR_HH
	MOVX  @DPTR,A
	MOV   DPTR,#REG_A+OREG_C+OMR_FLG
	MOV   A,#MMR_ENI OR MMR_ENR OR MMR_ENG OR MMR_BSY
	MOVX  @DPTR,A
	MOV   R0,#14H
	JMP   ASH_890
ASH_140:DJNZ  ACC,ASH_150
	MOV   DPTR,#REG_A+OREG_C+OMR_FLG
	MOVX  A,@DPTR
	JB    ACC.BMR_ERR,ASH_10E
	JB    ACC.BMR_BSY,ASH_900V
	MOV   R0,#0H
	JMP   ASH_890
ASH_150:
    )FI
ASH_180:JMP   ASH_10E
ASH_900V:JMP  ASH_900

; Pohyb ramenka nahoru
ASH_200:CJNE  A,#20H,ASH_300
	XRL   A,R0
	JNZ   ASH_210
	SETB  MR_FLGA.BMR_BSY
	CALL  MR_RDAP
	CALL  MR_WRRP
	CALL  ASH_TZ
	JB    FL_ASH0,ASH_229
	MOV   R0,#21H
	%LDR45i(CASH_SH)
	JMP   ASH_880
ASH_210:DJNZ  ACC,ASH_220
	CALL  ASH_TZ
	JB    FL_ASH0,ASH_10S
	CALL  MR_GPSC
	MOV   A,R5
	SUBB  A,#LOW  (CASH_ZP-CASH_HR)
	MOV   A,R6
	SUBB  A,#HIGH (CASH_ZP-CASH_HR)
	JC    ASH_227
	MOV   R0,#22H
	%LDR45i(CASH_SL)
	JMP   ASH_880
ASH_220:DJNZ  ACC,ASH_230
	CALL  ASH_TZ
	JB    FL_ASH0,ASH_228
	CALL  MR_GPSC
	MOV   A,R5
	SUBB  A,#LOW  (CASH_ZP+20)
	MOV   A,R6
	SUBB  A,#HIGH (CASH_ZP+20)
	JNC   ASH_20E
ASH_227:JMP   ASH_870
ASH_228:
    %IF(NOT %WITH_WHEEL80)THEN(
	CALL  ASH_ZER
    )FI
ASH_229:
    %IF(%WITH_WHEEL80)THEN(
	CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R7,A
	MOV   R1,#2
	CALL  GO_GEP		; Otocit do nulove polohy
	MOV   R0,#23H
    )ELSE(
	MOV   R0,#40H
    )FI
	%LDR45i(0)
	JMP   ASH_880
ASH_230:
    %IF(%WITH_WHEEL80)THEN(
	DJNZ  ACC,ASH_240
	MOV   DPTR,#REG_A+OREG_C+OMR_FLG
	MOVX  A,@DPTR
	JB    ACC.BMR_ERR,ASH_20E
	JB    ACC.BMR_BSY,ASH_900V
	MOV   R0,#40H
	JMP   ASH_890
ASH_240:
    )FI
ASH_20E:SETB  MR_FLGA.BMR_ERR
	JMP   ASH_090

ASH_400V:JMP  ASH_400

; Pohyb ramenka dolu
ASH_300:CJNE  A,#30H,ASH_400V
	XRL   A,R0
	JNZ   ASH_310
	SETB  MR_FLGA.BMR_BSY
	CALL  MR_RDAP
	CALL  MR_WRRP
    %IF(%WITH_WHEEL80)THEN(
	MOV   R0,#31H
	%LDR45i(CASH_SH)
    )ELSE(
	CALL  ASH_TZ
	JNB   FL_ASH0,ASH_20E
	MOV   R0,#35H
	%LDR45i(-CASH_SL/4)
    )FI
	JMP   ASH_880
ASH_310:
    %IF(%WITH_WHEEL80)THEN(
	DJNZ  ACC,ASH_320	; Vytahnout z odpadu
	CALL  MR_GPSC
	MOV   A,R5
	SUBB  A,#LOW  CASH_ZP
	MOV   A,R6
	SUBB  A,#HIGH CASH_ZP
	JC    ASH_357
	MOV   R0,#32H
	%LDR45i(0)
	JMP   ASH_880
ASH_320:DJNZ  ACC,ASH_330	; Rezerva
	MOV   R0,#33H
	JMP   ASH_890
ASH_330:DJNZ  ACC,ASH_340
	CALL  ASH_WTPAR
	CALL  GO_GEP		; Otocit nad kotouc
	MOV   R0,#34H
	JMP   ASH_890
ASH_340:DJNZ  ACC,ASH_350
	MOV   DPTR,#REG_A+OREG_C+OMR_FLG
	MOVX  A,@DPTR
	JB    ACC.BMR_ERR,ASH_30E
	JB    ACC.BMR_BSY,ASH_900U
	MOV   R0,#35H
	%LDR45i(-CASH_SL/4)
	JMP   ASH_880
ASH_900U:JMP  ASH_900
    )ELSE(
	ADD   A,#-4
    )FI
ASH_350:DJNZ  ACC,ASH_360
	CALL  ASH_TZ
	JNC   ASH_354
;       CALL  ASH_ZER           ; !!!!!!!!!!!!!!!!
	MOV   R0,#36H
	%LDR45i(-CASH_SL)
	JMP   ASH_880
ASH_354:CALL  MR_GPSC
	MOV   A,R5
	SUBB  A,#LOW  (CASH_ZP-CASH_MT)
	MOV   A,R6
	SUBB  A,#HIGH (CASH_ZP-CASH_MT)
	JC    ASH_30E
ASH_357:JMP   ASH_870
ASH_360:DJNZ  ACC,ASH_370
	CALL  MR_GPSC
	MOV   A,R5
	SUBB  A,#LOW  (CASH_ZP-CASH_HR)
	MOV   A,R6
	SUBB  A,#HIGH (CASH_ZP-CASH_HR)
	JNC   ASH_357
    %IF(%WITH_ASH_VIP)THEN(
	CALL  ASH_TVIP
	JNC   ASH_30E
    )FI
	MOV   R0,#37H
	%LDR45i(-CASH_SH)
	JMP   ASH_880
ASH_370:DJNZ  ACC,ASH_380
	CALL  MR_GPSC
	CALL  MR_CMEP
	JNC   ASH_357
	MOV   R0,#38H
	%LDR45i(0)
	JMP   ASH_880
ASH_380:DJNZ  ACC,ASH_390
	CLR   MR_FLGA.BMR_BSY
	JMP  ASH_900
ASH_390:
ASH_30E:SETB  MR_FLGA.BMR_ERR
	JMP   ASH_090

; Pridrzeni ramenka v horni poloze
ASH_400:CJNE  A,#40H,ASH_500
	XRL   A,R0
	JNZ   ASH_410
	MOV   R0,#41H
	%LDR45i(CASH_SL/4)
	JMP   ASH_880
ASH_410:DJNZ  ACC,ASH_420
	CALL  ASH_TZ
;       JNB   FL_ASH0,ASH_40E   ; !!!!!!!!!!!!!
	CALL  MR_GPSC
	MOV   A,R5
	SUBB  A,#LOW  (CASH_HP+2)
	MOV   A,R6
	SUBB  A,#HIGH (CASH_HP+2)
	JC    ASH_415
	CLR   MR_FLGA.BMR_BSY
	MOV   R0,#42H
	%LDR45i(0)
	JMP   ASH_880
ASH_415:JMP   ASH_870
ASH_420:
    %IF(%WITH_WHEEL80)THEN(
	CLR   A
	MOV   R4,A
	MOV   R5,A
	JMP   ASH_990
    )ELSE(
	ADD   A,#-10
        JC    ASH_430
        %MR_OFS2DP(OMR_GST)
        MOVX  A,@DPTR
        INC   A
        MOVX  @DPTR,A
        JMP   ASH_435
ASH_430:JNZ   ASH_440
	CALL  ASH_TZ
	JC    ASH_431		; Chyba az podruhe
	JNB   FL_ASH0,ASH_40E	; !!!!!!!!!!!!!!!!
ASH_431:
	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	CLR   C
	MOV   A,#OMR_AP
	MOVC  A,@A+DPTR
	SUBB  A,#LOW  (CASH_HP)
	MOV   R4,A
	MOV   A,#OMR_AP+1
	MOVC  A,@A+DPTR
	SUBB  A,#HIGH (CASH_HP)
	MOV   R5,A
	JC    ASH_435
	ORL   A,R4
	JZ    ASH_434
	MOV   R4,#00H
	MOV   R5,#1CH		; Nizsi energie
	JMP   ASH_990
ASH_434:MOV   R4,#00H
	MOV   R5,#1CH		; Stabilni energie
	JMP   ASH_990
ASH_435:MOV   R4,#00H
	MOV   R5,#28H		; Vyssi energie
	JMP   ASH_990
    )FI

ASH_440:
ASH_40E:SETB  MR_FLGA.BMR_ERR
	JMP   ASH_090

; Ramenko do polohy pro cisteni
ASH_500:CJNE  A,#50H,ASH_600
    %IF(NOT %WITH_WHEEL80)THEN(
	MOV   R0,#20H
	JMP   ASH_880		; Pouze vyjet a pridrzet nahore
    )ELSE(
	XRL   A,R0
	JNZ   ASH_510
	MOV   DPTR,#REG_A+OREG_C+OMR_AP+1
	MOVX  A,@DPTR
	INC   A
	ANL   A,#0FEH
	JZ    ASH_515		; Preskocit vytahovani
	MOV   R0,#51H
	%LDR45i(CASH_SH)
	JMP   ASH_880
ASH_510:DJNZ  ACC,ASH_520
	CALL  MR_GPSC   	; Vytahnout jehlu
	MOV   A,R5
	SUBB  A,#LOW  (CASH_ZP)
	MOV   A,R6
	SUBB  A,#HIGH (CASH_ZP)
	JC    ASH_870
ASH_515:CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R7,A
	MOV   R1,#2
	CALL  GO_GEP		; Otocit do nulove polohy
	MOV   R0,#52H
	%LDR45i(0)
	JMP   ASH_880
ASH_520:DJNZ  ACC,ASH_530
	MOV   DPTR,#REG_A+OREG_C+OMR_FLG
	MOVX  A,@DPTR
	JB    ACC.BMR_ERR,ASH_50E
	JB    ACC.BMR_BSY,ASH_900
	MOV   R0,#53H
	%LDR45i(-CASH_SH)
	JMP   ASH_880
ASH_530:DJNZ  ACC,ASH_540
	CALL  MR_GPSC   	; Zasunout jehlu do odpadu
	MOV   DPTR,#ASH_WASH
	MOVX  A,@DPTR
	SUBB  A,R5
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,R6
	JC    ASH_870
	MOV   R0,#0
	%LDR45i(0)
	JMP   ASH_880
ASH_540:
ASH_50E:SETB  MR_FLGA.BMR_ERR
	JMP   ASH_090
    )FI

ASH_600:
	JMP   ASH_990

ASH_870:CALL  MR_WRRP		; Nova pozadovana poloha R4567
	JMP   ASH_900
ASH_880:%MR_OFS2DP(OMR_RS)	; Nova rychlost R45 a stav R0
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   R1,#0
	JNB   ACC.7,ASH_881
	DEC   R1
ASH_881:MOV   A,R1
	MOVX  @DPTR,A
ASH_890:%MR_OFS2DP(OMR_GST)	; Prechod na dalsi stav R0
	MOV   A,R0
	MOVX  @DPTR,A
ASH_900:CALL  %MR_REG_TYPE
	JNB   FL_ASH0,ASH_990
	MOV   A,R5
	JB    ACC.7,ASH_990
	ADD   A,#-030H
	JNC   ASH_990
	MOV   R5,#030H
ASH_990:JMP   VR_REG1


%IF(%WITH_WHEEL80)THEN(

ASH_TRZF:
	CLR   A
	MOV   C,ASH_RZF
	ADDC  A,#0
	MOV   C,ASH_RZF
	ADDC  A,#0
	MOV   C,ASH_RZF
	ADDC  A,#-2
	MOV   C,ACC.7
	CPL   C
	RET

CI_ASHR_HH:
	MOV   R4,#0
	MOV   R5,#LOW CASH_MR
	MOV   R6,#HIGH CASH_MR
	MOV   R7,#0
	CALL  CD_HHC0
	%MR_OFS2DP(OMR_MS)
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  NEGi
	CALL  MR_GPSV
	CALL  MR_WRRP
	MOV   DPTR,#CD_ASHR_HH1
CI_SET_VG:
	MOV   R4,DPL
	MOV   R5,DPH
	%MR_OFS2DP(OMR_VG)
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R4
	MOVX  @DPTR,A
	JMP   VR_REG

CD_ASHR_HH1:	; Odklaneni do znacky
	JNB   MR_FLGA.BMR_BSY,CD_ASHR_HH9
	JB    MR_FLGA.BMR_ERR,CD_ASHR_HHE
	CALL  ASH_TRZF
	JNC   CD_ASHR_HH8
	%MR_OFS2DP(OMR_MS)
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  MR_GPSV
	CALL  MR_WRRP
	MOV   DPTR,#CD_ASHR_HH2
	JMP   CI_SET_VG

CD_ASHR_HH2:	; Vyjeti ze znacky
	JNB   MR_FLGA.BMR_BSY,CD_ASHR_HH9
	JB    MR_FLGA.BMR_ERR,CD_ASHR_HHE
	CALL  ASH_TRZF
	JC    CD_ASHR_HH8
	CALL  CD_HHCL
	CLR   MR_FLGA.BMR_BSY
	JMP   VR_REG

CD_ASHR_HH8:
	CALL  MR_GPSC
	CALL  MR_WRRP
	JMP   VR_REG

CD_ASHR_HHE:
	CLR   MR_FLGA.BMR_BSY
	SETB  MR_FLGA.BMR_ERR
CD_ASHR_HH9:
	JMP   VR_REG


; Nastaveni hloubky zajeti podle typu kotouce
; Vraci R4567 pozadovane natoceni podle typu kotouce

ASH_WTPAR:
	MOV   DPTR,#ASH1_BOT1	; Hloubka zajeti do 25
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   DPTR,#STP_WT	; Typ kotouce
	MOVX  A,@DPTR
	MOV   DPTR,#ASH_RP1	; Kotouc 25 pozic
	ADD   A,#-2
	JNC   ASH_WTPAR1
	MOV   DPTR,#ASH1_BOT2	; Hloubka zajeti do 80
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   DPTR,#SAMPNUM	; Kotouc 80 pozic
	MOVX  A,@DPTR
	MOV   DPTR,#ASH_RP2	; Pozice 1-40
	ADD   A,#-41
	JNC   ASH_WTPAR1
	MOV   DPTR,#ASH_RP3	; Pozice 41-80
ASH_WTPAR1:CLR A
	MOV   R4,A
	MOV   R7,A
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R6,A
	MOV   R1,#2
	MOV   DPTR,#ASH1_BOT	; Hloubka zajeti
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	RET

)FI

; *******************************************************************
; Nastaveni pro AS

MR_INIS:CALL  MR_ZER
	%LDR45i (REG_A)
	%LDR23i (STDR_A)
	%LDR01i (STDR_AE-STDR_A)
	CALL  cxMOVE
	MOV   A,#MMR_ENI 		; OR MMR_ENR
	MOV   DPTR,#REG_A+OREG_A+OMR_FLG
	MOVX  @DPTR,A
	MOV   DPTR,#REG_A+OREG_B+OMR_FLG
	MOVX  @DPTR,A
    %IF(%WITH_WHEEL80)THEN(
	MOV   DPTR,#REG_A+OREG_C+OMR_FLG
	MOVX  @DPTR,A
    )FI
	RET

STDR_A:	DS    STDR_A+OREG_A-$
	DB    0, 0,0,0, 0,0
	DB    0
	DB    0,0   ; 	(CASH_ZP)	; RP
	DB    0
	DB    0,0,0			; RS
	DB    080H,0, 002H,0, 0FFH,0	; P I D
	DB    000H,0, 000H,0, 0,060H	; 1 2 ME
	DB    080H,0, 010H,0		; MS MA
	DB    0,0,0,0,0,0,0
	DB    LOW (AIR_CF0+6),LOW APWM0
	DB    2				; JMP
	DW    VG_ASH
	DB    10			; GST = inicializace
	DB    0				; GEP
	%W    (2800)
	DB    0

	DS    STDR_A+OREG_B-$
	DB    0, 0,0,0, 0,0, 0,0,0,0, 0,0,0
	DB    014H,0, 002H,0, 070H,0	; P I D
	DB    000H,0, 000H,0, 0,060H	; 1 2 ME
	%W    (80)			; MS
	%W    (010H)			; MA
	DB    0,0,0,0,0,0,0
	DB    LOW (AIR_CF0+4),LOW APWM1
	DB    2				; JMP
	DW    VR_REG

	DS    STDR_A+OREG_C-$
	DB    0, 0,0,0, 0,0
	DB    0
	DB    0,0   ;			; RP
	DB    0
	DB    0,0,0			; RS
	DB    030H,0, 002H,0, 080H,0	; P I D
	DB    000H,0, 000H,0, 0,060H	; 1 2 ME
	DB    0,004H, 010H,0		; MS MA
	DB    0,0,0,0,0,0,0
	DB    LOW (AIR_CF0+2),LOW APWM2
	DB    2				; JMP
	DW    VR_RET
STDR_AE:

; pro regulator R1 vypocte adresu parametru v ACC

MR_GPA1:ADD   A,#LOW  REG_A
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH REG_A
	MOV   DPH,A
	MOV   A,R1
	MOV   B,#REG_LEN
	MUL   AB
	ADD   A,DPL
	MOV   DPL,A
	MOV   A,B
	ADDC  A,DPH
	MOV   DPH,A
	RET

TM_GEP: MOV   DPTR,#TM_GEPT
	CALL  cPRINT
	MOV   R6,#8
	MOV   R7,#0C0H
	CALL  INPUTi
	JB    F0,GO_GEPR
	MOV   A,R5
	MOV   R6,A
	MOV   A,R4
	MOV   R5,A
	CLR   A
	MOV   R4,A
	MOV   R7,A
	MOV   R1,#0		; motor A

; Najede motorem R1 na polohu R4567
GO_GEP:	MOV   A,#OMR_FLG	; odpojit generator
	CALL  MR_GPA1
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	JB    ACC.7,GO_GEPE
	ANL   A,#NOT MMR_ENG
	ORL   A,#080H
	MOVX  @DPTR,A
	MOV   EA,C
	MOV   A,#OMR_GEP	; koncova poloha OMR_GPE
	CALL  MR_GPA1
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	%LDR45i(CI_GEP)
; Nastavi vektor a spusti regulator R1
GO_VG:	MOV   A,#OMR_VGJ
	CALL  MR_GPA1
	MOV   A,#2
	MOVX  @DPTR,A		; instrukce LJMP
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A		; OMR_VG
	INC   DPTR
	MOV   A,R4
	MOVX  @DPTR,A
	MOV   A,#OMR_FLG	; spusteni rizeni
	CALL  MR_GPA1
	MOVX  A,@DPTR
	ANL   A,#MMR_DBG
	ORL   A,#MMR_ENI OR MMR_ENR OR MMR_ENG
	MOVX  @DPTR,A
	SETB  MR_FLG.BMR_BSY
; !!!!	CALL  GO_NOTIFY		; upozorneni na start pohybu
GO_GEPR:RET
GO_GEPE:MOV   EA,C
	SETB  F0
	RET

; Priprava na zmenu generatoru regulatoru R1
; vraci:  ACC = [DPTR]	.. OMR_FLG
;	  F0=1		.. pokud je reentrance zmeny
GO_PRPC:MOV   A,R1		; odpojit generator
	MOV   B,#REG_LEN
	MUL   AB
	ADD   A,#LOW  REG_A
	MOV   DPL,A
	MOV   A,B
	ADDC  A,#HIGH REG_A
	MOV   DPH,A
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	JNB   ACC.7,GO_PRP1
	MOV   EA,C
	SETB  F0
	RET
GO_PRP1:ANL   A,#NOT MMR_ENG
	ORL   A,#080H
	MOVX  @DPTR,A
	MOV   EA,C
	CLR   F0
	RET

; Vynuluje polohu regulatoru R1
CLR_GEP:MOV   A,#OMR_FLG	; odpojit generator
	CALL  MR_GPA1
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	JB    ACC.7,GO_GEPE
	ANL   A,#NOT (MMR_ENG OR MMR_ENI OR MMR_ENR)
	ORL   A,#080H
	MOVX  @DPTR,A
	MOV   EA,C
	CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R7,A
	MOV   A,#0
	CALL  MR_GPA1
	CALL  CD_HHC1		; nulovani polohy
	CLR   A			; nulova vystupni energie
	MOV   R4,A
	MOV   R5,A
	CALL  MR_GPA1
	CALL  MR_SENEP
	MOV   A,#OMR_FLG	; spusteni odmeru IRC
	CALL  MR_GPA1
	MOV   A,#MMR_ENI
	MOVX  @DPTR,A
	RET

; Zastavi regulator R1
STP_GEP:MOV   A,#OMR_FLG	; odpojit generator
	CALL  MR_GPA1
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	JB    ACC.7,GO_GEPE
	ANL   A,#NOT MMR_ENG
	ORL   A,#080H
	MOVX  @DPTR,A
	MOV   EA,C
	MOV   A,#OMR_RS
	CALL  MR_GPA1
	CLR   A
	MOV   R0,#OMR_P-OMR_RS
STP_GE1:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,STP_GE1
	MOV   A,#OMR_FLG
	CALL  MR_GPA1
	MOVX  A,@DPTR
	ANL   A,#NOT (080H OR MMR_BSY)
	MOVX  @DPTR,A
	RET

; -----------------------------------
; Ramenko

TM_ASH_UP:
	MOV   R2,#20H
	SJMP  TM_ASH

TM_ASH_DOWN:
	MOV   R2,#30H
	MOV   A,P5
	ANL   A,#STP_MSA
	JZ    TM_ASH
	RET

TM_ASH_HOME:
	MOV   R2,#10H
	SJMP  TM_ASH

TM_ASH:	MOV   DPTR,#REG_A+OREG_A+OMR_FLG
	MOV   A,#80H
	MOVX  @DPTR,A
	MOV   DPTR,#REG_A+OREG_A+OMR_VG
	MOV   A,#HIGH VG_ASH
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW  VG_ASH
	MOVX  @DPTR,A
	MOV   DPTR,#ASH1_GST
	MOV   A,R2
	MOVX  @DPTR,A
	MOV   DPTR,#REG_A+OREG_A+OMR_FLG
	MOV   A,#MMR_ENI OR MMR_ENR OR MMR_ENG
	MOVX  @DPTR,A
	RET

TM_GEPT:DB    LCD_CLR,'Kam :',0

%IF(%WITH_WHEEL80)THEN(
TM_GOROT:MOV  R1,#2	; REG_C
	CLR   A
	XCH   A,R4
	XCH   A,R5
	MOV   R6,A
	RLC   A
	SUBB  A,ACC
	MOV   R7,A
	JMP   GO_GEP
)FI

; -----------------------------------
; Peristaltika

RSEG	AS____X

P_IRC_W:DS    2         ; Pocet impulsu na IRC kolecku

RSEG    AS____C

; Odcerpani objemu v R45 rychlosti R23
GO_PER: MOV   DPTR,#P_IRC_W
	MOVX  A,@DPTR
	MOV   R7,A
	JNB   ACC.7,GO_PER1
	CPL   A
	INC   A
	MOV   R7,A
	CLR   C
	CLR   A
	SUBB  A,R4
	MOV   R4,A
	CLR   A
	SUBB  A,R5
	MOV   R5,A
GO_PER1:MOV   A,#OMR_FLG        ; odpojit generator
	CALL  MR_GPA1
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	JB    ACC.7,GO_PERE
	ANL   A,#NOT (MMR_ENG OR MMR_ENI OR MMR_ENR)
	ORL   A,#080H
	MOVX  @DPTR,A
	MOV   EA,C
	MOV   A,DPL             ; OMR_GEP
	ADD   A,#LOW(OMR_GEP-OMR_FLG)
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#HIGH(OMR_GEP-OMR_FLG)
	MOV   DPH,A
	CLR   A                 ; koncova poloha OMR_GEP
	MOVX  @DPTR,A           ; bude R45*R7
	INC   DPTR
	MOV   A,R4
	MOV   B,R7
	MUL   AB
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,B
	XCH   A,R5
	MOV   B,R7
	MOV   R0,B
	JB    ACC.7,GO_PER2
	MOV   R0,#0
GO_PER2:MUL   AB
	ADD   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,B
	ADDC  A,#0
	SUBB  A,R0
	MOVX  @DPTR,A
	MOV   A,DPL             ; OMR_MS
	ADD   A,#LOW(OMR_MS-OMR_GEP-3)
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#HIGH(OMR_MS-OMR_GEP-3)
	MOV   DPH,A
	MOV   A,R2              ; maximalni rychlost OMR_MS
	MOV   B,R7              ; bude R23*R7
	MUL   AB
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,B
	XCH   A,R3
	MOV   B,R7
	MUL   AB
	ADD   A,R3
	MOVX  @DPTR,A
	CLR   A                 ; nulovani polohy
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R7,A
	MOV   A,#0
	CALL  MR_GPA1
	CALL  CD_HHC1
	%LDR45i(CI_GEP)         ; start generatoru a regulatoru
	JMP   GO_VG
GO_PERE:MOV   EA,C
	SETB  F0
	RET

; -----------------------------------
; Propojeni na user interface

; Nacte do R4567 aktualni polohu z regulatoru [DPTR+OU_DP]
UR_MRPl:MOV   A,#OU_DP
	MOVC  A,@A+DPTR
	MOV   R1,A
	MOV   A,#OMR_AP
	CALL  MR_GPA1
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	SETB  EA
	MOVX  A,@DPTR
	MOV   R6,A
	MOV   R7,#0
	JNB   ACC.7,UR_MRPl5
	DEC   R7
UR_MRPl5:
	MOV   A,#1
	RET

; Spusti pohyb regulatoru [DPTR+OU_DP] na polohu R4567
UW_MRPl:MOV   A,#OU_DP
	MOVC  A,@A+DPTR         ; cislo regulatoru
	MOV  R1,A
	CLR   F0
	MOV   A,R6
	MOV   C,ACC.7
	XCH   A,R7
	ADDC  A,#0
	JNZ   UW_MRPl5
	MOV   A,R5
	MOV   R6,A
	MOV   A,R4
	MOV   R5,A
	MOV   R4,#0
	CALL  GO_GEP
	RET
UW_MRPl5:SETB F0
	RET

; *******************************************************************
; Krokovy motorek

; Cidla jsou na brane P5 a pri zarezu je na nich nizka uroven
STP_BSB	EQU   7		; 1.
STP_BSA	EQU   6		; 2.
STP_BSC	EQU   5		; 3.
STP_MSB	EQU   1 SHL STP_BSB
STP_MSA	EQU   1 SHL STP_BSA
STP_MSC	EQU   1 SHL STP_BSC

M_STP	EQU   1111B

RSEG	AS____X

STP_ST:	DS    1		; stav automatu krokace
SAMPNUM:DS    2		; cislo zkumavky k najeti
STP_WT:	DS    1		; typ kotouce
STP_WN:	DS    1		; cislo kotouce
STP_RP:	DS    1		; pozadovana poloha
STP_AP:	DS    1		; aktualni poloha
STP_FRC:DS    1		; delicka frekvence
STP_SPD:DS    1		; rychlost
STP_PHA:DS    1		; faze krokace
%IF(0)THEN(
STP_PDCNT:DS  2	; citac pro automaticky power down !!!!!
)FI
STP_TMP:DS    1		; !!!!!!!!!!

RSEG	AS____C

STP:	MOV   DPTR,#STP_ST
	MOVX  A,@DPTR
	JNZ   STP_004
    %IF(0)THEN(
	JNB   STP_APD,STP_003
	MOV   DPTR,#STP_PDCNT	; Automaticky power down
	MOVX  A,@DPTR
	JNZ   STP_002
	DEC   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	JNZ   STP_002
	ORL   P4,#M_STP		; Uvolneni krokace
	CLR   STP_APD
	RET
STP_002:DEC   A
	MOVX  @DPTR,A
    )FI
STP_003:RET
STP_004:
    %IF(0)THEN(
	CLR   STP_APD
    )FI
	JB    STP_ERR,STP_003
	MOV   R7,A
	MOV   DPTR,#STP_FRC
	MOVX  A,@DPTR
	JZ    STP_005
	DEC   A
	MOVX  @DPTR,A
	MOV   A,#080H
	RET
STP_20V:JMP   STP_200
STP_005:MOV   A,R7
	ANL   A,#0F0H

	CJNE  A,#10H,STP_20V
	XRL   A,R7
	JNZ   STP_110
	CLR   A				; Rozbeh motoru
	MOV   DPTR,#STP_SPD
	MOVX  @DPTR,A
	MOV   DPTR,#STP_AP
	MOVX  @DPTR,A
	MOV   DPTR,#STP_RP
	MOV   A,#250
	MOVX  @DPTR,A
	CALL  STP_GEO
	MOV   DPTR,#STP_FRC		; Doba na sfazovani motoru
	MOV   A,#240
	MOVX  @DPTR,A
	SJMP  STP_NST
STP_110:DJNZ  ACC,STP_120
	CALL  STP_GS
	JZ    STP_10E
	CJNE  R2,#20,STP_112		; Prvnich 20 kroku rozjezd
STP_112:JC    STP_114
	MOV   A,P5
	CPL   A
	ANL   A,#STP_MSA OR STP_MSB	; Tma na cidlech
	JZ    STP_NST
STP_114:RET
STP_120:DJNZ  ACC,STP_130
	MOV   A,P5
	ANL   A,#STP_MSA OR STP_MSB
	JNZ   STP_121
	MOV   DPTR,#STP_PHA
	MOVX  A,@DPTR		; Znacka musi byt na fazi 0
    %IF(NOT %STP_8_PHASE)THEN(
	ANL   A,#3
    )ELSE(
	ANL   A,#7
    )FI
	JZ    STP_122
STP_121:CALL  STP_GS			; Neni znacka
	JNZ   STP_114
STP_10E:SETB  STP_ERR
	ORL   P4,#M_STP
	CLR   A
	RET
STP_122:MOV   DPTR,#STP_AP		; Nalezena pocatecni znacka
	MOV   A,#0		; Offset pocatku
	MOVX  @DPTR,A
	MOV   DPTR,#STP_RP
	MOV   A,#50
	MOVX  @DPTR,A
	CALL  STP_GS
STP_NST:MOV   A,R7
	INC   A
	MOV   DPTR,#STP_ST
	MOVX  @DPTR,A
STP_10R:RET
STP_130:DJNZ  ACC,STP_140
	CALL  STP_GS
	JZ    STP_10E
	MOV   A,P5
	CPL   A
	ANL   A,#STP_MSA OR STP_MSB
	JZ    STP_NST
	RET
STP_140:DJNZ  ACC,STP_150
	MOV   A,P5
	ANL   A,#STP_MSA
	JZ    STP_145
	CALL  STP_GS
	JZ    STP_10E
	RET
STP_145:MOV   DPTR,#STP_AP		; Urceni typu kotouce
	MOVX  A,@DPTR
	CALL  STP_SWT
	JZ    STP_10E
	MOV   B,A
	MOV   DPTR,#SAMPNUM
	MOVX  A,@DPTR
	JZ    STP_147
	DEC   A
	ADD   A,#-40
	JC    STP_147		; Omezeni do 40
	ADD   A,#40
STP_147:MUL   AB
	MOV   B,A
	CALL  STP_GWSO		; Pocatecni offset
	ADD   A,B		; Bylo 40, pro 25 o 56, pro 80 o 55
	MOV   DPTR,#STP_RP
	MOVX  @DPTR,A
	CALL  STP_GS
STP_NSV:JMP   STP_NST
STP_150:DJNZ  ACC,STP_160
	CALL  STP_GS
	JZ    STP_155
	RET
STP_155:MOV   DPTR,#STP_FRC
	MOV   A,#100
	MOVX  @DPTR,A
	JMP   STP_NSV
STP_160:DJNZ  ACC,STP_170
	MOV   A,#20H
	MOV   DPTR,#STP_ST
	MOVX  @DPTR,A
	RET
STP_170:

	JMP   STP_10E

STP_200:CJNE  A,#20H,STP_300
	CLR   A
	RET

STP_300:RET

STP_OFF:CLR   A
	MOV   DPTR,#STP_ST
	MOVX  @DPTR,A
	ORL   P4,#M_STP
	CLR   STP_ERR
    %IF(0)THEN(
	CLR   STP_APD
    )FI
	RET


; Krokuje dokud STP_AP rovno STP_RP
; =================================
;stara se o rychlosti rozjezdu a brzdeni STP_SPD a STP_FRC
;vraci: R2 .. STP_AP
;	ACC = 0 .. konec pohybu
;rusi:	R1
STP_GS:
    %IF(%STP_8_PHASE)THEN(
	MOV   DPTR,#STP_PHA
	MOVX  A,@DPTR
	JNB   ACC.0,STP_GS0
	MOV   DPTR,#STP_AP
	MOVX  A,@DPTR
	MOV   R2,A
	MOV   DPTR,#STP_SPD
	MOVX  A,@DPTR
	JMP   STP_GS9
STP_GS0:
    )FI
	MOV   DPTR,#STP_RP
	MOVX  A,@DPTR
	MOV   R1,A
	MOV   DPTR,#STP_AP
	MOVX  A,@DPTR
	MOV   R2,A		; R2=STP_AP
	XCH   A,R1
	CLR   C
	SUBB  A,R1
	JZ    STP_GSR
	DEC   A
	XCH   A,R1		; R1=STP_RP-STP_AP-1
	INC   A
	MOVX  @DPTR,A		; STP_AP+=1
STP_GS1:MOV   DPTR,#STP_SPD
	MOVX  A,@DPTR
	SUBB  A,R1
	JC    STP_GS3
	MOV   A,R1
	SJMP  STP_GS8
STP_GS3:MOVX  A,@DPTR
	INC   A
	CJNE  A,#STP_PRN,STP_GS8
	DEC   A
STP_GS8:MOVX  @DPTR,A		; STP_SPD
STP_GS9:ADD   A,#STP_PRT-STP_Gb1
	MOVC  A,@A+PC
STP_Gb1:MOV   DPTR,#STP_FRC
	MOVX  @DPTR,A
STP_GEO:MOV   DPTR,#STP_PHA
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
STP_GEN:
    %IF(0)THEN(
	CLR   STP_APD
    )FI
    %IF(NOT %STP_8_PHASE)THEN(
	ANL   A,#3
    )ELSE(
	ANL   A,#7
    )FI
	ADD   A,#STP_TAB-STP_Gb2
	MOVC  A,@A+PC
STP_Gb2:ORL   P4,#M_STP
	ANL   P4,A
STP_GSR:RET

%IF(NOT %STP_8_PHASE)THEN(
STP_TAB:DB    11110011B
	DB    11111001B
	DB    11111100B
	DB    11110110B

	DB    11110111B
	DB    11111011B
	DB    11111101B
	DB    11111110B
)ELSE(
STP_TAB:DB    11110011B
	DB    11111011B
	DB    11111001B
	DB    11111101B
	DB    11111100B
	DB    11111110B
	DB    11110110B
	DB    11110111B
)FI

STP_PRT:
    %IF(NOT %STP_8_PHASE)THEN(
	DB   200
    )FI
	DB    80
	DB    40
	DB    20
	DB    10
	DB    7
	DB    5
    %IF(%STP_8_PHASE)THEN(
	DB    3
	DB    2
    )FI
STP_PRN	SET   $-STP_PRT

; Try find wheel type  8, 5, (zatim ne 4 )
STP_SWT:MOV   DPTR,#STP_TMP	; !!!!!!!!!!
	MOVX  @DPTR,A		; !!!!!!!!!!!
	MOV   R0,#1
	ADD   A,#-10
	JC    STP_SWE
	ADD   A,#-7+10
	JC    STP_SW1		; 7 az 9
	INC   R0
	ADD   A,#-7+7
	JC    STP_SWE
	ADD   A,#-4+7
	JC    STP_SW1		; 4 az 6
STP_SWE:CLR   A
	MOV   DPTR,#STP_WT
	MOVX  @DPTR,A
	RET
STP_SW1:MOV   DPTR,#STP_WT
	MOVX  A,@DPTR
	XRL   A,R0
	JZ    STP_SW2
	SETB  STP_WCH
STP_SW2:MOV   A,R0
	MOVX  @DPTR,A
STP_GWD:MOV   DPTR,#STP_WT
	MOVX  A,@DPTR
	ANL   A,#3
	ADD   A,#STP_WTT-STP_GW8
	MOVC  A,@A+PC
STP_GW8:RET
STP_WTT:DB    0, 8, 5, 0

; Pocatecni ofsety podle typu kotouce
STP_GWSO:MOV  DPTR,#STP_WT
	MOVX  A,@DPTR
	ANL   A,#3
	ADD   A,#STP_WTSO-STP_GWSO1
	MOVC  A,@A+PC
STP_GWSO1:RET

%IF(%WITH_WHEEL80)THEN(
STP_WTSO:DB   55,56,55,55
)ELSE(
STP_WTSO:DB   40,40,40,40
)FI

; Cteni optocidel polohy kotouce
STP_PSR:CLR   A
	MOV   R5,A
	MOV   B,P5
	MOV   C,B.STP_BSC
	RLC   A
	MOV   C,B.STP_BSB
	RLC   A
	MOV   C,B.STP_BSA
	RLC   A
	MOV   R4,A
	MOV   A,#1
	RET

TM_STP:	MOV   DPTR,#TM_STPT
	CALL  cPRINT
	MOV   R6,#8
	MOV   R7,#0C0H
	CALL  INPUTi
	JB    F0,TM_STPR
TM_STP1:MOV   A,R4
	MOV   DPTR,#SAMPNUM
	MOVX  @DPTR,A
	JNZ   TM_STP2
	JMP   STP_OFF
TM_STP2:MOV   A,#10H
	MOV   DPTR,#STP_ST
	MOVX  @DPTR,A
TM_STPR:RET

TM_STPT:DB    LCD_CLR,'Sample :',0

TM_SUP:	MOV   DPTR,#STP_AP
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	MOV   DPTR,#STP_PHA
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	JMP   STP_GEN

TM_SDO:	MOV   DPTR,#STP_AP
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	MOV   DPTR,#STP_PHA
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	JMP   STP_GEN

; Drzeni krokace na fazi 0
TM_STPHLD:CLR A
	MOV   DPTR,#STP_PHA
	MOVX  @DPTR,A
	JMP   STP_GEN

%IF(0)THEN(
; Pridrzeni kotouce pro obsluhu
TM_STPHLDT:
	%LDR45i(600*10)
	CLR   EA
	MOV   DPTR,#STP_ST
	MOVX  A,@DPTR
	JNZ   TM_STPHLDT9
    %IF(0)THEN(
	JB    MR_FLG.BMR_BSY,TM_STPHLDT9
	MOV   DPTR,#SEK_ST
	MOVX  A,@DPTR
	JNZ   TM_STPHLDT9
    )FI
	MOV   DPTR,#STP_PDCNT
	MOV   A,R4
	MOVX  @DPTR,A		; Doba pridrzeni
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   DPTR,#STP_PHA
	MOVX  A,@DPTR
	CALL  STP_GEN
	SETB  STP_APD
TM_STPHLDT9:
	SETB  EA
	RET
)FI

; *******************************************************************
; System prevodniku

RSEG	AS____X

ADC0:	DS    2
ADC1:	DS    2
ADC2:	DS    2
ADC3:	DS    2
ADC4:	DS    2
ADC5:	DS    2
ADC6:	DS    2
ADC7:	DS    2

RSEG	AS____C

ADC_D:	MOV   B,ADCON
	JNB   B.4,ADC_DR
	MOV   A,B
	ANL   A,#7
	RL    A
	ADD   A,#LOW  ADC0
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH ADC0
	MOV   DPH,A
	MOV   A,B
	ANL   A,#0C0H
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,ADCH
	MOVX  @DPTR,A
	MOV   A,B
	ANL   A,#7
	INC   A
	JNB   ACC.3,ADC_S2
	ANL   ADCON,#NOT 10H
ADC_DR:	RET

ADC_S:  CLR   A
	MOV   B,ADCON
	JB    B.3,ADC_DR
ADC_S2:	ANL   A,#07H
	MOV   ADCON,A
	SETB  ACC.3
	MOV   ADCON,A
	RET

; *******************************************************************
; Davkovaci ventil

INV_P1	BIT   P3.4
INV_P2	BIT   P1.4
INV_E	BIT   P4.4

RSEG	AS____X

INV_ST:	DS    1		; pozadovany a skutecny stav ventilu
BINV_P1 EQU   0		; aktualni poloha 1
BINV_P2 EQU   1		; aktualni poloha 2
BINV_SF EQU   2		; filtr senzoru
BINV_IP EQU   3		; v pohybu
BINV_G1 EQU   4		; pozadavek na polohu 1
BINV_G2 EQU   5		; pozadavek na polohu 2
BINV_ERR EQU  7		; chyba

INV_TIM:DS    1
INV_TIML EQU  50	; casovy limit pohybu v 1/25 s
INV_AUX:DS    2		; !=0 pozadavek na rizeni AUX v manualnim r.
MIVA_ON EQU   80H

RSEG	AS____C

; Prikaz pro pohyb ventilu do PO1 a PO2
INV_GP1:MOV   R3,#(1 SHL BINV_IP)OR(1 SHL BINV_G1)
	MOV   DPTR,#CA_OFF
	SJMP  INV_G01
; Druha poloha
INV_GP2:MOV   R3,#(1 SHL BINV_IP)OR(1 SHL BINV_G2)
	MOV   DPTR,#CA_INJ
INV_G01:MOVX  A,@DPTR
	MOV   R4,A
	MOV   DPTR,#INV_TIM
	MOV   A,#INV_TIML
	MOVX  @DPTR,A
	MOV   A,R3
	MOV   DPTR,#INV_ST
	MOVX  @DPTR,A		; prikaz pro ventil
	JB    %RUN_FL,INV_G05
	MOV   DPTR,#INV_AUX
	MOVX  A,@DPTR
	ANL   A,#MIVA_ON
	JZ    INV_G05
	JMP   S_AUX		; prenos informace na AUX
INV_G05:RET

; Rizeni ventilu podle stavu INV_ST
INV_G:	MOV   DPTR,#INV_ST
	MOVX  A,@DPTR
	JB    ACC.BINV_IP,INV_G08
	CLR   C
	CLR   A
	RET
INV_G08:JB    ACC.BINV_G1,INV_G10
	JB    ACC.BINV_G2,INV_G20
	RET
INV_G10:JB    INV_P1,INV_G12	; Presun do polohy 1
	CLR   INV_E
	CLR   ACC.BINV_SF
	MOVX  @DPTR,A
	JB    FL_25Hz,INV_G40
	RET
INV_G12:JB    ACC.BINV_SF,INV_G14
	SETB  ACC.BINV_SF
	MOVX  @DPTR,A
	RET
INV_G14:SETB  INV_E
	ANL   A,#0
	ORL   A,#(1 SHL BINV_P1)
	MOVX  @DPTR,A
	CLR   A
	CLR   C
	RET
INV_G20:JB    INV_P2,INV_G22	; Presun do polohy 2
	CLR   INV_E
	CLR   ACC.BINV_SF
	MOVX  @DPTR,A
	JB    FL_25Hz,INV_G40
	RET
INV_G22:JB    ACC.BINV_SF,INV_G24
	SETB  ACC.BINV_SF
	MOVX  @DPTR,A
	RET
INV_G24:SETB  INV_E
	ANL   A,#0
	ORL   A,#(1 SHL BINV_P2)
	MOVX  @DPTR,A
	CLR   A
	CLR   C
	RET
INV_G40:MOV   DPTR,#INV_TIM	; Kontrola cas limitu
	MOVX  A,@DPTR
	DJNZ  ACC,INV_G42
	SETB  INV_E
	MOV   DPTR,#INV_ST
	MOV   A,#(1 SHL BINV_ERR)
	MOVX  @DPTR,A
	SETB  C
	CLR   A
	RET
INV_G42:MOVX  @DPTR,A
	RET

TM_INV:	MOV   A,R2
	MOV   DPTR,#INV_ST
	ORL   A,#1 SHL BINV_IP
	MOVX  @DPTR,A
	RET

TM_MAN:	MOV   DPTR,#SEK_ST
	MOVX  A,@DPTR
	JNZ   TM_MAN9
	MOV   DPTR,#INV_ST
	MOVX  A,@DPTR
	JB    ACC.BINV_IP,TM_MAN9
	JB    ACC.BINV_P2,TM_MAN7
	JMP   INV_GP2
TM_MAN7:JMP   INV_GP1
TM_MAN9:RET

; *******************************************************************
; Cidla kapaliny ve smycce

%DEFINE (LPS_NEW_CAL) (1)

%IF (%LPS_NEW_CAL) THEN (
LPS_FILTC SET 64	; 64 odpovida pomeru 1
)ELSE(
LPS_FILTC SET 32	; 32 odpovida pomeru 1/2
)FI

LPS_AD1	XDATA ADC3
LPS_AD2	XDATA ADC4

LPS_FLG	DATA  HW_FLG1	; FL_LPS1, FL_LPS2
LPS_FLGB0 BIT LPS_FLG.0
B_LPS1	SET   FL_LPS1-LPS_FLGB0
B_LPS2	SET   FL_LPS2-LPS_FLGB0

RSEG	AS____X

LPS_LEN EQU   7

LPS_BAS:
LPS_CN1:DS    1         ; citac zmeny cidla 1
LPS_TR1:DS    2         ; komparacni uroven cidla 1
LPS_SM1:DS    2         ; pro kalibraci
LPS_TD1:DS    2         ; doba pro preklopeni cidla 1

LPS_CN2:DS    1         ; citac zmeny cidla 2
LPS_TR2:DS    2         ; komparacni uroven cidla 2
LPS_SM2:DS    2         ; pro kalibraci
LPS_TD2:DS    2         ; doba pro preklopeni cidla 2

LPS_END	SET   $

RSEG	AS____C

LPS_TST:JNB   FL_25Hz,LPS_T99
	CALL  LPS_TS1
	CALL  LPS_TS2
LPS_T99:RET

LPS_TS1:MOV   DPTR,#LPS_AD1	; ADC
	MOV   R1,#LPS_CN1-LPS_BAS ; posun baze
	MOV   R2,#1 SHL B_LPS1	; maska bitu
	SJMP  LPS_T10
LPS_TS2:MOV   DPTR,#LPS_AD2
	MOV   R1,#LPS_CN2-LPS_BAS
	MOV   R2,#1 SHL B_LPS2
LPS_T10:MOVX  A,@DPTR		; Nacteni ADC do R45
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   A,R1
	ADD   A,#LOW  LPS_BAS
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH LPS_BAS
	MOV   DPH,A
	CLR   C			; porovnani R45 s LPS_TR?
	MOV   A,#1
	MOVC  A,@A+DPTR		; LPS_TRx
	SUBB  A,R4
	MOV   A,#2
	MOVC  A,@A+DPTR
	SUBB  A,R5
	MOV   A,R2
	JNC   LPS_T20
	CLR   A
LPS_T20:XRL   A,LPS_FLG
	ANL   A,R2
	JZ    LPS_T39		; Beze zmeny
	MOV   A,#5		; LPS_TDx
	MOVC  A,@A+DPTR
	MOV   R3,A
	MOVX  A,@DPTR
	CLR   C
	SUBB  A,R3
	ADDC  A,R3
	JC    LPS_T39
	MOV   A,R2		; Skutecne zmena
	XRL   LPS_FLG,A
	CLR   A
LPS_T39:MOVX  @DPTR,A
	RET

; Priprava kalibrace cidel
LPS_CS: MOV   DPTR,#LPS_CN1
	MOV   R0,#LPS_TD1-LPS_CN1
	CLR   A
LPS_CS1:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,LPS_CS1
	MOV   DPTR,#LPS_CN2
	MOV   R0,#LPS_TD2-LPS_CN2
	CLR   A
LPS_CS2:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,LPS_CS2
	RET

; Prvni kalibrace - kalibrace vody
LPS_CW: JNB   FL_25Hz,LPS_CW9
	MOV   DPTR,#LPS_AD1	; ADC
	MOV   R1,#LPS_TR1-LPS_BAS ; posun baze
	CALL  LPS_ADD
	MOV   DPTR,#LPS_AD2	; ADC
	MOV   R1,#LPS_TR2-LPS_BAS ; posun baze
	CALL  LPS_ADD
	MOV   DPTR,#LPS_CN1
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	CJNE  A,#LPS_FILTC,LPS_CW9
	CLR   A
	MOVX  @DPTR,A
LPS_CW9:RET

; Druha kalibrace - kalibrace vzduchu
LPS_CA: JNB   FL_25Hz,LPS_CA9
	MOV   DPTR,#LPS_AD1	; ADC
	MOV   R1,#LPS_SM1-LPS_BAS ; posun baze
	CALL  LPS_ADD
	MOV   DPTR,#LPS_AD2	; ADC
	MOV   R1,#LPS_SM2-LPS_BAS ; posun baze
	CALL  LPS_ADD
	MOV   DPTR,#LPS_CN1
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	CJNE  A,#LPS_FILTC,LPS_CA9
	CLR   A
	MOVX  @DPTR,A
	CLR   F0
	MOV   DPTR,#LPS_TR1
	CALL  LPS_CTR
	MOV   DPTR,#LPS_TR2
	CALL  LPS_CTR
	CLR   A
	RET
LPS_CA9:MOV   A,#1
	RET

LPS_ADD:MOVX  A,@DPTR		; Nacteni ADC/64 do R45
	INC   DPTR
	RL    A
	RL    A
	ANL   A,#3
	MOV   R4,A
	MOVX  A,@DPTR
	RL    A
	RL    A
	MOV   R5,A
	ANL   A,#3
	XCH   A,R5
	ANL   A,#NOT 3
	ORL   A,R4
	MOV   R4,A
	MOV   A,R1		; LPS polozka do DPTR
	ADD   A,#LOW  LPS_BAS
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH LPS_BAS   ; [DPTR] += R45
	MOV   DPH,A
	MOVX  A,@DPTR
	ADD   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOVX  @DPTR,A
	RET

%IF (%LPS_NEW_CAL) THEN (

; LPS_TRx = Voda + (Vzduch - Voda) / 4
LPS_CTR:MOVX  A,@DPTR		; LPS_TRx
	MOV   R4,A
	MOV   A,#1
	MOVC  A,@A+DPTR         ; LPS_TRx+1
	MOV   R5,A             	; R45 = LPS_TRx
	MOV   A,#2
	MOVC  A,@A+DPTR		; LPS_SMx
	CLR   C
	SUBB  A,R4
	MOV   R6,A
	MOV   A,#3
	MOVC  A,@A+DPTR		; LPS_SMx+1
	SUBB  A,R5
	MOV   R7,A		; R67 = LPS_SMx - LPS_TRx
	JC    LPS_CT6
	ADD   A,#-3H	; max 80h
	JC    LPS_CT9
LPS_CT6:SETB  F0
LPS_CT9:CLR   C			; R67 >>= 1
	MOV   A,R7
	RRC   A
	MOV   R7,A
	MOV   A,R6
	RRC   A
	MOV   R6,A
	CLR   C			; R67 >>= 1
	MOV   A,R7
	RRC   A
	MOV   R7,A
	MOV   A,R6
	RRC   A
	MOV   R6,A
	MOV   A,R6		; R45 + R67
        ADD   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	ADDC  A,R5
	MOVX  @DPTR,A
	INC   DPTR
	RET

)ELSE(

; LPS_TRx = (Vzduch + Voda) /2
LPS_CTR:MOVX  A,@DPTR		; LPS_TRx
	MOV   R4,A
	MOV   A,#1
	MOVC  A,@A+DPTR         ; LPS_TRx+1
	MOV   R5,A
	MOV   A,#2
	MOVC  A,@A+DPTR		; LPS_SMx
	MOV   R6,A
	ADD   A,R4
	MOVX  @DPTR,A
	MOV   A,#3
	MOVC  A,@A+DPTR		; LPS_SMx+1
	MOV   R7,A
	ADDC  A,R5
	INC   DPTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	SUBB  A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	SUBB  A,R5
	MOVX  @DPTR,A
	JC    LPS_CT6
	ADD   A,#-3H	; max 80h
	JC    LPS_CT9
LPS_CT6:SETB  F0
LPS_CT9:RET

)FI

; Kalibrace cidel
TM_LPS:	MOV   DPTR,#SEK_ST
	MOVX  A,@DPTR
	JNZ   TM_LPS9
	MOV   A,#SEK_ST_LPSC
	MOVX  @DPTR,A
	MOV   DPTR,#SEK_CFG
	MOVX  A,@DPTR
	ORL   A,#MSEK_LPS
	MOVX  @DPTR,A
TM_LPS9:RET

; *******************************************************************
; Sekvencer davkovani

RSEG	AS____X

SEK_ST:	DS    1		; stav automatu davkovani

SEK_CFG:DS    1		; konfigurace sekvenceru
BSEK_LPS SET  0		; pouzivaji se cidla kapaliny
BSEK_NOV SET  1		; je pritomen dusikovy ventil
BSEK_REV SET  2		; kalibrace po davkovani
MSEK_LPS SET  1 SHL BSEK_LPS
MSEK_NOV SET  1 SHL BSEK_NOV
MSEK_REV SET  1 SHL BSEK_REV

SEK_TIM:DS    2		; Timer sekvenceru
P_VOL1:	DS    2		; Pevny objem sani vzorku po 1 cidlu
P_INJT:	DS    2		; Doba po kterou je otoceny davk. ventil

PER_SPD_HI EQU	8	; Vysoka rychlost peristaltiky
PER_SPD_LO EQU	2	; Nizka rychlost peristaltiky
PER_SPD_ZM EQU	1	; Rychlost slimak

RSEG	AS____C

; Pricte P_VOL1 k R45
ADD_VOL1:MOV  DPTR,#P_VOL1
	MOVX  A,@DPTR
	ADD   A,R4
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOV   R5,A
	RET

; Odecte P_VOL1 od R45
SUB_VOL1:MOV  DPTR,#P_VOL1
	SETB  C
	MOVX  A,@DPTR
	CPL   A
	ADDC  A,R4
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	CPL   A
	ADDC  A,R5
	MOV   R5,A
	RET

; Cekani na dobehnuti citace SEK_TIM
SEK_WFT:JNB   FL_25Hz,SEK_WF8
	MOV   DPTR,#SEK_TIM
	MOVX  A,@DPTR
	MOV   R0,A
	MOV   A,#1
	MOVC  A,@A+DPTR
	MOV   R1,A
	ORL   A,R0
	JZ    SEK_WF9
	MOV   A,R0
	ADD   A,#0FFH
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	ADDC  A,#0FFH
	MOVX  @DPTR,A
SEK_WF8:MOV   A,#1
SEK_WF9:RET

SEK_E:	SETB  SEK_ERR
	CALL  STP_OFF
	MOV   R1,#1
	CALL  CLR_GEP
	SETB  GS_BUF.BGS_RDY		; signal pro cekajici proces
SEK_E1:	RET

SEK:	JB    SEK_ERR,SEK_E1
	MOV   DPTR,#SEK_ST
	MOVX  A,@DPTR
	CJNE  A,#SEK_STN,SEK1
SEK1:	JNC   SEK_E
	RL    A
	MOV   DPTR,#SEK_TAB
	MOV   R0,A
	MOVC  A,@A+DPTR
	PUSH  ACC
	MOV   A,R0
	INC   A
	MOVC  A,@A+DPTR
	PUSH  ACC
	RET
; Tabulka stavu sekvenceru
SEK_TAB:%W    (SEK_00)
	%W    (SEK_01)
	%W    (SEK_02)
SEK_ST_DOFCLEAN SET ($-SEK_TAB)/2 ; Cekani na vodu pri cisteni
	%W    (SEK_03)
	%W    (SEK_04)
SEK_ST_FAIR SET ($-SEK_TAB)/2	; Cekani na vzduch
	%W    (SEK_05)
	%W    (SEK_06)
	%W    (SEK_07)
SEK_ST_STPS SET	($-SEK_TAB)/2	; Rozjeti krokace
	%W    (SEK_08)
	%W    (SEK_09)
SEK_ST_LPLA SET	($-SEK_TAB)/2	; Sani do cidla A
	%W    (SEK_10)
	%W    (SEK_11)
	%W    (SEK_12)
SEK_ST_LPLB SET	($-SEK_TAB)/2	; Sani do cidla B
	%W    (SEK_13)
	%W    (SEK_14)
SEK_ST_WINJ SET	($-SEK_TAB)/2	; Stav cekani na nadavkovani
	%W    (SEK_15)
	%W    (SEK_16)
	%W    (SEK_17)
	%W    (SEK_18)
	%W    (SEK_19)
SEK_ST_FCLN SET	($-SEK_TAB)/2	; Vyplach ve volnem case
	%W    (SEK_20)
SEK_ST_LPSC SET	($-SEK_TAB)/2	; Stav pocatku kalibrace cidel
	%W    (SEK_21)
	%W    (SEK_22)
	%W    (SEK_23)
	%W    (SEK_24)
	%W    (SEK_25)
	%W    (SEK_26)
SEK_ST_LPEC SET	($-SEK_TAB)/2	; Konec kalibrace cidel
	%W    (SEK_27)
	%W    (SEK_28)
SEK_STN	SET   ($-SEK_TAB)/2

; Klidovy stav
SEK_00:	CALL  INV_G
	JMP   STP

; Stav 1 - zavrit ventil zkontrolovat ramenko
SEK_01: JB    MR_FLG.BMR_ERR,SEK_02E
	JB    MR_FLG.BMR_BSY,SEK_R
	MOV   DPTR,#ASH1_GST
	MOV   A,#50H
	MOVX  @DPTR,A

SEK_NST:MOV   DPTR,#SEK_ST
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
SEK_R:	RET

; Stav 2 - cekat na vytazeni ramenka pak cisteni
SEK_02:	JB    MR_FLG.BMR_ERR,SEK_02E
	JB    MR_FLG.BMR_BSY,SEK_02R
    %IF(NOT %WITH_WHEEL80)THEN(
	JNB   ASH_ZF,SEK_02E
    )FI
	MOV   DPTR,#SEK_CFG
	MOVX  A,@DPTR
	JNB   ACC.BSEK_REV,SEK_025
	JMP   SEK_STPS
SEK_025:CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_HI)	; Vyplach peristaltiky
	%LDR45i(614)		; do cidel
	CALL  ADD_VOL1		; Pridani objemu P_VOL1
	CALL  GO_PER
	JB    F0,SEK_02E
	CLR   FL_LPS1
	CLR   FL_LPS2
	MOV   DPTR,#SEK_ST
	MOV   A,#SEK_ST_DOFCLEAN
	MOVX  @DPTR,A
SEK_02R:RET
SEK_02E:JMP   SEK_E


; Stav 3 - cekat na vodu v obou cidlech, pak vyplach objemem
SEK_03: JB    MR_FLG.BMR_ERR,SEK_02E
	JNB   FL_ENLPS,SEK_032
	JNB   MR_FLG.BMR_BSY,SEK_02E
	CALL  LPS_TST			; Test cidel kapaliny
	JNB   FL_LPS1,SEK_02R
	JNB   FL_LPS2,SEK_02R		; V 1 nebo 2 neni kapalina
	SJMP  SEK_033
SEK_032:JB    MR_FLG.BMR_BSY,SEK_02R
SEK_033:CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_HI)
	%LDR45i(204)		; Vyplach peristaltiky
	CALL  GO_PER		; pevny objem
	JB    F0,SEK_02E
	JMP   SEK_NST

; Stav 4 - vyplach urcitym objemem, pak oddeleni vzduchem
SEK_04: JB    MR_FLG.BMR_ERR,SEK_02E
	JNB   FL_ENLPS,SEK_044
	CALL  LPS_TST			; Test cidel kapaliny
	JNB   FL_LPS1,SEK_042	; V 1 nebo 2 neni kapalina
	JB    FL_LPS2,SEK_044
SEK_042:SJMP  SEK_025
SEK_044:JB    MR_FLG.BMR_BSY,SEK_02R
SEK_045:CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_HI)	; Oddeleni vzduchem
	%LDR45i(-307)		; do cidel
	CALL  SUB_VOL1		; Pridani objemu P_VOL1
	CALL  GO_PER
	JB    F0,SEK_02E
	MOV   DPTR,#SEK_ST
	MOV   A,#SEK_ST_FAIR
	MOVX  @DPTR,A
SEK_04R:RET

; Stav 5 - cekat vzduch v cidlech, pak pevny objem vzduchu
SEK_05:	JB    MR_FLG.BMR_ERR,SEK_02E
	JNB   FL_ENLPS,SEK_052
	JNB   MR_FLG.BMR_BSY,SEK_02E
	CALL  LPS_TST			; Test cidel kapaliny
	JB    FL_LPS1,SEK_04R
	JB    FL_LPS2,SEK_04R		; V 1 a 2 musi byt sucho
	SJMP  SEK_053
SEK_052:JB    MR_FLG.BMR_BSY,SEK_04R
SEK_053:CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_HI)
	%LDR45i(-102)		; Oddeleni vzduchem
	CALL  GO_PER		; pevny objem
SEK_058:JB    F0,SEK_09E
	JMP   SEK_NST

; Stav 6 - natazeni pevneho objemu vzduchu
SEK_06:	JB    MR_FLG.BMR_ERR,SEK_09E
	JNB   FL_ENLPS,SEK_062
	CALL  LPS_TST			; Test cidel kapaliny
	JB    FL_LPS1,SEK_061
	JNB   FL_LPS2,SEK_062		; V 1 a 2 musi byt sucho
SEK_061:SJMP  SEK_045
SEK_062:JB    MR_FLG.BMR_BSY,SEK_04R
SEK_063:MOV   R1,#1
	CALL  STP_GEP
	JMP   SEK_STPS

; Stav 7 - rezerva
SEK_07:	JMP   SEK_STPS

; Zacatek vlasni davkovaci sekvence
SEK_STPS:MOV  DPTR,#STP_ST
	MOV   A,#10H			; Start krokace
	MOVX  @DPTR,A
	MOV   DPTR,#SEK_ST
	MOV   A,#SEK_ST_STPS
	MOVX  @DPTR,A
	RET

; Stav 8 - cekani na krokac pak start ramenka dolu
SEK_08: JB    STP_ERR,SEK_09E
	CALL  STP
	JNZ   SEK_10R
	JB    STP_ERR,SEK_09E
	MOV   A,P5
	ANL   A,#STP_MSA
	JNZ   SEK_09E
	MOV   DPTR,#ASH1_GST
	MOV   A,#30H
	MOVX  @DPTR,A
	JMP   SEK_NST

; Stav 9 - cekani na ramenko a start sani
SEK_09:	JB    MR_FLG.BMR_ERR,SEK_09E
	JB    MR_FLG.BMR_BSY,SEK_10R
SEK_09N:CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_LO)
	%LDR45i(-307)		; Objem do ktereho musi prijit
	CALL  GO_PER		; kapalina do 1 cidla
	JB    F0,SEK_09E
	CLR   FL_LPS1
	CLR   FL_LPS2
	MOV   DPTR,#SEK_ST
	MOV   A,#SEK_ST_LPLA
	MOVX  @DPTR,A
	RET
SEK_09E:JMP   SEK_E

; Stav 10 - sani do cidla 1 pak sani urciteho objemu
SEK_10:	JB    MR_FLG.BMR_ERR,SEK_09E
	JNB   FL_ENLPS,SEK_101
	CALL  LPS_TST			; Test cidel kapaliny
	JB    FL_LPS1,SEK_104		; V 1 je kapalina
	JNB   MR_FLG.BMR_BSY,SEK_09E
	RET
SEK_101:JNB   MR_FLG.BMR_BSY,SEK_104	; Na objem bez cidla
SEK_10R:RET
SEK_104:JB    FL_LPS2,SEK_09E		; V 2 je kapalina - error
	CLR   F0
	MOV   R1,#1
	MOV   DPTR,#P_VOL1	; Dosati objemu vzorku
	MOVX  A,@DPTR
	CPL   A
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	CPL   A
	MOV   R5,A
	%LDR23i(PER_SPD_ZM)
	CALL  GO_PER
	JB    F0,SEK_09E
	%LDMXi(SEK_TIM,50)
	JMP   SEK_NST

; Stav 11 - dosati objemu pak ramenko nahoru
SEK_11: JNB   FL_ENLPS,SEK_112
	CALL  LPS_TST
	JB    FL_LPS1,SEK_112
	MOV   R1,#1		; Vzduch v cidle 1
	MOV   A,#OMR_AP
	CALL  MR_GPA1
	MOVX  A,@DPTR		; Ignorovat bubliny na zacatku
	MOV   R4,A
	INC   DPTR              ; Objem - pozor na znamenko
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   A,R4
	ADD   A,#LOW (20)
	MOV   A,R5
	ADDC  A,#HIGH(20)
	JB    ACC.7,SEK_111	; Zopakovat pokus o nasati
	JMP   SEK_09N
SEK_111:MOV   A,R4
	ADD   A,#LOW (40)
	MOV   A,R5
	ADDC  A,#HIGH(40)	; Ignorovat bubliny na zacatku
	JB    ACC.7,SEK_09E	; jinak ERROR
SEK_112:JNB   FL_LPS2,SEK_114
	CALL  SEK_WFT		; Kapalina i jiz v druhem cidle
	JZ    SEK_118
	SJMP  SEK_116
SEK_114:%LDMXi(SEK_TIM,50)
SEK_116:JB    MR_FLG.BMR_ERR,SEK_12E
	JB    MR_FLG.BMR_BSY,SEK_12R
SEK_118:CLR   F0
	MOV   R1,#1		; Zastavit peristaltiku
	CALL  STP_GEP
	MOV   DPTR,#ASH1_GST
	MOV   A,#20H		; Pohyb ramenka nahoru
	MOVX  @DPTR,A
	JMP   SEK_NST

; Stav 12 - jizda ramenka nahoru pak sani do cidla 2
SEK_12:	JB    MR_FLG.BMR_ERR,SEK_12E
	JB    MR_FLG.BMR_BSY,SEK_12R
	CALL  STP_OFF
SEK_125:CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_ZM)
	%LDR45i(-204)		; Objem do ktereho musi prijit
	CALL  GO_PER		; kapalina do 2 cidla
	JB    F0,SEK_12E
	MOV   DPTR,#SEK_ST
	MOV   A,#SEK_ST_LPLB
	MOVX  @DPTR,A
SEK_12R:RET
SEK_12E:JMP   SEK_E

; Stav 13 - sani do cidla 2
SEK_13: JB    MR_FLG.BMR_ERR,SEK_12E
	JNB   FL_ENLPS,SEK_132
	CALL  LPS_TST			; Test cidel
	JB    FL_LPS2,SEK_134		; Kapalina v cidle 2
	JNB   FL_LPS1,SEK_12E		; Bublina v cidle 1
	JNB   MR_FLG.BMR_BSY,SEK_12E
	RET
SEK_132:JNB   MR_FLG.BMR_BSY,SEK_134	; Jede se bez cidel
	RET
SEK_134:CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_ZM)
	%LDR45i(-4)		; Objem presati vzorku
	CALL  GO_PER		; za 2 cidlo
	JB    F0,SEK_12E
	JMP   SEK_NST

; Stav 14 - sani vzorku kousek za cidlo 2
SEK_14:	JB    MR_FLG.BMR_ERR,SEK_12E
	JNB   FL_ENLPS,SEK_142
	CALL  LPS_TST			; Test cidel
	JNB   FL_LPS2,SEK_125		; Bublina v cidle 2
	JNB   FL_LPS1,SEK_12E		; Bublina v cidle 1
SEK_142:JNB   MR_FLG.BMR_BSY,SEK_144	; Jede se bez cidel
	RET
SEK_144:MOV   R1,#1
	CALL  STP_GEP
	SETB  GS_BUF.BGS_RDY		; pripraveno na davkovani
%IF(0)THEN(
	MOV   R1,#1			; Vypmout rizeni peristaltiky
	CALL  CLR_GEP			; !!!!!!!!!!!!!!!!!
)FI
%IF(0)THEN(
	CLR   A				; ukoncit drzeni ramenka
	MOV   DPTR,#ASH1_GST
	MOVX  @DPTR,A			; !!!!!!!!!!!!!!!!!
)FI
	JMP   SEK_NST

; Stav 15 - casova syncronizace pak davkovani do 2
SEK_15: JNB   FL_WINJ,SEK_158
	JB    FL_DINJ,SEK_158
SEK_15R:RET
SEK_158:CLR   FL_DINJ
	CALL  INV_GP2		; Otocit do 2
	JMP   SEK_NST

; Stav 16 - dokonceni pohybu davkovace do 2
SEK_16: CALL  INV_G
	JNZ   SEK_15R
	JC    SEK_12E		; Chyba, ventil se neotocil
	CALL  LAN_MRK
	MOV   DPTR,#P_INJT	; Doba otoceni ventilu
	MOVX  A,@DPTR
	MOV   B,#15
	MUL   AB
	MOV   R0,A
	MOV   R1,B
	INC   DPTR
	MOVX  A,@DPTR
	MOV   B,#15
	MUL   AB
	ADD   A,R1
	XCH   A,R0
	MOV   DPTR,#SEK_TIM
	MOVX  @DPTR,A
	INC   DPTR
	XCH   A,R0
	MOVX  @DPTR,A
	JMP   SEK_NST

; Stav 17 - cekani po urcitou dobu pak davkovac do 1
SEK_17:	CALL  SEK_WFT
	JNZ   SEK_15R
	CALL  INV_GP1		; Otocit do 1
%IF(1)THEN(
	MOV   A,#50H		; pridrzet ramenko
	MOV   DPTR,#ASH1_GST
	MOVX  @DPTR,A		; !!!!!!!!!!!!!!!!!
)FI
	JMP   SEK_NST

; Stav 18 - davkovaci kohout do 1 pak vyplach smycky
SEK_18: JB    MR_FLG.BMR_ERR,SEK_22E
	CALL  INV_G
	JNZ   SEK_15R
	JC    SEK_22E		; Chyba, ventil se neotocil
	JB    MR_FLG.BMR_BSY,SEK_15R ; Hybe se ramenko
	CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_HI)
	%LDR45i(614)		; Vyplach smycky
	CALL  ADD_VOL1		; Pridani objemu P_VOL1
	CALL  ADD_VOL1		; Pridani objemu P_VOL1
	CALL  GO_PER
	JB    F0,SEK_22E
	JMP   SEK_NST

; Stav 19 - vyplach smycky - pak konec nebo kalibrace
SEK_19:	JB    MR_FLG.BMR_ERR,SEK_22E
	JB    MR_FLG.BMR_BSY,SEK_23R
	MOV   DPTR,#SEK_CFG
	MOVX  A,@DPTR
	JB    ACC.BSEK_REV,SEK_20N
	JMP   SEK_END

; Stav 20 - rezerva
SEK_20:
SEK_20N:JMP   SEK_NST

; Stav 21 - pocatek kalibrace - rezerva
SEK_21:	JB    MR_FLG.BMR_ERR,SEK_22E
	CALL  INV_G
	JNZ   SEK_21R
	JNB   MR_FLG.BMR_BSY,SEK_212
SEK_21R:RET
SEK_212:MOV   DPTR,#ASH1_GST       ; Kontrola ramenka
	MOV   A,#50H
	MOVX  @DPTR,A
	JMP   SEK_NST

; Stav 22 - pocatek vyplachy peristaltiky
SEK_22:	JB    MR_FLG.BMR_ERR,SEK_22E
	JB    MR_FLG.BMR_BSY,SEK_21R
	CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_HI)
	%LDR45i(1200)		; Vyplach peristaltiky
	CALL  ADD_VOL1		; Pridani objemu P_VOL1
	CALL  GO_PER
	JB    F0,SEK_22E
	JMP   SEK_NST
SEK_22E:JMP   SEK_E

; Stav 23 - peristaltika vyplachuje pak priprava kalibrace cidel
SEK_23: JB    MR_FLG.BMR_ERR,SEK_22E
	JNB   MR_FLG.BMR_BSY,SEK_232
SEK_23R:RET
SEK_232:MOV   DPTR,#SEK_CFG
	MOVX  A,@DPTR
	JB    ACC.BSEK_LPS,SEK_235
	CALL  SEK_NST		; preskocit jeden stav
	SJMP  SEK_242
SEK_235:CALL  LPS_CS		; pocatek kalibrace cidel
	JMP   SEK_NST

; Stav 24 - kalibrace cidel na vodu pak suseni
SEK_24: CALL  LPS_CW
	JNZ   SEK_23R
SEK_242:MOV   DPTR,#SEK_CFG
	MOVX  A,@DPTR
	JNB   ACC.BSEK_NOV,SEK_245
	; Susici ventil ON
	JMP   SEK_NST
SEK_245:CLR   F0		; Neni ventil
	MOV   R1,#1
	%LDR23i(PER_SPD_HI)
	%LDR45i(-614)		; Objem pro vysusebi trubicky
	CALL  SUB_VOL1		; Pridani objemu P_VOL1
	CALL  GO_PER
	JB    F0,SEK_22E
	CALL  SEK_NST
	JMP   SEK_NST

; Stav 25 - doba sepnuti susiciho
SEK_25:
	; Susici ventil OFF
	CALL  SEK_NST
	SJMP  SEK_265

; Stav 26 - suseni peristaltikou pak kalibrace vzduchu
SEK_26: JB    MR_FLG.BMR_ERR,SEK_27E
	JNB   MR_FLG.BMR_BSY,SEK_265
SEK_26R:RET
SEK_265:MOV   DPTR,#SEK_CFG
	MOVX  A,@DPTR
	JNB   ACC.BSEK_LPS,SEK_268
	JMP   SEK_NST
SEK_268:CALL  SEK_NST		; cidla nejsou
	SJMP  SEK_275

; Stav 27 - kalibrace vzduchu, pak naplneni vodou
SEK_27:	CALL  LPS_CA
	JNZ   SEK_26R
	CLR   FL_ENLPS
	JB    F0,SEK_27E
	SETB  FL_ENLPS		; Nakalibrovana cidla
SEK_275:MOV   DPTR,#SEK_CFG
	MOVX  A,@DPTR
	JB    ACC.BSEK_REV,SEK_END
	CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_HI)
	%LDR45i(1200)		; Vyplach peristaltiky
	CALL  ADD_VOL1		; Pridani objemu P_VOL1
	CALL  GO_PER
	JB    F0,SEK_27E
	CLR   FL_LPS1
	CLR   FL_LPS2
	JMP   SEK_NST
SEK_27E:JMP   SEK_E

; Stav 28 - dokonceni plneni vodou
SEK_28: JB    MR_FLG.BMR_ERR,SEK_27E
	JNB   MR_FLG.BMR_BSY,SEK_27E
	CALL  LPS_TST
	JNB   FL_LPS1,SEK_26R
	JNB   FL_LPS2,SEK_26R
SEK_END:MOV   R1,#1		; ukoncit rizeni peristaltiky
	CALL  CLR_GEP
	CLR   A
	MOV   DPTR,#ASH1_GST
	MOVX  @DPTR,A
	MOV   DPTR,#SEK_ST
	MOVX  @DPTR,A
	SETB  GS_BUF.BGS_RDY	; signal pro cekajici proces
	RET

; Dokonceni pripraveneho davkovani
TM_INJ:	JNB   FL_WINJ,TM_INJ9
	MOV   DPTR,#SEK_ST
	MOVX  A,@DPTR
	CJNE  A,#SEK_ST_WINJ,TM_INJE
	SETB  FL_DINJ		; Dokonci davkovani
TM_INJ9:RET
TM_INJE:JMP   SEK_E

; Manualni spusteni davkovani
TM_LOAD:JB    FL_WINJ,TM_INJ
	JMP   TM_SEK

; Davkovani s cekanim na TM_INJ
TM_PREP:CLR   FL_DINJ
	SETB  FL_WINJ
	SJMP  TM_SEK1

; Davkovani bez cekanim TM_INJ
TM_SEK:	CLR   FL_WINJ
	CLR   FL_DINJ
TM_SEK1:MOV   DPTR,#SEK_CFG
	MOVX  A,@DPTR
	JNB   ACC.BSEK_LPS,TM_SEK2
	JNB   FL_ENLPS,TM_SEKE
TM_SEK2:MOV   DPTR,#SEK_ST
	MOVX  A,@DPTR
	JZ    TM_SEK3
	CJNE  A,#SEK_ST_FCLN,TM_SEKE
	CLR   F0
	MOV   R1,#1             ; Zastavit peristaltiku
	CALL  STP_GEP
TM_SEK3:MOV   DPTR,#SEK_ST
	MOV   A,#1
	MOVX  @DPTR,A
	CLR   SEK_ERR
	RET
TM_SEKE:JMP   SEK_E

; Rekonfigurace a inicializace vcetne kalibrace cidel
; R45 nova konfigurace
TM_INICFG:
	MOV   A,R4
	JNZ   TM_ICF1
	MOV   A,#MSEK_LPS	; Musi se pouzivat cidla
TM_ICF1:MOV   DPTR,#SEK_CFG
	MOVX  @DPTR,A
	CALL  SEK_INI
	CALL  TM_LPS
	RET

; Pouze zastavani a navrat ramenka
SEK_INI:CLR   A
	CLR   FL_WINJ
	CLR   FL_DINJ
	MOV   DPTR,#SEK_ST
	MOVX  @DPTR,A
	CLR   SEK_ERR
	MOV   R1,#0		; Ramenko
	MOV   A,#OMR_FLG
	CALL  MR_GPA1
	MOV   A,#MMR_ENI
	MOVX  @DPTR,A
	MOV   A,#OMR_ERC
	CALL  MR_GPA1
	CLR   A
	MOVX  @DPTR,A
	MOV   R1,#1		; Peristaltika
	MOV   A,#OMR_FLG
	CALL  MR_GPA1
	MOV   A,#MMR_ENI
	MOVX  @DPTR,A
	CALL  CLR_GEP
	MOV   MR_FLG,#0		; nulovat chyby
	CALL  STP_OFF		; vypnout krokac
	CALL  TM_ASH_HOME
	CALL  INV_GP1		; ventil do polohy 1
	RET

G_SEK_ST:
	JNB   STP_ERR,G_SEKS1
	MOV   R5,#0FCH		; Chyba krokoveho motoru
	MOV   DPTR,#STP_ST
	MOVX  A,@DPTR
	MOV   R4,A
	SJMP  G_SEKS9
G_SEKS1:JNB   MR_FLG.BMR_ERR,G_SEKS3
	MOV   R4,#090H		; Chyba stejnosmerneho motoru
	MOV   R5,#0FEH
	MOV   R2,#REG_N
	MOV   DPTR,#REG_A+OREG_A+OMR_FLG
G_SEKS2:MOVX  A,@DPTR
	JB    ACC.BMR_ERR,G_SEKS9
	MOV   A,#OMR_LEN
	CALL  ADDATDP
	INC   R4
	DJNZ  R2,G_SEKS2
	MOV   R4,#080H
	SJMP  G_SEKS9
G_SEKS3:JNB   SEK_ERR,G_SEKS5
	MOV   R5,#0FDH		; Chyba pri cinnosti sekvenceru
	SJMP  G_SEKS7
G_SEKS5:
G_SEKS6:MOV   R5,#0
G_SEKS7:MOV   DPTR,#SEK_ST
	MOVX  A,@DPTR
	CJNE  A,#SEK_ST_WINJ,G_SEKS81
	MOV   A,#81H
G_SEKS81:CJNE A,#SEK_ST_LPEC,G_SEKS82
	MOV   A,#88H
G_SEKS82:CJNE A,#SEK_ST_LPLA,G_SEKS83
	MOV   A,#89H
G_SEKS83:CJNE A,#SEK_ST_LPLB,G_SEKS84
	MOV   A,#8AH
G_SEKS84:CJNE A,#SEK_ST_FCLN,G_SEKS85
	MOV   A,#90H
G_SEKS85:
G_SEKS88:MOV  R4,A
G_SEKS9:MOV   DPTR,#STATUS
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	CLR   F0
	MOV   A,#1
	RET

; *******************************************************************
; Rizeni teploty

EXTRN	CODE(MR_PIDP)

TMC_OUT	BIT   P4.5
TC_ACL	XDATA 0FFE3H
TC_MCL	EQU   10H
TC_ADC	XDATA ADC0
TC_LIMT EQU  50*100	; Limit pro chybu chlazeni po prepoctu

RSEG	AS____X

TMC1:	DS    OMR_LEN
TMC1_FLG XDATA TMC1+OMR_FLG
TMC1_AT	XDATA TMC1+OMR_AP
TMC1_AG	XDATA TMC1+OMR_AS
TMC1_RT	XDATA TMC1+OMR_RPI
TMC1_EN	XDATA TMC1+OMR_ENE
TMC1_FL:DS    2
TMC1_RD:DS    2

TMC1_OC:DS    2
TMC1_MC:DS    2

TMC_PWC:DS    1

RSEG	AS____C

TMC_REG:MOV   DPTR,#TMC1_FLG
	MOVX  A,@DPTR
	JNB   ACC.BMR_ENI,TMC_39	; mereni vypnuto
	MOV   R7,A
; Mereni
	MOV   DPTR,#TC_ADC
	MOVX  A,@DPTR
	INC   DPTR
TMC_R15:RL    A
	RL    A
	ANL   A,#3
	MOV   R4,A
	MOVX  A,@DPTR
	RL    A
	RL    A
	MOV   R5,A
	ANL   A,#3
	XCH   A,R5
	ANL   A,#NOT 3
	ORL   A,R4
	MOV   R4,A
	MOV   DPTR,#TMC1_FL
	MOVX  A,@DPTR
	ADD   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOVX  @DPTR,A
; Generovani PWM
	MOV   DPTR,#TMC_PWC
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	ANL   A,#1FH
	MOV   R4,A
	MOV   A,R7
	JNB   ACC.BMR_ENR,TMC_38	; energie vypnuta
	JB    ACC.BMR_ERR,TMC_32
	MOV   DPTR,#TMC1_EN+1
	MOVX  A,@DPTR
%IF(1) THEN (				; pro chlazeni
	CPL   A
	INC   A
)FI
	JNB   ACC.7,TMC_33
	SETB  TMC_OUT
	ADD   A,#10H		; Posun vetraku
	JC    TMC_34
	MOV   A,#TC_MCL
	SJMP  TMC_35
TMC_32:	SETB  TMC_OUT		; Error
	MOV   A,#TC_MCL
	SJMP  TMC_35
TMC_33: CLR   C
	RRC   A
	CLR   C
	RRC   A
	ADDC  A,#0
	SETB  C
	SUBB  A,R4
	MOV   TMC_OUT,C
TMC_34:
%IF(1) THEN(
	MOV   A,#TC_MCL
)ELSE(
	CLR   A
)FI
TMC_35:	MOV   DPTR,#TC_ACL	; Spinani vetraku
	MOVX  @DPTR,A
TMC_38:	MOV   DPTR,#TMC_PWC
	MOVX  A,@DPTR
	ANL   A,#1FH
	JZ    TMC_R50
TMC_39:	RET
; Nacteni filtru a prepocet
TMC_R50:CLR   F0
	MOV   DPTR,#TMC1_FL
	MOVX  A,@DPTR
	MOV   R4,A
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A		; R45 = TMC1_FL/2
	CLR   A
	MOVX  @DPTR,A
	MOV   DPTR,#TMC1_RD	; Ulozeni raw data
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
%IF(1) THEN(
	JZ    TMC_R51		; Kontrola chyby prevodniku
	CJNE  A,#07FH,TMC_R52
TMC_R51:SETB  F0
)FI
TMC_R52:MOV   DPTR,#TMC1_MC	; R45=R45*TMC1_MC/2^16
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	CALL  MULsi
	MOV   A,R6
	MOV   R4,A
	MOV   A,R7
	MOV   R5,A
	MOV   DPTR,#TMC1_OC	; R45=R45+TMC1_OC
	MOVX  A,@DPTR
	ADD   A,R4
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOV   R5,A
	JNB   OV,TMC_R53
	%LDR45i(7FFFH)
	JB    ACC.7,TMC_R53
	%LDR45i(-7FFFH)
TMC_R53:
%IF(1) THEN(
	JB    F0,TMC_R54
	MOV   A,R4		; Kontrola prehrati
	ADD   A,#LOW  (-TC_LIMT)
	MOV   A,R5
	ADDC  A,#HIGH (-TC_LIMT)
	MOV   C,OV
	XRL   A,PSW
	JB    ACC.7,TMC_R55
TMC_R54:SETB  F0
	MOV   DPTR,#TMC1_FLG
	MOVX  A,@DPTR
	JNB   ACC.BMR_ENR,TMC_R55
	ORL   A,#MMR_ERR
	MOVX  @DPTR,A
)FI
; Rozdil
TMC_R55:CLR   C
	MOV   DPTR,#TMC1_AT	; TMC1_AT=R45
	MOVX  A,@DPTR           ; R45-=old TMC1_AT
	XCH   A,R4
	MOVX  @DPTR,A
	SUBB  A,R4
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R5
	MOVX  @DPTR,A
	MOV   R3,#0
	JNB   ACC.7,TMC_R56	; znamenkove rozsireni
	DEC   R3
TMC_R56:SUBB  A,R5
	MOV   R5,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	MOV   DPTR,#TMC1_AG
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
; Vlastni regulator
TMC_R60:MOV   DPTR,#TMC1+OMR_ERC
	CLR   A
	MOVX  @DPTR,A
	MOV   DPTR,#TMC1
	CALL  MR_PIDP
	MOV   DPTR,#TMC1_EN
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	RET

TMC_INI:MOV   DPTR,#TMC1_FLG
	MOV   A,#80H
	MOVX  @DPTR,A
	MOV   DPTR,#TC_ACL
	CLR   A
	MOVX  @DPTR,A
	%LDR45i (TMC1)
	%LDR23i (TMC1PPR)
	%LDR01i (OMR_LEN)
	CALL  cxMOVE
	MOV   DPTR,#TMC1_FL
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	%LDMXi (TMC1_OC,5157)	; T2
	%LDMXi (TMC1_MC,-15527)	; ((T2-T1)*100*10000h)/7FE0h
; Vypne topeni a vetrak kolony
TMC_OFF:MOV   DPTR,#TMC1_FLG
	MOV   A,#MMR_ENI
	MOVX  @DPTR,A
	SETB  TMC_OUT		; Vypnout topeni
	CLR   A
	MOV   DPTR,#TC_ACL	; Spinani vetraku
	MOVX  @DPTR,A
	MOV   A,#1
	RET

; Zapne regulaci teploty kolony
TMC_ON:	MOV   DPTR,#TMC1_FLG
	MOV   A,#MMR_ENI OR MMR_ENR
	MOVX  @DPTR,A
	RET

TMC_ONOFF:
	MOV   DPTR,#TMC1_FLG
	MOVX  A,@DPTR
	JB    ACC.BMR_ENR,TMC_OFF
	SJMP  TMC_ON

G_TMC_ST:MOV  DPTR,#TMC1_FLG
	MOVX  A,@DPTR
	JNB   ACC.BMR_ERR,TMC_ST1
	%LDR45i(0FFF1H)
	RET
TMC_ST1:MOV   R4,#0
	JNB   ACC.BMR_ENR,TMC_ST3
	MOV   A,#OMR_ENE+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   A,#OMR_ENE
	MOVC  A,@A+DPTR
	ORL   A,R5
	MOV   R4,#1
	JZ    TMC_ST3
	MOV   A,R5
	MOV   R4,#2
	JNB   ACC.7,TMC_ST3
	MOV   R4,#3
TMC_ST3:MOV   R5,#0
	RET

TMC1PPR:DB    0, 0,0,0, 0,0
	DB    0
	DB    0,0   			; RP
	DB    0
	DB    0,0,0			; RS
	DB    018H,0, 002H,0, 20H,0	; P I D
	DB    000H,0, 000H,0, 0,07FH	; 1 2 ME
	DB    080H,0, 010H,0		; MS MA
	DB    0,0,0,0,0,0,0

UR_TEMP:MOV   A,#OU_DP
	CALL  ADDATDP
G_TEMP:	%LDR23i(10)
	CALL  xMDPDP
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   EA,C
	JB    ACC.7,G_TEMP1
	CALL  DIVi	; R45=R45/R23
	MOV   A,R4
	ADDC  A,#0
	MOV   R4,A
	MOV   A,R5
	ADDC  A,#0
	MOV   R5,A
	ORL   A,#1
	RET
G_TEMP1:CALL  NEGi
	CALL  DIVi	; R45=R45/R23
	CLR   A
	SUBB  A,R4
	MOV   R4,A
	CLR   A
	SUBB  A,R5
	MOV   R5,A
	ORL   A,#1
	RET

UW_TEMP:MOV   A,#OU_DPSI
	CALL  ADDATDP
S_TEMP:	%LDR23i(10)
	CALL  MULsi
	CALL  xMDPDP
	MOV   C,EA
	CLR   EA
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   EA,C
	JMP   TMC_ON

; *******************************************************************
; Prace s pameti EEPROM 8582

EEP_ADR	EQU   0A0H		; Adresa EEPROM na IIC sbernici
C_S1CON EQU   11000000B ; 11000001B ; Pocatecni stav S1CON
			; 11000000B ; S1CON s delenim 960
			; 01000000B ; S1CON pro X 24.000 MHz

EEA_RD	EQU   1		; akce cteni
EEA_WR	EQU   2		; akce zapisu

RSEG	AS____D

RSEG	AS____X

EE_PTR:	DS    2		; ukazatel do MEM_BUF na data
EEP_TAB:DS    2		; popis dat ulozenych v bloku EEPROM

MEM_BUF:DS    100H	; Buffer pameti EEPROM

RSEG	AS____C

EE_WAIT:JNB   SI,$
	MOV   A,S1STA
	RET

EE_WRA:	MOV   S1CON,#C_S1CON	; OR 20H
	SETB  STO
	CLR   SI
	SETB  STA		; START
	CALL  EE_WAIT
	CLR   STA
	CJNE  A,#008H,EE_WRA
	MOV   A,R5
	MOV   S1DAT,A		; SLA and W
	CLR   SI
	CALL  EE_WAIT
	XRL   A,#018H
	JNZ   EE_WRA9
	MOV   A,R4
	MOV   S1DAT,A		; Adresa v EEPROM
	CLR   SI
	CALL  EE_WAIT
	XRL   A,#028H
EE_WRA9:RET

; Cteni pameti EEPROM
;	DPTR .. pointer na buffer
;	R2   .. pocet ctenych byte
;	R4   .. adresa, od ktere se cte

EE_RD:	MOV   R5,#EEP_ADR
	MOV   R1,#0
EE_RD10:CLR   ES1
	CALL  EE_WRA
	JNZ   EE_ERR
	SETB  STA		; repeated START
	CLR   SI
	CALL  EE_WAIT
	CLR   STA
	CJNE  A,#010H,EE_ERR
	MOV   A,R5
	ORL   A,#1
	MOV   S1DAT,A		; SLA and R
	CLR   SI
	CALL  EE_WAIT
	CJNE  A,#040H,EE_ERR
	SETB  AA
EE_RD30:CLR   SI
	CALL  EE_WAIT
	CJNE  A,#050H,EE_ERR
	MOV   A,S1DAT		; DATA
	MOVX  @DPTR,A
	XRL   A,R1
	INC   A
	MOV   R1,A
	INC   DPTR
	DJNZ  R2,EE_RD30
	CLR   AA
	CLR   SI
	CALL  EE_WAIT
	CJNE  A,#058H,EE_ERR
	MOV   A,S1DAT		; XORSUM
	XRL   A,R1
	SETB  STO		; STOP
	CLR   SI
	RET

EE_ERR:	SETB  STO
	CLR   SI
	SETB  F0
	ORL   A,#07H
	RET

; Zapis do pameti EEPROM
;	DPTR .. pointer na buffer
;	R2   .. pocet zapisovanych byte
;	R4   .. adresa, od ktere se zapisuje

EE_WR:	MOV   R5,#EEP_ADR
	MOV   R1,#0
EE_WR10:CLR   ES1
EE_WR20:MOV   R0,#0
EE_WR21:NOP
	DJNZ  R0,EE_WR21
	MOV   R0,#100
	MOV   A,R2
	JZ    EE_WR30
EE_WR22:CALL  EE_WRA
	JZ    EE_WR23
	DJNZ  R0,EE_WR22
	SJMP  EE_ERR
EE_WR23:MOV   R0,#4
EE_WR24:MOVX  A,@DPTR
	MOV   S1DAT,A
	CLR   SI
	XRL   A,R1
	INC   A
	MOV   R1,A
	INC   DPTR
	DEC   R2
	INC   R4
	CALL  EE_WAIT
	CJNE  A,#028H,EE_ERR
	MOV   A,R2
	JZ    EE_WR26
	DJNZ  R0,EE_WR24
EE_WR26:SETB  STO
	CLR   SI
	SJMP  EE_WR20
EE_WR30:CALL  EE_WRA
	JZ    EE_WR34
	DJNZ  R0,EE_WR30
	SJMP  EE_ERR
EE_WR34:MOV   A,R1
	MOV   S1DAT,A
	CLR   SI
	CALL  EE_WAIT
	CJNE  A,#028H,EE_ERR
	SETB  STO
	CLR   SI
	RET

; R45 := [EE_PTR++]
EEA_RDi:MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOV   R0,DPH
	MOV   A,DPL
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	RET

; [EE_PTR++] := R45
EEA_WRi:MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   R0,DPH
	MOV   A,DPL
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	RET

; provadi EEA_RD, EEA_WR pro iteger cislo
EEA_Mi:	CJNE  R0,#EEA_RD,EEA_Mi5
	CALL  EEA_RDi
	MOV   DPL,R2
	MOV   DPH,R3
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	RET
EEA_Mi5:CJNE  R0,#EEA_WR,EEA_Mi9
	MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  EEA_WRi
EEA_Mi9:RET

EEA_PRO:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#EE_PTR
	MOV   A,#LOW MEM_BUF
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH MEM_BUF
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
EEA_PR2:MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	ORL   A,R4
	JZ    EEA_Mi9
	PUSH  DPL
	PUSH  DPH
	MOV   A,R0
	PUSH  ACC
	CALL  JMPR45
	POP   ACC
	MOV   R0,A
	POP   DPH
	POP   DPL
	JMP   EEA_PR2

JMPR45:	MOV   A,R4
	PUSH  ACC
	MOV   A,R5
	PUSH  ACC
	RET

; Nastavi EEP_TAB a DPTR na R23
EEP_PTS:MOV   DPTR,#EEP_TAB
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	MOV   DPL,R2
	MOV   DPH,R3
	RET

; Ulozit data podle tabulky R23
EEP_WRS:CALL  EEP_PTS
	INC   DPTR
	INC   DPTR	; popis akci
	MOV   R0,#EEA_WR
	CALL  EEA_PRO
	MOV   DPTR,#EEP_TAB
	CALL  xMDPDP
	MOVX  A,@DPTR
	MOV   R2,A	; pocet prenasenych byte
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A	; pocatecni adresa v EEPROM
	MOV   DPTR,#MEM_BUF
	JMP   EE_WR

; Nacist data podle tabulky R23
EEP_RDS:CALL  EEP_PTS
	MOVX  A,@DPTR
	MOV   R2,A	; pocet prenasenych byte
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A	; pocatecni adresa v EEPROM
	MOV   DPTR,#MEM_BUF
	CALL  EE_RD
	JNZ   EEP_RD9
	MOV   DPTR,#EEP_TAB
	CALL  xMDPDP
	INC   DPTR
	INC   DPTR	; popis akci
	MOV   R0,#EEA_RD
	JMP   EEA_PRO
EEP_RD9:RET


; *******************************************************************
; Ukladani a cteni nastaveni z EEPROM

; Akce TMC ON/OFF
; R2 cislo regulatoru
EEA_TMC:CJNE  R0,#EEA_RD,EEA_TMC5
	CALL  EEA_RDi
	MOV   A,R2
	MOV   R1,A
	MOV   A,R4
	JZ    EEA_TMC2
	JMP   TMC_ON
EEA_TMC2:JMP  TMC_OFF
EEA_TMC5:CJNE R0,#EEA_WR,EEA_TMC9
	MOV   A,R2
	MOV   R1,A
	CALL  G_TMC_ST
	CALL  EEA_WRi
EEA_TMC9:RET

; Tabulky popisu akci pro ulozeni a cteni dat

; Tabulka pro SETTINGS
EEC_SET:DB    060H	; pocet byte ukladanych dat
	DB    040H	; pocatecni adresa v EEPROM

	%W    (CC_FRST)	; R23 info pro akci
	%W    (EEA_Mi)	; akce

	%W    (CC_LAST)
	%W    (EEA_Mi)

	%W    (CC_REP)
	%W    (EEA_Mi)

	%W    (CC_TIME)
	%W    (EEA_Mi)

	%W    (P_INJT)
	%W    (EEA_Mi)

	%W    (P_VOL1)
	%W    (EEA_Mi)

	%W    (CA_OFF)
	%W    (EEA_Mi)

	%W    (CA_NEXT)
	%W    (EEA_Mi)

	%W    (CA_RDY)
	%W    (EEA_Mi)

	%W    (CA_INJ)
	%W    (EEA_Mi)

	%W    (CA_WAIT)
	%W    (EEA_Mi)

	%W    (CA_END)
	%W    (EEA_Mi)

	%W    (TMC1_RT)
	%W    (EEA_Mi)

	%W    (1)
	%W    (EEA_TMC)

	%W    (COM_TYP)
	%W    (EEA_Mi)

	%W    (COM_ADR)
	%W    (EEA_Mi)

	%W    (COM_SPD)
	%W    (EEA_Mi)

	%W    (INV_AUX)
	%W    (EEA_Mi)

	%W    (CU_PROT)
	%W    (EEA_Mi)

	%W    (CU_DADR)
	%W    (EEA_Mi)

	%W    (0)
	%W    (0)

; Tabulka pro SERVICE
EEC_SER:DB    020H	; pocet byte ukladanych dat
	DB    010H	; pocatecni adresa v EEPROM

	%W    (TMC1_OC)
	%W    (EEA_Mi)

	%W    (TMC1_MC)
	%W    (EEA_Mi)

	%W    (ASH1_BOT1)
	%W    (EEA_Mi)

	%W    (LPS_TD1)
	%W    (EEA_Mi)

	%W    (LPS_TD2)
	%W    (EEA_Mi)

	%W    (P_IRC_W)
	%W    (EEA_Mi)

	%W    (ASH1_BOT2)
	%W    (EEA_Mi)

    %IF(%WITH_WHEEL80)THEN(
	%W    (ASH_WASH)
	%W    (EEA_Mi)

	%W    (ASH_RP1)
	%W    (EEA_Mi)

	%W    (ASH_RP2)
	%W    (EEA_Mi)

	%W    (ASH_RP3)
	%W    (EEA_Mi)
    ) FI

	%W    (0)
	%W    (0)

; *******************************************************************

RSEG	AS____C

DB_W_10:MOV   R0,#10H
	SJMP  DB_WAI1

DB_WAIT:MOV   R0,#0H
DB_WAI1:%WATCHDOG
	DJNZ  R1,DB_WAI1
	DJNZ  R0,DB_WAI1
	RET

DEVER_T:DB    LCD_CLR,'%VERSION'
	DB    C_LIN2 ,' (c) PiKRON 2012',0

L0:	MOV   SP,#STACK
	SETB  EA
	JNB   FL_DIPR,L007
	MOV   DPTR,#DEVER_T
	CALL  cPRINT
	CALL  DB_WAIT

	MOV   A,#LCD_CLR
	CALL  LCDWCOM
L007:
	SETB  %STBY_FL
	CALL  AUX_INIT
	MOV   R4,#0	; Inicializace hardware
	CALL  S_AUX
	CALL  CF_INIT
	CALL  MR_INIS
	CALL  SEK_INI
	CALL  TMC_INI

	%LDMXi(COM_TYP,1)	; Nastaveni parametru komunikace
	%LDMXi(COM_ADR,7)	; Adresa 7
	%LDMXi(COM_SPD,4)	; Rychlost 19200

	%LDR23i(EEC_SER); Cteni parametru z EEPROM
	CALL  EEP_RDS
	%LDR23i(EEC_SET)
	CALL  EEP_RDS

	MOV   DPTR,#CA_OFF	; Pocatecni nastaveni AUX
	CALL  SQ_SAUX

    %IF(%WITH_UL_DY_EEP) THEN (
	CALL  INI_SERNUM	; Initialize serial number
    )FI
	CALL  COM_INI		; Spusteni komunikace

	JB    FL_DIPR,L090
L080:	CALL  UI_PR	; Bez displeje
    %IF(%WITH_UL_DY) THEN (
	CALL  UD_RQ		; dynamicaka adresace
    )FI
	JMP   L080

L090:
%IF (NOT %DEBUG_FL) THEN (
	JMP   UT
)ELSE(
	MOV   A,#K_HELP
	CALL  TESTKEY
	JZ    L1
	JMP   UT

L1:
	MOV   SP,#STACK
	MOV   A,#LCD_HOM
	CALL  LCDWCOM

	MOV   DPTR,#REG_A+OREG_A+OMR_AP
	CALL  xPR_AD

	MOV   DPTR,#REG_A+OREG_B+OMR_AP
	CALL  xPR_AD

	MOV   A,#LCD_HOM+40H
	CALL  LCDWCOM

	MOV   DPTR,#SEK_ST		; Sekvencer
	MOVX  A,@DPTR
	CALL  PRINThb

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#STP_ST		; Krokac
	MOVX  A,@DPTR
	CALL  PRINThb

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#STP_SPD
	MOVX  A,@DPTR
	CALL  PRINThb

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#STP_AP
	MOVX  A,@DPTR
	CALL  PRINThb

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#STP_TMP
	MOVX  A,@DPTR
	CALL  PRINThb

	MOV   A,#'>'
	CALL  LCDWR

	MOV   DPTR,#STP_WT
	MOVX  A,@DPTR
	CALL  PRINThb

	MOV   A,#' '
	CALL  LCDWR

	MOV   A,P5
	CALL  PRINThb

	MOV   A,#' '
	CALL  LCDWR

	MOV   A,#LCD_HOM+14H
	CALL  LCDWCOM

	MOV   DPTR,#ADC0		; Prevodniky
	CALL  xLDR45i
	CALL  PRINThw

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#ADC6
	CALL  xLDR45i
	CALL  PRINThw

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#ADC7
	CALL  xLDR45i
	CALL  PRINThw

	MOV   A,#' '
	CALL  LCDWR

	MOV   A,#LCD_HOM+54H
	CALL  LCDWCOM

	MOV   DPTR,#ASH1_GST	; Ramenko
	MOVX  A,@DPTR
	CALL  PRINThb

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#REG_A+OREG_A+OMR_FOD
	CALL  xLDR45i
	CALL  PRINThw

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#REG_A+OREG_A+OMR_FOI
	CALL  xLDR45i
	CALL  PRINThw

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#REG_A+OREG_A+OMR_RP
	CALL  xLDR45i
	CALL  PRINThw

L2:	CALL  SCANKEY
	JZ    L3
	MOV   R7,A
	MOV   DPTR,#SFT_D1
	CALL  SEL_FNC
L3:
	JNB   FL_ECUL,L4
	MOV   R0,#40H	; Echo uLANU pro kontrolu komunikace
	CALL  uL_FNC
L4:

	JMP   L1

)FI
%IF (%DEBUG_FL) THEN (

T_RDVAL:MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR
	PUSH  ACC
	INC   DPTR
	MOVX  A,@DPTR
	PUSH  ACC
	INC   DPTR
	CALL  cPRINT
	MOV   R6,#8
	MOV   R7,#40H
	CALL  INPUTi
	POP   DPH
	POP   DPL
	JB    F0,T_RDV99
	CALL  xSVR45i
T_RDV99:RET


T_DISP:	MOV   A,#LCD_CLR
	CALL  LCDWCOM
T_DISP0:MOV   R6,#8
	MOV   R7,#40H
	CALL  INPUThw
	MOV   A,R4
	JZ    T_DISP9
	CALL  LCDWCOM
	MOV   A,R5
	MOV   R1,#'A'
	MOV   R0,#5
T_DISP1:MOV   A,R1
	CALL  LCDWR
	MOV   A,#0F0H
	CALL  LCDWR
	MOV   R2,#14
T_DISP2:MOV   A,#' '
	CALL  LCDWR
	DJNZ  R2,T_DISP2
	INC   R1
	DJNZ  R0,T_DISP1
	SJMP  T_DISP0
T_DISP9:RET

T_COFF:	SETB  TMC_OUT
	RET

T_CON:	CLR   TMC_OUT
	RET

%IF (%WITH_RS232) THEN (
T_RS232:MOV   R7,#10
	CALL  RS232_INI
	JMP   IHEXLD
)FI

SFT_D1:	DB    K_END
	%W    (0)
	%W    (L0)

	DB    K_RUN
	%W    (0)
	%W    (TM_SEK)

	DB    K_SAMPL
	%W    (0)
	%W    (TM_STP)

	DB    K_LOAD
	%W    (0)
	%W    (TM_GEP)

	DB    K_H_A
	%W    (T_PWM0)
	%W    (T_RDVAL)

	DB    K_H_B
	%W    (T_PWM1)
	%W    (T_RDVAL)

	DB    K_0
	%W    (0)
	%W    (TM_STPHLD)

	DB    K_1
	%W    (0)
	%W    (T_DISP)

	DB    K_3
	%W    (10H)
	%W    (TM_ASH)

	DB    K_4
	%W    (20H)
	%W    (TM_ASH)

	DB    K_5
	%W    (30H)
	%W    (TM_ASH)

	DB    K_6
	%W    (0)
	%W    (UT)

	DB    K_7
	%W    (0)
	%W    (T_CON)

	DB    K_8
	%W    (0)
	%W    (T_COFF)

    %IF (%WITH_RS232) THEN (
	DB    K_9
	%W    (0)
	%W    (T_RS232)
    )FI

	DB    K_DP
	%W    (0)
	%W    (MONITOR)

	DB    K_UP
	%W    (0)
	%W    (TM_SUP)

	DB    K_DOWN
	%W    (0)
	%W    (TM_SDO)

	DB    0

T_PWM0:	%W    (0FFE0H)
	DB    LCD_CLR,'PWM0 :',0

T_PWM1:	%W    (0FFE1H)
	DB    LCD_CLR,'PWM1 :',0

xPR_AD: MOV   A,#' '
	CALL  LCDWR
	MOV   R1,#3
	SJMP  xPRThl1

xPRThl:	MOV   R1,#4
xPRThl1:MOV   A,R1
	DEC   A
	MOVC  A,@A+DPTR
	CALL  PRINThb
	DJNZ  R1,xPRThl1
	RET

)FI

; *******************************************************************
; Pomocne vstupy vystupy

%IF(%OLD_AUX_IN1)THEN(
AUX_IN_BIT1 BIT  P1.1 
)ELSE(
AUX_IN_BIT1 BIT  P1.5 
)FI
%IF(%WITH_WHEEL80)THEN(
AUX_IN_BIT2 BIT  P4.7 
)ELSE(
AUX_IN_BIT2 BIT  P4.6 
)FI


AUX_POR	XDATA AIR_CC1
AUX_BUF XDATA AIR_CC1_BUF

AUX_IN_MSK EQU 030H

RSEG	AS____X

AUX_INO:DS    1

RSEG	AS____C

; Nastaveni AUX podle R4
; ======================
S_AUX:  MOV   A,R4
	ANL   A,#0FH
	SWAP  A
	MOV   R4,A
	MOV   DPTR,#AUX_BUF
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	ANL   A,#0FH
	ORL   A,R4
	MOVX  @DPTR,A
	MOV   DPTR,#AUX_POR
	MOV   EA,C
	MOVX  @DPTR,A
	RET

; Nacte AUX do R4
; ======================
G_AUX:  MOV   DPTR,#AUX_BUF
	MOVX  A,@DPTR
	SWAP  A
	ANL   A,#0FH
G_AUX1:
	MOV   C,AUX_IN_BIT1
	MOV   ACC.4,C
	MOV   C,AUX_IN_BIT2
	MOV   ACC.5,C
	MOV   R4,A
	MOV   R5,#0
	MOV   A,#1
	RET

; Nastavi ACC pokud doslo ke zmene vstupu
; =======================================
AUX_ICH:CLR   A
	CALL  G_AUX1
	MOV   DPTR,#AUX_INO
	MOVX  A,@DPTR
	XCH   A,R4
	MOVX  @DPTR,A
	XRL   A,R4
	RET

; *******************************************************************
; Konfigurace komunikaci RS232 a uLAN

;FL_ECUL	Povolit komunikaci uLAN
;FL_ECRS	Povolit komunikaci RS232

RSEG	AS____X

COM_TYP:DS    2		; Typ komunikace
COM_ADR:DS    2		; Adresa jednotky
COM_SPD:DS    2		; Rychlost komunikace

RSEG	AS____C

COM_INI:CLR   ES
	CLR   FL_ECUL
	CLR   FL_ECRS
	MOV   DPTR,#COM_TYP
	MOVX  A,@DPTR
	DJNZ  ACC,COM_I50
	; Bude se pouzivat uLAN
	SETB  FL_ECUL
	CALL  I_U_LAN
	CALL  uL_OINI
	RET
COM_I50:DJNZ  ACC,COM_I99
    %IF (%WITH_RS232) THEN (
	; Bude se pouzivat RS232
	SETB  FL_ECRS
	CALL  COM_DIV
	MOV   R7,A
	CALL  RS232_INI
    )FI
COM_RS_OPL:
    %IF (%WITH_RS232) THEN (
	%LDMXi(RS_OPL,RS_OPL1)
	JNB   %STBY_FL,COM_I99
	%LDMXi(RS_OPL,RS_OPL2)
    )FI
COM_I99:RET

COM_DIVT:		; pro krystal 18432
	DB    80  ;  1200
	DB    40  ;  2400
	DB    20  ;  4800
	DB    10  ;  9600
	DB    5   ; 19200
	DB    0

; Vraci v ACC divisor pro danou rychlost
COM_DIV:MOV   DPTR,#COM_SPD
	MOVX  A,@DPTR
	MOV   DPTR,#COM_DIVT
	MOVC  A,@A+DPTR
	RET

; Zmeni rychlost komunikace
COM_SPDCH:
	MOV   DPTR,#COM_SPD
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	MOV   DPTR,#COM_DIVT
	MOVC  A,@A+DPTR
	MOV   DPTR,#COM_SPD
	JNZ   COM_SC1
	MOVX  @DPTR,A
COM_SC1:INC   DPTR
	CLR   A
	MOVX  @DPTR,A
	JMP   COM_RQINI

UW_COMi:
	CALL  UW_Mi
	JMP   COM_RQINI

COM_WR23:
	MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	JMP   UB_A_WR

COM_TYPCH:
	MOV   DPTR,#COM_TYP
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	ADD   A,#-3
	JNC   COM_TC1
	CLR   A
	MOVX  @DPTR,A
COM_TC1:INC   DPTR
	CLR   A
	MOVX  @DPTR,A
COM_RQINI:
	SETB  FL_REFR
	MOV   R2,#GL_COMCH
	MOV   R3,#0
	JMP   GLOB_RQ23

I_U_LAN:CALL  COM_DIV
	MOV   R0,#1
	CALL  uL_FNC	; Rychlost
	MOV   DPTR,#COM_ADR
	MOVX  A,@DPTR
	MOV   R0,#2
	CALL  uL_FNC	; Adresa
	MOV   R2,#0
	MOV   R0,#3
	CALL  uL_FNC	; Delka IB OB
	MOV   R2,#0
	MOV   R0,#4
	CALL  uL_FNC	; Rychle bloky
	MOV   R0,#0
	CALL  uL_FNC	; Start
%IF(%WITH_UL_DY) THEN (
	%LDR45i(STATUS)
	MOV   R6,#2
	CALL  UD_INIT
)FI
	MOV   A,#1
	RET

%IF(%WITH_UL_DY_EEP) THEN (
	DB    -'U',-'L',-'D',-'Y'
	%W   (0)
	%W   (SER_NUM)
	%W   (WR_SERNUM)
INI_SERNUM:
	MOV   R2,#010H	; pocet prenasenych byte
	MOV   R4,#0F0H	; pocatecni adresa v EEPROM
	MOV   DPTR,#SER_NUM
	CALL  EE_RD
	JZ    INI_SERNUM9
INI_SERNUM7:
	MOV   R2,#8
	MOV   DPTR,#SER_NUM
	MOV   A,#0FFH
INI_SERNUM8:
	MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R2,INI_SERNUM8
INI_SERNUM9:
	RET
WR_SERNUM:
        CLR   EAD
	CLR   ES
	MOV   PSW,#0
	MOV   SP,#80H
	CALL  I_TIMRI
	CALL  I_TIMRI
	CALL  WR_SERNUM1
	MOV   PWM0,#020H
	JMP   RESET
WR_SERNUM1:
	MOV   PWM0,#0C0H
	MOV   R2,#010H	; pocet prenasenych byte
	MOV   R4,#0F0H	; pocatecni adresa v EEPROM
	MOV   DPTR,#SER_NUM
	CALL  EE_WR
	RET

PUBLIC	UD_NCS_ADRNVSV

; Dodana funkce pro trvale ulozeni adresy v EEPROM
UD_NCS_ADRNVSV:
	MOV   DPTR,#uL_ADR
	MOVX  A,@DPTR
	MOV   DPTR,#COM_ADR
	MOVX  @DPTR,A
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A
	%LDR23i(EEC_SET)
	JMP   EEP_WRS
)FI

; *******************************************************************
; Komunikace RS232 pro AS

%IF (%WITH_RS232) THEN (

%*DEFINE (RS_TID(NAME)) (
RST_%NAME:DB '%NAME',0
)

%*DEFINE (RS_TIDSUF(NAME)) (
RST_%(%NAME)_SUF:DB '%NAME#',0
)

%*DEFINE (RSD_NEW (NAME)) (
RSD_P	SET   RSD_T
RSD_T	SET   $
	DB   LOW (RSD_P),HIGH (RSD_P)
	DB   LOW (RST_%NAME),HIGH (RST_%NAME)
)

RSEG	AS____X

RS_TMP:	DS    4

RSEG	AS____C

RSO_VER_T:DB  'VER=%VERSION',0

RSO_VER:MOV   DPTR,#RSO_VER_T
	JMP   RS_WRL1

RSO_TEST_T:DB 'TEST OK',0
RSO_TEST:MOV  DPTR,#RSO_TEST_T
	JMP   RS_WRL1

; Zapina/vypina ohlas READY/FAILED
RS_SRDY:MOV   A,R4
	ADD   A,#-'1'-1
	JNC   RS_SRDY1
	SETB  F0
	RET
RS_SRDY1:ADD  A,#1
	MOV   RSF_RDYR,C
	SETB  RSF_BSYO
	RET

; Zapina/vypina ohlas REPLY
RS_SRPLY:MOV  A,R4
	ADD   A,#-'1'-1
	JNC   RS_SRPL1
	SETB  F0
	RET
RS_SRPL1:ADD  A,#1
	MOV   RSF_RPLY,C
	MOV   RSF_LNT,C
	RET

RS_GLOB:CALL  xLDR23i
	JMP   GLOB_RQ23

RS_OFF:	JNB   %RUN_FL,RS_OFF2
	MOV   R2,#GL_END
	JMP   GLOB_RQ23
RS_OFF2:JMP   SEK_INI


; -----------------------------------
; Listy prikazu

RS_OPL1:DB    ':'
	%W    (RS_CML1I)
	DB    '?'
	%W    (RS_CML1O)
	DB    0

RS_OPL2:DB    ':'
	%W    (RS_CML2I)
	DB    '?'
	%W    (RS_CML2O)
	DB    0

; -----------------------------------
; Jmena prikazu

%RS_TID(SNUM)
%RS_TID(PRSN)
%RS_TID(PREP)
%RS_TID(INJ)
%RS_TID(OFF)
%RS_TID(CALSN)
%RS_TID(VER)
%RS_TID(TEST)
%RS_TID(IHEXLD)
%RS_TID(READY)
%RS_TID(REPLY)
%RS_TID(TEMP1)
%RS_TID(STBY)
%RS_TID(ACTIV)
%RS_TID(STAT)

; -----------------------------------
; Vstupni prikazy

RSD_T	SET   0

%RSD_NEW(TEST)
	%W    (RS_CMDA)
	%W    (RSO_TEST)

%IF (%DEBUG_FL) THEN (
%RSD_NEW(IHEXLD)
	%W    (RS_CMDA)
	%W    (IHEXLD)
)FI

%RSD_NEW(REPLY)
	%W    (RSI_ONOFA)
	%W    (RS_SRPLY)

%RSD_NEW(READY)
	%W    (RSI_ONOFA)
	%W    (RS_SRDY)

%RSD_NEW(ACTIV)
	%W    (RS_CMDA)
	%W    (RS_GLOB)
	DB    GL_PWRON,0

RS_CML2I SET  RSD_T

%RSD_NEW(SNUM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (SAMPNUM)	; Adresa
	%W    (0)	; Funkce

%RSD_NEW(PRSN)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (SAMPNUM)	; Adresa
	%W    (TM_PREP)	; Funkce

%RSD_NEW(PREP)
	%W    (RS_CMDA)
	%W    (TM_PREP)

%RSD_NEW(INJ)
	%W    (RS_CMDA)
	%W    (TM_LOAD)

%RSD_NEW(TEMP1)
	%W    (RSI_INTA)
	DB    0,001H
	%W    (0)
	%W    (S_TEMP)
	%W    (TMC1_RT)

%RSD_NEW(CALSN)
	%W    (RS_CMDA)
	%W    (TM_LPS)

%RSD_NEW(STBY)
	%W    (RS_CMDA)
	%W    (RS_GLOB)
	DB    GL_STBY,0

%RSD_NEW(OFF)
	%W    (RS_CMDA)
	%W    (RS_OFF)

RS_CML1I SET  RSD_T

; -----------------------------------
; Vystupni prikazy
RSD_T	SET   0

%RSD_NEW(TEST)
	%W    (RSO_TEST)

%RSD_NEW(VER)
	%W    (RSO_VER)

%RSD_NEW(STAT)
	%W    (RSO_INTE)
	DB    0,080H
	%W    (0)	; Adresa
	%W    (G_SEK_ST); Funkce

RS_CML2O SET  RSD_T

%RSD_NEW(SNUM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (SAMPNUM)	; Adresa
	%W    (0)	; Funkce

%RSD_NEW(TEMP1)
	%W    (RSO_INTE)
	DB    0,001H
	%W    (0)
	%W    (G_TEMP)
	%W    (TMC1_AT)

RS_CML1O SET  RSD_T

)FI

; *******************************************************************
; Komunikace s ostatnimi jednotkami

%OID_ADES(AI_STATUS,STATUS,u2)
%OID_ADES(AI_ERRCLR,ERRCLR,e)
I_OFF	  EQU   250
%OID_ADES(AI_OFF,OFF,e)
I_ON	  EQU   251
%OID_ADES(AI_ON,ON,e)
I_PREPSAMP EQU  401
%OID_ADES(AI_PREPSAMP,PREPSAMP,u2)
I_INJECT  EQU   402
%OID_ADES(AI_INJECT,INJECT,e)
I_SINICFG EQU   403
I_CALLPS  EQU   404
%OID_ADES(AI_CALLPS,CALLPS,e)
I_ASPREPARE EQU 405
%OID_ADES(AI_ASPREPARE,ASPREPARE,e)
I_ASLOAD  EQU   406
%OID_ADES(AI_ASLOAD,ASLOAD,e)
I_MANINJ  EQU   407
I_SAMPNUM EQU   408
%OID_ADES(AI_SAMPNUM,SAMPNUM,u2)
I_WHELLSTP EQU  421
I_WHELLPSR EQU  422
I_WHELLHLDT EQU 423
I_WHELLHLD EQU  424
I_WHELLGO EQU   425
I_ASH_UP  EQU   426
I_ASH_DOWN EQU  427
I_ASH_HOME EQU  428
I_ASH_BOT EQU   429
I_ASH_BOT2 EQU  430
I_LPS_AD1 EQU   431
I_LPS_AD2 EQU   432
I_LPS_TD1 EQU   433
I_LPS_TD2 EQU   434
I_PER_IRC_W EQU 435
I_PER_INJT EQU  436
I_PER_VOL1 EQU  437
%IF(%WITH_WHEEL80)THEN(
I_ASH_ROT EQU   441	; Manualni rizeni rotace
I_ASH_RGH EQU   442	; Prikaz home
I_ASH_RP1 EQU   443	; Pozice pro 25 kotouc
I_ASH_RP2 EQU   444	; Pozice pro 80 kotouc, 1-40
I_ASH_RP3 EQU   445	; Pozice pro 80 kotouc, 41-80
I_ASH_WASH EQU  446	; Hloubka sjeti pro cisteni
)FI
I_SAVECFG EQU   451
I_MARK_DADR EQU 461
%OID_ADES(AI_MARK_DADR,MARK_DADR,u2)

I_TEMP1	  EQU   301
%OID_ADES(AI_TEMP1,TEMP1,s2/.1)
I_TEMP1RQ EQU   311
%OID_ADES(AI_TEMP1RQ,TEMP1RQ,s2/.1)
I_TEMP_OFF EQU  334
%OID_ADES(AI_TEMP_OFF,TEMP_OFF,e)
I_TEMP_ON EQU   335
%OID_ADES(AI_TEMP_ON,TEMP_ON,e)
I_TEMP_ST EQU	336
%OID_ADES(AI_TEMP_ST,TEMP_ST,u2)
I_TEMP1MC EQU   351
I_TEMP1OC EQU   361
I_TEMP1RD EQU   371

RSEG	AS____C

; inicializace objektovych komunikaci
uL_OINI:%LDR45i (OID_1IN)	; seznam prijimanych prikazu
	%LDR67i (OID_1OUT)	; seznam vysilanych prikazu
	JMP    US_INIT

; Identifikace typu pristroje
PUBLIC	uL_IDB,uL_IDE
uL_IDB: DB    '.mt %VERSION .uP 51x',0
uL_IDE:

; *******************************************************************
; prikazy pro AS

G_STATUS:JMP  G_SEK_ST

ERRCLR_U:JMP  SEK_INI

OFF_U:	JMP   SEK_INI

ON_U:	MOV   R2,#GL_PWRON
	MOV   R3,#0
	JMP   GLOB_RQ23

PREP_U: MOV   DPTR,#SEK_ST
	; zpracovat predchozi stav davkovace
	MOV   DPTR,#SAMPNUM
	CALL  xSVR45i
	JMP   TM_PREP

INJ_U:	JMP   TM_INJ

SAVECFG_U:%LDR23i(EEC_SER); Ulozeni parametru do EEPROM
	JMP   EEP_WRS

MARK_DADR_U:
	%LDMXi(CU_PROT,1)
	RET

; Prijimane hodnoty

OID_T	SET   $
	%W    (I_ERRCLR)
	%W    (OID_ISTD)
	%W    (AI_ERRCLR)
	%W    (ERRCLR_U)

%OID_NEW(I_SAVECFG ,0)
	%W    (SAVECFG_U)

%IF(%WITH_WHEEL80)THEN(
%OID_NEW(I_ASH_ROT,0)
	%W    (UI_INT)
	%W    (0)
	%W    (TM_GOROT)

%OID_NEW(I_ASH_RP1,0)
	%W    (UI_INT)
	%W    (ASH_RP1)
	%W    (0)

%OID_NEW(I_ASH_RP2,0)
	%W    (UI_INT)
	%W    (ASH_RP2)
	%W    (0)

%OID_NEW(I_ASH_RP3,0)
	%W    (UI_INT)
	%W    (ASH_RP3)
	%W    (0)

%OID_NEW(I_ASH_WASH,0)
	%W    (UI_INT)
	%W    (ASH_WASH)
	%W    (0)
)FI

%OID_NEW(I_WHELLHLD,0)
	%W    (TM_STPHLD)

%OID_NEW(I_WHELLGO,0)
	%W    (TM_STP2)

%OID_NEW(I_ASH_UP,0)
	%W    (TM_ASH_UP)

%OID_NEW(I_ASH_DOWN,0)
	%W    (TM_ASH_DOWN)

%OID_NEW(I_ASH_HOME,0)
	%W    (TM_ASH_HOME)

%OID_NEW(I_OFF,AI_OFF)
	%W    (OFF_U)

%OID_NEW(I_ON,AI_ON)
	%W    (ON_U)

%OID_NEW(I_TEMP1,AI_TEMP1)
	%W    (UI_INT)
	%W    (0)
	%W    (S_TEMP)
	%W    (TMC1_RT)

%OID_NEW(I_TEMP1RQ,AI_TEMP1RQ)
	%W    (UI_INT)
	%W    (0)
	%W    (S_TEMP)
	%W    (TMC1_RT)

%OID_NEW(I_TEMP_OFF,AI_TEMP_OFF)
	%W    (TMC_OFF)

%OID_NEW(I_TEMP_ON,AI_TEMP_ON)
	%W    (TMC_ON)

%OID_NEW(I_MARK_DADR,AI_MARK_DADR)
	%W    (UI_INT)
	%W    (CU_DADR)
	%W    (MARK_DADR_U)

%OID_NEW(I_TEMP1MC,0)
	%W    (UI_INT)
	%W    (TMC1_MC)
	%W    (0)

%OID_NEW(I_TEMP1OC,0)
	%W    (UI_INT)
	%W    (TMC1_OC)
	%W    (0)

%OID_NEW(I_INJECT,AI_INJECT)
	%W    (INJ_U)

%OID_NEW(I_CALLPS,AI_CALLPS)
	%W    (TM_LPS)

%OID_NEW(I_ASPREPARE,AI_ASPREPARE)
	%W    (TM_PREP)

%OID_NEW(I_ASLOAD,AI_ASLOAD)
	%W    (TM_LOAD)

%OID_NEW(I_MANINJ ,0)
	%W    (TM_MAN)

%OID_NEW(I_ASH_BOT,0)
	%W    (UI_INT)
	%W    (ASH1_BOT1)
	%W    (0)

%OID_NEW(I_ASH_BOT2,0)
	%W    (UI_INT)
	%W    (ASH1_BOT2)
	%W    (0)

%OID_NEW(I_LPS_TD1,0)
	%W    (UI_INT)
	%W    (LPS_TD1)
	%W    (0)

%OID_NEW(I_LPS_TD2,0)
	%W    (UI_INT)
	%W    (LPS_TD2)
	%W    (0)

%OID_NEW(I_PER_INJT,0)
	%W    (UI_INT)
	%W    (P_INJT)
	%W    (0)

%OID_NEW(I_PER_VOL1,0)
	%W    (UI_INT)
	%W    (P_VOL1)
	%W    (0)

%OID_NEW(I_PER_IRC_W,0)
	%W    (UI_INT)
	%W    (P_IRC_W)
	%W    (0)

%OID_NEW(I_SAMPNUM,AI_SAMPNUM)
	%W    (UI_INT)
	%W    (SAMPNUM)
	%W    (0)

%OID_NEW(I_PREPSAMP,AI_PREPSAMP)
	%W    (UI_INT)
	%W    (0)
	%W    (PREP_U)

%OID_NEW(I_SINICFG,0)
	%W    (UI_INT)
	%W    (0)
	%W    (TM_INICFG)

OID_1IN SET   OID_T

; Vysilane hodnoty

OID_T	SET   0

%OID_NEW(I_MARK_DADR,AI_MARK_DADR)
	%W    (UO_INT)
	%W    (CU_DADR)
	%W    (0)

%IF(%WITH_WHEEL80)THEN(
%OID_NEW(I_ASH_ROT,0)
	%W    (UO_INT)
	%W    (REG_A+OREG_C+OMR_AP)
	%W    (0)

%OID_NEW(I_ASH_RP1,0)
	%W    (UO_INT)
	%W    (ASH_RP1)
	%W    (0)

%OID_NEW(I_ASH_RP2,0)
	%W    (UO_INT)
	%W    (ASH_RP2)
	%W    (0)

%OID_NEW(I_ASH_RP3,0)
	%W    (UO_INT)
	%W    (ASH_RP3)
	%W    (0)

%OID_NEW(I_ASH_WASH,0)
	%W    (UO_INT)
	%W    (ASH_WASH)
	%W    (0)
)FI

%OID_NEW(I_WHELLPSR,0)
	%W    (UO_INT)
	%W    (0)
	%W    (STP_PSR)

%OID_NEW(I_LPS_TD1,0)
	%W    (UO_INT)
	%W    (LPS_TD1)
	%W    (0)

%OID_NEW(I_LPS_TD2,0)
	%W    (UO_INT)
	%W    (LPS_TD2)
	%W    (0)

%OID_NEW(I_LPS_AD1,0)
	%W    (UO_INT)
	%W    (LPS_AD1)
	%W    (0)

%OID_NEW(I_LPS_AD2,0)
	%W    (UO_INT)
	%W    (LPS_AD2)
	%W    (0)

%OID_NEW(I_STATUS,AI_STATUS)
	%W    (UO_INT)
	%W    (0)
	%W    (G_STATUS)

%OID_NEW(I_SAMPNUM,AI_SAMPNUM)
	%W    (UO_INT)
	%W    (SAMPNUM)
	%W    (0)

%OID_NEW(I_ASH_BOT,0)
	%W    (UO_INT)
	%W    (ASH1_BOT1)
	%W    (0)

%OID_NEW(I_ASH_BOT2,0)
	%W    (UO_INT)
	%W    (ASH1_BOT2)
	%W    (0)

%OID_NEW(I_PER_INJT,0)
	%W    (UO_INT)
	%W    (P_INJT)
	%W    (0)

%OID_NEW(I_PER_VOL1,0)
	%W    (UO_INT)
	%W    (P_VOL1)
	%W    (0)

%OID_NEW(I_PER_IRC_W,0)
	%W    (UO_INT)
	%W    (P_IRC_W)
	%W    (0)

%OID_NEW(I_TEMP1,AI_TEMP1)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TEMP)
	%W    (TMC1_AT)

%OID_NEW(I_TEMP1RQ,AI_TEMP1RQ)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TEMP)
	%W    (TMC1_RT)

%OID_NEW(I_TEMP_ST,AI_TEMP_ST)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TMC_ST)

%OID_NEW(I_TEMP1MC,0)
	%W    (UO_INT)
	%W    (TMC1_MC)
	%W    (0)

%OID_NEW(I_TEMP1OC,0)
	%W    (UO_INT)
	%W    (TMC1_OC)
	%W    (0)

%OID_NEW(I_TEMP1RD,0)
	%W    (UO_INT)
	%W    (TMC1_RD)
	%W    (0)

OID_1OUT SET  OID_T


; Vysle znacku na adresu v R4
LAN_MRK:JNB   FL_ECUL,L_MRKR
	MOV   DPTR,#CU_PROT
	MOVX  A,@DPTR
	CJNE  A,#1,L_MRKR
	MOV   DPTR,#CU_DADR
	MOVX  A,@DPTR
	MOV   R4,A

	MOV   DPTR,#uL_SBP
	MOVX  A,@DPTR
	PUSH  ACC
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	PUSH  ACC
	CLR   A
	MOVX  @DPTR,A

	;MOV   DPTR,#uL_GRP
	;MOVX  A,@DPTR
	;MOV   A,#2
	;MOV   R4,A
	MOV   R5,#4EH
	CLR   F0
	CALL  uL_S_OP
	MOV   DPTR,#L_MRKD
	MOV   R4,#4
	MOV   R5,#0
	CALL  uL_S_WR
	CALL  uL_S_CL

	JNB   F0,L_MRK1
	; informovat o chybe
	;SETB  uLE_ABS
L_MRK1:
	MOV   DPTR,#uL_SBP+1
	POP   ACC
	MOVX  @DPTR,A
	CALL  DECDPTR
	POP   ACC
	MOVX  @DPTR,A
L_MRKR:	MOV   A,#1
	RET

L_MRKD: DB    0,7,0,0


; *******************************************************************
; Rizeni cyklu

RSEG	AS____X

CC_FRST:DS    2		; Prvni vzorek v cyklu
CC_LAST:DS    2		; Posledni vzorek v cyklu
CC_REP:	DS    2		; Pocet opakovani vzorku
CC_TIME:DS    2		; Cas jednoho cyklu
SAMPREP:DS    2		; Aktualni cislo opakovani vzorku

; Co vysilat a ocekavat na AUX
CA_OFF:	DS    2		; Pocatecni aux
CA_NEXT:DS    2		; Pocatek davkovani (IN)
CA_RDY:	DS    2		; Pripraveno
CA_INJ:	DS    2		; Provedst davku    (IN)
CA_WAIT:DS    2		; Provedst davku
CA_END:	DS    2		; Skonceni sekvence

; Vysilani znacky
CU_PROT:DS    2		; Protokol pro vysilani znacky
CU_DADR:DS    2		; Cilova adresa vysilane znacky
CA_CLREND:

RSEG	AS____C

; Spustit beh cyklu
SQ_RUN:	MOV   DPTR,#GS_MSK
	MOVX  A,@DPTR
	ANL   A,#NOT (MGS_TIM OR MGS_RDY OR MGS_AUX)
	MOVX  @DPTR,A
	CALL  G_SEK_ST
	MOV   A,R4
	ORL   A,R5
	JZ    SQ_RUN5
	CALL  ERRBEEP
	RET
SQ_RUN5:CALL  UI_ABORT
	SETB  %RUN_FL
	%LDR23i(UT_GR51)
	CALL  GR_RQ23
	MOV   DPTR,#CA_WAIT	; AUX .. cekani na start
	CALL  SQ_SAUX
	MOV   DPTR,#CC_FRST
	CALL  xLDR45i
	MOV   DPTR,#SAMPNUM
	CALL  xSVR45i
	SJMP  SQ_NE40

; Novy cyklus
SQ_NEXT:MOV   DPTR,#GS_MSK
	MOVX  A,@DPTR
	ANL   A,#NOT (MGS_TIM OR MGS_RDY)
	MOVX  @DPTR,A
	CALL  G_SEK_ST
	MOV   A,R5
	JNB   ACC.7,SQ_NE03
	JMP   SQ_ERR
SQ_NE03:ORL   A,R4
	JZ    SQ_NE05
	MOV   R7,#BGS_RDY
	%LDR45i(SQ_NEXT)
	CALL  GS_SIGA
	RET
SQ_NE05:MOV   DPTR,#CC_REP
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   DPTR,#SAMPREP
	MOVX  A,@DPTR
	CLR   C
	SUBB  A,R4
	JNC   SQ_NE20
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	SJMP  SQ_NE50
SQ_NE20:MOV   DPTR,#CC_LAST
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   DPTR,#SAMPNUM
	MOVX  A,@DPTR
	CLR   C
	SUBB  A,R4
	JC    SQ_NE24
	JMP   SQ_END
SQ_NE24:MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
SQ_NE40:MOV   DPTR,#SAMPREP
	MOV   A,#1
	MOVX  @DPTR,A
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A
SQ_NE50:MOV   DPTR,#CA_NEXT	; Cekani na AUX pro pripravu
	CALL  SQ_WAUX
	JNZ   SQ_NE99
	MOV   R7,#BGS_RDY	; Pockat na konec pripravy
	%LDR45i(SQ_RDY)
	CALL  GS_SIGA
	CALL  TM_PREP		; Spustit pripravu
	CLR   FL_RTIM
SQ_NE99:RET

; Davka je pripravena => nadavkuj
SQ_RDY: CALL  G_SEK_ST
	MOV   A,R5
	JNB   ACC.7,SQ_RDY1
	JMP   SQ_ERR
SQ_RDY1:MOV   DPTR,#CA_RDY		; Pripraveno
	CALL  SQ_SAUX
	MOV   DPTR,#CA_INJ		; Cekat na povel
	CALL  SQ_WAUX
	JNZ   SQ_NE99
	CALL  G_SEK_ST			; Je davka pripravena ?
	MOV   A,R4
	CJNE  A,#081H,SQ_RDY2		; SEK_ST_WINJ
	MOV   A,R5
	JZ    SQ_RDY3
SQ_RDY2:JMP   SQ_ERR
SQ_RDY3:MOV   DPTR,#CC_TIME
	CALL  xLDR45i
	CALL  S_ALTIM
	MOV   R7,#BGS_TIM	; Kdy spustit pristi davku
	%LDR45i(SQ_NEXT)	; rizena casem
	CALL  GS_SIGA
	MOV   R7,#BGS_RDY	; Kdy volny pro dalsi davku
	%LDR45i(SQ_INFR)
	CALL  GS_SIGA
	CALL  TM_LOAD
	SETB  FL_RTIM
	RET

SQ_INFR:MOV   DPTR,#CA_WAIT	; Muze znovu davkovat
	CALL  SQ_SAUX
	RET

; Chyba v sekvenci
SQ_ERR:

; Konec davky
SQ_END:	CALL  UI_ABORT
	CLR   %RUN_FL
	%LDR23i(UT_GR11)
	CALL  GR_RQ23
	MOV   DPTR,#GS_MSK
	MOVX  A,@DPTR
	ANL   A,#NOT (MGS_TIM OR MGS_RDY OR MGS_AUX)
	MOVX  @DPTR,A
	MOV   DPTR,#CA_END
	CALL  SQ_SAUX
	CLR   FL_RTIM
	CALL  ERRBEEP
	%LDR45i(10)		; Kdy skoncit Aux End
	CALL  S_ALTIM
	MOV   R7,#BGS_TIM
	%LDR45i(SQ_AOFF)	; rizena casem
	CALL  GS_SIGA
	RET

; Konec signalu Aux End
SQ_AOFF:MOV   DPTR,#CA_OFF
	CALL  SQ_SAUX
	RET

; Vynulovani promennych AUX a ULAN
AUX_INIT:CLR  A
	MOV   DPTR,#AUX_BUF
	MOVX  @DPTR,A
	MOV   DPTR,#CA_OFF
	MOV   R0,#CA_CLREND-CA_OFF
AUX_INIT1:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,AUX_INIT1
	RET

; Zpracuj AUX na adrese DPTR
SQ_WAUX:PUSH  DPL
	PUSH  DPH
	CALL  G_AUX
	POP   DPH
	POP   DPL
	MOV   ACC,#1
	MOVC  A,@A+DPTR
	MOV   R2,A
	MOVX  A,@DPTR
	XRL   A,R4
	ANL   A,R2
	ANL   A,#AUX_IN_MSK
	JZ    SQ_SAUX
	MOV   R0,SP	; Je treba cekat na AUX
	DEC   R0
	MOV   A,@R0
	ADD   A,#LOW  (-6)
	MOV   R4,A
	INC   R0
	MOV   A,@R0
	ADDC  A,#HIGH (-6)
	MOV   R5,A
	MOV   R7,#BGS_AUX
	CALL  GS_SIGA
	MOV   A,#1
	RET
SQ_SAUX:MOVX  A,@DPTR	; Vyslani novych AUX
	MOV   R4,A
	CALL  S_AUX
	CLR   A
	RET

; Nastavi cas alarmu
S_ALTIM:MOV   DPTR,#AL_TIME
	CALL  xSVR45i
	MOV   DPTR,#TIMRI
	CLR   A
	CLR   EA
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	SETB  EA
	RET

; *******************************************************************
; Globalni udalosti a signaly

; Signaly

GS_NUM	SET   3		; pocet globalnich signalu

BGS_TIM	SET   0		; nastaven pri AL_TIME <= TIME
BGS_RDY	SET   1		; nastaven po skonceni ukolu
BGS_AUX	SET   2		; zmena na vtupech AUX

MGS_TIM	SET   1 SHL BGS_TIM
MGS_RDY	SET   1 SHL BGS_RDY
MGS_AUX	SET   1 SHL BGS_AUX

RSEG	AS____B

GS_BUF:	DS    1		; buffer globalnich signalu

RSEG	AS____X

GS_MSK:	DS    1		; maska povolenych signalu
AL_TIME:DS    2		; kdyz AL_TIME <= TIME nastavi BGS_TIM

GS_VECT:DS    4*GS_NUM

RSEG	AS____C

; Kontrola a zpracovani signalu
; vola scheduled funkci pro nastaveny signal
; ktery je povolen GS_MSK, nuluje masku nalezeneho signalu
; registry volane funkce
;	R23 .. uzivatelem definovana hodnota
;	R4  .. cislo signalu
;	R5  .. maska signalu

GS_DO:	MOV   DPTR,#GS_MSK
	MOVX  A,@DPTR
	ANL   A,GS_BUF
	JZ    GS_DO9
	MOV   R2,A
	MOV   R4,#-1
	MOV   R5,#080H
GS_DO2:	INC   R4
	MOV   A,R5
	RL    A
	MOV   R5,A
	ANL   A,R2
	JZ    GS_DO2
	MOV   DPTR,#GS_MSK
	MOVX  A,@DPTR
	ORL   A,R5
	XRL   A,R5
	MOVX  @DPTR,A
	MOV   A,R4
	RL    A
	RL    A
	ADD   A,#LOW GS_VECT
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH GS_VECT
	MOV   DPH,A
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	INC   DPTR
	MOVX  A,@DPTR
	PUSH  ACC
	INC   DPTR
	MOVX  A,@DPTR
	PUSH  ACC
GS_DO9:	RET

; GS_SIG nastavi akci pro pozadovany signal
; GS_SIGM navic povoli signal v GS_MSK
; GS_SIGA nejdrive nuluje signal v GS_BUF
; vstup:R23 .. uzivatelem definovana hodnota
;	R45 .. adresa volane funkce
;	R7  .. cislo signalu

GS_GLOB:%LDR45i(GLOB_RQ23)
GS_SIGA:CALL  GS_CLR
GS_SIGM:MOV   A,R7
	MOV   DPTR,#BITMASK
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   DPTR,#GS_MSK
	MOVX  A,@DPTR
	ORL   A,R6
	MOVX  @DPTR,A
GS_SIG: MOV   A,R7
	RL    A
	RL    A
	ADD   A,#LOW GS_VECT
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH GS_VECT
	MOV   DPH,A
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	RET

; Vynuluje priznak signalu R7
GS_CLR: MOV   A,R7
	MOV   DPTR,#BITMASK
	MOVC  A,@A+DPTR
	CPL   A
	ANL   GS_BUF,A
	RET

BITMASK:DB    001h
	DB    002h
	DB    004h
	DB    008h
	DB    010h
	DB    020h
	DB    040h
	DB    080h

; Posle globalni udalost
GLOB_RQ23:MOV A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	MOV   R7,#ET_GLOB
	JMP   EV_POST

; Zpracovani globalnich udalosti
GLOB_DO:MOV   A,R4
	MOV   R7,A
	MOV   A,R5
	MOV   R4,A
	MOV   DPTR,#GLOB_SF1
	JMP   SEL_FNC

; Globalni udalosti

GL_STBY  EQU  1
GL_PWRON EQU  2
GL_RUN	 EQU  3
GL_END	 EQU  4
GL_COMCH EQU  5

; Tabulka globalnich udalosti

GLOB_SF1:
	DB    GL_RUN
	%W    (0)
	%W    (SQ_RUN)

	DB    GL_END
	%W    (0)
	%W    (SQ_END)

	DB    GL_STBY
	%W    (0)
	%W    (GO_STBY)

	DB    GL_PWRON
	%W    (0)
	%W    (GO_PWRON)

	DB    GL_COMCH
	%W    (0)
	%W    (COM_INI)

	DB    0

GO_PWRON:MOV  SP,#STACK
	CALL  UI_ABORT
	CLR   %STBY_FL
	CALL  COM_RS_OPL
	CALL  TM_INICFG
	%LDR23i(UT_GR11)
	CALL  GR_RQ23
	JMP   UT_ML

GO_STBY:MOV   SP,#STACK
	CALL  UI_ABORT
	SETB  %STBY_FL
	CALL  COM_RS_OPL
	CALL  SEK_INI
	%LDR23i(UT_GR91)
	CALL  GR_RQ23
	JMP   UT_ML

; *******************************************************************
; User interface

RSEG	AS____X

UT_UIAD:DS    40
UT_DATA:DS    40

RSEG	AS____C

UT_INIT:CLR   D4LINE
	SETB  FL_CMAV
	MOV   DPTR,#UI_MV_SX
	MOV   A,#16
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#2
	MOVX  @DPTR,A
	MOV   DPTR,#UT_UIAD
	MOV   UI_AD,DPL
	MOV   UI_AD+1,DPH
	CLR   A
	MOV   DPTR,#EV_BUF
	MOVX  @DPTR,A
	MOV   DPTR,#GR_ACT
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#REF_TIM
	MOVX  @DPTR,A
	RET

UT_TREF:MOV   DPTR,#REF_TIM
	MOVX  A,@DPTR
	JNZ   UT_TRE1
	MOV   DPTR,#REF_PER
	MOVX  A,@DPTR
	MOV   DPTR,#REF_TIM
	MOVX  @DPTR,A
	MOV   A,#1
	RET
UT_TRE1:CLR   A
	RET

UT:     CALL  UT_INIT
	SETB  %STBY_FL
	MOV   R7,#ET_RQGR
	%LDR45i(UT_GR91)
	CALL  EV_POST

UT_ML:  CALL  EV_GET
	JZ    UT_ML50
UT_ML44:CJNE  R7,#ET_GLOB,UT_ML45
	CALL  GLOB_DO		; Globalni udalosti
	SJMP  UT_ML
UT_ML45:CALL  EV_DO
	JMP   UT_ML
UT_ML50:CALL  GS_DO		; vyvola handler signalu
    %IF (%WITH_RS232) THEN (
	JNB   FL_ECRS,UT_ML53
	CALL  RS_POOL		; Komunikace RS232
	JNZ   UT_ML53
	MOV   DPTR,#SEK_ST
	MOVX  A,@DPTR
	CJNE  A,#SEK_ST_WINJ,UT_ML51
	CLR   A
UT_ML51:JNZ   UT_ML52
	MOV   A,MR_FLG
	ANL   A,#MMR_BSY
UT_ML52:MOV   C,MR_FLG.BMR_ERR	; READY/FAIL?
	ORL   C,SEK_ERR
	ORL   C,STP_ERR
	MOV   ACC.7,C
	CALL  RS_RDYSND
    )FI
UT_ML53:JNB   FL_ECUL,UT_ML55
	;CALL  UC_OI		; uL master OC
	CALL  UI_PR		; uL slave OI
    %IF(%WITH_UL_DY) THEN (
	CALL  UD_RQ		; dynamicaka adresace
    )FI
	JB    uLF_INE,UT_ML55
	;JB    UCF_RDP,UT_ML57
UT_ML55:CALL  UT_TREF		; Urceni okamziku refrese od casu
	JZ    UT_ML60
	JNB   FL_ECUL,UT_ML57
	;CALL  UC_REFR		; uL master OC
UT_ML57:;CLR   UCF_RDP
	SETB  FL_REFR
	SJMP  UT_ML65
UT_ML60:CALL  UT_ULED
UT_ML65:JMP   UT_ML

UT_ULED:CALL  G_SEK_ST
	MOV   R2,#1 SHL LFB_LOAD
	MOV   DPTR,#SEK_ST
	MOVX  A,@DPTR
	JZ    UT_ULE5
	ADD   A,#-SEK_ST_WINJ-1
	JC    UT_ULE1
	JNB   FL_WINJ,UT_ULE5
	MOV   R2,#1 SHL LFB_PREP
	SJMP  UT_ULE5
UT_ULE1:ADD   A,#SEK_ST_WINJ+1-SEK_ST_LPSC
	JNC   UT_ULE5
	MOV   R2,#1 SHL LFB_CAL
UT_ULE5:
      MOV A,#NOT((1 SHL LFB_LOAD)OR(1 SHL LFB_PREP)OR(1 SHL LFB_CAL))
	ORL   A,R2
	ANL   LEB_FLG,A
	ANL   LED_FLG,A
	CALL  UT_UST1
	MOV   A,R5
	MOV   C,ACC.7
	MOV   DPTR,#TMC1_FLG
	MOVX  A,@DPTR
	JNB   ACC.BMR_ERR,UT_ULE7
	SETB  C
UT_ULE7:MOV   LEB_FLG.LFB_BEEP,C
	MOV   DPTR,#INV_ST
	MOVX  A,@DPTR
	MOV   C,ACC.BINV_ERR
	MOV   LEB_FLG.LFB_INJ,C
	JC    UT_ULE8
	MOV   C,ACC.BINV_P2
	MOV   LED_FLG.LFB_INJ,C
UT_ULE8:;CALL  G_TMC_ST
	;MOV   R2,#1 SHL LFB_TEMPER
	;CALL  UT_UST1
	JMP   LEDWR

; kontrola aktualnosti stavu
UT_UST1:MOV   A,R5
	JB    ACC.7,UT_UST7
	ORL   A,R4
UT_UST2:JZ    UT_UST5
UT_UST3:MOV   A,R2		; On
	CPL   A
	ANL   LEB_FLG,A
	CPL   A
	ORL   LED_FLG,A
	RET
UT_UST5:MOV   A,R2		; Off
	CPL   A
	ANL   LEB_FLG,A
	ANL   LED_FLG,A
	RET
UT_UST7:MOV   A,R2		; Error
	ORL   LEB_FLG,A
	RET

; *******************************************************************
; Podminene metody objektu

EXTRN	DATA(EV_TYPE)

; AUX nastavit jen mimo program
BFL_EV_NR:
	MOV   A,EV_TYPE
	CJNE  A,#ET_GETF,BFL_EV_NR1
	JB    %RUN_FL,BFL_EV_NR9
BFL_EV_NR1:
	JMP   BFL_EV
BFL_EV_NR9:
	RET

; INT nastavit jen mimo program
UIN_EV_NR:
	MOV   A,EV_TYPE
	CJNE  A,#ET_GETF,UIN_EV_NR1
	JB    %RUN_FL,UIN_EV_NR9
UIN_EV_NR1:
	JMP   UIN_EV
UIN_EV_NR9:
	RET

; INT zobrazit jen pri behu casu
UIN_EV_TIM:
	MOV   A,EV_TYPE
	CJNE  A,#ET_DRAW,UIN_EV_TIM1
	SJMP  UIN_EV_TIM2
UIN_EV_TIM1:
	CJNE  A,#ET_REFR,UIN_EV_TIM3
UIN_EV_TIM2:
	JNB   FL_RTIM,UIN_EV_TIM9
UIN_EV_TIM3:
	JMP   UIN_EV
UIN_EV_TIM9:
	RET

; Nacte hodnotu, pricte R2, a kontroluje max R3
MUT_AD23:
	MOV   A,R2
	PUSH  ACC
	MOV   A,R3
	PUSH  ACC
	CALL  UB_A_RD
	POP   B
	POP   ACC
	ADD   A,R4
	JB    ACC.7,MUT_AD23N
	MOV   R4,A
	CLR   C
	JBC   B.7,MUT_AD23K
	SUBB  A,B
	JC    MUT_AD23W
	MOV   R4,B
	DEC   R4
	SJMP  MUT_AD23W
MUT_AD23K:
	SUBB  A,B
	JC    MUT_AD23W
	MOV   R4,A
	SJMP  MUT_AD23W
MUT_AD23N:
	JNB   B.7,MUT_AD23M
	ADD   A,B
	MOV   R4,A
	DB    074H	; MOV  A,#n
MUT_AD23M:
	MOV   R4,#0
MUT_AD23W:
	MOV   R5,#0
	SETB  FL_REFR
	JMP   UB_A_WR

; *******************************************************************

UT_SF1:	DB    K_STBY		; Globalni udalosti
	%W    (GL_STBY)
	%W    (GLOB_RQ23)

	DB    K_RUN
	%W    (GL_RUN)
	%W    (GLOB_RQ23)

	DB    K_CAL		; Manualni rizeni
	%W    (0)
	%W    (TM_LPS)

	DB    K_PREP
	%W    (0)
	%W    (TM_PREP)

	DB    K_LOAD
	%W    (0)
	%W    (TM_LOAD)

	DB    K_MAN
	%W    (0)
	%W    (TM_MAN)

	DB    K_STOP
	%W    (0)
	%W    (SEK_INI)

	DB    K_FIX
	%W    (0)
	%W    (TM_STPHLD)

	DB    K_SAMPL		; prepinani displeju
	%W    (UT_GR11)
	%W    (GR_RQ23)

	DB    K_SET
	%W    (UT_GR12)
	%W    (GR_RQ23)

	DB    K_AUX
	%W    (UT_GR13)
	%W    (GR_RQ23)

	DB    K_CYCLE
	%W    (UT_GR14)
	%W    (GR_RQ23)

	DB    K_TEMPER
	%W    (UT_GR15)
	%W    (GR_RQ23)

UT_SF80:DB    K_MODE
	%W    (UT_GR70)
	%W    (GR_RQ23)

UT_SFTN:DB    K_RIGHT
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    K_LEFT
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    K_UP
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    K_DOWN
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

UT_SF0:	DB    0

; *******************************************************************
; Manualni rezim rezim

; ---------------------------------
; Sampler
UT_GR11:DS    UT_GR11+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR11+OGR_BTXT-$
	%W    (UT_GT11)
	DS    UT_GR11+OGR_STXT-$
	%W    (0)
	DS    UT_GR11+OGR_HLP-$
	%W    (0)
	DS    UT_GR11+OGR_SFT-$
	%W    (UT_SF11)
	DS    UT_GR11+OGR_PU-$
	%W    (UT_U1101)
	%W    (UT_U1102)
	%W    (0)

UT_GT11:DB    'Sample  Status',0

UT_SF11:DB    -1
	%W    (UT_SF1)

UT_U1101:
	DS    UT_U1101+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1101+OU_MSK-$
	DB    0
	DS    UT_U1101+OU_X-$
	DB    8,1,8,1
	DS    UT_U1101+OU_HLP-$
	%W    (0)
	DS    UT_U1101+OU_SFT-$
	%W    (0)
	DS    UT_U1101+OU_A_RD-$
	%W    (G_SEK_ST)	; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1101+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U1101+OU_M_S-$
	%W    (0)
	DS    UT_U1101+OU_M_P-$
	%W    (0)
	DS    UT_U1101+OU_M_F-$
	%W    (0)
	DS    UT_U1101+OU_M_T-$
	%W    (0FF00H)
	%W    (100H)
	%W    (UT_U1101TR)
	%W    (0FFFFH)
	%W    (0)
	%W    (UT_U1101T0)
	%W    (0FFFFH)
	%W    (00081H)
	%W    (UT_U1101TP)
	%W    (08000H)
	%W    (0)
	%W    (UT_U1101T1)
	%W    (0FFFFH)
	%W    (0FD88H)
	%W    (UT_U1101TE1)	; kalibrace
	%W    (0FFFFH)
	%W    (0FD89H)
	%W    (UT_U1101TE2)	; cidlo 1
	%W    (0FFFFH)
	%W    (0FD0BH)
	%W    (UT_U1101TE2)	; cidlo 1
	%W    (0FFFFH)
	%W    (0FD8AH)
	%W    (UT_U1101TE3)	; cidlo 2
	%W    (0FFFFH)
	%W    (0FD03H)
	%W    (UT_U1101TE4)	; vyplach
	%W    (0FFFFH)
	%W    (0FD1CH)
	%W    (UT_U1101TE4)	; vyplach
	%W    (08000H)
	%W    (08000H)
	%W    (UT_U1101TE)
	%W    (0)
UT_U1101T0:DB    'Off',0
UT_U1101T1:DB    'Working',0
UT_U1101TP:DB    'Prepared',0
UT_U1101TR:DB    'Running',0
UT_U1101TE1:DB   'Err Cal',0
UT_U1101TE2:DB   'Err Sn 1',0
UT_U1101TE3:DB   'Err Sn 2',0
UT_U1101TE4:DB   'Err Wash',0
UT_U1101TE:DB    'Error',0

UT_U1102:
	DS    UT_U1102+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1102+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1102+OU_X-$
	DB    0,1,5,1
	DS    UT_U1102+OU_HLP-$
	%W    (0)
	DS    UT_U1102+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1102+OU_A_RD-$
	%W    (UR_Mi)		; A_RD !!!!!!!!!!!!!!!!!
	%W    (UW_Mi)		; A_WR
	DS    UT_U1102+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (SAMPNUM)		; DP
	DS    UT_U1102+OU_I_F-$
	DB    00H		; format I_F
	%W    (1)		; I_L
	%W    (C_SAMPLAST)	; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Settings

UT_GR12:DS    UT_GR12+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR12+OGR_BTXT-$
	%W    (UT_GT12)
	DS    UT_GR12+OGR_STXT-$
	%W    (0)
	DS    UT_GR12+OGR_HLP-$
	%W    (0)
	DS    UT_GR12+OGR_SFT-$
	%W    (UT_SF12)
	DS    UT_GR12+OGR_PU-$
	%W    (UT_U1208)
	%W    (UT_U1210)
	%W    (UT_U1230)
	%W    (UT_U1231)
	%W    (UT_U1232)
	%W    (UT_U1233)
	%W    (UT_U1234)
	%W    (UT_U1235)
	%W    (UT_U1236)
	%W    (UT_U1240)
	%W    (UT_U1241)
	%W    (UT_U1250)
	%W    (0)

UT_GT12:DB    '  Settings',C_NL
	DB    'Inject T',C_NL
	DB    'Fill Vol',C_NL
	DB    'Aux Off',C_NL
	DB    'Aux Next',C_NL
	DB    'Aux Rdy',C_NL
	DB    'Aux Inj',C_NL
	DB    'Aux Wait',C_NL
	DB    'Aux End',C_NL
	DB    'Manual Inj',C_NL
	DB    'Send',C_NL
	DB    0

UT_SF12:DB    -1
	%W    (UT_SF1)

UT_U1208:
	DS    UT_U1208+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1208+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1208+OU_X-$
	DB    10,1,5,1
	DS    UT_U1208+OU_HLP-$
	%W    (0)
	DS    UT_U1208+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1208+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1208+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (P_INJT)		; DP
	DS    UT_U1208+OU_I_F-$
	DB    02H		; format I_F
	%W    (0)		; I_L
	%W    (2000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1210:
	DS    UT_U1210+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1210+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1210+OU_X-$
	DB    10,2,5,1
	DS    UT_U1210+OU_HLP-$
	%W    (0)
	DS    UT_U1210+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1210+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1210+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (P_VOL1)		; DP
	DS    UT_U1210+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (2000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; -- AUX --

UT_U1230:
	DS    UT_U1230+OU_VEVJ-$
	DB    2
	DW    BFL_EV
	DS    UT_U1230+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1230+OU_X-$
	DB    9,3,4,1
	DS    UT_U1230+OU_HLP-$
	%W    (0)
	DS    UT_U1230+OU_SFT-$
	%W    (UT_SFAUX1)
	DS    UT_U1230+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1230+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (CA_OFF)		; DP
	DS    UT_U1230+OU_BF_S-$
	%W    (0)
	DS    UT_U1230+OU_BF_P-$
	%W    (0)
	DS    UT_U1230+OU_BF_F-$
	%W    (0)
	DS    UT_U1230+OU_BF_T-$
	DB    0
	DB    '_',0
	DB    '1',0
	DB    1
	DB    '_',0
	DB    '2',0
	DB    2
	DB    '_',0
	DB    '3',0
	DB    3
	DB    '_',0
	DB    '4',0
	DB    -1

UT_U1231:
	DS    UT_U1231+OU_VEVJ-$
	DB    2
	DW    BFL_EV
	DS    UT_U1231+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1231+OU_X-$
	DB    9,4,7,1
	DS    UT_U1231+OU_HLP-$
	%W    (0)
	DS    UT_U1231+OU_SFT-$
	%W    (UT_SFAUX2)
	DS    UT_U1231+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1231+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (CA_NEXT)		; DP
	DS    UT_U1231+OU_BF_S-$
	%W    (0)
	DS    UT_U1231+OU_BF_P-$
	%W    (0)
	DS    UT_U1231+OU_BF_F-$
	%W    (0)
	DS    UT_U1231+OU_BF_T-$
	DB    0
	DB    '_',0
	DB    '1',0
	DB    1
	DB    '_',0
	DB    '2',0
	DB    2
	DB    '_',0
	DB    '3',0
	DB    3
	DB    '_',0
	DB    '4',0
	DB    8+4
	DB    ' x',C_BS,0
	DB    ' ',0
	DB    4
	DB    '_',0
	DB    '5',0
	DB    8+5
	DB    'x',C_BS,0
	DB    '',0
	DB    5
	DB    '_',0
	DB    '6',0
	DB    -1

UT_U1232:
	DS    UT_U1232+OU_VEVJ-$
	DB    2
	DW    BFL_EV
	DS    UT_U1232+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1232+OU_X-$
	DB    9,5,4,1
	DS    UT_U1232+OU_HLP-$
	%W    (0)
	DS    UT_U1232+OU_SFT-$
	%W    (UT_SFAUX1)
	DS    UT_U1232+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1232+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (CA_RDY)		; DP
	DS    UT_U1232+OU_BF_S-$
	%W    (0)
	DS    UT_U1232+OU_BF_P-$
	%W    (0)
	DS    UT_U1232+OU_BF_F-$
	%W    (0)
	DS    UT_U1232+OU_BF_T-$
	DB    0
	DB    '_',0
	DB    '1',0
	DB    1
	DB    '_',0
	DB    '2',0
	DB    2
	DB    '_',0
	DB    '3',0
	DB    3
	DB    '_',0
	DB    '4',0
	DB    -1

UT_U1233:
	DS    UT_U1233+OU_VEVJ-$
	DB    2
	DW    BFL_EV
	DS    UT_U1233+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1233+OU_X-$
	DB    9,6,7,1
	DS    UT_U1233+OU_HLP-$
	%W    (0)
	DS    UT_U1233+OU_SFT-$
	%W    (UT_SFAUX2)
	DS    UT_U1233+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1233+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (CA_INJ)		; DP
	DS    UT_U1233+OU_BF_S-$
	%W    (0)
	DS    UT_U1233+OU_BF_P-$
	%W    (0)
	DS    UT_U1233+OU_BF_F-$
	%W    (0)
	DS    UT_U1233+OU_BF_T-$
	DB    0
	DB    '_',0
	DB    '1',0
	DB    1
	DB    '_',0
	DB    '2',0
	DB    2
	DB    '_',0
	DB    '3',0
	DB    3
	DB    '_',0
	DB    '4',0
	DB    8+4
	DB    ' x',C_BS,0
	DB    ' ',0
	DB    4
	DB    '_',0
	DB    '5',0
	DB    8+5
	DB    'x',C_BS,0
	DB    '',0
	DB    5
	DB    '_',0
	DB    '6',0
	DB    -1

UT_U1234:
	DS    UT_U1234+OU_VEVJ-$
	DB    2
	DW    BFL_EV
	DS    UT_U1234+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1234+OU_X-$
	DB    9,7,4,1
	DS    UT_U1234+OU_HLP-$
	%W    (0)
	DS    UT_U1234+OU_SFT-$
	%W    (UT_SFAUX1)
	DS    UT_U1234+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1234+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (CA_WAIT)		; DP
	DS    UT_U1234+OU_BF_S-$
	%W    (0)
	DS    UT_U1234+OU_BF_P-$
	%W    (0)
	DS    UT_U1234+OU_BF_F-$
	%W    (0)
	DS    UT_U1234+OU_BF_T-$
	DB    0
	DB    '_',0
	DB    '1',0
	DB    1
	DB    '_',0
	DB    '2',0
	DB    2
	DB    '_',0
	DB    '3',0
	DB    3
	DB    '_',0
	DB    '4',0
	DB    -1

UT_U1235:
	DS    UT_U1235+OU_VEVJ-$
	DB    2
	DW    BFL_EV
	DS    UT_U1235+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1235+OU_X-$
	DB    9,8,4,1
	DS    UT_U1235+OU_HLP-$
	%W    (0)
	DS    UT_U1235+OU_SFT-$
	%W    (UT_SFAUX1)
	DS    UT_U1235+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1235+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (CA_END)		; DP
	DS    UT_U1235+OU_BF_S-$
	%W    (0)
	DS    UT_U1235+OU_BF_P-$
	%W    (0)
	DS    UT_U1235+OU_BF_F-$
	%W    (0)
	DS    UT_U1235+OU_BF_T-$
	DB    0
	DB    '_',0
	DB    '1',0
	DB    1
	DB    '_',0
	DB    '2',0
	DB    2
	DB    '_',0
	DB    '3',0
	DB    3
	DB    '_',0
	DB    '4',0
	DB    -1

UT_U1236:
	DS    UT_U1236+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1236+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1236+OU_X-$
	DB    11,9,3,1
	DS    UT_U1236+OU_HLP-$
	%W    (0)
	DS    UT_U1236+OU_SFT-$
	%W    (0)
	DS    UT_U1236+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1236+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (INV_AUX)		; DP
	DS    UT_U1236+OU_M_S-$
	%W    (0)
	DS    UT_U1236+OU_M_P-$
	%W    (MIVA_ON)
	DS    UT_U1236+OU_M_F-$
	%W    (BFL_XOR)
	DS    UT_U1236+OU_M_T-$
	%W    (MIVA_ON)
	%W    (0)
	%W    (UT_U1236T0)
	%W    (MIVA_ON)
	%W    (MIVA_ON)
	%W    (UT_U1236T1)
	%W    (0)
UT_U1236T0:DB    ' No',0
UT_U1236T1:DB    'Yes',0

; -- uLan Mark --

UT_U1240:
	DS    UT_U1240+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1240+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1240+OU_X-$
	DB    5,10,4,1
	DS    UT_U1240+OU_HLP-$
	%W    (0)
	DS    UT_U1240+OU_SFT-$
	%W    (0)
	DS    UT_U1240+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1240+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (CU_PROT)		; DP
	DS    UT_U1240+OU_M_S-$
	%W    (0)
	DS    UT_U1240+OU_M_P-$
	DB    1,2+080H
	DS    UT_U1240+OU_M_F-$
	%W    (MUT_AD23)
	DS    UT_U1240+OU_M_T-$
	%W    (0FFH)
	%W    (0)
	%W    (UT_U1240T0)
	%W    (0FFH)
	%W    (1)
	%W    (UT_U1240T1)
	%W    (0FFH)
	%W    (1)
	%W    (UT_U1240T2)
	%W    (0)
UT_U1240T0:DB    ' Off',0
UT_U1240T1:DB    'Mark',0
UT_U1240T2:DB    '----',0

UT_U1241:
	DS    UT_U1241+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1241+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1241+OU_X-$
	DB    10,10,3,1
	DS    UT_U1241+OU_HLP-$
	%W    (0)
	DS    UT_U1241+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1241+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1241+OU_DPSI-$
	%W    (0)		; DPSI
	%W    (CU_DADR)		; DP
	DS    UT_U1241+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (99)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; -- Save --

UT_U1250:
	DS    UT_U1250+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U1250+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1250+OU_X-$
	DB    0,11,13,1
	DS    UT_U1250+OU_HLP-$
	%W    (0)
	DS    UT_U1250+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U1250+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U1250+OU_B_P-$
	%W    (EEC_SET)		; Uzivatelske nastaveni
	DS    UT_U1250+OU_B_F-$
	%W    (EEP_WRS)		; Zapis do EEPROM
	DS    UT_U1250+OU_B_T-$
	DB    'Save settings',0

; ---------------------------------
; Pomocne vstupy vystupy

UT_GR13:DS    UT_GR13+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR13+OGR_BTXT-$
	%W    (UT_GT13)
	DS    UT_GR13+OGR_STXT-$
	%W    (0)
	DS    UT_GR13+OGR_HLP-$
	%W    (0)
	DS    UT_GR13+OGR_SFT-$
	%W    (UT_SF13)
	DS    UT_GR13+OGR_PU-$
	%W    (UT_U1301)
	%W    (UT_U1302)
	%W    (0)

UT_GT13:DB    'Aux    -S-  -R- ',C_NL
	DB    'output',0

UT_SF13:DB    -1
	%W    (UT_SF1)

UT_U1301:
	DS    UT_U1301+OU_VEVJ-$
	DB    2
	DW    BFL_EV_NR
	DS    UT_U1301+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1301+OU_X-$
	DB    7,1,4,1
	DS    UT_U1301+OU_HLP-$
	%W    (0)
	DS    UT_U1301+OU_SFT-$
	%W    (UT_SFAUX1)
	DS    UT_U1301+OU_A_RD-$
	%W    (G_AUX)		; A_RD
	%W    (S_AUX)		; A_WR
	DS    UT_U1301+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U1301+OU_BF_S-$
	%W    (0)
	DS    UT_U1301+OU_BF_P-$
	%W    (0)
	DS    UT_U1301+OU_BF_F-$
	%W    (0)
	DS    UT_U1301+OU_BF_T-$
	DB    0
	DB    '_',0
	DB    '1',0
	DB    1
	DB    '_',0
	DB    '2',0
	DB    2
	DB    '_',0
	DB    '3',0
	DB    3
	DB    '_',0
	DB    '4',0
	DB    -1

UT_SFAUX2:
	DB    K_5
	%W    (1 SHL 4)
	%W    (BFL_3ST)

	DB    K_6
	%W    (1 SHL 5)
	%W    (BFL_3ST)

UT_SFAUX1:
	DB    K_1
	%W    (1 SHL 0)
	%W    (BFL_XOR)

	DB    K_2
	%W    (1 SHL 1)
	%W    (BFL_XOR)

	DB    K_3
	%W    (1 SHL 2)
	%W    (BFL_XOR)

	DB    K_4
	%W    (1 SHL 3)
	%W    (BFL_XOR)

	DB    0

UT_U1302:
	DS    UT_U1302+OU_VEVJ-$
	DB    2
	DW    BFL_EV
	DS    UT_U1302+OU_MSK-$
	DB    0
	DS    UT_U1302+OU_X-$
	DB    12,1,4,1
	DS    UT_U1302+OU_HLP-$
	%W    (0)
	DS    UT_U1302+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1302+OU_A_RD-$
	%W    (G_AUX)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1302+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U1302+OU_BF_S-$
	%W    (0)
	DS    UT_U1302+OU_BF_P-$
	%W    (0)
	DS    UT_U1302+OU_BF_F-$
	%W    (0)
	DS    UT_U1302+OU_BF_T-$
	DB    4
	DB    '_',0
	DB    '5',0
	DB    5
	DB    '_',0
	DB    '6',0
	DB    -1

; ---------------------------------
; Rizeni cyklu

UT_GR14:DS    UT_GR14+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR14+OGR_BTXT-$
	%W    (UT_GT14)
	DS    UT_GR14+OGR_STXT-$
	%W    (0)
	DS    UT_GR14+OGR_HLP-$
	%W    (0)
	DS    UT_GR14+OGR_SFT-$
	%W    (UT_SF14)
	DS    UT_GR14+OGR_PU-$
	%W    (UT_U1401)
	%W    (UT_U1402)
	%W    (UT_U1403)
	%W    (UT_U1404)
	%W    (0)

UT_GT14:DB    'Sample  Rep Time',C_NL
	DB    'xx-xx   xx  xxxx',0

UT_SF14:DB    -1
	%W    (UT_SF1)

UT_U1401:
	DS    UT_U1401+OU_VEVJ-$
	DB    2
	DW    UIN_EV_NR
	DS    UT_U1401+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1401+OU_X-$
	DB    0,1,2,1
	DS    UT_U1401+OU_HLP-$
	%W    (0)
	DS    UT_U1401+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1401+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1401+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (CC_FRST)		; DP
	DS    UT_U1401+OU_I_F-$
	DB    00H		; format I_F
	%W    (1)		; I_L
	%W    (C_SAMPLAST)	; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1402:
	DS    UT_U1402+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1402+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1402+OU_X-$
	DB    3,1,2,1
	DS    UT_U1402+OU_HLP-$
	%W    (0)
	DS    UT_U1402+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1402+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1402+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (CC_LAST)		; DP
	DS    UT_U1402+OU_I_F-$
	DB    00H		; format I_F
	%W    (1)		; I_L
	%W    (C_SAMPLAST)	; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1403:
	DS    UT_U1403+OU_VEVJ-$
	DB    2
	DW    UIN_EV_NR
	DS    UT_U1403+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1403+OU_X-$
	DB    8,1,2,1
	DS    UT_U1403+OU_HLP-$
	%W    (0)
	DS    UT_U1403+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1403+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1403+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (CC_REP)		; DP
	DS    UT_U1403+OU_I_F-$
	DB    00H		; format I_F
	%W    (1)		; I_L
	%W    (20)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1404:
	DS    UT_U1404+OU_VEVJ-$
	DB    2
	DW    UIN_EV_NR
	DS    UT_U1404+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1404+OU_X-$
	DB    12,1,4,1
	DS    UT_U1404+OU_HLP-$
	%W    (0)
	DS    UT_U1404+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1404+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1404+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (CC_TIME)		; DP
	DS    UT_U1404+OU_I_F-$
	DB    02H		; format I_F
	%W    (1)		; I_L
	%W    (60000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Temperatures
UT_GR15:DS    UT_GR15+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR15+OGR_BTXT-$
	%W    (UT_GT15)
	DS    UT_GR15+OGR_STXT-$
	%W    (0)
	DS    UT_GR15+OGR_HLP-$
	%W    (0)
	DS    UT_GR15+OGR_SFT-$
	%W    (UT_SF15)
	DS    UT_GR15+OGR_PU-$
	%W    (UT_U1501)
	%W    (UT_U1502)
	%W    (UT_U1503)
	%W    (0)

UT_GT15:DB    'Temper  Fin  Act',0

UT_SF15:
%IF(0) THEN (
	DB    K_F5
	%W    (0)
	%W    (TMC_OFF)

	DB    K_F4
	%W    (0)
	%W    (TMC_ON)
)FI
	DB    -1
	%W    (UT_SF1)

UT_U1501:
	DS    UT_U1501+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1501+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1501+OU_X-$
	DB    1,1,3,1
	DS    UT_U1501+OU_HLP-$
	%W    (0)
	DS    UT_U1501+OU_SFT-$
	%W    (0)
	DS    UT_U1501+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1501+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TMC1_FLG)	; DP
	DS    UT_U1501+OU_M_S-$
	%W    (0)
	DS    UT_U1501+OU_M_P-$
	%W    (TMC1)
	DS    UT_U1501+OU_M_F-$
	%W    (TMC_ONOFF)
	DS    UT_U1501+OU_M_T-$
	%W    (MMR_ERR)
	%W    (MMR_ERR)
	%W    (UT_U1501TE)
	%W    (MMR_ENR)
	%W    (0)
	%W    (UT_U1501T0)
	%W    (MMR_ENR)
	%W    (MMR_ENR)
	%W    (UT_U1501T1)
	%W    (0)
UT_U1501T0:DB    'Off',0
UT_U1501T1:DB    ' On',0
UT_U1501TE:DB    'Err',0

UT_U1502:
	DS    UT_U1502+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1502+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1502+OU_X-$
	DB    7,1,4,1
	DS    UT_U1502+OU_HLP-$
	%W    (0)
	DS    UT_U1502+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1502+OU_A_RD-$
	%W    (UR_TEMP)		; A_RD
	%W    (UW_TEMP)		; A_WR
	DS    UT_U1502+OU_DPSI-$
	%W    (TMC1_RT)         ; DPSI
	%W    (TMC1_RT)		; DP
	DS    UT_U1502+OU_I_F-$
	DB    01H		; format I_F
	%W    (0)		; I_L
	%W    (350)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1503:
	DS    UT_U1503+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1503+OU_MSK-$
	DB    0
	DS    UT_U1503+OU_X-$
	DB    12,1,4,1
	DS    UT_U1503+OU_HLP-$
	%W    (0)
	DS    UT_U1503+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1503+OU_A_RD-$
	%W    (UR_TEMP)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1503+OU_DPSI-$
	%W    (TMC1_AT)         ; DPSI
	%W    (TMC1_AT)		; DP
	DS    UT_U1503+OU_I_F-$
	DB    81H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; *******************************************************************
; Run rezim

UT_SFRUN:
	DB    K_END
	%W    (GL_END)
	%W    (GLOB_RQ23)

	DB    K_SAMPL		; prepinani displeju
	%W    (UT_GR51)
	%W    (GR_RQ23)

	DB    K_AUX
	%W    (UT_GR53)
	%W    (GR_RQ23)

	DB    K_CYCLE
	%W    (UT_GR54)
	%W    (GR_RQ23)

	DB    K_TEMPER
	%W    (UT_GR55)
	%W    (GR_RQ23)

	DB    -1
	%W    (UT_SFTN)

; ---------------------------------
; Sampler
UT_GR51:DS    UT_GR51+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR51+OGR_BTXT-$
	%W    (UT_GT51)
	DS    UT_GR51+OGR_STXT-$
	%W    (0)
	DS    UT_GR51+OGR_HLP-$
	%W    (0)
	DS    UT_GR51+OGR_SFT-$
	%W    (UT_SFRUN)
	DS    UT_GR51+OGR_PU-$
	%W    (UT_U5101)
	%W    (UT_U5102)
	%W    (UT_U5103)
	%W    (UT_U5104)
	%W    (0)

UT_GT51:DB    'Sample  Stat/Tim',0

UT_U5101:
	DS    UT_U5101+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U5101+OU_MSK-$
	DB    0
	DS    UT_U5101+OU_X-$
	DB    9,1,7,1
	DS    UT_U5101+OU_HLP-$
	%W    (0)
	DS    UT_U5101+OU_SFT-$
	%W    (0)
	DS    UT_U5101+OU_A_RD-$
	%W    (G_SEK_ST)	; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U5101+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U5101+OU_M_S-$
	%W    (0)
	DS    UT_U5101+OU_M_P-$
	%W    (0)
	DS    UT_U5101+OU_M_F-$
	%W    (0)
	DS    UT_U5101+OU_M_T-$
	%W    (0FF00H)
	%W    (100H)
	%W    (UT_U5101TR)
	%W    (0FFFFH)
	%W    (0)
	%W    (UT_U5101T0)
	%W    (0FFFFH)
	%W    (00081H)
	%W    (UT_U5101TP)
	%W    (08000H)
	%W    (0)
	%W    (UT_U5101T1)
	%W    (8000H)
	%W    (8000H)
	%W    (UT_U5101TE)
	%W    (0)
UT_U5101T0:DB    'Wait',0
UT_U5101T1:DB    'Working',0
UT_U5101TP:DB    'Prepared',0
UT_U5101TR:DB    'Running',0
UT_U5101TE:DB    'Error',0

UT_U5102:
	DS    UT_U5102+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U5102+OU_MSK-$
	DB    0
	DS    UT_U5102+OU_X-$
	DB    0,1,2,1
	DS    UT_U5102+OU_HLP-$
	%W    (0)
	DS    UT_U5102+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U5102+OU_A_RD-$
	%W    (UR_Mi)		; A_RD !!!!!!!!!!!!!!!!!
	%W    (UW_Mi)		; A_WR
	DS    UT_U5102+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (SAMPNUM)		; DP
	DS    UT_U5102+OU_I_F-$
	DB    00H		; format I_F

UT_U5103:
	DS    UT_U5103+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U5103+OU_MSK-$
	DB    0
	DS    UT_U5103+OU_X-$
	DB    3,1,2,1
	DS    UT_U5103+OU_HLP-$
	%W    (0)
	DS    UT_U5103+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U5103+OU_A_RD-$
	%W    (UR_Mi)		; A_RD !!!!!!!!!!!!!!!!!
	%W    (UW_Mi)		; A_WR
	DS    UT_U5103+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (SAMPREP)		; DP
	DS    UT_U5103+OU_I_F-$
	DB    00H		; format I_F

UT_U5104:
	DS    UT_U5104+OU_VEVJ-$
	DB    2
	DW    UIN_EV_TIM
	DS    UT_U5104+OU_MSK-$
	DB    0
	DS    UT_U5104+OU_X-$
	DB    9,1,7,1
	DS    UT_U5104+OU_HLP-$
	%W    (0)
	DS    UT_U5104+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U5104+OU_A_RD-$
	%W    (UR_Mi)		; A_RD !!!!!!!!!!!!!!!!!
	%W    (UW_Mi)		; A_WR
	DS    UT_U5104+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TIME)		; DP
	DS    UT_U5104+OU_I_F-$
	DB    02H		; format I_F

; ---------------------------------
; Pomocne vstupy vystupy

UT_GR53:DS    UT_GR53+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR53+OGR_BTXT-$
	%W    (UT_GT13)
	DS    UT_GR53+OGR_STXT-$
	%W    (0)
	DS    UT_GR53+OGR_HLP-$
	%W    (0)
	DS    UT_GR53+OGR_SFT-$
	%W    (UT_SFRUN)
	DS    UT_GR53+OGR_PU-$
	%W    (UT_U1301)
	%W    (UT_U1302)
	%W    (0)

; ---------------------------------
; Rizeni cyklu

UT_GR54:DS    UT_GR54+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR54+OGR_BTXT-$
	%W    (UT_GT14)
	DS    UT_GR54+OGR_STXT-$
	%W    (0)
	DS    UT_GR54+OGR_HLP-$
	%W    (0)
	DS    UT_GR54+OGR_SFT-$
	%W    (UT_SFRUN)
	DS    UT_GR54+OGR_PU-$
	%W    (UT_U1401)
	%W    (UT_U1402)
	%W    (UT_U1403)
	%W    (UT_U1404)
	%W    (0)

; ---------------------------------
; Temperatures
UT_GR55:DS    UT_GR55+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR55+OGR_BTXT-$
	%W    (UT_GT15)
	DS    UT_GR55+OGR_STXT-$
	%W    (0)
	DS    UT_GR55+OGR_HLP-$
	%W    (0)
	DS    UT_GR55+OGR_SFT-$
	%W    (UT_SFRUN)
	DS    UT_GR55+OGR_PU-$
	%W    (UT_U1501)
	%W    (UT_U1502)
	%W    (UT_U1503)
	%W    (0)

; *******************************************************************
; Main menu

UT_GR70:DS    UT_GR70+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR70+OGR_BTXT-$
	%W    (UT_GT70)
	DS    UT_GR70+OGR_STXT-$
	%W    (0)		; UT_GS70
	DS    UT_GR70+OGR_HLP-$
	%W    (0)
	DS    UT_GR70+OGR_SFT-$
	%W    (UT_SF70)
	DS    UT_GR70+OGR_PU-$
	%W    (UT_U7001)
	%W    (UT_U7002)
	%W    (UT_U7080)
	%W    (0)

UT_GT70:DB    '  Mode menu',0

UT_SF70:DB    -1
	%W    (UT_SF1)

UT_U7001:
	DS    UT_U7001+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U7001+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7001+OU_X-$
	DB    0,1,13,1
	DS    UT_U7001+OU_HLP-$
	%W    (0)
	DS    UT_U7001+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U7001+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U7001+OU_B_P-$
	%W    (EEC_SET)		; Uzivatelske nastaveni
	DS    UT_U7001+OU_B_F-$
	%W    (EEP_WRS)		; Zapis do EEPROM
	DS    UT_U7001+OU_B_T-$
	DB    'Save settings',0

UT_U7002:
	DS    UT_U7002+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U7002+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7002+OU_X-$
	DB    0,2,12,1
	DS    UT_U7002+OU_HLP-$
	%W    (0)
	DS    UT_U7002+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U7002+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U7002+OU_B_P-$
	%W    (UT_GR72)
	DS    UT_U7002+OU_B_F-$
	%W    (GR_RQ23)
	DS    UT_U7002+OU_B_T-$
	DB    'Comunication',0

UT_U7080:
	DS    UT_U7080+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U7080+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7080+OU_X-$
	DB    0,3,7,1
	DS    UT_U7080+OU_HLP-$
	%W    (0)
	DS    UT_U7080+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U7080+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U7080+OU_B_P-$
	%W    (UT_GR81)
	DS    UT_U7080+OU_B_F-$
	%W    (GR_RQ23)
	DS    UT_U7080+OU_B_T-$
	DB    'Service',0


; ---------------------------------
; Nastaveni parametru komunikace
UT_GR72:DS    UT_GR72+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR72+OGR_BTXT-$
	%W    (UT_GT72)
	DS    UT_GR72+OGR_STXT-$
	%W    (0)		; UT_GS72
	DS    UT_GR72+OGR_HLP-$
	%W    (0)
	DS    UT_GR72+OGR_SFT-$
	%W    (UT_SF72)
	DS    UT_GR72+OGR_PU-$
	%W    (UT_U7201)
	%W    (UT_U7202)
	%W    (UT_U7203)
	%W    (0)

UT_GT72:DB    'Type  Adr  Spd',0

UT_SF72:DB    -1
	%W    (UT_SF1)

UT_U7201:
	DS    UT_U7201+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U7201+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7201+OU_X-$
	DB    0,1,5,1
	DS    UT_U7201+OU_HLP-$
	%W    (0)
	DS    UT_U7201+OU_SFT-$
	%W    (UT_SF7201)
	DS    UT_U7201+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_COMi)		; A_WR
	DS    UT_U7201+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (COM_TYP)		; DP
	DS    UT_U7201+OU_M_S-$
	%W    (0)
	DS    UT_U7201+OU_M_P-$
	%W    (0)
	DS    UT_U7201+OU_M_F-$
	%W    (COM_TYPCH)
	DS    UT_U7201+OU_M_T-$
	%W    (0FFFFH)
	%W    (00000H)
	%W    (UT_U7201T0)
	%W    (0FFFFH)
	%W    (00001H)
	%W    (UT_U7201T1)
	%W    (0FFFFH)
	%W    (00002H)
	%W    (UT_U7201T2)
	%W    (0)
UT_U7201T0:DB    'None',0
UT_U7201T1:DB    'uLAN',0
UT_U7201T2:DB    'RS232',0

UT_SF7201:
	DB    K_2
	%W    (2)
	%W    (COM_WR23)

	DB    K_1
	%W    (1)
	%W    (COM_WR23)

	DB    K_0
	%W    (0)
	%W    (COM_WR23)

	DB    0

UT_U7202:
	DS    UT_U7202+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U7202+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7202+OU_X-$
	DB    6,1,2,1
	DS    UT_U7202+OU_HLP-$
	%W    (0)
	DS    UT_U7202+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U7202+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_COMi)	; A_WR
	DS    UT_U7202+OU_DPSI-$
	%W    (0)		; DPSI
	%W    (COM_ADR)		; DP
	DS    UT_U7202+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (99)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U7203:
	DS    UT_U7203+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U7203+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7203+OU_X-$
	DB    10,1,5,1
	DS    UT_U7203+OU_HLP-$
	%W    (0)
	DS    UT_U7203+OU_SFT-$
	%W    (UT_SF7203)
	DS    UT_U7203+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_COMi)		; A_WR
	DS    UT_U7203+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (COM_SPD)		; DP
	DS    UT_U7203+OU_M_S-$
	%W    (0)
	DS    UT_U7203+OU_M_P-$
	%W    (0)
	DS    UT_U7203+OU_M_F-$
	%W    (COM_SPDCH)
	DS    UT_U7203+OU_M_T-$
	%W    (0FFFFH)
	%W    (00000H)
	%W    (UT_U7203T0)
	%W    (0FFFFH)
	%W    (00001H)
	%W    (UT_U7203T1)
	%W    (0FFFFH)
	%W    (00002H)
	%W    (UT_U7203T2)
	%W    (0FFFFH)
	%W    (00003H)
	%W    (UT_U7203T3)
	%W    (0FFFFH)
	%W    (00004H)
	%W    (UT_U7203T4)
	%W    (0)
UT_U7203T0:DB    ' 1200',0
UT_U7203T1:DB    ' 2400',0
UT_U7203T2:DB    ' 4800',0
UT_U7203T3:DB    ' 9600',0
UT_U7203T4:DB    '19200',0

UT_SF7203:
	DB    K_9
	%W    (3)
	%W    (COM_WR23)

	DB    K_4
	%W    (2)
	%W    (COM_WR23)

	DB    K_2
	%W    (1)
	%W    (COM_WR23)

	DB    K_1
	%W    (4)
	%W    (COM_WR23)

	DB    K_0
	%W    (0)
	%W    (COM_WR23)

	DB    0

; *******************************************************************
; Servisni rezim

UT_GR81:DS    UT_GR81+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR81+OGR_BTXT-$
	%W    (UT_GT81)
	DS    UT_GR81+OGR_STXT-$
	%W    (UT_GS81)
	DS    UT_GR81+OGR_HLP-$
	%W    (0)
	DS    UT_GR81+OGR_SFT-$
	%W    (UT_SF81)
	DS    UT_GR81+OGR_PU-$
	%W    (UT_U8101)	; Exit
	%W    (UT_U8116)	; LPS_AD1
	%W    (UT_U8117)	; LPS_AD2
	%W    (UT_U8118)	; LPS_TD1
	%W    (UT_U8119)	; LPS_TD2
	%W    (UT_U8123)	; Inject - Po1 Po2
	%W    (UT_U8124)	; SAMPNUM
	%W    (UT_U8125)	; STM sensors
	%W    (UT_U8128)	; Sequencer state

	%W    (UT_U8131)	; Handler - Hld Down Up  Ini
	%W    (UT_U8132)	; ASH1_BOT1

	%W    (UT_U8133)	; ASH1_BOT1
	%W    (UT_U8134)	; ASH1_BOT2
%IF(%WITH_WHEEL80)THEN(
	%W    (UT_U8135)	; ASH_WASH
	%W    (UT_U8136)	; 
	%W    (UT_U8137)	; ASH_RP1
	%W    (UT_U8138)	; ASH_RP2
	%W    (UT_U8139)	; ASH_RP3
) FI
	%W    (UT_U8151)	; TMC1_RD
	%W    (UT_U8152)	; TMC1_OC
	%W    (UT_U8153)	; TMC1_MC
	%W    (UT_U8161)	; P_IRC_W
	%W    (UT_U8162)	; UW_MRPl REG_C - peristaltika
	%W    (UT_U8190)	; Save service
%IF (%DEBUG_FL) THEN (
	%W    (UT_U8199)
)FI
	%W    (0)

UT_GT81:DB    '  Service',C_NL
	DB    'Sens',C_NL
	DB    'Sens td',C_NL
	DB    '',C_NL
	DB    '',C_NL
	DB    '',C_NL
	DB    '       Samp.',C_NL
	DB    'Wheel sens.',C_NL
	DB    'Seq. st.',C_NL

	DB    '',C_NL	; Handler


	DB    'Bottom 25',C_NL
	DB    'Bottom 80',C_NL
	DB    'Wash depth',C_NL
	DB    'Rot act',C_NL
	DB    'Rot pos1',C_NL
	DB    'Rot pos2',C_NL
	DB    'Rot pos3',C_NL

	DB    'Temp ADC',C_NL
	DB    'Temp Offs',C_NL
	DB    'Temp Slop',C_NL
	DB    'Per IRC',C_NL
	DB    'Per test',0

UT_GS81:DB    '=== ===  === ===',0

UT_SF81:DB    K_CAL
	%W    (0)
	%W    (TM_LPS)

	DB    K_PREP
	%W    (0)
	%W    (TM_PREP)

	DB    K_LOAD
	%W    (0)
	%W    (TM_LOAD)

	DB    K_MAN
	%W    (0)
	%W    (TM_MAN)

	DB    K_STOP
	%W    (0)
	%W    (SEK_INI)

	DB    -1
	%W    (UT_SF80)


UT_U8101:
	DS    UT_U8101+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U8101+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8101+OU_X-$
	DB    12,0,4,1
	DS    UT_U8101+OU_HLP-$
	%W    (0)
	DS    UT_U8101+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8101+OU_B_S-$
	%W    (UT_US8101)
	DS    UT_U8101+OU_B_P-$
	%W    (UT_GR70)
	DS    UT_U8101+OU_B_F-$
	%W    (GR_RQ23)
	DS    UT_U8101+OU_B_T-$
	DB    'Exit',0

UT_US8101:DB  '=== === ==== === ===',0

; ---------------------------------
; Snimace bublin

UT_U8116:
	DS    UT_U8116+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8116+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8116+OU_X-$
	DB    5,1,5,1
	DS    UT_U8116+OU_HLP-$
	%W    (0)
	DS    UT_U8116+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8116+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U8116+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (LPS_AD1)		; DP
	DS    UT_U8116+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8117:
	DS    UT_U8117+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8117+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8117+OU_X-$
	DB    11,1,5,1
	DS    UT_U8117+OU_HLP-$
	%W    (0)
	DS    UT_U8117+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8117+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U8117+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (LPS_AD2)		; DP
	DS    UT_U8117+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8118:
	DS    UT_U8118+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8118+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8118+OU_X-$
	DB    8,2,3,1
	DS    UT_U8118+OU_HLP-$
	%W    (0)
	DS    UT_U8118+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8118+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U8118+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (LPS_TD1)		; DP
	DS    UT_U8118+OU_I_F-$
	DB    000H		; format I_F
	%W    (2)		; I_L
	%W    (100)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8119:
	DS    UT_U8119+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8119+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8119+OU_X-$
	DB    12,2,3,1
	DS    UT_U8119+OU_HLP-$
	%W    (0)
	DS    UT_U8119+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8119+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U8119+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (LPS_TD2)		; DP
	DS    UT_U8119+OU_I_F-$
	DB    000H		; format I_F
	%W    (2)		; I_L
	%W    (100)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Inject, Krokac

UT_U8123:
	DS    UT_U8123+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U8123+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8123+OU_X-$
	DB    0,6,6,1
	DS    UT_U8123+OU_HLP-$
	%W    (0)
	DS    UT_U8123+OU_SFT-$
	%W    (UT_SF8123)
	DS    UT_U8123+OU_B_S-$
	%W    (UT_US8123)
	DS    UT_U8123+OU_B_P-$
	%W    (0)
	DS    UT_U8123+OU_B_F-$
	%W    (0)
	DS    UT_U8123+OU_B_T-$
	DB    'Inject',0

UT_US8123:DB  'Po1 Po2  === ===',0

UT_SF8123:
	DB    K_F1
	%W    (10H)
	%W    (TM_INV)

	DB    K_F2
	%W    (20H)
	%W    (TM_INV)

	DB    0

UT_U8124:
	DS    UT_U8124+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8124+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8124+OU_X-$
	DB    12,6,3,1
	DS    UT_U8124+OU_HLP-$
	%W    (0)
	DS    UT_U8124+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8124+OU_A_RD-$
	%W    (UR_Mi)		; A_RD !!!!!!!!!!!!!!!!!
	%W    (TM_STP1)		; A_WR
	DS    UT_U8124+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (SAMPNUM)		; DP
	DS    UT_U8124+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (C_SAMPLAST)	; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8125:
	DS    UT_U8125+OU_VEVJ-$
	DB    2
	DW    BFL_EV
	DS    UT_U8125+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8125+OU_X-$
	DB    12,7,3,1
	DS    UT_U8125+OU_HLP-$
	%W    (0)
	DS    UT_U8125+OU_SFT-$
	%W    (UT_SF8125)
	DS    UT_U8125+OU_A_RD-$
	%W    (STP_PSR)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U8125+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U8125+OU_BF_S-$
	%W    (UT_US8125)
	DS    UT_U8125+OU_BF_P-$
	%W    (0)
	DS    UT_U8125+OU_BF_F-$
	%W    (0)
	DS    UT_U8125+OU_BF_T-$
	DB    0
	DB    '_',0
	DB    'A',0
	DB    1
	DB    '_',0
	DB    'B',0
	DB    2
	DB    '_',0
	DB    'C',0
	DB    -1

UT_US8125:DB  'St+ St-  === ===',0

UT_SF8125:
	DB    K_F1
	%W    (0)
	%W    (TM_SUP)

	DB    K_F2
	%W    (0)
	%W    (TM_SDO)

	DB    K_FIX
	%W    (0)
	%W    (TM_STPHLD)

	DB    0

UT_U8128:
	DS    UT_U8128+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8128+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8128+OU_X-$
	DB    9,8,7,1
	DS    UT_U8128+OU_HLP-$
	%W    (0)
	DS    UT_U8128+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8128+OU_A_RD-$
	%W    (G_SEK_ST)	; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U8128+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U8128+OU_I_F-$
	DB    80H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Ramenko

UT_U8131:
	DS    UT_U8131+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U8131+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8131+OU_X-$
	DB    0,9,7,1
	DS    UT_U8131+OU_HLP-$
	%W    (0)
	DS    UT_U8131+OU_SFT-$
	%W    (UT_SF8131)
	DS    UT_U8131+OU_B_S-$
	%W    (UT_US8131)
	DS    UT_U8131+OU_B_P-$
	%W    (0)
	DS    UT_U8131+OU_B_F-$
	%W    (TM_SEK)
	DS    UT_U8131+OU_B_T-$
	DB    'Handler',0

UT_US8131:DB  'Hld Down Up  Ini',0

UT_SF8131:
	DB    K_F1
	%W    (0)
	%W    (TM_STPHLD)

	DB    K_F2
	%W    (0)
	%W    (TM_ASH_DOWN)

	DB    K_F3
	%W    (0)
	%W    (TM_ASH_UP)

	DB    K_F4
	%W    (0)
	%W    (TM_ASH_HOME)

	DB    0

UT_U8132:
	DS    UT_U8132+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8132+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8132+OU_X-$
	DB    8,9,7,1
	DS    UT_U8132+OU_HLP-$
	%W    (0)
	DS    UT_U8132+OU_SFT-$
	%W    (UT_SF8131)
	DS    UT_U8132+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U8132+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (REG_A+OREG_A+OMR_AP) ; DP
	DS    UT_U8132+OU_I_F-$
	DB    080H		; format I_F
	%W    (0)		; I_L
	%W    (CASH_ZP-CASH_HR)	; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Hloubka zajeti ramenka

UT_U8133: ; Nastaveni hloubky ramenka pro 25 kotouc
	DS    UT_U8133+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8133+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8133+OU_X-$
	DB    10,10,6,1
	DS    UT_U8133+OU_HLP-$
	%W    (0)
	DS    UT_U8133+OU_SFT-$
	%W    (UT_SF8131)
	DS    UT_U8133+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U8133+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (ASH1_BOT1)	; DP
	DS    UT_U8133+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (CASH_ZP-CASH_HR)	; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru


UT_U8134: ; Nastaveni hloubky ramenka pro 80 kotouc
	DS    UT_U8134+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8134+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8134+OU_X-$
	DB    10,11,6,1
	DS    UT_U8134+OU_HLP-$
	%W    (0)
	DS    UT_U8134+OU_SFT-$
	%W    (UT_SF8131)
	DS    UT_U8134+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U8134+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (ASH1_BOT2)	; DP
	DS    UT_U8134+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (CASH_ZP-CASH_HR)	; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

%IF(%WITH_WHEEL80)THEN(

UT_U8135: ; Nastaveni hloubky ramenka pro vyplach
	DS    UT_U8135+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8135+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8135+OU_X-$
	DB    10,12,6,1
	DS    UT_U8135+OU_HLP-$
	%W    (0)
	DS    UT_U8135+OU_SFT-$
	%W    (UT_SF8131)
	DS    UT_U8135+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U8135+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (ASH_WASH)	; DP
	DS    UT_U8135+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (CASH_ZP-CASH_HR)	; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

) FI
%IF(%WITH_WHEEL80)THEN(

; ---------------------------------
; Otaceni ramenka

UT_U8136:
	DS    UT_U8136+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8136+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8136+OU_X-$
	DB    10,13,6,1
	DS    UT_U8136+OU_HLP-$
	%W    (0)
	DS    UT_U8136+OU_SFT-$
	%W    (UT_SF8131)
	DS    UT_U8136+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U8136+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (REG_A+OREG_C+OMR_AP) ; DP
	DS    UT_U8136+OU_I_F-$
	DB    080H		; format I_F
	%W    (-7FFFH)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8137: ; Nastaveni odklonu poloha 1
	DS    UT_U8137+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8137+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8137+OU_X-$
	DB    10,14,6,1
	DS    UT_U8137+OU_HLP-$
	%W    (0)
	DS    UT_U8137+OU_SFT-$
	%W    (UT_SF8131)
	DS    UT_U8137+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U8137+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (ASH_RP1)		; DP
	DS    UT_U8137+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (6000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8138: ; Nastaveni odklonu poloha 2
	DS    UT_U8138+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8138+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8138+OU_X-$
	DB    10,15,6,1
	DS    UT_U8138+OU_HLP-$
	%W    (0)
	DS    UT_U8138+OU_SFT-$
	%W    (UT_SF8131)
	DS    UT_U8138+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U8138+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (ASH_RP2)		; DP
	DS    UT_U8138+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (6000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8139: ; Nastaveni odklonu poloha 3
	DS    UT_U8139+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8139+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8139+OU_X-$
	DB    10,16,6,1
	DS    UT_U8139+OU_HLP-$
	%W    (0)
	DS    UT_U8139+OU_SFT-$
	%W    (UT_SF8131)
	DS    UT_U8139+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U8139+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (ASH_RP3)		; DP
	DS    UT_U8139+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (6000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

) FI

; ---------------------------------
; Teplota

UT_U8151:
	DS    UT_U8151+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8151+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8151+OU_X-$
	DB    9,17,7,1
	DS    UT_U8151+OU_HLP-$
	%W    (0)
	DS    UT_U8151+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8151+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U8151+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TMC1_RD)		; DP
	DS    UT_U8151+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8152:
	DS    UT_U8152+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8152+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8152+OU_X-$
	DB    9,18,7,1
	DS    UT_U8152+OU_HLP-$
	%W    (0)
	DS    UT_U8152+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8152+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U8152+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TMC1_OC)		; DP
	DS    UT_U8152+OU_I_F-$
	DB    82H		; format I_F
	%W    (-30000)		; I_L
	%W    (30000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8153:
	DS    UT_U8153+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8153+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8153+OU_X-$
	DB    9,19,7,1
	DS    UT_U8153+OU_HLP-$
	%W    (0)
	DS    UT_U8153+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8153+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U8153+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TMC1_MC)		; DP
	DS    UT_U8153+OU_I_F-$
	DB    82H		; format I_F
	%W    (-30000)		; I_L
	%W    (30000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Konfigurace IRC kotoucku a test

UT_U8161:
	DS    UT_U8161+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8161+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8161+OU_X-$
	DB    9,20,7,1
	DS    UT_U8161+OU_HLP-$
	%W    (0)
	DS    UT_U8161+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8161+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U8161+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (P_IRC_W)		; DP
	DS    UT_U8161+OU_I_F-$
	DB    80H		; format I_F
	%W    (-127)		; I_L
	%W    (127)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8162:
	DS    UT_U8162+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8162+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8162+OU_X-$
	DB    9,21,7,1
	DS    UT_U8162+OU_HLP-$
	%W    (0)
	DS    UT_U8162+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8162+OU_A_RD-$
	%W    (UR_MRPl)		; A_RD
	%W    (UW_MRPl)	; A_WR
	DS    UT_U8162+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (1)		; DP
	DS    UT_U8162+OU_I_F-$
	DB    0C0H		; format I_F
	%W    (-10000)		; I_L
	%W    (10000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Ulozeni do EEPROM a prasomod

UT_U8190:
	DS    UT_U8190+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U8190+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8190+OU_X-$
	DB    0,22,12,1
	DS    UT_U8190+OU_HLP-$
	%W    (0)
	DS    UT_U8190+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U8190+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U8190+OU_B_P-$
	%W    (EEC_SER)		; Servisni nastaveni
	DS    UT_U8190+OU_B_F-$
	%W    (EEP_WRS)		; Zapis do EEPROM
	DS    UT_U8190+OU_B_T-$
	DB    'Save service',0

%IF (%DEBUG_FL) THEN (
UT_U8199:
	DS    UT_U8199+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U8199+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8199+OU_X-$
	DB    0,23,11,1
	DS    UT_U8199+OU_HLP-$
	%W    (0)
	DS    UT_U8199+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U8199+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U8199+OU_B_P-$
	%W    (0)
	DS    UT_U8199+OU_B_F-$
	%W    (L1)
	DS    UT_U8199+OU_B_T-$
	DB    'Low HW test',0
)FI

; *******************************************************************
; STAND-BY rezim

UT_GR91:DS    UT_GR91+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR91+OGR_BTXT-$
	%W    (UT_GT91)
	DS    UT_GR91+OGR_STXT-$
	%W    (0)
	DS    UT_GR91+OGR_HLP-$
	%W    (0)
	DS    UT_GR91+OGR_SFT-$
	%W    (UT_SF91)
	DS    UT_GR91+OGR_PU-$
	%W    (0)

UT_GT91:DB    '%VERSION',C_NL
	DB    ' (c) PiKRON 2012',0

UT_SF91:DB    K_STBY
	%W    (GL_PWRON)
	%W    (GLOB_RQ23)

	DB    0

END
