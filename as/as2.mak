#   Project file pro automaticky davkovac AS
#         (C) Pisoft 1996

as.obj	: as.asm config.h
	a51 as.asm $(par) debug

#pb_ui.obj : pb_ui.asm
#	a51 pb_ui.asm $(par) debug

as.	: as.obj ..\pblib\pb.lib
	l51 as.obj,..\pblib\pb.lib code(08800H) xdata(8000H) ramsize(100h) ixref

as.hex	: as.
	ohs51 as

	  : as.hex
	com_shex 2 as.hex 8800	
#	sendhex as.hex /p4 /m3 /t2 /g40960 /b16000
#	sendhex as.hex /p4 /m3 /t2 /g36864
#	sendhex as.hex /p4 /m7 /t2 /g34816 /b19200
