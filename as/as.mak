#   Project file pro automaticky davkovac AS
#         (C) Pisoft 1996

as.obj	: as.asm config.h
	a51 as.asm $(par) debug

#pb_ui.obj : pb_ui.asm
#	a51 pb_ui.asm $(par) debug

as.	: as.obj ..\pblib\pb.lib
	l51 as.obj,..\pblib\pb.lib code(08600H) xdata(8000H) ramsize(100h) ixref

as.hex	: as.
	ohs51 as

	  : as.hex
#	sendhex as.hex /p4 /m3 /t2 /g40960 /b16000
#	sendhex as.hex /p4 /m3 /t2 /g36864
#	sendhex as.hex /p3 /m7 /t2 /g34304 /b19200
	unixcmd -d ul_sendhex -m 7 -g 0
	pause
	unixcmd -d ul_sendhex as.hex -m 7 -g 0x8600
