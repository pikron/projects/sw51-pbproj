;********************************************************************
;*                     UI_DEFS.H                                    *
;*     Definice datovych struktur uzivatelskeho interface           *
;*                  Stav ke dni 12.10.1996                          *
;*                      (C) Pisoft 1996                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

; Vyzaduje jiz naincludovany INCH_MMAC

; Dalsi definice

C_NL	EQU	0AH	; prechod na nasledujici radku
C_BS	EQU	08H	; pohyb zpet

; Struktura popisu polozky
OU_LEN	SET	0
%STRUCTM(OU,VEVJ,1)	; instrukce jmp
%STRUCTM(OU,VEV ,2)	; rutina zpracovani udalosti
%STRUCTM(OU,LN  ,1)	; delka zbytku definice
%STRUCTM(OU,R1  ,2)	;
%STRUCTM(OU,R2  ,2)	;
%STRUCTM(OU,MSK ,1)	; masky udalosti
%STRUCTM(OU,X   ,1)	; pocatek polozky
%STRUCTM(OU,Y   ,1)	;
%STRUCTM(OU,SX  ,1)	; velikost polozky
%STRUCTM(OU,SY  ,1)	;
%STRUCTM(OU,HLP ,2)	; ukazatel na help
%STRUCTM(OU,SFT ,2)	; ukazatel na tabulku klaves
%STRUCTM(OU,A_RD,2)	; rutina volana pro nacteni dat
			; pro R1=ET_REFR vraci pouze zmenu
%STRUCTM(OU,A_WR,2)	; rutina volana pro ulozeni dat
%STRUCTM(OU,DPSI,2)	; info pro up todate a sit
%STRUCTM(OU,DP  ,2)	; ukazatel na menena data
; dalsi cast je specificka pro vstup cisel
OU_I_LEN SET	OU_LEN
%STRUCTM(OU_I,F ,1)	; SLxMMDDD - S znamenkove, L - longint
			; DD desetin, pri MM=0 nic, 1 vys*K,
			; 2 vys*K/256, 3 vys*K/65536
%STRUCTM(OU_I,L ,2)	; spodni limit
%STRUCTM(OU_I,H ,2)	; horni limit
%STRUCTM(OU_I,K ,2)	; konstanta nasobeni
%STRUCTM(OU_I,E ,0)	; hlaska pri chybe


; Struktura dat polozky
OUD_LEN	SET   0
%STRUCTM(OUD,LN ,1)	; delka dat
%STRUCTM(OUD,FLG,1)	; priznaky
; dalsi cast je specificka pro vstup cisel
%STRUCTM(OUD,I_V ,4)	; hodnota
%STRUCTM(OUD,I_DP,1)	; desetinna tecka

; Jednotlive bity OUD_FLG
UFB_FOC	EQU	0	; focusovany
UFB_EDV	EQU	1	; uzivatel meni data

UFM_FOC	EQU	1 SHL UFB_FOC
UFM_EDV	EQU	1 SHL UFB_EDV


; Jednotlive udalosti
ET_INIT	EQU	1	; volana po aktivaci polozky
ET_DONE	EQU	2	; deaktivace polozky
ET_GETF	EQU	3	; vybrani polozky pro vstup
			; pri uspechu smaze EV_TYPE
ET_RELF	EQU	4	; ukonceni vybrani
			; pri neochote smaze EV_TYPE
ET_KEY	EQU	5	; stisknuta klavesa
ET_VAL	EQU	6	; kontrola validity a vraceni hodnoty
ET_RQF	EQU	7	; pozaduje fokus pro EV_DATA
ET_RQGR	EQU	8	; pozaduje skupinu EV_DATA
ET_DRAW	EQU	9	; musi se znovu vykreslit
ET_REFR	EQU	10	; refresh hodnot
ET_CNAV	EQU	11	; pohyby cursoru
ET_ERR	EQU	12	; zprava o chybe
ET_HLP	EQU	13	; hledani helpu
ET_RQHLP EQU	14	; pozadavek na vypsani helpu
ET_GLOB EQU	15	; globalni udalost pro modalni smycku

; Pro pohyby cursoru ET_CNAV
ETC_NXT	EQU	1
ETC_PRE	EQU	2

; Priznaky zpracovani udalosti
ETB_FOC	EQU	0	; Udalost pouze pro focusovanou polozku

ETM_FOC	EQU	1 SHL ETB_FOC


; Popis skupiny polozek
OGR_LEN	SET	0
%STRUCTM(OGR,VEVJ,1)	; instrukce jmp
%STRUCTM(OGR,VEV ,2)	; rutina zpracovani udalosti
%STRUCTM(OGR,BTXT,2)	; text v pozadi
%STRUCTM(OGR,STXT,2)	; text spodni radky
%STRUCTM(OGR,HLP ,2)	; ukazatel na help
%STRUCTM(OGR,SFT ,2)	; funkce volane pro jednotlive klavesy
OGR_PU	EQU	OGR_LEN	; Pole ukazatelu na polozky


; Ruzne druhy cursoru
CPT_NOR	EQU	1

%DEFINE	(STATIC_UI_AD) (1)

%*DEFINE (UI_OU2DP(OFS)) (
	MOV   A,UI_AU
	ADD   A,#%OFS
	MOV   DPL,A
	MOV   A,UI_AU+1
	ADDC  A,#0
	MOV   DPH,A
)

%*DEFINE (UI_OUD2DP(OFS)) (
	MOV   A,UI_AD
	ADD   A,#%OFS
	MOV   DPL,A
	MOV   A,UI_AD+1
	ADDC  A,#0
	MOV   DPH,A
)

; Objekt tlacitka
OU_B_LEN SET	OU_A_RD
%STRUCTM(OU_B,S ,2)	; Status line
%STRUCTM(OU_B,P ,2)	; Parametry funkce
%STRUCTM(OU_B,F ,2)	; Volana funkce
%STRUCTM(OU_B,T ,0)	; Text tlacitka

; Multi text
OU_M_LEN SET	OU_LEN
%STRUCTM(OU_M,S ,2)	; Status line
%STRUCTM(OU_M,P ,2)	; Parametry funkce
%STRUCTM(OU_M,F ,2)	; Volana funkce
%STRUCTM(OU_M,T ,0)	; Mozne texty

; Bitove pole
OU_BF_LEN SET	OU_LEN
%STRUCTM(OU_BF,S ,2)	; Status line
%STRUCTM(OU_BF,P ,2)	; Parametry funkce
%STRUCTM(OU_BF,F ,2)	; Volana funkce
%STRUCTM(OU_BF,T ,0)	; Mozne texty
