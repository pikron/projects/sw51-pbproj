;********************************************************************
;*                        PB_UI1.ASM                                *
;*                Uzivatelske rozhrani - rozsirujici funkce	    *
;*                  Stav ke dni 24.04.1997                          *
;*                      (C) Pisoft 1997 Pavel Pisa Praha            *
;********************************************************************

$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_UI_DEFS)
$LIST

EXTRN	NUMBER(K_ENTER)

EXTRN	CODE(xLDR45i,xSVR45i,xMDPDP,SEL_FNC,PRINTi,TSTBR45)
EXTRN	CODE(ERRBEEP)

EXTRN	BIT(D4LINE,FL_REFR,FL_DRAW,FL_DRCP,FL_CMAV)

EXTRN	DATA(UI_AU,UI_AD,EV_TYPE,EV_DATA)

EXTRN	CODE(UI_WR,UI_CLR,GOTOXY,GOTO_A,GOTO_S,SHOWCP)
EXTRN	CODE(JMP_VEV,JMP_VE1,EV_CLR)
EXTRN	CODE(UB_EV,UB_GFT,UB_RFT,UB_GFLG,UB_SHCP,UB_A_RD,UB_A_WR)
EXTRN	CODE(UB_DRAW,UB_DRAW1,UB_WRDPN,UB_WRDP,UB_SFT,UB_DRS1)

PUBLIC	BFL_EV,BFL_XOR,BFL_3ST,BFL_3HL

PB_UI_C SEGMENT CODE
PB_UI_X SEGMENT XDATA
PB_UI_D SEGMENT DATA
PB_UI_B SEGMENT DATA BITADDRESSABLE

RSEG	PB_UI_C

; *******************************************************************
; Bitove pole

BFL_EV:	MOV   A,EV_TYPE
	CJNE  A,#ET_REFR,BFL_E10	; REFRESH
	SJMP  BFL_E16
BFL_E10:CJNE  A,#ET_DRAW,BFL_E20	; DRAW
	CALL  UB_GFLG
	JNB   ACC.UFB_FOC,BFL_E16
BFL_E15:%UI_OU2DP(OU_BF_S)
	CALL  UB_DRS1
BFL_E16:CALL  GOTO_A
	JZ    BFL_E1R
	MOV   R1,EV_TYPE
	CALL  UB_A_RD
	JB    F0,BFL_E19
	JZ    BFL_E18
	JMP   BFL_D10
BFL_E1R:RET
BFL_E18:MOV   A,EV_TYPE
	XRL   A,#ET_REFR
	JZ    BFL_E1R
BFL_E19:MOV   A,#'-'
	JMP   UB_DRAW1
BFL_E20:CJNE  A,#ET_GETF,BFL_E30	; GET FOCUS
	CALL  UB_GFT
	JNZ   BFL_E1R
	CALL  EV_CLR
	CALL  UB_SHCP
	JMP   BFL_E15
BFL_E30:CJNE  A,#ET_RELF,BFL_E40	; RELEASE FOCUS
	%UI_OU2DP(OU_BF_S)
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	ORL   A,R0
	JZ    BFL_E35
	SETB  FL_DRAW
BFL_E35:JMP   UB_RFT
BFL_E40:CJNE  A,#ET_KEY,BFL_E50		; KLAVESA
	MOV   A,EV_DATA
	CJNE  A,#K_ENTER,BFL_E44
	%UI_OU2DP(OU_BF_P)
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R0
	MOV   DPH,A
	ORL   A,R0
	JZ    BFL_E44
	CALL  JMP_VE1
	CALL  EV_CLR
BFL_E44:JMP   UB_SFT
BFL_E50:JMP   UB_EV

BFL_D10:MOV   DPL,UI_AU
	MOV   DPH,UI_AU+1
	MOV   A,#OU_SX
	MOVC  A,@A+DPTR		; OU_SX
	MOV   R6,A
	%UI_OU2DP(OU_BF_T)
	MOV   R7,#0		; Nepreskakuje se
BFL_D20:MOVX  A,@DPTR
	CJNE  A,#-1,BFL_D40
BFL_D30:MOV   A,R6	; Doplneni mezerami
	JZ    BFL_D90
	MOV   A,#' '
	CALL  UI_WR
	DEC   R6
	SJMP  BFL_D30
BFL_D40:INC   DPTR
	CALL  TSTBR45	; Test pritomnosti bitu
	JZ    BFL_D45
	MOV   A,#1
BFL_D45:ORL   A,R7	; Preskoceni vypisu
	MOV   R7,#0
	MOV   R3,A
	MOV   R2,#2
	SJMP  BFL_D60
BFL_D50:CALL  UI_WR
	DEC   R6
BFL_D60:MOVX  A,@DPTR	; Vypis retezce pri R3=0
	INC   DPTR
	JZ    BFL_D70
	CJNE  R3,#0,BFL_D60
	CJNE  A,#C_BS,BFL_D65
	MOV   R7,#0FFH	; Preskocit pristi vypis
	SJMP  BFL_D60
BFL_D65:CJNE  R6,#0,BFL_D50
	SJMP  BFL_D60
BFL_D70:DEC   R3
BFL_D80:DJNZ  R2,BFL_D60
	SJMP  BFL_D20
BFL_D90:RET

; XORuje data registry R23
BFL_XOR:MOV   A,R2
	PUSH  ACC
	MOV   A,R3
	PUSH  ACC
	MOV   R1,#ET_KEY
	CALL  UB_A_RD
	POP   ACC
	XRL   A,R5
	MOV   R5,A
	POP   ACC
	XRL   A,R4
	MOV   R4,A
	JB    F0,BFL_XR9
	CALL  UB_A_WR
BFL_XR9:SETB  FL_REFR
	RET

; Tri stavove tlacitko
; R2 maska do obou byte
BFL_3ST:MOV   A,R2
	PUSH  ACC
	MOV   R1,#ET_KEY
	CALL  UB_A_RD
	POP   ACC
	JB    F0,BFL_3S9
	MOV   R2,A
	MOV   A,R5
	ANL   A,R2
	JNZ   BFL_3S2
BFL_3S1:MOV   A,R5	; M=1, V=0
	ORL   A,R2
	MOV   R5,A
	SJMP  BFL_3S6
BFL_3S2:MOV   A,R4
	ANL   A,R2
	JNZ   BFL_3S4
BFL_3S3:MOV   A,R4	; M=1,V=1
	ORL   A,R2
	MOV   R4,A
	SJMP  BFL_3S8
BFL_3S4:MOV   A,R2	; M=0,V=0
	CPL   A
	ANL   A,R5
	MOV   R5,A
BFL_3S6:MOV   A,R2
	CPL   A
	ANL   A,R4
	MOV   R4,A
BFL_3S8:CALL  UB_A_WR
BFL_3S9:SETB  FL_REFR
	RET

; Tri stavove hi/lo tlacitko
; R2 maska do obou byte
BFL_3HL:MOV   A,R2
	PUSH  ACC
	MOV   R1,#ET_KEY
	CALL  UB_A_RD
	POP   ACC
	JB    F0,BFL_3S9
	MOV   R2,A
	MOV   A,R5
	ANL   A,R2
	JNZ   BFL_3S4	; -> H=0, L=0
	MOV   A,R4
	ANL   A,R2
	JNZ   BFL_3S1	; -> H=1, L=0
	SJMP  BFL_3S3	; -> H=0, L=1

	END
