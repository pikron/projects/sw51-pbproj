;********************************************************************
;*                    PB_AI.ASM                                     *
;*     Aritmetika v pevne radove carce                              *
;*                  Stav ke dni 18.06.1995                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

EXTRN   CODE(INPUTc)

PUBLIC  ADDi,SUBi,NEGi,CMPi,MULi,SMULi,DIVi,DIVi1,MODi,DIVihf
PUBLIC  MULsi,TSTMULO
PUBLIC  SHRi,SHR1i,TSTBR45,TSTBR4A,TSTBR5A
PUBLIC  xLDR23i,xLDR45i,xSVR45i,cLDR23i,DECDPTR,MR45R67
PUBLIC  rLDR23i,O10E4i,xLDR123,xSVR23i,xSVR123
PUBLIC  PRINTi,PRINTiP,OUTi
PUBLIC  INPUTi,INP_POS,Xi,FORMi

$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_TTY)
$LIST

INT_A_C SEGMENT CODE
INT_A_D SEGMENT DATA
INT_A_B SEGMENT DATA BITADDRESSABLE

RSEG INT_A_B

INP_POS:DS    1
DP_FL   BIT   INP_POS.7

RSEG INT_A_D

Xi:     DS    2
FORMi:  DS    1

RSEG INT_A_C

ADDi:   MOV   A,R4            ;:R45:=R45+R23
	ADD   A,R2
        MOV   R4,A
        MOV   A,R5
        ADDC  A,R3
        MOV   R5,A
        RET

SUBi:   CLR   C               ;:R45:=R45-R23
        MOV   A,R4
        SUBB  A,R2
        MOV   R4,A
        MOV   A,R5
        SUBB  A,R3
	MOV   R5,A
	ORL   A,R4
        RET

NEGi:   CLR   C               ;:R45:=-R45
        CLR   A
	SUBB  A,R4
        MOV   R4,A
        CLR   A
        SUBB  A,R5
        MOV   R5,A
	RET

CMPi:   CLR   C               ;?R45>=R23
        MOV   A,R4
        SUBB  A,R2
        JZ    CMPi1
        MOV   A,R5
        SUBB  A,R3
	ORL   A,#1
        RET
CMPi1:  MOV   A,R5
	SUBB  A,R3
	RET

MULsi:  MOV   A,R5            ;:R4567:=R45*R23 znamenkove
	MOV   B,R3
	MUL   AB              ; R5*R3
	MOV   R6,A
	MOV   R7,B
	MOV   A,R5
	JNB   ACC.7,MULsi1
	MOV   A,R6
	SUBB  A,R2
	MOV   R6,A
	MOV   A,R7
	SUBB  A,R3
	MOV   R7,A
	CLR   C
MULsi1: MOV   A,R3
	JNB   ACC.7,MULi1
	MOV   A,R6
	SUBB  A,R4
	MOV   R6,A
	MOV   A,R7
	SUBB  A,R5
	MOV   R7,A
	SJMP  MULi1

MULi: 	MOV   A,R5            ;:R4567:=R45*R23
	MOV   B,R3
	MUL   AB              ; R5*R3
	MOV   R6,A
	MOV   R7,B
MULi1:	MOV   A,R5
	MOV   B,R2
	MUL   AB              ; R5*R2
	MOV   R5,A
	MOV   A,B
	ADD   A,R6
	MOV   R6,A
	CLR   A
	ADDC  A,R7
	MOV   R7,A
	MOV   A,R4
	MOV   B,R3
	MUL   AB              ; R4*R3
	ADD   A,R5
	MOV   R5,A
	MOV   A,B
	ADDC  A,R6
	MOV   R6,A
	CLR   A
	ADDC  A,R7
	MOV   R7,A
	MOV   A,R4
	MOV   B,R2
	MUL   AB              ; R4*R2
	MOV   R4,A
	MOV   A,B
	ADD   A,R5
	MOV   R5,A
	CLR   A
	ADDC  A,R6
	MOV   R6,A
	CLR   A
	ADDC  A,R7
	MOV   R7,A
	RET

TSTMULO:MOV   A,R5
	RLC   A
	MOV   A,R7
	JB    ACC.7,TSTMUL1
	ADDC  A,#0
	ORL   A,R6
	JZ    TSTMULR
	MOV   R4,#0FFH
	MOV   R5,#07FH
	RET
TSTMUL1:ADDC  A,#0FFH
	ANL   A,R6
	INC   A
	JZ    TSTMULR
	MOV   R4,#000H
	MOV   R5,#080H
TSTMULR:RET

MULTENi:MOV   A,#00AH         ;:R456:=R45*10
SMULi:  MOV   R6,A            ;:R456:=R45*A
	MOV   B,R4
	MUL   AB              ; R4*A
        MOV   R4,A
        MOV   A,B
	XCH   A,R5
	MOV   B,R6
        MUL   AB              ; R5*A
        ADD   A,R5
        MOV   R5,A
        MOV   A,B
	ADDC  A,#0
	MOV   R6,A
	RET

DIVihf: MOV   A,R4            ; Poloplovouci deleni
	ORL   A,R5            ; R1 je exponent
	SETB  F0              ; R45*2^R1=R45*2^R1/R23
	JNZ   DIVi0
	CLR   F0
	CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   A,R1
	ADD   A,#-16
	MOV   R1,A
	RET

MODi:   MOV   R0,#080H        ;:R45:=R45 mod R23,R67:=R45/R23
	DB    07FH            ; MOV   R7,#d8

DIVi:   MOV   R0,#000H        ;:R45:=R45/R23
	MOV   R1,#000H
	CLR   F0
DIVi0:	MOV   A,R2
	ORL   A,R3
	JNZ   DIVi1
	SETB  F0
	RET
DIVi1:  MOV   A,R3            ; Vstup s F0=1 pro poloplovouci
        CLR   C               ; deleni, R1 je exponent
DIVi2:  INC   R1
        JB    ACC.7,DIVi3
        MOV   A,R2
        RLC   A
        MOV   R2,A
        MOV   A,R3
        RLC   A
        MOV   R3,A
        SJMP  DIVi2
DIVi3:  CLR   A
        MOV   R6,A
	MOV   R7,A
	MOV   A,R1
	ORL   A,R0
	MOV   R0,A
DIVi4:  JC    DIVi5
	MOV   A,R4
	SUBB  A,R2
	MOV   A,R5
	SUBB  A,R3
	CPL   C
	JNC   DIVi6
DIVi5:  CLR   C
	MOV   A,R4
	SUBB  A,R2
	MOV   R4,A
	MOV   A,R5
	SUBB  A,R3
	MOV   R5,A
	SETB  C
DIVi6:  MOV   A,R6
	RLC   A
	MOV   R6,A
	MOV   A,R7
	RLC   A
	MOV   R7,A
	CLR   C
	MOV   A,R4
	RLC   A
	MOV   R4,A
	MOV   A,R5
	RLC   A
	MOV   R5,A
	JB    F0,DIVi7
	DJNZ  R1,DIVi4
	MOV   A,R0
	XRL   A,#080H
	JNB   ACC.7,SHRi+1
	JC    MR45R67	      ; Zaokrouhleni do CY
	MOV   A,R4
	SUBB  A,R2
	MOV   A,R5
	SUBB  A,R3
	CPL   C
MR45R67:MOV   A,R6
	MOV   R4,A
	MOV   A,R7
	MOV   R5,A
SHRiR:	RET

DIVi7:  DEC   R1
	MOV   A,R7
	JNB   ACC.7,DIVi4
	CLR   F0
	SJMP  MR45R67

SHR1i:  MOV   A,#001H         ; R45:=R45 shr 1
SHRi:   CLR   C               ; R45:=R45 shr A
	JZ    SHRiR
	XCH   A,R5
	RRC   A
	XCH   A,R5
	XCH   A,R4
	RRC   A
	XCH   A,R4
	DEC   A
	SJMP  SHRi

TSTBR45:JB    ACC.3,TSTBR5A   ; Testuje bit A registru R45 vysledek v A
TSTBR4A:ANL   A,#7            ; Testuje bit A registru R4 vysledek v A
	ADD   A,#TSTBR_t-TSTBR4t
	MOVC  A,@A+PC
TSTBR4t:ANL   A,R4
	RET
TSTBR5A:ANL   A,#7            ; Testuje bit A registru R5 vysledek v A
	ADD   A,#TSTBR_t-TSTBR5t
	MOVC  A,@A+PC
TSTBR5t:ANL   A,R5
	RET

TSTBR_t:DB    001h
	DB    002h
	DB    004h
	DB    008h
	DB    010h
	DB    020h
	DB    040h
	DB    080h

xLDR45i:MOVX  A,@DPTR         ;:R45:=x(DPTR)
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
        MOV   R5,A
DECDPTR:INC   DPL             ;:DPTR--
        DJNZ  DPL,xLDR45R
        DEC   DPH
xLDR45R:DEC   DPL
        RET

xLDR123:MOVX  A,@DPTR         ;:R123:=x(DPTR)
        MOV   R1,A
        INC   DPTR
xLDR23i:MOVX  A,@DPTR         ;:R23:=x(DPTR)
        MOV   R2,A
	INC   DPTR
        MOVX  A,@DPTR
	MOV   R3,A
        INC   DPTR
        RET

xSVR45i:MOV   A,R4            ;:x(DPTR):=R45
        MOVX  @DPTR,A
        INC   DPTR
        MOV   A,R5
        MOVX  @DPTR,A
        SJMP  DECDPTR

xSVR123:MOV   A,R1            ;:x(DPTR):R123
        MOVX  @DPTR,A
	INC   DPTR
xSVR23i:MOV   A,R2            ;:x(DPTR):=R23
        MOVX  @DPTR,A
	INC   DPTR
        MOV   A,R3
        MOVX  @DPTR,A
        INC   DPTR
        RET

cLDR23i:CLR    A              ;:c(DPTR):=R45
        MOVC   A,@A+DPTR
        MOV    R2,A
        INC    DPTR
	CLR    A
        MOVC   A,@A+DPTR
        MOV    R3,A
        INC    DPTR
        RET

rLDR23i:MOV   R3,#2           ;:R23:=c(A)
cLDR231:MOV   R2,A
	MOV   A,R0
        INC   R0
        MOVC  A,@A+PC
cLDR23K:DJNZ  R3,cLDR231
        MOV   R3,A
	RET

K10E4i: DB    010H,027H
        DB    0E8H,003H
        DB    064H,000H
K10i:   DB    00AH,000H
K1i:    DB    001H,000H
O10E4i  SET   K10E4i-cLDR23K

KINPi:  DB    0,K_0
	DB    1,K_1
	DB    2,K_2
	DB    3,K_3
	DB    4,K_4
	DB    5,K_5
	DB    6,K_6
	DB    7,K_7
	DB    8,K_8
	DB    9,K_9
	DB    0,0

; Vystup cisla v R5,R4
; ====================
; Vstup :R5,R4 .. cislo integer
;        R7    .. format vystupu cila
;                  SLLLxxDD
;            S   - signed
;            LLL - delka vypisu > 0
;            DD  - pocet desetinych mist
;
; Meni  :vsechny registry
;         nevyuziva pamet
;
; vstup OUTi s F0=1 umoznuje vypsani na DPTR

PRINTiP:CALL  xLDR45i
PRINTi: CLR   F0              ; vystup na LCD display
OUTi:   MOV   R6,#0FFH        ; pocet nutnych znaku vystupu
	MOV   A,R5
        ORL   A,#07FH
	ANL   A,R7
        MOV   R7,A
        JNB   ACC.7,PRINTi1
        CALL  NEGi
        DEC   R6              ; kvuli '-' vystup delsi
PRINTi1:MOV   A,R7
        ANL   A,#003H         ; pocet desetinych mist
        CPL   A
        INC   A
        JZ    PRINTi2
        DEC   R6
        XCH   A,R6            ; desetiny prodluzuji vystup
        ADD   A,R6
        XCH   A,R6
PRINTi2:ADD   A,#4            ; pocet cislic, ktere se nemusi
        MOV   R1,A            ; tisknout
	MOV   R0,#K10E4i-cLDR23K   ; tabulka nasobku 10
PRINTi3:CALL  rLDR23i
        CALL  CMPi
        JNC   PRINTi4
        DJNZ  R1,PRINTi3
        CALL  rLDR23i
PRINTi4:MOV   A,R7
        SWAP  A
        ANL   A,#007H         ; pocet znaku na vystup
        XCH   A,R6
        ADD   A,R6
        CLR   C
        SUBB  A,R1
        JNB   ACC.7,PRINTi5
        MOV   R1,A
        MOV   A,R7
        ANL   A,#003H
        ADD   A,R1
	JNZ   PRINTi7
        MOV   A,#1
PRINTi5:JZ    PRINTi7
        MOV   R1,A
PRINTi6:CALL  OUTC_A
        MOV   A,#' '
        CALL  OUTC_B
        DEC   R6
        DJNZ  R1,PRINTi6
PRINTi7:MOV   A,R7
        ANL   A,#080H
        JZ    PRINTi8
        DEC   R6
        CALL  OUTC_A
        MOV   A,#'-'
        CALL  OUTC_B
PRINTi8:MOV   A,R7
        ANL   A,#003H
        RL    A
        ADD   A,#-(K1i-cLDR23K+4)
        MOV   R1,A
PRINTi9:MOV   A,R1
        ADD   A,R0
        JNZ   PRINT10
        DEC   R6
        CALL  OUTC_A
        MOV   A,#'.'
        CALL  OUTC_B
PRINT10:MOV   R7,#'0'-1
PRINT11:CALL  SUBi
        INC   R7
        JNC   PRINT11
        CALL  ADDi
        CALL  OUTC_A
        MOV   A,R7
        CALL  OUTC_B
        CALL  rLDR23i
        DJNZ  R6,PRINTi9
        RET

OUTC_B: JNB   F0,OUTC_B1
        MOVX  @DPTR,A
        INC   DPTR
OUTC_A1:RET
OUTC_B1:JMP   LCDWR1
OUTC_A: JB    F0,OUTC_A1
        JMP   LCDNBUS

; Vstup cisla z klavesnice
; ========================
;
; Vstup :R7    .. format vystupu cila
;                  SLLLxxDD
;            S   - signed
;            LLL - delka vypisu > 0
;            DD  - pocet desetinych mist
;
; Vystup:R5,R4 .. cislo integer pri F0=0,A=0
;        jinak A=kod stisknute klavesy nebo -1
;
;
; Meni  :vsechny registry
;        INP_POS,FORMi,Xi
;

INPUTiP:CALL  xLDR45i
	CALL  INPSUB2
INPUTi: MOV   INP_POS,R6
	MOV   A,R7
	RL    A
	RL    A
	ANL   A,#00CH
	ORL   A,R7
	ANL   A,#0FCH
	MOV   FORMi,A
	CLR   DP_FL
	CLR   A
	MOV   Xi,A
	MOV   Xi+1,A
	CALL  INPSUB5
	CJNE  A,#K_ENTER,INPUTiS
	SETB  F0
	RET

INPUTiI:CALL  INPSUB3
INPUTi1:CALL  INPSUB2
	CALL  INPSUB5
INPUTiS:CALL  INPSUB3
	JZ    INPUTiI
	CJNE  A,#K_PM,INPUTi2
	MOV   A,FORMi
	JNB   ACC.7,INPUTiN
	CALL  NEGi
	SJMP  INPUTi1

INPUTiN:MOV   A,#K_PM
INPUTi2:CJNE  A,#K_DP,INPUTi3
	SETB  DP_FL
	SJMP  INPUTiI

INPUTi3:CJNE  A,#K_ENTER,INPUTi7
	CLR   F0
	CALL  INPSUB1
	MOV   R0,A
	JZ    INPUTi5
INPUTi4:CALL  INPSUB6
	INC   FORMi
	MOV   A,#-1
	JB    F0,INPSUB3
	DJNZ  R0,INPUTi4
INPUTi5:CALL  INPSUB2
	CLR   A
INPUTi6:
INPSUB3:MOV   R4,Xi
	MOV   R5,Xi+1
	RET

INPUTi7:MOV   R0,#KINPi-cLDR23K
	CALL  INPSUB4
	JB    F0,INPUTi6
INPUTi8:CALL  INPSUB6
	JB    F0,INPUTiI

	JNB   DP_FL,INPUTi9
	CALL  INPSUB1
	JZ    INPUTiI
	INC   FORMi

INPUTi9:MOV   R3,#000H
	MOV   A,R7
	ANL   A,R5
	JNB   ACC.7,INPUT10
	CALL  SUBi
	SJMP  INPUT11
INPUT10:CALL  ADDi
INPUT11:JC    INPUTiI
	MOV   A,R7
	JNB   ACC.7,INPUTi1
	JB    OV,INPUTiI
	JMP   INPUTi1


INPSUB1:MOV   R7,FORMi
	MOV   A,R7
	ANL   A,#003H
	MOV   R1,A
	MOV   A,R7
	ANL   A,#00CH
	RR    A
	RR    A
	CLR   C
	SUBB  A,R1
	RET

INPSUB2:MOV   R7,FORMi
	MOV   Xi,R4
	MOV   Xi+1,R5
	CALL  LCDNBUS
	MOV   A,INP_POS
	ORL   A,#LCD_HOM
	CALL  LCDWCO1
	JMP   PRINTi

INPSUB4:SETB  F0
	MOV   R6,A
INPSU41:CALL  rLDR23i
	JZ    INPSU43
	XRL   A,R6
	JNZ   INPSU41
INPSU42:CLR   F0
INPSU43:MOV   A,R6
	RET

INPSUB5:MOV   A,FORMi
	SWAP  A
	ANL   A,#007H
	MOV   R1,A
	CALL  LCDNBUS
	MOV   A,INP_POS
	ORL   A,#LCD_HOM
	ADD   A,R1
	DEC   A
	CALL  LCDWCO1
	JMP   INPUTc

INPSUB6:CLR   F0
	MOV   R7,FORMi
	MOV   A,R5
	MOV   R1,A
	CALL  MULTENi
	MOV   A,R7
	JNB   ACC.7,INPSU61
	MOV   A,R5
	MOV   C,ACC.7
	XRL   A,R1
	JB    ACC.7,INPSU62
	JNC   INPSU61
	MOV   A,#-009H
	ADD   A,R6
	DB    079H
INPSU61:MOV   A,R6
	JZ    INPSU63
INPSU62:SETB  F0
INPSU63:RET

END
