;********************************************************************
;*                        PB_AL.H                                   *
;*                      Longint aritmetika                          *
;*                  Stav ke dni 10.03.1996                          *
;*                      (C) Pisoft 1996 Pavel Pisa Praha            *
;********************************************************************

EXTRN	CODE(xLDl,xiLDl,xiLDs,xSVl,xiSVl,xiSVs)
EXTRN	CODE(xADDl,xiADDl,xiADDs)
EXTRN	CODE(xSUBl,xiSUBl,xiSUBs,xCMPl,xiCMPl,xiCMPs)
EXTRN	CODE(NEGl,ZERROl,SHRl,xNULl,xNULs)
EXTRN	CODE(NORMli,NORMli1,DENOil)
