%*DEFINE (W (WO)) (
	DB   LOW (%WO),HIGH (%WO)
)

%*DEFINE (LDR67i (WO)) (
	MOV  R6,#LOW  (%WO)
	MOV  R7,#HIGH (%WO)
)

%*DEFINE (LDR45i (WO)) (
	MOV  R4,#LOW  (%WO)
	MOV  R5,#HIGH (%WO)
)

%*DEFINE (LDR23i (WO)) (
	MOV  R2,#LOW  (%WO)
	MOV  R3,#HIGH (%WO)
)

%*DEFINE (LDR01i (WO)) (
	MOV  R0,#LOW  (%WO)
	MOV  R1,#HIGH (%WO)
)

%*DEFINE (LDMDi (DATM,WO)) (
	MOV  %DATM+0,#LOW  (%WO)
	MOV  %DATM+1,#HIGH (%WO)
)

%*DEFINE (LDMIi (DATM,WO)) (
	MOV  R0,#%DATM
	MOV  A,#LOW  (%WO)
	MOV  @R0,A
	INC  R0
	MOV  A,#HIGH (%WO)
	MOV  @R0,A
)

%*DEFINE (LDMXi (XDATM,WO)) (
	MOV  DPTR,#%XDATM
	MOV  A,#LOW  (%WO)
	MOVX @DPTR,A
	INC  DPTR
	MOV  A,#HIGH (%WO)
	MOVX @DPTR,A
)

%*DEFINE (VJMP (ADR)) (
	DB    02H ; JMP
	DW    %ADR
)

%*DEFINE (VCALL (ADR)) (
	DB    12H ; CALL
	DW    %ADR
)

%*DEFINE (STRUCTM (S,M,ML)) (
%(%S)_%M	EQU   %(%S)_LEN
%(%S)_LEN	SET   %(%S)_LEN+%ML
)

%*DEFINE (ADDDPci (CONSTI)) (
	MOV   A,DPL
	ADD   A,#LOW (%CONSTI)
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#HIGH (%CONSTI)
	MOV   DPH,A
)
