;********************************************************************
;*              Seriova komunikace - RSOI.ASM                       *
;*                       Object Interface                           *
;*                  Stav ke dni 10.05.1997                          *
;*                      (C) Pisoft 1997                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_RS232)
$LIST

EXTRN	CODE(xMDPDP,xLDR23i)

PUBLIC	APTR,RS_OPL
PUBLIC	L_APTR,S_APTR,X_APTR,RS_FNOP,RS_FNCM,RS_DOCM
PUBLIC	RS_ENDL,RS_EQUL,RS_ACKL,RS_ACK1
PUBLIC	RSI_INTA,RSI_INT,RSO_INTE,RSO_INT
PUBLIC	RSI_ONOFA,RSI_ONOF,RS_CMDA
PUBLIC	RS_RDYSND,RS_POOL


RS232_C SEGMENT CODE
RS232_X SEGMENT XDATA

RSEG	RS232_X
APTR:	DS    2
RS_OPL:	DS    2

RSEG	RS232_C

; Nacte APTR do DPTR, rusi R0
L_APTR:	MOV   DPTR,#APTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R0
	MOV   DPH,A
	RET

; Ulozi DPTR do APTR, rusi pouze R0
S_APTR: MOV   A,DPL
	MOV   R0,DPH
	MOV   DPTR,#APTR
	MOVX  @DPTR,A
	INC   DPTR
	XCH   A,R0
	MOVX  @DPTR,A
	MOV   DPL,R0
	MOV   DPH,A
	RET

;  Vymeni DPTR a APTR, rusi R0 a R1
X_APTR: MOV   R0,DPL
	MOV   R1,DPH
	MOV   DPTR,#APTR
	MOVX  A,@DPTR
	XCH   A,R0
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R1
	MOVX  @DPTR,A
	MOV   DPL,R0
	MOV   DPH,R1
	RET

; Nalezne podle R7 prislusny list prikazu
RS_FNOP:MOV   DPTR,#RS_OPL
	CALL  xMDPDP
	SJMP  RS_FNO2
RS_FNO1:INC   DPTR
	INC   DPTR
RS_FNO2:MOVX  A,@DPTR
	INC   DPTR
	JZ    RS_FNO3
	XRL   A,R7
	JNZ   RS_FNO1
	JMP   xMDPDP
RS_FNO3:SETB  F0
	RET

; Hleda prikaz [R45] v listu prikazu v DPTR
; vraci DPTR popis+4, R23 jmeno prikazu, R1 delka jmena
;       R6 prvni znak sufixu prikazu kde v popisu '#'
RS_FNCM:
RS_FN04:MOV   A,DPL
	ORL   A,DPH
	JZ    RS_FN98
	PUSH  DPL
	PUSH  DPH
	INC   DPTR
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   R1,#0
RS_FN20:MOV   DPL,R2
	MOV   DPH,R3
	MOV   A,R1
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   DPL,R4
	MOV   DPH,R5
	JZ    RS_FN32
	XRL   A,#'#'
	JZ    RS_FN30
	MOV   A,R1
	MOVC  A,@A+DPTR
	XRL   A,R0
	INC   R1
	JZ    RS_FN20
RS_FN28:POP   DPH
	POP   DPL
RS_FN29:CALL  xMDPDP
	SJMP  RS_FN04
RS_FN30:MOV   A,R1
	MOVC  A,@A+DPTR
	MOV   R6,A
	INC   R1
RS_FN32:MOV   A,R1
	MOVC  A,@A+DPTR
	POP   DPH
	POP   DPL
	CALL  ISALPHANUM
	JZ    RS_FN29
	CJNE  R0,#'#',RS_FN36
	MOV   A,R6
	CALL  ISALPHANUM
	JNZ   RS_FN29
	SJMP  RS_FN38
RS_FN36:MOV   R6,#0
RS_FN38:INC   DPTR
	INC   DPTR
	INC   DPTR
	INC   DPTR
	CLR   F0
	RET
RS_FN98:SETB  F0
	RET

; Provede prikaz, DPTR ukazuje na popis, APTR na op znak
RS_DOCM:MOVX  A,@DPTR
	PUSH  ACC
	INC   DPTR
	JNZ   RS_DOC1
	MOVX  A,@DPTR
	JNZ   RS_DOC2
	POP   ACC
	CLR   A
	INC   DPTR
	RET
RS_DOC1:MOVX  A,@DPTR
RS_DOC2:PUSH  ACC
	INC   DPTR
	RET

RS_ENDL:MOV   R7,#0
	SJMP  RS_ACK1
RS_EQUL:MOV   R7,#'='
	SJMP  RS_ACK1
RS_ACKL:MOV   R7,#'\'
	JNB   RSF_RPLY,RS_ACK1
	SETB  RSF_LNT
RS_ACK1:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#APTR
	MOVX  A,@DPTR
	MOV   R0,A
	ADD   A,#1
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R1,A
	ADDC  A,#0
	MOVX  @DPTR,A
	MOV   DPL,R0
	MOV   DPH,R1
	MOV   A,R7
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
	RET

; Prijem cisla
; [DPTR+0] .. format
; [DPTR+2] .. adresa
; [DPTR+4] .. funkce
RSI_INTA:CALL RS_ACKL
RSI_INT:PUSH  DPL
	PUSH  DPH
	CALL  xLDR23i
	CALL  L_APTR
	CALL  xATOIF
	CALL  S_APTR
	POP   DPH
	POP   DPL
	JB    F0,RSI_INTR
	CALL  xLDR23i
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	INC   DPTR
	JZ    RSI_INT8
	PUSH  DPL	; XDATA
	PUSH  DPH	; x[DPTR] := R45(67)
	MOV   DPH,A
	MOV   DPL,R0
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   A,R3
	JNB   ACC.6,RSI_INT7
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
RSI_INT7:POP  DPH
	POP   DPL
	SJMP  RSI_INT9
RSI_INT8:MOV  A,R0
	JZ    RSI_INT9
	MOV   A,R4	; i[R0] := R45(67)
	MOV   @R0,A
	INC   R0
	MOV   A,R5
	MOV   @R0,A
	MOV   A,R3
	JNB   ACC.6,RSI_INT9
	INC   R0
	MOV   A,R6
	MOVX  @R0,A
	INC   R0
	MOV   A,R7
	MOVX  @R0,A
RSI_INT9:JMP  RS_DOCM
RSI_INTR:RET

; Vyslani cisla
RSO_INTE:CALL RS_EQUL
RSO_INT:PUSH  DPL
	PUSH  DPH
	CALL  xLDR23i
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	INC   DPTR
	JZ    RSO_INT8
	PUSH  DPL	; XDATA
	PUSH  DPH	; R45(67) := x[DPTR]
	MOV   DPH,A
	MOV   DPL,R0
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   A,R3
	JNB   ACC.6,RSO_INT7
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
RSO_INT7:POP  DPH
	POP   DPL
	SJMP  RSO_INT9
RSO_INT8:MOV  A,R0
	JZ    RSO_INT9
	MOV   A,@R0	; R45(67) := i[R0]
	MOV   R4,A
	INC   R0
	MOV   A,@R0
	MOV   R5,A
	MOV   A,R3
	JNB   ACC.6,RSO_INT9
	INC   R0
	MOVX  A,@R0
	MOV   R6,A
	INC   R0
	MOVX  A,@R0
	MOV   R7,A
RSO_INT9:CALL RS_DOCM
	POP   DPH
	POP   DPL
	CALL  xLDR23i
	CALL  L_APTR
	CALL  xITOAF
	CALL  S_APTR
	JB    F0,RSO_INTR
	SETB  RSF_LNT
RSO_INTR:RET

; Prijem jedne cislice
RSI_ONOFA:CALL RS_ACKL
RSI_ONOF:PUSH DPL
	PUSH  DPH
	CALL  L_APTR
	CALL  SKIPSPC
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  S_APTR
	POP   DPH
	POP   DPL
	MOV   A,R5
	CALL  ISALPHANUM
	JZ    RSI_ONOFE
	MOV   A,R4
	CALL  ISNUM
	JNZ   RSI_ONOFE
	JMP   RS_DOCM
RSI_ONOFE:SETB F0
	RET

; Potvrzeni prikazu bez parametru
RS_CMDA:CALL  RS_ACKL
	JMP   RS_DOCM

RS_READYT:DB  'R!',0
RS_FAILT:DB   'FAIL!',0

; Vhodne volat pro informaci o stavu READY
; vstup: ACC = 0  ready
;	 ACC > 0  busy
;	 ACC < 0  error
RS_RDYSND:
	JNB   RSF_RDYR,RS_RSN9
	JB    ACC.7,RS_RSN2
	JNZ   RS_RSN6
RS_RSN2:JNB   RSF_BSYO,RS_RSN9
	MOV   DPTR,#RS_READYT
	JNB   ACC.7,RS_RSN4
	MOV   DPTR,#RS_FAILT
RS_RSN4:CALL  RS_WRL1
	CLR   RSF_BSYO
	MOV   A,#1
	RET
RS_RSN6:SETB  RSF_BSYO
RS_RSN9:CLR   A
	RET

; Zpracovani komunikace pres RS232
RS_POOL:CLR   F0
	CALL  RS_RDLN
	JB    F0,RS_PL20; Input overflow
	JNC   RS_PL05	; ESC received
	CALL  RS_RDLF	; Flush vstupu radky
	JMP   RS_WRFL	; Flush vystupu
RS_PL05:JNZ   RS_PL40
RS_PL19:RET
RS_PL20:CALL  RS_RDLF	; Flush all input
	JMP   RS_RDFL
RS_PL40:CLR   RSF_LNT
	CLR   F0
	CALL  SKIPSPC
	JZ    RS_PL19
	CALL  S_APTR
RS_PL41:MOVX  A,@DPTR
	CALL  ISALPHANUM
	JNZ   RS_PL42
	INC   DPTR
	SJMP  RS_PL41
RS_PL42:CALL  SKIPSPC
	MOV   R7,A	; Znak operace '?', ':', '=', '\'
	CALL  X_APTR
	MOV   R4,DPL
	MOV   R5,DPH
	CALL  RS_FNOP
	JB    F0,RS_PLER0
	CALL  RS_FNCM
	JB    F0,RS_PLER1
	CALL  RS_DOCM
	JB    F0,RS_PLER2
	JNB   RSF_LNT,RS_PL99
RS_PL80:CALL  RS_ENDL
	CALL  RS_WRLI
RS_PL99:ORL   A,#1
	RET

RS_PLER0:MOV  DPTR,#RS_PLET
RS_PLER:CLR   F0
	JMP   RS_WRL1
RS_PLET:DB    'ERROR',0

RS_PLER1:MOV   DPTR,#RS_PLET1
	SJMP  RS_PLER
RS_PLET1:DB    'ERROR - UNKNOWN CMD NAME',0

RS_PLER2:MOV   DPTR,#RS_PLET2
	SJMP  RS_PLER
RS_PLET2:DB    'ERROR - BAD PARAMETERS',0

	END