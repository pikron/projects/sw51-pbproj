;********************************************************************
;*                    uLAN pro 8051 - ULAN.H                        *
;*     Komunikace  RS - 232  a  RS - 485                            *
;*                  Stav ke dni  3. 4.1992                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

EXTRN   CODE(uL_FNC)  ; Vstup funkci site u_Lan
;R0 = 0 Spusti u_Lan podle ostatnich nastavenych dat
;     1 Nastavi ryclost 57.6 kBd/ACC
;     2 Nastavuje adresu podle ACC
;     3 Nastavi pocatek IB na R2 delku IB na R3 a delku OB na R4
;       Kdyz je R2=0 provede se autoinicializace
;     4 Nastavi uL_SBLE=R2 uL_SBCO=R3 uL_SBP=R45
;       Kdyz je R2=0 a R45=0 provede se autoinicializace
;       Musi se volat az po sluzbe 3 a system se musi restartovat 0
;     5 Nastavi skupinu podle ACC
;     6 Spusti komunikaci stejne jako 0, ale nenastavi
;	baud generator, umoznuje vyuzit nestandartni metody
;
;    10 uL_O_OP  Otevre vystupni zpravu pro R4 s Com R5
;    11 uL_WR    Zapise R45 bytu z @DP - Rusi R012345
;    12 uL_O_CL  Uzavre vystupni zpravu
;    13 uL_WRB   Zapise byte z @DP - Rusi R0123
;    14 uL_O_LN  Zkontroluje misto OB pro R45 a vrati
;		 max moznou delku - Rusi R012345
;    15 uL_WRc   Zapise R45 bytu z CODE @DP - Rusi R012345
;    20 uL_I_OP  Otevre vstupni zpravu
;		 vraci R4 Adr a R5 Com
;    21	uL_RD    Nacte R45 bytu na @DP - Rusi R012345
;    22	uL_I_CL  Uzavre vstupni zpravu
;    23	uL_RDB   Nacte byte na @DP - Rusi R0123
;    24 uL_I_LN  Zkontroluje misto IB pro R45 a vrati
;		 max moznou delku - Rusi R012345
;    30 uL_S_OP  Otevre rychly vystup zpravy pro R4 s Com R5
;    31 uL_S_WR  Zapise R45 bytu z @DP - Rusi R012345
;    32 uL_S_CL  Uzavre rychly vystup zpravy
;    33 uL_S_DO  Registrace vlastniho bufferu vystupu
;                zpravy na adr [R45]

EXTRN   CODE(uL_INIT) ; Shodne uL_FNC 0
EXTRN   CODE(uL_O_OP,uL_WR,uL_O_CL,uL_WRB,uL_O_LN,uL_WRc)
EXTRN   CODE(uL_I_OP,uL_RD,uL_I_CL,uL_RDB,uL_I_LN)
EXTRN   CODE(uL_S_OP,uL_S_WR,uL_S_CL)
EXTRN   CODE(uL_O_DO) ; Registrace primeho vystupu z DPTR

EXTRN   CODE(uL_STR)  ; Nutne volat 2x az 25x za 1s - Naprava havarie

EXTRN   XDATA(uL_ADR,uL_GRP,uL_SPD,uL_HBIB,uL_IB_L,uL_OB_L,uL_FORM)
EXTRN   XDATA(uL_SBP,uL_SA,uL_CMD)

EXTRN   DATA(uL_FLG,uL_FLH)

EXTRN   BIT(uLF_ERR,uLF_INE,uLF_KMP)

; Pokud nejsou pouzity vektory
; Vnejsi funkce uL_R_BU je volana po prijeti zpravy do bufferu
; Vnejsi funkce uL_R_CO je volana po prijeti zpravy s PRQ
; V ACC je prijaty prikaz, v R0 je hodnota z CMP_END
; Pokud dany prikaz neni zpracovan, funkce se vrati RET
; Pokud je prikaz zpracovavan snizi se SP o 2 a vraci se
; na navesti S_WAITD

; Pokud jsou pouzity vektory
V_uL_FNC EQU   26H ; Skok na fce ulan
V_uL_ADD EQU   1EH ; Skok pro rozsirene interruptu uLAN
;  A ..   prijaty prikaz
;  R0.0 = nutne vyslat potvrzeni
;  R0.1 = pozadavek na provedeni prikazu
;  R0.3 = data prijata do IB
;  [R23] az [R67-1] prijata data
;  rozpoznane prikazy NAK_CMD | ACK_CMD {S_R0FB...}
;    [SND_BEB .. SND_END|REC_BEG..REC_CME] konci JMP S_WAITD
;  nerozpoznane prikazy konci JMP old vector V_uL_ADD | V_uL_EADD
EXTRN   CODE (ACK_CMD,SND_BEB,SND_CHC,SND_END,S_WAITD,NAK_CMD)
EXTRN   CODE (	      REC_BEG,REC_CHR,REC_END,REC_CME)
EXTRN   CODE (REC_Bi, SND_Bi, REC_Bx, SND_Bx, SND_Bc, S_R0FB, S_EQP)
