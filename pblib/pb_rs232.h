;********************************************************************
;*                     PB_RS232.H                                   *
;*               Include pro komunikaci pres RS232                  *
;*                  Stav ke dni 20. 1.1997                          *
;*                      (C) Pisoft 1997                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

EXTRN	BIT(RSF_LNT,RSF_RPLY,RSF_RDYR,RSF_BSYO)
EXTRN	CODE(RS232_INI,RS_RD,RS_WR,RS_RDFL,RS_WRFL)
EXTRN	CODE(RS_RDLN,RS_WRLN,RS_RDLF,RS_WRL1,RS_WRLI)
EXTRN	CODE(CPC2PS,SKIPSPC,ISALPHANUM,ISNUM,ISALPHA)
EXTRN	CODE(xATOIF,xITOAF)
