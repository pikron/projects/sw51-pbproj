;********************************************************************
;*                    LCP 4000 - LP_MONIT.ASM                       *
;*                       Monitor                                    *
;*                  Stav ke dni 14.08.1992                          *
;*                      (C) Pisoft 1992                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

;$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_AI)
;$LIST

$LIST

PUBLIC  MONITOR,INPUTh,INPUThw,PRINThw,PRINThb,RL4R45,INPUThb

EXTRN   CODE(INPUTc,SEL_FNC,ADDATDP,xMDPDP)
EXTRN   DATA(INP_POS,FORMi)
%IF(%USE_WR_MASK) THEN (
EXTRN   DATA(WR_MASK,WR_UPDA)
)FI

MONIT_C SEGMENT CODE
MONIT_X SEGMENT XDATA

%SVECTOR(020H,MONITOR)

RSEG    MONIT_X

REGS:
R_ACC:  DS    1
R_PSW:  DS    1
R_DPL:  DS    1
R_DPH:  DS    1
R_B:    DS    1
R_R0:   DS    4
R_R4:   DS    4
R_WRMSK:DS    1

CODE_BU:DS    10

ADR_D:  DS    3
DIS_POZ:DS    1
DIS_MAX:DS    1

RSEG    MONIT_C

MONITOR:PUSH  B
        PUSH  DPH
        PUSH  DPL
        PUSH  PSW
        PUSH  ACC
        MOV   B,#5
        MOV   DPTR,#R_ACC
MONIT0: POP   ACC
        MOVX  @DPTR,A
        INC   DPTR
        DJNZ  B,MONIT0
        MOV   B,#8
        CALL  MR0REGB
MONIT1: MOVX  @DPTR,A
        INC   DPTR
        INC   R0
        MOV   A,@R0
        DJNZ  B,MONIT1
%IF(%USE_WR_MASK) THEN (
	MOV   A,WR_MASK
	MOVX  @DPTR,A
	MOV   WR_MASK,#0
)FI
MONIT2: MOV   DPTR,#T_MON1
        CALL  cPRINT
        CALL  INPUTc
        CJNE  A,#K_DP,MONITE1
        SJMP  MONITEX
MONITE1:CALL  INPh_S4
        MOV   A,#LCD_CLR
        CALL  LCDWCOM
        MOV   A,R2
        JNZ   MONIT4
MONITEX:MOV   DPTR,#R_R0+1
        CALL  MR0REGB
        MOV   B,#7
MONIT3: MOVX  A,@DPTR
        INC   DPTR
        INC   R0
        MOV   @R0,A
	DJNZ  B,MONIT3
%IF(%USE_WR_MASK) THEN (
	MOVX  A,@DPTR
	MOV   WR_MASK,A
	MOV   WR_UPDA,#0FFH
)FI
	MOV   DPTR,#R_ACC
        MOV   B,#6
MONITE2:MOVX  A,@DPTR
        PUSH  ACC
        INC   DPTR
        DJNZ  B,MONITE2
        POP   ACC
        MOV   R0,A
        POP   B
        POP   DPH
        POP   DPL
        POP   PSW
        POP   ACC
	RET
MONIT4: CJNE  A,#6,MONIT5
        MOV   DPTR,#T_MON2
        CALL  cPRINT
        CALL  INPUTc
        CJNE  A,#K_DP,MON_R2
MON_R1: JMP   MONIT2
MON_R2: CALL  INPh_S4
        MOV   A,R2
        JZ    MON_R1
        ADD   A,#-4
        JC    MON_R1
        MOV   A,R2
        RL    A
        RL    A
        ADD   A,R2
        MOV   DPTR,#MON_RT1-5
        CALL  ADDATDP
        CALL  MR0REGB
        MOV   R6,#5
MON_R3: CLR   A
        MOVC  A,@A+DPTR
        INC   DPTR
        INC   R0
        MOV   @R0,A
        DJNZ  R6,MON_R3
        MOV   DPL,R4
        MOV   DPH,R5
        CALL  cPRINT
        MOV   A,R3
        MOV   R5,A
        MOV   R3,#6
        MOV   DPTR,#ADR_D
        CALL  xSVR123
        CALL  MEM_MS2
        MOV   R6,#040H
MON_R4: MOV   DPTR,#ADR_D
        CALL  xLDR123
        CALL  MEM_PRR
        CALL  INPUThb
        CJNE  A,#K_DP,MON_R5
        SJMP  MON_R1
MON_R5: CALL  MEM_EDL
        JMP   MON_R4

MONIT5: JNC   MONIT6
MON_M1: MOV   DPTR,#ADR_D+2
        MOVX  @DPTR,A
MON_M2: CALL  MEM_MS1
        CALL  MEM_PRA
        MOV   R6,#7
        CALL  INPUThw
        JB    F0,MON_M3
        MOV   DPTR,#ADR_D
        CALL  xSVR45i
        CLR   A
MON_M3: CJNE  A,#K_DP,MON_M4
        JMP   MONIT2
MON_M4: MOV   R6,#040H
MON_M5: CALL  MEM_PRA
        CALL  INPUThb
        CJNE  A,#K_DP,MON_M6
        SJMP  MON_M3+3
MON_M6: CJNE  A,#K_UP,MON_M7
        JMP   MON_M2
MON_M7: CALL  MEM_EDL
        SJMP  MON_M5

MONIT6: CJNE  A,#7,MONIT9
MONIT7: PUSH  ACC
        MOV   DPTR,#T_MON6
        CALL  cPRINT
        MOV   R6,#7
        CALL  INPUThw
        POP   ACC
        JB    F0,MONITL
        JNZ   MONIT8
        MOV   A,#LOW MONITOR
        PUSH  ACC
        MOV   A,#HIGH MONITOR
        PUSH  ACC
        SJMP  MONIT8+4
MONIT8: POP   ACC
        POP   ACC
        MOV   A,R4
        PUSH  ACC
        MOV   A,R5
        PUSH  ACC
        JMP   MONITEX

MONIT9: CJNE  A,#8,MONITL
        CLR   A
        SJMP  MONIT7

MONITL: JMP   MONIT2

MR0REGB:MOV   A,PSW
        ANL   A,#018H
        XCH   A,R0
        RET

MEM_MS1:MOV   R5,#045H
MEM_MS2:MOV   R4,#0
        MOV   DPTR,#DIS_POZ
        JMP   xSVR45i

MEM_EDL:MOV   R0,A
        MOV   A,R4
        MOV   R7,A
        MOV   DPTR,#ADR_D
        CALL  xLDR123
        CALL  xLDR45i
        JB    F0,MEM_ED2
        MOV   A,R4
        ADD   A,R1
        MOV   R1,A
        JNC   MEM_ED1
        INC   R2
MEM_ED1:MOV   A,R7
        CALL  SVACCcp
        SETB  F0
        MOV   A,#K_RIGHT
        SJMP  MEM_EDL
MEM_ED2:CJNE  R0,#K_ENTER,MEM_ED3
        SJMP  MEM_ERI
MEM_ED3:CJNE  R0,#K_RIGHT,MEM_ED4
MEM_ERI:INC   R4
        CLR   C
        MOV   A,R5
        ANL   A,#03FH
        SUBB  A,R4
        JNZ   MEM_EDR
        DEC   R4
        CALL  INCcp0
MEM_EDR:MOV   DPTR,#ADR_D
        CALL  xSVR123
        MOV   A,R5
        ANL   A,#0C0H
        MOV   R5,A
        MOV   A,R4
        MOVX  @DPTR,A
        RL    A
        ADD   A,R4
        ADD   A,R5
        MOV   R6,A
        MOV   A,R0
        RET
MEM_ED4:CJNE  R0,#K_LEFT,MEM_ED5
        DEC   R4
        CJNE  R4,#0FFH,MEM_EDR
        CALL  DECcp0
        INC   R4
MEM_ED5:SJMP  MEM_EDR

MEM_PRA:MOV   DPTR,#ADR_D
        CALL  xLDR123
MEM_PRT:MOV   A,#LCD_HOM
        CALL  LCDWCOM
        MOV   A,R3
        RL    A
        RL    A
        RL    A
        MOV   DPTR,#MEM_PT1-8
        CALL  ADDATDP
        CALL  cPRINT
        MOV   A,R1
        MOV   R4,A
        MOV   A,R2
        MOV   R5,A
        CALL  PRINThw
MEM_PRR:MOV   DPTR,#DIS_MAX
        MOVX  A,@DPTR
	  ANL   A,#03FH
        MOV   R5,A
	  MOV   A,#C_LIN2
MEM_PRL:CALL  LCDWR
        CALL  LDACCcp
        CALL  INCcp
        CALL  PRINThb
        MOV   A,#' '
        DJNZ  R5,MEM_PRL
        RET

T_MON1: DB    C_CLR, 'M:I1 X2 P3 S4 C5'
        DB    C_LIN2,'Esc0. Reg6 Go7c8',0

T_MON2: DB    C_CLR, 'R0123..1 ADPB..3'
        DB    C_LIN2,'R4567..2 Esc ..0',0

T_MON3: DB    C_CLR, 'R0 R1 R2 R3',0
T_MON4: DB    C_CLR, 'R4 R5 R6 R7',0
T_MON5: DB    C_CLR, 'A PSW DPL H  B',0

T_MON6: DB    C_CLR, 'Adress:',0

MEM_PT1:DB    'IDATA :',0
        DB    'XDATA :',0
        DB    'PDATA :',0
        DB    'SDATA :',0
        DB    'CODE  :',0

MON_RT1:DB    LOW R_R0,HIGH R_R0
        DB    44H
	  DB    LOW T_MON3,HIGH T_MON3,LOW R_R4,HIGH R_R4
        DB    44H
	  DB    LOW T_MON4,HIGH T_MON4,LOW R_ACC,HIGH R_ACC
        DB    45H
	  DB    LOW T_MON5,HIGH T_MON5

; Operace s C pointrem
; =====================
; pointer R123
; typ pameti je v R3
; 1 IDATA, 2 XDATA , 3 PDATA , 4 SDATA , 5 CODE

INCcp0: CJNE  R3,#6,INCcp
        RET
INCcp:  INC   R1
        CJNE  R1,#0,INCcp1
        INC   R2
INCcp1: RET

DECcp0: CJNE  R3,#6,DECcp
        RET
DECcp:  DEC   R1
        CJNE  R1,#0FFH,DECcp1
        DEC   R2
DECcp1: RET

; Nacte do A byte podle R123
; ===========================
; meni :DP

LDACCcp:CJNE  R3,#1,LDACCc1   ; 1 .. IDATA
        MOV   A,@R1
        RET
LDACCc1:CJNE  R3,#3,LDACCc2   ; 3 .. PDATA
        MOVX  A,@R1
        RET
LDACCc2:CJNE  R3,#4,LDACCc3   ; 4 .. SDATA
        MOV   DPTR,#CODE_BU
        MOV   A,#0E5H         ; MOV  A,dir
        MOVX  @DPTR,A
        INC   DPTR
        MOV   A,R1
        MOVX  @DPTR,A
        INC   DPTR
        MOV   A,#022H         ; RET
        MOVX  @DPTR,A
        MOV   DPTR,#CODE_BU
        CLR   A
        JMP   @A+DPTR
LDACCc3:MOV   DPL,R1
        MOV   DPH,R2
        CJNE  R3,#5,LDACCc4   ; 5 .. CODE
        CLR   A
        MOVC  A,@A+DPTR
        RET
LDACCc4:MOVX  A,@DPTR         ; 2 .. XDATA
        RET

; Ulozi byte v A podle R123
; ===========================
; meni :DP

SVACCcp:CJNE  R3,#1,SVACCc1   ; 1 .. IDATA
        MOV   @R1,A
        RET
SVACCc1:CJNE  R3,#3,SVACCc2   ; 3 .. PDATA
        MOVX  @R1,A
        RET
SVACCc2:CJNE  R3,#4,SVACCc3   ; 4 .. SDATA
        MOV   R2,A
        MOV   DPTR,#CODE_BU
        MOV   A,#08AH         ; MOV  dir,R2
        MOVX  @DPTR,A
        INC   DPTR
        MOV   A,R1
        MOVX  @DPTR,A
        INC   DPTR
        MOV   A,#0CAH         ; XCH  R2,A
        MOVX  @DPTR,A
        INC   DPTR
        MOV   A,#022H         ; RET
        MOVX  @DPTR,A
        MOV   DPTR,#CODE_BU
        CLR   A
        JMP   @A+DPTR
SVACCc3:MOV   DPL,R1          ; 2 .. XDATA
        MOV   DPH,R2
        MOVX  @DPTR,A
        RET

; Vstup hexa cisla do R45
; ========================
; meni :vse
; vstup:R7 .. format
;             3 word , 1 byte
;       R6 .. pozice vstupu

INPUThb:MOV   R7,#1
        DB    090H
INPUThw:MOV   R7,#3
INPUTh: MOV   INP_POS,R6
        ORL   INP_POS,#LCD_HOM
        MOV   FORMi,R7
        CLR   A
        MOV   Xi,A
        MOV   Xi+1,A
        CALL  INPh_S1
        CJNE  A,#K_ENTER,INPUTh3
INPUTh1:SETB  F0
        RET
INPUTh2:CALL  INPh_S2
        CALL  INPh_S1
INPUTh3:CLR   F0
        MOV   R4,Xi
        MOV   R5,Xi+1
        CJNE  A,#K_ENTER,INPUTh4
        CLR   A
        RET
INPUTh4:CALL  INPh_S4
        JNB   F0,INPUTh5
        MOV   A,R6
        RET
INPUTh5:MOV   R3,#0
        CALL  RL4R45
        ANL   A,#0F0H
        MOV   R4,A
        CALL  ADDi
        MOV   Xi,R4
        MOV   Xi+1,R5
        JMP   INPUTh2

INPh_S4:MOV   DPTR,#INPh_T1
        MOV   R6,A
        CLR   F0
INPh_S5:CALL  cLDR23i
        JZ    INPUTh1
        ADD   A,R6
        JNZ   INPh_S5
        RET

INPh_S1:CALL  LCDNBUS
        MOV   A,FORMi
        ADD   A,INP_POS
        CALL  LCDWCO1
        JMP   INPUTc

INPh_S2:MOV   R0,FORMi
        CALL  LCDNBUS
        MOV   A,INP_POS
        CALL  LCDWCO1
        CJNE  R0,#1,INPh_S3
        MOV   A,R4
        MOV   R5,A
INPh_S3:INC   R0
        JMP   PRINTh1

INPh_T1:DB    0,-K_0
        DB    1,-K_1
        DB    2,-K_2
        DB    3,-K_3
        DB    4,-K_4
        DB    5,-K_5
        DB    6,-K_6
        DB    7,-K_7
        DB    8,-K_8
        DB    9,-K_9
        DB    0AH,-K_H_A
        DB    0BH,-K_H_B
        DB    0CH,-K_H_C
        DB    0DH,-K_H_D
        DB    0EH,-K_H_E
        DB    0FH,-K_H_F
        DB    0,0


; Hexa vypis cisla v A
; ========================
; meni :R0,R4

PRINThb:XCH   A,R5
        MOV   R4,A
        MOV   R0,#2
        SJMP  PRINTh1

; Hexa vypis cisla v R45
; ========================
; meni :A,R0

PRINThw:MOV   R0,#4
PRINTh1:CALL  LCDNBUS
        MOV   A,R5
        SWAP  A
        ANL   A,#00FH
        ADD   A,#-10
        JNC   PRINTh2
        ADD   A,#7
PRINTh2:ADD   A,#03AH
        CALL  LCDWR1
        CALL  RL4R45
        DJNZ  R0,PRINTh1
        RET


RL4R45: MOV   A,R4
        SWAP  A
        MOV   R4,A
        ANL   A,#0F0H
        XCH   A,R4
        ANL   A,#00FH
        XCH   A,R5
        SWAP  A
        XCH   A,R4
        XRL   A,R4
        XCH   A,R4
        ANL   A,#0F0H
        XCH   A,R5
        ORL   A,R5
        XCH   A,R5
        XRL   A,R4
        MOV   R4,A
        RET

END
