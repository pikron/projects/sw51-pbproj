;********************************************************************
;*                        PB_RPI                                    *
;*                Rutiny PI regulatoru polohy                       *
;*                  Stav ke dni 12.03.1996                          *
;*                      (C) Pisoft 1996 Pavel Pisa Praha            *
;********************************************************************

EXTRN	CODE(xLDR45i,xLDR23i,xSVR45i,CMPi)
EXTRN	CODE(MULi,NEGi)
EXTRN	XDATA(REG_FOD,POS_RQI,SPD_RQI,REG_ERC,MAX_ENE)
EXTRN	XDATA(REG_P,REG_D)
EXTRN	DATA(POS_ACT,SPD_ACT)
EXTRN	BIT(FL_ENOV,FL_RERR,FL_RSP)

PUBLIC  REG_PD

MX_PDEL	EQU   3000	; rozdil polohy, pri kterem se hlasi FL_RERR
C_DPOL  EQU   -25*256	; casova konstantu filtru derivace

PB____C SEGMENT CODE

; PD regulator polohy

RSEG PB____C

REG_PD:
	MOV   DPTR,#POS_RQI	; Generovani POS_DEL = POS_RQI - POS_ACT
	CLR   C
	MOVX  A,@DPTR	; POZ_RQI
	INC   DPTR
	SUBB  A,POS_ACT
	MOV   R2,A
	MOVX  A,@DPTR	; POZ_RQI+1
	INC   DPTR
	SUBB  A,POS_ACT+1
	MOV   R3,A		; R23:=POS_DEL
	ANL   A,#80H
	MOV   R1,A		; znamenko
	MOVX  A,@DPTR	; POZ_RQI+2
	SUBB  A,POS_ACT+2
	CJNE  R1,#80H,RPD_20
	CPL   A
	XCH   A,R2
	CPL   A
	ADD   A,#1
	XCH   A,R2
	XCH   A,R3
	CPL   A
	ADDC  A,#0
	XCH   A,R3
RPD_20:	JNZ   RPD_ERR
	MOV   R4,#LOW  (MX_PDEL+1)
	MOV   R5,#HIGH (MX_PDEL+1)
	CALL  CMPi
	JNC   RPD_30
RPD_ERR:SETB  FL_RERR		; detekovana chyba regulace
	CLR   A
	MOV   R4,A
	MOV   R5,A
	RET

RPD_30: MOV   DPTR,#REG_P	; Generovani M_ENERG = POS_DEL*M_ENERI
	CALL  xLDR45i
	CALL  MULi
	CJNE  R7,#0,RPD_35
%IF (0) THEN (                ; Mala energie z odchylky polohy
	MOV   A,R5
	MOV   R4,A
	MOV   A,R6
	MOV   R5,A
)ELSE(                        ; Velka energie z odchylky polohy
	CJNE  R6,#0,RPD_35
	MOV   A,R5
)FI
	JNB   ACC.7,RPD_37
RPD_35:	MOV   R4,#0FFH
	MOV   R5,#07FH
RPD_37:	CJNE  R1,#80H,RPD_40
	CALL  NEGi
RPD_40:
;	MOV   M_ENERG,R4
;	MOV   M_ENERG+1,R5

	MOV   A,R5
	PUSH  ACC
	MOV   A,R4
	PUSH  ACC

	MOV   DPTR,#REG_FOD	; R45 = REG_FOD * M_DPOL/65536
	CALL  xLDR45i		;       znamenko v F0
	MOV   A,R5
	MOV   C,ACC.7
	MOV   F0,C
	JNC   RPD_42
	CALL  NEGi
RPD_42:	MOV   R2,#LOW (C_DPOL)
	MOV   R3,#HIGH(C_DPOL)
	CALL  MULi
	JNB   FL_RSP,RPD_F2	; Posun pri presnosti FL_RSP
	MOV   A,R6		; znamenko v F0 jednotky R67 zlomky v R5
	MOV   R5,A
	MOV   A,R7
	MOV   R6,A
	MOV   R7,#0
RPD_F2:	CLR   A			; R23 = SPD_ACT-SPD_RQI
	CLR   C			;       zlomky v R1
	MOV   DPTR,#SPD_RQI-1
	MOVX  A,@DPTR	; SPD_RQI-1
	INC   DPTR
	MOV   R1,A
	CLR   A
	SUBB  A,R1
	MOV   R1,A
	MOVX  A,@DPTR	; SPD_RQI
	INC   DPTR
	MOV   R2,A
	MOV   A,SPD_ACT
	SUBB  A,R2
	MOV   R2,A
	MOVX  A,@DPTR	; SPD_RQI+1
	MOV   R3,A
	MOV   A,SPD_ACT+1
	SUBB  A,R3
	MOV   R3,A
	CLR   C
	MOV   A,R1
	JB    F0,RPD_F3		; R56 =  SPD_ACT-POZ_INC/2^24 +
	ADDC  A,R5		;        + M_DFILT * M_DPOL/2^24
	MOV   R4,A		;        zlomky v R4
	MOV   A,R2
	ADDC  A,R6
	MOV   R5,A
	MOV   A,R3
	ADDC  A,R7
	MOV   R6,A
	SJMP  RPD_F4
RPD_F3:	SUBB  A,R5
	MOV   R4,A
	MOV   A,R2
	SUBB  A,R6
	MOV   R5,A
	MOV   A,R3
	SUBB  A,R7
	MOV   R6,A
RPD_F4:	MOV   A,R5		; Zjisteni radu REG_FOD
	RLC   A
	CLR   A
	ADDC  A,R6
	JZ    RPD_F5
	CLR   FL_RSP
	MOV   A,R5
	MOV   R4,A
	MOV   A,R6
	MOV   R5,A
	SJMP  RPD_F6
RPD_F5:	SETB  FL_RSP
RPD_F6:	MOV   DPTR,#REG_FOD	; Ulozeni REG_FOD a radu
	MOV   A,R4
	MOVX  @DPTR,A	; REG_FOD
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A	; REG_FOD+1
	MOV   DPTR,#REG_D
	CALL  xLDR23i
	MOV   A,R5
	MOV   C,ACC.7
	MOV   F0,C
	JNC   RPD_F7
	CALL  NEGi
RPD_F7:	CALL  MULi            ; R45 = -R45*REG_D

%IF (0) THEN (
	JB    FL_RSP,RPD_F8 ; Mala energie z derivace
	MOV   A,R5
	MOV   R4,A
	MOV   A,R6
	MOV   R5,A
	SJMP  RPD_F9
RPD_F8:	MOV   A,R6
	MOV   R4,A
	MOV   A,R7
	MOV   R5,A
	MOV   R7,#0
RPD_F9:	ANL   A,#080H
	ORL   A,R7
)ELSE(
	JNB   FL_RSP,RPD_F9 ; Velka energie z derivace
RPD_F8:	MOV   A,R5
	MOV   R4,A
	MOV   A,R6
	MOV   R5,A
	MOV   R6,#0
RPD_F9:	MOV   A,R5
	ANL   A,#080H
	ORL   A,R7
	ORL   A,R6
)FI
	MOV   C,F0
	JZ    RPD_G1
	MOV   R4,#0FFH
	MOV   R5,#07FH
RPD_G1:	JC    RPD_G2
	CALL  NEGi
RPD_G2:
;	MOV   R2,M_ENERG
;	MOV   R3,M_ENERG+1
;	CALL  ADDi
	POP   ACC
	ADD   A,R4
	MOV   R4,A
	POP   ACC
	ADD   A,R5
	MOV   R5,A
	JNB   OV,RPD_G5
	MOV   C,ACC.7
RPD_G3:	JNC   RPD_G4		; Zajisteni max +- energie pri OV
	MOV   R4,#0FFH		; v CY non SGN
	MOV   R5,#07FH
	SJMP  RPD_G5
RPD_G4:	MOV   R4,#001H
	MOV   R5,#080H
RPD_G5:	RET

	END