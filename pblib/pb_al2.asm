;********************************************************************
;*                        PB_AL2.ASM                                *
;*                      Longint aritmetika                          *
;*                  Stav ke dni 10.10.2000                          *
;*                      (C) Pisoft 1996 Pavel Pisa Praha            *
;********************************************************************

PUBLIC	SHRls,xLDi_PA,xLDl_PA,xLDl_P0,xSVl_PA,xSVi_PA

LINT_A_C SEGMENT CODE

RSEG LINT_A_C

; Posune R4567 znamenkove doprava o ACC

SHRls:	ADD   A,#-8
	JNC   SHRls2
	XCH   A,R7
	MOV   C,ACC.7
	XCH   A,R6
	XCH   A,R5
	XCH   A,R4
	CLR   A
	JNC   SHRls1
	CPL   A
SHRls1:	XCH   A,R7
	SJMP  SHRls
SHRls2:	ADD   A,#8
SHRls3:	JZ    SHRlsR
	XCH   A,R7
	MOV   C,ACC.7
	RRC   A
	XCH   A,R7
	XCH   A,R6
	RRC   A
	XCH   A,R6
	XCH   A,R5
	RRC   A
	XCH   A,R5
	XCH   A,R4
	RRC   A
	XCH   A,R4
	DEC   A
	SJMP  SHRls3
SHRlsR:	RET


; Nacte R4567=x[ACC+DPTR]
xLDl_P0:CLR   A
xLDl_PA:MOV   R4,A
	ADD   A,#3
	MOVC  A,@A+DPTR
	MOV   R7,A
	MOV   A,R4
	ADD   A,#2
	MOVC  A,@A+DPTR
	MOV   R6,A
; Nacte R45=x[ACC+DPTR]
xLDi_PA:MOV   A,R4
	INC   A
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   A,R4
	MOVC  A,@A+DPTR
	XCH   A,R4
	RET

; Ulozi R4567 do x[ACC+DPTR]
xSVl_PA:ADD   A,DPL
	XCH   A,DPL
	PUSH  ACC
	CLR   A
	ADDC  A,DPH
	XCH   A,DPH
	PUSH  ACC
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	POP   ACC
	MOV   DPH,A
	POP   ACC
	MOV   DPL,A
	RET

xSVi_PA:ADD   A,DPL
	XCH   A,DPL
	PUSH  ACC
	CLR   A
	ADDC  A,DPH
	XCH   A,DPH
	PUSH  ACC
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	POP   ACC
	MOV   DPH,A
	POP   ACC
	MOV   DPL,A
	RET

	END
