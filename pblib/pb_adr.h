;********************************************************************
;*                    IC_ADR.H                                      *
;*     Soubor definic fyzickych adres pro KLV                       *
;*                  Stav ke dni 24.03.1991                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************


LCD_INST    XDATA 0FF00H    ; zapis instrukce
LCD_STAT    XDATA 0FF01H    ; cteni statusu
LCD_WDATA   XDATA 0FF02H    ; zapis dat
LCD_RDATA   XDATA 0FF03H    ; cteni dat

;------------------ Ovladani LED diod -------------------------------

LED         XDATA 0FF01H

;------------------ Cteni/zapis do klavesnice -----------------------

KBDWR       XDATA 0FF03H
KBDRD       XDATA 0FF04H

;---------------------- zapis do vystupu ----------------------------

AUX_OUT     XDATA 0FF20H