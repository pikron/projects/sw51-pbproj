;********************************************************************
;*            uLAN object interface - UL_OI.ASM                     *
;*     Komunikace  RS - 232  a  RS - 485                            *
;*                  Stav ke dni 16. 8.2001                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

$INCLUDE(ULAN.H)
$INCLUDE(PB_MMAC.H)
$INCLUDE(PB_AI.H)

%DEFINE (VECTOR_FL) (1)       ; Vyuzivaji se vektory pro skoky v RAM

%IF(%VECTOR_FL)THEN(
EXTRN   CODE(VEC_USR,VEC_SET,VEC_GET)
)FI
EXTRN   CODE(xMDPDP,uL_IDB,uL_IDE,SER_NUM)
EXTRN	CODE(UD_NCS_ADRNVSV)

%IF(%VECTOR_FL)THEN(
%DEFINE	(MOVC_A_DPTR) (MOVX  A,@DPTR)
)ELSE(
%DEFINE	(MOVC_A_DPTR)(
	CLR   A
	MOVC  A,@A+DPTR)
)FI

PUBLIC	UD_INIT,UD_RQ,uLD_RQA,uLD_SSIP,uLD_SCIP,UD_SNST_DEF
PUBLIC  US_INIT,US_INIO,US_INIU
PUBLIC  UV_RD,UV_WR,UV_RDT,UV_WRT,UV_RDOP,UV_WROP,UV_RDCL,UV_WRCL
PUBLIC  OID_IN,OID_OUT,CAL_DPc
PUBLIC  UI_PR,UI_PRAO
PUBLIC  UI_INT,UO_RDRQ,UO_INT,OID_ISTD

UL_OI_C SEGMENT CODE
UL_OI_B SEGMENT DATA BITADDRESSABLE
UL_OI_X SEGMENT XDATA

;=================================================================
; System dynamicke adresace a vysilani statusu

RSEG UL_OI_B

uL_DYFL:DS    1
uLD_RQA	BIT   uL_DYFL.2	; Pozadavek na pripojeni do site
uLD_SSIP BIT  uL_DYFL.3	; Send status in progress
uLD_SCIP BIT  uL_DYFL.4	; Status is changed

RSEG UL_OI_X

UD_INTO:DS    3		; vektor na puvodni V_uL_ADD
UD_SFN:	DS    1		; prave ctena subfunkce
UD_DYSA:DS    1		; adresa serveru adres
UD_STAD:DS    2		; ukazatel na buffer statusu
UD_STLN:DS    1		; delka bufferu

RSEG UL_OI_C

UD_INIT:MOV   DPTR,#UD_STAD	; Dynamicke adresovani
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
    %IF(%VECTOR_FL)THEN(
	MOV   R4,#V_uL_ADD
	CALL  VEC_GET
	MOV   DPTR,#UD_INTO
	CALL  VEC_USR
	MOV   R4,#V_uL_ADD
	MOV   DPTR,#UD_INT
	CALL  VEC_SET
    )FI
	MOV   uL_DYFL,#3	; uLD_RQA
	RET

%IF(%VECTOR_FL)THEN(
UD_INT: CJNE  A,#0C1H,UD_INT1 ; uL_GST
	JMP   UD_SNST
UD_INT1:
%IF(0)THEN(
	CJNE  A,#0F0H,UD_INT9 ; uL_SID
; Vysle svoji identifikaci
	CALL  ACK_CMD
	CALL  SND_BEB
	MOV   R2,#LOW uL_IDB; Vysle svoji identifikaci
	MOV   R3,#HIGH uL_IDB
	MOV   R6,#LOW uL_IDE
	MOV   R7,#HIGH uL_IDE
	MOV   A,R4
	MOV   R5,A
	CALL  SND_Bx
	CALL  SND_END
	JMP   S_WAITD
)FI
UD_INT9:%VJMP (UD_INTO)
)FI

; Rutina vyslani zadosti o prideleni dynamicke adresy

UD_RQ:  JB    uLD_RQA,UD_RQ01
	RET
UD_RQ01:DEC   uL_DYFL
	MOV   DPTR,#UD_DYSA
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#07FH
	CLR   F0
	CALL  uL_O_OP
	MOV   DPTR,#UD_RQC1
	%LDR45i (1)
	CALL  uL_WR
	MOV   DPTR,#SER_NUM
	%LDR45i (4)
	CALL  uL_WR
	CALL  uL_O_CL
	RET

UD_RQC1:DB    0C0H	; Zadost o dynamickou adresu

; Komparace serioveho cisla

UD_SNCMP:
	MOV   DPTR,#UL_TMP
UD_SNCMP1:
	MOV   R0,#0
UD_SNCMP2:
	MOV   R2,DPL
	MOV   R3,DPH
	MOV   DPTR,#SER_NUM
	MOV   A,R0
	MOVC  A,@A+DPTR
	MOV   DPL,R2
	MOV   DPH,R3
	MOV   R2,A
	MOVX  A,@DPTR
	XRL   A,R2
	JNZ   UD_SNCMPR
	INC   DPTR
	INC   R0
	CJNE  R0,#4,UD_SNCMP2
UD_SNCMPR:
	RET

; Rutina zpracuje jiz otevrenou zpravu s CMD=7FH

UD_NCS:	MOV   DPTR,#UL_TMP
	CALL  UL_RDB
	CJNE  A,#0C1H,UD_NC20
	%LDR45i(4)		; NCS 0C1H set address
	MOV   DPTR,#UL_TMP
	CALL  UL_RD
	CALL  UD_SNCMP		; Kontrola serioveho cisla
	JNZ   UD_NCSR
	MOV   DPTR,#UL_TMP
	CALL  UL_RDB
	JB    F0,UD_NCSR
	MOV   DPTR,#uL_ADR
	MOVX  @DPTR,A
	ANL   uL_DYFL,#NOT 7
	SJMP  UD_NCSR
UD_NC20:CJNE  A,#0C2H,UD_NC40
	MOV   R5,#07FH		; NCS 0C2H -> 0C3H
	MOV   DPTR,#UL_BADR	; send identification
	MOVX  A,@DPTR
	MOV   R4,A
	CLR   F0
	CALL  uL_O_OP
	MOV   DPTR,#UD_NCC1
    %IF(%VECTOR_FL)THEN(
	CALL  uL_WRB
    )ELSE(
	%LDR45i (1)
	CALL  uL_WRc
    )FI
	MOV   DPTR,#SER_NUM
	%LDR45i (4)
    %IF(%VECTOR_FL)THEN(
	CALL  uL_WR
    )ELSE(
	CALL  uL_WRc
    )FI
	MOV   DPTR,#uL_IDB
    %IF(%VECTOR_FL)THEN(
UD_NC25:CALL  uL_WRB
	MOVX  A,@DPTR
	JNZ   UD_NC25
	CALL  uL_WRB
    )ELSE(
	CLR   C
	MOV   A,#LOW uL_IDE
	SUBB  A,DPL
	MOV   R4,A
	MOV   A,#HIGH uL_IDE
	SUBB  A,DPH
	MOV   R5,A
	CALL  uL_WRc
    )FI
	CALL  uL_O_CL
UD_NCSR:CLR   F0
	JMP   UL_I_CL
UD_NCC1:DB    0C3H	; Identification reply
UD_NC40:CJNE  A,#0C4H,UD_NCSR
	%LDR45i(4)
	MOV   DPTR,#UL_TMP
	CALL  UL_RD
	CALL  UD_SNCMP		; Kontrola serioveho cisla
	JNZ   UD_NCSR
	; Projektem dodana funkce ulozeni adresy
	CALL  UD_NCS_ADRNVSV
	SJMP  UD_NCSR
	

; Rutina vysilani statusu CMD=0C1H

UD_SNST_DEF:
UD_SNST:MOV   A,R0
	MOV   C,ACC.0
	MOV   F0,C
	CALL  S_EQP
	JZ    SNSTA04
	CALL  S_R0FB
	MOV   A,R0
	ANL   A,#0FCH		; Funkce 0-3, start of cycle 
	JNZ   SNSTA10
	JB    uLD_RQA,SNSTA04	; Snaha o zviditelneni
	INC   uL_DYFL
	JNB   uLD_RQA,SNSTA04
SNSTA02:; ORL   uL_DYFL,#7
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#uL_SA
	MOVX  A,@DPTR
	MOV   DPTR,#UD_DYSA	; Server dynamickych adres
	MOVX  @DPTR,A
	MOV   A,R0
	JNZ   SNSTA03		; Nastavi vlastni adresu
	MOV   DPTR,#uL_ADR	; na nulu pri spatnem SN
	MOVX  @DPTR,A		; v dotazu nebo funkci 0
SNSTA03:POP   DPH
	POP   DPL
SNSTA04:JMP   SNSTAR

SNSTA10:ANL   A,#0F0H
	CJNE  A,#010H,SNSTA04
	MOV   A,R0		; Prikaz cteni udaju
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#UD_SFN
	MOVX  @DPTR,A
	MOV   DPTR,#SER_NUM	; Kontrola serioveho cisla
	MOV   R1,#4
SNSTA11:CALL  S_EQP
	JZ    SNSTA12
	CALL  S_R0FB
	MOVX  A,@DPTR
	XRL   A,R0
	JNZ   SNSTA12
	INC   DPTR
	DJNZ  R1,SNSTA11
SNSTA12:POP   DPH
	POP   DPL
	MOV   R0,#0
	JNZ   SNSTA02		; Nesouhlasi cislo
SNSTA13:MOV   C,F0
	MOV   ACC.0,C
	MOV   R0,A
	CALL  ACK_CMD
	ANL   uL_DYFL,#NOT 7
SNSTA20:CALL  SND_BEB
	MOV   R2,#4		; Vysli seriove cislo
	MOV   R3,#0
SNSTA21:PUSH  DPL
	PUSH  DPH
	MOV   A,R3
	MOV   DPTR,#SER_NUM
	MOVC  A,@A+DPTR
	INC   R3
	POP   DPH
	POP   DPL
	CALL  SND_CHC
	DJNZ  R2,SNSTA21
	JB    uLD_SCIP,SNSTA50
	SETB  uLD_SSIP
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#UD_STAD	; ukazatel na buffer stavu
	MOVX  A,@DPTR		; do R23 a delka do R6
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R6,A
	MOV   DPTR,#UD_SFN	; stavova subfunkce
	MOVX  A,@DPTR
	POP   DPH
	POP   DPL
	CJNE  A,#010H,SNSTA30
	MOV   A,R4		; Vyslani zakladnich udaju
	MOV   R5,A		; no buff wrap
	MOV   A,R6
	JZ    SNSTA50
	CJNE  A,#2,SNSTA28
SNSTA27:CALL  S_R0FB		; vysle 2 byte z UD_STAD
	MOV   A,R0
	MOV   R6,A
	CALL  S_R0FB
	MOV   A,R6
	CALL  SND_CHC ; mod
	MOV   A,R0
	CALL  SND_CHC ; chyby
	SJMP  SNSTA50
SNSTA28:ADD   A,R2
	MOV   R6,A
	CLR   A			; vysle UD_STLN byte z UD_STAD
	ADDC  A,R3
	MOV   R7,A
	CALL  SND_Bx
	SJMP  SNSTA50
SNSTA30:CJNE  A,#011H,SNSTA50
				; Vyslani servisnich udaju

SNSTA50:CALL  SND_END
	CLR   uLD_SSIP
SNSTAR: JMP   S_WAITD
	JMP   NAK_CMD

SND_IDi:MOV   A,@R0
	MOV   R4,A
	INC   R0
	MOV   A,@R0
	MOV   R5,A
	INC   R0
SNDR45i:MOV   A,R4
	CALL  SND_CHC
	MOV   A,R5
	JMP   SND_CHC

;=================================================================
; Objektova komunikace

; Hlavicka definice noveho typu objektu
; OID_N je specificke cislo objektu
; OID_D je ukazatel na popis typu a jmena objektu
;		na teto adrese je 1B celkova delka popisu
;		dale pascalsky (s delkou v 1.byte) string jmena
;		a stringy dalsich popisu
;
%*DEFINE (OID_NEW (OID_N,OID_D)) (
OID_P	SET   OID_T
OID_T	SET   $
	DB   LOW (%OID_N),HIGH (%OID_N)
	DB   LOW (OID_P),HIGH (OID_P)
	DB   LOW (%OID_D),HIGH (%OID_D)
)


RSEG UL_OI_X
UL_BADR:DS    1		; Adresa na kterou vyslat odpoved
UL_BCMD:DS    1		; Sluzba pro kterou vyslat odpoved
UL_SN:  DS    1		; Sekvence number
UL_BSN: DS    1		; Back sekvence number

UL_OID: DS    2		; Identifikace zpracovavaneho objektu
UL_OIDA:DS    13	; Pro definici objektu jmenem

%IF(%VECTOR_FL)THEN(
UV_RD:	DS    3		; Skok na rutinu cteni
UV_WR:	DS    3		; Skok na rutinu zapisu
UV_RDT:	DS    3		; Test delky ke cteni
UV_WRT:	DS    3		; Test delky k zapisu
UV_RDOP:DS    3		; Otevre zpravu pro cteni
UV_WROP:DS    3		; Otevre zpravu pro zapis
UV_RDCL:DS    3		; Uzavre zpravu pro cteni
UV_WRCL:DS    3		; Uzavre zpravu pro zapis
UV_WRc	XDATA uV_WR	; sloucene XDATA/CODE
)FI

UL_TMP: DS    10H	; Pomocny buffer

OID_IN:	DS    2		; Tabulka prijimanych objektu
OID_OUT:DS    2		; Tabulka vysilanych objektu

RSEG UL_OI_C

; Inicializace uL object interface systemu
; vstup: R45 .. OID_IN
;	 R67 .. OID_OUT

US_INIT:CALL  US_INIO
US_INIU:
%IF(%VECTOR_FL)THEN(
	%LDR45i (UL_RD)
	MOV   DPTR,#UV_RD
	CALL  VEC_USR
	%LDR45i (UL_WR)
	MOV   DPTR,#UV_WR
	CALL  VEC_USR
	%LDR45i (UL_I_LN)
	MOV   DPTR,#UV_RDT
	CALL  VEC_USR
	%LDR45i (UL_O_LN)
	MOV   DPTR,#UV_WRT
	CALL  VEC_USR
	%LDR45i (UL_I_OP)
	MOV   DPTR,#UV_RDOP
	CALL  VEC_USR
	%LDR45i (UO_WROP)
	MOV   DPTR,#UV_WROP
	CALL  VEC_USR
	%LDR45i (UL_I_CL)
	MOV   DPTR,#UV_RDCL
	CALL  VEC_USR
	%LDR45i (UL_O_CL)
	MOV   DPTR,#UV_WRCL
	CALL  VEC_USR
	RET
)ELSE(
	RET
UV_RD:	JMP   UL_RD
UV_WR:	JMP   UL_WR
UV_RDT:	JMP   UL_I_LN
UV_WRT:	JMP   UL_O_LN
UV_RDOP:JMP   UL_I_OP
UV_WROP:JMP   UO_WROP
UV_RDCL:JMP   UL_I_CL
UV_WRCL:JMP   UL_O_CL
UV_WRc:	JMP   UL_WRc
)FI

US_INIO:MOV   DPTR,#OID_IN
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	INC   DPTR
	RET

; Rutina testu a prijmu a zpracovani zpravy pres uLan

UI_PR:  CLR   F0
	CLR   uLF_INE
	CALL  uL_I_OP	; vraci R4 Adr a R5 Com
	JNB   F0,UI_PRAO
	CLR   A
	RET
UI_PRAO:MOV   DPTR,#UL_BADR
	MOV   A,R4
	MOVX  @DPTR,A
	SETB  uLF_INE
	CJNE  R5,#7FH,UI_PR11
	JMP   UD_NCS		; Network control services
UI_PR11:CJNE  R5,#10H,UI_PR70
    %IF(%VECTOR_FL)THEN(
	%LDR45i (UL_RD)		; Objektova komunikace
	MOV   DPTR,#UV_RD
	CALL  VEC_USR
	%LDR45i (UL_I_LN)
	MOV   DPTR,#UV_RDT
	CALL  VEC_USR
	%LDR45i (UL_I_CL)
	MOV   DPTR,#UV_RDCL
	CALL  VEC_USR
	%LDR45i (UO_WROP)	; Odpoved take pres uLAN
	MOV   DPTR,#UV_WROP
	CALL  VEC_USR
    )FI
	MOV   DPTR,#UL_BCMD
	%LDR45i (3)
	%VCALL  (UV_RD)
	JB    F0,UI_PR50
UI_PR20:MOV   DPTR,#UL_OID
	%LDR45i (2)
	%VCALL  (UV_RD)
	JB    F0,UI_PR50
	MOV   DPTR,#UL_OID
	CALL  xLDR45i
	ORL   A,R4
	JZ    UI_PR60
	MOV   DPTR,#OID_IN
	CALL  xMDPDP
	CALL  FND_OID
	CALL  JMP_OID
	JNB   F0,UI_PR20
	; Spatny objekt
UI_PR50:CLR   F0
	%VCALL (UV_RDCL)
	SETB  F0
	CLR   A
	RET
UI_PR60:%VCALL (UV_RDCL)
	CLR   A
	RET
UI_PR70:CALL  UL_I_CL
	RET

; Otevre vystup pres uLAN

UO_WROP:
    %IF(%VECTOR_FL)THEN(
	%LDR45i (UL_WR)
	MOV   DPTR,#UV_WR
	CALL  VEC_USR
	%LDR45i (UL_O_LN)
	MOV   DPTR,#UV_WRT
	CALL  VEC_USR
	%LDR45i (UL_O_CL)
	MOV   DPTR,#UV_WRCL
	CALL  VEC_USR
    )FI
	MOV   DPTR,#UL_BADR
	CALL  xLDR45i
	CALL  UL_O_OP	; Pripravi DADR,SADR,COM
	JB    F0,UO_WRO9
	MOV   DPTR,#UL_TMP
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A
	MOV   DPTR,#UL_SN
	MOVX  A,@DPTR
	MOV   DPTR,#UL_TMP+2
	MOVX  @DPTR,A
	MOV   DPTR,#UL_TMP
	%LDR45i (3)
	%VCALL  (UV_WR)	; BCOM,SN,BSN
UO_WRO9:RET

; Cte OID ze vstupu a podle nich vola prikazy pro vystup

UO_RDRQ:%VCALL (UV_WROP)
	JB    F0,UO_RDER
	MOV   DPTR,#UL_OID
	MOVX  A,@DPTR
	ORL   A,#1
	MOVX  @DPTR,A
	%LDR45i (2)
	%VCALL  (UV_WR)	; Vysle identifikaci odpovedi
UO_RD1: MOV   DPTR,#UL_OID
	%LDR45i (2)
	%VCALL  (UV_RD)	; Nacte OID
	JB    F0,UO_RDER
	MOV   DPTR,#UL_OID
	%LDR45i (2)
	%VCALL  (UV_WR)	; Vysle OID
	JB    F0,UO_RDER
	MOV   DPTR,#UL_OID
	CALL  xLDR45i
	ORL   A,R4
	JZ    UO_RDR
	MOV   DPTR,#OID_OUT
	CALL  xMDPDP
	CALL  FND_OID	; Vysle data objektu
	CALL  JMP_OID
	JNB   F0,UO_RD1
	; Spatny objekt
UO_RDER:SETB  F0
	RET
UO_RDR: %VCALL (UV_WRCL)
	RET

FND_OI0:%MOVC_A_DPTR
	MOV   R0,A
	INC   DPTR
	%MOVC_A_DPTR
	MOV   DPH,A
	MOV   DPL,R0
; Nalezne polozku podle typu objektu
; DPTR musi ukazovat na seznam typu objektu
; R45 je typ objektu
; pokud je typ nalezen je vraceno DPTR na popisku+4
; jinak se vraci s F0

FND_OID:MOV   A,DPH
	ORL   A,DPL
	JNZ   FND_OI1
	SETB  F0
	RET
FND_OI1:%MOVC_A_DPTR
	XRL   A,R4
	INC   DPTR
	MOV   R0,A
	%MOVC_A_DPTR
	XRL   A,R5
	INC   DPTR
	ORL   A,R0
	JNZ   FND_OI0
	INC   DPTR
	INC   DPTR
	RET

; Hleda OID zadane ASCII retezcem ve vstupu

FNA_OID:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#UL_OIDA
	%LDR45i (1)
	%VCALL  (UV_RD)
	MOV   DPTR,#UL_OIDA
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#0
	ADD   A,#-13
	JC    FNA_OIE
	INC   DPTR
	%VCALL  (UV_RD)
	JB    F0,FNA_OIE
	POP   DPH
	POP   DPL
	SJMP  FNA_OI2
FNA_OI1:POP   DPH
	POP   DPL
	%MOVC_A_DPTR		; Prechod na nasledujici OID
	MOV   R0,A
	INC   DPTR
	%MOVC_A_DPTR
	MOV   DPH,A
	MOV   DPL,R0
FNA_OI2:MOV   A,DPL
	ORL   A,DPH
	JZ    FNA_OIE1
	INC   DPTR		; Skip OID_N
	INC   DPTR
	PUSH  DPL
	PUSH  DPH
	INC   DPTR		; Skip pointer to next
	INC   DPTR
	%MOVC_A_DPTR		; Nacteni ukazatele na popis
	MOV   R0,A
	INC   DPTR
	%MOVC_A_DPTR
	MOV   DPH,A
	MOV   DPL,R0
	ORL   A,R0		; Chybi popis
	JZ    FNA_OI1
	INC   DPTR		; Skip total length
	%MOVC_A_DPTR		; Porovnani s UL_OIDA
	JZ    FNA_OI1
	MOV   R0,A
	INC   R0
	%LDR45i (UL_OIDA)
FNA_OI3:%MOVC_A_DPTR
	INC   DPTR
	MOV   R1,A
	MOV   R2,DPL
	MOV   R3,DPH
	MOV   DPL,R4
	MOV   DPH,R5
	MOVX  A,@DPTR
	INC   DPTR
	XRL   A,R1
	JNZ   FNA_OI1
	MOV   R4,DPL
	MOV   R5,DPH
	MOV   DPL,R2
	MOV   DPH,R3
	DJNZ  R0,FNA_OI3
	POP   DPH
	POP   DPL
	INC   DPTR
	INC   DPTR
	RET
FNA_OIE:POP   DPH
	POP   DPL
FNA_OIE1:SETB F0
JMP_OI9:RET

; Skok na funkci prislusejici OID
; vstup: DPTR .. ukazuje na popis OID +4

JMP_OID:JB    F0,JMP_OI9
	INC   DPTR
	INC   DPTR
	%MOVC_A_DPTR
	PUSH  ACC
	INC   DPTR
	%MOVC_A_DPTR
	PUSH  ACC
	INC   DPTR
	RET

; Funkce volana pro prijem cisla integer
; DPTR ukazuje na parametry ( adresa a volana fce )
; adresa 0 nic se nezapise
;	 1 az 255 IDATA
;        >=256    XDATA

UI_INT: PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#UL_TMP
	%LDR45i (2)
	%VCALL  (UV_RD)
	MOV   DPTR,#UL_TMP
	CALL  xLDR45i
	POP   DPH
	POP   DPL
	JB    F0,UI_INTR
	%MOVC_A_DPTR
	MOV   R0,A
	INC   DPTR
	%MOVC_A_DPTR
	INC   DPTR
	JZ    UI_INT1
	PUSH  DPL
	PUSH  DPH
	MOV   DPH,A
	MOV   DPL,R0
	CALL  xSVR45i
	POP   DPH
	POP   DPL
	SJMP  UI_INT2
UI_INT1:MOV   A,R0
	JZ    UI_INT2
	MOV   A,R4
	MOV   @R0,A
	INC   R0
	MOV   A,R5
	MOV   @R0,A
UI_INT2:
CAL_DPc:%MOVC_A_DPTR
	MOV   R0,A
	INC   DPTR
	%MOVC_A_DPTR
	ORL   A,R0
	JZ    UI_INT3
	MOV   A,R0
	PUSH  ACC
	%MOVC_A_DPTR
	PUSH  ACC
UI_INT3:INC   DPTR
UI_INTR:RET

; Funkce volana pro vyslani cisla integer
; DPTR ukazuje na parametry ( adresa a volana fce )

UO_INT:	%MOVC_A_DPTR
	MOV   R0,A
	INC   DPTR
	%MOVC_A_DPTR
	INC   DPTR
	JZ    UO_INT1
	PUSH  DPL
	PUSH  DPH
	MOV   DPH,A
	MOV   DPL,R0
	CALL  xLDR45i
	POP   DPH
	POP   DPL
	SJMP  UO_INT2
UO_INT1:MOV   A,R0
	JZ    UO_INT2
	MOV   A,@R0
	MOV   R4,A
	INC   R0
	MOV   A,@R0
	MOV   R5,A
UO_INT2:CALL  CAL_DPc
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#UL_TMP
	CALL  xSVR45i
	MOV   DPTR,#UL_TMP
	%LDR45i (2)
	%VCALL  (UV_WR)
	POP   DPH
	POP   DPL
UO_INTR:RET

; Vystup popisu objektu

UO_DOII:MOV   DPTR,#OID_IN
	SJMP  UO_DOID
UO_DOIO:MOV   DPTR,#OID_OUT
UO_DOID:CALL  xLDR45i
	MOV   DPTR,#UL_TMP+4
	CALL  xSVR45i
	%VCALL (UV_WROP)
	JB    F0,UO_DOIE1
	MOV   DPTR,#UL_OID
	MOVX  A,@DPTR
	ORL   A,#1
	MOVX  @DPTR,A
	%LDR45i (2)
	%VCALL  (UV_WR)	; Vysle identifikaci odpovedi
	JNB   F0,UO_DOI3
UO_DOIE1:JMP  UO_DOIER
UO_DOI3:MOV   DPTR,#UL_OID
	%LDR45i (2)
	%VCALL  (UV_RD)	; Nacte OID
	JB    F0,UO_DOIER
	MOV   DPTR,#UL_OID
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	JNZ   UO_DOI4
	CJNE  R0,#I_AOID,UO_DOI4
	MOV   DPTR,#UL_TMP+4
	CALL  xMDPDP
	CALL  FNA_OID
	JB    F0,UO_DOI3
	MOV   A,DPL
	ADD   A,#LOW (-4)
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#HIGH (-4)
	MOV   DPH,A
	CALL  cLDR23i
	MOV   DPTR,#UL_OID
	CALL  xSVR23i
UO_DOI4:MOV   DPTR,#UL_OID
	%LDR45i (2)
	%VCALL  (UV_WR)	; Vysle OID
	JB    F0,UO_DOIER
	MOV   DPTR,#UL_OID
	CALL  xLDR45i
	ORL   A,R4
	JZ    UO_DOIR
	MOV   DPTR,#UL_TMP+4
	CALL  xMDPDP
	CALL  FND_OID		; Najde popis objektu
	JNB   F0,UO_DOI8
UO_DOI7:MOV   DPTR,#UO_DOI_Z	; Neni informace o typu
	MOV   R4,#1
	CLR   F0
	SJMP  UO_DOI9
UO_DOI8:%MOVC_A_DPTR
	MOV   R0,A
	INC   DPTR
	%MOVC_A_DPTR
	MOV   DPH,A
	ORL   A,R0
	JZ    UO_DOI7		; Typ objektu existuje ale nema popis
	MOV   DPL,R0
	%MOVC_A_DPTR
	INC   A
	MOV   R4,A
UO_DOI9:MOV   R5,#0
	%VCALL (UV_WRc)		; Vyslani popisu objektu
	JNB   F0,UO_DOI3
UO_DOIER:SETB F0
	RET
UO_DOIR:%VCALL (UV_WRCL)
	RET
UO_DOI_Z:DB   0

UO_QOII:MOV   DPTR,#OID_IN
	SJMP  UO_QOID
UO_QOIO:MOV   DPTR,#OID_OUT
UO_QOID:CALL  xLDR45i
	MOV   DPTR,#UL_TMP+4
	CALL  xSVR45i
	%VCALL (UV_WROP)
	JB    F0,UO_DOIER
	MOV   DPTR,#UL_OID
	MOVX  A,@DPTR
	ORL   A,#1
	MOVX  @DPTR,A
	%LDR45i (2)
	%VCALL  (UV_WR)		; Vysle identifikaci odpovedi
	MOV   DPTR,#UL_TMP
	%LDR45i (4)
	%VCALL  (UV_RD)		; Nacte od ktereho
	JB    F0,UO_DOIER	; a kolik OID_N vyslat
	MOV   DPTR,#UL_TMP
	CALL  xLDR45i
	MOV   DPTR,#UL_TMP+4
	CALL  xMDPDP
	MOV   A,R4
	ORL   A,R5
	JZ    UO_QOI2
	CALL  FND_OID		; Nalezne prvni
	JBC   F0,UO_QOIR
UO_QOI1:MOV   A,DPL
	ADD   A,#LOW (-4)
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#HIGH (-4)
	MOV   DPH,A
UO_QOI2:MOV   A,DPL
	ORL   A,DPH
	JZ    UO_QOIR
	%MOVC_A_DPTR
	MOV   R4,A		; Nacte OID_N do R45
	INC   DPTR
	%MOVC_A_DPTR
	MOV   R5,A
	INC   DPTR
	%MOVC_A_DPTR		; Pripravi ukazatel na pokracovani
	PUSH  ACC
	INC   DPTR
	%MOVC_A_DPTR
	PUSH  ACC
	MOV   DPTR,#UL_TMP+2	; Snizeni pozadovaneho poctu
	MOVX  A,@DPTR
	ADD   A,#-1
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#-1
	MOVX  @DPTR,A
	JNC   UO_QOI8
	MOV   DPTR,#UL_OID
	CALL  xSVR45i
	MOV   DPTR,#UL_OID
	%LDR45i (2)
	%VCALL  (UV_WR)		; Vysle OID_N
	POP   DPH
	POP   DPL
	JB    F0,UO_QOIE
	SJMP  UO_QOI2
UO_QOI8:POP   DPH
	POP   DPL
UO_QOIR:MOV   DPTR,#UL_TMP
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#UL_TMP
	%LDR45i (2)
	%VCALL (UV_WR)
	%VCALL (UV_WRCL)
UO_QOIE:RET

; Zkontroluje shodu serioveho cisla
; je-li spatne pozada o novou adresu
UO_SNCHK:%LDR45i(4)
	MOV   DPTR,#UL_TMP
	%VCALL (UV_RD)
	CALL  UD_SNCMP		; Kontrola serioveho cisla
	JNZ   UO_SNC9
	ANL   uL_DYFL,#NOT 7	; uLD_RQA
	RET
UO_SNC9:SETB  F0
	SETB  uLD_RQA
	RET

; Nekolik definic standartnich typu objektu

I_AOID	SET   10	; ASCII zadani typu objektu v I_DOID
I_DOII	SET   12	; Pro nasledujici IN objektu vysle popis
I_DOIO	SET   14	; Pro nasledujici OUT objekty vysle popis
I_QOII	SET   16	; Vysle jeden nebo vice IN OID_N
I_QOIO	SET   18	; Vysle jeden nebo vice OUT OID_N
I_RDRQ  SET   20	; Zavola zpracovani pres OID_OUT
I_SNCHK SET   29	; Kontrola shody serioveho cisla

OID_T	SET   0

%OID_NEW(I_SNCHK,0)
	%W    (UO_SNCHK)

%OID_NEW(I_QOII,0)
	%W    (UO_QOII)

%OID_NEW(I_QOIO,0)
	%W    (UO_QOIO)

%OID_NEW(I_DOII,0)
	%W    (UO_DOII)

%OID_NEW(I_DOIO,0)
	%W    (UO_DOIO)

%OID_NEW(I_RDRQ,0)
	%W    (UO_RDRQ)

OID_ISTD SET   OID_T

	END