;********************************************************************
;*                        PB_RPZ                                    *
;*                Rutiny Z filtroveho regulatoru polohy             *
;*                  Stav ke dni 12.06.1996                          *
;*                      (C) Pisoft 1996 Pavel Pisa Praha            *
;********************************************************************

EXTRN	CODE(xLDR45i,xLDR23i,xSVR45i,CMPi)
EXTRN	CODE(MULsi,NEGi)
EXTRN	XDATA(REG_FOI,REG_FOD,POS_RQI,REG_ERC,MAX_ENE)
EXTRN	XDATA(REG_P,REG_I,REG_D,REG_S1,REG_S2)
EXTRN	DATA(POS_ACT,SPD_ACT)
EXTRN	BIT(FL_ENOV)

PUBLIC  REG_PZ

PB____C SEGMENT CODE

; PI regulator polohy

RSEG PB____C

REG_PZ:	MOV   DPTR,#REG_FOI	; R01=REG_FOI*REG_I/256
	CALL  xLDR45i
	MOV   DPTR,#REG_I
	MOVX  A,@DPTR
	MOV   R0,A
	MOV   B,R5
	MOV   C,B.7
	MOV   F0,C
	MUL   AB
	XCH   A,R0
	XCH   A,B
	JNB   F0,REG_PZ1
	SUBB  A,B
REG_PZ1:MOV   R1,A
	MOV   A,R4
	MUL   AB
	ADD   A,#0FFH
	ANL   C,F0
	MOV   A,B
	ADDC  A,R0
	MOV   R0,A
	CLR   A
	ADDC  A,R1
	MOV   R1,A

	MOV   DPTR,#REG_FOD	; R23=REG_FOD*REG_D/256
	CALL  xLDR45i
	MOV   DPTR,#REG_D
	MOVX  A,@DPTR
	MOV   R2,A
	MOV   B,R5
	MOV   C,B.7
	MOV   F0,C
	MUL   AB
	XCH   A,R2
	XCH   A,B
	JNB   F0,REG_PZ2
	SUBB  A,B
REG_PZ2:MOV   R3,A
	MOV   A,R4
	MUL   AB
	ADD   A,#0FFH
	ANL   C,F0
	MOV   A,B
	ADDC  A,R2
	MOV   R2,A
	CLR   A
	ADDC  A,R3
	MOV   R3,A

	CLR   C
	MOV   DPTR,#POS_RQI	; R45=POS_RQI-POS_ACT
	MOVX  A,@DPTR
	SUBB  A,POS_ACT
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,POS_ACT+1
	MOV   R5,A
	MOV   B,A
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,POS_ACT+2
	MOV   C,B.7
	JB    ACC.7,REG_PZ3
	ADDC  A,#0
	JZ    REG_PZ4
	MOV   R4,#0FFH
	MOV   R5,#07FH
	SJMP  REG_PZ4
REG_PZ3:ADDC  A,#0
	JZ    REG_PZ4
	MOV   R4,#000H
	MOV   R5,#080H
REG_PZ4:MOV   DPTR,#REG_FOD	; REG_FOD=R45
	CALL  xSVR45i

	CLR   C			; R45=R45-R23
	MOV   A,R4
	SUBB  A,R2
	MOV   R4,A
	MOV   A,R5
	SUBB  A,R3
	MOV   R5,A
	JNB   OV,REG_PZ5
	MOV   R4,#0FFH
	MOV   R5,#07FH
	JB    ACC.7,REG_PZ5
	INC   R4
	INC   R5
REG_PZ5:MOV   DPTR,#REG_P	; R567=R45*REG_P
	MOVX  A,@DPTR
	MOV   R6,A
	MOV   B,R5
	MOV   C,B.7
	MOV   F0,C
	MUL   AB
	XCH   A,R6
	XCH   A,B
	JNB   F0,REG_PZ6
	SUBB  A,B
REG_PZ6:MOV   R7,A
	MOV   A,R4
	MUL   AB
	MOV   R5,A
	MOV   A,B
	ADDC  A,R6
	MOV   R6,A
	CLR   A
	ADDC  A,R7
	MOV   R7,A

	MOV   A,R0		; R45=R567+R01
	ADD   A,R5
	MOV   R4,A
	MOV   A,R1
	MOV   R0,#0
	JNB   ACC.7,REG_PZ7
	DEC   R0
REG_PZ7:ADDC  A,R6
	MOV   R5,A
	MOV   B,A
	MOV   A,R0
	ADDC  A,R7
	MOV   C,B.7
	JB    ACC.7,REG_PZ8
	ADDC  A,#0
	JZ    REG_PZ11
	MOV   R4,#0FFH
	MOV   R5,#07FH
	SJMP  REG_PZ11
REG_PZ8:ADDC  A,#0
	JZ    REG_PZ11
	MOV   R4,#000H
	MOV   R5,#080H
REG_PZ11:MOV  DPTR,#REG_FOI	; REG_FOI=R45
	CALL  xSVR45i

	MOV   DPTR,#MAX_ENE
	CALL  xLDR23i
	MOV   A,R5
	JB    ACC.7,REG_PZ13
	ORL   A,R4
	JZ    REG_PZ16
	MOV   DPTR,#REG_S1	; Pridani praso +
	MOVX  A,@DPTR
	ADD   A,R5
	JB    OV,REG_PZ12
	MOV   R5,A
	CALL  CMPi
	JC    REG_PZ16
REG_PZ12:MOV  A,R2		; Kladne preteceni MAX_ENE
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	SJMP  REG_PZ15
REG_PZ13:MOV  DPTR,#REG_S2	; Pridani praso -
	MOVX  A,@DPTR
	CPL   A
	INC   A
	ADD   A,R5
	JB    OV,REG_PZ14
	MOV   R5,A
	MOV  A,R2
	ADD   A,R4
	MOV   A,R3
	ADDC  A,R5
	JC    REG_PZ16
REG_PZ14:CLR  C			; Zaporne preteceni MAX_ENE
	CLR   A
	SUBB  A,R2
	MOV   R4,A
	CLR   A
	SUBB  A,R3
	MOV   R5,A
REG_PZ15:MOV  A,SPD_ACT
	ORL   A,SPD_ACT+1
	JNZ   REG_PZ16
	MOV   DPTR,#REG_ERC
	MOVX  A,@DPTR
	INC   A
	CJNE  A,#40,REG_PZ17
	SETB  FL_ENOV
	SJMP  REG_PZ18
REG_PZ16:MOV  DPTR,#REG_ERC
	CLR   A
REG_PZ17:MOVX @DPTR,A
REG_PZ18:RET

	END