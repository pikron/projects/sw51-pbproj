;********************************************************************
;*                        MR_PIDB                                   *
;*                Rutiny PID multi regulatoru polohy - vektor       *
;*                  Stav ke dni 14.09.1996                          *
;*                      (C) Pisoft 1996 Pavel Pisa Praha            *
;********************************************************************

EXTRN	DATA(MR_BAS)
EXTRN	CODE(MR_PIDP)

PUBLIC  MR_PID

MR____C SEGMENT CODE

; PI regulator polohy

RSEG MR____C

MR_PID:	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	JMP   MR_PIDP

	END