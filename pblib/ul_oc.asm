;********************************************************************
;*            Rizeni pres uLan OI - UL_OC.ASM                       *
;*                 Dotazove cykly a prikazy                         *
;*                  Stav ke dni 10.02.1997                          *
;*                      (C) Pisoft 1997                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_ULAN)
$INCLUDE(%INCH_UL_OI)
$INCLUDE(%INCH_BREAK)
$LIST

EXTRN	CODE(xMDPDP,ADDATDP)

PUBLIC	UCF_RIP,UCF_RDP
PUBLIC	UC_AIID,UC_TMP,UC_AUPP,UC_NUPP,UC_NUP
PUBLIC	UC_SND,UC_OCDP
PUBLIC	JMP_VOC,UC_CMD,UC_RD,UC_WR
PUBLIC	UC_DC,UC_RDi,UC_RQD,UC_WRi,UC_DRDY,UC_DSND
PUBLIC	FND_NUP,UC_RECL,UC_REOP,UC_REFR,UC_OI

; definice struktury pro popis prenasenych parametru
OC_LEN	SET   0
%STRUCTM(OC,VOCJ,1)	; instrukce jmp
%STRUCTM(OC,VOC ,2)	; rutina zpracovani udalosti !!! DW
%STRUCTM(OC,FLG ,1)	; definicni priznaky
%STRUCTM(OC,IID ,1)	; identifikacni cislo pristroje
%STRUCTM(OC,OID ,2)	; identifikacni cislo objektu
%STRUCTM(OC,DL  ,1)	; delka datove casti bufferu
%STRUCTM(OC,DP  ,2)	; ukazatel na pocatek bufferu (OCD_FLG)
%STRUCTM(OC,UC  ,0)	; delka a prikaz k ziskani a zapisu dat

; zkracene OC pro vysilani prikazu
OC_C_LEN SET OC_OID
%STRUCTM(OC_C,UC  ,0)	; delka a prikaz


; popis priznaku v OC_FLG
BCF_REF	EQU   0		; stale refresovani
BCF_DYW	EQU   1		; pri zapisu se nevysila okamzite
MCF_REF EQU   1 SHL BCF_REF
MCF_DYW EQU   1 SHL BCF_DYW

; popis priznaku v 1.byte bufferu dat OCD_FLG
MCD_DOK	EQU   003H	; nenulova hodnota => data jsou cerstva
BCD_CWR	EQU   2		; data byla prave zmenena a vyslana
BCD_DCH	EQU   3		; data byla zmenena a je nutne je vyslat
BCD_RDO	EQU   4		; data jsou potreba => obnovuj
MCD_CWR	EQU   1 SHL BCD_CWR
MCD_DCH	EQU   1 SHL BCD_DCH
MCD_RDO	EQU   1 SHL BCD_RDO

; jednotlive prikazy pro objekt OC
OCT_RD	EQU   1		; pozadavek na precteni dat
OCT_WR	EQU   2		; pozadavek na zapis dat
OCT_DR	EQU   3		; prisla data z linky
OCT_DSN	EQU   4		; vysli data na linku
OCT_RQD	EQU   5		; vysli zadost o data
OCT_CMD	EQU   6		; vysli prikaz



UL_OC_C SEGMENT CODE
UL_OC_B SEGMENT DATA BITADDRESSABLE
UL_OC_X SEGMENT XDATA

RSEG	UL_OC_B

UC_FLG:	DS    1
UCF_RIP	BIT   UC_FLG.0	; Jiz zapsano I_RDRQ
UCF_RDP	BIT   UC_FLG.1	; Prijmuta zprava s I_RDRQ

RSEG	UL_OC_X

UC_AIID:DS    1		; adresat prave otevrene zpravy nebo -1
UC_TMP:	DS    16
UC_AUPP:DS    2		; ukazatel na dotazovaci seznam
UC_NUPP:DS    2		; ukazatel do seznamu pri zpracovavani
UC_NUP:	DS    2		; ukazatel na zpracovavany popis parametru UP

RSEG	UL_OC_C

; Standardni pocatek objektove zpravy
UC_HSND:DB    11H,0,0

; Otevre pocatek objektove zpravy R5 pro R4
UC_OP:  CLR   F0
	CALL  uL_O_OP
	MOV   DPTR,#UC_HSND
	%LDR45i(3)
	CALL  uL_WR
	RET

; vysle pascalsky string
UC_WRS:	MOVX  A,@DPTR
	INC   DPTR
	MOV   R4,A
	MOV   R5,#0
	JMP   uL_WR

; vysle zpravu s prikazem definovanym na adrese DPTR
UC_SND:	MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#10H
	INC   DPTR
	PUSH  DPL
	PUSH  DPH
	CALL  UC_OP
	POP   DPH
	POP   DPL
	CALL  UC_WRS
	CALL  uL_O_CL
	RET

; Nacte OC_DP do DPTR
; DPTR := [DPTR+OC_DP]
UC_OCDP:MOV   A,#OC_DP
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OC_DP+1
	MOVC  A,@A+DPTR
	MOV   DPL,R0
	MOV   DPH,A
	RET

; ---------------------------------
; Standartni sluzby objektu OC

; Prikaz
UC_CMD:	MOV   R0,#OCT_CMD
	SJMP  JMP_VOC

; Cteni dat
UC_RD:	MOV   R0,#OCT_RD
	SJMP  JMP_VOC

; Zapis dat
UC_WR:	MOV   R0,#OCT_WR
	SJMP  JMP_VOC

; Skok na rutinu OC, sluzba v R0
JMP_VOC:CLR   A
	JMP   @A+DPTR

; Defaultni rutina OC
UC_DC:	CJNE  R0,#OCT_RD,UC_DC10
; Nacte integer hodnotu z bufferu do R45
UC_RDi: CALL  UC_OCDP
	MOVX  A,@DPTR		; Priznaky OCD_FLG
	ORL   A,#MCD_RDO
	MOVX  @DPTR,A
	ANL   A,#MCD_DOK	; data ready ?
	JZ    UC_RDi5
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
	MOV   A,#1
	RET
UC_RDi5:SETB  F0
	RET

UC_DC10:CJNE  R0,#OCT_RQD,UC_DC20
; vysle zadost o data
UC_RQD:	MOV   A,DPL
	ADD   A,#OC_OID
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#0
	MOV   DPH,A
	%LDR45i(2)
	JMP   uL_WR

UC_DC20:CJNE  R0,#OCT_WR,UC_DC30
; Zapise hodnotu R45 do bufferu a vysle data do jednotky
UC_WRi: PUSH  DPL
	PUSH  DPH
	MOV   A,#OC_IID
	MOVC  A,@A+DPTR
	MOV   R1,A
	MOV   A,#OC_DL
	MOVC  A,@A+DPTR
	MOV   R2,A
	MOV   A,#OC_FLG
	MOVC  A,@A+DPTR
	MOV   R3,A
	CALL  UC_OCDP
	MOVX  A,@DPTR	; Priznaky OCD_FLG
	ORL   A,#MCD_CWR; data byla zmenena
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R2
	JZ    UC_WRi3
	MOV   A,PSW
	ANL   A,#018H
	ADD   A,#4
	MOV   R0,A
UC_WRi2:MOV   A,@R0
	MOVX  @DPTR,A
	INC   R0
	INC   DPTR
	DJNZ  R2,UC_WRi2
UC_WRi3:MOV   A,R3
	JNB   BCF_DYW,UC_WRiP
	POP   DPH
	POP   DPL	; zpozdeny zapis
	CALL  UC_OCDP
	MOVX  A,@DPTR	; Priznaky OCD_FLG
	ORL   A,#MCD_DCH; data byla zmenena
	MOVX  @DPTR,A
	RET
UC_WRiP:MOV   A,R1
	MOV   R4,A
	MOV   R5,#10H
	CALL  UC_OP
	POP   DPH
	POP   DPL
	PUSH  DPL
	PUSH  DPH
	MOV   A,DPL
	ADD   A,#OC_OID
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#0
	MOV   DPH,A
	%LDR45i(2)
	CALL  uL_WR
	POP   DPH
	POP   DPL
	MOV   A,#OC_DL
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   R5,#0
	CALL  UC_OCDP
			; Priznaky OCD_FLG
	INC   DPTR
	CALL  uL_WR
	CALL  uL_O_CL
	RET

UC_DC30:CJNE  R0,#OCT_DR,UC_DC40
; zpracovani odpovedi na I_RDRQ
UC_DRDY:MOV   A,#OC_DL
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   R5,#0
	CALL  UC_OCDP
	MOVX  A,@DPTR		; Priznaky OCD_FLG
	ORL   A,#MCD_DOK
	MOVX  @DPTR,A		; nova data prisla
	ANL   A,#MCD_CWR OR MCD_DCH
	JZ    UC_DRD5
	MOV   A,R4		; buffer obsahuje aktualni zapisovana data
	JZ    UC_DRD4
UC_DRD3:MOV   DPTR,#UC_TMP+4
	CALL  UL_RDB
	DJNZ  R4,UC_DRD3
UC_DRD4:RET
UC_DRD5:INC   DPTR
	JMP   uL_RD

UC_DC40:CJNE  R0,#OCT_DSN,UC_DC50
; provedeni zpozdeneho zapisu
UC_DSND:PUSH  DPL
	PUSH  DPH
	MOV   A,DPL
	ADD   A,#OC_OID
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#0
	MOV   DPH,A
	%LDR45i(2)
	CALL  uL_WR
	POP   DPH
	POP   DPL
	MOV   A,#OC_DL
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   R5,#0
	CALL  UC_OCDP
	MOVX  A,@DPTR	; Priznaky OCD_FLG
	ANL   A,#NOT MCD_DCH
	ORL   A,#MCD_CWR
	MOVX  @DPTR,A
	INC   DPTR
	JMP   uL_WR

UC_DC50:SETB  F0
	RET

; ---------------------------------
UDC_RE1:%W    (I_RDRQ)
UDC_RE2:%W    (0)

; Hleda k objetku OID pristroje UC_AIID jeho popis UP
; vstup:  R45   ... OID objektu
; vystup: DPTR  ... UP popis objektu
;	  ACC=0 ... OID nenalezen
FND_NUP:MOV   DPTR,#UC_AIID	; identifikace pristroje
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   DPTR,#UC_AUPP
FND_NU0:CALL  xMDPDP
FND_NU1:MOVX  A,@DPTR
	INC   DPTR
	MOV   R0,A
	MOVX  A,@DPTR
	INC   DPTR
	MOV   R1,A
	ORL   A,R0
	JZ    FND_NU9
	CJNE  R1,#0FFH,FND_NU3
	CJNE  R0,#0FFH,FND_NU3
	SJMP  FND_NU0
FND_NU3:PUSH  DPL
	PUSH  DPH
	MOV   DPL,R0
	MOV   DPH,R1
	MOV   A,#OC_IID		; ? R3 == OC_IID
	MOVC  A,@A+DPTR
	XRL   A,R3
	JNZ   FND_NU8
	MOV   A,#OC_OID		; ? R45 == OC_OID
	MOVC  A,@A+DPTR
	XRL   A,R4
	JNZ   FND_NU8
	MOV   A,#OC_OID+1
	MOVC  A,@A+DPTR
	XRL   A,R5
	JNZ   FND_NU8
	DEC   SP
	DEC   SP
	MOV   A,#1
	RET
FND_NU8:POP   DPH
	POP   DPL
	JMP   FND_NU1
FND_NU9:CLR   A
	RET

; Uzavre prave generovanou zpravu pro UC_AIID
; spravne zakonci I_RDRQ
UC_RECL:MOV   DPTR,#UC_AIID
	MOVX  A,@DPTR
	INC   A
	JZ    UC_REC2
	MOV   A,#-1
	MOVX  @DPTR,A
	JNB   UCF_RIP,UC_REC1
	CLR   UCF_RIP
	MOV   DPTR,#UDC_RE2
	%LDR45i(2)
	CALL  UL_WR
UC_REC1:JMP   UL_O_CL
UC_REC2:RET

; Otevre novou objektovou zpravu pro R4, je-li to potreba
UC_REOP:MOV   DPTR,#UC_NUP
	CALL  xMDPDP
	MOV   A,#OC_IID
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   DPTR,#UC_AIID
	MOVX  A,@DPTR
	XRL   A,R4
	JZ    UC_REO9
	MOV   A,R4
	PUSH  ACC
	CALL  UC_RECL
	POP   ACC
	MOV   DPTR,#UC_AIID
	MOVX  @DPTR,A
	MOV   R4,A
	MOV   R5,#10H
	JMP   UC_OP
UC_REO9:RET

; Vytvori dotazy a vysle BCD_DCH podle seznamu UC_AUPP
UC_REFR:CLR   UCF_RIP
	MOV   DPTR,#UC_AIID
	MOV   A,#-1
	MOVX  @DPTR,A
	MOV   DPTR,#UC_AUPP
UC_RE08:CALL  xLDR45i
	MOV   DPTR,#UC_NUPP
	CALL  xSVR45i
UC_RE10:MOV   DPTR,#UC_NUPP
	CALL  xLDR45i
	MOV   A,R4
	ADD   A,#2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	ADDC  A,#0
	MOVX  @DPTR,A
	MOV   DPL,R4
	MOV   DPH,R5
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	CJNE  R5,#0FFH,UC_RE15
	CJNE  R4,#0FFH,UC_RE15
	CALL  UC_RECL		; navazani pres -1
	MOV   DPTR,#UC_NUPP
	CALL  xMDPDP
	SJMP  UC_RE08

UC_RE15:ORL   A,R4
	JNZ   UC_RE20
	JMP   UC_RECL
UC_RE20:MOV   DPTR,#UC_NUP
	CALL  xSVR45i
	MOV   DPL,R4
	MOV   DPH,R5
	MOV   A,#OC_FLG
	MOVC  A,@A+DPTR
	MOV   R3,A	; R3 = OC_FLG
UC_RE41:CALL  UC_OCDP
			; DPTR = OC_DP
	MOVX  A,@DPTR
	JNB   ACC.BCD_DCH,UC_RE51

	CALL  UC_REOP	; vyslani zmenenych dat
	JNB   UCF_RIP,UC_RE45
	MOV   DPTR,#UDC_RE2
	%LDR45i(2)
	CALL  uL_WR
	CLR   UCF_RIP
UC_RE45:MOV   DPTR,#UC_NUP
	CALL  xMDPDP
	MOV   R0,#OCT_DSN
	CALL  JMP_VOC

UC_RE50:MOV   DPTR,#UC_NUP
	CALL  xMDPDP
	MOV   A,#OC_FLG
	MOVC  A,@A+DPTR
	MOV   R3,A	; R3 = OC_FLG
	CALL  UC_OCDP
UC_RE51:JB    ACC.BCD_RDO,UC_RE52
	MOV   A,R3
	JB    ACC.BCF_REF,UC_RE52
	JMP   UC_RE10
UC_RE52:MOVX  A,@DPTR	; vysle zadost o precteni dat
	ANL   A,#NOT (MCD_CWR OR MCD_RDO)
	MOVX  @DPTR,A	;  Priznaky OCD_FLG
	ANL   A,#MCD_DOK
	JZ    UC_RE53
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A	; informuje o starnuti dat v bufferu
UC_RE53:CALL  UC_REOP
	JB    UCF_RIP,UC_RE55
	MOV   DPTR,#UDC_RE1
	%LDR45i(2)
	CALL  uL_WR
	SETB  UCF_RIP
UC_RE55:MOV   DPTR,#UC_NUP
	CALL  xMDPDP
	MOV   R0,#OCT_RQD
	CALL  JMP_VOC
	JMP   UC_RE10

; Zpracovavani prijatych zprav
UC_OI:  JBC   uLF_INE,UC_OI04
	RET
UC_OI04:CLR   F0
	CALL  uL_I_OP	; vraci R4 Adr a R5 Com
	JNB   F0,UC_OIAO
	RET
UC_OIAO:SETB  uLF_INE	; zpracuje jiz otevrenou zpravu
	CJNE  R5,#11H,UC_OI50
	MOV   DPTR,#UC_AIID
	MOV   A,R4
	MOVX  @DPTR,A
	CLR   UCF_RIP
	MOV   DPTR,#UC_TMP
	%LDR45i(3)
	CALL  uL_RD
	MOV   DPTR,#UC_TMP
	%LDR45i(2)
	CALL  uL_RD
	MOV   DPTR,#UC_TMP
	MOVX  A,@DPTR
	CJNE  A,#I_RDRQ+1,UC_OI49
	INC   DPTR
	MOVX  A,@DPTR
	JNZ   UC_OI49
	SETB  UCF_RDP
UC_OI20:MOV   DPTR,#UC_TMP
	%LDR45i(2)
	CALL  uL_RD
	JB    F0,UC_OI45
	MOV   DPTR,#UC_TMP
	CALL  xLDR45i
	ORL   A,R4
	JZ    UC_OI49		; Konec RDRQ
	CALL  FND_NUP
	JZ    UC_OI49		; Input error
	MOV   R0,#OCT_DR
	CALL  JMP_VOC
	JB    F0,UC_OI45
	JMP   UC_OI20
UC_OI45:			; Chyba v datove komunikaci
	CLR   F0
UC_OI49:CALL  uL_I_CL
	RET
UC_OI50:JMP   UI_PRAO

END