;********************************************************************
;*                     UI_DEFS.H                                    *
;*               Include uzivatelskeho interface                    *
;*                  Stav ke dni 12.10.1996                          *
;*                      (C) Pisoft 1996                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

; Vyzaduje jiz naincludovany INCH_MMAC

$INCLUDE(%INCH_UI_DEFS)

; Bitove priznaky
EXTRN	BIT(D4LINE,FL_REFR,FL_DRAW,FL_DRCP,FL_CMAV)

; Datove polozky v DATA
EXTRN	DATA(UI_AU,UI_AD)

; Datove polozky v XDATA
EXTRN	XDATA(UI_AV_X,UI_AV_Y,UI_AV_SX,UI_AV_SY,UI_AV_OX,UI_AV_OY)
EXTRN	XDATA(UI_MV_SX,UI_MV_SY)
EXTRN	XDATA(EV_BUF,GR_ACT)

; Funkce pro tvorbu dalsich objektu
EXTRN	CODE(UI_WR,UI_CLR,GOTOXY,GOTO_A,GOTO_S,SHOWCP)

; Zpracovani udalosti jednotlivych objektu a prace s udalostmi
EXTRN	CODE(GR_EV,UB_EV,UIN_EV,BUT_EV,MUT_EV)
EXTRN	CODE(UR_Mi,UR_MiT,UW_Mi,NULL_A)
EXTRN	CODE(EV_POST,EV_GET,EV_DO,EV_PO23,GR_RQ23)
EXTRN	CODE(UI_ABORT)
