;********************************************************************
;*                    PB_TTY_C.H                                    *
;*     Include file se scankody a ovladanim displaye                *
;*                  Stav ke dni 24.03.1991                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

$INCLUDE(%INCH_LCD)

;********************************************************************

; Scan kody klaves

K_0      EQU   01BH
K_1      EQU   015H
K_2      EQU   018H
K_3      EQU   016H
K_4      EQU   021H
K_5      EQU   024H
K_6      EQU   022H
K_7      EQU   00FH
K_8      EQU   012H
K_9      EQU   010H
K_DP     EQU   01EH
K_PM     EQU   0FFH  ; 00DH ; +/- neni definovano
K_ENTER  EQU   01CH

K_PROG   EQU   011H
K_CTRL   EQU   00DH
K_LEFT   EQU   023H
K_RIGHT  EQU   01FH
K_UP     EQU   00EH
K_DOWN   EQU   020H
K_INS    EQU   017H
K_DEL    EQU   014H
K_MODE   EQU   013H
K_HELP	 EQU   005H
K_LIST   EQU   002H
K_CYCLE  EQU   001H
K_RUN    EQU   00AH
K_END    EQU   009H

K_ZERO   EQU   00CH
K_ABS    EQU   004H
K_UVH    EQU   007H
K_UVL    EQU   008H
K_VIS    EQU   00BH
K_LOFF   EQU   01DH
K_PRET   EQU   01AH
K_MARK   EQU   019H
K_ATTEN  EQU   006H
K_AUX    EQU   003H

K_H_A    EQU   01DH
K_H_B    EQU   01AH
K_H_C    EQU   019H
K_H_D    EQU   017H
K_H_E    EQU   014H
K_H_F    EQU   013H

; Virtualni scankody

KV_V_OK	EQU    071H

;********************************************************************

; Kody led diod

%DEFINE (PROG_FL)  (KBD_FLG.7)
%DEFINE (BEEP_FL)  (LED_FLG.7)
%DEFINE (LVIS_FL)  (LED_FLG.6)
%DEFINE (LHIG_FL)  (LED_FLG.5)
%DEFINE (LHIG_FL)  (LED_FLG.4)
%DEFINE (RUN_FL)   (LED_FLG.3)

%DEFINE (ALRM_FL)  (LED_FLH.0)

EXTRN    CODE(LCDINST,LCDNBUS,LCDWCOM,LCDWCO1,LCDWR,LCDWR1)
EXTRN    CODE(PRINT,PRINTH,xPRINT,cPRINT)

EXTRN    CODE(SCANKEY,TESTKEY)
EXTRN    CODE(KBDSTDB)

EXTRN    CODE(LEDWR)
EXTRN    DATA(LED_FLG,LED_FLH,KBD_FLG)
