;********************************************************************
;*              Seriova komunikace - RSOI.H                         *
;*                       Object Interface                           *
;*                  Stav ke dni 10.05.1997                          *
;*                      (C) Pisoft 1997                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

EXTRN	XDATA(APTR,RS_OPL)
EXTRN	CODE(L_APTR,S_APTR,X_APTR,RS_FNOP,RS_FNCM,RS_DOCM)
EXTRN	CODE(RS_ENDL,RS_EQUL,RS_ACKL,RS_ACK1)
EXTRN	CODE(RSI_INTA,RSI_INT,RSO_INTE,RSO_INT)
EXTRN	CODE(RSI_ONOFA,RSI_ONOF,RS_CMDA)
EXTRN	CODE(RS_RDYSND,RS_POOL)

