;********************************************************************
;*            uLAN object interface - UL_OI.H                       *
;*              Komunikace uLan RS - 485                            *
;*                  Stav ke dni  3. 4.1996                          *
;*                      (C) Pisoft 1996                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

I_AOID	 SET   10	; ASCII zadani typu objektu v I_DOID
I_DOII	 SET   12	; Pro nasledujici IN objektu vysle popis
I_DOIO	 SET   14	; Pro nasledujici OUT objekty vysle popis
I_QOII	 SET   16	; Vysle jeden nebo vice IN OID_N
I_QOIO	 SET   18	; Vysle jeden nebo vice OUT OID_N
I_RDRQ   SET   20	; Zavola zpracovani pres OID_OUT
I_SNCHK  SET   29	; Kontrola shody serioveho cisla
I_STATUS SET   30	; Ziskani stavove informace o pristroji
I_ERRCLR SET   31	; Mazani chyby

; Hlavicka definice noveho typu objektu
; OID_N je specificke cislo objektu
; OID_D je ukazatel na popis typu a jmena objektu
;		na teto adrese je 1B celkova delka popisu
;		dale pascalsky (s delkou v 1.byte) string jmena
;		a stringy dalsich popisu
;
%*DEFINE (OID_NEW (OID_N,OID_D)) (
OID_P	SET   OID_T
OID_T	SET   $
	DB   LOW (%OID_N),HIGH (%OID_N)
	DB   LOW (OID_P),HIGH (OID_P)
	DB   LOW (%OID_D),HIGH (%OID_D)
)

; Textovy popis objektu pro I_DOII a I_DOIO
; AOID		label pro referenci na popis
; OID_ASCII	ASCII jmeno objektu
%*DEFINE (OID_ADES (AOID,OID_ASCII,OID_TYPE)) (
%AOID	SET   $
	DB   %AOID%()_END-$-1
	DB   %AOID%()_TYPE-$-1
	DB   '%OID_ASCII'
%AOID%()_TYPE	SET   $
	DB   %AOID%()_END-$-1
	DB   '%OID_TYPE'
%AOID%()_END	SET   $
)

EXTRN	CODE(US_INIT,US_INIO,US_INIU)
EXTRN	XDATA(UV_RD,UV_WR,UV_RDT,UV_WRT,UV_RDOP)
EXTRN	XDATA(UV_WROP,UV_RDCL,UV_WRCL)
EXTRN	XDATA(OID_IN,OID_OUT)
EXTRN	CODE(UI_PR,UI_PRAO)
EXTRN	CODE(UI_INT,UO_RDRQ,UO_INT,OID_ISTD)