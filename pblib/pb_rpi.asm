;********************************************************************
;*                        PB_RPI                                    *
;*                Rutiny PI regulatoru polohy                       *
;*                  Stav ke dni 12.03.1996                          *
;*                      (C) Pisoft 1996 Pavel Pisa Praha            *
;********************************************************************

EXTRN	CODE(xLDR45i,xLDR23i,xSVR45i,CMPi)
EXTRN	CODE(MULsi,TSTMULO,NEGi)
EXTRN	XDATA(REG_FOI,POS_RQI,REG_ERC,MAX_ENE)
EXTRN	XDATA(REG_I,REG_P)
EXTRN	DATA(POS_ACT,SPD_ACT)
EXTRN	BIT(FL_ENOV)

PUBLIC  REG_PI

PB____C SEGMENT CODE

; PI regulator polohy

RSEG PB____C

REG_PI: MOV   DPTR,#REG_FOI
	CALL  xLDR45i
	MOV   DPTR,#REG_I
	CALL  xLDR23i
	CALL  MULsi

	CLR   C
	MOV   DPTR,#POS_RQI
	MOVX  A,@DPTR
	SUBB  A,POS_ACT
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,POS_ACT+1
	MOV   R1,A
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,POS_ACT+2
	MOV   R2,A
	MOV   R3,#0
	JNB   ACC.7,REG_PI1
	MOV   R3,#0FFH
REG_PI1:MOV   A,R5
	ADD   A,R0
	MOV   R4,A
	MOV   A,R6
	ADDC  A,R1
	MOV   R5,A
	MOV   A,R7
	MOV   R0,#0
	JNB   ACC.7,REG_PI2
	MOV   R0,#0FFH
REG_PI2:ADDC  A,R2
	MOV   R6,A
	MOV   A,R0
	ADDC  A,R3
	MOV   R7,A
	CALL  TSTMULO

	MOV   DPTR,#REG_FOI
	CALL  xSVR45i
	MOV   DPTR,#REG_P
	CALL  xLDR23i
	CALL  MULsi
	CALL  TSTMULO
	MOV   DPTR,#MAX_ENE
	CALL  xLDR23i
	MOV   A,R5
	JB    ACC.7,REG_PI3
	CALL  CMPi
	JNC   REG_PI4
	SJMP  REG_PI6
REG_PI3:MOV   A,R2
	ADD   A,R4
	MOV   A,R3
	ADDC  A,R5
	JC    REG_PI6
REG_PI4:MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	XCH   A,R5
	JNB   ACC.7,REG_PI5
	CALL  NEGi
REG_PI5:MOV   A,SPD_ACT
	ORL   A,SPD_ACT+1
	JNZ   REG_PI6
	MOV   DPTR,#REG_ERC
	MOVX  A,@DPTR
	INC   A
	CJNE  A,#20,REG_PI7
	SETB  FL_ENOV
	SJMP  REG_PI8
REG_PI6:MOV   DPTR,#REG_ERC
	CLR   A
REG_PI7:MOVX  @DPTR,A
REG_PI8:RET

	END