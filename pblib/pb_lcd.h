;********************************************************************
;*                    PB_LCD.H                                      *
;*     Include file s ridicimi kody pro DISPLAY 2x16                *
;*                  Stav ke dni 24.03.1991                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************


; Ridici kody pro displej

LCD_BF   EQU   080H  ; busy flag - cteni z LCD_STAT

; Rizeni pres LCD_INST

LCD_HOM  EQU   080H  ; nastaveni cursoru
LCD_CLR  EQU   001H  ; smazani displeje
LCD_HOME EQU   002H  ; cursor na pocatek
LCD_MOD  EQU   038H  ; nastaveni modu dual line, 8 bitu
LCD_NROL EQU   010H  ; stabilni displej
LCD_DON  EQU   00CH  ; displej on
LCD_CON  EQU   00AH  ; cursor on
LCD_BON  EQU   009H  ; blink on
LCD_NSH  EQU   004H  ; pohyb
LCD_CGA  EQU   040H  ; nastaveni adresy do generatoru znaku

; Definice ridicich znaku

C_CLR    EQU   LCD_CLR
C_HOME   EQU   LCD_HOME
C_LIN2   EQU   00FH

;********************************************************************
