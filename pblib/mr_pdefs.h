;********************************************************************
;*                        MR_PDEFS.H                                *
;*          Definice pro polohovaci subsystem                       *
;*                  Stav ke dni 14.09.1999                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

; Vyzaduje jiz naincludovany INCH_MMAC
; Vyzaduje jiz naincludovany INCH_MR_DEFS

%STRUCTM(OMR,AIR,1)	; spodni cast adresy IRC snimace
%STRUCTM(OMR,APW,1)	; spodni cast adresy PWM vystupu
%STRUCTM(OMR,CFG,2)	; konfiguracni priznaky  xxxxxxxT xxxRDSSS
%STRUCTM(OMR,VRJ,1)	; instrukce JMP
%STRUCTM(OMR,VR,2)	; adresa rutiny regulatoru, koncit RET
%STRUCTM(OMR,VGJ,1)	; instrukce JMP
%STRUCTM(OMR,VG,2)	; adresa generatoru, koncit VR_RET, VR_REGx
%STRUCTM(OMR,GST,1)	; soucasny stav generovani polohy
%STRUCTM(OMR,GEP,12)	; koncova poloha nebo dalsi informace

