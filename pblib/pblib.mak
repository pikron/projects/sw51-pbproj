#   Project file pro cerpadlo LCP4000
#         (C) Pisoft 1992

pb_ai.obj   : pb_ai.asm pb_tty.h
	a51   pb_ai.asm  $(par)

pb_al.obj   : pb_al.asm
	a51   pb_al.asm  $(par)

pb_al1.obj  : pb_al1.asm
	a51   pb_al1.asm  $(par)

pb_al2.obj  : pb_al2.asm
	a51   pb_al2.asm  $(par)

pb_af.obj   : pb_af.asm
	a51   pb_af.asm  $(par)

pb_tty.obj  : pb_tty.asm
	a51   pb_tty.asm $(par)

pb_ut1.obj  : pb_ut1.asm
	a51   pb_ut1.asm $(par)

pb_ut2.obj  : pb_ut2.asm
	a51   pb_ut2.asm $(par)

pb_vec.obj  : pb_vec.asm
	a51   pb_vec.asm $(par)

# bloky pro rizeni polohy

pb_rpi.obj  : pb_rpi.asm pb_ai.h
	a51   pb_rpi.asm  $(par)

pb_rpd.obj  : pb_rpd.asm pb_ai.h
	a51   pb_rpd.asm  $(par)

pb_rpz.obj  : pb_rpz.asm pb_ai.h
	a51   pb_rpz.asm  $(par)

pb_gpos.obj : pb_gpos.asm
	a51   pb_gpos.asm  $(par)

# bloky pro viceose rizeni

mr_rpz.obj  : mr_rpz.asm
	a51   mr_rpz.asm $(par)

mr_pid.obj  : mr_pid.asm
	a51   mr_pid.asm $(par)

mr_pidb.obj : mr_pidb.asm
	a51   mr_pidb.asm $(par)

mr_pidl.obj : mr_pidl.asm
	a51   mr_pidl.asm $(par)

mr_pidnl.obj: mr_pidnl.asm
	a51   mr_pidnl.asm $(par)

mr_pidt.obj : mr_pidt.asm
	a51   mr_pidt.asm $(par)

# monitor

monitor.obj : monitor.asm
	a51   monitor.asm $(par)

# komunikacni moduly

ulan.obj    : ulan.asm
	a51   ulan.asm $(par)

ulan1.obj   : ulan1.asm
	a51   ulan1.asm $(par)

ulan2.obj   : ulan2.asm
	a51   ulan2.asm $(par)

ulan3.obj   : ulan3.asm
	a51   ulan3.asm $(par)

ulan4.obj   : ulan4.asm
	a51   ulan4.asm $(par)

ul_oi.obj   : ul_oi.asm
	a51   ul_oi.asm $(par)

ul_oc.obj   : ul_oc.asm
	a51   ul_oc.asm $(par)

pb_iic.obj  : pb_iic.asm
	a51   pb_iic.asm $(par)

pb_rs232.obj: pb_rs232.asm
	a51   pb_rs232.asm

pb_rshld.obj: pb_rshld.asm
	a51   pb_rshld.asm

pb_rsoi.obj : pb_rsoi.asm
	a51   pb_rsoi.asm

# user interface

pb_ui.obj   : pb_ui.asm   ui_defs.h
	a51   pb_ui.asm   $(par)

pb_ui1.obj  : pb_ui1.asm  ui_defs.h pb_tty.h
	a51   pb_ui1.asm  $(par)

pb_uihw.obj : pb_uihw.asm ui_defs.h
	a51   pb_uihw.asm $(par)

# ================================================
# zaktualizovani knihovny

       : pb.lib
  copy pb.lib pb.old

pb.lib :
  lib51 create pb.lib

pb.old : pb_ai.obj pb_al.obj pb_al1.obj pb_al2.obj
  lib51 replace pb_ai.obj,pb_al.obj,pb_al1.obj,pb_al2.obj in pb.lib

pb.old : pb_tty.obj pb_vec.obj
  lib51 replace pb_tty.obj,pb_vec.obj in pb.lib

pb.old : pb_af.obj
  lib51 replace pb_af.obj in pb.lib

pb.old : pb_ut1.obj pb_ut2.obj monitor.obj
  lib51 replace pb_ut1.obj,pb_ut2.obj,monitor.obj in pb.lib

pb.old : ulan.obj ulan1.obj ulan2.obj ulan3.obj ulan4.obj
  lib51 replace ulan.obj,ulan1.obj,ulan2.obj,ulan3.obj,ulan4.obj in pb.lib

pb.old : ul_oi.obj ul_oc.obj
  lib51 replace ul_oi.obj,ul_oc.obj in pb.lib

pb.old : pb_iic.obj
  lib51 replace pb_iic.obj in pb.lib

pb.old : pb_rs232.obj pb_rshld.obj pb_rsoi.obj
  lib51 replace pb_rs232.obj,pb_rshld.obj,pb_rsoi.obj in pb.lib

pb.old : pb_gpos.obj pb_rpi.obj pb_rpd.obj pb_rpz.obj
  lib51 replace pb_gpos.obj,pb_rpi.obj,pb_rpd.obj,pb_rpz.obj in pb.lib

pb.old : mr_rpz.obj
  lib51 replace mr_rpz.obj in pb.lib

pb.old : mr_pid.obj mr_pidb.obj
  lib51 replace mr_pid.obj,mr_pidb.obj in pb.lib

pb.old : mr_pidl.obj mr_pidnl.obj
  lib51 replace mr_pidl.obj,mr_pidnl.obj in pb.lib

pb.old : mr_pidt.obj
  lib51 replace mr_pidt.obj in pb.lib

pb.old : pb_ui.obj pb_ui1.obj pb_uihw.obj
  lib51 replace pb_ui.obj,pb_ui1.obj,pb_uihw.obj in pb.lib

       : pb.old
  del pb.old