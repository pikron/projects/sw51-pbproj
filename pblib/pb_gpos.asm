$NOMOD51
;********************************************************************
;*                        PB_RPI                                    *
;*                Sejmuti aktualni polohy motoru                    *
;*                  Stav ke dni 12.03.1996                          *
;*                      (C) Pisoft 1996 Pavel Pisa Praha            *
;********************************************************************

$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_REGS)
$LIST

EXTRN	DATA(POS_ACT,SPD_ACT)

PUBLIC  G_POS

PB____C	SEGMENT CODE

RSEG PB____C

; Vraci POS_ACT a SPD_ACT

G_POS:	MOV   R5,TMH2         ; Vypocet aktualni pozice motoru
	MOV   R4,TML2
	MOV   A,TMH2
	XRL   A,R5
	JZ    G_POS1
	MOV   A,R4
	JB    ACC.7,G_POS1
	INC   R5
G_POS1:	MOV   R3,TH0
	MOV   R2,TL0
	MOV   A,TH0
	XRL   A,R3
	JZ    G_POS2
	MOV   A,R2
	JB    ACC.7,G_POS2
	INC   R3
G_POS2:	CLR   C
	MOV   A,R4
	SUBB  A,R2
	MOV   R4,A
	MOV   A,R5
	SUBB  A,R3
	MOV   R5,A

	CLR   C               ; Generovani SPD_ACT = COUNTER-POS_ACT
	MOV   A,R4
	SUBB  A,POS_ACT
	MOV   SPD_ACT,A
	MOV   POS_ACT,R4
	MOV   A,R5
	SUBB  A,POS_ACT+1
	MOV   SPD_ACT+1,A
	MOV   POS_ACT+1,R5
	JNC   G_POS3
	CPL   A
G_POS3:	JNB   ACC.7,G_POS4   ; Prodlouzeni POS_ACT
	INC   POS_ACT+2
	JC    G_POS4
	DEC   POS_ACT+2
	DEC   POS_ACT+2
G_POS4:	RET

	END
