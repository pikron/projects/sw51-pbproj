Cerpadlo

%DEFINE (PROG_FL)  (LED_FLH.7)
%DEFINE (BEEP_FL)  (LED_FLG.7)
%DEFINE (HOLD_FL)  (LED_FLG.6)
%DEFINE (START_FL) (LED_FLG.5)
%DEFINE (PURGE_FL) (LED_FLG.4)
%DEFINE (RUN_FL)   (LED_FLG.3)

G_A_LMSK EQU   002H
G_B_LMSK EQU   001H
G_C_LMSK EQU   004H
GRAD_LMSK EQU   007H

%DEFINE (ALRM_FL)  (LED_FLH.0)
%DEFINE (G_BF_FL)  (LED_FLH.1)


Stary detektor - ten vyhodime

%DEFINE (PROG_FL)  (KBD_FLG.7)
%DEFINE (BEEP_FL)  (LED_FLG.7)
%DEFINE (LVIS_FL)  (LED_FLG.6)
%DEFINE (LHIG_FL)  (LED_FLG.5)
%DEFINE (LHIG_FL)  (LED_FLG.4)
%DEFINE (RUN_FL)   (LED_FLG.3)

Davkovac

LFB_BEEP   EQU  7
LFB_CAL	   EQU  6
LFB_PREP   EQU  5
LFB_LOAD   EQU  4
LFB_RUN	   EQU  3
LFB_STBY   EQU  2
LFB_INJ    EQU  1
LFB_FIX	   EQU  0

%DEFINE (BEEP_FL)   (LED_FLG.LFB_BEEP)
%DEFINE (CAL_FL)    (LED_FLG.LFB_CAL)
%DEFINE (PREP_FL)   (LED_FLG.LFB_PREP)
%DEFINE (LOAD_FL)   (LED_FLG.LFB_LOAD)
%DEFINE (RUN_FL)    (LED_FLG.LFB_RUN)
%DEFINE (STBY_FL)   (LED_FLG.LFB_STBY)
%DEFINE (INJ_FL)    (LED_FLG.LFB_INJ)
%DEFINE (FIX_FL)    (LED_FLG.LFB_FIX)

%DEFINE (PROG_FL)   (LED_FLH.7)
%DEFINE (END_FL)    (LED_FLH.6)

Novy detektor - muzeme asi jeste zmenit, ptotoze Rosembergovi 
                muzeme se softem zmenit klavesnici a tu pouzit v cerpadle

LFB_BEEP   EQU  7
LFB_LVIS   EQU  6
LFB_LHIG   EQU  5
LFB_LLOW   EQU  4
LFB_RUN    EQU  3

%DEFINE (PROG_FL)  (KBD_FLG.7)
%DEFINE (BEEP_FL)  (LED_FLG.LFB_BEEP)
%DEFINE (LVIS_FL)  (LED_FLG.LFB_LVIS)
%DEFINE (LHIG_FL)  (LED_FLG.LFB_LHIG)
%DEFINE (LLOW_FL)  (LED_FLG.LFB_LLOW)
%DEFINE (RUN_FL)   (LED_FLG.LFB_RUN)

Poznamky

LED_FLG			odpovida portu ledek
LED_FLH nebo KBD_FLG	odpovidaji zbyvajicim bitum
			na portu klavesnice

Pokusim se to zrekapitulovat podle pozic ledek z leva do prava a PROG,
bity budu znacit A.0 nulty bit portu ledek
                 B.7 sedmy bit portu klavesnice
v zavorce () je moje doporuceni na zmenu

Pozice		1	2	3	4	5	6	7	8	9	10

Cerpadlo	RUN	HOLD		START	PURGE		A	B	C	PROG
		A.3	A.6		A.5	A.4		A.1	A.0	A.2	B.7

Davkovac	RUN		CAL	PREP	LOAD	INJ		FIX	STANDBY	PROG
		A.3		A.6	A.5	A.4	A.1		A.0	A.2	B.7

Detektor	RUN			UVH	UVL	VIS				PROG
novy		A.3			A.5	A.4	A.6(1)				B.7

Dale BEEP je pro vsechny A.7