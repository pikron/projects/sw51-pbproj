;********************************************************************
;*                        PB_UIHW.ASM                               *
;*                Uzivatelske rozhrani - hardwarove zavisla cast    *
;*                  Stav ke dni 24.04.1997                          *
;*                      (C) Pisoft 1997 Pavel Pisa Praha            *
;********************************************************************

$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_UI_DEFS)
$LIST

EXTRN	CODE(UI_PV2G,UI_SDSY)
EXTRN	XDATA(UI_CP_T,UI_CP_X,UI_AV_OY,UI_MV_SY)
EXTRN	BIT(FL_DRCP,FL_CMAV,FL_DRAW)

PUBLIC	UI_WR,UI_CLR,UI_GKEY,GOTOXY,GOTO_S,SHOWCP,SHOW_CP,CLR_CP

PUBLIC	K_ENTER,K_DP,K_PM,KV_V_OK
PUBLIC	K_0,K_1,K_2,K_3,K_4,K_5,K_6,K_7,K_8,K_9

PB_UI_C SEGMENT CODE

RSEG	PB_UI_C

; Smaze cast displeje obsluhovanou user interfacem
UI_CLR:	MOV   A,#LCD_CLR
	JMP   LCDWCOM

; Vypise znak v A na display
; nesmi rusit zadny registr ani DPTR
UI_WR:	JMP   LCDWR

UI_GKEY:JMP   SCANKEY

; Nastavi polohu vypisu na R45 v lokalnich souradnicich,
; je-li treba skryje kurzor
; funkce musi zachovat R4567 vracene UI_PV2G
; ostatni registry a DPTR jsou k dispozici
; vraci A .. 0 je-li bod mimo aktivni oblast

GOTOXY: CALL  CLR_CP
GOTOXYC:CALL  UI_PV2G
	JZ    GOTOXY9
GOTOXY1:MOV   A,R5
	ADD   A,#GXY_D4L-GOTOXY2
	MOVC  A,@A+PC
GOTOXY2:
GOTOXY3:ADD   A,R4
	ORL   A,#LCD_HOM
	JMP   LCDWCOM
GOTOXY9:RET

GXY_D4L:DB    0
	DB    40H
	DB    14H
	DB    54H

; Nastavi typ cursoru na R7 a polohu na R45
SHOWCP:	CALL  CLR_CP
	MOV   DPTR,#UI_CP_T
	MOV   A,R7
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	RET

; Docasne skryti kurzoru v dobe vypisu na displej
CLR_CP:	JNB   FL_DRCP,CLR_CP1
	RET
CLR_CP1:SETB  FL_DRCP
CLR_CP2:MOV   A,#LCD_DON
	JMP   LCDWCOM

; Provede obnoveni zviditelneni kurzoru
SHOW_CP:CLR   FL_DRCP
	MOV   DPTR,#UI_CP_T
	MOVX  A,@DPTR
	JZ    CLR_CP2
	MOV   DPTR,#UI_CP_X
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  GOTOXYC
	JZ    SHOW_C2
SHOW_C1:MOV   A,#LCD_DON OR LCD_CON
	JMP   LCDWCOM
SHOW_C2:JNB   FL_CMAV,CLR_CP2
	JB    F0,CLR_CP2
	MOV   DPTR,#UI_AV_OY
	MOV   A,R5
	JB    ACC.7,SHOW_C3
	MOV   A,R7
	JNB   ACC.7,SHOW_C4
	MOVX  A,@DPTR
	ADD   A,R7
	JB    OV,SHOW_C4
	MOVX  @DPTR,A
	SJMP  SHOW_C4
SHOW_C3:MOVX  A,@DPTR
	CLR   C
	SUBB  A,R5
	JB    OV,SHOW_C4
	MOVX  @DPTR,A
SHOW_C4:MOV   DPTR,#UI_CP_X
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  GOTOXYC
	JZ    CLR_CP2
	SETB  FL_DRAW
	RET

; Pripravi polohu nasledujiciho vypisu do stavove radky
GOTO_S:	MOV   DPTR,#UI_MV_SY
	MOVX  A,@DPTR
	DEC   A
	MOV   R5,A
	MOV   R4,#0
	CALL  UI_SDSY
	JMP   GOTOXY1

	END