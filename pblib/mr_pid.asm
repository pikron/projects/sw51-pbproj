;********************************************************************
;*                        MR_PID                                    *
;*                Rutiny PID multi regulatoru polohy                *
;*                  Stav ke dni 14.09.1996                          *
;*                      (C) Pisoft 1996 Pavel Pisa Praha            *
;********************************************************************

$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MR_DEFS)
$LIST

%DEFINE (DEBUG_FL)	(0)	; Povoleni vlozeni debug vystupu
%DEFINE (SPEEDDOWN_FL)	(1)	; Zrychlene odcitani I
%DEFINE (TRYNULL_FL)	(0)	; Snaha o pomale vynulovani I
%DEFINE (LATCHUP_FL)	(0)	; Pridani male necitlivosti
%DEFINE (INSCOR1_FL)	(0)	; Nepatrne jine pridani praso

%DEFINE (MR_TYPE)	(MR_PIDP) ; Pojmenovani typu regulatoru

PUBLIC  %MR_TYPE

MR____C SEGMENT CODE

; PI regulator polohy

RSEG MR____C

%MR_TYPE:CLR  C
	MOV   A,#OMR_AP		; R45=OMR_P*(OMR_RPI-OMR_AP)
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_RPI
	MOVC  A,@A+DPTR
	SUBB  A,R4
	MOV   R4,A
	MOV   A,#OMR_AP+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   A,#OMR_RPI+1
	MOVC  A,@A+DPTR
	SUBB  A,R5
	MOV   R5,A
	MOV   A,#OMR_AP+2
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   A,#OMR_RPI+2
	MOVC  A,@A+DPTR
	SUBB  A,R6
	JNB   ACC.7,L_P_P
L_P_N:	ANL   A,R5		; -
	CJNE  A,#-1,L_PL_N
%IF(%LATCHUP_FL) THEN (
	CJNE  R4,#-1,L_P_N1
	SJMP  L_PL_Z
)FI
L_P_N1:	MOV   A,#OMR_P		; R45 = R4*OMR_P
	MOVC  A,@A+DPTR
	JZ    L_P_P2
	MOV   R6,A
	MOV   B,R4
	MUL   AB
	MOV   R4,A
	MOV   A,B
	SUBB  A,R6
	MOV   R5,A
	JB    ACC.7,L_P_OK
L_PL_N:	MOV   R4,#000H
	MOV   R5,#080H
	SJMP  L_P_OK
L_P_P:	ORL   A,R5		; +
	JNZ   L_PL_P
%IF(%LATCHUP_FL) THEN (
	CJNE  R4,#0,L_P_P0
	SJMP  L_PL_Z
L_P_P0:	CJNE  R4,#1,L_P_P1
L_PL_Z:	CLR   A
	MOV   R4,A
	MOV   R5,A
	SJMP  L_P_OK
)FI
L_P_P1:	MOV   A,#OMR_P		; R45 = R4*OMR_P
	MOVC  A,@A+DPTR
L_P_P2:	MOV   B,R4
	MUL   AB
	MOV   R4,A
	MOV   A,B
	MOV   R5,A
	JNB   ACC.7,L_P_OK
L_PL_P:	MOV   R4,#0FFH
	MOV   R5,#07FH
L_P_OK:				; _ -> R45 ; R6 B

	CLR   C
	MOV   A,#OMR_AS		; R23=OMR_D*(OMR_RSI-OMR_AS)
	MOVC  A,@A+DPTR
	MOV   R2,A
	MOV   A,#OMR_RSI
	MOVC  A,@A+DPTR
	SUBB  A,R2
	MOV   R2,A
	MOV   A,#OMR_AS+1
	MOVC  A,@A+DPTR
	MOV   R3,A
	MOV   A,#OMR_RSI+1
	MOVC  A,@A+DPTR
	SUBB  A,R3
	JNB   ACC.7,L_D_P
L_D_N:  CJNE  A,#-1,L_DL_N	; -
	MOV   A,#OMR_D		; R23 = R23*OMR_D
	MOVC  A,@A+DPTR
	JZ    L_D_P1
	MOV   R6,A
	MOV   B,R2
	MUL   AB
	MOV   R2,A
	MOV   A,B
	SUBB  A,R6
	MOV   R3,A
	JB    ACC.7,L_D_OK
L_DL_N:	MOV   R2,#000H
	MOV   R3,#080H
	SJMP  L_D_OK
L_D_P:	JNZ   L_DL_P		; +
	MOV   A,#OMR_D		; R23 = R23*OMR_D
	MOVC  A,@A+DPTR
L_D_P1:	MOV   B,R2
	MUL   AB
	MOV   R2,A
	MOV   A,B
	MOV   R3,A
	JNB   ACC.7,L_D_OK
L_DL_P:	MOV   R2,#0FFH
	MOV   R3,#07FH
L_D_OK:				; _ -> R23 ; R6 B

	MOV   A,R4		; R45 = (R45 + R23)*16
	ADD   A,R2
	SWAP  A
	MOV   R6,A
	ANL   A,#0F0H
	MOV   R4,A
	MOV   A,R5
	ADDC  A,R3
	JB    OV,L_PDL
	MOV   R5,A
	ANL   A,#0F8H
	JB    ACC.7,L_PD_N
L_PD_P:	JZ    L_PD_2
L_PDL_P:MOV   R4,#0FFH
	MOV   R5,#07FH
	SJMP  L_PD_OK
L_PD_N: CJNE  A,#0F8H,L_PDL_N
	SJMP  L_PD_2
L_PDL:	JNC   L_PDL_P
L_PDL_N:MOV   R4,#000H
	MOV   R5,#080H
	SJMP  L_PD_OK
L_PD_2: MOV   A,R5
	SWAP  A
	ANL   A,#0F0H
	XRL   A,R6
	XRL   A,R4
	MOV   R5,A
L_PD_OK:

%IF (%DEBUG_FL) THEN (
	MOV   A,R4		; R01 = mezivysledek
	MOV   R0,A
	MOV   A,R5
	MOV   R1,A
)FI

%IF (%TRYNULL_FL) THEN (	; pri nulove chybe zmensuj OMR_FOI
	ORL   A,R4
	JNZ   L_I_1
	MOV   A,#OMR_FOI	; R67 = OMR_FOI
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   A,#OMR_FOI+1
	MOVC  A,@A+DPTR
	MOV   R7,A
	ORL   A,R6
	JZ    L_I_8
	MOV   R2,#001H
	MOV   R3,#000H
	MOV   A,R7
	JB    ACC.7,L_I_7
	MOV   R2,#0FFH
	MOV   R3,#0FFH
	SJMP  L_I_7
)FI
L_I_1:	MOV   A,#OMR_FOI	; R67 = OMR_FOI
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   A,#OMR_FOI+1
	MOVC  A,@A+DPTR
	MOV   R7,A
%IF (%SPEEDDOWN_FL) THEN (	; dvakrat zmensuj OMR_FOI
	XRL   A,R5
	JNB   ACC.7,L_I_4
	MOV   A,R6
	ORL   A,R7
	JZ    L_I_4
	MOV   A,#OMR_I		; R23=R45*OMR_I*2/256
	MOVC  A,@A+DPTR
	RL    A
	JZ    L_I_OK
	MOV   B,R5
	MOV   R2,A
	MOV   C,B.7
	MOV   F0,C
	MUL   AB
	XCH   A,R2
	XCH   A,B
	JNB   F0,L_I_2
	SUBB  A,B
L_I_2:	MOV   R3,A
	MOV   A,R4
	MUL   AB
	ADD   A,#080H
;	ANL   C,F0
	MOV   A,B
	ADDC  A,R2
	MOV   R2,A
	CLR   A
	ADDC  A,R3
	MOV   R3,A		; _ R45 -> 23
	XRL   A,R7
	JB    ACC.7,L_I_7
	MOV   R2,#0
	MOV   R3,#0
	SJMP  L_I_7
) FI
L_I_4:	MOV   A,#OMR_I		; R23=R45*OMR_I/256
	MOVC  A,@A+DPTR
	JZ    L_I_OK
	MOV   B,R5
	MOV   R2,A
	MOV   C,B.7
	MOV   F0,C
	MUL   AB
	XCH   A,R2
	XCH   A,B
	JNB   F0,L_I_5
	SUBB  A,B
L_I_5:	MOV   R3,A
	MOV   A,R4
	MUL   AB
	ADD   A,#080H
;	ANL   C,F0
	MOV   A,B
	ADDC  A,R2
	MOV   R2,A
	CLR   A
	ADDC  A,R3
	MOV   R3,A		; _ R45 -> 23

L_I_7:	MOV   A,R6		; R67 = R67 + R45
	ADD   A,R2
	MOV   R6,A
	MOV   A,R7
	ADDC  A,R3
	MOV   R7,A
	JNB   OV,L_I_8
	MOV   R6,#000H
	MOV   R7,#080H
	JC    L_I_8
	DEC   R6
	DEC   R7
L_I_8:  			; R67 = OMR_FOI

	MOV   A,R4		; R45 = R45 + R67
	ADD   A,R6
	MOV   R4,A
	MOV   A,R5
	ADDC  A,R7
	MOV   R5,A
	JNB   OV,L_I_OK
	MOV   R4,#0FFH
	MOV   R5,#07FH
	JNC   L_I_OK
	INC   R4
	INC   R5
L_I_OK:

	CLR   F0
	MOV   A,#OMR_ME		; Limit maximalni energie
	MOVC  A,@A+DPTR
	MOV   R2,A
	MOV   A,#OMR_ME+1
	MOVC  A,@A+DPTR
	MOV   R3,A
	MOV   A,R5
	JB    ACC.7,MR_PI22
%IF(%INSCOR1_FL)THEN(
	ORL   A,R4
)FI
	JZ    MR_PI27
	MOV   A,#OMR_S1		; Pridani praso +
	MOVC  A,@A+DPTR
	ADD   A,R5
	JB    OV,MR_PI20
	MOV   R5,A
	CLR   C			; R45 < R23
	MOV   A,R4
	SUBB  A,R2
	MOV   A,R5
	SUBB  A,R3
	JC    MR_PI28
MR_PI20:MOV   A,R2		; Kladne preteceni MAX_ENE
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	SJMP  MR_PI26
MR_PI22:
%IF(NOT %INSCOR1_FL)THEN(
	CPL   A
	JZ    MR_PI27
)FI
	MOV   A,#OMR_S2		; Pridani praso -
	MOVC  A,@A+DPTR
	CPL   A
	INC   A
	ADD   A,R5
	JB    OV,MR_PI24
	MOV   R5,A		; R45 > -R23
	MOV   A,R2
	ADD   A,R4
	MOV   A,R3
	ADDC  A,R5
	JC    MR_PI28
MR_PI24:CLR   C			; Zaporne preteceni MAX_ENE
	CLR   A
	SUBB  A,R2
	MOV   R4,A
	CLR   A
	SUBB  A,R3
	MOV   R5,A
MR_PI26:MOV   A,#OMR_AS		; Kontrola pohybu pri max energii
	MOVC  A,@A+DPTR
	MOV   R2,A
	MOV   A,#OMR_AS+1
	MOVC  A,@A+DPTR
	ORL   A,R2
	JNZ   MR_PI28
	MOV   A,#OMR_ERC
	MOVC  A,@A+DPTR
	INC   A
	CJNE  A,#40,MR_PI30
	SETB  F0		; Preteceni energie
	DEC   A
	SJMP  MR_PI30
MR_PI27:
%IF(NOT %INSCOR1_FL)THEN(
	MOV   R4,#0
	MOV   R5,#0
)FI
MR_PI28:CLR   A
MR_PI30:MOV   R2,A
	MOV   A,DPL
	ADD   A,#OMR_ERC
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#0
	MOV   DPH,A
	MOV   A,R2
	MOVX  @DPTR,A
	MOV   A,DPL
	ADD   A,#LOW  (OMR_FOI-OMR_ERC)
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#HIGH (OMR_FOI-OMR_ERC)
	MOV   DPH,A
	MOV   A,R6		; OMR_FOI=R67
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
%IF (%DEBUG_FL) THEN (
	INC   DPTR
	MOV   A,R0		; OMR_FOD=R01= mezivysledek
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A
)FI
	RET

	END