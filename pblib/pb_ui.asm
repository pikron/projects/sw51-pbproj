;********************************************************************
;*                        PB_UI.ASM                                 *
;*                Uzivatelske rozhrani - zakladni modul		    *
;*                  Stav ke dni 24.04.1997                          *
;*                      (C) Pisoft 1997 Pavel Pisa Praha            *
;********************************************************************

$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_UI_DEFS)
$LIST

; Obecne knihovni funkce
EXTRN	CODE(xLDR45i,xSVR45i,xMDPDP,SEL_FNC,PRINTi)

; Pipnuti pri chybe, definovano aplikaci
EXTRN	CODE(ERRBEEP)

; Hardwarove zavisla cast PB_UIHW
EXTRN	CODE(UI_WR,UI_CLR,UI_GKEY,GOTOXY,GOTO_S)
EXTRN	CODE(SHOWCP,SHOW_CP,CLR_CP)
EXTRN	NUMBER(K_ENTER,K_DP,K_PM,KV_V_OK)
EXTRN	NUMBER(K_0,K_1,K_2,K_3,K_4,K_5,K_6,K_7,K_8,K_9)

; Bitove priznaky
PUBLIC	D4LINE,FL_REFR,FL_DRAW,FL_DRCP,FL_CMAV

; Datove polozky v DATA
PUBLIC	UI_AU,UI_AD,EV_TYPE,EV_DATA

; Datove polozky v XDATA
PUBLIC	UI_CP_T,UI_CP_X,UI_CP_Y
PUBLIC  UI_AV_X,UI_AV_Y,UI_AV_SX,UI_AV_SY,UI_AV_OX,UI_AV_OY
PUBLIC  UI_MV_SX,UI_MV_SY
PUBLIC	EV_BUF,GR_ACT

; Funkce pro tvorbu dalsich objektu
PUBLIC	GOTO_A,UI_PV2G,UI_SDSY
PUBLIC	JMP_VEV,JMP_VE1,EV_CLR
PUBLIC	UB_EV,UB_GFT,UB_RFT,UB_GFLG,UB_SHCP,UB_A_RD,UB_A_WR
PUBLIC	UB_DRAW,UB_DRAW1,UB_WRDPN,UB_WRDP,UB_SFT,UB_DRS1
PUBLIC	FND_MINT

; Zpracovani udalosti jednotlivych objektu a prace s udalostmi
PUBLIC	GR_EV,UIN_EV,BUT_EV,MUT_EV
PUBLIC	UR_Mi,UR_MiT,UW_Mi,NULL_A
PUBLIC	EV_POST,EV_GET,EV_DO,EV_PO23,GR_RQ23
PUBLIC	UI_ABORT

; Konverzni funkce a tisk
PUBLIC	xITOAF,MULTENl,MULTENlF,PRINTli

PB_UI_C SEGMENT CODE
PB_UI_X SEGMENT XDATA
PB_UI_D SEGMENT DATA
PB_UI_B SEGMENT DATA BITADDRESSABLE

RSEG	PB_UI_B

UI_FLG:	DS    1
D4LINE	BIT   UI_FLG.7
FL_REFR	BIT   UI_FLG.6	; casovy refresh hodnot
FL_DRAW	BIT   UI_FLG.5	; nutno znovu vykreslit
FL_DRCP	BIT   UI_FLG.4	; je treba obnovit cursor
FL_CMAV	BIT   UI_FLG.3	; cursor muze pohybovat viewem

RSEG	PB_UI_D

UI_AU:	DS    2		; ukazatel na popis polozky
UI_AD:	DS    2		; ukazatel na data polozky

EV_TYPE:DS    1		; typ zpracovavane udalosti
EV_DATA:DS    2         ; data

RSEG	PB_UI_X

EV_BUF:	DS    3		; buffer pristi udalosti

UI_CP_T:DS    1		; typ cursoru
UI_CP_X:DS    1		; poloha cursoru
UI_CP_Y:DS    1

; aktualni pohled
UI_AV_OX:DS   1		; posun informace v pohledu
UI_AV_OY:DS   1
UI_AV_X:DS    1		; poloha pohledu
UI_AV_Y:DS    1
UI_AV_SX:DS   1		; velikost pohledu
UI_AV_SY:DS   1

UI_MV_SX:DS   1		; velikost displeje
UI_MV_SY:DS   1

GR_ACT:	DS    2		; aktualni skupina
GR_UACT:DS    1		; cislo zpracovavane polozky
GR_UFOC:DS    1		; cislo focusovane polozky
GR_UFFL:DS    1		; priznaky focusovane polozky
GR_TMP:	DS    4		; pomocny buffer

UI_ROOT	XDATA	GR_ACT	; koren stromu objektu pro udalosti

EV_DO_T:DS    2		; buffer pro EV_DO

RSEG	PB_UI_C

GOTO_A:	MOV   DPL,UI_AU
	MOV   DPH,UI_AU+1
	MOV   A,#OU_X
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OU_Y
	MOVC  A,@A+DPTR
	MOV   R5,A
	JMP   GOTOXY

; *******************************************************************
; Rizeni pohledu

; Nastavi pohled na cely displej
UI_SMAXV:MOV  DPTR,#UI_AV_OX
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   R4,A
	MOV   R5,A
	MOV   DPTR,#UI_MV_SX
	MOVX  A,@DPTR
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
UI_SAV:	MOV   DPTR,#UI_AV_X
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	RET

; Prepocet lokalnich na globalni souradnice
UI_PV2G:MOV   DPTR,#UI_AV_OX
	MOVX  A,@DPTR	; OX
	ADD   A,R4
	JB    OV,UI_L2GE
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR	; OY
	ADD   A,R5
	JB    OV,UI_L2GE
	MOV   R5,A
UI_PL2G:MOV   DPTR,#UI_AV_X
	MOV   A,#3
	MOVC  A,@A+DPTR
	SETB  C
	SUBB  A,R5
	MOV   R7,A
	JC    UI_L2GZ
	INC   R7
	MOV   A,#2
	MOVC  A,@A+DPTR
	SETB  C
	SUBB  A,R4
	MOV   R6,A
	JC    UI_L2GZ
	INC   R6
	MOVX  A,@DPTR	; X
	ADD   A,R4
	JB    OV,UI_L2GE
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR	; Y
	ADD   A,R5
	JB    OV,UI_L2GE
	MOV   R5,A
	MOV   A,#1
	CLR   F0
	RET
UI_L2GZ:CLR   F0
	CLR   A
	RET
UI_L2GE:SETB  F0
	CLR   A
	RET

; Zmensi UI_AV_SY tak aby R5 bylo pod pohledem
UI_SDSY:MOV   DPTR,#UI_AV_Y
	MOVX  A,@DPTR
	MOV   R0,A
	MOV   DPTR,#UI_AV_SY
	MOVX  A,@DPTR
	XCH   A,R0
	ADD   A,R0
	CPL   A
	INC   A
	ADD   A,R5
	JNB   ACC.7,UI_SDSY9
	ADD   A,R0
	MOVX  @DPTR,A
	SETB  FL_DRAW
UI_SDSY9:RET

; *******************************************************************
; Prace s udalostmi

EV_PO23:MOV   A,R2
	MOV   R7,A
	MOV   A,R3
	MOV   R4,A
	MOV   R5,#0

; Ulozi zpravu typu R7 s daty R45 nelze-li nastavi F0
EV_POST:MOV   DPTR,#EV_BUF
	MOVX  A,@DPTR
	JZ    EV_POS1
	SETB  F0
	RET
EV_POS1:CLR   F0
	MOV   A,R7
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	RET

; Naplni R7 R45 novou udalosti neni-li vraci A=0
EV_GET:	MOV   DPTR,#EV_BUF
	MOVX  A,@DPTR
	JZ    EV_GE10
	MOV   R7,A
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   A,R7
	RET
EV_GE10:CALL  UI_GKEY
	JZ    EV_GE20
	MOV   R7,#ET_KEY	; Stisknuta klavesa
	MOV   R4,A
	MOV   R5,#0
	MOV   A,R7
	RET
EV_GE15:MOV   R7,#ET_DRAW	; Prekreslit displej
	CLR   FL_REFR
	SJMP  EV_GE17
EV_GE16:MOV   R7,#ET_REFR	; Obnovit obsah dipleje
EV_GE17:CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   A,R7
	RET
EV_GE20:JBC   FL_DRAW,EV_GE15
	JBC   FL_REFR,EV_GE16



	JNB   FL_DRCP,EV_GE99
	CALL  SHOW_CP		; Zviditelnit cursor
EV_GE99:CLR   A
	RET

; Podvrdi zpracovani udalosti
EV_CLR: MOV   EV_TYPE,#0
	MOV   EV_DATA,UI_AU
	MOV   EV_DATA+1,UI_AU+1
	RET

; Zlikviduje vsechny UI objekty
UI_ABORT:CLR  A
	MOV   DPTR,#EV_DO_T
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	JMP   EV_DO11

; Spusti zpracovani udalosti R7 R45
EV_DO:	MOV   EV_TYPE,R7
	MOV   EV_DATA,R4
	MOV   EV_DATA+1,R5
EV_DO10:MOV   A,EV_TYPE
	CJNE  A,#ET_RQGR,EV_DO20
	MOV   DPTR,#EV_DO_T		; Nova skupina
	MOV   A,EV_DATA
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,EV_DATA+1
	MOVX  @DPTR,A
	MOV   DPTR,#GR_ACT
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	ORL   A,R0
	JZ    EV_DO14
	MOV   EV_TYPE,#ET_RELF
	CLR   A
	MOV   EV_DATA,A
	MOV   EV_DATA+1,A
	CALL  EV_DOGR
	MOV   A,EV_TYPE
	JZ    EV_DO12
EV_DO11:MOV   EV_TYPE,#ET_DONE		; Abort skupiny
	CLR   A				; nova skupina EV_DO_T
	MOV   EV_DATA,A
	MOV   EV_DATA+1,A
	CALL  EV_DOGR
	MOV   A,EV_TYPE
	JNZ   EV_DO14
EV_DO12:RET
EV_DO14:CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   R7,A
	CALL  SHOWCP		; Schovat kurzor
	CALL  UI_SMAXV
	CALL  UI_CLR
	MOV   EV_TYPE,#ET_INIT
	MOV   DPTR,#EV_DO_T
	CALL  xLDR45i
	MOV   DPTR,#GR_ACT
	CALL  xSVR45i
	CALL  EV_DOGR
EV_DO16:MOV   EV_TYPE,#ET_GETF
	SETB  FL_DRAW
	CLR   A
	MOV   EV_DATA,A
	MOV   EV_DATA+1,A
	JMP   EV_DOGR
EV_DO20:CJNE  A,#ET_RQF,EV_DO30
	MOV   DPTR,#EV_DO_T		; Presunout focus
	MOV   A,EV_DATA
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,EV_DATA+1
	MOVX  @DPTR,A
	MOV   EV_TYPE,#ET_RELF
	CLR   A
	MOV   EV_DATA,A
	MOV   EV_DATA+1,A
	CALL  EV_DOGR
	MOV   A,EV_TYPE
	JZ    EV_DO12
	CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   R7,A
	CALL  SHOWCP		; Schovat kurzor
	MOV   EV_TYPE,#ET_GETF
	SETB  FL_DRAW
	MOV   DPTR,#EV_DO_T
	MOVX  A,@DPTR
	MOV   EV_DATA,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   EV_DATA+1,A
	CALL  EV_DOGR
	MOV   A,EV_TYPE
	JNZ   EV_DO16
	RET
EV_DO30:CJNE  A,#ET_ERR,EV_DO40
	CALL  ERRBEEP			; Error

	RET
EV_DO40:

EV_DOGR:MOV   DPTR,#UI_ROOT
	MOVX  A,@DPTR
	MOV   UI_AU,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   UI_AU+1,A
	ORL   A,UI_AU
	JZ    EV_DOGE
	JMP   JMP_VEV
EV_DOGE:RET

JMP_VEV:MOV   DPL,UI_AU
	MOV   DPH,UI_AU+1
JMP_VE1:CLR   A
	JMP   @A+DPTR

; *******************************************************************
; Zakladni operace

UB_EV:  MOV   A,EV_TYPE
	CJNE  A,#ET_HLP,UB_EV10
	%UI_OU2DP(OU_HLP)
UB_HLP1:CALL  xLDR45i
	ORL   A,R4
	JZ    UB_EV09
	MOV   R7,#ET_RQHLP
	CALL  EV_POST
	CALL  EV_CLR
UB_EV09:RET
UB_EV10:RET

; vraci ACC=0 pokud je prijat focus
UB_GFT:	MOV   A,EV_DATA
	ORL   A,EV_DATA+1
	JZ    UB_GFT1
	MOV   A,EV_DATA
	XRL   A,UI_AU
	MOV   R0,A
	MOV   A,EV_DATA+1
	XRL   A,UI_AU+1
	ORL   A,R0
	JNZ   UB_GFTR
UB_GFT1:MOV   DPL,UI_AU
	MOV   DPH,UI_AU+1
	MOV   A,#OU_MSK
	MOVC  A,@A+DPTR
	CPL   A
	ANL   A,#UFM_FOC
	JNZ   UB_GFTR
	MOV   DPL,UI_AD
	MOV   DPH,UI_AD+1
	INC   DPTR
	MOVX  A,@DPTR
	ORL   A,#UFM_FOC
	MOVX  @DPTR,A
	CLR   A
UB_GFTR:RET

; Standartni uvolneni fokusu
UB_RFT:	MOV   DPL,UI_AD
	MOV   DPH,UI_AD+1
	INC   DPTR
	MOVX  A,@DPTR
	ANL   A,#NOT (UFM_FOC)
	MOVX  @DPTR,A
	RET

UB_GFLG:%UI_OUD2DP(OUD_FLG)
	MOVX  A,@DPTR
	RET

UB_SHCP:MOV   R7,#CPT_NOR
	%UI_OU2DP(OU_X)
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	ADD   A,R4
	DEC   A
	MOV   R4,A
	INC   DPTR
	JMP   SHOWCP

UB_A_RD:CLR   F0
	MOV   DPL,UI_AU
	MOV   DPH,UI_AU+1
	MOV   A,#OU_A_RD
	MOVC  A,@A+DPTR
	PUSH  ACC
	MOV   A,#OU_A_RD+1
	MOVC  A,@A+DPTR
	PUSH  ACC
	RET

UB_A_WR:CLR   F0
	MOV   DPL,UI_AU
	MOV   DPH,UI_AU+1
	MOV   A,#OU_A_WR
	MOVC  A,@A+DPTR
	PUSH  ACC
	MOV   A,#OU_A_WR+1
	MOVC  A,@A+DPTR
	PUSH  ACC
	RET

UB_DRAW:MOV   A,#' '
UB_DRAW1:PUSH ACC
	CALL  GOTO_A
	POP   B
	JZ    UB_DRAW9
	MOV   R1,B
	MOV   DPL,UI_AU
	MOV   DPH,UI_AU+1
	MOV   A,#OU_SX
	MOVC  A,@A+DPTR
	JZ    UB_DRAW9
	MOV   R0,A
UB_DRAW2:MOV  A,R1
	CALL  UI_WR
	DJNZ  R0,UB_DRAW2
UB_DRAW9:RET

UB_WRD1:CALL  UI_WR
	INC   DPTR
	DEC   R6
UB_WRDPN:MOVX A,@DPTR
	JZ    UB_WRD5
	CJNE  A,#C_NL,UB_WRD2
	SJMP  UB_WRD5
UB_WRD2:INC   R6
	DJNZ  R6,UB_WRD1
UB_WRD5:MOV   A,R6
	JZ    UB_WRD9
UB_WRD7:MOV   A,#' '
	CALL  UI_WR
	DJNZ  R6,UB_WRD7
UB_WRD9:RET

; Vypise text z DPTR
UB_WRDP:MOVX  A,@DPTR
	JZ    UB_WRD9
	CALL  UI_WR
	INC   DPTR
	SJMP  UB_WRDP

; *******************************************************************
; Rutiny cteni a zapisu dat volane pres OU_A_RD a OU_A_WR

NULL_A:	SETB  F0
	CLR   A
	RET

UR_MiT:	CJNE  R1,#ET_REFR,UR_Mi
	CLR   A
	RET
UR_Mi:	MOV   A,#OU_DP
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OU_DP+1
	MOVC  A,@A+DPTR
	JZ    UR_Mi1
	MOV   DPL,R0
	MOV   DPH,A
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   A,#1
	RET
UR_Mi1:	MOV   A,@R0
	MOV   R4,A
	INC   R0
	MOV   A,@R0
	MOV   R5,A
	MOV   A,#1
	RET

UW_Mi:	MOV   A,#OU_DP
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OU_DP+1
	MOVC  A,@A+DPTR
	JZ    UW_Mi1
	MOV   DPL,R0
	MOV   DPH,A
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   A,#1
	RET
UW_Mi1:	MOV   A,R4
	MOV   @R0,A
	INC   R0
	MOV   A,R5
	MOV   @R0,A
	MOV   A,#1
	RET

; Zpracovani klaves
UB_SFT:	CALL  UB_SFT1
	JZ    UB_SFT9
	JMP   EV_CLR
UB_SFT1:%UI_OU2DP(OU_SFT)
UB_SFT2:MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	ORL   A,R0
	JZ    UB_SFT9
	MOV   A,EV_DATA
	MOV   R7,A
	JMP   SEL_FNC
UB_SFT9:RET

; *******************************************************************
; Ciselny vstup

UIN_EV:	MOV   A,EV_TYPE
	CJNE  A,#ET_REFR,UIN_E10	; REFRESH
	CALL  UB_GFLG
	JB    ACC.UFB_EDV,UIN_E09
	JMP   UIN_DAR
UIN_E09:RET
UIN_E10:CJNE  A,#ET_DRAW,UIN_E20	; DRAW
	CALL  UB_GFLG
	JNB   ACC.UFB_EDV,UIN_E16
	JMP   UIN_DE
UIN_E16:JMP   UIN_DA
UIN_E20:CJNE  A,#ET_GETF,UIN_E30	; GET FOCUS
	CALL  UB_GFT
	JNZ   UIN_E2R

	CALL  EV_CLR
	JMP   UB_SHCP
UIN_E2R:RET
UIN_E30:CJNE  A,#ET_RELF,UIN_E40	; RELEASE FOCUS
	MOV   DPL,UI_AD
	MOV   DPH,UI_AD+1
	INC   DPTR
	MOVX  A,@DPTR
	ANL   A,#NOT (UFM_FOC OR UFM_EDV)
	MOVX  @DPTR,A
	JMP   UIN_DA
UIN_E6V:JMP   UIN_E60
UIN_E40:CJNE  A,#ET_KEY,UIN_E6V		; KLAVESA
	MOV   A,EV_DATA
	CJNE  A,#K_ENTER,UIN_E44
	CALL  UB_GFLG
	JNB   ACC.UFB_EDV,UIN_E47
	CALL  EV_CLR
	CALL  UIN_VEN
	JB    F0,UIN_E41
	MOV   R1,#0
	CALL  UB_A_WR
	JB    F0,UIN_E41
	MOV   R7,#ET_KEY
	MOV   R4,#KV_V_OK
	MOV   R5,#0
	CALL  EV_POST
	JMP   UIN_E47
UIN_E41:MOV   R7,#ET_ERR		; Error pri ENTERu
	MOV   A,UI_AU
	ADD   A,#OU_I_E
	MOV   R4,A
	MOV   A,UI_AU+1
	ADDC  A,#0
	MOV   R5,A
	CALL  EV_POST
	JMP   UIN_E47

UIN_E44:CJNE  A,#K_DP,UIN_E45
	MOV   DPL,UI_AU		; desetinna tecka
	MOV   DPH,UI_AU+1
	MOV   A,#OU_I_F
	MOVC  A,@A+DPTR
	ANL   A,#7		; max maska desetin
	JZ    UIN_E47
	CALL  UIN_VRT		; nacte R4567 a R1, R2
	MOV   A,R2
	ORL   A,#80H
	MOV   R2,A
	JMP   UIN_E5W

UIN_E45:CJNE  A,#K_PM,UIN_E46
	MOV   DPL,UI_AU
	MOV   DPH,UI_AU+1
	MOV   A,#OU_I_F
	MOVC  A,@A+DPTR
	JNB   ACC.7,UIN_E47
	CALL  UIN_VRT		; nacte R4567 a R1, R2
	CLR   C
	CLR   A
	SUBB  A,R4		; NEGl
	MOV   R4,A
	CLR   A
	SUBB  A,R5
	MOV   R5,A
	CLR   A
	SUBB  A,R6
	MOV   R6,A
	CLR   A
	SUBB  A,R7
	MOV   R7,A
	JMP   UIN_E5W

UIN_E46:MOV   DPTR,#TK_DEC
	MOV   A,EV_DATA
	CALL  CNV_KEY
	JNC   UIN_53
UIN_E47:CALL  UB_SFT1		; Ignorovana klavesa
	JZ    UIN_E48
	CALL  EV_CLR
UIN_E48:MOV   DPL,UI_AD
	MOV   DPH,UI_AD+1
	INC   DPTR
	MOVX  A,@DPTR
	JBC   ACC.UFB_EDV,UIN_E49
	RET
UIN_E49:MOVX  @DPTR,A
	JMP   UIN_DA

UIN_53:	MOV   EV_DATA+1,A
	CALL  UIN_VRT		; nacte R4567 a R1, R2
	MOV   A,R2
	JNB   ACC.7,UIN_E54
	INC   R2
	XRL   A,R1
	ANL   A,#7		; test max poctu desetin
	JZ    UIN_E59
UIN_E54:CALL  UIN_MT		; Nasobeni 10
	JNZ   UIN_E59
	MOV   B,R1
	JNB   B.7,UIN_E56	; Bez znamenka
	MOV   A,R5
	JNB   B.6,UIN_E55
	MOV   A,R7
UIN_E55:JB    ACC.7,UIN_E57
UIN_E56:MOV   A,R4
	ADD   A,EV_DATA+1
	MOV   R4,A
	MOV   A,R5
	ADDC  A,#0
	MOV   R5,A
	JNB   B.6,UIN_E58
	MOV   A,R6
	ADDC  A,#0
	MOV   R6,A
	MOV   A,R7
	ADDC  A,#0
	MOV   R7,A
	SJMP  UIN_E58
UIN_E57:CLR   C
	MOV   A,R4
	SUBB  A,EV_DATA+1
	MOV   R4,A
	MOV   A,R5
	SUBB  A,#0
	MOV   R5,A
	JNB   B.6,UIN_E58
	MOV   A,R6
	SUBB  A,#0
	MOV   R6,A
	MOV   A,R7
	SUBB  A,#0
	MOV   R7,A
UIN_E58:ANL   C,/B.7
	JC    UIN_E59
	JB    OV,UIN_E59
UIN_E5W:CALL  UIN_VWR		; ulozi R4567 a R1, R2
UIN_E59:CALL  EV_CLR
	JMP   UIN_DE
UIN_E60:
	JMP   UB_EV

; Draw actual ounly if refresh needed
UIN_DAR:CALL  GOTO_A
	JZ    UIN_DA9
	MOV   R1,#ET_REFR
	CALL  UB_A_RD
	SJMP  UIN_DA1
; Draw actual data
UIN_DA:	CALL  GOTO_A
	JZ    UIN_DA9
	MOV   R1,#0
	CALL  UB_A_RD
UIN_DA1:JB    F0,UIN_DA2
	JNZ   UIN_DA3
	MOV   A,EV_TYPE
	CJNE  A,#ET_DRAW,UIN_DA9
UIN_DA2:MOV   A,#'-'
	JMP   UB_DRAW1
UIN_DA3:MOV   DPL,UI_AU
	MOV   DPH,UI_AU+1
	MOV   A,#OU_I_F
	MOVC  A,@A+DPTR
UIN_DA5:MOV   R3,A
%IF (0) THEN (
;	JB    ACC.6,UIN_DA8	; longint vystup
	ANL   A,#07CH
	JNZ   UIN_DA8		; longint vystup
	MOV   A,#OU_SX
	MOVC  A,@A+DPTR
	SWAP  A
	ANL   A,#070H
	ORL   A,R3
	MOV   R7,A
	JMP   PRINTi		; !!!!!!!!rozvinout
) FI
UIN_DA8:MOV   A,#OU_SX
	MOVC  A,@A+DPTR
	MOV   R2,A
	JMP   PRINTli
UIN_DA9:RET

; Draw edited data
UIN_DE:	CALL  GOTO_A
	JZ    UIN_DA9
	CALL  UIN_VRD		; nacte R4567 a R1, R2
	MOV   A,R1
	ANL   A,#NOT 7		; vymaskovat desetiny
	MOV   R1,A
	MOV   A,R2
	ANL   A,#7		; Max pocet desetin
	ORL   A,R1
	MOV   DPL,UI_AU
	MOV   DPH,UI_AU+1
	JMP   UIN_DA5

; R4567=R4567*10
; rusi R1
MULTENl:MOV   A,R1
	ORL   A,#0C0H
	MOV   R1,A

; R4567=R4567*10 a R1 format
; pokud se vysledek vejde do formatu vraci ACC=0
MULTENlF:
UIN_MT: MOV   A,R4
	MOV   B,#10
	MUL   AB
	MOV   R4,A
	MOV   A,B
	XCH   A,R5
	MOV   B,#10
	MUL   AB
	ADD   A,R5
	MOV   R5,A
	MOV   A,R1
	JB    ACC.6,UIN_MT1
	MOV   A,R5
	SJMP  UIN_MT2
UIN_MT1:MOV   A,B
	ADDC  A,#0
	XCH   A,R6
	MOV   B,#10
	MUL   AB
	ADD   A,R6
	MOV   R6,A
	MOV   A,B
	ADDC  A,#0
	XCH   A,R7
	MOV   B,#10
	MUL   AB
	ADD   A,R7
	MOV   R7,A
UIN_MT2:XCH   A,B
	ADDC  A,#0
	MOV   C,B.7
	MOV   B,R1
	JNB   B.7,UIN_MT3
	ADDC  A,#0
	CJNE  A,#10,UIN_MT3
	CLR   A
UIN_MT3:RET

UIN_VRT:%UI_OUD2DP(OUD_FLG)
	MOVX  A,@DPTR
	JB    ACC.UFB_EDV,UIN_VRD
	ORL   A,#UFM_EDV
	MOVX  @DPTR,A
	%UI_OUD2DP(OUD_I_V)
	MOV   R0,#5
	CLR   A
UIN_VR1:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,UIN_VR1
; nacte R4567 a R1, R2
UIN_VRD:MOV   DPL,UI_AU
	MOV   DPH,UI_AU+1
	MOV   A,#OU_I_F
	MOVC  A,@A+DPTR
	MOV   R1,A
	MOV   DPL,UI_AD
	MOV   DPH,UI_AD+1
	MOV   A,#OUD_I_V
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OUD_I_V+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   A,#OUD_I_V+2
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   A,#OUD_I_V+3
	MOVC  A,@A+DPTR
	MOV   R7,A
	MOV   A,#OUD_I_DP
	MOVC  A,@A+DPTR
	MOV   R2,A
	RET

; ulozi R4567 a R1, R2
UIN_VWR:%UI_OUD2DP(OUD_I_V)
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R2
	MOVX  @DPTR,A
	RET

UIN_VEN:CLR   F0
	CALL  UIN_VRD
UIN_VE1:MOV   A,R1
	XRL   A,R2
	ANL   A,#7		; Max pocet desetin
	JZ    UIN_VE2
	CALL  UIN_MT
	JNZ   UIN_VE8
	INC   R2
	SJMP  UIN_VE1
UIN_VE2:MOV   B,R1
	MOV   A,R5
	JNB   B.6,UIN_VE3
	MOV   A,R7		; !!!!!! v budoucnu kontrolovat vse
	SJMP  UIN_VE7		; pro long int se nic nekontroluje
UIN_VE3:MOV   C,ACC.7
	ANL   C,B.7
	MOV   DPL,UI_AU
	MOV   DPH,UI_AU+1
	JC    UIN_VE4
	MOV   A,#OU_I_H
	MOVC  A,@A+DPTR
	CLR   C
	SUBB  A,R4
	MOV   A,#OU_I_H+1
	MOVC  A,@A+DPTR
	SUBB  A,R5
	JC    UIN_VE8
	JB    B.7,UIN_VE5
	MOV   A,#OU_I_L
	MOVC  A,@A+DPTR
	SETB  C
	SUBB  A,R4
	MOV   A,#OU_I_L+1
	MOVC  A,@A+DPTR
	SUBB  A,R5
	JNC   UIN_VE8
	SJMP  UIN_VE5
UIN_VE4:MOV   A,#OU_I_L
	MOVC  A,@A+DPTR
	SETB  C
	SUBB  A,R4
	MOV   A,#OU_I_L+1
	MOVC  A,@A+DPTR
	SUBB  A,R5
	JNC   UIN_VE8
UIN_VE5:


UIN_VE7:RET
UIN_VE8:SETB  F0
	RET


TK_DEC:	DB    K_0,0
	DB    K_1,1
	DB    K_2,2
	DB    K_3,3
	DB    K_4,4
	DB    K_5,5
	DB    K_6,6
	DB    K_7,7
	DB    K_8,8
	DB    K_9,9
	DB    -1

CNV_KEY:MOV   R0,A
	SJMP  CNV_KE2
CNV_KE1:INC   DPTR
	XRL   A,R0
	JZ    CNV_KE3
	INC   DPTR
CNV_KE2:MOVX  A,@DPTR
	CJNE  A,#-1,CNV_KE1
	SETB  C
	RET
CNV_KE3:CLR   C
	MOVX  A,@DPTR
	RET

; Prevod cisla v R4567 do retezce v DPTR
; format R3 a R2
xITOAF:	SETB F0
	SJMP  PRTli01

; Tisk longint cisla v R4567
; R3 .. SLZxxDDD - S .. signed, L .. longint, Z .. fill by 0
;		   DDD .. desetiny
; R2 .. delka vypisu, 0 znamena promennou delku
PRINTli:CLR   F0
PRTli01:MOV   A,R3
	MOV   R1,A	; format cisla
	MOV   R3,#0
	JB    ACC.6,PRTli05
	MOV   R6,#0
	MOV   R7,#0
	MOV   A,R5
	SJMP  PRTli07
PRTli05:MOV   A,R7
PRTli07:ORL   A,#07FH
	ANL   A,R1
	MOV   R1,A
	JNB   ACC.7,PRTli10
	DEC   R3	; kvuli '-' vystup delsi
	CLR   C
	CLR   A
	SUBB  A,R4
	MOV   R4,A
	CLR   A
	SUBB  A,R5
	MOV   R5,A
	MOV   A,R1
	JNB   ACC.6,PRTli10
	CLR   A
	SUBB  A,R6
	MOV   R6,A
	CLR   A
	SUBB  A,R7
	MOV   R7,A
PRTli10:MOV   R0,#K10l-PRTliSk
	MOV   A,R1
	ANL   A,#7	; pocet desetinnych mist
	JZ    PRTli15
	DEC   R3	; '.' prodluzuje vystup
	RL    A
	RL    A
	CPL   A
	INC   A
	ADD   A,R0
	MOV   R0,A
PRTli15:SETB  C
	CALL  PRTliS
	SUBB  A,R4
	CALL  PRTliS
	SUBB  A,R5
	CALL  PRTliS
	SUBB  A,R6
	CALL  PRTliS
	SUBB  A,R7
	MOV   A,R0
	JNC   PRTli20
	ADD   A,#-8
	MOV   R0,A
	CJNE  A,#K10E9l-PRTliSk-4,PRTli15
	ADD   A,#4
	MOV   R0,A
PRTli20:ADD   A,#PRTliSk-K1l-4
	RR    A
	RR    A
	ORL   A,#0C0H
	ADD   A,R3	; v R3 zaporny pocet znaku k vypsani vseho
	CJNE  R2,#0,PRTli21
	CPL   A		; promenna delka vypisu, vypis vse
	INC   A
	MOV   R2,A
	SJMP  PRTli25
PRTli21:ADD   A,R2	; pevna delka vypisu
	MOV   R3,A
	JZ    PRTli25
	JNB   ACC.7,PRTli23
	MOV   A,R1
	ANL   A,#7
	ADD   A,R3
	JNZ   PRTli25
	MOV   R3,#1
PRTli23:MOV   A,R1
	JB    ACC.5,PRTli27
PRTli24:MOV   A,#' '	; doplneni mezerou
	CALL  PRTliWC
	DEC   R2
	DJNZ  R3,PRTli24
PRTli25:MOV   A,R1
	JNB   ACC.7,PRTli30
	MOV   A,#'-'
	CALL  PRTliWC
	DJNZ  R2,PRTli30
	CLR   F0
	RET
PRTli27:MOV   A,R1	; doplneni znakem '0'
	JNB   ACC.7,PRTli28
	MOV   A,#'-'
	CALL  PRTliWC
	DEC   R2
PRTli28:MOV   A,#'0'
	CALL  PRTliWC
	DEC   R2
	DJNZ  R3,PRTli28
PRTli30:MOV   A,R1	; vypis cislic a znaku '.'
	ANL   A,#7	; pocet desetinnych mist
	RL    A
	RL    A
	CPL   A
	INC   A
	ADD   A,#K1l+4-PRTliSk
	MOV   R1,A      ; R2 pocet znaku do konce pole
PRTli40:MOV   A,R1	; R1 ukazuje na K10Ex kde bude '.'
	XRL   A,R0	; R0 ukazatel na pristi K10Ex
	JNZ   PRTli45
	MOV   A,#'.'
	CALL  PRTliWC
	DJNZ  R2,PRTli45
	CLR   F0
	RET
PRTli45:MOV   R3,#'0'-1	; R3 vypocet ASCII cislice
	CLR   C
PRTli50:INC   R3
	CALL  PRTliS	; R4567-[R0]
	XCH   A,R4
	SUBB  A,R4
	MOV   R4,A
	CALL  PRTliS
	XCH   A,R5
	SUBB  A,R5
	MOV   R5,A
	CALL  PRTliS
	XCH   A,R6
	SUBB  A,R6
	MOV   R6,A
	CALL  PRTliS
	XCH   A,R7
	SUBB  A,R7
	MOV   R7,A
	DEC   R0
	DEC   R0
	DEC   R0
	DEC   R0
	JNC   PRTli50
	MOV   A,R3	; Tisk cislice
	CALL  PRTliWC
	SETB  C		; R4567+[R0++]
	CALL  PRTliS
	ADD   A,R4
	MOV   R4,A
	CALL  PRTliS
	ADDC  A,R5
	MOV   R5,A
	CALL  PRTliS
	ADDC  A,R6
	MOV   R6,A
	CALL  PRTliS
	ADDC  A,R7
	MOV   R7,A
	DJNZ  R2,PRTli40
	CLR   F0
	RET

PRTliWC:JNB   F0,PRTliWC1
	MOVX  @DPTR,A
	INC   DPTR
	RET
PRTliWC1:JMP  UI_WR

PRTliS:	MOV   A,R0
	INC   R0
	MOVC  A,@A+PC
PRTliSk:RET

K10E9l:	DB    000H,0CAH,09AH,03BH
	DB    000H,0E1H,0F5H,005H
	DB    080H,096H,098H,000H
	DB    040H,042H,00FH,000H
	DB    0A0H,086H,001H,000H
K10E4l:	DB    010H,027H,000H,000H
	DB    0E8H,003H,000H,000H
	DB    064H,000H,000H,000H
K10l:   DB    00AH,000H,000H,000H
K1l:    DB    001H,000H,000H,000H


; *******************************************************************
; Text volajici akci

BUT_EV:	MOV   A,EV_TYPE
BUT_E10:CJNE  A,#ET_DRAW,BUT_E20	; DRAW
	CALL  GOTO_A
	JZ    BUT_E1R
	%UI_OU2DP(OU_B_T)
	CALL  UB_WRDP
	CALL  UB_GFLG
	JNB   ACC.UFB_FOC,BUT_E1R
BUT_E15:%UI_OU2DP(OU_B_S)
	JMP   UB_DRS1
BUT_E1R:RET
BUT_E20:CJNE  A,#ET_GETF,BUT_E30	; GET FOCUS
	CALL  UB_GFT
	JNZ   BUT_E1R
	CALL  EV_CLR
	CALL  UB_SHCP
	JMP   BUT_E15
BUT_E30:CJNE  A,#ET_RELF,BUT_E40	; RELEASE FOCUS
	%UI_OU2DP(OU_B_S)
BUT_E32:MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	ORL   A,R0
	JZ    BUT_E35
	SETB  FL_DRAW
BUT_E35:JMP   UB_RFT
BUT_E40:CJNE  A,#ET_KEY,BUT_E50		; KLAVESA
	MOV   A,EV_DATA
	CJNE  A,#K_ENTER,BUT_E44
	%UI_OU2DP(OU_B_P)
BUT_E42:MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R0
	MOV   DPH,A
	ORL   A,R0
	JZ    BUT_E44
	CALL  JMP_VE1
	CALL  EV_CLR
BUT_E44:JMP   UB_SFT
BUT_E50:JMP   UB_EV

; *******************************************************************
; Multi text volajici akci

MUT_EV:	MOV   A,EV_TYPE
	CJNE  A,#ET_REFR,MUT_E10	; REFRESH
	SJMP  MUT_E16
MUT_E10:CJNE  A,#ET_DRAW,MUT_E20	; DRAW
	CALL  UB_GFLG
	JNB   ACC.UFB_FOC,MUT_E16
MUT_E15:%UI_OU2DP(OU_M_S)
	CALL  UB_DRS1
MUT_E16:CALL  GOTO_A
	JZ    MUT_E1R
	MOV   R1,EV_TYPE
	CALL  UB_A_RD
	JB    F0,MUT_E19
	JZ    MUT_E18
	MOV   DPL,UI_AU
	MOV   DPH,UI_AU+1
	MOV   A,#OU_SX
	MOVC  A,@A+DPTR		; OU_SX
	MOV   R6,A
	%UI_OU2DP(OU_M_T)
	CALL  FND_MINT
	JZ    MUT_E19
	CALL  UB_WRDPN
MUT_E1R:RET
MUT_E18:MOV   A,EV_TYPE
	XRL   A,#ET_REFR
	JZ    MUT_E1R
MUT_E19:MOV   A,#'-'
	JMP   UB_DRAW1
MUT_E20:CJNE  A,#ET_GETF,MUT_E30	; GET FOCUS
	CALL  UB_GFT
	JNZ   MUT_E1R
	CALL  EV_CLR
	CALL  UB_SHCP
	JMP   MUT_E15
MUT_E30:CJNE  A,#ET_RELF,MUT_E40	; RELEASE FOCUS
	%UI_OU2DP(OU_M_S)
	JMP   BUT_E32
MUT_E40:CJNE  A,#ET_KEY,MUT_E50		; KLAVESA
	MOV   A,EV_DATA
	CJNE  A,#K_ENTER,MUT_E42
	%UI_OU2DP(OU_M_P)
	JMP   BUT_E42
MUT_E42:JMP   UB_SFT
MUT_E50:JMP   UB_EV

FND_MI1:INC   DPTR
	INC   DPTR
FND_MINT:MOVX A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX A,@DPTR
	MOV   R1,A
	INC   DPTR
	ORL   A,R0
	JZ    FND_MIR
	MOVX A,@DPTR
	XRL   A,R4
	ANL   A,R0
	MOV   R0,A
	INC   DPTR
	MOVX A,@DPTR
	XRL   A,R5
	ANL   A,R1
	INC   DPTR
	ORL   A,R0
	JNZ   FND_MI1
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	ORL   A,R0
FND_MIR:RET

; *******************************************************************
; Skupina polozek

GR_PUA:	MOV   DPTR,#GR_UACT
	MOVX  A,@DPTR
; Nalezne ukazatel na N-tou polozku skupiny
; vstup: A cislo polozky, vystup: DPTR
GR_PUN: JB    ACC.7,GR_PUN9
	RL    A
	ADD   A,#OGR_PU
	MOV   R0,A
	MOV   DPTR,#GR_ACT
	MOVX  A,@DPTR
	ADD   A,R0
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#0
	MOV   DPH,A
	MOV   DPL,R0
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	ORL   A,R0
	RET
GR_PUN9:CLR   A
	RET

; Zpracovani udalosti skupinou polozek
GR_EV:	MOV   A,EV_TYPE
	; Zde muze byt osetreni nekterych udalosti
	CJNE  A,#ET_INIT,GR_EV10
	MOV   DPTR,#GR_UFOC	; Inicializace skupiny
	MOV   A,#-1
	MOVX  @DPTR,A
	JMP   GR_EA
GR_EV10:CJNE  A,#ET_DRAW,GR_EV20
	CALL  GR_DRAW		; Draw
	CALL  GR_DRS
	MOV   A,#ET_DRAW
GR_EV20:MOV   DPTR,#ET_FLGS
	MOVC  A,@A+DPTR
	JNB   ACC.ETB_FOC,GR_EA
GR_EF:	MOV   DPTR,#GR_UFOC	; Focusovana udalost
	MOVX  A,@DPTR
	MOV   R0,A
	INC   A
	JZ    GR_EVMY
%IF (%STATIC_UI_AD) THEN (
	MOV   DPTR,#GR_UFFL
	MOVX  A,@DPTR
	MOV   DPL,UI_AD
	MOV   DPH,UI_AD+1
	INC   DPTR
	MOVX  @DPTR,A
)FI
	MOV   A,R0
	CALL  GR_PUN		; Ukazatel na polozku
	JZ    GR_EVMY
	MOV   UI_AU,DPL		; Naplneni UI_AU
	MOV   UI_AU+1,DPH
				; UI_AD zatim vzdy konstantni
	CALL  JMP_VEV		; UI_AU a UI_AD musi byt aktualni
%IF (%STATIC_UI_AD) THEN (
	MOV   DPL,UI_AD
	MOV   DPH,UI_AD+1
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPTR,#GR_UFFL
	MOVX  @DPTR,A
	JB    ACC.UFB_FOC,GR_EF30
	MOV   DPTR,#GR_UFOC
	MOV   A,#-1
	MOVX  @DPTR,A
GR_EF30:
)FI
GR_EVMY:MOV   A,EV_TYPE
	JZ    GR_ERET
	; Zde muze byt osetreni nekterych udalosti
	CJNE  A,#ET_KEY,GR_EV55
	MOV   DPTR,#GR_ACT
	MOVX  A,@DPTR
	MOV   UI_AU,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   UI_AU+1,A
	MOV   DPL,UI_AU
	MOV   DPH,A
	MOV   A,#OGR_SFT	; Zpracovani klaves skupinou
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OGR_SFT+1
	MOVC  A,@A+DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOV   A,EV_DATA
	MOV   R7,A
	CALL  SEL_FNC
	SJMP  GR_ERET
GR_EV55:CJNE  A,#ET_CNAV,GR_EV58
	JMP   GR_CNAV
GR_EV58:CJNE  A,#ET_HLP,GR_EV60
	MOV   DPTR,#GR_ACT
	MOVX  A,@DPTR
	ADD   A,#OGR_HLP
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#0
	MOV   DPH,A
	MOV   DPL,R0
	JMP   UB_HLP1
GR_EV60:
GR_ERET:RET

GR_EA:	MOV   DPTR,#GR_UACT	; Pro vsechny
	MOV   A,#-1
	MOVX  @DPTR,A
GR_EA10:MOV   A,EV_TYPE
	JZ    GR_ERET
	MOV   DPTR,#GR_UACT
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
%IF (%STATIC_UI_AD) THEN (
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	XRL   A,R0
	JZ    GR_EA20
	CLR   A
	SJMP  GR_EA25
GR_EA20:INC   DPTR
	MOVX  A,@DPTR
GR_EA25:MOV   DPL,UI_AD
	MOV   DPH,UI_AD+1
	INC   DPTR
	MOVX  @DPTR,A
	MOV   A,R0
)FI
	CALL  GR_PUN		; Ukazatel na polozku
	JZ    GR_EVMY
	MOV   UI_AU,DPL		; Naplneni UI_AU
	MOV   UI_AU+1,DPH
				; UI_AD zatim vzdy konstantni
	CALL  JMP_VEV		; UI_AU a UI_AD musi byt aktualni
%IF (%STATIC_UI_AD) THEN (
	MOV   DPL,UI_AD
	MOV   DPH,UI_AD+1
	INC   DPTR
	MOVX  A,@DPTR
	JNB   ACC.UFB_FOC,GR_EA55
	MOV   DPTR,#GR_UFFL
	MOVX  @DPTR,A
	MOV   DPTR,#GR_UACT
	MOVX  A,@DPTR
	INC   DPTR
	MOVX  @DPTR,A
	SJMP  GR_EA60
GR_EA55:MOV   DPTR,#GR_UACT
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	XRL   A,R0
	JNZ   GR_EA60
	MOV   A,#-1
	MOVX  @DPTR,A
GR_EA60:
)FI
	JMP   GR_EA10

GR_CNAT:MOV   A,R2
	CALL  GR_PUN
	SETB  C
	JZ    GR_CNAR
	MOV   A,#OU_MSK
	MOVC  A,@A+DPTR
	ANL   A,#UFM_FOC
	CLR   C
GR_CNAR:RET

; Pohyby cursoru
GR_CNAV:MOV   DPTR,#GR_UFOC
	MOVX  A,@DPTR
	MOV   R2,A
	INC   A
	JZ    GR_CN80
	MOV   A,EV_DATA
	CJNE  A,#ETC_NXT,GR_CN10
GR_CN05:INC   R2
	CALL  GR_CNAT
	JC    GE_CN79
	JZ    GR_CN05
	JMP   GR_CN82
GR_CN10:CJNE  A,#ETC_PRE,GR_CN20
GR_CN15:DEC   R2
	CALL  GR_CNAT
	JC    GE_CN79
	JZ    GR_CN15
	JMP   GR_CN82
GR_CN20:
GE_CN79:RET
GR_CN80:CLR   A
	MOV   R4,A
	MOV   R5,A
	SJMP  GR_CN85
GR_CN82:MOV   R4,DPL
	MOV   R5,DPH
GR_CN85:CALL  EV_CLR
	MOV   R7,#ET_RQF
	JMP   EV_POST

; Draw group background
GR_DRAW:CLR   A
	MOV   DPTR,#GR_TMP+2
	MOVX  @DPTR,A
	%UI_OU2DP(OGR_BTXT)
	CALL  xLDR45i
	ORL   A,R4
	JZ    GR_DR60
	MOV   DPTR,#GR_TMP
	CALL  xSVR45i
GR_DR10:MOV   DPTR,#GR_TMP+2
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   R4,#0
	CALL  GOTOXY
	JZ    GR_DR40
	MOV   DPTR,#GR_TMP
	CALL  xMDPDP
	CALL  UB_WRDPN
GR_DR40:MOV   DPTR,#GR_TMP+2
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	MOV   DPTR,#GR_TMP
	CALL  xMDPDP
	MOV   A,#C_NL
	CALL  US_FNCH
	JZ    GR_DR60
	MOV   R4,DPL
	MOV   R5,DPH
	MOV   DPTR,#GR_TMP
	CALL  xSVR45i
	JMP   GR_DR10
GR_DR50:MOV   A,#' '
	CALL  UI_WR
	DJNZ  R6,GR_DR50
	MOV   DPTR,#GR_TMP+2
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
GR_DR60:MOV   DPTR,#GR_TMP+2
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   R4,#0
	CALL  GOTOXY
	JNZ   GR_DR50
	JB    F0,GR_DR99
	MOV   A,R5
	JNB   ACC.7,GR_DR99
	MOV   DPTR,#GR_TMP+2
	MOVX  A,@DPTR
	CLR   C
	SUBB  A,R5
	MOVX  @DPTR,A
	JB    ACC.7,GR_DR99
	MOV   R5,A
	MOV   R4,#0
	CALL  GOTOXY
	JNZ   GR_DR50
GR_DR99:RET

GR_DRS:	MOV   DPTR,#GR_ACT
	MOVX  A,@DPTR
	ADD   A,#OGR_STXT
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#0
	MOV   DPH,A
	MOV   DPL,R0
UB_DRS1:MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   B,A
	ORL   A,R0
	JZ    GR_DRS9
	MOV   A,R0
	PUSH  ACC
	PUSH  B
	CALL  GOTO_S
	POP   DPH
	POP   DPL
	JZ    GR_DRS9
GR_DRS7:JMP   UB_WRDP
GR_DRS9:RET

; Vyvola skupinu na adrese R23
GR_RQ23:MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	MOV   R7,#ET_RQGR
	JMP   EV_POST

; Hleda prvni vyskyt znaku A v stringu DPTR
; rusi R0, pri nalezeni ukazuje DPTR za znak
US_FNCH:MOV   R0,A
US_FNC1:MOVX  A,@DPTR
	JZ    US_FNC2
	INC   DPTR
	XRL   A,R0
	JNZ   US_FNC1
	MOV   A,#1
US_FNC2:RET

; Priznaky pro zpracovavani jednotlivych udalosti
ET_FLGS:DB    0		; 0
	DB    0		; ET_INIT
	DB    0		; ET_DONE
	DB    0		; ET_GETF
	DB    0		; ET_RELF
	DB    ETM_FOC	; ET_KEY
	DB    0		; ET_VAL
	DB    0		; ET_RQF
	DB    0		; ET_RQGR
	DB    0		; ET_DRAW
	DB    0		; ET_REFR
	DB    0		; ET_CNAV
	DB    0		; ET_ERR
	DB    ETM_FOC	; ET_HLP
	DB    0		; ET_RQHLP
	DB    0		; ET_GLOB
	DB    0		;
	DB    0		;

%IF(0) THEN (
; *******************************************************************
; Testovaci rutiny a pamet

PUBLIC	UT

RSEG	PB_UI_X

UT_UIAD:DS    40
UT_DATA:DS    40

RSEG	PB_UI_C

UT_INIT:SETB  D4LINE
	SETB  FL_CMAV
	MOV   DPTR,#UI_MV_SX
	MOV   A,#20
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#4
	MOVX  @DPTR,A
	MOV   DPTR,#UT_UIAD
	MOV   UI_AD,DPL
	MOV   UI_AD+1,DPH
	CLR   A
	MOV   DPTR,#EV_BUF
	MOVX  @DPTR,A
	MOV   DPTR,#GR_ACT
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	RET

UT:     CALL  UT_INIT
	MOV   R7,#ET_RQGR
	%LDR45i(UT_GR1)
	CALL  EV_POST

UT_ML:	CALL  EV_GET
	JZ    UT_ML50
	CALL  EV_DO
UT_ML50:
	JMP   UT_ML

UT_G1K3:MOV   R7,#0
UT_G001:MOV   A,#'*'
	CALL  UI_WR
	DJNZ  R7,UT_G001
	SETB  FL_DRAW
	RET

UT_G1K2:CALL  UI_CLR
UT_G1K1:SETB  FL_DRAW
	RET

UT_GR1:	DS    UT_GR1+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR1+OGR_BTXT-$
	%W    (0)
	DS    UT_GR1+OGR_STXT-$
	%W    (0)
	DS    UT_GR1+OGR_HLP-$
	%W    (0)
	DS    UT_GR1+OGR_SFT-$
	%W    (UT_SF1)
	DS    UT_GR1+OGR_PU-$
	%W    (UT_U1)
	%W    (UT_U2)
	%W    (UT_U3)
	%W    (UT_U4)
	%W    (UT_U5)
	%W    (UT_U6)
	%W    (0)

UT_GR2:	DS    UT_GR2+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR2+OGR_BTXT-$
	%W    (UT_GT2)
	DS    UT_GR2+OGR_STXT-$
	%W    (UT_GS2)
	DS    UT_GR2+OGR_HLP-$
	%W    (0)
	DS    UT_GR2+OGR_SFT-$
	%W    (UT_SF1)
	DS    UT_GR2+OGR_PU-$
	%W    (UT_U3)
	%W    (UT_U4)
	%W    (UT_U5)
	%W    (UT_U6)
	%W    (0)

UT_GT2:	DB    'Tohle je druha skupina',C_NL
	DB    '1 radka',C_NL
	DB    '2 radka',C_NL
	DB    '3 radka',C_NL
	DB    '4 radka',C_NL
	DB    '5 radka',C_NL
	DB    '6 radka',0

UT_GS2:	DB    '=== === === === === ',0

UT_U1:	DS    UT_U1+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1+OU_X-$
	DB    2,1,7,1
	DS    UT_U1+OU_HLP-$
	%W    (0)
	DS    UT_U1+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1+OU_A_RD-$
	%W    (NULL_A)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U1+OU_I_F-$
	DB    82H		; format I_F
	%W    (-1000)		; I_L
	%W    (5000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2:	DS    UT_U2+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2+OU_X-$
	DB    7,0,6,1
	DS    UT_U2+OU_HLP-$
	%W    (0)
	DS    UT_U2+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U2+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UT_DATA)		; DP
	DS    UT_U2+OU_I_F-$
	DB    80H		; format I_F
	%W    (-1000)		; I_L
	%W    (5000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U3:	DS    UT_U3+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U3+OU_MSK-$
	DB    0
	DS    UT_U3+OU_X-$
	DB    10,1,2,1
	DS    UT_U3+OU_HLP-$
	%W    (0)
	DS    UT_U3+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U3+OU_A_RD-$
	%W    (NULL_A)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U3+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U3+OU_I_F-$
	DB    82H		; format I_F
	%W    (-1000)		; I_L
	%W    (5000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U4:	DS    UT_U4+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U4+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U4+OU_X-$
	DB    14,1,2,1
	DS    UT_U4+OU_HLP-$
	%W    (0)
	DS    UT_U4+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U4+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U4+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UT_DATA+2)	; DP
	DS    UT_U4+OU_I_F-$
	DB    00H		; format I_F
	%W    (10)		; I_L
	%W    (50)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U5:	DS    UT_U5+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U5+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U5+OU_X-$
	DB    3,2,7,1
	DS    UT_U5+OU_HLP-$
	%W    (0)
	DS    UT_U5+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U5+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U5+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UT_DATA+4)	; DP
	DS    UT_U5+OU_I_F-$
	DB    83H		; format I_F
	%W    (10)		; I_L
	%W    (50)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U6:	DS    UT_U6+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U6+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U6+OU_X-$
	DB    8,3,7,1
	DS    UT_U6+OU_HLP-$
	%W    (0)
	DS    UT_U6+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U6+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U6+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UT_DATA+4)	; DP
	DS    UT_U6+OU_I_F-$
	DB    80H		; format I_F
	%W    (10)		; I_L
	%W    (50)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_SF1:	DB    K_RUN
	%W    (0)
	%W    (UT_G1K1)

	DB    K_END
	%W    (0)
	%W    (UT_G1K2)

	DB    K_PUMP1
	%W    (0)
	%W    (UT_G1K3)

	DB    K_RIGHT
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    K_LEFT
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    K_H_A
	%W    (UT_GR1)
	%W    (GR_RQ23)

	DB    K_H_B
	%W    (UT_GR2)
	%W    (GR_RQ23)

UT_SF0:	DB    0
)FI

	END