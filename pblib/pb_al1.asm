;********************************************************************
;*                        PB_AL1.ASM                                *
;*           Longint aritmetika - prevod ASCII na R4567             *
;*                  Stav ke dni 10.05.1997                          *
;*                      (C) Pisoft 1997 Pavel Pisa Praha            *
;********************************************************************

EXTRN	CODE(MULTENlF,NEGl)

PUBLIC	xATOIF

LINT_A_C SEGMENT CODE

RSEG LINT_A_C

; prevede ASCII na drese DPTR do R4567
; R3 .. SLZxxDDD - S .. signed, L .. longint, Z .. fill by 0
;		   DDD .. desetiny
; R2 .. delka vstupu, 0 znamena promennou delku
xATOIF: MOV   A,R3
	MOV   R1,A
	CLR   A
	MOV   R3,A
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R7,A
	SJMP  xATOI50

xATOI20:INC   DPTR
	DJNZ  R2,xATOI50
xATOI30:MOV   A,R3
	JNB   ACC.4,xATOI35 ; zadna cislice v cislu

xATOI34:MOV   A,R3
	XRL   A,R1
	ANL   A,#7	; pocet desetinnych mist
	JZ    xATOI36
	CALL  MULTENlF
	INC   R3
	JZ    xATOI34
xATOI35:JMP   xATOI90
xATOI36:MOV   A,R3
	JNB   ACC.7,xATOI38
	CALL  NEGl
xATOI38:RET

xATOI50:MOVX  A,@DPTR
	MOV   R0,A
	ADD   A,#-'9'-1
	JC    xATOI60
	ADD   A,#10
	JNC   xATOI60
	MOV   R0,A	; je to cislice
	MOV   A,R3
	ORL   A,#030H
	MOV   R3,A
	JNB   ACC.6,xATOI52
	XRL   A,R1
	ANL   A,#7	; pocet desetinnych mist
	JZ    xATOI30
	INC   R3
xATOI52:CALL  MULTENlF
	JNZ   xATOI90
	MOV   B,R1
	MOV   A,R0
	ADD   A,R4
	MOV   R4,A
	CLR   A
	ADDC  A,R5
	MOV   R5,A
	JNB   B.6,xATOI54
	CLR   A
	ADDC  A,R6
	MOV   R6,A
	CLR   A
	ADDC  A,R7
	MOV   R7,A
xATOI54:JC    xATOI90
	JNB   B.7,xATOI20
	JB    OV,xATOI90
	JMP   xATOI20
xATOI60:MOV   A,R0	; specialni znaky
	CJNE  A,#'.',xATOI62
	MOV   A,R3	; desetinna tecka
	JB    ACC.6,xATOI90 ; dvakrat -> error
	ORL   A,#060H
	MOV   R3,A
	SJMP  xATOI20
xATOI62:CJNE  A,#'-',xATOI64
	MOV   A,R1	; znamenko
	JNB   ACC.7,xATOI30 ; format bez znamenka -> konec
	MOV   A,R3
	JB    ACC.5,xATOI30 ; uprostred -> konec
	ORL   A,#0A0H
	MOV   R3,A
	SJMP  xATOI20
xATOI64:CJNE  A,#' ',xATOI66
	MOV   A,R3
	JB    ACC.5,xATOI30 ; uz neco bylo -> konec
	MOV   A,R1
	JB    ACC.5,xATOI90 ; mezera neni dovolena-> error
	JMP   xATOI20
xATOI66:JMP   xATOI30

xATOI90:SETB  F0
	RET

	END
