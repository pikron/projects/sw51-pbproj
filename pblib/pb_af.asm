;********************************************************************
;*                        PB_AF.H                                   *
;*              Aritmetika v plovouci radove carce   		    *
;*                  Stav ke dni 10.09.1996                          *
;*                      (C) Pisoft 1996 Pavel Pisa Praha            *
;********************************************************************
;
;            Exponent+80h +/- Mantisa <0.5;1)
;
;                eeeeeeee Smmmmmmm mmmmmmmm mmmmmmmm
;
;   operand Rf ..R7      ,R6      ,R5      ,R4
;   operand Af ..AKUM+3  ,AKUM+2  ,AKUM+1  ,AKUM
;
;

	PUBLIC AKUM,IRAMTXT
	PUBLIC ADDf,SUBf,CMPf,MULf,DIVf
	PUBLIC LOGfRf
	PUBLIC CONVlRf,CONVl_S
	PUBLIC f2IEEE,IEEE2f
	PUBLIC AKUM2Rf,Rf2AKUM
	PUBLIC CONVbRf

PB_AF_D SEGMENT DATA

PB_AF_C SEGMENT CODE INBLOCK


RSEG	PB_AF_D

; pro celou knihovnu
AKUM:   DS    4         ; operand Af

; pro prevody
; pro logaritmus
VYSL:
AKUM1:  DS    4
AKUM2:  DS    4
EXP:    DS    1
KONSTP: DS    1

; pro vystup textu do pameti
ITXTP  :DS    1
IRAMTXT:DS    20

RSEG	PB_AF_C

; Odcitani Rf,Af:=Af-Rf
; =====================

SUBf:                  ;:Rf,Af:=Af-Rf
	MOV   A,#80H
	XRL   A,R6
	MOV   R6,A

; Scitani Rf,Af:=Af+Rf
; ====================

ADDf:		       ;:Rf,Af:=Af+Rf
	MOV   A,R7
	JZ    LOADA    ; operand v Rf=0
	MOV   A,AKUM+3
	JZ    STORA    ; operand v Af=0
	CLR   C
	SUBB  A,R7     ; rozdil exponentu
	MOV   R1,A     ; do R1
	JNC   ADDf1    ; rozdil kladny
	CPL   A
	INC   A        ; negace rozdilu
	MOV   R1,A
	ACALL CHANGE   ; vymena operandu
ADDf1:  CJNE  R1,#25,ADDf12 ; rozdil <> 25

AKUM2Rf:
LOADA:  MOV   R4,AKUM  ;:Rf:=Af
	MOV   R5,AKUM+1
	MOV   R6,AKUM+2
	MOV   R7,AKUM+3
	RET

ADDf12: JNC   LOADA    ; rozdil > 24
	ACALL UZNAM    ; uprava znamenek
	MOV   R0,A
	ACALL POSP     ; posun registru na stejny
	MOV   A,R0     ; exponent
	RLC   A        ; ruzna znamenka
	JNC   ADDf2    ; ano
	ACALL ADDRE0   ; secteni AKUM a REGISTRU
	JNC   STORARO  ; neni preteceni
	INC   R7       ; exponent+1
	MOV   A,R7
	JZ    OVER     ; preteceni
	MOV   R1,#1
	ACALL POSP3+1  ; posun REG. o 1 misto vpravo
STORARO:ACALL ROUNDR3  ; zaokrouhleni podle MSB R3
	MOV   A,R2     ; znamenko vysledku
	ANL   A,#80H   ; vymaskovani
	XRL   A,R6     ; zapis do REG.
	MOV   R6,A

Rf2AKUM:
STORA:  MOV   AKUM,R4  ;:Af:=Rf
	MOV   AKUM+1,R5
	MOV   AKUM+2,R6
	MOV   AKUM+3,R7
	RET

ADDf2:  ACALL SUBRE0   ; odecteni REG-AKUM
	SJMP  STORARO

OVER:   SETB  F0       ; doslo k preteceni
	RET            ; F0 = 1

; uprava znamenek

UZNAM:  MOV   A,AKUM+2
        CPL   A
	MOV   R2,A     ; uloz znamenko Af
        ORL   AKUM+2,#80H
        MOV   A,R6     ; obnov MSB Af
        ORL   A,#80H
        XCH   A,R6     ; obnov MSB Rf
	XRL   A,R2     ; porovnej znamenka
        RET

; posun cisla v REG o R1 bitu vpravo

POSP:   MOV   R3,#0
        CJNE  R1,#0,POSP1
        RET            ; bez posunu
POSP1:  MOV   A,R1
        SUBB  A,#8     ; posun >= 8 bitu ?
        JC    POSP3    ; ne
        MOV   R1,A
        CLR   A        ; posun o 8 vpravo
        XCH   A,R6
        XCH   A,R5
        XCH   A,R4
        XCH   A,R3
        SJMP  POSP+2
POSP3:  CLR   C
        MOV   A,R6     ; posun o 1 bit
	RRC   A
        MOV   R6,A
        MOV   A,R5
        RRC   A
        MOV   R5,A
        MOV   A,R4
        RRC   A
        MOV   R4,A
	MOV   R3,PSW
	DJNZ  R1,POSP3 ; opakuj
	RET

; zaokrouhleni podle MSB R3

ROUNDR3:MOV   A,R3
	RLC   A        ; zaokrouhlovat ?
	JNC   ROUNDR   ; ne
REGA1:  MOV   R1,#1    ; pricti 1
	MOV   A,R4
	ADD   A,R1
	MOV   R4,A
	MOV   A,R5
	ADDC  A,#0
	MOV   R5,A
	MOV   A,R6
	ADDC  A,#0
	MOV   R6,A
	JC    ROUNDR4  ; preteceni
ROUNDR:	RET
ROUNDR4:INC   R7
	MOV   A,R7
	JZ    OVER
	SJMP  POSP3+1  ; posun vpravo

; vymena operandu

CHANGE: MOV   A,R4     ;:Af <--> Rf
        XCH   A,AKUM
        MOV   R4,A
        MOV   A,R5
	XCH   A,AKUM+1
        MOV   R5,A
        MOV   A,R6
        XCH   A,AKUM+2
        MOV   R6,A
        MOV   A,R7
        XCH   A,AKUM+3
        MOV   R7,A
        RET

; secteni mantisy REG+AKUM

ADDRE0: MOV   R7,AKUM+3
        MOV   A,#80H
        ADD   A,R3
        MOV   R3,#0
ADCREG: MOV   A,R4
        ADDC  A,AKUM
        MOV   R4,A
        MOV   A,R5
	ADDC  A,AKUM+1
        MOV   R5,A
        MOV   A,R6
        ADDC  A,AKUM+2
        MOV   R6,A
        RET

; odecteni mantisy REG-AKUM

SUBRE0: CLR   C
        MOV   A,R4
        SUBB  A,AKUM
        MOV   R4,A
        MOV   A,R5
        SUBB  A,AKUM+1
        MOV   R5,A
        MOV   A,R6
        SUBB  A,AKUM+2
        MOV   R6,A
        MOV   R7,AKUM+3
	JC    SUBRE1   ; AKUM vetsi
        MOV   A,R2
        CPL   A        ; zmena znamenka
	MOV   R2,A     ; vysledku
	SJMP  NORM2
SUBRE1: ACALL NEGREG   ; negace vysledku
	SJMP  NORM2    ; v REG

NEGREG: CLR   C
	CLR   A
	SUBB  A,R3
	MOV   R3,A
	CLR   A
	SUBB  A,R4
	MOV   R4,A
	CLR   A
	SUBB  A,R5
	MOV   R5,A
	CLR   A
	SUBB  A,R6
	MOV   R6,A
	RET

; prevod longint cisla R4567 na float Rf
; !!!!!!!! zatim vraci zaporny vysledek

CONVlRf:CLR   A
CONVl_S:ADD   A,#98H
	XCH   A,R7
	JZ    NORM1
	XCH   A,R7
	ADD   A,#8
	XCH   A,R7
	XCH   A,R6
	XCH   A,R5
	XCH   A,R4
	XCH   A,R3
	SJMP  NORM2

; normalizace cisla v REG a MSB R3=1

NORMA:  ACALL LOADA
NORM1:	MOV   R3,#0
NORM2:	CLR   A
NORM3:	MOV   R1,A
NORM4:	MOV   A,R6
	JNZ   NORM5
	XCH   A,R3     ; posun o 8 bitu
	XCH   A,R4     ; vlevo
	XCH   A,R5
	XCH   A,R6
	MOV   A,R1
	ADD   A,#8
	CJNE  A,#24,NORM3
CLEAR:  MOV   R7,#0
	RET
NORM5:  JB    ACC.7,NORM6
	MOV   A,R3     ; posun o bit
	RLC   A        ; vlevo
	MOV   R3,A
	MOV   A,R4
	RLC   A
	MOV   R4,A
	MOV   A,R5
	RLC   A
	MOV   R5,A
	MOV   A,R6
	RLC   A
	MOV   R6,A
	INC   R1
	SJMP  NORM5
NORM6:  CLR   C
	MOV   A,R7
	SUBB  A,R1     ; uprava exponentu
	JC    CLEAR
	MOV   R7,A
	RET

; nasobeni Rf,Af:=Af*Rf
; =====================

MULf:                  ;:Rf,Af:=Af*Rf
MUL0:   MOV   R1,#0
	ACALL ADDEXP   ; soucet exponentu
        JZ    CLEAA    ; jedno cislo = 0
	ACALL UZNAM    ; uprava znamenek
        MOV   R2,A     ; vysledne znamenko
        MOV   A,R1
        JZ    OVER1    ; preteceni
        MOV   A,R6
        PUSH  ACC
        MOV   A,R5
        PUSH  ACC
        MOV   A,R4
        PUSH  ACC
        MOV   R1,#3
	CLR   A
        MOV   R6,A
        MOV   R5,A
        MOV   R4,A
MUL1:   CLR   A
	XCH   A,R6
        XCH   A,R5
	XCH   A,R4
	XCH   A,R3
	POP   ACC
	JZ    MUL2
	PUSH  ACC
	MOV   B,A
	MOV   A,AKUM
	MUL   AB
	ADD   A,R3
	MOV   R3,A
	MOV   A,B
	ADDC  A,R4
	MOV   R4,A
	JNC   MUL3     ; pro zrychleni
        CLR   A
        ADDC  A,R5
        MOV   R5,A
        CLR   A
	ADDC  A,R6
        MOV   R6,A
MUL3:   POP   B
	PUSH  B
        MOV   A,AKUM+1
        MUL   AB
        ADD   A,R4
        MOV   R4,A
        MOV   A,B
        ADDC  A,R5
        MOV   R5,A
        CLR   A
        ADDC  A,R6
        MOV   R6,A
        POP   B
        MOV   A,AKUM+2
        MUL   AB
        ADD   A,R5
        MOV   R5,A
        MOV   A,B
	ADDC  A,R6
        MOV   R6,A
MUL2:   DJNZ  R1,MUL1
	ACALL NORM2
	AJMP  STORARO

CLEAA:  MOV   R7,A
        MOV   AKUM+3,A
        RET

OVER1:  AJMP  OVER

; soucet/rozdil exponentu
; -----------------------

ADDEXP: MOV   A,AKUM+3
        JZ    KADDE
        MOV   A,R7
        JZ    KADDE
        XRL   A,R1
	INC   R1
        INC   R1
	DJNZ  R1,$+3
        INC   A
        ADD   A,AKUM+3
        MOV   R7,A
        XRL   A,PSW
        RLC   A
        MOV   A,R7
        JNC   ADDE1
        XRL   A,#80H
	MOV   R7,A
        INC   R1
KADDE:  RET
ADDE1:  ANL   A,#80H
        MOV   R1,#0
        RET

ERRDIV: SETB  F0
	AJMP  STORA

; deleni Af,Rf:=Af/Rf
; ===================

DIVf:                  ;:Af,Rf:=Af/Rf
DIV0:   MOV   A,R7
	JZ    ERRDIV
	MOV   R1,#0FFH
	ACALL ADDEXP
	JZ    CLEAA
	INC   A
	JZ    OVER1
	MOV   AKUM+3,A
        MOV   A,R1
        JZ    OVER1
	ACALL UZNAM
        MOV   R2,A
	ACALL CHANGE
        CLR   A
        XCH   A,R6
        MOV   R3,A
        CLR   A
        XCH   A,R5
        MOV   R1,A
        CLR   A
        MOV   B,A
        XCH   A,R4
        MOV   R0,A
DIV2:   MOV   A,R3
        PUSH  ACC
        MOV   A,R1
        PUSH  ACC
	MOV   A,R0
        PUSH  ACC
DIV8:   CLR   C
        MOV   A,R0
        SUBB  A,AKUM
        MOV   R0,A
        MOV   A,R1
        SUBB  A,AKUM+1
        MOV   R1,A
        MOV   A,R3
        SUBB  A,AKUM+2
        MOV   R3,A
        JC    DIV1
        DEC   SP
        DEC   SP
        DEC   SP
        SETB  C
DIV5:   MOV   A,R6
        JB    ACC.7,DIV6
        MOV   A,R4
	RLC   A
        MOV   R4,A
        MOV   A,R5
        RLC   A
        MOV   R5,A
        MOV   A,R6
        RLC   A
        MOV   R6,A
        ORL   A,R5
        ORL   A,R4
        JNZ   $+3
        DEC   R7
	MOV   A,R0
        RLC   A
        MOV   R0,A
        MOV   A,R1
        RLC   A
        MOV   R1,A
        MOV   A,R3
        RLC   A
	MOV   R3,A
        JNC   DIV2
        INC   B
        SJMP  DIV8
DIV1:   JBC   B.0,DIV5
        POP   ACC
        MOV   R0,A
        POP   ACC
        MOV   R1,A
        POP   ACC
        MOV   R3,A
        CLR   C
	SJMP  DIV5
DIV6:   MOV   R3,PSW
	AJMP  STORARO

; porovnani Af-Rf -> Carry a Zero
; ===============================

CMPf:	ACALL PORO     ;:Af-Rf
	JZ    CMPfK
	MOV   A,R6
	XRL   A,PSW
	RLC   A
	MOV   A,R6
	XRL   A,AKUM+2
	JNB   ACC.7,CMPf1
	MOV   A,AKUM+2
	RLC   A
CMPf1:	MOV   A,#1
CMPfK:	RET

; porovnani AKUM-REG se shodnymi znamenky

PORO:   MOV   A,AKUM+3
        CLR   C
        SUBB  A,R7
        JNZ   POROK
        MOV   A,R7
        JZ    POROK
        MOV   A,AKUM
        SUBB  A,R4
        MOV   A,AKUM+1
	SUBB  A,R5
        MOV   A,AKUM+2
        SUBB  A,R6
POROK:  RET


; logaritmus Af,Rf:=LOG(Af)
; =========================

LOGf:   ACALL LOADA    ;:Af,Rf:=LOG(Af)
LOGfRf: MOV   KONSTP,#LOGK-LOADKB
	ACALL cLOADAf
	MOV   A,#80H
	XCH   A,R7
	ADD   A,R7     ; Rf e <0.5;1)
	MOV   EXP,A    ; EXP:=exp.Rf
	DEC   AKUM+3   ;         _
	ACALL ADDf     ; Rf:=Rf+V2/2
	MOV   KONSTP,#LOGK-LOADKB
	ACALL cLOADAf  ;      _
	ACALL DIV0     ; Rf:=V2/Rf
	ACALL cLOADAf
	ACALL SUBf     ; Af:=1-Rf
	MOV   R1,#AKUM2; A2f:=Af
	ACALL iMOVEf1
	ACALL MUL0     ; Rf,A1f:=Af*Af
	MOV   R1,#AKUM1
	ACALL iMOVEf1
	ACALL cLOADAf  ; nacti konstantu
	SJMP  LOGf3

LOGf1:  ACALL ADDf     ; generator rad
LOGf2:  MOV   R0,#AKUM1
	MOV   R1,#AKUM
	ACALL iMOVEf   ; Af:=A1f
LOGf3:  ACALL MUL0
	ACALL cLOADAf  ; nacti konstantu
	ADD   A,#10H
	JNC   LOGf1
;
	MOV   R3,A
	DEC   R1
	MOV   R2,#1
	ACALL cLOADs
;
	ACALL ADDf
	MOV   R0,#AKUM2
	MOV   R1,#AKUM
	ACALL iMOVEf
	ACALL MUL0
	ACALL cLOADAf
	ACALL ADDf    ; Af:=Rf-0.5
        MOV   A,EXP
	JZ    LOGf4
	ACALL NORMRB
	ACALL ADDf     ; Af:=Af+exp
LOGf4:  ACALL cLOADAf
	ACALL MUL0     ; Af,Rf:=Rf*log(2) (ln(2))
	RET

; prevod bytu v A na float v Rf

CONVbRf:
NORMRB: MOV   R4,#0
	MOV   R5,#0
	MOV   R6,A
	CLR   C
	RLC   A
	JZ    CLEARRf
	JNC   NORMRB1
	CPL   A
	INC   A
NORMRB1:MOV   R7,#87H
NORMRB2:CLR   C
	RLC   A
	JC    NORMRB3
	DEC   R7
	SJMP  NORMRB2
NORMRB3:RRC   A
        XCH   A,R6
	CPL   A
	ANL   A,#80H
        XRL   A,R6
	MOV   R6,A
	RET
CLEARRf:MOV   R7,#0
	RET

iMOVEf1:MOV   R0,#AKUM
iMOVEf: MOV   R2,#4
iMOVEs: MOV   A,@R0
        INC   R0
        MOV   @R1,A
        INC   R1
        DJNZ  R2,iMOVEs
        RET

cLOADAf:MOV   R2,#4
        MOV   R1,#AKUM
cLOADs :MOV   A,KONSTP
	INC   KONSTP
	MOVC  A,@A+PC
LOADKB: MOV   @R1,A
        INC   R1
        DJNZ  R2,cLOADs
        RET

; konstanty pro vypocet logaritmu
                                  ;  _
LOGK:   DB    0F3H,004H,035H,081H ; V2
        DB    000H,000H,000H,081H ; 1
        DB    0AAH,056H,019H,080H ; 1/5*2/ln(2)
        DB    0F1H,022H,076H,080H ; 1/3*2/ln(2)
        DB    045H,0AAH,038H,0FFH,082H; 2/ln(2)
        DB    000H,000H,080H,080H ; -0.5
; dekadicky logaritmus
        DB    09BH,020H,01AH,07FH ; log(2)
; prirozeny logaritmus
;       DB    018H,072H,031H,080H ; ln(2)

; konstanty pro prevod do desitkove soustavy

TODECK: DB    040H,042H,00FH ; 1E6
        DB    0A0H,086H,001H ; 1E5
        DB    010H,027H,000H ; 1E4
        DB    0E8H,003H,000H ; 1E3
        DB    064H,000H,000H ; 1E2
        DB    00AH,000H,000H ; 10
        DB    001H,000H,000H ; 1

; nasobeni deseti

MULTEN :ACALL LOADA
        INC   R7
        INC   R7
        INC   R7
        INC   AKUM+3
	AJMP  ADDf

; deleni deseti

DIVTEN :MOV   R4,#0CDH ; 1E-1
	MOV   R5,#0CCH
        MOV   R6,#04CH
        MOV   R7,#07DH
        AJMP  MUL0

SUBREG: CLR   C
        MOV   A,R4
        SUBB  A,AKUM
        MOV   R4,A
        MOV   A,R5
        SUBB  A,AKUM+1
        MOV   R5,A
        MOV   A,R6
        SUBB  A,AKUM+2
        MOV   R6,A
        RET

; prevod cisla v Af na BCD ve vysl

TOBCDf :ACALL NULVYS
	MOV   A,AKUM+3
        JZ    TOBCDf1
        MOV   VYSL+4,#6
        ADD   A,#07FH
        JC    TOBCDf2
        MOV   R4,#000H ; 1E6
        MOV   R5,#024H
        MOV   R6,#074H
        MOV   R7,#094H
	ACALL MUL0
        MOV   VYSL+4,#0
TOBCDf2:MOV   A,AKUM+2
        ANL   A,#080H
        MOV   VYSL+3,A
        ORL   AKUM+2,#080H
TOBCDf4:MOV   R4,#07FH ; -1E7
        MOV   R5,#096H
        MOV   R6,#098H
        MOV   R7,#098H
	ACALL PORO
	JC    TOBCDf3
	ACALL DIVTEN
        INC   VYSL+4
        SJMP  TOBCDf4
TOBCDf3:MOV   R4,#0FFH ; -1E6
        MOV   R5,#023H
        MOV   R6,#0F4H
        MOV   R7,#094H
	ACALL PORO
        JNC   TOBCDf5
	ACALL MULTEN
        DEC   VYSL+4
        SJMP  TOBCDf3
TOBCDf5:ACALL LOADA
        MOV   A,#098H
        CLR   C
        SUBB  A,R7
        MOV   R1,A
	ACALL POSP
        MOV   R7,VYSL+3
	ACALL BINBCD
        MOV   A,R7
        ORL   VYSL+3,A
TOBCDf1:RET

BINBCD: MOV   B,#2
        MOV   A,R6
	ACALL BINB1
        INC   B
        MOV   A,R5
	ACALL BINB1
        INC   B
        MOV   A,R4
BINB1:  MOV   R2,#8
BINB3:  RLC   A
        MOV   R6,A
        MOV   R3,B
        MOV   R1,#VYSL
BINB2:  MOV   A,@R1
        ADDC  A,@R1
	DA    A
        MOV   @R1,A
        INC   R1
        DJNZ  R3,BINB2
        MOV   A,R6
        DJNZ  R2,BINB3
        RET

NULVYS: MOV   R1,#VYSL
        MOV   R2,#5
iNULs:  CLR   A
        MOV   @R1,A
        INC   R1
        DJNZ  R2,iNULs+1
        RET

; nacteni cisla ze vstupu READ do Af
; ==================================

DECTOf: CALL  READ
	SETB  F0           ; moznost chyby
        MOV   AKUM1,#0     ; plus
        MOV   AKUM1+2,#0   ; exponent
        MOV   AKUM1+3,#0   ; neni des. tecka
        MOV   AKUM+3,#0    ; cislo 0
        CJNE  A,#'-',DECTOf1
        MOV   AKUM1,#080H  ; minus

DECTOf0:CALL  READ
DECTOf1:CJNE  A,#'.',DECTOf2
        MOV   A,#0FFH
        XCH   A,AKUM1+3    ; je des. tecka
        JNZ   DECTOf3
        CALL  READ
DECTOf2:ACALL ISANUM
        JC    DECTOf4
        CLR   F0       ; neni chyba
	MOV   AKUM1+1,A; uloz cislici
	ACALL MULTEN
        MOV   A,AKUM1+1; cislici zpet
	ACALL NORMRB
        MOV   A,R6
        XRL   A,AKUM1  ; pridej znaminko
        MOV   R6,A
	ACALL ADDf
        MOV   A,AKUM1+3
        JZ    DECTOf0  ; neni tecka
        DEC   AKUM1+2  ; sniz exponent
        SJMP  DECTOf0
DECTOf3:MOV   A,#'.'-48
DECTOf4:ADD   A,#48
        MOV   R1,#0    ; exponent
        MOV   R2,#0    ; exp. +
        CJNE  A,#'e',DECTOf5
        SJMP  DECTOf6
DECTOf5:CJNE  A,#'E',DECTOf9 ; bez exp
DECTOf6:CALL  READ
        MOV   R0,#3    ; max cislic v exp.
        CJNE  A,#'+',DECTO15
        SJMP  DECTO14
DECTO15:CJNE  A,#'-',DECTOf7
        MOV   R2,#1    ; exp -
DECTO14:CALL  READ
DECTOf7:ACALL ISANUM
        JC    DECTOf8  ; konec exp
        MOV   R3,A
        MOV   A,R1
        RL    A
        RL    A
        ADD   A,R1
        RL    A        ; exp*10
        ADD   A,R3     ; +cislice
        MOV   R1,A
        DJNZ  R0,DECTO14
        SETB  F0       ; chyba v exponentu
        RET
DECTOf8:ADD   A,#48
DECTOf9:PUSH  ACC
        MOV   A,R1     ; znam exp
        DJNZ  R2,DECTO10
        CPL   A
        INC   A
DECTO10:ADD   A,AKUM1+2; pricti pom exp
DECTO12:MOV   AKUM1+2,A
        JZ    DECTOfK
        JNB   ACC.7,DECTO13
	ACALL DIVTEN
        MOV   A,AKUM1+2
        INC   A
        SJMP  DECTO12
DECTO13:ACALL MULTEN
        DJNZ  AKUM1+2,DECTO13
DECTOfK:POP   ACC
        RET

; vystup cisla v Af na zarizeni
; =============================

TODECf: MOV   A,AKUM+3
	JNZ   TODECf0
WRITEZR:MOV   A,'0'
        JMP   WRITE
TODECf0:MOV   AKUM1,#6
        MOV   A,AKUM+2
        ORL   AKUM+2,#080H
        RLC   A
        MOV   A,#' '
	JNC   TODECf1
        MOV   A,#'-'
TODECf1:CALL  WRITE
TODECf2:MOV   A,AKUM+3
        ADD   A,#068H
	JNC   TODECf4
        MOV   R4,#017H ; 1E-4
        MOV   R5,#0B7H
        MOV   R6,#051H
        MOV   R7,#073H
	ACALL MUL0
        MOV   A,AKUM1
        ADD   A,#4
        MOV   AKUM1,A
	SJMP  TODECf2

TODECf3:CLR   A       ; 1E3
        MOV   R4,A
        MOV   R5,A
        MOV   R6,#07AH
        MOV   R7,#08AH
	ACALL MUL0
        DEC   AKUM1
        DEC   AKUM1
        DEC   AKUM1
TODECf4:MOV   A,AKUM+3
        ADD   A,#072H
	JNC   TODECf3
        MOV   R4,#000H; -1E6
        MOV   R5,#024H
        MOV   R6,#0F4H
        MOV   R7,#094H
	ACALL PORO
	JNC   TODECf5
	ACALL MULTEN
	SJMP  TODECf4-2

TODECf5:ACALL LOADA
        MOV   A,#098H
        CLR   C
        SUBB  A,R7
        MOV   R1,A
	ACALL POSP
        MOV   A,AKUM1
        MOV   R0,A
        MOV   R3,#0
        CJNE  A,#8,$+2
	JC    TODECf6
        MOV   R3,A
        MOV   R0,#0
TODECf6:INC   R0

	MOV   KONSTP,#TODECK-LOADKB
        MOV   R7,#7

        MOV   A,R0
	JNB   ACC.7,TODECf8
	ACALL WRITEZR

TODECf8:MOV   A,R0
	JNZ   TODECf9
        MOV   A,#'.'
        CALL  WRITE
TODECf9:DEC   R0
        MOV   R2,#3
	ACALL cLOADAf+2  ; PRASI R1,R2
        MOV   R1,#'0'-1

TODEC10:INC   R1
	ACALL SUBREG
	JNC   TODEC10
        CLR   C
	ACALL ADCREG
        MOV   A,R1
	ACALL WRITE
        MOV   A,R0
	JNB   ACC.7,TODEC11
        MOV   A,R4
        ORL   A,R5
        ORL   A,R6
	JZ    TODEC11+2
TODEC11:DJNZ  R7,TODECf8
        MOV   A,R3
        JZ    ISANUM1
        MOV   A,#'E'
        CALL  WRITE
        MOV   A,R3
        MOV   R0,#'+'
	JNB   ACC.7,TODEC12
        CPL   A
        INC   A
        MOV   R3,A
        MOV   R0,#'-'
TODEC12:MOV   A,R0
        CALL  WRITE
        MOV   A,R3
        MOV   B,#00AH
        DIV   AB
	ACALL TODEC13
        MOV   A,B
TODEC13:ADD   A,#'0'
        JMP   WRITE

ISANUM: ADD   A,#-58
        ADD   A,#10
        CPL   C
ISANUM1:RET

WRITE  :XCH   A,R1
        PUSH  ACC
;
        MOV   A,R1
        MOV   R1,ITXTP
        MOV   @R1,A
        INC   ITXTP
        POP   ACC
        MOV   R1,A
        RET

READ:   MOV   A,R1
        PUSH  ACC
;
        MOV   R1,ITXTP
        MOV   A,@R1
        INC   ITXTP
        MOV   R1,A
        POP   ACC
        XCH   A,R1
        RET

POM:    MOV   ITXTP,#IRAMTXT
	ACALL TODECf
        MOV   A,#00
        CALL  WRITE
	RET


; Prevod vlastniho formatu do IEEE formatu
f2IEEE: MOV   A,R6
	RLC   A
	MOV   A,R7
	JZ    f2IEEE1
	DJNZ  ACC,f2IEEE2
f2IEEE1:MOV   R6,A
	MOV   R5,A
	MOV   R4,A
	RRC   A
	MOV   R7,A
	RET
f2IEEE2:DEC   A
	RRC   A
	MOV   R7,A
	MOV   A,R6
	MOV   ACC.7,C
	MOV   R6,A
	RET

; Prevod z IEEE do vlastniho formatu
IEEE2f: MOV   A,R7
	RLC   A
	MOV   A,R6
	MOV   ACC.7,C
	XCH   A,R6
	RLC   A
	MOV   A,R7
	RLC   A
	JZ    IEEE2f2
	ADD   A,#2
	JC    IEEE2f1
	MOV   R7,A
	RET
IEEE2f1:MOV   A,#0FFH
	MOV   R7,A
	RET
IEEE2f2:MOV   R7,A	; nula
	RRC   A
	MOV   R6,A
	RET

END