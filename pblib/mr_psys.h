;********************************************************************
;*              Multiosa regulace  - MR_PSYS.H                      *
;*                Modul rizeni a generovani polohy                  *
;*                  Stav ke dni 10. 8.1997                          *
;*                      (C) Pisoft 1996                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

; Vyzaduje jiz naincludovany INCH_MMAC
; Vyzaduje jiz naincludovany INCH_MR_DEFS
; Vyzaduje jiz naincludovany INCH_MR_PDEFS

; vyzaduje uzivatelem publikovane symboly
; NUMBER(REG_LEN,REG_A,REG_N)
; BIT(FL_MRCE)
; DATA(MR_BAS,MR_FLG,MR_FLGA)
; CODE(DO_REGDBG,GO_NOTIFY)

EXTRN	CODE(MR_REG,MR_RNUL,MR_ZER,MR_GPSC,MR_GPSV,MR_CMEP)
EXTRN	CODE(MR_RDRP,MR_WRRP,MR_RDAP,CI_VG)
EXTRN	CODE(MR_GPA1,GO_GEP,GO_GEPT,GO_VG,GO_PRPC,GO_RSFT)
EXTRN	CODE(CLR_GEP,REL_GEP,CER_GEP,STP_GEP,GO_HH)

EXTRN	CODE(IRC_INIT,IRC_RD,IRC_RDP,IRC_WR,IRC_WRP,HH_MARK)
EXTRN	CODE(MR_SENE,MR_SENEP)
EXTRN	CODE(DO_REG,VR_REG,VR_REG1,VR_RET)
