;********************************************************************
;*                        PB_AL.ASM                                 *
;*                      Longint aritmetika                          *
;*                  Stav ke dni 10.03.1996                          *
;*                      (C) Pisoft 1996 Pavel Pisa Praha            *
;********************************************************************

PUBLIC	xLDl,xiLDl,xiLDs,xSVl,xiSVl,xiSVs
PUBLIC  xADDl,xiADDl,xiADDs
PUBLIC	xSUBl,xiSUBl,xiSUBs,xCMPl,xiCMPl,xiCMPs
PUBLIC	NEGl,ZERROl,SHRl,xNULl,xNULs
PUBLIC  NORMli,NORMli1,DENOil

EXTRN   CODE(SHRi)

LINT_A_C SEGMENT CODE

RSEG LINT_A_C

; Longint aritmetika
; Operandy R4567 a [DPTR]

xLDl:	MOV   A,PSW
	ANL   A,#18H
	ORL   A,#4
	MOV   R0,A
xiLDl:	MOV   R2,#4
xiLDs:	MOVX  A,@DPTR
	MOV   @R0,A
	INC   R0
	INC   DPTR
	DJNZ  R2,xiLDs
	RET

xSVl:	MOV   A,PSW
	ANL   A,#18H
	ORL   A,#4
	MOV   R0,A
xiSVl:	MOV   R2,#4
xiSVs:	MOV   A,@R0
	MOVX  @DPTR,A
	INC   R0
	INC   DPTR
	DJNZ  R2,xiSVs
	RET

xADDl:	MOV   A,PSW
	ANL   A,#18H
	ORL   A,#4
	MOV   R0,A
xiADDl:	MOV   R2,#4
xiADDs: CLR   C
xiADD1:	MOVX  A,@DPTR
	ADDC  A,@R0
	MOV   @R0,A
	INC   R0
	INC   DPTR
	DJNZ  R2,xiADD1
	RET

xSUBl:	MOV   A,PSW
	ANL   A,#18H
	ORL   A,#4
	MOV   R0,A
xiSUBl:	MOV   R2,#4
xiSUBs: CLR   C
xiSUB1:	MOVX  A,@DPTR
	XCH   A,@R0
	SUBB  A,@R0
	MOV   @R0,A
	INC   R0
	INC   DPTR
	DJNZ  R2,xiSUB1
	RET

xCMPl:	MOV   A,PSW
	ANL   A,#18H
	ORL   A,#4
	MOV   R0,A
xiCMPl:	MOV   R2,#4
xiCMPs: CLR   C
	MOV   B,#0
xiCMP1:	MOVX  A,@DPTR
	SUBB  A,@R0
	ORL   B,A
	INC   R0
	INC   DPTR
	DJNZ  R2,xiCMP1
	INC   B
	DJNZ  B,xiCMP2
	RET
xiCMP2:	CPL   C
	CPL   A
	ORL   A,#1
	RET

NEGl:   CLR   C
	CLR   A
	SUBB  A,R4
	MOV   R4,A
	CLR   A
	SUBB  A,R5
	MOV   R5,A
	CLR   A
	SUBB  A,R6
	MOV   R6,A
	CLR   A
	SUBB  A,R7
	MOV   R7,A
	RET

ZERROl:	MOV   A,R4
	ORL   A,R5
	ORL   A,R6
	ORL   A,R7
SHRlR:	RET

SHRl:   ADD   A,#-8
	JNC   SHRL1
	XCH   A,R7
	XCH   A,R6
	XCH   A,R5
	XCH   A,R4
	CLR   A
	XCH   A,R7
	SJMP  SHRl
SHRL1:  ADD   A,#8
SHRl2:	JZ    SHRlR
	CLR   C
	XCH   A,R7
	RRC   A
	XCH   A,R7
	XCH   A,R6
	RRC   A
	XCH   A,R6
	XCH   A,R5
	RRC   A
	XCH   A,R5
	XCH   A,R4
	RRC   A
	XCH   A,R4
	DEC   A
	SJMP  SHRl2

xNULl:	MOV   R2,#4
xNULs:	CLR   A
xNULs1:	MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R2,xNULs1
	RET

; Rotuje R4567 tak ze se cislo zkrati na R45 pocet posunu ulozi do R1

NORMli: MOV   R1,#0
NORMli1:MOV   A,R7
	JZ    NORMli2
	CLR   A
	XCH   A,R7
	XCH   A,R6
	XCH   A,R5
	XCH   A,R4
	MOV   A,R1
	ADD   A,#8
	MOV   R1,A
NORMli2:MOV   A,R6
	JZ    NORMliR
	CLR   C
	MOV   A,R6
	RRC   A
	MOV   R6,A
	MOV   A,R5
	RRC   A
	MOV   R5,A
	MOV   A,R4
	RRC   A
	MOV   R4,A
	INC   R1
	SJMP  NORMli2
NORMliR:RET

; Roztahne R45 exp R1 na R4567

DENOil: CLR   A
	MOV   R6,A
	MOV   R7,A
	MOV   A,R1
	JB    ACC.7,DENOil5   ; Exponent je zaporny
	ANL   A,#7            ; Posun R45 vlevo do R4567
	JZ    DENOil2
	MOV   R0,A            ; Posun o R1 mod 8
DENOil1:CLR   C
	MOV   A,R4
	RLC   A
	MOV   R4,A
	MOV   A,R5
	RLC   A
	MOV   R5,A
	MOV   A,R6
	RLC   A
	MOV   R6,A
	DJNZ  R0,DENOil1
DENOil2:MOV   A,R1
	ANL   A,#NOT 7
	RR    A
	RR    A
	RR    A
	MOV   R0,A            ; Posun o (R1 div 8)*8
	JZ    DENOilR
DENOil3:CLR   A
	XCH   A,R4
	XCH   A,R5
	XCH   A,R6
	XCH   A,R7
	DJNZ  R0,DENOil3
DENOilR:CLR   F0
	RET
DENOil5:CPL   A               ; Posum R45 vpravo a R67=0
	INC   A
	CLR   F0
	JMP   SHRi

	END
