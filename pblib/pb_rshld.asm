;********************************************************************
;*              Seriova komunikace - RSHLD.ASM                      *
;*                       Intel hex loader                           *
;*                  Stav ke dni 10.11.1996                          *
;*                      (C) Pisoft 1996                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_AI)
$LIST

; RS_ILB
EXTRN	CODE(RS232_INI,RS_RD,RS_WR,RS_RDFL,SKIPSPC)
EXTRN	CODE(RS_RDLN,RS_WRL1,RS_RDLF,RS_WRFL)
EXTRN   XDATA(RS_ILB)
EXTRN	CODE(MONITOR)

PUBLIC  IHEXLD,IHEXLDND

RS232_C SEGMENT CODE
RS232_X SEGMENT XDATA

RSEG	RS232_X
OCNT:	DS    2

RSEG	RS232_C

HEXNIB:	MOVX  A,@DPTR
	ADD   A,#-3AH
	JC    HEXNIB1
	ADD   A,#10
	CPL   C
	JNC   HEXNIB7
	RET
HEXNIB1:ADD   A,#-13
	JC    HEXNIB8
	ADD   A,#6
	JNC   HEXNIB9
	ADD   A,#10
HEXNIB7:INC   DPTR
HEXNIB8:RET
HEXNIB9:SETB  C
	RET

HEXBYTE:CALL  HEXNIB
	SWAP  A
	MOV   R0,A
	CALL  HEXNIB
	PUSH  PSW
	ORL   A,R0
	XCH   A,R1
	ADD   A,R1
	XCH   A,R1
	POP   PSW
	RET

IHEXR10:INC   DPTR
	MOV   R1,#0
	CALL  SKIPSPC
	CALL  HEXBYTE	; pocet znaku
	JC    IHEXR99
	MOV   R2,A
	CALL  SKIPSPC
	CALL  HEXBYTE	; adresa
	MOV   R5,A
	CALL  HEXBYTE
	MOV   R4,A
	JC    IHEXR99
	CALL  SKIPSPC
	CALL  HEXBYTE	; typ
	JC    IHEXR99
	JNZ   IHEXR99
	INC   R2
IHEXR40:DJNZ  R2,IHEXR42
	CALL  SKIPSPC
	CALL  HEXBYTE	; check sum
	JC    IHEXR99
	MOV   A,R1
	ADD   A,#-1
	INC   A
	RET
IHEXR42:CALL  SKIPSPC
	CALL  HEXBYTE	; data
	JC    IHEXR99
	XCH   A,R4
	XCH   A,DPL
	XCH   A,R4
	XCH   A,R5
	XCH   A,DPH
	XCH   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	XCH   A,R4
	XCH   A,DPL
	XCH   A,R4
	XCH   A,R5
	XCH   A,DPH
	XCH   A,R5
	SJMP  IHEXR40
IHEXR99:RET

IHEXLDt:DB    C_CLR,'Intel hex loader',0

; Prace bez displaye
IHEXLDND:
	MOV   B,#0
	SJMP  IHEXL01

; Prace i s displayem
IHEXLD: MOV   B,#1
IHEXL01:CLR   A
	MOV   DPTR,#OCNT
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
IHEXL05:JNB   B.0,IHEXL06
	MOV   DPTR,#IHEXLDt
	CALL  cPRINT
IHEXL06:

IHEXL10:CLR   F0
	CALL  RS_RDLN
	JZ    IHEXL50
	MOVX  A,@DPTR
	CJNE  A,#':',IHEXL30
	CALL  IHEXR10
	JC    IHEXL30
	CJNE  A,#1,IHEXL20
	MOV   A,R4	; skok na adresu
	PUSH  ACC
	MOV   A,R5
	PUSH  ACC
	RET
IHEXL20:MOV   DPTR,#OCNT
	MOVX  A,@DPTR
	ADD   A,#1
	MOVX  @DPTR,A
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#0
	MOVX  @DPTR,A
	MOV   R5,A
	JNB   B.0,IHEXL28
	MOV   A,#LCD_HOM+40H
	CALL  LCDWCOM
	MOV   R7,#070H
	CALL  PRINTi
IHEXL28:MOV   R0,#'.'
	CALL  RS_WR
	JMP   IHEXL10

IHEXL30:MOV   DPTR,#RS_ILB
	CALL  RS_WRL1
	JMP   IHEXL10

IHEXL50:JNB   B.0,IHEXL55
	CALL  SCANKEY
	JZ    IHEXL10
	CJNE  A,#K_DP,IHEXL60
	CALL  MONITOR
IHEXL55:JMP   IHEXL05

IHEXL60:RET

%IF (0) THEN (

L0:	MOV   R7,#10
	CALL  RS232_INI
	CLR   A
	MOV   DPTR,#OCNT
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A

L1:     CALL  SCANKEY
	JNZ   L4
	CALL  RS_RD
	JZ    L1
	MOV   A,R0
	PUSH  ACC
;	CALL  LCDWR
	MOV   A,#LCD_HOM
	CALL  LCDWCOM
	MOV   R7,#070H
	CALL  PRINTi
	POP   ACC
	MOV   R0,A
	CALL  RS_WR
	MOV   A,#LCD_HOM+8
	CALL  LCDWCOM
	MOV   R7,#070H
	CALL  PRINTi
	MOV   DPTR,#OCNT
	MOVX  A,@DPTR
	ADD   A,#1
	MOVX  @DPTR,A
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#0
	MOVX  @DPTR,A
	MOV   R5,A
	MOV   A,#LCD_HOM+40H
	CALL  LCDWCOM
	MOV   R7,#070H
	CALL  PRINTi
	JMP   L1
L4:	CJNE  A,#K_DP,L5
	CALL  MONITOR
	MOV   A,#LCD_CLR
	CALL  LCDWR
	JMP   L1
L5:	CJNE  A,#K_1,L6
	CALL  RS_RDFL
	CALL  RS_WRFL
	CALL  RS_RDLF
	CLR   A
	MOV   DPTR,#OCNT
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	JMP   L1

L6:	JMP   L1

) FI

END
