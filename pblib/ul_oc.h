;********************************************************************
;*            Rizeni pres uLan OI - UL_OC.H                         *
;*                 Dotazove cykly a prikazy                         *
;*                  Stav ke dni 10.02.1997                          *
;*                      (C) Pisoft 1997                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

; definice struktury pro popis prenasenych parametru
OC_LEN	SET   0
%STRUCTM(OC,VOCJ,1)	; instrukce jmp
%STRUCTM(OC,VOC ,2)	; rutina zpracovani udalosti
%STRUCTM(OC,FLG ,1)	; definicni priznaky
%STRUCTM(OC,IID ,1)	; identifikacni cislo pristroje
%STRUCTM(OC,OID ,2)	; identifikacni cislo pristroje
%STRUCTM(OC,DL  ,1)	; delka datove casti bufferu
%STRUCTM(OC,DP  ,2)	; ukazatel na pocatek bufferu (OCD_FLG)
%STRUCTM(OC,UC  ,0)	; delka a prikaz k ziskani a zapisu dat

; zkracene OC pro vysilani prikazu
OC_C_LEN SET OC_OID
%STRUCTM(OC_C,UC  ,0)	; delka a prikaz


; popis priznaku v OC_FLG
BCF_REF	EQU   0		; stale refresovani
BCF_DYW	EQU   1		; pri zapisu se nevysila okamzite
MCF_REF EQU   1 SHL BCF_REF
MCF_DYW EQU   1 SHL BCF_DYW

; popis priznaku v 1.byte bufferu dat OCD_FLG
MCD_DOK	EQU   003H	; nenulova hodnota => data jsou cerstva
BCD_CWR	EQU   2		; data byla prave zmenena a vyslana
BCD_DCH	EQU   3		; data byla zmenena a je nutne je vyslat
BCD_RDO	EQU   4		; data jsou potreba => obnovuj
MCD_CWR	EQU   1 SHL BCD_CWR
MCD_DCH	EQU   1 SHL BCD_DCH
MCD_RDO	EQU   1 SHL BCD_RDO

; jednotlive prikazy pro objekt OC
OCT_RD	EQU   1		; pozadavek na precteni dat
OCT_WR	EQU   2		; pozadavek na zapis dat
OCT_DR	EQU   3		; prisla data z linky
OCT_DSN	EQU   4		; vysli data na linku
OCT_RQD	EQU   5		; vysli zadost o data
OCT_CMD	EQU   6		; vysli prikaz

; UC_AUPP ukazatel na pole odkazu na objekty

EXTRN	BIT(UCF_RIP,UCF_RDP)

EXTRN	XDATA(UC_AIID,UC_TMP,UC_AUPP,UC_NUPP,UC_NUP)

EXTRN	CODE(UC_SND,UC_OCDP)
EXTRN	CODE(JMP_VOC,UC_CMD,UC_RD,UC_WR)
EXTRN	CODE(UC_DC,UC_RDi,UC_RQD,UC_WRi,UC_DRDY,UC_DSND)
EXTRN	CODE(FND_NUP,UC_RECL,UC_REOP,UC_REFR,UC_OI)

%*DEFINE (UCINT(FLG,IID,OID,DP)) (
	DB    2		; OC_VOCJ
	DW    UC_DC	; OC_VOC
	DB    %FLG	; OC_FLG
	DB    %IID	; OC_IID
	%W    (%OID)	; OC_OID
	DB    2		; OC_DL
	%W    (%DP)	; OC_DP
)
%*DEFINE (UCLONG(FLG,IID,OID,DP)) (
	DB    2		; OC_VOCJ
	DW    UC_DC	; OC_VOC
	DB    %FLG	; OC_FLG
	DB    %IID	; OC_IID
	%W    (%OID)	; OC_OID
	DB    4		; OC_DL
	%W    (%DP)	; OC_DP
)