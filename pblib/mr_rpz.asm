;********************************************************************
;*                        MR_RPZ                                    *
;*                Rutiny Z filtroveho multi regulatoru polohy       *
;*                  Stav ke dni 14.09.1996                          *
;*                      (C) Pisoft 1996 Pavel Pisa Praha            *
;********************************************************************

$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MR_DEFS)
$LIST

PUBLIC  MR_PZP

MR____C SEGMENT CODE

; PI regulator polohy

RSEG MR____C

MR_PZP:	MOV   A,#OMR_FOD	; R23=OMR_FOD*OMR_D/256
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_FOD+1
	MOVC  A,@A+DPTR

	MOV   B,A
	MOV   A,#OMR_D
	MOVC  A,@A+DPTR
	MOV   R2,A
	MOV   C,B.7
	MOV   F0,C
	MUL   AB
	XCH   A,R2
	XCH   A,B
	JNB   F0,MQ_PZ2
	SUBB  A,B
MQ_PZ2:	MOV   R3,A
	MOV   A,R4
	MUL   AB
	ADD   A,#0FFH
	ANL   C,F0
	MOV   A,B
	ADDC  A,R2
	MOV   R2,A
	CLR   A
	ADDC  A,R3
	MOV   R3,A              ; _ -> R23 ; R45

	CLR   C
	MOV   A,#OMR_AP		; R45=OMR_RQI-OMR_ACT
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_RPI
	MOVC  A,@A+DPTR
	SUBB  A,R4
	MOV   R4,A
	MOV   A,#OMR_AP+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   A,#OMR_RPI+1
	MOVC  A,@A+DPTR
	SUBB  A,R5
	MOV   R5,A
	MOV   B,A
	MOV   A,#OMR_AP+2
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   A,#OMR_RPI+2
	MOVC  A,@A+DPTR
	SUBB  A,R6
	MOV   C,B.7
	JB    ACC.7,MR_PZ4
	ADDC  A,#0
	JZ    MR_PZ6
	MOV   R4,#0FFH
	MOV   R5,#07FH
	SJMP  MR_PZ6
MR_PZ4:	ADDC  A,#0
	JZ    MR_PZ6
	MOV   R4,#000H
	MOV   R5,#080H		; _ -> R45 ; R6
MR_PZ6:
	MOV   A,#OMR_P		; R456=R45*OMR_P
	MOVC  A,@A+DPTR
	MOV   B,R4
	MOV   R4,A
	MUL   AB
	XCH   A,R4
	XCH   A,B
	XCH   A,R5
	MOV   R6,#0
	JNB   ACC.7,MR_PZ7
	MOV   R6,B
MR_PZ7:	MUL   AB
	ADD   A,R5
	MOV   R5,A
	CLR   A
	ADDC  A,B
	SUBB  A,R6
	MOV   R6,A		; _ R45 -> R456

	CLR   C			; R45=R456-R23
	MOV   A,R4
	MOV   R0,A		; R01=OMR_FOD
	SUBB  A,R2
	MOV   R4,A
	MOV   A,R5
	MOV   R1,A
	SUBB  A,R3
	MOV   R5,A
	MOV   A,R6
	SUBB  A,#0
	MOV   B,R3		; expand R23
	JNB   B.7,MR_PZ8
	INC   A
MR_PZ8: MOV   B,R5		; limitace R456-R23 na R45
	MOV   C,B.7
	MOV   B,A
	ADDC  A,#0
	JZ    MR_PZ9
	MOV   R4,#0FFH
	MOV   R5,#07FH
	JNB   B.7,MR_PZ9
	INC   R4
	INC   R5
MR_PZ9: MOV   A,R1		; limitace R016 na R01
	RLC   A
	MOV   A,R6
	ADDC  A,#0
	JZ    MR_PZ10
	MOV   R0,#000H
	MOV   R1,#080H
	MOV   A,R6
	JB    ACC.7,MR_PZ10
	DEC   R0
	DEC   R1		; R23 R456 -> R45 R01
MR_PZ10:

	MOV   A,#OMR_FOI	; R23=OMR_FOI*OMR_I/256
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   A,#OMR_FOI+1
	MOVC  A,@A+DPTR

	MOV   B,A
	MOV   A,#OMR_I
	MOVC  A,@A+DPTR
	MOV   R2,A
	MOV   C,B.7
	MOV   F0,C
	MUL   AB
	XCH   A,R2
	XCH   A,B
	JNB   F0,MR_PZ12
	SUBB  A,B
MR_PZ12:MOV   R3,A
	MOV   A,R6
	MUL   AB
	ADD   A,#0FFH
	ANL   C,F0
	MOV   A,B
	ADDC  A,R2
	MOV   R2,A
	CLR   A
	ADDC  A,R3
	MOV   R3,A		; _ -> 23 ; R6

	MOV   A,R2		; R45=R45+R23
	ADD   A,R4
	MOV   R4,A
	MOV   A,R3
	ADDC  A,R5
	MOV   R5,A
	JNB   OV,MR_PZ18
	JNB   ACC.7,MR_PZ16
	MOV   R4,#0FFH
	MOV   R5,#07FH
	SJMP  MR_PZ18
MR_PZ16:MOV   R4,#000H
	MOV   R5,#080H
MR_PZ18:MOV   A,R4		; OMR_FOI=R67=R45
	MOV   R6,A
	MOV   A,R5
	MOV   R7,A

	CLR   F0
	MOV   A,#OMR_ME		; Limit maximalni energie
	MOVC  A,@A+DPTR
	MOV   R2,A
	MOV   A,#OMR_ME+1
	MOVC  A,@A+DPTR
	MOV   R3,A
	MOV   A,R5
	JB    ACC.7,MR_PZ22
	ORL   A,R4
	JZ    MR_PZ28
	MOV   A,#OMR_S1		; Pridani praso +
	MOVC  A,@A+DPTR
	ADD   A,R5
	JB    OV,MR_PZ20
	MOV   R5,A
	CLR   C			; R45 < R23
	MOV   A,R4
	SUBB  A,R2
	MOV   A,R5
	SUBB  A,R3
	JC    MR_PZ28
MR_PZ20:MOV   A,R2		; Kladne preteceni MAX_ENE
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	SJMP  MR_PZ26
MR_PZ22:MOV   A,#OMR_S2		; Pridani praso -
	MOVC  A,@A+DPTR
	CPL   A
	INC   A
	ADD   A,R5
	JB    OV,MR_PZ24
	MOV   R5,A		; R45 > -R23
	MOV   A,R2
	ADD   A,R4
	MOV   A,R3
	ADDC  A,R5
	JC    MR_PZ28
MR_PZ24:CLR   C			; Zaporne preteceni MAX_ENE
	CLR   A
	SUBB  A,R2
	MOV   R4,A
	CLR   A
	SUBB  A,R3
	MOV   R5,A
MR_PZ26:MOV   A,#OMR_AS		; Kontrola pohybu pri max energii
	MOVC  A,@A+DPTR
	MOV   R2,A
	MOV   A,#OMR_AS+1
	MOVC  A,@A+DPTR
	ORL   A,R2
	JNZ   MR_PZ28
	MOV   A,#OMR_ERC
	MOVC  A,@A+DPTR
	INC   A
	CJNE  A,#40,MR_PZ30
	SETB  F0		; Preteceni energie
	DEC   A
	SJMP  MR_PZ30
MR_PZ28:CLR   A
MR_PZ30:MOV   R2,A
	MOV   A,DPL
	ADD   A,#OMR_ERC
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#0
	MOV   DPH,A
	MOV   A,R2
	MOVX  @DPTR,A
	MOV   A,DPL
	ADD   A,#LOW  (OMR_FOI-OMR_ERC)
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#HIGH (OMR_FOI-OMR_ERC)
	MOV   DPH,A
	MOV   A,R6		; OMR_FOI=R67
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0		; OMR_FOD=R01
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A
	RET

	END