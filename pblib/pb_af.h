;********************************************************************
;*                        PB_AF.H                                   *
;*              Aritmetika v plovouci radove carce   		    *
;*                  Stav ke dni 10.09.1996                          *
;*                      (C) Pisoft 1996 Pavel Pisa Praha            *
;********************************************************************

;EXTRN	CODE(xLDl,xiLDl,xiLDs,xSVl,xiSVl,xiSVs)

EXTRN	CODE(ADDf,SUBf,CMPf,MULf,DIVf)
EXTRN	CODE(LOGfRf)
EXTRN	CODE(CONVlRf,CONVl_S)
EXTRN	CODE(f2IEEE,IEEE2f)
EXTRN	CODE(Rf2AKUM,AKUM2Rf,CONVbRf)

EXTRN	DATA(AKUM)