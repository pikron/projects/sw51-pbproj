;********************************************************************
;*                        MR_DEFS.H                                 *
;*          Definice pro  multiregulatory polohy                    *
;*                  Stav ke dni 14. 8.1997                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

; Vyzaduje jiz naincludovany INCH_MMAC

POS_SD	EQU   1	  ; Subdilky celych IRC v 1 byte

; ofsety dat od adresy MR_BAS
OMR_FLG	EQU   0			;1B ; priznaky regulatoru
OMR_AP	EQU   OMR_FLG +1	;3B ; aktualni pozice
OMR_AS	EQU   OMR_AP  +3	;2B ; aktualni rychlost

; Pozadavky na PID
OMR_RP  EQU   OMR_AS  +2	;?B ; Pozadavek na pozici pro regulator
OMR_RPI EQU   OMR_RP  +POS_SD	;3B
OMR_RS  EQU   OMR_RPI +3	;?D ; Pozadovana rychlost pro regulator
OMR_RSI EQU   OMR_RS  +POS_SD	;2B

; Konstanty regulatoru
OMR_P   EQU   OMR_RSI +2	;2B ; Konstanty regulatoru
OMR_I   EQU   OMR_P   +2        ;2B
OMR_D   EQU   OMR_I   +2	;2B
OMR_S1 	EQU   OMR_D   +2        ;2B ; Dalsi parametry regulatoru
OMR_S2  EQU   OMR_S1  +2        ;2B
OMR_ME	EQU   OMR_S2  +2	;2B ; Maximalni povolena hodnota energie
OMR_MS	EQU   OMR_ME  +2	;2B ; Maximalni rychlost
OMR_ACC EQU   OMR_MS  +2	;2B ; Maximalni zrychleni
RP_NUM	EQU   8			; Pocet parametru regulatoru

OMR_FOI EQU   OMR_ACC +2	;2B ; Minula slozka I
OMR_FOD EQU   OMR_FOI +2	;2B ; Minula slozka D
OMR_ENE EQU   OMR_FOD +2	;2B ; Vypoctena energie pro motor
OMR_ERC EQU   OMR_ENE +2	;1B ; Cas po ktery se nehybe motor pri max energii

OMR_LEN SET   OMR_ERC+1

; Jednotlive masky bitu v OMR_FLG

BMR_ENI	EQU   0		; Povoleni cteni IRC
BMR_ENR	EQU   1		; Povoleni regulatoru
BMR_ENG	EQU   2		; Povoleni generatoru polohy
BMR_ERR	EQU   3		; Chyba v motorku
BMR_BSY	EQU   4		; Probiha minuly prikaz
BMR_DBG	EQU   5		; Vystup testovacich informaci

MMR_ENI	EQU   1 SHL BMR_ENI
MMR_ENR	EQU   1 SHL BMR_ENR
MMR_ENG	EQU   1 SHL BMR_ENG
MMR_ERR	EQU   1 SHL BMR_ERR
MMR_BSY	EQU   1 SHL BMR_BSY
MMR_DBG	EQU   1 SHL BMR_DBG

; Makro pro naplneni DPTR hodnotou MR_BAS+OFS
; Rusi ACC

%*DEFINE (MR_OFS2DP(OFS)) (
	MOV   A,MR_BAS
	ADD   A,#%OFS
	MOV   DPL,A
	MOV   A,MR_BAS+1
	ADDC  A,#0
	MOV   DPH,A
)