;********************************************************************
;*                    PB_UT1.ASM                                    *
;*     Pomocne rutiny pro praci s pointry, funkcemi, atd            *
;*                  Stav ke dni 18.06.1995                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

EXTRN	CODE(cLDR23i,cJMPDPP)

PUBLIC	SEL_FNC

UTIL_C	SEGMENT CODE

RSEG	UTIL_C

; Vyber funkce podle tabulky
; vstup : DPTR .. ukazatel na tabulku
;	  R7   .. klic
; vystup: vola podle tabulky funkci
;	  R23  .. podle tabulky
; format tabulky:
;     +0  Klic, 00 .. konec nebo FF .. pokracovani jinde
;     +1  Parametr R23
;     +3  Adresa fce

SEL_FNC:
SEL_FN2:CLR   A
	MOVC  A,@A+DPTR
	JZ    SEL_FNR
	INC   DPTR
	INC   A
	JZ    SEL_FN4
	DEC   A
	XRL   A,R7
	JNZ   SEL_FN3
	CALL  cLDR23i
	JMP   cJMPDPP
SEL_FN3:INC   DPTR
	INC   DPTR
	INC   DPTR
	INC   DPTR
	SJMP  SEL_FN2
SEL_FN4:CALL  cLDR23i
	MOV   DPL,R2
	MOV   DPH,R3
	JMP   SEL_FN2
SEL_FNR:RET

END