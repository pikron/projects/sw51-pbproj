#   Project file pro viceosou regulaci
#         (C) Pisoft 1996

tc.obj : tc.asm config.h
     a51 tc.asm $(par) debug

pb_uihw.obj : pb_uihw.asm config.h
	a51   pb_uihw.asm $(par) debug

codeadr=8800H
#codeadr=0000H

tc.     : tc.obj pb_uihw.obj ..\pblib\pb.lib
	l51 tc.obj,pb_uihw.obj,..\pblib\pb.lib code($(codeadr)) xdata(8000H) ramsize(100h) ixref

tc.hex  : tc.
	ohs51 tc

	  : tc.hex
	#sendhex tc.hex /p3 /m3 /t2 /g34816 /b10724
	unixcmd -d ul_sendhex -m 3 -g 0
	pause
	unixcmd -d ul_sendhex tc.hex -m 3 -g 0x8800

