$NOMOD51
;********************************************************************
;*            application template - APPTEMPL.ASM                   *
;*                       Hlavni modul                               *
;*                  Stav ke dni 13.12.2000                          *
;*                      (C) Pisoft 2000                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_AL)
;INCLUDE(%INCH_AF)
$INCLUDE(%INCH_ADR)
$INCLUDE(%INCH_REGS)
$INCLUDE(%INCH_MR_DEFS)
$INCLUDE(%INCH_UI)
$INCLUDE(%INCH_RS232)
$INCLUDE(%INCH_RSOI)
$LIST

; *******************************************************************

%DEFINE (WITH_RS232)	(0)	; s komunikaci RS232
%DEFINE (WITH_ULAN)	(1)	; s komunikaci uLan
%DEFINE (WITH_UL_OI)	(1)	; s objektovou komunikaci uLan
%DEFINE (WITH_UL_DY)	(1)	; s dynamickou adresaci
%DEFINE (WITH_UL_DY_EEP) (1)	; vyrobnim cislem v EEPROM
%DEFINE (WITH_IIC)	(1)	; s komunikaci IIC
%DEFINE (WITH_IICKB)	(1)	; se vzalenou IIC klavesnici
%DEFINE (WITH_IICRVO)	(1)	; se jednoduchou IIC klavesnici
%DEFINE (WITH_41_KL)	(0)	; se lokalni klavesnici
%DEFINE (TIME_INT_CM)   (1)	; Ktery z komparatoru pro preruseni
				; CMx & ECMx -> int T2CMPx, CMIx

;DINT600 EQU  2560 ; Deleni CLK/12 na 600 Hz pro X 18.432 MHz
;DINT25  EQU   24  ; Delitel EXINT1 na 25 Hz
DINT300 EQU   5120 ; Deleni CLK/12 na 300 Hz pro X 18.432 MHz
DINT25  EQU   12   ; Delitel EXINT1 na 25 Hz

BAUDDIV_9600	EQU	10	; pro 18.432 MHz

%DEFINE (CTRQ_MAX)   (1000)	; Max nastavitelna teplota v 0.1 C
				; bezne 800 pro 80.0 C
				
%IF(%CTRQ_MAX GE 990) THEN (
TEMP_I_F SET 0C5H
)ELSE(
TEMP_I_F SET 0C1H
)FI

; *******************************************************************

TC____C SEGMENT CODE
TC____D SEGMENT DATA
TC____B SEGMENT DATA BITADDRESSABLE
TC____X SEGMENT XDATA

; Vyuziti bank registru (USING)
;  0 .. beh v popredi
;  1 .. komunikace uLAN
;  2 .. casove preruseni
;  3 .. komunikace IIC

RSEG	TC____C
	JMP   RES_STAR		; Skok na zacatek programu

%IF (%WITH_ULAN) THEN (
EXTRN	CODE(uL_FNC,uL_STR)
EXTRN	CODE(uL_S_OP,uL_S_WR)
EXTRN	BIT(uLF_INE)
EXTRN	XDATA(uL_SBP)
EXTRN	XDATA(uL_ADR)
%IF (%WITH_UL_OI) THEN (
$INCLUDE(%INCH_UL_OI)
)FI
%IF(%WITH_UL_DY) THEN (
EXTRN	CODE(UD_INIT,UD_RQ)
)FI
)FI


%IF(%WITH_IIC)THEN(
$INCLUDE(%INCH_IIC)
)FI

PUBLIC	LEB_FLG

%IF(%WITH_IICKB)THEN(
EXTRN	CODE(UI_INIHW,UI_BEEP,KB_KPUSH,KB_IICWRLN,IHEXLDND)
EXTRN	BIT(FL_IICKB)
)FI
EXTRN	BIT(FL_DIPR)

EXTRN	CODE(cxMOVE,xxMOVE,xMDPDP,xJMPDPP,SEL_FNC,ADDATDP)
EXTRN	CODE(xLDl_PA,xLDi_PA,xSVl_PA,xSVi_PA)
EXTRN	CODE(UB_A_WR)
%IF(%WITH_41_KL)THEN(
EXTRN	CODE(PRINThb,PRINThw,INPUThw)
EXTRN	CODE(MONITOR)
EXTRN   CODE(IHEXLD)
)ELSE(
MONITOR	CODE  0
PUBLIC	MONITOR
)FI

PUBLIC	INPUTc,BEEP,KBDBEEP,ERRBEEP,RES_STAR

%IF(%WITH_UL_DY_EEP) THEN (
PUBLIC	SER_NUM
CSEG AT	8080H
SER_NUM:DS    10H	; Instrument unigue serial number
XSEG AT	8080H
	DS    10H	; XDATA overlay
)FI

RSEG	TC____B

LEB_FLG:DS    1		; Blikani ledek

HW_FLG: DS    1
ITIM_RF BIT   HW_FLG.7	; Kontrola reentrance preruseni
FL_25Hz	BIT   HW_FLG.4	; Nastaven pri preruseni
LEB_PHA	BIT   HW_FLG.3	; Pro blikani led
TMC_FANRQ BIT HW_FLG.2	; Pozadavek na vetrak
TMC_IRQEN BIT HW_FLG.1	; Povoleno volani TMC_REG
TMC_TOLOK BIT HW_FLG.0	; Regulace v toleranci

HW_FLG1: DS   1
FL_RDYR1 BIT  HW_FLG1.7 ; Vysli pouze jedno READY
FL_ECUL	 BIT  HW_FLG1.6	; Povolit komunikaci uLAN
FL_ECRS	 BIT  HW_FLG1.5	; Povolit komunikaci RS232
FL_CONF2 BIT  HW_FLG1.4 ; Prepinani konfigurece 1/2
FL_CONFK BIT  HW_FLG1.3 ; Klavesa konfigurece 1/2

RSEG	TC____X

TMP:	DS    16

C_R_PER	EQU   5
REF_PER:DS    1		; Perioda refrese displeje

RSEG	TC____C

RES_STAR:
	MOV   IEN0,#0
	MOV   IEN1,#0
	MOV   IP0,#0
	MOV   IP1,#0
	MOV   PSW,#0
	MOV   SP,#80H
	MOV   PCON,#10000000B	; Bd = OSC/12/16/(256-TH1)
	MOV   TM2CON,#10000001B; timer 2 CLK, TR2 disabled, 16 bit OV
	%VECTOR(T2CMP%TIME_INT_CM,I_TIME1); Realny cas z komparatoru
	SETB  ECM%TIME_INT_CM	; povoleni casu od CMx

	MOV   PWMP,#0FFH
	MOV   PWM0,#01H

	MOV   P4,#0FFH

	CALL  I_TIMRI
	CALL  I_TIMRI
	MOV   CINT25,#10
	CLR   A
	MOV   DPTR,#TIMRI
	MOVX  @DPTR,A
	MOV   DPTR,#TIME
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#STATUS
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A

	CLR   A
	MOV   HW_FLG,A
	MOV   HW_FLG1,A
	MOV   LEB_FLG,A
	MOV   LED_FLG,A
    %IF(0)THEN(
	MOV   DPTR,#uL_SBPO
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
    )FI
	SETB  ITIM_RF

    %IF(%WITH_41_KL)THEN(
	CALL  LCDINST
	CLR   FL_DIPR
	JNZ   RES_ST2		; Test pritomnosti LCD displaye
	SETB  FL_DIPR
	CALL  LEDWR
    )ELSE(
	CLR   FL_DIPR
    )FI
RES_ST2:
    %IF(%WITH_IIC)THEN(
	CALL  INI_IIC		; Inicializace IIC komunikace
    )FI
	MOV   DPTR,#REF_PER
	MOV   A,#C_R_PER
	MOVX  @DPTR,A

	SETB  EA		; Povolit preruseni
	JNB   FL_DIPR,L002
	MOV   DPTR,#DEVER_T
	CALL  cPRINT
	;CALL  DB_WAIT
	CALL  DB_W_10
L002:
	%LDMXi(COM_TYP,1)	; Nastaveni parametru komunikace
	%LDMXi(COM_ADR,3)	; Adresa 3
	;LDMXi(COM_SPD,3)	; Rychlost 9600
	%LDMXi(COM_SPD,4)	; Rychlost 19200

	CALL  TMC_INI		; Inicializace regulace teploty

    %IF(%WITH_IIC)THEN(
	%LDR23i(EEC_SER)	; Cteni parametru z EEPROM
	CALL  EEP_RDS
	%LDR23i(EEC_CF1)	; Cteni configurace z EEPROM
	CALL  EEP_RDS
    )FI
    %IF(%WITH_UL_DY_EEP) THEN (
	CALL  INI_SERNUM	; Initialize serial number
    )FI
	CALL  COM_INI		; Odstartovani komunikace
	SETB  TMC_IRQEN		; Spusteni regulace teploty
    %IF(%WITH_IICRVO)THEN(
	CALL  RVK_INI		; Inicializace IIC klavesnice
    )FI
	JMP   UT

DB_W_10:MOV   R0,#10H
	SJMP  DB_WAI1

DB_WAIT:MOV   R0,#0H
DB_WAI1:%WATCHDOG
	DJNZ  R1,DB_WAI1
	DJNZ  R0,DB_WAI1
	RET

DEVER_T:DB    LCD_CLR,'%VERSION'
	DB    C_LIN2 ,' (c) PiKRON 2000',0

; *******************************************************************

INPUTc:	CALL  SCANKEY
	JZ    INPUTc
	RET

; Pipnuti na klavese klavesnice
;KBDBEEP:JMP   KBDSTDB
KBDBEEP:MOV   A,#2
BEEP:	MOV   DPTR,#BEEPTIM
	MOVX  @DPTR,A
	SETB  %BEEP_FL
	MOV   DPTR,#LED       ; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
	MOV   A,R2
	RET
ERRBEEP:MOV   A,#8
    %IF(%WITH_IICKB)THEN(
	JMP   UI_BEEP
    )ELSE(
	JMP   BEEP
    )FI

; *******************************************************************
;
; Casove preruseni

PUBLIC  KBDTIMR

RSEG	TC____D

CINT25: DS    1

RSEG	TC____X

BEEPTIM:DS    1	   ; Timer delky pipani

N_OF_T  EQU   6    ; Pocet timeru
TIMR1:  DS    1    ; Timery jsou decrementovany
RVK_TIM:
TIMR2:  DS    1    ; s frekvenci 25 Hz
RVK_TUNST:
TIMR_WAIT:
TIMR3:  DS    1
REF_TIM:DS    1	   ; Refresh timr
KBDTIMR:DS    1
TIMRI:  DS    1
TIME:   DS    2    ; Cas v 0.01 min              =====

RSEG	TC____C

USING   2
I_TIME1:PUSH  ACC		; Cast s pruchodem 600 Hz
	SETB   P4.7		; !!!!!!!!!!!!!!!!!!!!!
	PUSH  PSW
	MOV   PSW,#AR0		; Prepnuti banky registru
	PUSH  B
	PUSH  DPL
	PUSH  DPH
	MOV   A,CML%TIME_INT_CM
	ADD   A,#LOW  DINT300
	MOV   CML%TIME_INT_CM,A
	MOV   A,CMH%TIME_INT_CM
	ADDC  A,#HIGH DINT300
	MOV   CMH%TIME_INT_CM,A
	CLR   CMI%TIME_INT_CM

	CLR   FL_25Hz
	DJNZ  CINT25,I_TIM10
	SETB  FL_25Hz
I_TIM10:
	CALL  ADC_D		; Cteni AD prevodniku

	JNB   FL_25Hz,I_TIMR1	; Konec casti spruchodem 600 Hz
	MOV   CINT25,#DINT25

	JNB   TMC_IRQEN,I_TIM25
	CALL  TMC_REG		; Regulace teploty
I_TIM25:CALL  ADC_S		; Spusteni cyklu prevodu ADC

    %IF(%WITH_ULAN)THEN(
	JNB   FL_ECUL,I_TIM70
	CALL  uL_STR
    )FI

I_TIM70:%WATCHDOG
	MOV   DPTR,#BEEPTIM
	MOVX  A,@DPTR
	JZ    I_TIM80
	DEC   A
	MOVX  @DPTR,A
	JNZ   I_TIM80
	CLR   %BEEP_FL
	JNB   FL_DIPR,I_TIM80
	MOV   DPTR,#LED		; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
I_TIM80:MOV   DPTR,#TIMR1
	MOV   B,#N_OF_T-1
I_TIME2:MOVX  A,@DPTR
	JZ    I_TIME3
	DEC   A
	MOVX  @DPTR,A
I_TIME3:INC   DPTR
	DJNZ  B,I_TIME2
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	JNB   ITIM_RF,I_TIMR1
	JB    ACC.7,I_TIME4
I_TIMR1:POP   DPH
	POP   DPL
	POP   B
	POP   PSW
	POP   ACC
	SETB  EA
	CLR   P4.7		; !!!!!!!!!!!!!!!!!!!!!
I_TIMRI:RETI

I_TIME4:CLR   ITIM_RF		; Pruchod 0.6 sec
;	CALL  I_TIMRI
;	MOV   PSW,#AR0		; Banka 2
	ADD   A,#15
	MOVX  @DPTR,A
    %IF(%WITH_IIC)THEN(
	CALL  IIC_STR	      ; naprava zboreneho IIC
    )FI
	MOV   A,LEB_FLG
	JBC   LEB_PHA,I_TIM42
	ORL   LED_FLG,A
	SETB  LEB_PHA
	SJMP  I_TIM43
I_TIM42:CPL   A
	ANL   LED_FLG,A
I_TIM43:SETB  ITIM_RF
	JMP   I_TIMR1

WAIT_T:	MOV   DPTR,#TIMR_WAIT
	MOVX  @DPTR,A
WAIT_T1:MOV   DPTR,#TIMR_WAIT
	MOVX  A,@DPTR
	JNZ   WAIT_T1
	RET

; *******************************************************************
; System prevodniku

RSEG	TC____X

ADC0:	DS    2
ADC1:	DS    2
ADC2:	DS    2
ADC3:	DS    2
ADC4:	DS    2
ADC5:	DS    2
ADC6:	DS    2
ADC7:	DS    2

RSEG	TC____C

ADC_D:	MOV   B,ADCON
	JNB   B.4,ADC_DR
	MOV   A,B
	ANL   A,#7
	RL    A
	ADD   A,#LOW  ADC0
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH ADC0
	MOV   DPH,A
	MOV   A,B
	ANL   A,#0C0H
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,ADCH
	MOVX  @DPTR,A
	MOV   A,B
	ANL   A,#7
	INC   A
	JNB   ACC.3,ADC_S2
	ANL   ADCON,#NOT 10H
ADC_DR:	RET

ADC_S:  CLR   A
	MOV   B,ADCON
	JB    B.3,ADC_DR
ADC_S2:	ANL   A,#07H
	MOV   ADCON,A
	SETB  ACC.3
	MOV   ADCON,A
	RET

; *******************************************************************
; Obecne definice a rutiny pro rizeni teploty

; Definice struktury regulatoru teploty zalozeneho na multiregu
OTMC_LEN SET  OMR_LEN
OTMC_FLG SET  OMR_FLG		; priznaky regulatoru MMR_ENR,...
OTMC_AT	SET   OMR_AP		; aktualni teplota v 0.01 deg
OTMC_AG	SET   OMR_AS		; gradient teploty v 0.01 deg
OTMC_RT	SET   OMR_RPI		; pozadovana teplota v 0.01 deg
OTMC_EN	SET   OMR_ENE		; pozadovane PWM
; Poradi nasledujicich promennych nelze menit ( TMC_FILT )
%STRUCTM(OTMC,FL,2)		; filtr merenych hodnot
%STRUCTM(OTMC,RD,2)		; prima data z prevodniku
%STRUCTM(OTMC,MC,2)		; sklon pro prepocet
%STRUCTM(OTMC,OC,2)		; offset pro prepocet
%STRUCTM(OTMC,TOL,2)		; tolerence regulace teploty
; Konfigurace vstupu a vystupu
%STRUCTM(OTMC,PAD,2)		; ukazatel na data z prevodniku
%STRUCTM(OTMC,VPWMJ,1)
%STRUCTM(OTMC,VPWM,2)		; ukazatel na rutinu PWM
%STRUCTM(OTMC,VOUTJ,1)
%STRUCTM(OTMC,VOUT,2)		; ukazatel na vystupni rutinu
%STRUCTM(OTMC,CFG,2)		; konfiguracni bity regulatoru
%STRUCTM(OTMC,SIB,2)		; adresa svazaneho regulatoru

BTMC_PFOI SET 1		; Pouze kladna hodnota z I
MTMC_PFOI SET 1 SHL BTMC_PFOI

BMR_TOL	EQU   6		; Teplota mimo toleranci
MMR_TOL	EQU   1 SHL BMR_TOL

%*DEFINE (TMC_OFS2DP(OFS)) (
	MOV   A,TMC_BAS
	ADD   A,#%OFS
	MOV   DPL,A
	MOV   A,TMC_BAS+1
	ADDC  A,#0
	MOV   DPH,A
)

RSEG	TC____D

TMC_BAS: DS   2		; zpracovavany regulator pro preruseni
TMC_ICNT:DS   1		; promena cyklu regulatoru

RSEG	TC____C

; Predpoklada ze DPTR ukazuje na OTMC_FL nebo TPS_FL
TMC_FILT:MOVX A,@DPTR	; OTMC_FL
	MOV   R2,A
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A		; R23 = OTMC_FL
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R2		; Ulozeni raw data
	MOVX  @DPTR,A	; OTMC_RD
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	INC   DPTR
	ORL   A,R2		; Kontrola zkratu
	JNZ   TMC_FI2
	SETB  F0
TMC_FI2:CJNE  R3,#0FFH,TMC_FI3
	MOV   A,R2
	ADD   A,#50H
	JNC   TMC_FI3		; Kontrola rozpojeni
	SETB  F0
TMC_FI3:MOVX  A,@DPTR	; OTMC_MC
	MOV   R4,A		; R45=R23*OTMC_MC/2^16
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	JNB   ACC.7,TMC_FI5
	CALL  MULi		; Zaporna konstanta OTMC_MC
	MOV   A,R6
	SUBB  A,R2
	MOV   R4,A
	MOV   A,R7
	SUBB  A,R3
	MOV   R5,A
	SJMP  TMC_FI6
TMC_FI5:CALL  MULi		; Kladna konstanta OTMC_MC
	MOV   A,R6
	MOV   R4,A
	MOV   A,R7
	MOV   R5,A
TMC_FI6:
	MOVX  A,@DPTR	; OTMC_OC
	ADD   A,R4
	MOV   R4,A		; R45=R45+OTMC_OC
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOV   R5,A
	JNB   OV,TMC_FI8
	%LDR45i(7FFFH)
	JB    ACC.7,TMC_FI8
	%LDR45i(-7FFFH)
TMC_FI8:RET

; Ulozi aktualni teplotu v R45 do struktury regulatoru
; Predpoklada ze DPTR ukazuje na OTMC, OTMC odpovida strukture OMR
; Nemeni hodnotu DPTR
TMC_SREGAT:
	%ADDDPci(OMR_AP)	; OTMC_AT=R45 a vypocet OTMC_GT
	CLR   C			; R45=OTMC_AT-old OTMC_AT
	MOVX  A,@DPTR
	XCH   A,R4
	MOVX  @DPTR,A
	SUBB  A,R4
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R5
	MOVX  @DPTR,A
	MOV   R3,#0
	JNB   ACC.7,TMC_SREGAT1	; znamenkove rozsireni
	DEC   R3
TMC_SREGAT1:
	SUBB  A,R5
	MOV   R5,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	%ADDDPci(OMR_AS-OMR_AP-2)
	MOV   A,R4		; Vypocteny gradient
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	%ADDDPci(0-OMR_AS-1)
	RET

; *******************************************************************
; Rizeni teploty

%DEFINE (TMC_EMC_IM)	(0)	; se zlepsenym EMC

EXTRN	CODE(MR_PIDLP)

EXTRN	CODE(MR_PIDP)

TC_COOL11 BIT P1.0	; Chlazeni 1. bloku
TC_HEAT11 BIT P1.1	; Topeni 1. bloku
TC_COOL12 BIT P1.2	; Chlazeni 2. bloku
TC_HEAT12 BIT P1.3	; Topeni 2. bloku
TC_HEAT2  BIT P4.0	; Externi topeni
TC_HEAT3  BIT P4.4	; Externi topeni
TC_TOLOUT BIT P4.6	; Vystup tolerance
TC_FAN    BIT P4.1	; Ventilator
%DEFINE(TC_OUTOFF)(
	ORL   P1,#0FH
)
; auxual vystupy P4.4 az P4.7, vstupy P5.5 a P5.6

TC_ADC11 XDATA ADC0	; Teplota 1. bloku
TC_ADC12 XDATA ADC1	; Teplota 2. bloku
TC_ADC2	 XDATA ADC2	; 1. externi cidlo
TC_ADC3	 XDATA ADC3	; 2. externi cidlo

RSEG	TC____X

; Struktury regulatoru teploty
TMC_1ST:
TMC11:	DS    OTMC_LEN
TMC12:	DS    OTMC_LEN
TMC2:	DS    OTMC_LEN
TMC3:	DS    OTMC_LEN
TMC_STEND:
TMC_N	EQU   (TMC_STEND-TMC_1ST)/OTMC_LEN

; Promenne pro omezeni rozdilu mezi cidly
TMC12_DL:DS   2			; maximalni pracovni rozdil

TPS_CNT:
TMC_PWC:DS    1			; Citani 25Hz do 255

%IF(%TMC_EMC_IM)THEN(
TMC_PWPM EQU  07FH		; Maska periody PWM 25Hz/128
)ELSE(
TMC_PWPM EQU  03FH		; Maska periody PWM 25Hz/64
)FI

RSEG	TC____C

; Regulace teploty
TMC_REG:
; Mereni teploty TMCx
TMC_R10:MOV   DPTR,#TMC_PWC
	MOVX  A,@DPTR
    %IF(%TMC_EMC_IM)THEN(
	JNB   ACC.1,TMC_R20
    )FI	
	; Smycka nacteni dat z prevodniku
	MOV   TMC_ICNT,#TMC_N
	MOV   DPTR,#TMC_1ST
	SJMP  TMC_R12
TMC_R11:%TMC_OFS2DP(OTMC_LEN)
TMC_R12:MOV   TMC_BAS,DPL
	MOV   TMC_BAS+1,DPH
	MOVX  A,@DPTR
	JNB   ACC.BMR_ENI,TMC_R19	; mereni vypnuto
	MOV   A,#OTMC_PAD
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OTMC_PAD+1
	MOVC  A,@A+DPTR
	MOV   DPL,R0
	MOV   DPH,A
	MOVX  A,@DPTR		; nacteni dat z prevodniku
	INC   DPTR
	RL    A
	RL    A
	ANL   A,#3
	MOV   R4,A
	MOVX  A,@DPTR
	RL    A
	RL    A
	MOV   R5,A
	ANL   A,#3
	XCH   A,R5
	ANL   A,#NOT 3
	ORL   A,R4
	MOV   R4,A
	%TMC_OFS2DP(OTMC_FL)	; Ulozeni dat z prevodniku
	MOVX  A,@DPTR
	ADD   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOVX  @DPTR,A
TMC_R19:DJNZ  TMC_ICNT,TMC_R11

; Generovani PWM
TMC_R20:MOV   TMC_ICNT,#TMC_N
	MOV   DPTR,#TMC_1ST
	SJMP  TMC_R22
TMC_R21:%TMC_OFS2DP(OTMC_LEN)
TMC_R22:MOV   TMC_BAS,DPL
	MOV   TMC_BAS+1,DPH
	MOVX  A,@DPTR
	JNB   ACC.BMR_ENI,TMC_R29	; mereni vypnuto
    %IF(0)THEN(
	JNB   ACC.BMR_ENR,TMC_R23	; regulator vypnut
    )FI
	CALL  TMC_PWMJ
TMC_R29:DJNZ  TMC_ICNT,TMC_R21

	; Citac PWM
	MOV   DPTR,#TMC_PWC
	MOVX  A,@DPTR
	INC   A			; Citac PWM casovani
	MOVX  @DPTR,A
	ANL   A,#TMC_PWPM	; Maska periody PWM 7Fh/3Fh
	JZ    TMC_R40
	RET

; Nacteni filtru a prepocet pro TMCx
TMC_R40:SETB  TMC_TOLOK
	MOV   TMC_ICNT,#TMC_N
	MOV   DPTR,#TMC_1ST
	SJMP  TMC_R42
TMC_R41:%TMC_OFS2DP(OTMC_LEN)
TMC_R42:MOV   TMC_BAS,DPL
	MOV   TMC_BAS+1,DPH
	MOVX  A,@DPTR
	JNB   ACC.BMR_ENI,TMC_R49	; mereni vypnuto
	CLR   F0
	%TMC_OFS2DP(OTMC_FL)
	CALL  TMC_FILT		; vypocet teploty TMCx
	MOV   DPL,TMC_BAS
	MOV   DPH,TMC_BAS+1	; R45=Nova Teplota 2
	CALL  TMC_SREGAT	; do TMCx_AT ^ OMR_AP
    %IF(1)THEN(
	CLR   C			; Kontrola toleranci
	MOV   A,#OTMC_AT
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OTMC_RT
	MOVC  A,@A+DPTR
	SUBB  A,R4
	MOV   R4,A
	MOV   A,#OTMC_AT+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   A,#OTMC_RT+1
	MOVC  A,@A+DPTR
	SUBB  A,R5
	MOV   R5,A		; R45 = OTMC_RT-OTMC_AT
	MOV   C,OV
	XRL   A,PSW
	JNB   ACC.7,TMC_R45
        CALL  NEGi		; abs(R45)
TMC_R45:CLR   C
	MOV   A,#OTMC_TOL
	MOVC  A,@A+DPTR
	SUBB  A,R4
	MOV   A,#OTMC_TOL+1
	MOVC  A,@A+DPTR
	SUBB  A,R5
	MOVX  A,@DPTR	; OTMC_FLG
	MOV   ACC.BMR_TOL,C
	MOVX  @DPTR,A
	JB    ACC.BMR_ERR,TMC_R46
	JNB   ACC.BMR_ENR,TMC_R47
	JNC   TMC_R47
TMC_R46:CLR   TMC_TOLOK
TMC_R47:
    )FI
	JNB   F0,TMC_R49
	CALL  TMC_SETERR
TMC_R49:DJNZ  TMC_ICNT,TMC_R41

	; Zajisteni vazeb mezi TMC11 a TMC12
	CLR   F0
	MOV   DPTR,#TMC11+OTMC_AT	; Teplota 1
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   DPTR,#TMC12+OTMC_AT	; Teplota 2
	MOVX  A,@DPTR
	SUBB  A,R4
	MOV   R4,A		; R45=TMC12_AT-R45
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,R5
	MOV   R5,A
	JB    OV,TMC_R56		; Rozdil >300 deg
	JNB   ACC.7,TMC_R53	; TMC12_AT-TMC11_AT > 0
	CALL  NEGi
TMC_R53:MOV   DPTR,#TMC12_DL
	MOVX  A,@DPTR
	SUBB  A,R4
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,R5
	MOV   R5,A		; R45=TMC1_DL-R45
	JC    TMC_R56		; mimo mezich
TMC_R54:JNB   F0,TMC_R58
; Chyba v prevodniku
TMC_R56:MOV   DPTR,#TMC11
	CALL  TMC_SETERR
	MOV   DPTR,#TMC12
	CALL  TMC_SETERR
TMC_R58:MOV   C,TMC_TOLOK	; Vystup informace o tolerancich
	MOV   TC_TOLOUT,C

; Vlastni regulatory teploty 
TMC_R70:MOV   TMC_ICNT,#TMC_N
	MOV   DPTR,#TMC_1ST
	SJMP  TMC_R72
TMC_R71:%TMC_OFS2DP(OTMC_LEN)
TMC_R72:MOV   TMC_BAS,DPL
	MOV   TMC_BAS+1,DPH
	MOVX  A,@DPTR
	JNB   ACC.BMR_ENR,TMC_R79	; regulator vypnut
	%TMC_OFS2DP(OMR_ERC)
	CLR   A
	MOVX  @DPTR,A
	MOV   DPL,TMC_BAS
	MOV   DPH,TMC_BAS+1	; Volani vlastniho regulatoru
	CALL  MR_PIDLP		; MR_PIDLP, MR_PIDP
    %IF(1)THEN(
	%TMC_OFS2DP(OTMC_EN)
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   A,#OTMC_CFG-OTMC_EN-1
	MOVC  A,@A+DPTR
	JNB   ACC.BTMC_PFOI,TMC_R75
	%TMC_OFS2DP(OMR_FOI)		; pouze kladna hodnota z I
	MOV   A,#1
	MOVC  A,@A+DPTR
	JNB   ACC.7,TMC_R75
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
TMC_R75:
    )FI
TMC_R79:DJNZ  TMC_ICNT,TMC_R71

; Vyhodnoceni ventilatoru
    %IF(0)THEN(
	SETB  TMC_FANRQ
    )FI
TMC_R95:MOV   C,TMC_FANRQ
	CPL   C
	MOV   TC_FAN,C
	CLR   TMC_FANRQ
	RET

; Skok na rutinu generovani PWM
TMC_PWMJ:MOV  A,#OTMC_VPWMJ
	JMP   @A+DPTR

; Set error for regulator pointed by DPTR
TMC_SETERR:
	MOVX  A,@DPTR
	JB    ACC.BMR_ENR,TMC_SETERR1
	RET
TMC_SETERR1:
	SETB  ACC.BMR_ERR		; Nastavit chybu
	MOVX  @DPTR,A
; Vypne regulator s adresou v DPTR i pripadny svazany regulator
TMC_OFFDP:
	MOV   A,#OTMC_SIB+1
	MOVC  A,@A+DPTR
	JZ    TMC_OFFDP1
	CALL  TMC_OFFDP1		; Vypnout definovany
	MOV   A,#OTMC_SIB		; i svazany regulator
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OTMC_SIB+1
	MOVC  A,@A+DPTR
	MOV   DPL,R0
	MOV   DPH,A
; Vypne regulator s adresou v DPTR
TMC_OFFDP1:
	MOVX  A,@DPTR
	ANL   A,#NOT (MMR_ENR OR MMR_ENG)
	MOVX  @DPTR,A
	MOV   R4,DPL
	MOV   R5,DPH
	MOV   A,#OTMC_EN
	ADD   A,R4
	MOV   DPL,A
	CLR   A
	ADDC  A,R5
	MOV   DPH,A
	CLR   A
	MOVX  @DPTR,A	; OTMC_EN=0
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPL,R4
	MOV   DPH,R5
	MOV   R4,#0
	MOV   R5,#0
; Skok na rutinu vystupu 
; sgn(R5) urcuje pozadavek topit/chladit
; nesmi menit DPTR, R1 a R67
TMC_OUTJ:MOV  A,#OTMC_VOUTJ
	JMP   @A+DPTR

; Generovani PWM pro interni termostat kolony
TMC_PTC:MOV   DPTR,#TMC_PWC
	MOVX  A,@DPTR			; Basic phase PWM
	SJMP  TMC_P30
TMC_PTCI:MOV  DPTR,#TMC_PWC
	MOVX  A,@DPTR			; Phase reversed PWM
	CPL   A
TMC_P30:ANL   A,#TMC_PWPM		; Maska periody PWM
	MOV   R4,A
	MOV   R5,#0
	MOV   DPL,TMC_BAS		
	MOV   DPL+1,TMC_BAS+1
	MOVX  A,@DPTR		; OTMC_FLG
	JNB   ACC.BMR_ENR,TMC_P39	; energie vypnuta
	MOV   A,#OTMC_AT+1
	MOVC  A,@A+DPTR		; OTMC_AT+1
	JB    ACC.7,TMC_P31
	ADD   A,#-((64*100)/256)	; Teplota > 64 stupnu
	JC    TMC_P32
TMC_P31:SETB  TMC_FANRQ			; Zapnout vetrak
TMC_P32:MOV   A,#OTMC_EN+1
	MOVC  A,@A+DPTR		; OTMC_EN+1
	JZ    TMC_P39
	JNB   ACC.7,TMC_P33
	SETB  TMC_FANRQ		; PWM na chlazeni
	CPL   A
    %IF(NOT %TMC_EMC_IM)THEN(
	CLR   C
	RRC   A
	ADDC  A,#0
	SETB  C
    )ELSE(
	INC   A
	CJNE  A,#TMC_PWPM,$+3
    )FI
	SUBB  A,R4
	JC    TMC_P39
	DEC   R5		; chladit
	SJMP  TMC_P39
TMC_P33:			; PWM na topeni
    %IF(NOT %TMC_EMC_IM)THEN(
	CLR   C
	RRC   A
	ADDC  A,#0
	SETB  C
    )ELSE(
	CJNE  A,#TMC_PWPM,$+3
    )FI
	SUBB  A,R4
	JC    TMC_P39
	INC   R5
TMC_P39:MOV   R4,#0
	SJMP  TMC_OUTJ

; Generovani PWM pro externi regulaci
TMC_PEX:MOV   DPTR,#TMC_PWC
	MOVX  A,@DPTR			; Basic phase PWM
	ANL   A,#TMC_PWPM		; Maska periody PWM
	MOV   R4,A
	MOV   R5,#0
	MOV   DPL,TMC_BAS		
	MOV   DPL+1,TMC_BAS+1
	MOVX  A,@DPTR		; OTMC_FLG
	JNB   ACC.BMR_ENR,TMC_P69	; energie vypnuta
TMC_P62:MOV   A,#OTMC_EN+1
	MOVC  A,@A+DPTR		; OTMC_EN+1
	JZ    TMC_P69
	JB    ACC.7,TMC_P69
    %IF(NOT %TMC_EMC_IM)THEN(	; PWM na topeni
	CLR   C
	RRC   A
	ADDC  A,#0
	SETB  C
    )ELSE(
	CJNE  A,#TMC_PWPM,$+3
    )FI
	SUBB  A,R4
	JC    TMC_P69
	INC   R5
TMC_P69:MOV   R4,#0
	SJMP  TMC_OUTJ

; Vystup pro 1. blok interniho termostatu kolony
TMC_O11:MOV   A,R5
	JB    ACC.7,TMC_O11N
	SETB  TC_COOL11		; Chlazeni vypnuto
	JZ    TMC_O11Z
	CLR   TC_HEAT11		; Topeni zapnuto 
	RET
TMC_O11Z:SETB TC_HEAT11		; Topeni i chlazeni vypnuto
	RET
TMC_O11N:SETB TC_HEAT11		; Topeni vypnuto 
	CLR   TC_COOL11		; Chlazeni zapnuto
	RET

; Vystup pro 2. blok interniho termostatu kolony
TMC_O12:MOV   A,R5
	JB    ACC.7,TMC_O12N
	SETB  TC_COOL12		; Chlazeni vypnuto
	JZ    TMC_O12Z
	CLR   TC_HEAT12		; Topeni zapnuto 
	RET
TMC_O12Z:SETB TC_HEAT12		; Topeni i chlazeni vypnuto
	RET
TMC_O12N:SETB TC_HEAT12		; Topeni vypnuto 
	CLR   TC_COOL12		; Chlazeni zapnuto
	RET

; Vystup pro 1. externi regulator
TMC_O2: MOV   A,R5
	JB    ACC.7,TMC_O2Z
	JZ    TMC_O2Z
	CLR   TC_HEAT2		; Topeni zapnuto 
	RET
TMC_O2Z:SETB TC_HEAT2		; Topeni vypnuto
	RET

; Vystup pro 2. externi regulator
; sgn(R5) urcuje pozadavek topit/chladit
; nesmi menit DPTR, R1 a R67
TMC_O3: MOV   A,R5
	JB    ACC.7,TMC_O3Z
	JZ    TMC_O3Z
	CLR   TC_HEAT3		; Topeni zapnuto 
	RET
TMC_O3Z:SETB TC_HEAT3		; Topeni vypnuto
	RET

TMC_INI:CLR   TMC_IRQEN
	MOV   R7,#TMC_N
	MOV   DPTR,#TMC_1ST
TMC_II1:MOV   A,#80H
	MOVX  @DPTR,A
	MOV   A,#OTMC_LEN
	CALL  ADDATDP
	DJNZ  R7,TMC_II1
	%LDR45i (TMC_1ST)
	%LDR23i (TMC_1ST_D)
	%LDR01i (OTMC_LEN*TMC_N)
	CALL  cxMOVE		; Inicializacni hodnoty vsech TMC
	MOV   R7,#TMC_N
	MOV   DPTR,#TMC_1ST
TMC_II4:MOV   A,#MMR_ENI	; Spusteni mereni
	MOVX  @DPTR,A
	%LDR45i(0)
	CALL  TMC_OUTJ		; Vypne vystupy topeni/chlazeni
	MOV   A,#OTMC_LEN
	CALL  ADDATDP
	DJNZ  R7,TMC_II4

	%LDMXi (TMC12_DL,1500)	; limit rozdilu T2-T1
	RET

; pro regulator R1 vypocte adresu parametru v ACC

TMC_GP01:CLR  A
TMC_GPA1:ADD  A,#LOW  TMC_1ST
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH TMC_1ST
	MOV   DPH,A
	MOV   A,R1
	MOV   B,#OTMC_LEN
	MUL   AB
	ADD   A,DPL
	MOV   DPL,A
	MOV   A,B
	ADDC  A,DPH
	MOV   DPH,A
	RET

; Maze chyby v regulatorech
TMC_CERDP:
	MOV   A,#OTMC_SIB+1
	MOVC  A,@A+DPTR
	JZ    TMC_CERDP1
	CALL  TMC_CERDP1
	MOV   A,#OTMC_SIB
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OTMC_SIB+1
	MOVC  A,@A+DPTR
	MOV   DPL,R0
	MOV   DPH,A
TMC_CERDP1:
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	JNB   ACC.BMR_ERR,TMC_CERDP9
	ANL   A,#NOT (MMR_ERR OR MMR_ENR OR MMR_ENG)
	MOVX  @DPTR,A
	MOV   EA,C
	JMP   TMC_OFFDP1
TMC_CERDP9:
	MOV   EA,C
	RET

; Vypne vsechna topeni
TMC_OFF:
	MOV   R7,#TMC_N
	MOV   DPTR,#TMC_1ST
TMC_OFF1:
	CALL  TMC_OFFDP1
	MOV   A,#OTMC_LEN
	CALL  ADDATDP
	DJNZ  R7,TMC_OFF1
	RET

; Vypne regulator podle R23
TMC_OFF23:
	MOV   DPL,R2
	MOV   DPH,R3
	CALL  TMC_CERDP
	JMP   TMC_OFFDP

; Zapne regulaci teploty
TMC_ON:
	%LDR23i(TMC11)
TMC_ON23:
	MOV   DPL,R2
	MOV   DPH,R3
; Zapne regulator s adresou v DPTR i pripadny svazany regulator
TMC_ONDP:
	MOV   A,#OTMC_SIB+1
	MOVC  A,@A+DPTR
	JZ    TMC_ONDP1
	CALL  TMC_ONDP1
	MOV   A,#OTMC_SIB
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OTMC_SIB+1
	MOVC  A,@A+DPTR
	MOV   DPL,R0
	MOV   DPH,A
; Zapne regulator s adresou v DPTR
TMC_ONDP1:
	MOVX  A,@DPTR
	JB    ACC.BMR_ENR,TMC_ONDP9
	PUSH  DPL
	PUSH  DPH
	%ADDDPci(OMR_FOI)
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
	MOVX  A,@DPTR
	ORL   A,#MMR_ENR OR MMR_ENI
	MOVX  @DPTR,A
TMC_ONDP9:
	RET

TMC_ONOFF:
	%LDR23i(TMC11)
TMC_ONOFF23:
	MOV   DPL,R2
	MOV   DPH,R3
TMC_ONOFFDP:
	CALL  G_TMC_STDP
	JB    ACC.BMR_ENR,TMC_ONOFF231
	JNB   ACC.BMR_ERR,TMC_ONDP
TMC_ONOFF231:
	CALL  TMC_CERDP
	JMP   TMC_OFFDP

G_TMC_STDP:
	MOV   R5,#0
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   A,#OTMC_SIB+1
	MOVC  A,@A+DPTR
	JZ    G_TMC_STDP2
	PUSH  DPL
	PUSH  DPH
	MOV   R0,A
	MOV   A,#OTMC_SIB
	MOVC  A,@A+DPTR
	MOV   DPL,A
	MOV   DPH,R0
	MOVX  A,@DPTR
	ORL   A,R4
	MOV   R4,A
	POP   DPH
	POP   DPL
G_TMC_STDP2:
	MOV   A,R4
	JNB   ACC.BMR_ERR,G_TMC_STDP3
	DEC   R5
G_TMC_STDP3:
	ANL   A,#NOT MMR_ENI
	XCH   A,R4
	RET

G_TMC_ST_S:
	CALL  G_TMC_STDP
	JB    ACC.BMR_ERR,G_TMC_ST_S9
	JNB   ACC.BMR_ENR,G_TMC_ST_S9
	MOV   A,#OMR_ENE+1
	MOVC  A,@A+DPTR
	MOV   R0,#01010101B
	JZ    G_TMC_ST_S5
	MOV   R0,#10101010B
	JNB   ACC.7,G_TMC_ST_S5
	MOV   R0,#11111111B
G_TMC_ST_S5:
	MOV   A,R0
	ANL   A,R2
	ORL   B,A
G_TMC_ST_S9:
	MOV   A,R4
	RET

G_TMC_ST:
	MOV   B,#0
	MOV   R2,#00000011B
	MOV   R3,#0F1H
	MOV   DPTR,#TMC11
	CALL  G_TMC_ST_S
	JB    ACC.BMR_ERR,TMC_ST8
	MOV   R2,#00001100B
	MOV   R3,#0F2H
	MOV   DPTR,#TMC2
	CALL  G_TMC_ST_S
	JB    ACC.BMR_ERR,TMC_ST8
	MOV   R2,#00110000B
	MOV   R3,#0F3H
	MOV   DPTR,#TMC3
	CALL  G_TMC_ST_S
	JB    ACC.BMR_ERR,TMC_ST8
	MOV   R4,B
	CLR   A
	MOV   C,TMC_TOLOK
	CPL   C
	MOV   ACC.6,C
	MOV   R5,A
	RET
TMC_ST8:MOV   A,R3
	MOV   R4,A
	RET

TMC_1ST_D:
TMCx_D	SET   $
TMC11_D:DB    0, 0,0,0, 0,0
	DB    0
	DB    0,0   			; RP
	DB    0
	DB    0,0,0			; RS
	DB    008H,0, 003H,0, 04H,0	; P I D
	DB    000H,0, 000H,0, 0,07FH	; 1 2 ME
	DB    080H,0, 010H,0		; MS MA
	DB    0,0,0,0,0,0,0
	DS    TMCx_D+OTMC_MC-$		; MC - sklon pro prepocet
	%W    (11771)			; ((T2-T1)*100*10000h)/7FE0h
	DS    TMCx_D+OTMC_OC-$		; OC - offset pro prepocet
	%W    (-586)			; T2
	DS    TMCx_D+OTMC_TOL-$
	%W    (50)			; tolerance regulace
	DS    TMCx_D+OTMC_PAD-$
	%W    (TC_ADC11)		; hodnota z prevodniku
	DS    TMCx_D+OTMC_VPWMJ-$
	DB    2				; JMP
	DW    TMC_PTC			; rutina generatoru PWM
	DS    TMCx_D+OTMC_VOUTJ-$
	DB    2				; JMP
	DW    TMC_O11			; vystupni rutina
	DS    TMCx_D+OTMC_CFG-$
	DB    0,0			; konfigurace (MTMC_PFOI,..)
	DS    TMCx_D+OTMC_SIB-$
	%W    (TMC12)			; svazany regulator
	DS    TMCx_D+OTMC_LEN-$

TMCx_D	SET   $
TMC12_D:DB    0, 0,0,0, 0,0
	DB    0
	DB    0,0   			; RP
	DB    0
	DB    0,0,0			; RS
	DB    008H,0, 003H,0, 04H,0	; P I D
	DB    000H,0, 000H,0, 0,07FH	; 1 2 ME
	DB    080H,0, 010H,0		; MS MA
	DB    0,0,0,0,0,0,0
	DS    TMCx_D+OTMC_MC-$		; MC - sklon pro prepocet
	%W    (11771)			; ((T2-T1)*100*10000h)/7FE0h
	DS    TMCx_D+OTMC_OC-$		; OC - offset pro prepocet
	%W    (-586)			; T2
	DS    TMCx_D+OTMC_TOL-$
	%W    (50)			; tolerance regulace
	DS    TMCx_D+OTMC_PAD-$
	%W    (TC_ADC12)		; hodnota z prevodniku
	DS    TMCx_D+OTMC_VPWMJ-$
	DB    2				; JMP
	DW    TMC_PTCI			; rutina generatoru PWM
	DS    TMCx_D+OTMC_VOUTJ-$
	DB    2				; JMP
	DW    TMC_O12			; vystupni rutina
	DS    TMCx_D+OTMC_CFG-$
	DB    0,0			; konfigurace (MTMC_PFOI,..)
	DS    TMCx_D+OTMC_SIB-$
	%W    (TMC11)			; svazany regulator
	DS    TMCx_D+OTMC_LEN-$

TMCx_D	SET   $
TMC2_D:	DB    0, 0,0,0, 0,0
	DB    0
	DB    0,0   			; RP
	DB    0
	DB    0,0,0			; RS
	DB    008H,0, 003H,0, 04H,0	; P I D
	DB    000H,0, 000H,0, 0,07FH	; 1 2 ME
	DB    080H,0, 010H,0		; MS MA
	DB    0,0,0,0,0,0,0
	DS    TMCx_D+OTMC_MC-$		; MC - sklon pro prepocet
	%W    (11771)			; ((T2-T1)*100*10000h)/7FE0h
	DS    TMCx_D+OTMC_OC-$		; OC - offset pro prepocet
	%W    (-586)			; T2
	DS    TMCx_D+OTMC_TOL-$
	%W    (50)			; tolerance regulace
	DS    TMCx_D+OTMC_PAD-$
	%W    (TC_ADC2)			; hodnota z prevodniku
	DS    TMCx_D+OTMC_VPWMJ-$
	DB    2				; JMP
	DW    TMC_PEX			; rutina generatoru PWM
	DS    TMCx_D+OTMC_VOUTJ-$
	DB    2				; JMP
	DW    TMC_O2			; vystupni rutina
	DS    TMCx_D+OTMC_CFG-$
	DB    MTMC_PFOI,0		; konfigurace (MTMC_PFOI,..)
	DS    TMCx_D+OTMC_SIB-$
	%W    (0)			; svazany regulator
	DS    TMCx_D+OTMC_LEN-$

TMCx_D	SET   $
TMC3_D:	DB    0, 0,0,0, 0,0
	DB    0
	DB    0,0   			; RP
	DB    0
	DB    0,0,0			; RS
	DB    008H,0, 003H,0, 04H,0	; P I D
	DB    000H,0, 000H,0, 0,07FH	; 1 2 ME
	DB    080H,0, 010H,0		; MS MA
	DB    0,0,0,0,0,0,0
	DS    TMCx_D+OTMC_MC-$		; MC - sklon pro prepocet
	%W    (11771)			; ((T2-T1)*100*10000h)/7FE0h
	DS    TMCx_D+OTMC_OC-$		; OC - offset pro prepocet
	%W    (-586)			; T2
	DS    TMCx_D+OTMC_TOL-$
	%W    (50)			; tolerance regulace
	DS    TMCx_D+OTMC_PAD-$
	%W    (TC_ADC3)			; hodnota z prevodniku
	DS    TMCx_D+OTMC_VPWMJ-$
	DB    2				; JMP
	DW    TMC_PEX			; rutina generatoru PWM
	DS    TMCx_D+OTMC_VOUTJ-$
	DB    2				; JMP
	DW    TMC_O3			; vystupni rutina
	DS    TMCx_D+OTMC_CFG-$
	DB    MTMC_PFOI,0		; konfigurace (MTMC_PFOI,..)
	DS    TMCx_D+OTMC_SIB-$
	%W    (0)			; svazany regulator
	DS    TMCx_D+OTMC_LEN-$
TMC_END_D:

UR_TEMP:MOV   A,#OU_DP
	CALL  ADDATDP
G_TEMP:	%LDR23i(10)
	CALL  xMDPDP
G_TEMP1:MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   EA,C
G_TEMP2:JB    ACC.7,G_TEMP4
G_TEMP3:CALL  DIVi	; R45=R45/R23
	MOV   A,R4
	ADDC  A,#0
	MOV   R4,A
	MOV   A,R5
	ADDC  A,#0
	MOV   R5,A
	ORL   A,#1
	RET
G_TEMP4:CALL  NEGi
	CALL  DIVi	; R45=R45/R23
	CLR   A
	SUBB  A,R4
	MOV   R4,A
	CLR   A
	SUBB  A,R5
	MOV   R5,A
	ORL   A,#1
	RET

UW_TEMP:MOV   A,#OU_DPSI
	CALL  ADDATDP
S_TEMP:	%LDR23i(10)
	CALL  MULsi
	CALL  xMDPDP
S_TEMP1:MOV   A,R4	; Limitace maximalni zadavatelne teploty
	ADD   A,#LOW (-10*%CTRQ_MAX)
	MOV   A,R5
	JNB   ACC.7,S_TEMP2
	CLR   A
	MOV   R4,A
	MOV   R5,A
	SJMP  S_TEMP3
S_TEMP2:ADDC  A,#HIGH (-10*%CTRQ_MAX)
	MOV   C,OV
	XRL   A,PSW
	JB    ACC.7,S_TEMP3
	%LDR45i(10*%CTRQ_MAX)
S_TEMP3:MOV   C,EA
	CLR   EA
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   EA,C
	ORL   A,#1
	RET

UR_TEMP_BOTH:
G_TEMP_BOTH:
	MOV   DPTR,#TMC11
	MOV   A,#OTMC_SIB+1
	MOVC  A,@A+DPTR
	JNZ   G_TEMP_B2
	MOV   A,#OTMC_AT
	CALL  ADDATDP
	%LDR23i(10)
	SJMP  G_TEMP1
G_TEMP_B2:
	MOV   C,EA
	CLR   EA
	MOV   A,#OTMC_AT
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OTMC_AT+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   EA,C
	MOV   A,#OTMC_SIB+1
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OTMC_SIB
	MOVC  A,@A+DPTR
	MOV   DPH,R0
	MOV   DPL,A
	MOV   C,EA
	CLR   EA
	MOV   A,#OTMC_AT+1
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OTMC_AT
	MOVC  A,@A+DPTR
	MOV   EA,C
	ADD   A,R4
	MOV   R4,A
	MOV   A,R0
	ADDC  A,R5
	MOV   R5,A
	MOV   C,OV
	XRL   A,PSW
	%LDR23i(20)
	JMP   G_TEMP2	; ACC.7 - Zaporny vysledek

UW_TEMP_BOTH:
S_TEMP_BOTH:
	%LDR23i(10)
	CALL  MULsi
	MOV   DPTR,#TMC11+OTMC_RT
	CALL  S_TEMP1
	MOV   DPTR,#TMC12+OTMC_RT
	JMP   S_TEMP1


; *******************************************************************
; Konfigurace komunikaci RS232 a uLAN

;FL_ECUL	Povolit komunikaci uLAN
;FL_ECRS	Povolit komunikaci RS232

RSEG	TC____X

COM_TYP:DS    2		; Typ komunikace
COM_ADR:DS    2		; Adresa jednotky
COM_SPD:DS    2		; Rychlost komunikace

RSEG	TC____C

COM_INI:CLR   ES
	CLR   FL_ECUL
	CLR   FL_ECRS
	MOV   DPTR,#COM_TYP
	MOVX  A,@DPTR
	DJNZ  ACC,COM_I50
	; Bude se pouzivat uLAN
%IF(%WITH_ULAN)THEN(
	SETB  FL_ECUL
	CALL  I_U_LAN
	CALL  uL_OINI
)FI
	RET
COM_I50:DJNZ  ACC,COM_I99
	; Bude se pouzivat RS232
%IF(%WITH_RS232)THEN(
	SETB  FL_ECRS
	CALL  COM_DIV
	MOV   R7,A
	CALL  RS232_INI
	SETB  PS
COM_RS_OPL:
	%LDMXi(RS_OPL,RS_OPL1)
)FI
COM_I99:RET

COM_DIVT:	; Tabulka delitelu frekvence
	DB    BAUDDIV_9600*8	;  1200
	DB    BAUDDIV_9600*4	;  2400
	DB    BAUDDIV_9600*2	;  4800
	DB    BAUDDIV_9600	;  9600
	DB    BAUDDIV_9600/2	; 19200
	DB    BAUDDIV_9600/3	; 28800
	DB    0

; Vraci v ACC divisor pro danou rychlost
COM_DIV:MOV   DPTR,#COM_SPD
	MOVX  A,@DPTR
	MOV   DPTR,#COM_DIVT
	MOVC  A,@A+DPTR
	RET

; Zmeni rychlost komunikace
COM_SPDCH:
	MOV   DPTR,#COM_SPD
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	MOV   DPTR,#COM_DIVT
	MOVC  A,@A+DPTR
	MOV   DPTR,#COM_SPD
	JNZ   COM_SC1
	MOVX  @DPTR,A
COM_SC1:INC   DPTR
	CLR   A
	MOVX  @DPTR,A
	JMP   COM_RQINI

UW_COMi:
	CALL  UW_Mi
	JMP   COM_RQINI

COM_WR23:
	MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	JMP   UB_A_WR

COM_TYPCH:
	MOV   DPTR,#COM_TYP
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	ADD   A,#-3
	JNC   COM_TC1
	CLR   A
	MOVX  @DPTR,A
COM_TC1:INC   DPTR
	CLR   A
	MOVX  @DPTR,A
COM_RQINI:
	SETB  FL_REFR
%IF (0) THEN (
	MOV   R2,#GL_COMCH
	MOV   R3,#0
	JMP   GLOB_RQ23
)ELSE(
	JMP   COM_INI
)FI

%IF(%WITH_ULAN)THEN(
I_U_LAN:
    %IF(0)THEN(
	CLR   A
	MOV   DPTR,#uL_SBPO
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
    )FI
	CALL  COM_DIV
	MOV   R0,#1
	CALL  uL_FNC	; Rychlost
	MOV   DPTR,#COM_ADR
	MOVX  A,@DPTR
	MOV   R0,#2
	CALL  uL_FNC	; Adresa
	MOV   R2,#0
	MOV   R0,#3
	CALL  uL_FNC	; Delka IB OB
	MOV   R2,#0
	MOV   R0,#4
	CALL  uL_FNC	; Rychle bloky
	MOV   R0,#0
	CALL  uL_FNC	; Start
%IF(%WITH_UL_DY) THEN (
	%LDR45i(STATUS)
	MOV   R6,#2
	CALL  UD_INIT
)FI
	MOV   A,#1
	RET

%IF(%WITH_UL_DY_EEP) THEN (
	DB    -'U',-'L',-'D',-'Y'
	%W   (0)
	%W   (SER_NUM)
	%W   (WR_SERNUM)
INI_SERNUM:
	MOV   R2,#010H	; pocet prenasenych byte
	MOV   R4,#0F0H	; pocatecni adresa v EEPROM
	MOV   DPTR,#SER_NUM
	CALL  EE_RD
	JZ    INI_SERNUM9
INI_SERNUM7:
	MOV   R2,#8
	MOV   DPTR,#SER_NUM
	MOV   A,#0FFH
INI_SERNUM8:
	MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R2,INI_SERNUM8
INI_SERNUM9:
	RET
WR_SERNUM:
	CLR   ES
	MOV   PSW,#0
	MOV   SP,#80H
	CALL  I_TIMRI
	CALL  I_TIMRI
	CALL  WR_SERNUM1
	JMP   RESET
WR_SERNUM1:
	MOV   R2,#010H	; pocet prenasenych byte
	MOV   R4,#0F0H	; pocatecni adresa v EEPROM
	MOV   DPTR,#SER_NUM
	CALL  EE_WR
	RET

PUBLIC	UD_NCS_ADRNVSV

; Dodana funkce pro trvale ulozeni adresy v EEPROM
UD_NCS_ADRNVSV:
	MOV   DPTR,#uL_ADR
	MOVX  A,@DPTR
	MOV   DPTR,#COM_ADR
	MOVX  @DPTR,A
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A
	JMP   MR_EEWR

)FI
)FI

; *******************************************************************
; Komunikace pres uLan - cast slave

%IF (%WITH_ULAN) THEN (

RSEG	TC____B
UD_BBBB:DS    1
UDF_RDP	BIT   UD_BBBB.7

RSEG	TC____X

TMP_U:	DS    16

RSEG	TC____C

%IF (%WITH_UL_OI) THEN (
uL_OINI:%LDR45i (OID_1IN)	; seznam prijimanych prikazu
	%LDR67i (OID_1OUT)	; seznam vysilanych prikazu
	JMP   US_INIT
UD_OI:  CALL  UI_PR
    %IF(%WITH_UL_DY) THEN (
	CALL  UD_RQ		; dynamicaka adresace
    )FI
UD_REFR:RET
; Inicializace servisni urovne
uL_SLINI:
	MOV   A,R4
	CJNE  A,#(uL_SLINIte-uL_SLINIt)/4,uL_SLINI3
uL_SLINI3:JC  uL_SLINI5
	CLR   A
uL_SLINI5:
	RL    A
	RL    A
	ADD   A,#LOW uL_SLINIt
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH uL_SLINIt
	MOV   DPH,A
	CALL  xLDl
	JMP   US_INIO
uL_SLINIt:
	%W (OID_1IN)		; Uroven 0 prijimane prikazu
	%W (OID_1OUT)		;          vysilane prikazu
	%W (OID_1INSL1)		; Uroven 1 prijimane prikazu
	%W (OID_1OUTSL1)	;          vysilane prikazu
uL_SLINIte:

)ELSE(
uL_OINI:RET
UD_OI:	RET
UD_REFR:RET
)FI

; Identifikace typu pristroje
PUBLIC	uL_IDB,uL_IDE
uL_IDB: DB    '.mt %VERSION .uP 51x'
    %IF(%WITH_UL_DY) THEN (
	DB    ' .dy'
    )FI
	DB    0
uL_IDE:

)FI

; *******************************************************************
; Prikazy pres uLan

%IF (%WITH_UL_OI) THEN (

; Kody prikazu

%OID_ADES(AI_STATUS,STATUS,u2)
%OID_ADES(AI_ERRCLR,ERRCLR,e)
I_ADCFILT EQU   208
I_ADCAl	  EQU   210
I_ADCBl	  EQU   211
I_CHA	  EQU   220
I_CHB	  EQU   221
I_CHAi	  EQU   230
I_CHBi	  EQU   231
I_OFF	  EQU   250
I_ON	  EQU   251
I_ZERRO	  EQU   255
I_FIC	  EQU   256
I_TEMP1	  EQU   301
%OID_ADES(AI_TEMP1,TEMP1,s2/.1)
I_TEMP2	  EQU   302
%OID_ADES(AI_TEMP2,TEMP2,s2/.1)
I_TEMP3	  EQU   303
%OID_ADES(AI_TEMP3,TEMP3,s2/.1)
I_TEMP11  EQU   308
%OID_ADES(AI_TEMP11,TEMP11,s2/.1)
I_TEMP12  EQU   309
%OID_ADES(AI_TEMP12,TEMP12,s2/.1)
I_TEMP1RQ EQU   311
%OID_ADES(AI_TEMP1RQ,TEMP1RQ,s2/.1)
I_TEMP2RQ EQU   312
%OID_ADES(AI_TEMP2RQ,TEMP2RQ,s2/.1)
I_TEMP3RQ EQU   313
%OID_ADES(AI_TEMP3RQ,TEMP3RQ,s2/.1)
I_TEMP_ST EQU	320
%OID_ADES(AI_TEMP_ST,TEMP_ST,u2)
I_TEMP1ST EQU	321
%OID_ADES(AI_TEMP1ST,TEMP1ST,u2)
I_TEMP2ST EQU	322
%OID_ADES(AI_TEMP2ST,TEMP2ST,u2)
I_TEMP3ST EQU	323
%OID_ADES(AI_TEMP3ST,TEMP3ST,u2)
I_TEMP_ON EQU   330
%OID_ADES(AI_TEMP_ON,TEMP_ON,e)
I_TEMP1ON EQU   331
%OID_ADES(AI_TEMP1ON,TEMP1ON,e)
I_TEMP2ON EQU   332
%OID_ADES(AI_TEMP2ON,TEMP2ON,e)
I_TEMP3ON EQU   333
%OID_ADES(AI_TEMP3ON,TEMP3ON,e)
I_TEMP_OFF EQU  340
%OID_ADES(AI_TEMP_OFF,TEMP_OFF,e)
I_TEMP1OFF EQU  341
%OID_ADES(AI_TEMP1OFF,TEMP1OFF,e)
I_TEMP2OFF EQU  342
%OID_ADES(AI_TEMP2OFF,TEMP2OFF,e)
I_TEMP3OFF EQU  343
%OID_ADES(AI_TEMP3OFF,TEMP3OFF,e)
I_TEMP1MC EQU   351
%OID_ADES(AI_TEMP1MC,TEMP1MC,s2/.2)
I_TEMP2MC EQU   352
%OID_ADES(AI_TEMP2MC,TEMP2MC,s2/.2)
I_TEMP3MC EQU   353
%OID_ADES(AI_TEMP3MC,TEMP3MC,s2/.2)
I_TEMP11MC EQU  358
%OID_ADES(AI_TEMP11MC,TEMP11MC,s2/.2)
I_TEMP12MC EQU  359
%OID_ADES(AI_TEMP12MC,TEMP12MC,s2/.2)
I_TEMP1OC EQU   361
%OID_ADES(AI_TEMP1OC,TEMP1OC,s2/.2)
I_TEMP2OC EQU   362
%OID_ADES(AI_TEMP2OC,TEMP2OC,s2/.2)
I_TEMP3OC EQU   363
%OID_ADES(AI_TEMP3OC,TEMP3OC,s2/.2)
I_TEMP11OC EQU  368
%OID_ADES(AI_TEMP11OC,TEMP11OC,s2/.2)
I_TEMP12OC EQU  369
%OID_ADES(AI_TEMP12OC,TEMP12OC,s2/.2)
I_TEMP1RD EQU   371
%OID_ADES(AI_TEMP1RD,TEMP1RD,u2/.2)
I_TEMP2RD EQU   372
%OID_ADES(AI_TEMP2RD,TEMP2RD,u2/.2)
I_TEMP3RD EQU   373
%OID_ADES(AI_TEMP3RD,TEMP3RD,u2/.2)
I_TEMP11RD EQU  378
%OID_ADES(AI_TEMP11RD,TEMP11RD,u2/.2)
I_TEMP12RD EQU  379
%OID_ADES(AI_TEMP12RD,TEMP12RD,u2/.2)
I_TEMP1ENE EQU	381
%OID_ADES(AI_TEMP1ENE,TEMP1ENE,s2)
I_TEMP2ENE EQU	382
%OID_ADES(AI_TEMP2ENE,TEMP2ENE,s2)
I_TEMP3ENE EQU	383
%OID_ADES(AI_TEMP3ENE,TEMP3ENE,s2)
I_TEMP11ENE EQU	388
%OID_ADES(AI_TEMP11ENE,TEMP11ENE,s2)
I_TEMP12ENE EQU	389
%OID_ADES(AI_TEMP12ENE,TEMP12ENE,s2)
I_SAVECFG EQU   451
%OID_ADES(AI_SAVECFG,SAVECFG,e)
I_DEFAULTCFG EQU 452
%OID_ADES(AI_DEFAULTCFG,DEFAULTCFG,e)
I_SERVLEV EQU   458
%OID_ADES(AI_SERVLEV,SERVLEV,u2)
)FI
%IF (%WITH_ULAN) THEN (
; TMCx PID controllers
I_TEMP1P  EQU  1311
%OID_ADES(AI_TEMP1P,TEMP1P,u2)
I_TEMP2P  EQU  1312
%OID_ADES(AI_TEMP2P,TEMP2P,u2)
I_TEMP3P  EQU  1313
%OID_ADES(AI_TEMP3P,TEMP3P,u2)
I_TEMP11P  EQU 1318
%OID_ADES(AI_TEMP11P,TEMP11P,u2)
I_TEMP12P  EQU 1319
%OID_ADES(AI_TEMP12P,TEMP12P,u2)
I_TEMP1I  EQU  1321
%OID_ADES(AI_TEMP1I,TEMP1I,u2)
I_TEMP2I  EQU  1322
%OID_ADES(AI_TEMP2I,TEMP2I,u2)
I_TEMP3I  EQU  1323
%OID_ADES(AI_TEMP3I,TEMP3I,u2)
I_TEMP11I  EQU 1328
%OID_ADES(AI_TEMP11I,TEMP11I,u2)
I_TEMP12I  EQU 1329
%OID_ADES(AI_TEMP12I,TEMP12I,u2)
I_TEMP1D  EQU  1331
%OID_ADES(AI_TEMP1D,TEMP1D,u2)
I_TEMP2D  EQU  1332
%OID_ADES(AI_TEMP2D,TEMP2D,u2)
I_TEMP3D  EQU  1333
%OID_ADES(AI_TEMP3D,TEMP3D,u2)
I_TEMP11D  EQU 1338
%OID_ADES(AI_TEMP11D,TEMP11D,u2)
I_TEMP12D  EQU 1339
%OID_ADES(AI_TEMP12D,TEMP12D,u2)

)FI
%IF (%WITH_ULAN) THEN (
; Prijimane hodnoty

OID_T	SET   $
	%W    (I_ERRCLR)
	%W    (OID_ISTD)
	%W    (AI_ERRCLR)
	%W    (ERRCLR_U)

%OID_NEW(I_SERVLEV,AI_SERVLEV)
	%W    (UI_INT)
	%W    (0)
	%W    (uL_SLINI)

%OID_NEW(I_SAVECFG ,AI_SAVECFG)
	%W    (SAVECFG_U)

%OID_NEW(I_DEFAULTCFG ,AI_DEFAULTCFG)
	%W    (DEFAULTCFG_U)

%OID_NEW(I_TEMP_OFF,AI_TEMP_OFF)
	%W    (TMC_OFF)

%OID_NEW(I_TEMP1OFF,AI_TEMP1OFF)
	%W    (TMC_OFF_U)
	%W    (TMC11)

%OID_NEW(I_TEMP2OFF,AI_TEMP2OFF)
	%W    (TMC_OFF_U)
	%W    (TMC2)

%OID_NEW(I_TEMP3OFF,AI_TEMP3OFF)
	%W    (TMC_OFF_U)
	%W    (TMC3)

%OID_NEW(I_TEMP_ON,AI_TEMP_ON)
	%W    (TMC_ON)

%OID_NEW(I_TEMP1ON,AI_TEMP1ON)
	%W    (TMC_ON_U)
	%W    (TMC11)

%OID_NEW(I_TEMP2ON,AI_TEMP2ON)
	%W    (TMC_ON_U)
	%W    (TMC2)

%OID_NEW(I_TEMP3ON,AI_TEMP3ON)
	%W    (TMC_ON_U)
	%W    (TMC3)

%OID_NEW(I_TEMP1,AI_TEMP1)
	%W    (UI_INT)
	%W    (0)
    %IF(1)THEN(
	%W    (S_TEMP_BOTH)
    )ELSE(
	%W    (S_TEMP)
	%W    (TMC1+OTMC_RT)
    )FI

%OID_NEW(I_TEMP1RQ,AI_TEMP1RQ)
	%W    (UI_INT)
	%W    (0)
    %IF(1)THEN(
	%W    (S_TEMP_BOTH)
    )ELSE(
	%W    (S_TEMP)
	%W    (TMC1+OTMC_RT)
    )FI

%OID_NEW(I_TEMP2,AI_TEMP2)
	%W    (UI_INT)
	%W    (0)
	%W    (S_TEMP)
	%W    (TMC2+OTMC_RT)

%OID_NEW(I_TEMP2RQ,AI_TEMP2RQ)
	%W    (UI_INT)
	%W    (0)
	%W    (S_TEMP)
	%W    (TMC2+OTMC_RT)

%OID_NEW(I_TEMP3,AI_TEMP3)
	%W    (UI_INT)
	%W    (0)
	%W    (S_TEMP)
	%W    (TMC3+OTMC_RT)

%OID_NEW(I_TEMP3RQ,AI_TEMP3RQ)
	%W    (UI_INT)
	%W    (0)
	%W    (S_TEMP)
	%W    (TMC3+OTMC_RT)

OID_1IN SET   OID_T

%OID_NEW(I_TEMP11,AI_TEMP11)
	%W    (UI_INT)
	%W    (0)
	%W    (S_TEMP)
	%W    (TMC11+OTMC_RT)

%OID_NEW(I_TEMP12,AI_TEMP12)
	%W    (UI_INT)
	%W    (0)
	%W    (S_TEMP)
	%W    (TMC12+OTMC_RT)

%IF(0)THEN(
%OID_NEW(I_TEMP1MC,AI_TEMP1MC)
	%W    (UI_INT)
	%W    (TMC1+OTMC_MC)
	%W    (0)
)FI

%OID_NEW(I_TEMP2MC,AI_TEMP2MC)
	%W    (UI_INT)
	%W    (TMC2+OTMC_MC)
	%W    (0)

%OID_NEW(I_TEMP3MC,AI_TEMP3MC)
	%W    (UI_INT)
	%W    (TMC3+OTMC_MC)
	%W    (0)

%OID_NEW(I_TEMP11MC,AI_TEMP11MC)
	%W    (UI_INT)
	%W    (TMC11+OTMC_MC)
	%W    (0)

%OID_NEW(I_TEMP12MC,AI_TEMP12MC)
	%W    (UI_INT)
	%W    (TMC12+OTMC_MC)
	%W    (0)

%IF(0)THEN(
%OID_NEW(I_TEMP1OC,AI_TEMP1OC)
	%W    (UI_INT)
	%W    (TMC1+OTMC_OC)
	%W    (0)
)FI

%OID_NEW(I_TEMP2OC,AI_TEMP2OC)
	%W    (UI_INT)
	%W    (TMC2+OTMC_OC)
	%W    (0)

%OID_NEW(I_TEMP3OC,AI_TEMP3OC)
	%W    (UI_INT)
	%W    (TMC3+OTMC_OC)
	%W    (0)

%OID_NEW(I_TEMP11OC,AI_TEMP11OC)
	%W    (UI_INT)
	%W    (TMC11+OTMC_OC)
	%W    (0)

%OID_NEW(I_TEMP12OC,AI_TEMP12OC)
	%W    (UI_INT)
	%W    (TMC12+OTMC_OC)
	%W    (0)
)FI
%IF (%WITH_ULAN) THEN (

%IF(0)THEN(
%OID_NEW(I_TEMP11ENE,AI_TEMP11ENE)
	%W    (UI_INT)
	%W    (TMC11+OTMC_EN)
	%W    (0)

%OID_NEW(I_TEMP12ENE,AI_TEMP12ENE)
	%W    (UI_INT)
	%W    (TMC12+OTMC_EN)
	%W    (0)
)FI

; PID controllers
%IF(0)THEN(
%OID_NEW(I_TEMP1P,AI_TEMP1P)
	%W    (UI_INT)
	%W    (TMC1+OMR_P)
	%W    (0)
)FI

%OID_NEW(I_TEMP2P,AI_TEMP2P)
	%W    (UI_INT)
	%W    (TMC2+OMR_P)
	%W    (0)

%OID_NEW(I_TEMP3P,AI_TEMP3P)
	%W    (UI_INT)
	%W    (TMC3+OMR_P)
	%W    (0)

%OID_NEW(I_TEMP11P,AI_TEMP11P)
	%W    (UI_INT)
	%W    (TMC11+OMR_P)
	%W    (0)

%OID_NEW(I_TEMP12P,AI_TEMP12P)
	%W    (UI_INT)
	%W    (TMC12+OMR_P)
	%W    (0)

%IF(0)THEN(
%OID_NEW(I_TEMP1I,AI_TEMP1I)
	%W    (UI_INT)
	%W    (TMC1+OMR_I)
	%W    (0)
)FI

%OID_NEW(I_TEMP2I,AI_TEMP2I)
	%W    (UI_INT)
	%W    (TMC2+OMR_I)
	%W    (0)

%OID_NEW(I_TEMP3I,AI_TEMP3I)
	%W    (UI_INT)
	%W    (TMC3+OMR_I)
	%W    (0)

%OID_NEW(I_TEMP11I,AI_TEMP11I)
	%W    (UI_INT)
	%W    (TMC11+OMR_I)
	%W    (0)

%OID_NEW(I_TEMP12I,AI_TEMP12I)
	%W    (UI_INT)
	%W    (TMC12+OMR_I)
	%W    (0)

%IF(0)THEN(
%OID_NEW(I_TEMP1D,AI_TEMP1D)
	%W    (UI_INT)
	%W    (TMC1+OMR_D)
	%W    (0)
)FI

%OID_NEW(I_TEMP2D,AI_TEMP2D)
	%W    (UI_INT)
	%W    (TMC2+OMR_D)
	%W    (0)

%OID_NEW(I_TEMP3D,AI_TEMP3D)
	%W    (UI_INT)
	%W    (TMC3+OMR_D)
	%W    (0)

%OID_NEW(I_TEMP11D,AI_TEMP11D)
	%W    (UI_INT)
	%W    (TMC11+OMR_D)
	%W    (0)

%OID_NEW(I_TEMP12D,AI_TEMP12D)
	%W    (UI_INT)
	%W    (TMC12+OMR_D)
	%W    (0)

OID_1INSL1 SET   OID_T

)FI
%IF (%WITH_ULAN) THEN (

; -------------------------------------
; Vysilane hodnoty

OID_T	SET   0

%OID_NEW(I_STATUS,AI_STATUS)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TMC_ST)

%OID_NEW(I_TEMP1ST,AI_TEMP1ST)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TMC_ST_U)
	%W    (TMC11)

%OID_NEW(I_TEMP2ST,AI_TEMP2ST)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TMC_ST_U)
	%W    (TMC2)

%OID_NEW(I_TEMP3ST,AI_TEMP3ST)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TMC_ST_U)
	%W    (TMC3)

%IF(0)THEN(
%OID_NEW(I_TEMP1ENE,AI_TEMP1ENE)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC1+OTMC_EN)
)FI

%OID_NEW(I_TEMP2ENE,AI_TEMP2ENE)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC2+OTMC_EN)

%OID_NEW(I_TEMP3ENE,AI_TEMP3ENE)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC3+OTMC_EN)

%OID_NEW(I_TEMP11ENE,AI_TEMP11ENE)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC11+OTMC_EN)

%OID_NEW(I_TEMP12ENE,AI_TEMP12ENE)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC12+OTMC_EN)

%IF(0)THEN(
%OID_NEW(I_TEMP1MC,AI_TEMP1MC)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC1+OTMC_MC)
)FI

%OID_NEW(I_TEMP2MC,AI_TEMP2MC)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC2+OTMC_MC)

%OID_NEW(I_TEMP3MC,AI_TEMP3MC)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC3+OTMC_MC)

%OID_NEW(I_TEMP11MC,AI_TEMP11MC)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC11+OTMC_MC)

%OID_NEW(I_TEMP12MC,AI_TEMP12MC)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC12+OTMC_MC)

%IF(0)THEN(
%OID_NEW(I_TEMP1OC,AI_TEMP1OC)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC1+OTMC_OC)
)FI

%OID_NEW(I_TEMP2OC,AI_TEMP2OC)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC2+OTMC_OC)

%OID_NEW(I_TEMP3OC,AI_TEMP3OC)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC3+OTMC_OC)

%OID_NEW(I_TEMP11OC,AI_TEMP11OC)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC11+OTMC_OC)

%OID_NEW(I_TEMP12OC,AI_TEMP12OC)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC12+OTMC_OC)

%IF(0)THEN(
%OID_NEW(I_TEMP1RD,AI_TEMP1RD)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC1+OTMC_RD)
)FI

%OID_NEW(I_TEMP2RD,AI_TEMP2RD)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC2+OTMC_RD)

%OID_NEW(I_TEMP3RD,AI_TEMP3RD)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC3+OTMC_RD)

%OID_NEW(I_TEMP11RD,AI_TEMP11RD)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC11+OTMC_RD)

%OID_NEW(I_TEMP12RD,AI_TEMP12RD)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC12+OTMC_RD)
)FI
%IF (%WITH_ULAN) THEN (

%OID_NEW(I_TEMP1,AI_TEMP1)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TEMP_BOTH)
	%W    (TMC11+OTMC_AT)

%OID_NEW(I_TEMP2,AI_TEMP2)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TEMP)
	%W    (TMC2+OTMC_AT)

%OID_NEW(I_TEMP3,AI_TEMP3)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TEMP)
	%W    (TMC3+OTMC_AT)

%OID_NEW(I_TEMP11,AI_TEMP11)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TEMP)
	%W    (TMC11+OTMC_AT)

%OID_NEW(I_TEMP12,AI_TEMP12)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TEMP)
	%W    (TMC12+OTMC_AT)

OID_1OUT SET  OID_T

)FI
%IF (%WITH_ULAN) THEN (

; PID controllers
%IF(0)THEN(
%OID_NEW(I_TEMP1P,AI_TEMP1P)
	%W    (UO_INT)
	%W    (TMC1+OMR_P)
	%W    (0)
)FI

%OID_NEW(I_TEMP2P,AI_TEMP2P)
	%W    (UO_INT)
	%W    (TMC2+OMR_P)
	%W    (0)

%OID_NEW(I_TEMP3P,AI_TEMP3P)
	%W    (UO_INT)
	%W    (TMC3+OMR_P)
	%W    (0)

%OID_NEW(I_TEMP11P,AI_TEMP11P)
	%W    (UO_INT)
	%W    (TMC11+OMR_P)
	%W    (0)

%OID_NEW(I_TEMP12P,AI_TEMP12P)
	%W    (UO_INT)
	%W    (TMC12+OMR_P)
	%W    (0)

%IF(0)THEN(
%OID_NEW(I_TEMP1I,AI_TEMP1I)
	%W    (UO_INT)
	%W    (TMC1+OMR_I)
	%W    (0)
)FI

%OID_NEW(I_TEMP2I,AI_TEMP2I)
	%W    (UO_INT)
	%W    (TMC2+OMR_I)
	%W    (0)

%OID_NEW(I_TEMP3I,AI_TEMP3I)
	%W    (UO_INT)
	%W    (TMC3+OMR_I)
	%W    (0)

%OID_NEW(I_TEMP11I,AI_TEMP11I)
	%W    (UO_INT)
	%W    (TMC11+OMR_I)
	%W    (0)

%OID_NEW(I_TEMP12I,AI_TEMP12I)
	%W    (UO_INT)
	%W    (TMC12+OMR_I)
	%W    (0)

%IF(0)THEN(
%OID_NEW(I_TEMP1D,AI_TEMP1D)
	%W    (UO_INT)
	%W    (TMC1+OMR_D)
	%W    (0)
)FI

%OID_NEW(I_TEMP2D,AI_TEMP2D)
	%W    (UO_INT)
	%W    (TMC2+OMR_D)
	%W    (0)

%OID_NEW(I_TEMP3D,AI_TEMP3D)
	%W    (UO_INT)
	%W    (TMC3+OMR_D)
	%W    (0)

%OID_NEW(I_TEMP11D,AI_TEMP11D)
	%W    (UO_INT)
	%W    (TMC11+OMR_D)
	%W    (0)

%OID_NEW(I_TEMP12D,AI_TEMP12D)
	%W    (UO_INT)
	%W    (TMC12+OMR_D)
	%W    (0)

OID_1OUTSL1 SET  OID_T
)FI
%IF (%WITH_ULAN) THEN (

; Nulovani chyboveho stavu
ERRCLR_U:
	MOV   R7,#TMC_N
	MOV   DPTR,#TMC_1ST
ERRCLR_U1:
	CALL  TMC_CERDP1
	MOV   A,#OTMC_LEN
	CALL  ADDATDP
	DJNZ  R7,ERRCLR_U1
	RET

; Ukladani konfigurace

SAVECFG_U:%LDR23i(EEC_SER); Ulozeni parametru do EEPROM
	JMP   EEP_WRS

DEFAULTCFG_U:
	CALL  TMC_OFF
	CALL  TMC_INI
	SETB  TMC_IRQEN
	RET

; Pomocne prikazy

G_ATOMi:CALL  xMDPDP
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   EA,C
	RET

; Rizeni teploty
TMC_ON_U:
	CALL  xMDPDP
	JMP   TMC_ONDP

TMC_OFF_U:
	CALL  xMDPDP
	JMP   TMC_OFFDP

G_TMC_ST_U:
	CALL  xMDPDP
	JMP   G_TMC_STDP

)FI

; *******************************************************************
; Komunikace IIC

%IF(%WITH_IIC)THEN(

; Typ v 1. byte IIC zpravy
; zpravy pro terminal
IIC_TEC	EQU   51H	; Rizeni IIC klavesnice a displaye
IIC_TEK	EQU   52H	; Informace o stisnutych klavesach
IIC_TED	EQU   53H	; Zobrazovani dat na display

; Adresa jednotky
IIC_ADR	EQU   10H

RSEG	TC____X

SLAV_BL EQU   100
IIC_INP:DS    SLAV_BL
IIC_OUT:DS    SLAV_BL
IIC_BUF:DS    SLAV_BL

RSEG	TC____C

INI_IIC:MOV   S1ADR,#IIC_ADR
	CALL  IIC_PRE		; S1CON 11000000B ; 11000001B
	;MOV   S1CON,#01000000B	; S1CON pro X 24.000 MHz
	MOV   S1CON,#11000000B	; 11000001B pro X 18.432
	%LDR45i(IIC_INP)
	%LDR67i(SL_CMIC)
	MOV   R2,#SLAV_BL OR 080H
	CALL  IIC_SLX
	CLR   A			; Nulovani I2M pridelovace mastera
	MOV   R0,#6
	MOV   DPTR,#I2M_RQP	; a I2M_RQA a I2M_CB
INI_II3:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,INI_II3
	RET

SL_CM_R:JMP   SL_JRET

; Zpracovavani prikazu z IIC pod prerusenim
; registry  R0  .. funkce SJ_TSTA,SJ_TEND,SJ_RSTA,SJ_REND
;           R12 .. ukazatel na data
;           R3  .. pocet byte do konce S_BLEN
;	    S_DP   .. ukazatel na zacatek bufferu
;	    S_RLEN .. pro SJ_REND jiz naplnen poctem prijatych byte

SL_CMIC:CJNE  R0,#SJ_REND,SL_CM_R
	PUSH  DPL
	PUSH  DPH
	MOV   DPL,S_DP		; Bufer s prijmutymi daty
	MOV   DPH,S_DP+1
	MOVX  A,@DPTR		; Prijmuty prikaz
	INC   DPTR
   %IF(%WITH_IICKB)THEN(
	CJNE  A,#IIC_TEK,SL_CM20
	MOV   A,S_RLEN		; Kody stisknutych klaves
	DEC   A			; Delka dat prikazu
	MOV   R3,A
	JZ    SL_CM19
SL_CM15:MOVX  A,@DPTR
	INC   DPTR
	MOV   R4,DPL
	MOV   R5,DPH
	CALL  KB_KPUSH		; Ulozit kod klavesy
	MOV   DPL,R4
	MOV   DPH,R5
	DJNZ  R3,SL_CM15
SL_CM19:JMP   SL_CM_A
   )FI
SL_CM20:
	JMP   SL_CM_N

SL_CM_A:CLR   ICF_SRC
	SETB  AA
SL_CM_N:MOV   R0,#SJ_REND
	POP   DPH
	POP   DPL
	JMP   SL_JRET

)FI

; *******************************************************************
; Pridelovani casu mastera

%IF(%WITH_IIC)THEN(

RSEG	TC____X

OI2M_LEN SET  0
%STRUCTM(OI2M,NEXT,2)	; Dalsi pozadavek
%STRUCTM(OI2M,FNCP,2)	; Ukazatel na funkci

I2M_RQP:DS    2		; Ukazatel na prvni pozadovanou akci
I2M_RQA:DS    2		; Ukazatel na prave probihajici akci
I2M_CB:	DS    2		; Callback pri ukonceni prenosu

RSEG	TC____C

; Test ukonceni prenosu
; pri volne sbernici vraci ACC=0
I2M_TBF:CLR   C
	JB    ICF_MER,I2M_TBF1
	JNB   ICF_MRQ,I2M_TBF2
	MOV   A,#1
I2M_RET1:RET
I2M_TBF1:CALL IIC_CER
I2M_TBF2:MOV  DPTR,#I2M_CB	; Konec
	MOVX  A,@DPTR
	MOV   R2,A
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	CLR   A
	MOVX  @DPTR,A
	MOV   A,R2
	ORL   A,R3
	JZ    I2M_RET1		; Je I2M_CB
I2M_TBF4:MOV  DPL,R2
	MOV   DPH,R3
	CLR   A
	JMP   @A+DPTR		; Pri chybe nastaveno CY

; Postupne pracuje podle I2M_RQP
I2M_POOL:CALL I2M_TBF
	JNZ   I2M_RET1
	MOV   DPTR,#I2M_RQA
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R0
	MOV   DPH,A
	ORL   A,R0
	JNZ   I2M_PO1
	MOV   DPTR,#I2M_RQP
I2M_PO1:MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   DPTR,#I2M_RQA
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	ORL   A,R4
	JZ    I2M_RET1
	MOV   DPL,R4
	MOV   DPH,R5
	INC   DPTR
	INC   DPTR
	JMP   xJMPDPP

; Stejne jako I2M_ADD ale nastavi funkci na R67
I2M_ADDF:MOV  DPL,R4
	MOV   DPH,R5
	INC   DPTR
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A

; Prida OI2M v R45 do dotazovaciho cyklu
I2M_ADD:MOV   DPTR,#I2M_RQP
	MOVX  A,@DPTR
	MOV   R6,A
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   DPL,R4
	MOV   DPH,R5
	MOV   A,R6
	MOVX  @DPTR,A		; OI2M_NEXT
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	RET

; Vyjme OI2M v R45 z dotazovaciho cyklu
I2M_REM:MOV   DPTR,#I2M_RQA
	MOVX  A,@DPTR
	XRL   A,R4
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	XRL   A,R5
	ORL   A,R0
	JNZ   I2M_REM2
	CLR   A
	MOV   DPTR,#I2M_RQA
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
I2M_REM2:MOV  DPTR,#I2M_RQP
I2M_REM3:MOV  R2,DPL		; Vyjmuti z linkovaneho listu
	MOV   R3,DPH
	MOVX  A,@DPTR
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
	ORL   A,R6
	JZ    I2M_REM5
	MOV   DPL,R6
	MOV   DPH,R7
	MOV   A,R6
	XRL   A,R4
	JNZ   I2M_REM3
	MOV   A,R7
	XRL   A,R5
	JNZ   I2M_REM3
	MOVX  A,@DPTR
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
	MOV   DPL,R2
	MOV   DPH,R3
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
I2M_REM5:RET

)FI

; *******************************************************************
; Ovladani displeje pres 8577

%IF(%WITH_IICRVO)THEN(

RVLED_A EQU	40H
RVKBD_A EQU	42H
RVDIS_A EQU     74H
RVDIS_L EQU     4

; Definice segmentu
lcda    SET     02h             ;lcd segment a
lcdb    SET     01h             ;lcd segment b
lcdc    SET     04h             ;lcd segment c
lcdd    SET     10h             ;lcd segment d
lcde    SET     40h             ;lcd segment e
lcdf    SET     08h             ;lcd segment f
lcdg    SET     20h             ;lcd segment g
lcdh    SET     80h             ;lcd segment h colon

RSEG    TC____D

WR_BUF: DS    RVDIS_L+1

RSEG    TC____C

; Vypsani WR_BUF po IIC na DISPLAY
OUT_BUF:MOV   A,R6
	ANL   A,#0F8H
	MOV   R0,#WR_BUF
	MOV   @R0,A
	MOV   R6,#RVDIS_A
	MOV   R4,#WR_BUF
	MOV   R2,#RVDIS_L+1
	MOV   R3,#0
	JMP   IIC_RQI

;Vystup cisla na LCD display pres prevod LCD_TBL a LCD_POS
;Vstup: R45   vypisovane cislo
;       R7    .. format vystupu cisla
;                  SLLLxADD
;            S   - signed
;            A   - znamenko nebo cislice
;                  jinak znamenko nebo blank
;            LLL - delka vypisu > 0
;            DD  - pocet desetinych mist
;       R6    .. pozice vypisu

iPRTLi: MOV   A,R5
	MOV   C,ACC.7
	MOV   A,R7
	JNB   ACC.7,iPRTLi3
	JC    iPRTLi1
	JB    ACC.2,iPRTLi3
	MOV   A,#lcdBlnk
	JNC   iPRTLi2
iPRTLi1:CALL  NEGi
	MOV   A,#lcdSig
iPRTLi2:CALL  iPRTLc
	MOV   A,R7
	ADD   A,#-10H
	MOV   R7,A
iPRTLi3:MOV   A,R7
	SWAP  A
	ANL   A,#7
	MOV   R1,A
iPRTLi9:DEC   R1
	MOV   A,R1
	RL    A
	CPL   A
	ADD   A,#O10E4i-1+5*2
	MOV   R0,A
	CALL  rLDR23i
iPRTL10:MOV   R0,#-1
iPRTL11:CJNE  R0,#9,iPRTL12
iPRTLiE:MOV   A,#lcd_Err
	CALL  iPRTLc
	MOV   A,R7
	ANL   A,#70H
	ADD   A,#-10H
	MOV   R7,A
	JNZ   iPRTLiE
	RET
iPRTL12:CALL  SUBi
	INC   R0
	JNC   iPRTL11
	CALL  ADDi
	MOV   A,R7
	ANL   A,#3
	DEC   A
	XRL   A,R1
	JZ    iPRTL13
	CLR   C
iPRTL13:MOV   A,R0
	MOV   ACC.7,C
	CALL  iPRTLc
	MOV   A,R7
	CLR   ACC.7
	ADD   A,#-10H
	MOV   R7,A
	ANL   A,#70H
	JNZ   iPRTLi9
	RET

; Rotace vlevo o 4 bity

RL4R45: MOV   A,R4
	SWAP  A
	MOV   R4,A
	ANL   A,#0F0H
	XCH   A,R4
	ANL   A,#00FH
	XCH   A,R5
	SWAP  A
	XCH   A,R4
	XRL   A,R4
	XCH   A,R4
	ANL   A,#0F0H
	XCH   A,R5
	ORL   A,R5
	XCH   A,R5
	XRL   A,R4
	MOV   R4,A
	RET

)FI
%IF(%WITH_IICRVO)THEN(

; Vystup hexa
;Vstup: R45   vypisovane cislo
;       R6    .. pozice vypisu
iPRTLhw:MOV   R7,#4
iPRTLh: MOV   A,R5
	SWAP  A
	ANL   A,#00FH
	CALL  iPRTLc
	CALL  RL4R45
	DJNZ  R7,iPRTLh
	RET

iPRTLc: MOV     R0,#0
	JNB     ACC.7,iPRTLc1
	CLR     ACC.7
	MOV     R0,#lcdH
iPRTLc1:ADD     A,#LCD_TBL-iPRTLck
	MOVC    A,@A+PC
iPRTLck:ORL     A,R0
iPRTLg: MOV     R0,A
	MOV     A,R6
	INC     R6
	ANL     A,#07H
	ADD     A,#LCD_POS-iPRTLcl
	MOVC    A,@A+PC
iPRTLcl:ADD     A,#WR_BUF+1
	XCH     A,R0
	MOV     @R0,A
	RET

lcdBlnk SET     10H
lcdSig  SET     17h
lcd_Err SET     17h ; = '-' dalsi moznost 0Eh = 'E'

LCD_POS:DB      3
	DB      2
	DB      0
	DB      1

LCD_TBL:db      lcda+lcdb+lcdc+lcdd+lcde+lcdf           ;0
	db      lcdb+lcdc                               ;1
	db      lcda+lcdb+lcdg+lcde+lcdd                ;2
	db      lcda+lcdb+lcdg+lcdc+lcdd                ;3
	db      lcdf+lcdg+lcdb+lcdc                     ;4
	db      lcda+lcdf+lcdg+lcdc+lcdd                ;5
	db      lcda+lcdf+lcdg+lcde+lcdd+lcdc           ;6
	db      lcda+lcdb+lcdc                          ;7
	db      lcda+lcdb+lcdc+lcdd+lcde+lcdf+lcdg      ;8
	db      lcda+lcdb+lcdf+lcdg+lcdc+lcdd           ;9
	db      lcda+lcdb+lcdf+lcdg+lcdc+lcde           ;A
	db      lcdc+lcdd+lcde+lcdf+lcdg                ;b
	db      lcda+lcdd+lcde+lcdf                     ;C
	db      lcde+lcdg+lcdd+lcdc+lcdb                ;d
	db      lcda+lcdd+lcde+lcdf+lcdg                ;E
	db      lcda+lcde+lcdf+lcdg                     ;F
	db      0                                       ;blank
	db      lcda                                    ;segment a
	db      lcdb                                    ;segment b
	db      lcdc                                    ;segment c
	db      lcdd                                    ;segment d
	db      lcde                                    ;segment e
	db      lcdf                                    ;segment f
	db      lcdg                                    ;segment g
	db      lcdb+lcdc+lcde+lcdf+lcdg                ;H
	db      lcdd+lcde+lcdf                          ;L
	db      lcda+lcdb+lcdf+lcdg                     ;degree
	db      lcdc+lcdd+lcde+lcdg                     ;lower degree
	db      0                                       ;spare
	db      0                                       ;spare
	db      0                                       ;spare
	db      0                                       ;spare
	db      lcdh                                    ;colon

)FI
%IF(%WITH_IICRVO)THEN(

RVK_TST:
	CALL  IIC_WME		; cekat na ukonceni prenosu
	%LDR45i(1234)
	MOV   R6,#10H
;       CALL  iPRTLhw
	MOV   R7,#0C4H
	CALL  iPRTLi
	MOV   R6,#10H
	CALL  OUT_BUF
	CALL  IIC_WME		; cekat na ukonceni prenosu

	;JNZ
RVK_TST1:
	MOV   A,#055H
RVK_TST2:
	CALL  SCANKEY
	JNZ   RVK_TST9

	MOV   WR_BUF,A
	MOV   R6,#RVLED_A
	MOV   R4,#WR_BUF
	MOV   R2,#1
	MOV   R3,#0
	CALL  IIC_RQI
	CALL  IIC_WME		; cekat na ukonceni prenosu

	MOV   R6,#RVKBD_A
	MOV   R4,#WR_BUF
	MOV   R2,#0
	MOV   R3,#1
	CALL  IIC_RQI
	CALL  IIC_WME		; cekat na ukonceni prenosu
	JNZ   RVK_TST1
	MOV   A,WR_BUF
	CPL   A
	JZ    RVK_TST2

	PUSH  ACC
	MOV   R4,A
	MOV   R5,B
	INC   B
	MOV   R6,#10H
	CALL  iPRTLhw
	MOV   R6,#10H
	CALL  OUT_BUF
	CALL  IIC_WME		; cekat na ukonceni prenosu

	POP   ACC
	JMP   RVK_TST2

RVK_TST9:
	RET

)FI

; *******************************************************************
; Klavesnice na IIC

%IF(%WITH_IICRVO)THEN(

RVKL_BASE EQU 060H
RVKL_LESS EQU 060H	; Klavesnice pro RVO
RVKL_MORE EQU 067H
RVKL_F	  EQU 066H
RVKL_MODE EQU 064H
RVKL_T1	  EQU 061H
RVKL_T2	  EQU 062H
RVKL_T3	  EQU 063H
RVKL_12   EQU 065H

BRVL_YT1  EQU 0
BRVL_YT2  EQU 1
BRVL_YT3  EQU 2
BRVL_GT1  EQU 3
BRVL_GT2  EQU 4
BRVL_GT3  EQU 5
BRVL_1    EQU 6
BRVL_2    EQU 7

RSEG    TC____X

RVK_MLED:DS   4
RVK_MKBD:DS   4
RVK_MDIS:DS   4

RVK_FLG:DS    1		; priznaky klavesnice
RVK_CHG:DS    1		; detekovane zmeny oproti minulemu stavu
RVK_KEY:DS    1		; akceptovany stav klaves
RVK_PUS:DS    1		; prodleva po stisku
RVK_REP:DS    1		; prodleva pri opakovani

RVK_REPC SET  5		; Pocet taktu repeatu
RVK_PUSC SET  20	; Cekani po stisku
RVK_OFFC SET  2		; Delka uvolneni

RSEG    TC____C

RVK_INI:%LDR45i(RVK_MLED)
	%LDR67i(RVK_FLED)
	CALL  I2M_ADDF
	%LDR45i(RVK_MKBD)
	%LDR67i(RVK_FKBD)
	CALL  I2M_ADDF
	%LDR45i(RVK_MDIS)
	%LDR67i(RVK_FDIS)
	CALL  I2M_ADDF
	MOV   RV_ACT,#LOW RVO_FRSD	; Pocatecni display
	MOV   RV_ACT+1,#HIGH RVO_FRSD
	CLR   A
	MOV   DPTR,#RV_EDREP
	MOVX  @DPTR,A
	MOV   DPTR,#RVK_TIM
	MOVX  @DPTR,A
	MOV   DPTR,#RVK_KEY
	MOVX  @DPTR,A
	MOV   DPTR,#RVK_FLG
	MOVX  @DPTR,A
	MOV   A,#RVK_PUSC
	MOV   DPTR,#RVK_PUS
	MOVX  @DPTR,A
	MOV   A,#RVK_REPC
	MOV   DPTR,#RVK_REP
	MOVX  @DPTR,A
	; Konfigurace obvodu klavesnice
	CALL  IIC_WME		; cekat na ukonceni prenosu
	MOV   WR_BUF,#0FFH
	MOV   R6,#RVKBD_A
	MOV   R4,#WR_BUF
	MOV   R2,#1
	MOV   R3,#1
	CALL  IIC_RQI
	CALL  IIC_WME		; cekat na ukonceni prenosu
	RET

; Vystup LED

RVK_FLED_TC:
	CALL  G_TMC_STDP
	MOV   C,ACC.BMR_ENR
	JNB   ACC.BMR_ERR,RVK_FLED_TC1
	MOV   C,LEB_PHA
RVK_FLED_TC1:
	RET

RVK_FLED:
	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_LED
	MOVC  A,@A+DPTR		; LED z modu displaye
	MOV   B,A
	CLR   A			; konfigurece 1/2
	JB    FL_CONFK,RVK_FLED1
	MOV   A,#1 SHL BRVL_1
	JNB   FL_CONF2,RVK_FLED1
	MOV   A,#1 SHL BRVL_2
RVK_FLED1:
	ORL   B,A
    %IF(1)THEN(
	MOV   DPTR,#TMC11	; Stavove led regulatoru
	CALL  RVK_FLED_TC
	MOV   B.BRVL_GT1,C
	MOV   DPTR,#TMC2
	CALL  RVK_FLED_TC
	MOV   B.BRVL_GT2,C
	MOV   DPTR,#TMC3
	CALL  RVK_FLED_TC
	MOV   B.BRVL_GT3,C
    )FI
	MOV   A,B
	CPL   A
	MOV   WR_BUF,A
	MOV   R6,#RVLED_A
	MOV   R4,#WR_BUF
	MOV   R2,#1
	MOV   R3,#0
	JMP   IIC_RQI

)FI
%IF(%WITH_IICRVO)THEN(
; Zpracovani stisknutych klaves

RVK_FKBD:MOV  DPTR,#I2M_CB
	MOV   A,#LOW  RVK_CKBD
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH RVK_CKBD
	MOVX  @DPTR,A
	MOV   R6,#RVKBD_A
	MOV   R4,#WR_BUF
	MOV   R2,#0
	MOV   R3,#1
	JMP   IIC_RQI

RVK_CKBD:JNC  RVK_CK1
	RET
RVK_CK1:MOV   A,WR_BUF
	CPL   A
	MOV   R2,A		; Stisknute klavesy
	MOV   DPTR,#RVK_KEY
	MOVX  A,@DPTR
	XRL   A,R2
	MOV   R2,A
	MOV   DPTR,#RVK_CHG
	MOVX  A,@DPTR
	XCH   A,R2
	MOVX  @DPTR,A
	ANL   A,R2		; Zmena
	JZ    RVK_CR1
	MOV   R3,#7
RVK_CK2:RLC   A
	JC    RVK_CK3
	DJNZ  R3,RVK_CK2
RVK_CK3:MOV   DPTR,#RVK_FLG	; Test stisku klavesy
	MOVX  A,@DPTR
	MOV   R2,A
	XRL   A,R3
	ANL   A,#7
	JNZ   RVK_CK4
	MOV   DPTR,#RVK_TIM
	MOVX  A,@DPTR
	JNZ   RVK_CR6
RVK_CK4:MOV   A,R3
	ADD   A,#RVK_CKt-RVK_CKb
	MOVC  A,@A+PC
RVK_CKb:MOV   R2,A
	MOV   DPTR,#RVK_CHG	; Registrace zmeny
	MOVX  A,@DPTR
	XRL   A,R2
	MOVX  @DPTR,A
	MOV   DPTR,#RVK_KEY
	MOVX  A,@DPTR
	XRL   A,R2
	MOVX  @DPTR,A
	ANL   A,R2
	JZ    RVK_CK7
RVK_CK5:MOV   DPTR,#RVK_FLG	; Stisk klavesy
	MOV   A,R3
	ORL   A,#0C0H
	MOVX  @DPTR,A
	MOV   DPTR,#RVK_PUS
	MOVX  A,@DPTR
RVK_CK6:MOV   DPTR,#RVK_TIM	; Cas blokovani
	MOVX  @DPTR,A
	MOV   A,R3
	ADD   A,#RVKL_BASE	; Baze kodu klavesy
	MOV   R2,A
	CALL  KBDBEEP
	SJMP  RVK_CK8
RVK_CK7:MOV   DPTR,#RVK_FLG	; Uvolneni klavesy
	MOV   A,R3
	MOVX  @DPTR,A
	MOV   A,#RVK_OFFC
	MOV   DPTR,#RVK_TIM
	MOVX  @DPTR,A
	MOV   A,R3
	ADD   A,#RVKL_BASE+080H	; Baze uvolnene klavesy
	MOV   R2,A
RVK_CK8:

	MOV   R7,#ET_KEY
	MOV   A,R2
	MOV   R4,A
	MOV   R5,#0
	JMP   EV_POST

RVK_CR1:MOV   DPTR,#RVK_FLG
	MOVX  A,@DPTR
	MOV   R2,A
	JNB   ACC.7,RVK_CR9
	JNB   ACC.6,RVK_CR4
	MOV   DPTR,#RVK_TIM
	MOVX  A,@DPTR
	JNZ   RVK_CR9
	MOV   A,R2		; Repeat
	ANL   A,#07H
	MOV   R3,A
	MOV   DPTR,#RVK_REP
	MOVX  A,@DPTR
	JZ    RVK_CR9
	SJMP  RVK_CK6
RVK_CR4:MOV   A,R2
	SETB  ACC.6
	MOV   R2,A
	MOV   DPTR,#RVK_PUS
	MOVX  A,@DPTR
	SJMP  RVK_CR8
RVK_CR6:MOV   A,R2
	JNB   ACC.6,RVK_CR9
	CLR   ACC.6
	MOV   R2,A
	MOV   A,#RVK_OFFC
RVK_CR8:MOV   DPTR,#RVK_TIM
	MOVX  @DPTR,A
	MOV   A,R2
	MOV   DPTR,#RVK_FLG
	MOVX  @DPTR,A
RVK_CR9:CLR   A
	MOV   R2,A
	RET

RVK_CKt:DB    00000001B
	DB    00000010B
	DB    00000100B
	DB    00001000B
	DB    00010000B
	DB    00100000B
	DB    01000000B
	DB    10000000B

)FI
%IF(%WITH_IICRVO)THEN(
; Numericky display na IIC

ORV_LEN	SET   0
%STRUCTM(ORV,NEXT,2)	; dalsi mod displeje
%STRUCTM(ORV,ALT ,2)	; alternativni mod displeje
%STRUCTM(ORV,DFN ,2)	; zobrazovaci rutina
%STRUCTM(ORV,EFN ,2)	; editacni rutina rutina
%STRUCTM(ORV,LED ,1)	; co ukazat na LED
%STRUCTM(ORV,SFT ,2)	; ukazatel na tabulku klaves
;----------------------   shodne s UI
%STRUCTM(ORV,A_RD,2)	; rutina volana pro nacteni dat
%STRUCTM(ORV,A_WR,2)	; rutina volana pro ulozeni dat
%STRUCTM(ORV,DPSI,2)	; info pro up todate a sit
%STRUCTM(ORV,DP  ,2)	; ukazatel na menena data
;----------------------
%STRUCTM(ORV,F   ,1)	; format cisla
%STRUCTM(ORV,L   ,2)	; spodni limit
%STRUCTM(ORV,H   ,2)	; horni limit

RVK_UNSTC EQU 25*2	; doba pocatecniho zobrazeni unst rezimu
RVK_UNSTE EQU 25*5	; doba pridrzeni pri editaci

RSEG    TC____D

RV_ACT:	DS    2		; Aktualni popis pro zobrazeni

RSEG    TC____X

RV_EDREP:DS   1		; Opakovani klavesy => zvetsit zmenu

RSEG    TC____C

; Nacteni dat pres ORV_A_RD
RV_RD:	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_A_RD
	MOVC  A,@A+DPTR
	PUSH  ACC
	MOV   A,#ORV_A_RD+1
RV_RD1:	MOVC  A,@A+DPTR
	PUSH  ACC
	MOV   R1,#0
	MOV   A,DPL
	ADD   A,#LOW  (ORV_A_RD-OU_A_RD)
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#HIGH (ORV_A_RD-OU_A_RD)
	MOV   DPH,A
	CLR   F0
	RET

; Ulozeni dat pres ORV_A_WR
RV_WR:	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_A_WR
	MOVC  A,@A+DPTR
	PUSH  ACC
	MOV   A,#ORV_A_WR+1
	SJMP  RV_RD1

)FI
%IF(%WITH_IICRVO)THEN(
; Periodicke zobrazovani
RVK_FDIS:
	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_DFN
	MOVC  A,@A+DPTR
	PUSH  ACC
	MOV   A,#ORV_DFN+1
	MOVC  A,@A+DPTR
	PUSH  ACC
	RET

; Casovane zobrazeni cisla integer
RV_DFNiU:
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#RVK_TUNST
	MOVX  A,@DPTR
	POP   DPH
	POP   DPL
	JZ    RVK_ALT
; Zobrazeni cisla integer
RV_DFNi:CALL  RV_RD	; Nacteni dat, pri chybe F0
	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_F
	MOVC  A,@A+DPTR
	MOV   R7,A	; Format, napr 0C4H
	MOV   R6,#10H
	CALL  iPRTLi
	MOV   R6,#10H
	JMP   OUT_BUF

; Nasledujici polozka
RVK_NEXT:
	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_NEXT	; nasledujici polozka
	MOVC  A,@A+DPTR
	MOV   RV_ACT,A
	MOV   A,#ORV_NEXT+1
	MOVC  A,@A+DPTR
	MOV   RV_ACT+1,A
RVK_NEXT1:
	MOV   A,#RVK_UNSTC
RVK_NEXT2:
	MOV   DPTR,#RVK_TUNST	; pro nestabilni rezimy
	MOVX  @DPTR,A
	CLR   A
	MOV   DPTR,#RV_EDREP	; nulovat pocitadlo opakovani
	MOVX  @DPTR,A
RVK_NEXT9:RET

; Alternativni nasledujici polozka
RVK_ALT:MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_ALT	; alternativni nasledujici polozka
	MOVC  A,@A+DPTR
	MOV   RV_ACT,A
	MOV   A,#ORV_ALT+1
	MOVC  A,@A+DPTR
	MOV   RV_ACT+1,A
	MOV   A,#RVK_UNSTE
	SJMP  RVK_NEXT2

; Prechod na ALT display pri editaci
RVK_EFNALT:
	CALL  RVK_ALT
; Editace
RVK_EDIT:
	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_EFN
	MOVC  A,@A+DPTR
	PUSH  ACC
	MOV   A,#ORV_EFN+1
	MOVC  A,@A+DPTR
	PUSH  ACC
	RET

)FI
%IF(%WITH_IICRVO)THEN(

; Editace cisla integer v docasnem rezimu
RV_EFNiU:
	PUSH  DPL
	PUSH  DPH
	MOV   A,#RVK_UNSTE	; podrzeni rezimu
	MOV   DPTR,#RVK_TUNST
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
; Editace cisla integer
RV_EFNi:
	MOV   A,R2
	PUSH  ACC
	CALL  RV_RD	; Nacteni dat, pri chybe F0
	JNB   F0,RV_Ei10
	CLR   A
	MOV   R4,A
	MOV   R5,A
RV_Ei10:POP  ACC
	CJNE  A,#RVKL_MORE,RV_Ei20
	CALL  RV_EPROF
	CALL  ADDi
	MOV   A,#ORV_H
	CALL  RV_ETSTL
	MOV   C,OV
	XRL   A,PSW
	JB    ACC.7,RV_Ei60
	SJMP  RV_Ei50
RV_Ei20:CJNE  A,#RVKL_LESS,RV_Ei80
	CALL  RV_EPROF
	CALL  SUBi
	MOV   A,#ORV_L
	CALL  RV_ETSTL
	MOV   C,OV
	XRL   A,PSW
	JNB   ACC.7,RV_Ei60
RV_Ei50:MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
RV_Ei60:JMP   RV_WR	; Ulozeni dat
RV_Ei80:MOV   DPTR,#RV_EDREP
	CLR   A
	MOVX  @DPTR,A
	RET

; Otestuje R45 proti limitu v ORV_L nebo ORV_H
RV_ETSTL:MOV  DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   R2,A
	MOVC  A,@A+DPTR
	XCH   A,R2
	INC   A
	MOVC  A,@A+DPTR
	MOV   R3,A
	JMP   CMPi

; Profil zrychlovani editace
RV_EPROF:MOV  DPTR,#RV_EDREP
	MOVX  A,@DPTR
	INC   A
	CJNE  A,#8*6,RV_EPR1
	DEC   A
RV_EPR1:MOVX  @DPTR,A
	ANL   A,#NOT 7
	RR    A
	RR    A
	MOV   R2,A
	ADD   A,#RV_EPRt-RV_EPRb1
	MOVC  A,@A+PC
RV_EPRb1:XCH  A,R2
	ADD   A,#RV_EPRt-RV_EPRb2+1
	MOVC  A,@A+PC
RV_EPRb2:MOV  R3,A
	RET

RV_EPRt:%W    (1)
	%W    (10)
	%W    (20)
	%W    (50)
	%W    (100)
	%W    (200)

; Stisknuti klavesy RVKL_12
RV_CNFP:JB    FL_CONFK,RV_CF20
	SETB  FL_CONFK
	MOV   A,#RVK_UNSTE
RV_CF18:MOV   DPTR,#RVK_TIM
	MOVX  @DPTR,A
	RET
; Ulozit aktualni konfiguraci
RV_CF20:CLR   FL_CONFK
	MOV   A,#255
	CALL  RV_CF18
	%LDR23i(EEC_CF1)
	JNB   FL_CONF2,RV_CF21
	%LDR23i(EEC_CF2)
RV_CF21:JMP   EEP_WRS

; Uvolneni klavesy RVKL_12
RV_CNFR:JBC   FL_CONFK,RV_CF40
	RET
RV_CF40:CALL  TMC_OFF
	JNB   FL_CONF2,RV_CF42
RV_CF41:CLR   FL_CONF2
	%LDR23i(EEC_CF1)
	JMP   EEP_RDS
RV_CF42:SETB  FL_CONF2
	%LDR23i(EEC_CF2)
	JMP   EEP_RDS
)FI
%IF(%WITH_IICRVO)THEN(

RVO_100:DS    RVO_100+ORV_NEXT-$
	%W    (RVO_115)		; dalsi mod displeje
	DS    RVO_100+ORV_ALT-$
	%W    (0)		; alternativni mod
	DS    RVO_100+ORV_DFN-$
	%W    (RV_DFNi)		; zobrazovaci rutina
	DS    RVO_100+ORV_EFN-$
	%W    (RV_EFNi)		; editacni rutina rutina
	DS    RVO_100+ORV_LED-$
	DB    0			; co ukazat na LED
	DS    RVO_100+ORV_SFT-$
	%W    (0)		; ukazatel na tabulku klaves
	DS    RVO_100+ORV_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    RVO_100+ORV_DPSI-$
	%W    (0)		; DPSI
	%W    (STATUS)		; DP
	DS    RVO_100+ORV_F-$
	DB     0C0H		; I_F
	%W    (-200)		; I_L
	%W    ( 200)		; I_H

RVO_FRSD:			; Prvni display pro TC5100
; ----- cteni teploty TMC11/TMC12
RVO_110:DS    RVO_110+ORV_NEXT-$
	%W    (RVO_125)		; dalsi mod displeje
	DS    RVO_110+ORV_ALT-$
	%W    (RVO_115)		; alternativni mod
	DS    RVO_110+ORV_DFN-$
	%W    (RV_DFNi)		; zobrazovaci rutina
	DS    RVO_110+ORV_EFN-$
	%W    (RVK_EFNALT)	; editacni rutina rutina
	DS    RVO_110+ORV_LED-$
	DB    1 SHL BRVL_YT1	; co ukazat na LED
	DS    RVO_110+ORV_SFT-$
	%W    (0)		; ukazatel na tabulku klaves
	DS    RVO_110+ORV_A_RD-$
	%W    (UR_TEMP_BOTH)	; A_RD
	%W    (UW_TEMP_BOTH)	; A_WR
	DS    RVO_110+ORV_DPSI-$
	%W    (TMC11+OTMC_RT)	; DPSI
	%W    (TMC11+OTMC_AT)	; DP
	DS    RVO_110+ORV_F-$
	DB    TEMP_I_F		; I_F
	%W    (0)		; I_L
	%W    (%CTRQ_MAX)	; I_H

; ----- nastaveni teploty TMC11/TMC12
RVO_115:DS    RVO_115+ORV_NEXT-$
	%W    (RVO_125)		; dalsi mod displeje
	DS    RVO_115+ORV_ALT-$
	%W    (RVO_110)		; alternativni mod
	DS    RVO_115+ORV_DFN-$
	%W    (RV_DFNiU)	; zobrazovaci rutina
	DS    RVO_115+ORV_EFN-$
	%W    (RV_EFNiU)	; editacni rutina rutina
	DS    RVO_115+ORV_LED-$
	DB    1 SHL BRVL_YT1	; co ukazat na LED
	DS    RVO_115+ORV_SFT-$
	%W    (0)		; ukazatel na tabulku klaves
	DS    RVO_115+ORV_A_RD-$
	%W    (UR_TEMP)		; A_RD
	%W    (UW_TEMP_BOTH)	; A_WR
	DS    RVO_115+ORV_DPSI-$
	%W    (TMC11+OTMC_RT)	; DPSI
	%W    (TMC11+OTMC_RT)	; DP
	DS    RVO_115+ORV_F-$
	DB    TEMP_I_F		; I_F
	%W    (0)		; I_L
	%W    (%CTRQ_MAX)	; I_H
)FI
%IF(%WITH_IICRVO)THEN(
; ----- cteni teploty TMC2
RVO_120:DS    RVO_120+ORV_NEXT-$
	%W    (RVO_135)		; dalsi mod displeje
	DS    RVO_120+ORV_ALT-$
	%W    (RVO_125)		; alternativni mod
	DS    RVO_120+ORV_DFN-$
	%W    (RV_DFNi)		; zobrazovaci rutina
	DS    RVO_120+ORV_EFN-$
	%W    (RVK_EFNALT)	; editacni rutina rutina
	DS    RVO_120+ORV_LED-$
	DB    1 SHL BRVL_YT2	; co ukazat na LED
	DS    RVO_120+ORV_SFT-$
	%W    (0)		; ukazatel na tabulku klaves
	DS    RVO_120+ORV_A_RD-$
	%W    (UR_TEMP)		; A_RD
	%W    (UW_TEMP)		; A_WR
	DS    RVO_120+ORV_DPSI-$
	%W    (TMC2+OTMC_RT)	; DPSI
	%W    (TMC2+OTMC_AT)	; DP
	DS    RVO_120+ORV_F-$
	DB    TEMP_I_F		; I_F
	%W    (0)		; I_L
	%W    (%CTRQ_MAX)	; I_H

; ----- nastaveni teploty TMC2
RVO_125:DS    RVO_125+ORV_NEXT-$
	%W    (RVO_135)		; dalsi mod displeje
	DS    RVO_125+ORV_ALT-$
	%W    (RVO_120)		; alternativni mod
	DS    RVO_125+ORV_DFN-$
	%W    (RV_DFNiU)	; zobrazovaci rutina
	DS    RVO_125+ORV_EFN-$
	%W    (RV_EFNiU)	; editacni rutina rutina
	DS    RVO_125+ORV_LED-$
	DB    1 SHL BRVL_YT2	; co ukazat na LED
	DS    RVO_125+ORV_SFT-$
	%W    (0)		; ukazatel na tabulku klaves
	DS    RVO_125+ORV_A_RD-$
	%W    (UR_TEMP)		; A_RD
	%W    (UW_TEMP)		; A_WR
	DS    RVO_125+ORV_DPSI-$
	%W    (TMC2+OTMC_RT)	; DPSI
	%W    (TMC2+OTMC_RT)	; DP
	DS    RVO_125+ORV_F-$
	DB    TEMP_I_F		; I_F
	%W    (0)		; I_L
	%W    (%CTRQ_MAX)	; I_H
)FI
%IF(%WITH_IICRVO)THEN(
; ----- cteni teploty TMC3
RVO_130:DS    RVO_130+ORV_NEXT-$
	%W    (RVO_100)		; dalsi mod displeje
	DS    RVO_130+ORV_ALT-$
	%W    (RVO_135)		; alternativni mod
	DS    RVO_130+ORV_DFN-$
	%W    (RV_DFNi)		; zobrazovaci rutina
	DS    RVO_130+ORV_EFN-$
	%W    (RVK_EFNALT)	; editacni rutina rutina
	DS    RVO_130+ORV_LED-$
	DB    1 SHL BRVL_YT3	; co ukazat na LED
	DS    RVO_130+ORV_SFT-$
	%W    (0)		; ukazatel na tabulku klaves
	DS    RVO_130+ORV_A_RD-$
	%W    (UR_TEMP)		; A_RD
	%W    (UW_TEMP)		; A_WR
	DS    RVO_130+ORV_DPSI-$
	%W    (TMC3+OTMC_RT)	; DPSI
	%W    (TMC3+OTMC_AT)	; DP
	DS    RVO_130+ORV_F-$
	DB    TEMP_I_F		; I_F
	%W    (0)		; I_L
	%W    (%CTRQ_MAX)	; I_H

; ----- nastaveni teploty TMC3
RVO_135:DS    RVO_135+ORV_NEXT-$
	%W    (RVO_100)		; dalsi mod displeje
	DS    RVO_135+ORV_ALT-$
	%W    (RVO_130)		; alternativni mod
	DS    RVO_135+ORV_DFN-$
	%W    (RV_DFNiU)	; zobrazovaci rutina
	DS    RVO_135+ORV_EFN-$
	%W    (RV_EFNiU)	; editacni rutina rutina
	DS    RVO_135+ORV_LED-$
	DB    1 SHL BRVL_YT3	; co ukazat na LED
	DS    RVO_135+ORV_SFT-$
	%W    (0)		; ukazatel na tabulku klaves
	DS    RVO_135+ORV_A_RD-$
	%W    (UR_TEMP)		; A_RD
	%W    (UW_TEMP)		; A_WR
	DS    RVO_135+ORV_DPSI-$
	%W    (TMC3+OTMC_RT)	; DPSI
	%W    (TMC3+OTMC_RT)	; DP
	DS    RVO_135+ORV_F-$
	DB    TEMP_I_F		; I_F
	%W    (0)		; I_L
	%W    (%CTRQ_MAX)	; I_H
)FI

; *******************************************************************
; Prace s pameti EEPROM 8582

%IF(%WITH_IIC)THEN(

EEP_ADR	EQU   0A0H	; Adresa EEPROM na IIC sbernici
C_S1CON EQU   11000000B ; 11000001B ; Pocatecni stav S1CON

EEA_RD	EQU   1		; akce cteni
EEA_WR	EQU   2		; akce zapisu

RSEG	TC____D

RSEG	TC____X

EE_CNT:	DS    1		; citac byte pro prenos z/do EEPROM
EE_PTR:	DS    2		; ukazatel do EE_MEM na data
EEP_TAB:DS    2		; popis dat ulozenych v bloku EEPROM

EE_MEM:	DS    100H	; Buffer pameti EEPROM

RSEG	TC____C

XOR_SUM:CLR   A
XOR_SU1:MOV   R3,A
	MOVX  A,@DPTR
	INC   DPTR
	XRL   A,R3
	INC   A
	DJNZ  R2,XOR_SU1
	RET

; Cteni pameti EEPROM
;	DPTR .. pointer na buffer
;	R2   .. pocet ctenych byte
;	R4   .. adresa, od ktere se cte

EE_RD:	MOV   A,DPL
	MOV   R0,DPH
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A		; cilova adresa
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	MOV   DPTR,#IIC_BUF
	MOV   A,R4		; adresa v pameti EEPROM
	MOVX  @DPTR,A
	MOV   DPTR,#EE_CNT
	MOV   A,R2		; pocet prenasenych byte
	MOVX  @DPTR,A
	CALL  IIC_WME		; cekat na ukonceni prenosu
EE_RD1:	%LDR45i(IIC_BUF)
	MOV   R2,#1
	MOV   R3,#010H
	MOV   R6,#EEP_ADR
	CALL  IIC_RQX		; pozadavek na IIC
	JNZ   EE_RD1
	CALL  IIC_WME		; cekat na ukonceni prenosu
	JNZ   EE_RDR
	MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR		; cilova adresa
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	%LDR23i(IIC_BUF+1)
	%LDR01i(10H)
	CALL  xxMOVE
	MOV   DPTR,#EE_PTR
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   DPTR,#IIC_BUF
	MOVX  A,@DPTR		; nova adresa v EEPROM
	ADD   A,#10H
	MOVX  @DPTR,A
	MOV   DPTR,#EE_CNT
	MOVX  A,@DPTR
	ADD   A,#-10H-1
	INC   A
	MOVX  @DPTR,A
	JC    EE_RD1
	CLR   A
EE_RDR:	RET

; Zapis do pameti EEPROM
;	DPTR .. pointer na buffer
;	R2   .. pocet zapisovanych byte
;	R4   .. adresa, od ktere se zapisuje

EE_WRAO EQU   4

EE_WR:	MOV   A,DPL
	MOV   R0,DPH
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A		; zdrojova adresa
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	MOV   DPTR,#IIC_BUF
	MOV   A,R4		; adresa v pameti EEPROM
	MOVX  @DPTR,A
	MOV   DPTR,#EE_CNT
	MOV   A,R2		; pocet prenasenych byte
	MOVX  @DPTR,A
	CALL  IIC_WME		; cekat na ukonceni prenosu
EE_WR1:	MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR		; zdrojova adresa
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	%LDR45i(IIC_BUF+1)
	%LDR01i(EE_WRAO)
	CALL  xxMOVE
	MOV   DPTR,#EE_PTR
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
EE_WR2:	%LDR45i(IIC_BUF)
	MOV   R2,#EE_WRAO+1
	MOV   R3,#0
	MOV   R6,#EEP_ADR
	CALL  IIC_RQX
	JNZ   EE_WR2
	CALL  IIC_WME
	JNZ   EE_WR2
	MOV   DPTR,#IIC_BUF
	MOVX  A,@DPTR		; adresa v EEPROM
	ADD   A,#EE_WRAO
	MOVX  @DPTR,A
	MOV   DPTR,#EE_CNT
	MOVX  A,@DPTR		; pocitani prenasenych byte
	ADD   A,#-EE_WRAO-1
	INC   A
	MOVX  @DPTR,A
	JC    EE_WR1
EE_WRR:	RET

)FI

%IF(%WITH_IIC)THEN(

; R45 := [EE_PTR++]
EEA_RDi:MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOV   R0,DPH
	MOV   A,DPL
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	RET

; [EE_PTR++] := R45
EEA_WRi:MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   R0,DPH
	MOV   A,DPL
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	RET

; provadi EEA_RD, EEA_WR pro iteger cislo
EEA_Mi:	CJNE  R0,#EEA_RD,EEA_Mi5
	CALL  EEA_RDi
	MOV   DPL,R2
	MOV   DPH,R3
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	RET
EEA_Mi5:CJNE  R0,#EEA_WR,EEA_Mi9
	MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  EEA_WRi
EEA_Mi9:RET

EEA_PRO:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#EE_PTR
	MOV   A,#LOW EE_MEM	; Pocatek dat pro EEPROM do EE_MEM
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH EE_MEM
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
EEA_PR2:MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A		; R23 parametr pro rutinu
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	ORL   A,R4		; R45 volana funkce
	JZ    EEA_Mi9
	PUSH  DPL
	PUSH  DPH
	MOV   A,R0
	PUSH  ACC
	CALL  JMPR45
	POP   ACC
	MOV   R0,A
	POP   DPH
	POP   DPL
	JMP   EEA_PR2

JMPR45:	MOV   A,R4
	PUSH  ACC
	MOV   A,R5
	PUSH  ACC
	RET

; Nastavi EEP_TAB na R23
EEP_PTS:MOV   DPTR,#EEP_TAB
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	RET
)FI
%IF(%WITH_IIC)THEN(

; Nastavi
; R2=[EEP_TAB]   .. delka prenasenych dat
; R4=[EEP_TAB+1] .. pocatecni adresa EEPROM
; DPTR=EE_MEM    .. adresa bufferu pameti
EEP_GM:	MOV   DPTR,#EEP_TAB
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R2
	MOV   DPH,A
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   DPTR,#EE_MEM
	RET

; Ulozit data podle tabulky R23
EEP_WRS:CALL  EEP_PTS
%IF(1)THEN(
EEP_WR1:CALL  I2M_TBF		; Uvolneni po I2M_POOL
	JNZ   EEP_WR1
)FI
	MOV   DPTR,#EEP_TAB
	CALL  xMDPDP
	INC   DPTR
	INC   DPTR	; popis akci
	MOV   R0,#EEA_WR
	CALL  EEA_PRO
	CALL  EEP_GM
	DEC   R2
	CALL  XOR_SUM
	MOVX  @DPTR,A		; kontrolni byte
	CALL  EEP_GM
	JMP   EE_WR

; Nacist data podle tabulky R23
EEP_RDS:CALL  EEP_PTS
%IF(1)THEN(
EEP_RD1:CALL  I2M_TBF		; Uvolneni po I2M_POOL
	JNZ   EEP_RD1
)FI
	CALL  EEP_GM
	CALL  EE_RD
	JNZ   EEP_RD9
	CALL  EEP_GM
	DEC   R2
	CALL  XOR_SUM
	MOV   R3,A
	MOVX  A,@DPTR		; kontrolni byte
	XRL   A,R3
	JNZ   EEP_RD9
	MOV   DPTR,#EEP_TAB
	CALL  xMDPDP
	INC   DPTR
	INC   DPTR	; popis akci
	MOV   R0,#EEA_RD
	JMP   EEA_PRO
EEP_RD9:SETB  F0
	RET

)FI

; *******************************************************************
; Ukladani a cteni nastaveni z EEPROM

; Tabulka ukladanych parametru
EEC_SER:DB    040H	; pocet byte - musi by delitelny 16
	DB    010H	; pocatecni adresa v EEPROM
    %IF(0)THEN(
	%W    (COM_TYP)
	%W    (EEA_Mi)

	%W    (COM_ADR)
	%W    (EEA_Mi)

	%W    (COM_SPD)
	%W    (EEA_Mi)
    )ELSE(
	%W    (0)
	%W    (EEA_Mi)

	%W    (COM_ADR)
	%W    (EEA_Mi)

	%W    (0)
	%W    (EEA_Mi)
    )FI

	%W    (TMC11+OTMC_OC)	; offset teploty TMC11
	%W    (EEA_Mi)

	%W    (TMC11+OTMC_MC)	; sklon teploty TMC11
	%W    (EEA_Mi)

	%W    (TMC11+OMR_P)	; teplota reg P
	%W    (EEA_Mi)

	%W    (TMC11+OMR_I)	; teplota reg I
	%W    (EEA_Mi)

	%W    (TMC11+OMR_D)	; teplota reg D
	%W    (EEA_Mi)

	%W    (TMC12+OTMC_OC)	; offset teploty TMC12
	%W    (EEA_Mi)

	%W    (TMC12+OTMC_MC)	; sklon teploty TMC12
	%W    (EEA_Mi)

	%W    (TMC12+OMR_P)	; teplota reg P
	%W    (EEA_Mi)

	%W    (TMC12+OMR_I)	; teplota reg I
	%W    (EEA_Mi)

	%W    (TMC12+OMR_D)	; teplota reg D
	%W    (EEA_Mi)

	%W    (TMC2+OTMC_OC)	; offset teploty TMC2
	%W    (EEA_Mi)

	%W    (TMC2+OTMC_MC)	; sklon teploty TMC2
	%W    (EEA_Mi)

	%W    (TMC2+OMR_P)	; teplota reg P
	%W    (EEA_Mi)

	%W    (TMC2+OMR_I)	; teplota reg I
	%W    (EEA_Mi)

	%W    (TMC2+OMR_D)	; teplota reg D
	%W    (EEA_Mi)

	%W    (TMC3+OTMC_OC)	; offset teploty TMC3
	%W    (EEA_Mi)

	%W    (TMC3+OTMC_MC)	; sklon teploty TMC3
	%W    (EEA_Mi)

	%W    (TMC3+OMR_P)	; teplota reg P
	%W    (EEA_Mi)

	%W    (TMC3+OMR_I)	; teplota reg I
	%W    (EEA_Mi)

	%W    (TMC3+OMR_D)	; teplota reg D
	%W    (EEA_Mi)

	%W    (0)
	%W    (0)

EEC_CF1:DB    010H	; pocet byte - musi by delitelny 16
	DB    080H	; pocatecni adresa v EEPROM

	%W    (TMC11+OTMC_RT)		; teplota 11
	%W    (EEA_Mi)

	%W    (TMC12+OTMC_RT)		; teplota 12
	%W    (EEA_Mi)

	%W    (TMC2+OTMC_RT)		; teplota 2
	%W    (EEA_Mi)

	%W    (TMC3+OTMC_RT)		; teplota 3
	%W    (EEA_Mi)

	%W    (0)
	%W    (0)

EEC_CF2:DB    010H	; pocet byte - musi by delitelny 16
	DB    0A0H	; pocatecni adresa v EEPROM

	%W    (TMC11+OTMC_RT)		; teplota 11
	%W    (EEA_Mi)

	%W    (TMC12+OTMC_RT)		; teplota 12
	%W    (EEA_Mi)

	%W    (TMC2+OTMC_RT)		; teplota 2
	%W    (EEA_Mi)

	%W    (TMC3+OTMC_RT)		; teplota 3
	%W    (EEA_Mi)

	%W    (0)
	%W    (0)

MR_EERD:%LDR23i(EEC_SER)
	JMP   EEP_RDS

MR_EEWR:%LDR23i(EEC_SER)
	JMP   EEP_WRS

; *******************************************************************
; Promenne

RSEG	TC____X

STATUS:	DS    2

; *******************************************************************
; Globalni udalosti

RSEG	TC____C

; Posle globalni udalost
GLOB_RQ23:MOV A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	MOV   R7,#ET_GLOB
	JMP   EV_POST

; Zpracovani globalnich udalosti
GLOB_DO:MOV   A,R4
	MOV   R7,A
	MOV   A,R5
	MOV   R4,A
	MOV   DPTR,#GLOB_SF1
	JMP   SEL_FNC

; Globalni udalosti

; Tabulka globalnich udalosti

GLOB_SF1:

	DB    0

; *******************************************************************
; Komunikace s uzivatelem

RSEG	TC____X

UT_UIAD:DS    40
UT_DATA:DS    40

RSEG	TC____C

UT_INIT:CLR   D4LINE
	SETB  FL_CMAV
	MOV   DPTR,#UI_MV_SX
	MOV   A,#20
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#2
	MOVX  @DPTR,A
	MOV   DPTR,#UT_UIAD
	MOV   UI_AD,DPL
	MOV   UI_AD+1,DPH
	CLR   A
	MOV   DPTR,#EV_BUF
	MOVX  @DPTR,A
	MOV   DPTR,#GR_ACT
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#REF_TIM
	MOVX  @DPTR,A
%IF(%WITH_IICKB)THEN(
	MOV   A,#25
	CALL  WAIT_T
	MOV   R6,#07CH	; Adresa vetsi IIC klavesnice
	CALL  UI_INIHW	; Inicializace hardware
	JB    FL_IICKB,UT_INI5

	MOV   R6,#07AH	; Adresa IIC klavesnice
	CALL  UI_INIHW	; Inicializace hardware
	JNB   FL_IICKB,UT_INI6

UT_INI5:
	MOV   DPTR,#DEVER_IT	; text
	MOV   R6,#1		; 1. radka
	MOV   R7,#DEVER_IE-DEVER_IT ; pocet znaku
	CALL  KB_IICWRLN
	MOV   A,#50
	CALL  WAIT_T

UT_INI6:
)FI
	RET

%IF(%WITH_IICKB)THEN(
DEVER_IT:DB   LCD_CLR,'%VERSION'
	DB    C_LIN2 ,'PiKRON'
DEVER_IE:
)FI

UT_TREF:MOV   DPTR,#REF_TIM
	MOVX  A,@DPTR
	JNZ   UT_TRE1
	MOV   DPTR,#REF_PER
	MOVX  A,@DPTR
	MOV   DPTR,#REF_TIM
	MOVX  @DPTR,A
	MOV   A,#1
	RET
UT_TRE1:CLR   A
	RET

UT:     CALL  UT_INIT
	MOV   R7,#ET_RQGR
	%LDR45i(UT_GR10)
	CALL  EV_POST

UT_ML:  CALL  EV_GET
	JZ    UT_ML50
	CJNE  R7,#ET_GLOB,UT_ML45
	CALL  GLOB_DO
	SJMP  UT_ML
UT_ML45:CALL  EV_DO
	JMP   UT_ML
UT_ML50:JNB   FL_ECRS,UT_ML53
   %IF(%WITH_RS232)THEN(
	CALL  RS_POOL		; Smycka zpracovani prikazu RS232
	JNZ   UT_ML55
   )FI
UT_ML53:
    %IF(%WITH_ULAN)THEN(
	JNB   FL_ECUL,UT_ML55
	CALL  UD_OI
	JB    uLF_INE,UT_ML55
	JB    UDF_RDP,UT_ML57
    )FI
UT_ML55:CALL  UT_TREF
	JZ    UT_ML60
    %IF(%WITH_ULAN)THEN(
	JNB   FL_ECUL,UT_ML57
	CALL  UD_REFR
UT_ML57:CLR   UDF_RDP
    )FI
	SETB  FL_REFR
	SJMP  UT_ML65
UT_ML60:
	CALL  UT_ULED
    %IF(%WITH_IIC)THEN(
	CALL  I2M_POOL
    )FI
UT_ML65:JMP   UT_ML

UT_ULED:
;       MOV   DPTR,#IP1_ST+1
;	MOV   R2,#1 SHL LFB_PUMP1
;	CALL  UT_USTS	; CALL  UT_UST1

	JMP   LEDWR

UT_USTS:MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
UT_UST1:MOV   A,R5
	JB    ACC.7,UT_UST7
	ORL   A,R4
UT_UST2:JZ    UT_UST5
UT_UST3:MOV   A,R2		; On
	CPL   A
	ANL   LEB_FLG,A
	CPL   A
	ORL   LED_FLG,A
	RET
UT_UST5:MOV   A,R2		; Off
	CPL   A
	ANL   LEB_FLG,A
	ANL   LED_FLG,A
	RET
UT_UST7:MOV   A,R2		; Error
	ORL   LEB_FLG,A
	RET

TST_IICKL:
	MOV   DPTR,#STATUS
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	RET

; ---------------------------------
; Definice klaves

UT_SF1:

UT_SFTN:DB    K_RIGHT
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    K_LEFT
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    K_DOWN
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    K_UP
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

%IF(%WITH_IICRVO)THEN(
RVO_SFT1:
	DB    RVKL_MORE
	DB    RVKL_MORE,0
	%W    (RVK_EDIT)

	DB    RVKL_LESS
	DB    RVKL_LESS,0
	%W    (RVK_EDIT)

	DB    RVKL_MORE OR 80H
	DB    0,0
	%W    (RVK_EDIT)

	DB    RVKL_LESS OR 80H
	DB    0,0
	%W    (RVK_EDIT)

	DB    RVKL_MODE
	%W    (0)
	%W    (RVK_NEXT)

	DB    RVKL_T1
	%W    (0)
	%W    (TMC_ONOFF)

	DB    RVKL_T2
	%W    (TMC2)
	%W    (TMC_ONOFF23)

	DB    RVKL_T3
	%W    (TMC3)
	%W    (TMC_ONOFF23)
)FI
%IF(%WITH_IICKB)THEN(
	DB    043H
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    041H
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    042H
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    046H
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    045H
	DB    0,0
	%W    (TST_IICKL)

	DB    047H
	DB    ET_KEY,K_ENTER
	%W    (EV_PO23)

	DB    RVKL_12
	%W    (0)
	%W    (RV_CNFP)

	DB    RVKL_12 OR 80H
	%W    (0)
	%W    (RV_CNFR)

)FI

UT_SF0:	DB    0


; ---------------------------------
; Zakladni display
UT_GR10:DS    UT_GR10+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR10+OGR_BTXT-$
	%W    (UT_GT10)
	DS    UT_GR10+OGR_STXT-$
	%W    (0)
	DS    UT_GR10+OGR_HLP-$
	%W    (0)
	DS    UT_GR10+OGR_SFT-$
	%W    (UT_SF10)
	DS    UT_GR10+OGR_PU-$
	%W    (UT_U1001)
	%W    (UT_U1002)
	%W    (0)

UT_GT10:DB    'STAT',C_NL
	DB    'VAL',0

UT_SF10:
	DB    -1
	%W    (UT_SF1)

UT_U1001: ; Rychlost
	DS    UT_U1001+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1001+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1001+OU_X-$
	DB    6,0,6,1
	DS    UT_U1001+OU_HLP-$
	%W    (0)
	DS    UT_U1001+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1001+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1001+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (STATUS)		; DP
	DS    UT_U1001+OU_I_F-$
	DB    80H		; format I_F
	%W    (-200)		; I_L
	%W    (200)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1002: ; Rychlost
	DS    UT_U1002+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1002+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1002+OU_X-$
	DB    6,1,6,1
	DS    UT_U1002+OU_HLP-$
	%W    (0)
	DS    UT_U1002+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1002+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1002+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (STATUS)		; DP
	DS    UT_U1002+OU_I_F-$
	DB    80H		; format I_F
	%W    (-200)		; I_L
	%W    (200)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

	END
