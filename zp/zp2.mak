#   Project file pro Zapalovani ZAP1
#         (C) Pisoft 1995

zp.obj    : zp.asm pb_ai.h ulan.h pb_iic.h pb_move.h
	a51 zp.asm $(par) debug

zp_ai.obj : zp_ai.asm pb_tty.h
	a51 zp_ai.asm  $(par)

pb_move.obj : pb_move.asm
	a51 pb_move.asm  $(par)

ii_iic.obj: ii_iic.asm
        a51 ii_iic.asm

          : zp.obj
	del zp.

zp.       : zp.obj zp_ai.obj pb_move.obj ii_plan.obj ii_iic.obj
	l51 zp.obj,zp_ai.obj,pb_move.obj,ii_plan.obj,ii_iic.obj xdata(8800H) ramsize(100h) stack(STACK) ixref

zp2.hex   : zp.
	del  zp2.
	ren zp. zp2.
	ohs51 zp2
	del zp2.

