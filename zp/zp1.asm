$NOMOD51
;********************************************************************
;*                    ZAP1  -   ZP.ASM                              *
;*                       Rizeni zapalovani                          *
;*                  Stav ke dni 26.11.1995                          *
;*                      (C) Pisoft 1995                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

%DEFINE (FINAL) (0)

$INCLUDE(REG552.H)
$INCLUDE(PB_AI.H)
$INCLUDE(PB_IIC.H)
$INCLUDE(PB_MOVE.H)

%IF (%FINAL) THEN (
$INCLUDE(II_PLAN.H)
)ELSE(
$INCLUDE(ULAN.H)
EXTRN   CODE(VEC_SET,VEC_GET,VEC_USR)
)FI

%IF (NOT %FINAL) THEN (
PUBLIC  KBDTIMR,TIMR_WAIT
)FI
PUBLIC  RES_STAR
PUBLIC  PRINTc,INPUTc

; Blokovani paleni na    P4.4  CMSR4
; Signal k paleni na     P4.3  CMSR3
; Cislo valce na P4.0 az P4.2
; Senzor polohy 	 P1.0  CT0
; Senzor zubu	 P3.4 a  P1.3  CT3

%IF (0) THEN (
; Favorit
; Celkem 124 zubu
; 2 paleni za otocku
; Posun znacky o 18 stupnu to je 6 zubu
%DEFINE (MOTOR_TYPE) (Favorit)
C_TAB_ADR	EQU   04000H;1800H;04000H; Pocatek tabulky predstihu
C_X_FREQ	EQU   11059	; Freq krystalu v kHz
C_N_ROT		EQU   124	; Pocet zubu
C_N_FLS		EQU   2		; Pocet paleni na otocku
C_NT_RPM	EQU   40H	; Pocet polozek v tabulce pro otacky
C_NT_PRE	EQU   20H	; Pocet polozek v tabulce pro tlak
C_NT_RPMS	EQU   728	; 2^16/rozliseni otacek
C_NT_PREM	EQU   2000H	; Nasobitel tlaku / 2^16
C_NT_PREO	EQU   0		; Posun tlaku
C_N_ROTDS	EQU   1		; Deleni zubu na pocet bitu
C_N_SHIFT	EQU   400H	; Posun predstihu
C_N_HISTR	EQU   500	; RPM pri kterem se meni hystereze RIL
C_N_HISTL	EQU   20H	; Hystereze pro male otacky
C_N_HISTH	EQU   40H	; Hystereze pro velke otacky
)FI

%IF (1) THEN (
; Avia 1B
; Celkem 120 zubu
; 2 paleni za otocku
; Posun znacky o 10 stupnu to je 3 zuby
; Rozliseni po 46.875 otackach
%DEFINE (MOTOR_TYPE) (Avia 1B)
C_TAB_ADR	EQU   04000H;1800H;04000H; Pocatek tabulky predstihu
C_X_FREQ	EQU   11059	; Freq krystalu v kHz
C_N_ROT		EQU   120	; Pocet zubu
C_N_FLS		EQU   2		; Pocet paleni na otocku
C_NT_RPM	EQU   40H	; Pocet polozek v tabulce pro otacky
C_NT_PRE	EQU   20H	; Pocet polozek v tabulce pro tlak
C_NT_RPMS	EQU   1398	; 2^16/rozliseni otacek
C_NT_PREM	EQU   2000H	; Nasobitel tlaku / 2^16
C_NT_PREO	EQU   0		; Posun tlaku
C_N_ROTDS	EQU   0		; Deleni zubu na pocet bitu
C_N_SHIFT	EQU   0H	; Posun predstihu
C_N_HISTR	EQU   500	; RPM pri kterem se meni hystereze RIL
C_N_HISTL	EQU   20H	; Hystereze pro male otacky
C_N_HISTH	EQU   40H	; Hystereze pro velke otacky
)FI

; Watchdog timer je nastaven na 100 ms
%DEFINE (WATCHDOG) (
	ORL   PCON,#10H
	MOV   T3,#0D3H
)

%*DEFINE (W (WO)) (
	DB   LOW (%WO),HIGH (%WO)
)

%*DEFINE (LDR67i (WO)) (
	MOV  R6,#LOW  (%WO)
	MOV  R7,#HIGH (%WO)
)

%*DEFINE (LDR45i (WO)) (
	MOV  R4,#LOW  (%WO)
	MOV  R5,#HIGH (%WO)
)

%*DEFINE (LDR23i (WO)) (
	MOV  R2,#LOW  (%WO)
	MOV  R3,#HIGH (%WO)
)

%*DEFINE (LDR01i (WO)) (
	MOV  R4,#LOW  (%WO)
	MOV  R5,#HIGH (%WO)
)

%*DEFINE (LDMDi (DATM,WO)) (
	MOV  %DATM+0,#LOW  (%WO)
	MOV  %DATM+1,#HIGH (%WO)
)

%*DEFINE (LDMIi (DATM,WO)) (
	MOV  R0,#%DATM
	MOV  A,#LOW  (%WO)
	MOV  @R0,A
	INC  R0
	MOV  A,#HIGH (%WO)
	MOV  @R0,A
)

%*DEFINE (LDMXi (XDATM,WO)) (
	MOV  DPTR,#%XDATM
	MOV  A,#LOW  (%WO)
	MOVX @DPTR,A
	INC  DPTR
	MOV  A,#HIGH (%WO)
	MOVX @DPTR,A
)

%IF (%FINAL) THEN (
CSEG	AT    RESET	; Zacatek programu
	JMP   RES_STAR
CSEG	AT    EXTI1	; Realny cas z vnejsiho zdroje
	JMP   I_TIME
CSEG	AT    TIMER0	; Preteceni citace zubu 0
	JMP   I_TIM0
CSEG	AT    TIMER2	; Preteceni casovace 2
	JMP   I_T2OV
CSEG	AT    T2CAP0	; Zachyt CT0=RIH znacka otocky
	JMP   I_CT0
CSEG	AT    T2CAP3	; Zachyt CT3=CIL preruseni od hrany zubu
	JMP   I_CT3
CSEG	AT    T2CMP0	; Comparator CM0 konec paleni
	JMP   I_CM0
CSEG	AT    ADCINT	; ADC konec prevodu
	JMP   I_ADC
)FI

ZP____C SEGMENT CODE
ZP____D SEGMENT DATA
ZP____B SEGMENT DATA BITADDRESSABLE
ZP____I SEGMENT IDATA
STACK   SEGMENT IDATA
SER_STACK SEGMENT DATA
%IF (NOT %FINAL) THEN (
ZP____X SEGMENT XDATA
)FI

RSEG	STACK
STACK_S EQU   40H
	DS    STACK_S

RSEG	SER_STACK
	DS    5

USING   0

CSEG    AT    7FF0H
SER_NUM:%W    (7)
	%W    (10H)
	DB    '%MOTOR_TYPE'

RSEG ZP____B

HFLG1:  DS    1
ITIM_RF BIT   HFLG1.0

RSEG ZP____D

TMP:	DS    5

DINT25  EQU   27   ; Delitel EXINT1 na 25 Hz
DINT06S EQU   15   ; Delitel 25 Hz na 0.6 s
RSEG ZP____D
CINT25: DS    1
%IF (NOT %FINAL) THEN (
RSEG ZP____X
N_OF_T  EQU   5    ; Pocet timeru
TIMR1:  DS    1    ; Timery jsou decrementovany
TIMR2:  DS    1    ; s frekvenci 25 Hz
TIMR_WAIT:
TIMR3:  DS    1
KBDTIMR:DS    1
TIMRI:  DS    1
TIME:   DS    2    ; Cas v 0.01 min              =====

TMP_BLN EQU   40
TMP_BUF:DS    TMP_BLN

MEM_BUF:DS    100H            ; Buffer pameti EEPROM

SLAV_BL EQU   100
IIC_INP:DS    SLAV_BL
IIC_OUT:DS    SLAV_BL
IIC_BUF:DS    SLAV_BL
)FI

RSEG ZP____C

RES_STAR:

	MOV   R0,#0F0H
	CLR   A
RES_ST1:MOV   @R0,A
	DJNZ  R0,RES_ST1

	MOV   IEN0,#01000110B ; preruseni ADC, EXT1, T0
	MOV   IEN1,#10000000B ; T2OV
	MOV   IP0, #00000010B ; priority preruseni T0
	MOV   IP1, #00011001B ; CM0,CT3I,CT0I
	%WATCHDOG	      ; Nulovani watch-dogu
	MOV   TMOD,#00100101B ; timer 1 mod 2; counter 0 mod 1
	MOV   TCON,#01000101B ; citac 0 stoji;timer 1 cita ; interapy hranou
	MOV   TM2CON,#10000001B; timer 2 CLK, TR2 disabled, 16 bit OV
	MOV   CTCON,#10001001B ; CT3 = CIL, CT1=RIL, CT0=RIH
	MOV   SCON,#11011000B ; dva stopbity
	MOV   PCON,#10000000B ; Bd = OSC/12/16/(256-TH1)
	MOV   TH1,#0FAH       ; 9600Bd
	MOV   PSW,#0          ; banka registru 0
	MOV   SP,#STACK       ; inicializace zasobniku
	MOV   P1,#0FFH
	MOV   P3,#0FFH
	MOV   P4,#0FFH
	MOV   R0,#0FFH
	INC   @R0

%IF (NOT %FINAL) THEN (
	MOV   R4,#EXTI1	      ; Realny cas z vnejsiho zdroje
	MOV   DPTR,#I_TIME
	CALL  VEC_SET

	MOV   R4,#TIMER0      ; Preteceni citace zubu 0
	MOV   DPTR,#I_TIM0
	CALL  VEC_SET

	MOV   R4,#TIMER2      ; Preteceni casovace 2
	MOV   DPTR,#I_T2OV
	CALL  VEC_SET

	MOV   R4,#T2CAP0      ; Zachyt CT0=RIH znacka otocky
	MOV   DPTR,#I_CT0
	CALL  VEC_SET

	MOV   R4,#T2CAP3      ; Zachyt CT3=CIL preruseni od hrany zubu
	MOV   DPTR,#I_CT3
	CALL  VEC_SET

	MOV   R4,#T2CMP0      ; Comparator CM0 konec paleni
	MOV   DPTR,#I_CM0
	CALL  VEC_SET

	MOV   R4,#ADCINT      ; ADC konec prevodu
	MOV   DPTR,#I_ADC
	CALL  VEC_SET
)FI
	MOV   PWMP,#0         ; PWM frekvence 23 kHz
	MOV   PWM0,#0

	SETB  ITIM_RF
%IF (NOT %FINAL) THEN (
	MOV   DPTR,#TIME
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
)FI

	%WATCHDOG	      ; Nulovani watch-dogu
START:	MOV   A,#3
	CALL  I_U_LAN
%IF (NOT %FINAL) THEN (
	CLR   PS
)FI
	MOV   A,#10H
	MOV   S1ADR,A
	CALL  IIC_PRE
%IF (NOT %FINAL) THEN (
	%LDR45i(IIC_INP)
	%LDR67i(0)
	MOV   R2,#SLAV_BL
	CALL  IIC_SLX
)FI
	SETB  EA
	;%WATCHDOG	      ; Nulovani watch-dogu
	JMP   ZPSTART


%IF (0) THEN (
	CALL  uL_STR
	MOV   R0,#0FEH
	MOV   A,@R0
	MOV   P4,A
	CPL   A
	MOV   @R0,A
TTT:	%WATCHDOG
	MOV   R0,#BEG_IB
	MOV   A,@R0
	JNB   ACC.7,TTT
	MOV   R0,#BEG_IB
	MOV   @R0,#0
	JMP   TTT
)FI

; Inicializace komunikace uLan dynamicke adresace
; ===============================================

%IF (%FINAL) THEN (
I_U_LAN:PUSH  ACC
	MOV   R5,#6           ; Rychlost 9600
	CALL  uL_INIT	      ; Zpusteni uLan komunikace
	POP   ACC
	MOV   uL_ADR,A
	RET
)ELSE(
I_U_LAN:PUSH  ACC
	MOV   A,#10     ; 9600 Bd pro 18.4321 krystal
	CLR   A
	MOV   R0,#1	; Nastaveni rychlosti
	CALL  uL_FNC
	POP   ACC
	MOV   R0,#2	; Nastavi adresu
	CALL  uL_FNC
	MOV   R2,#0
	MOV   R0,#3	; Pocatek IB delka IB a OB
	CALL  uL_FNC
	MOV   R2,#0
	MOV   R0,#4	; Nastavi rychlou komunikaci
	CALL  uL_FNC
	MOV   R0,#0     ; Spusti uLAN
	CALL  uL_FNC

	MOV   R4,#V_uL_ADD ; Dynamicke adresovani
	CALL  VEC_GET
	MOV   DPTR,#UD_INTO
	CALL  VEC_USR
	MOV   R4,#V_uL_ADD
	MOV   DPTR,#UD_INT
	CALL  VEC_SET
	MOV   uL_DYFL,#3	; uLD_RQA
	RET
)FI

; Ceka urcitou dobu
%IF (%FINAL) THEN (
DBWAIT:	RET
)ELSE(
DBWAIT:	MOV    DPTR,#TIMR_WAIT
	MOVX   @DPTR,A
DBWAIT1:MOV    DPTR,#TIMR_WAIT
	MOVX   A,@DPTR
	JNZ    DBWAIT1
DBWAITR:RET
)FI

;=================================================================
; Longint aritmetika
; Operandy R4567 a [DPTR]

xLDl:	MOV   A,PSW
	ANL   A,#18H
	ADD   A,#4
	MOV   R0,A
xiLDl:	MOV   R2,#4
xiLDs:	MOVX  A,@DPTR
	MOV   @R0,A
	INC   R0
	INC   DPTR
	DJNZ  R2,xiLDs
	RET

xSVl:	MOV   A,PSW
	ANL   A,#18H
	ADD   A,#4
	MOV   R0,A
xiSVl:	MOV   R2,#4
xiSVs:	MOV   A,@R0
	MOVX  @DPTR,A
	INC   R0
	INC   DPTR
	DJNZ  R2,xiSVs
	RET

xADDl:	MOV   A,PSW
	ANL   A,#18H
	ADD   A,#4
	MOV   R0,A
xiADDl:	MOV   R2,#4
xiADDs: CLR   C
xiADD1:	MOVX  A,@DPTR
	ADDC  A,@R0
	MOV   @R0,A
	INC   R0
	INC   DPTR
	DJNZ  R2,xiADD1
	RET

xSUBl:	MOV   A,PSW
	ANL   A,#18H
	ADD   A,#4
	MOV   R0,A
xiSUBl:	MOV   R2,#4
xiSUBs: CLR   C
xiSUB1:	MOVX  A,@DPTR
	XCH   A,@R0
	SUBB  A,@R0
	MOV   @R0,A
	INC   R0
	INC   DPTR
	DJNZ  R2,xiSUB1
	RET

xCMPl:	MOV   A,PSW
	ANL   A,#18H
	ADD   A,#4
	MOV   R0,A
xiCMPl:	MOV   R2,#4
xiCMPs: CLR   C
	MOV   B,#0
xiCMP1:	MOVX  A,@DPTR
	SUBB  A,@R0
	ORL   B,A
	INC   R0
	INC   DPTR
	DJNZ  R2,xiCMP1
	INC   B
	DJNZ  B,xiCMP2
	RET
xiCMP2:	CPL   C
	CPL   A
	ORL   A,#1
	RET

NEGl:   CLR   C
	CLR   A
	SUBB  A,R4
	MOV   R4,A
	CLR   A
	SUBB  A,R5
	MOV   R5,A
	CLR   A
	SUBB  A,R6
	MOV   R6,A
	CLR   A
	SUBB  A,R7
	MOV   R7,A
	RET

ZERROl:	MOV   A,R4
	ORL   A,R5
	ORL   A,R6
	ORL   A,R7
SHRlR:	RET

SHRl:   ADD   A,#-8
	JNC   SHRL1
	XCH   A,R7
	XCH   A,R6
	XCH   A,R5
	XCH   A,R4
	CLR   A
	XCH   A,R7
	SJMP  SHRl
SHRL1:  ADD   A,#8
SHRl2:	JZ    SHRlR
	CLR   C
	XCH   A,R7
	RRC   A
	XCH   A,R7
	XCH   A,R6
	RRC   A
	XCH   A,R6
	XCH   A,R5
	RRC   A
	XCH   A,R5
	XCH   A,R4
	RRC   A
	XCH   A,R4
	DEC   A
	SJMP  SHRl2

xNULl:	MOV   R2,#4
xNULs:	CLR   A
xNULs1:	MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R2,xNULs1
	RET

; Rotuje R4567 tak ze se cislo zkrati na R45 pocet posunu ulozi do R1

NORMli: MOV   R1,#0
NORMli1:MOV   A,R7
	JZ    NORMli2
	CLR   A
	XCH   A,R7
	XCH   A,R6
	XCH   A,R5
	XCH   A,R4
	MOV   A,R1
	ADD   A,#8
	MOV   R1,A
NORMli2:MOV   A,R6
	JZ    NORMliR
	CLR   C
	MOV   A,R6
	RRC   A
	MOV   R6,A
	MOV   A,R5
	RRC   A
	MOV   R5,A
	MOV   A,R4
	RRC   A
	MOV   R4,A
	INC   R1
	SJMP  NORMli2
NORMliR:RET

; Roztahne R45 exp R1 na R4567

DENOil: CLR   A
	MOV   R6,A
	MOV   R7,A
	MOV   A,R1
	JB    ACC.7,DENOil5   ; Exponent je zaporny
	ANL   A,#7            ; Posun R45 vlevo do R4567
	JZ    DENOil2
	MOV   R0,A            ; Posun o R1 mod 8
DENOil1:CLR   C
	MOV   A,R4
	RLC   A
	MOV   R4,A
	MOV   A,R5
	RLC   A
	MOV   R5,A
	MOV   A,R6
	RLC   A
	MOV   R6,A
	DJNZ  R0,DENOil1
DENOil2:MOV   A,R1
	ANL   A,#NOT 7
	RR    A
	RR    A
	RR    A
	MOV   R0,A            ; Posun o (R1 div 8)*8
	JZ    DENOilR
DENOil3:CLR   A
	XCH   A,R4
	XCH   A,R5
	XCH   A,R6
	XCH   A,R7
	DJNZ  R0,DENOil3
DENOilR:CLR   F0
	RET
DENOil5:CPL   A               ; Posum R45 vpravo a R67=0
	INC   A
	CLR   F0
	JMP   SHRi

;=================================================================
; Casove preruseni

USING   2

%IF (%FINAL) THEN (
I_TIME: CLR   IE1
	RETI
)ELSE(
I_TIME: PUSH  ACC
	PUSH  PSW
	PUSH  B
	CLR   IE1
	MOV   PSW,#AR0

	DJNZ  CINT25,I_TIMQ
	MOV   CINT25,#DINT25  ; Pruchod s periodou 25 Hz
	CALL  uL_STR
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#TIMR1
	MOV   B,#N_OF_T-1
I_TIME2:MOVX  A,@DPTR
	JZ    I_TIME3
	DEC   A
	MOVX  @DPTR,A
I_TIME3:INC   DPTR
	DJNZ  B,I_TIME2
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	JNB   ACC.7,I_TIMR1
	JBC   ITIM_RF,I_TIME4
I_TIMR1:POP   DPH
	POP   DPL
I_TIMQ: ;%WATCHDOG	; !!!!!!!!!!!
	POP   B
	POP   PSW
	POP   ACC
I_TIMRI:RETI

ITIM_R: SETB  ITIM_RF
	SJMP  I_TIMR1

I_TIME4:;CALL  I_TIMRI ;!!!!!!!!!!
	ADD   A,#DINT06S
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	INC   DPTR
	JNZ   I_TIME5
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
I_TIME5:
	JMP   ITIM_R
)FI

%IF (NOT %FINAL) THEN (
;=================================================================
; Prace s pameti EEPROM 8582

; Precte pamet do MEM_BUF
; pokud v poradku vraci A=0

RD_MEM:	MOV   DPTR,#IIC_BUF
	CLR   A
	MOVX  @DPTR,A
RD_MEM1:MOV   R4,#LOW  IIC_BUF
	MOV   R5,#HIGH IIC_BUF
	MOV   R2,#1
	MOV   R3,#010H
	MOV   R6,#0A0H
	CALL  IIC_RQX
	JNZ   RD_MEM1
	CALL  IIC_WME
	JNZ   RD_MEMR
	MOV   DPTR,#IIC_BUF
	MOVX  A,@DPTR
	MOV   DPTR,#MEM_BUF
	CALL  ADDATDP
	MOV   R4,DPL
	MOV   R5,DPH
	MOV   R2,#LOW  (IIC_BUF+1)
	MOV   R3,#HIGH (IIC_BUF+1)
	MOV   R0,#010H
	MOV   R1,#000H
	CALL  xxMOVE
	MOV   DPTR,#IIC_BUF
	MOVX  A,@DPTR
	ADD   A,#010H
	MOVX  @DPTR,A
	JNC   RD_MEM1
RD_MEMR:RET

; Zapise MEM_BUF do pameti
; pokud v poradku vraci A=0

WR_MEEB EQU   4

WR_MEM:	MOV   DPTR,#IIC_BUF
	CLR   A
	MOVX  @DPTR,A
WR_MEM1:MOV   DPTR,#IIC_BUF
	MOVX  A,@DPTR
	MOV   DPTR,#MEM_BUF
	CALL  ADDATDP
	MOV   R2,DPL
	MOV   R3,DPH
	MOV   R4,#LOW  (IIC_BUF+1)
	MOV   R5,#HIGH (IIC_BUF+1)
	MOV   R0,#WR_MEEB
	MOV   R1,#000H
	CALL  xxMOVE
WR_MEM2:MOV   R4,#LOW  IIC_BUF
	MOV   R5,#HIGH IIC_BUF
	MOV   R2,#WR_MEEB+1
	MOV   R3,#0
	MOV   R6,#0A0H
	CALL  IIC_RQX
	JNZ   WR_MEM2
	CALL  IIC_WME
	JNZ   WR_MEM2
	MOV   DPTR,#IIC_BUF
	MOVX  A,@DPTR
	ADD   A,#WR_MEEB
	MOVX  @DPTR,A
	JNC   WR_MEM1
WR_MEMR:RET

; Zapis parametru do EEPROM

SV_MPAR:CALL  XOR_SU0
	MOVX  @DPTR,A
	CALL  WR_MEM
	RET

; Cteni parametru z EEPROM

LD_MPAR:CALL  RD_MEM
	JNZ   LD_MPAE
	CALL  XOR_SU0
	MOV   R0,A
	MOVX  A,@DPTR
	XRL   A,R0
LD_MPAE:RET

XOR_SU0:MOV   R2,#0FFH
	MOV   DPTR,#MEM_BUF
XOR_SUM:CLR   A
XOR_SU1:MOV   R3,A
	MOVX  A,@DPTR
	INC   DPTR
	XRL   A,R3
	INC   A
	DJNZ  R2,XOR_SU1
	RET
)FI

;=================================================================

; Zpracovavani prikazu z IIC pod prerusenim
; registry  R0  .. funkce
;           R12 .. ukazatel na data
;           R3  .. pocet byte do konce S_BLEN

SL_CMIC:JMP   SL_JRET

;=================================================================

PRINTc:	RET
INPUTc:	RET

;=================================================================

RSEG ZP____B

ZP_FLG:	DS    1
FL_MSPD BIT   ZP_FLG.7
FL_ROT  BIT   ZP_FLG.6
FL_CAL	BIT   ZP_FLG.5
FL_MAN  BIT   ZP_FLG.4
FL_FLS  BIT   ZP_FLG.3

MSK_ROT EQU   0C0H

RSEG ZP____D

T2EXP:	DS    1		; Rozsireni casovace 2 poctu cyklu procesoru
T2EXPO:	DS    1		; pokud je T2OV musi se pouzit tato hodnota
			; specialni korekce

RTIM:   DS    3		; Cas pocatku otacky
RPER:   DS    3		; Doba otacky v poctu cyklu
MSPD:	DS    3		; Doba pruchodu PMES zubu v poctu cyklu uP

PCNT:	DS    1		; Cislo valce ktery bude palit
PMES:	DS    2		; Zaporny pocet zubu k mereni rychlosti
PFIRST: DS    2		; Zaporny pocet zubu k prvnimu paleni
PNEXT:  DS    2		; Pocet zubu k dalsimu paleni
PSHIFT: DS    2		; Casovy posun paleni od zubu v cyklech uP
PDELAY: DS    2		; Doba sepnuti tyristoru v cyklech uP
PBLOCK: DS    2		; Doba blokovani zdroje 350V v cyklech uP
CFIRST: DS    2		; Pristi PFIRST bude pouzit pri FL_CAL=1
CSHIFT: DS    2		; Pristi PSHIFT

CCNT:	DS    2		; Kontrolni udaj o poctu paleni za otocku
CCNTP:	DS    1		; Doplnek poctu zubu

RPM:	DS    2		; Pocet otacek za minutu
ZPTMP:	DS    4

RPMCON:	DS    3		; Konstanta pro vypocet RPM

FLSWDG: DS    1		; Kontrola doby paleni

DEB:	DS    2

RSEG ZP____I

ADC_DAT:
AD_PRE:	DS    2		; Prevodnik tlaku
AD_TEMP:DS    2		; Prevodnik teploty
AD_2:	DS    2
AD_3:	DS    2

%IF (%FINAL) THEN (
RSEG ZP____C
EEP_SID EQU   55AAH
EEP_SIG:%W    (EEP_SID)
TAB_ADR:%W    (C_TAB_ADR)
X_FREQ: %W    (C_X_FREQ)
N_ROT:  %W    (C_N_ROT)
N_FLS:  %W    (C_N_FLS)
NT_RPM: %W    (C_NT_RPM)
NT_PRE: %W    (C_NT_PRE)
NT_RPMS:%W    (C_NT_RPMS)
NT_PREM:%W    (C_NT_PREM)
NT_PREO:%W    (C_NT_PREO)
N_ROTDS:%W    (C_N_ROTDS)
N_SHIFT:%W    (C_N_SHIFT)
N_HISTR:%W    (C_N_HISTR)
N_HISTL:%W    (C_N_HISTL)
N_HISTH:%W    (C_N_HISTH)

)ELSE(
RSEG ZP____X

EEP_SID EQU   55AAH
EEP_SIG XDATA MEM_BUF+0	; Kontrolni hodnota 55AA
X_FREQ	XDATA MEM_BUF+2	; Freqvence krystalu v kHz
N_ROT	XDATA MEM_BUF+4	; Pocet zubu na otocku
N_FLS	XDATA MEM_BUF+6	; Pocet paleni na otocku
NT_RPM	XDATA MEM_BUF+8	; Pocet polozek v tabulce pro otacky
NT_PRE	XDATA MEM_BUF+10; Pocet polozek v tabulce pro tlak
NT_RPMS	XDATA MEM_BUF+12; 2^16/rozliseni otacek
NT_PREM	XDATA MEM_BUF+14; Nasobitel tlaku / 2^16
NT_PREO	XDATA MEM_BUF+16; Posun tlaku
N_ROTDS	XDATA MEM_BUF+18; Deleni zubu
TAB_ADR	XDATA MEM_BUF+20; Adresa Tabulka predstihu
N_SHIFT	XDATA MEM_BUF+22; Posun predstihu proti tabulce
N_HISTR	XDATA MEM_BUF+24; RPM pri kterem se meni hystereze RIL
N_HISTL	XDATA MEM_BUF+26; Hystereze pro male otacky
N_HISTH	XDATA MEM_BUF+28; Hystereze pro velke otacky
)FI

RSEG ZP____C

I_T2OV:	INC   T2EXP
	CLR   T2OV	; Preteceni casovace 2
	MOV   T2EXPO,T2EXP
	RETI

I_CT0:  CLR   CTI3	; Prisla znacka otocky
	SETB  ECT3
	CLR   CTI0	; Konec preruseni
	RETI

I_CT3:	PUSH  ACC	; Prisel prvni zub po znacce otocky
	PUSH  PSW
	PUSH  B
	ANL   CTCON,#NOT 80H	; zakaz CT3 = CIL
	CLR   TR0
	MOV   CCNT+0,TL0; Kontrolni sejmuti udaje o poctu zubu
	MOV   CCNT+1,TH0
	MOV   CCNTP,PCNT
	MOV   TL0,PMES+0; Nastaveni preruseni od PMES zubu
	MOV   TH0,PMES+1
	CLR   TF0
	SETB  TR0
	CLR   ECT3      ; Ignorovat dalsi zuby
	MOV   PCNT,#0
	CLR   C		; Vypocet doby jedne otacky RPER
	MOV   A,CTL3	; a naplneni promenne RTIM
	SUBB  A,RTIM+0
	MOV   RPER+0,A
	MOV   RTIM+0,CTL3
	MOV   A,CTH3
	MOV   B,A
	SUBB  A,RTIM+1
	MOV   RPER+1,A
	MOV   RTIM+1,CTH3
	MOV   A,T2EXP
	JNB   T2OV,I_CT3_1
	MOV   A,T2EXPO
	JB    B.7,I_CT3_1
	INC   A
I_CT3_1:MOV   B,A
	SUBB  A,RTIM+2
	MOV   RPER+2,A
	MOV   RTIM+2,B
	MOV   A,ADCON
	ANL   A,#18H
	JNZ   I_CT3_2
	MOV   ADCON,#0
	ORL   ADCON,#8
I_CT3_2:CLR   CTI3	; Konec preruseni
	SETB  FL_MSPD
	ORL   CTCON,#80H; povoleni CT3 = CIL
	JNB   FL_CAL,I_CT3_3
	CLR   FL_CAL
	MOV   PFIRST+0,CFIRST+0
	MOV   PFIRST+1,CFIRST+1
	MOV   PSHIFT+0,CSHIFT+0
	MOV   PSHIFT+1,CSHIFT+1
I_CT3_3:POP   B
	POP   PSW
	POP   ACC
	RETI

I_TIM0:	PUSH  ACC	; Preruseni od zubu
	PUSH  PSW
	PUSH  B
	CLR   TF0
	ANL   CTCON,#NOT 80H	; zakaz CT3 = CIL
	JBC   FL_MSPD,MSPD0
FLASH:	CLR   TR0	; Dalsi pali po PNEXT
	MOV   TL0,PNEXT+0
	MOV   TH0,PNEXT+1
	SETB  TR0
	MOV   A,PCNT	; Ktery valec
	JB    ACC.3,I_TIM0R
	ANL   P4,#NOT 7
	ORL   P4,A
	MOV   A,PSHIFT+0
	ADD   A,CTL3    ; Casovy posu o PSHIFT
	MOV   CML1,A
	MOV   A,PSHIFT+1
	ADDC  A,CTH3
	MOV   CMH1,A
	MOV   A,PDELAY+0
	ADD   A,CML1	; Delka paleni PDELAY
	MOV   CML0,A    ; !!!!!!!!!!!!!!!!!!!
;	MOV   A,PDELAY+1; Ne dele nez 256 cyklu procesoru
	CLR   A
	ADDC  A,CMH1
	MOV   CMH0,A
	MOV   RTE,#8	; Povoleni rizeni paleni
	MOV   STE,#8
	CLR   CMI0
	SETB  ECM0
	SETB  P4.4
	INC   PCNT	; Dalsi valec
	SETB  FL_FLS
I_TIM0R:ORL   CTCON,#80H; povoleni CT3 = CIL
	POP   B
	POP   PSW
	POP   ACC
	RETI

MSPD0:  CLR   TR0	; Prvni pali PMES+PFIRST
	MOV   TL0,PFIRST+0
	MOV   TH0,PFIRST+1
	SETB  TR0
	CLR   C         ; Vypocet casu PMES zubu do MSPD
	MOV   A,CTL3
	SUBB  A,RTIM+0
	MOV   MSPD+0,A
	MOV   A,CTH3
	MOV   B,A
	SUBB  A,RTIM+1
	MOV   MSPD+1,A
	MOV   A,T2EXP
	JNB   T2OV,MSPD1
	MOV   A,T2EXPO
	JB    B.7,MSPD1
	INC   A
MSPD1:  SUBB  A,RTIM+2
	MOV   MSPD+2,A
	SETB  FL_ROT
	SJMP  I_TIM0R

I_CM0:  PUSH  ACC	; Preruseni od konce paleni
	PUSH  PSW
	ANL   RTE,#NOT 1FH
	ANL   STE,#NOT 1FH
	CLR   ECM0
	CLR   CMI0
	MOV   A,PBLOCK+0
	ADD   A,CML0	; Delka blokovani PBLOCK
	MOV   CML1,A
	MOV   A,PBLOCK+1
	ADDC  A,CMH0
	MOV   CMH1,A
	ORL   RTE,#10H
	POP   PSW
	POP   ACC
	RETI

I_ADC:  PUSH  ACC	; Preruseni od konce prevodu ADC
	PUSH  PSW
	MOV   A,ADCON
	ANL   A,#7
	JNB   ACC.2,I_ADC4
	ANL   ADCON,#NOT 10H
	SJMP  I_ADCR	; Neni treba dalsi prevod
I_ADC4:	RL    A
	ADD   A,#ADC_DAT
	XCH   A,R0
	PUSH  ACC
	MOV   A,ADCON	; Ulozeni hodnoty ADC
	ANL   A,#0C0H
	MOV   @R0,A
	INC   R0
	MOV   @R0,ADCH
	POP   ACC
	MOV   R0,A
	ANL   ADCON,#NOT 10H
	INC   ADCON	; Start dalsiho prevodu
	ORL   ADCON,#8
I_ADCR:	POP   PSW
	POP   ACC
	RETI

; Cteni hodnoty z ADC_DAT z adresy R0

RD_ADT:	CLR   EAD
	MOV   A,@R0
	MOV   R4,A
	INC   R0
	MOV   A,@R0
	MOV   R5,A
	SETB  EAD
	RET

; Inicializace parametru

ZPINIT: CLR   ET0
	MOV   IEN1,#10000000B ; Zakaz preruseni pro paleni
	CLR   TR0
	MOV   P4,#0FFH
%IF (NOT %FINAL) THEN (
	CALL  LD_MPAR
	JNZ   ZPINI_D
	MOV   DPTR,#EEP_SIG
	MOVX  A,@DPTR
	XRL   A,#LOW EEP_SID
	JNZ   ZPINI_D
	INC   DPTR
	MOVX  A,@DPTR
	XRL   A,#HIGH EEP_SID
	JNZ   ZPINI_D
	JMP   ZPINI20
)FI
ZPINI_D:CLR   ET0
	MOV   IEN1,#10000000B ; Zakaz preruseni pro paleni
	CLR   TR0
	MOV   P4,#0FFH
%IF (NOT %FINAL) THEN (
	%LDMXi (EEP_SIG,EEP_SID)
	%LDMXi (TAB_ADR,C_TAB_ADR)
	%LDMXi (X_FREQ,C_X_FREQ)
	%LDMXi (N_ROT,C_N_ROT)
	%LDMXi (N_FLS,C_N_FLS)
	%LDMXi (NT_RPM,C_NT_RPM)
	%LDMXi (NT_PRE,C_NT_PRE)
	%LDMXi (NT_RPMS,C_NT_RPMS)
	%LDMXi (NT_PREM,C_NT_PREM)
	%LDMXi (NT_PREO,C_NT_PREO)
	%LDMXi (N_ROTDS,C_N_ROTDS)
	%LDMXi (N_SHIFT,C_N_SHIFT)
	%LDMXi (N_HISTR,C_N_HISTR)
	%LDMXi (N_HISTL,C_N_HISTL)
	%LDMXi (N_HISTH,C_N_HISTH)
)FI
ZPINI_C:
ZPINI20:%LDMDi (PMES,-8)	; Pocet zubu pro mereni rychlosti
	%LDMDi (PDELAY,50)	; Delka paleni
	%LDMDi (PBLOCK,600)	; Delka blokovani zdroje
	%LDMDi (TMP,0)
	MOV   DPTR,#N_ROT
	CALL  cLDR23i
	MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	MOV   DPTR,#N_FLS
	CALL  cLDR23i
	CALL  DIVi
	CALL  NEGi
	MOV   PNEXT+0,R4
	MOV   PNEXT+1,R5
	; 60*X_FREQ/12/N_ROT = 445935 = 55742 * 2^3
	MOV   DPTR,#X_FREQ
	CALL  cLDR23i
	%LDR45i (5*1000)
	CALL  MULi
	CALL  NORMli
	MOV   DPTR,#N_ROT
	CALL  cLDR23i
	CALL  DIVihf
	MOV   RPMCON+0,R1
	MOV   RPMCON+1,R4
	MOV   RPMCON+2,R5
	MOV   PWM1,#040H
	MOV   T2EXP,#0
	MOV   MSPD+2,#0FFH
	CALL  ZPC
	MOV   PFIRST+0,CFIRST+0
	MOV   PFIRST+1,CFIRST+1
	MOV   PSHIFT+0,CSHIFT+0
	MOV   PSHIFT+1,CSHIFT+1
	CLR   TF0
	CLR   CTI0
	MOV   IEN1,#10000001B ; T2OV, CTI0
	SETB  ET0
	RET

; Spusteni vlastniho programu pro zapalovani

ZPSTART:;MOV   A,#48
	;CALL  DBWAIT
	MOV   ZP_FLG,#0
	CALL  ZPINIT

ZPLOOP: MOV   A,SP
	XRL   A,#STACK        ; test zasobniku
	JNZ   ZPL_RES
ZPLOO10:MOV   A,P4
	CJNE  A,P4,ZPLOO14
	JB    ACC.3,ZPLOO14   ; P4.3  CMSR3 neni paleni
	JNB   ACC.4,ZPL_RES   ; paleni a neni blokovani
	JBC   FL_FLS,ZPLOO12
	INC   FLSWDG
	MOV   A,FLSWDG
	CJNE  A,#10,ZPLOO14
ZPL_RES:CLR   EA
	MOV   P4,#0FFH
	JMP   RESET
ZPLOO12:MOV   FLSWDG,#0
ZPLOO14:

ZPLOO20:%WATCHDOG
	JB    FL_CAL,ZPLOO21
	JB    FL_MAN,ZPLOO21
	JBC   FL_ROT,ZPCAL
ZPLOO21:JBC   uLF_INE,ZPLOO24
	CALL  UD_RQ
	JMP   ZPLOOP
ZPLOO24:JMP   ZPUL

ZPCAL:	CALL  ZPC
%IF (%FINAL) THEN (
	JMP   ZPLOOP
)FI
	MOV   A,TMP+1
	JNZ   ZPCAL1
	%LDMDi (TMP,0C000H)
	MOV   DPL,TMP
	MOV   DPH,TMP+1
ZPCAL0:	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,DPH
	CJNE  A,#0D0H,ZPCAL0
	JMP   ZPLOOP
ZPCAL1:	XRL   A,#0D0H
	JZ    ZPCAL2
	MOV   DPL,TMP
	MOV   DPH,TMP+1
%IF (0) THEN (
	MOV   A,CCNT
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,CCNT+1
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,CCNTP
	MOVX  @DPTR,A
	INC   DPTR
) ELSE (
	MOV   A,DEB
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,DEB+1
	MOVX  @DPTR,A
	INC   DPTR
)FI
	MOV   TMP,DPL
	MOV   TMP+1,DPH
ZPCAL2:
	JMP   ZPLOOP

	JMP   ZPLOOP

ZPC:
ZPC100:	MOV   R4,MSPD	; Cas 8 zubu
	MOV   R5,MSPD+1
	MOV   R6,MSPD+2
	MOV   A,MSPD
	XRL   A,R4
	JNZ   ZPC100
	MOV   A,MSPD+1
	XRL   A,R5
	JNZ   ZPC100
	MOV   A,MSPD+2
	XRL   A,R6
	JNZ   ZPC100
	MOV   R1,#-3    ; 1/8=2^-3
ZPC110:	MOV   A,R6
	JZ    ZPC120
	CLR   C
	RRC   A
	MOV   R6,A
	MOV   A,R5
	RRC   A
	MOV   R5,A
	MOV   A,R4
	RRC   A
	MOV   R4,A
	INC   R1
	SJMP  ZPC110
ZPC120: MOV   A,R4	; R45*2^R1 = cas 1 zubu
	MOV   ZPTMP,A	; do ZPTMP
	MOV   R2,A
	MOV   A,R5
	MOV   ZPTMP+1,A
	MOV   R3,A
	MOV   ZPTMP+2,R1
	; 60*fuP/12/N_zubu = 445935 = 55742 * 2^3
	MOV   A,RPMCON+0; (3)
	CLR   C
	SUBB  A,R1
	MOV   R1,A
	MOV   R4,RPMCON+1; (55742)
	MOV   R5,RPMCON+2
	CALL  DIVihf
	MOV   A,R1	; R45*2^R1 = RPM
	CPL   A
	INC   A
	JNB   ACC.7,ZPC140
	MOV   R4,#0FFH	; Vice jak 65535 RPM
	MOV   R5,#0FFH
	SJMP  ZPC145
ZPC140:	CALL  SHRi	; R45 = RPM
ZPC145: MOV   C,ES
	CLR   ES
	MOV   RPM,R4
	MOV   RPM+1,R5
	MOV   ES,C
	MOV   DPTR,#N_HISTR ; Jakou hysterezi snimace
	CALL  cLDR23i
	CALL  CMPi
	MOV   DPTR,#N_HISTL ; Hystereze pro male otacky
	JC    ZPC146
	MOV   DPTR,#N_HISTH ; Hystereze pro velke otacky
ZPC146: CLR   A
	MOVC  A,@A+DPTR	    ; cLDR
	MOV   PWM1,A
	MOV   DPTR,#NT_RPMS
	CALL  cLDR23i
	CALL  MULi	; RPM*2^16/Rozliseni tabulky
	MOV   DPTR,#NT_RPM
	CLR   A         ; cLDR
	MOVC  A,@A+DPTR	; Pocet hodnot pro otacky
	DEC   A
	MOV   ZPTMP+3,A	; Hodnota pouzita pri prekroceni
	CPL   A
	MOV   R0,A
	CJNE  R7,#0,ZPC150
	MOV   A,R6
	ADD   A,R0	; Prekroceny posun z RPM ?
	JC    ZPC150
	MOV   ZPTMP+3,R6; Posun z RPM
ZPC150: MOV   R0,#AD_PRE; Podtlak
	CALL  RD_ADT
	MOV   DPTR,#NT_PREM
	CALL  cLDR23i	; Koeficient
	CALL  MULi	; PRE*R23/2^16
	MOV   A,R6
	MOV   R4,A
	MOV   A,R7
	MOV   R5,A
	MOV   DPTR,#NT_PREO
	CALL  cLDR23i	; Offset
	CALL  ADDi
	MOV   A,R5
	MOV   R4,#0
	JB    ACC.7,ZPC160
	MOV   DPTR,#NT_PRE
	CLR   A
	MOVC  A,@A+DPTR	; cLDR
	DEC   A
	MOV   R4,A
	CPL   A
	MOV   R0,A
	MOV   A,R5
	ADD   A,R0	; Max hodnota podtlaku
	JC    ZPC160
	MOV   A,R5
	MOV   R4,A
ZPC160: MOV   DPTR,#NT_RPM
	CLR   A
	MOVC  A,@A+DPTR ; cLDR
	MOV   B,A	; Delka pro 1 hodnotu tlaku
	MOV   A,R4
	MUL   AB	; * Tlak
	MOV   R0,A
	MOV   DPTR,#TAB_ADR	; Pocatek tabulky
	CLR   A
	MOVC  A,@A+DPTR	; cLDR
	ADD   A,R0
	MOV   R0,A
	INC   DPTR
	CLR   A
	MOVC  A,@A+DPTR	; cLDR
	ADDC  A,B
	MOV   DPL,R0
	MOV   DPH,A
	MOV   A,ZPTMP+3
	CALL  ADDATDP
	MOV   DEB,DPL
	MOV   DEB+1,DPH
	CLR   A
	MOVC  A,@A+DPTR	; Hodnota predstihu z tabulky
	MOV   R5,A
	MOV   R4,#0
	MOV   DPTR,#N_ROTDS
	CLR   A
	MOVC  A,@A+DPTR	; cLDR
	CALL  SHRi      ; V R5 pocet zubu a v R4 pocet 1/256 zubu
	MOV   DPTR,#N_SHIFT
	CALL  cLDR23i
	CALL  ADDi
	MOV   A,R5
	ADD   A,#-8-1	; Pocet zubu pro mereni
	CPL   A
	INC   A
	MOV   CFIRST,A	; Pocet zubu pro paleni
	MOV   CFIRST+1,#0FFH
	MOV   R5,#1
	MOV   R2,ZPTMP	; Cas 1 zubu
	MOV   R3,ZPTMP+1
	MOV   R1,ZPTMP+2
	CALL  MULi
	MOV   A,R5
	MOV   R4,A
	MOV   A,R6
	MOV   R5,A
	MOV   A,R7
	MOV   R6,A
ZPC180:	MOV   A,R6
	JZ    ZPC185
	CLR   C
	RRC   A
	MOV   R6,A
	MOV   A,R5
	RRC   A
	MOV   R5,A
	MOV   A,R4
	RRC   A
	MOV   R4,A
	INC   R1
	SJMP  ZPC180
ZPC185: MOV   A,R1
	JB    ACC.7,ZPC194
	MOV   A,R5
	JB    ACC.7,ZPC193
ZPC190: MOV   A,R1
	JZ    ZPC195
	DEC   R1
	CLR   C
	MOV   A,R4
	RLC   A
	MOV   R4,A
	MOV   A,R5
	RLC   A
	MOV   R5,A
	ORL   C,ACC.7
	JNC   ZPC190
ZPC193:	MOV   R4,#0FFH
	MOV   R5,#07FH
	SJMP  ZPC195
ZPC194: CPL   A
	INC   A
	CALL  SHRi
	MOV   A,R5
	JB    ACC.7,ZPC193
ZPC195: MOV   CSHIFT,R4	; Casovy posun <1,2)*cas 1 zubu
	MOV   CSHIFT+1,R5
	SETB  FL_CAL
	RET

ZPT:	MOV   R4,RPM
	MOV   R5,RPM+1
	JMP   ZPC145

%IF (%FINAL) THEN (

;=================================================================
; System dynamicke adresace a vysilani statusu

PUBLIC	uL_SNST,uL_IDB,uL_IDE

RSEG ZP____B

uL_DYFL:DS    1
uLD_RQA	BIT   uL_DYFL.2       ; Pozadavek na pripojeni do site
uLF_INE BIT   uL_DYFL.1	      ; !!!!!!!!!!!!!!!! PRYC

RSEG ZP____C

uL_IDB: DB    '.mt ZAP1 v 0.2 .uP 51i .dy',0
uL_IDE:

uL_SNST:JMP   NAK_CMD

UD_RQ:	RET

ZPUL:	JMP   ZPLOOP

)ELSE(
;=================================================================
; System dynamicke adresace a vysilani statusu

RSEG ZP____B

uL_DYFL:DS    1
uLD_RQA	BIT   uL_DYFL.2       ; Pozadavek na pripojeni do site

RSEG ZP____X

UD_INTO:DS    3
UD_SFN:	DS    1
UD_DYSA:DS    1

RSEG ZP____C

UD_INT: CJNE  A,#0C1H,UD_INT1 ; uL_GST
	JMP   UD_SNST
UD_INT1:CJNE  A,#0F0H,UD_INT9 ; uL_SID
; Vysle svoji identifikaci
	CALL  ACK_CMD
	CALL  SND_BEB
	MOV   R2,#LOW uL_IDB; Vysle svoji identifikaci
	MOV   R3,#HIGH uL_IDB
	MOV   R6,#LOW uL_IDE
	MOV   R7,#HIGH uL_IDE
	MOV   A,R4
	MOV   R5,A
	CALL  SND_Bx
	CALL  SND_END
	JMP   S_WAITD
UD_INT9:DB    2 ; JMP
	DW    UD_INTO

uL_IDB: DB    '.mt ZAP1 pro %MOTOR_TYPE v 0.72 .uP 51x .dy',0
uL_IDE:

; Rutina vyslani zadosti o prideleni dynamicke adresy

UD_RQ:  JB    uLD_RQA,UD_RQ01
	RET
UD_RQ01:DEC   uL_DYFL
	MOV   DPTR,#UD_DYSA
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#07FH
	CLR   F0
	CALL  uL_O_OP
	MOV   DPTR,#UD_RQC1
	%LDR45i (1)
	CALL  uL_WR
	MOV   DPTR,#SER_NUM
	%LDR45i (4)
	CALL  uL_WR
	CALL  uL_O_CL
	RET

UD_RQC1:DB    0C0H	; Zadost o dynamickou adresu

; Rutina zpracuje jiz otevrenou zpravu s CMD=7FH

UD_NCS:	%LDR45i (1)
	MOV   DPTR,#UL_TMP
	CALL  UL_RDB
	CJNE  A,#0C1H,UD_NCSR
	MOV   R7,#0
	MOV   R6,#4
UD_NC10:MOV   DPTR,#UL_TMP
	CALL  UL_RDB
	MOV   R0,A
	MOV   A,R7
	MOV   DPTR,#SER_NUM
	MOVC  A,@A+DPTR
	XRL   A,R0
	JNZ   UD_NCSR
	INC   R7
	DJNZ  R6,UD_NC10
	MOV   DPTR,#UL_TMP
	CALL  UL_RDB
	JB    F0,UD_NCSR
	MOV   DPTR,#uL_ADR
	MOVX  @DPTR,A
	ANL   uL_DYFL,#NOT 7
UD_NCSR:CLR   F0
	JMP   UL_I_CL

; Rutina vysilani statusu CMD=0C1H

UD_SNST:MOV   A,R0
	MOV   C,ACC.0
	MOV   F0,C
	CALL  S_EQP
	JZ    SNSTA03
	CALL  S_R0FB
	MOV   A,R0
	JNZ   SNSTA10
	JB    uLD_RQA,SNSTA03	; Snaha o zviditelneni
	INC   uL_DYFL
	JNB   uLD_RQA,SNSTA03
SNSTA02:; ORL   uL_DYFL,#7
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#uL_SA
	MOVX  A,@DPTR
	MOV   DPTR,#UD_DYSA	; Server dynamickych adres
	MOVX  @DPTR,A
	MOV   DPTR,#uL_ADR
	CLR   A
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
SNSTA03:JMP   SNSTAR

SNSTA10:ANL   A,#0F0H
	CJNE  A,#010H,SNSTA03
	MOV   A,R0		; Prikaz cteni udaju
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#UD_SFN
	MOVX  @DPTR,A
	MOV   DPTR,#SER_NUM	; Kontrola serioveho cisla
	MOV   R1,#4
SNSTA11:CALL  S_EQP
	JZ    SNSTA12
	CALL  S_R0FB
	MOVX  A,@DPTR
	XRL   A,R0
	JNZ   SNSTA12
	INC   DPTR
	DJNZ  R1,SNSTA11
SNSTA12:POP   DPH
	POP   DPL
	JNZ   SNSTA02		; Nesouhlasi cislo
SNSTA13:MOV   C,F0
	MOV   ACC.0,C
	MOV   R0,A
	CALL  ACK_CMD
	ANL   uL_DYFL,#NOT 7
SNSTA20:CALL  SND_BEB
	MOV   R2,#LOW SER_NUM	; Vysle svoje seriove cislo
	MOV   R3,#HIGH SER_NUM
	MOV   R6,#LOW (SER_NUM+4)
	MOV   R7,#HIGH (SER_NUM+4)
	MOV   A,R4
	MOV   R5,A
	CALL  SND_Bx
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#UD_SFN
	MOVX  A,@DPTR
	POP   DPH
	POP   DPL
	CJNE  A,#010H,SNSTA30
	CLR   A			; Vyslani zakladnich udaju
	CALL  SND_CHC ; mod
	CLR   A
	CALL  SND_CHC ; chyby
	MOV   R0,#RPM
	CALL  SND_IDi
	MOV   R0,#ADC_DAT
	MOV   R2,#4
SNSTA25:CALL  SND_IDi
	DJNZ  R2,SNSTA25
	MOV   R0,#PFIRST
	CALL  SND_IDi
	MOV   R0,#PSHIFT
	CALL  SND_IDi
	SJMP  SNSTA50
SNSTA30:CJNE  A,#011H,SNSTA50
	MOV   R0,#PFIRST	; Vyslani servisnich udaju
	CALL  SND_IDi
	MOV   R0,#PSHIFT
	CALL  SND_IDi

SNSTA50:CALL  SND_END
SNSTAR: JMP   S_WAITD
	JMP   NAK_CMD

SND_IDi:MOV   A,@R0
	MOV   R4,A
	INC   R0
	MOV   A,@R0
	MOV   R5,A
	INC   R0
SNDR45i:MOV   A,R4
	CALL  SND_CHC
	MOV   A,R5
	JMP   SND_CHC
)FI

%IF (NOT %FINAL) THEN (
;=================================================================
; Objektova komunikace

; Hlavicka definice noveho typu objektu
; OID_N je specificke cislo objektu
; OID_D je ukazatel na popis typu a jmena objektu
%*DEFINE (OID_NEW (OID_N,OID_D)) (
OID_P	SET   OID_T
OID_T	SET   $
	DB   LOW (%OID_N),HIGH (%OID_N)
	DB   LOW (OID_P),HIGH (OID_P)
	DB   LOW (%OID_D),HIGH (%OID_D)
);

RSEG ZP____X
UL_BADR:DS    1
UL_BCMD:DS    1
UL_SN:  DS    1
UL_BSN: DS    1

UL_OID: DS    2

UL_TMP: DS    10H

RSEG ZP____C

ZPUL:	CLR   F0
	CLR   uLF_INE
	CALL  uL_I_OP	; vraci R4 Adr a R5 Com
	JNB   F0,ZPUL10
	JMP   ZPLOOP
ZPUL10: MOV   DPTR,#UL_BADR
	MOV   A,R4
	MOVX  @DPTR,A
	SETB  uLF_INE
	CJNE  R5,#7FH,ZPUL11
	CALL  UD_NCS		; Network control services
	JMP   ZPLOOP
ZPUL11:	CJNE  R5,#10H,ZPUL50
	MOV   DPTR,#UL_BCMD	; Objektova komunikace
	%LDR45i (3)
	CALL  UL_RD
	JB    F0,ZPUL50
ZPUL20:	MOV   DPTR,#UL_OID
	%LDR45i (2)
	CALL  UL_RD
	JB    F0,ZPUL50
	MOV   DPTR,#UL_OID
	CALL  xLDR45i
	MOV   DPTR,#OID_IN
	CALL  SEL_OID
	JNB   F0,ZPUL20
	; Spatny objekt
ZPUL50: CLR   F0
	CALL  uL_I_CL
	JMP   ZPLOOP

; Cte OID ze vstupu a podle nich vola prikazy pro vystup

OU_RDRQ:MOV   DPTR,#UL_BADR
	CALL  xLDR45i
	CALL  UL_O_OP	; Pripravi DADR,SADR,COM
	JB    F0,OU_RDER
	MOV   DPTR,#UL_TMP
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A
	MOV   DPTR,#UL_SN
	MOVX  A,@DPTR
	MOV   DPTR,#UL_TMP+2
	MOVX  @DPTR,A
	MOV   DPTR,#UL_TMP
	%LDR45i (3)
	CALL  UL_WR	; BCOM,SN,BSN
	MOV   DPTR,#UL_OID
	MOVX  A,@DPTR
	ORL   A,#1
	MOVX  @DPTR,A
	%LDR45i (2)
	CALL  UL_WR	; Vysle identifikaci odpovedi
OU_RD1: MOV   DPTR,#UL_OID
	%LDR45i (2)
	CALL  UL_RD	; Nacte OID
	JB    F0,OU_RDER
	MOV   DPTR,#UL_OID
	%LDR45i (2)
	CALL  UL_WR	; Vysle OID
	JB    F0,OU_RDER
	MOV   DPTR,#UL_OID
	CALL  xLDR45i
	ORL   A,R4
	JZ    OU_RDR
	MOV   DPTR,#OID_OUT
	CALL  SEL_OID	; Vysle data objektu
	JNB   F0,OU_RD1
	; Spatny objekt
OU_RDER:SETB  F0
	RET
OU_RDR: CALL  UL_O_CL
	RET

; Vyber funkce pro zpracovani objektu
; DPTR musi ukazovat na seznam typu objektu
; R45 je typ objektu
; pokud je typ nalezen vola se prislusna fce s DPTR na parametry
; jinak se vraci s F0

SEL_OID:MOV   A,DPH
	ORL   A,DPL
	JNZ   SEL_OI1
	SETB  F0
	RET
SEL_OI1:MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R1,A
	INC   DPTR
	MOV   A,R0
	XRL   A,R4
	XCH   A,R1
	XRL   A,R5
	ORL   A,R1
	JZ    SEL_OI2
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	SJMP  SEL_OID
SEL_OI2:INC   DPTR
	INC   DPTR
	INC   DPTR
	INC   DPTR
	MOVX  A,@DPTR
	PUSH  ACC
	INC   DPTR
	MOVX  A,@DPTR
	PUSH  ACC
	INC   DPTR
	RET

; Funkce volana pro prijem cisla integer
; DPTR ukazuje na parametry ( adresa a volana fce )
; adresa 0 nic se nezapise
;	 1 az 255 IDATA
;        >=256    XDATA

IN_INT: PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#UL_TMP
	%LDR45i (2)
	CALL  UL_RD
	MOV   DPTR,#UL_TMP
	CALL  xLDR45i
	POP   DPH
	POP   DPL
	JB    F0,IN_INTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	INC   DPTR
	JZ    IN_INT1
	PUSH  DPL
	PUSH  DPH
	MOV   DPH,A
	MOV   DPL,R0
	CALL  xSVR45i
	POP   DPH
	POP   DPL
	SJMP  IN_INT2
IN_INT1:MOV   A,R0
	JZ    IN_INT2
	MOV   A,R4
	MOV   @R0,A
	INC   R0
	MOV   A,R5
	MOV   @R0,A
IN_INT2:
CAL_DPx:MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	ORL   A,R0
	JZ    IN_INT3
	MOV   A,R0
	PUSH  ACC
	MOVX  A,@DPTR
	PUSH  ACC
IN_INT3:INC   DPTR
IN_INTR:RET

; Funkce volana pro vyslani cisla integer
; DPTR ukazuje na parametry ( adresa a volana fce )

OU_INT:	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	INC   DPTR
	JZ    OU_INT1
	PUSH  DPL
	PUSH  DPH
	MOV   DPH,A
	MOV   DPL,R0
	CALL  xLDR45i
	POP   DPH
	POP   DPL
	SJMP  OU_INT2
OU_INT1:MOV   A,R0
	JZ    OU_INT2
	MOV   A,@R0
	MOV   R4,A
	INC   R0
	MOV   A,@R0
	MOV   R5,A
OU_INT2:CALL  CAL_DPx
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#UL_TMP
	CALL  xSVR45i
	MOV   DPTR,#UL_TMP
	%LDR45i (2)
	CALL  UL_WR
	POP   DPH
	POP   DPL
OU_INTR:RET
)FI

%IF (NOT %FINAL) THEN (
;=================================================================
; Identifikace objektu

I_RDRQ  EQU   20
I_RPER  EQU   201
I_MSPD	EQU   202
I_PMES	EQU   203
I_PFRST EQU   204
I_PNEXT EQU   205
I_PSHFT EQU   206
I_PDEL  EQU   207
I_PBLCK EQU   208
I_CCNT	EQU   209
I_CCNTP	EQU   210
I_RPM	EQU   211
I_PRES	EQU   212
I_TEMP	EQU   213
I_AD_2	EQU   214
I_AD_3	EQU   215

I_FREQ	EQU   222	; Freqvence krystalu v kHz
I_ROT	EQU   223	; Pocet zubu na otocku
I_FLS	EQU   224	; Pocet paleni na otocku
I_TRPM	EQU   225	; Pocet polozek v tabulce pro otacky
I_TPRE	EQU   226	; Pocet polozek v tabulce pro tlak
I_TRPMS	EQU   227	; 2^16/rozliseni otacek
I_TPREM	EQU   228	; Nasobitel tlaku / 256
I_TPREO	EQU   229	; Posun tlaku
I_ROTDS	EQU   230	; Deleni zubu
I_TADR	EQU   231	; Adresa Tabulka predstihu
I_SHIFT	EQU   232	; Posun predstihu proti tabulce
I_HISTR	EQU   233	; RPM pri kterem se meni hystereze RIL
I_HISTL	EQU   234	; Hystereze pro male otacky
I_HISTH	EQU   235	; Hystereze pro velke otacky

I_C_RES	EQU   250	; Zresetuje zarizeni
I_C_DEF	EQU   251	; Nastavi defaultni hodnoty
I_C_INI	EQU   252	; Prepocita nastaveni
I_C_ZPC	EQU   253	; Spusti cely vypocet predstihu
I_C_ZPT	EQU   254	; Prepocita od RPM dal
I_C_SVP	EQU   255	; Ulozeni parametru do EEPROM
)FI

%IF (NOT %FINAL) THEN (
;=================================================================
; Vstupni objekty

OID_T	SET   0

%OID_NEW(I_RPER,0)
	%W    (IN_INT)	; Volana funkce - parametr DPTR -,
	%W    (RPER)	; Parametry pro funkci         <-'
	%W    (0)

%OID_NEW(I_MSPD,0)
	%W    (IN_INT)
	%W    (MSPD)
	%W    (0)

%OID_NEW(I_PMES,0)
	%W    (IN_INT)
	%W    (PMES)
	%W    (0)

%OID_NEW(I_PFRST,0)
	%W    (IN_INT)
	%W    (PFIRST)
	%W    (0)

%OID_NEW(I_PNEXT,0)
	%W    (IN_INT)
	%W    (PNEXT)
	%W    (0)

%OID_NEW(I_PSHFT,0)
	%W    (IN_INT)
	%W    (PSHIFT)
	%W    (0)

%OID_NEW(I_PDEL,0)
	%W    (IN_INT)
	%W    (PDELAY)
	%W    (0)

%OID_NEW(I_PBLCK,0)
	%W    (IN_INT)
	%W    (PBLOCK)
	%W    (0)

%OID_NEW(I_CCNT,0)
	%W    (IN_INT)
	%W    (CCNT)
	%W    (0)

%OID_NEW(I_CCNTP,0)
	%W    (IN_INT)
	%W    (CCNTP)
	%W    (0)

%OID_NEW(I_RDRQ,0)
	%W    (OU_RDRQ)

%OID_NEW(I_RPM,0)
	%W    (IN_INT)
	%W    (RPM)
	%W    (0)

%OID_NEW(I_PRES,0)
	%W    (IN_INT)
	%W    (AD_PRE)
	%W    (0)

%OID_NEW(I_TEMP,0)
	%W    (IN_INT)
	%W    (AD_TEMP)
	%W    (0)

%OID_NEW(I_FREQ,0)
	%W    (IN_INT)
	%W    (X_FREQ)
	%W    (0)

%OID_NEW(I_ROT,0)
	%W    (IN_INT)
	%W    (N_ROT)
	%W    (0)

%OID_NEW(I_FLS,0)
	%W    (IN_INT)
	%W    (N_FLS)
	%W    (0)

%OID_NEW(I_TRPM,0)
	%W    (IN_INT)
	%W    (NT_RPM)
	%W    (0)

%OID_NEW(I_TPRE,0)
	%W    (IN_INT)
	%W    (NT_PRE)
	%W    (0)

%OID_NEW(I_TRPMS,0)
	%W    (IN_INT)
	%W    (NT_RPMS)
	%W    (0)

%OID_NEW(I_TPREM,0)
	%W    (IN_INT)
	%W    (NT_PREM)
	%W    (0)

%OID_NEW(I_TPREO,0)
	%W    (IN_INT)
	%W    (NT_PREO)
	%W    (0)

%OID_NEW(I_ROTDS,0)
	%W    (IN_INT)
	%W    (N_ROTDS)
	%W    (0)

%OID_NEW(I_TADR,0)
	%W    (IN_INT)
	%W    (TAB_ADR)
	%W    (0)

%OID_NEW(I_SHIFT,0)
	%W    (IN_INT)
	%W    (N_SHIFT)
	%W    (0)

%OID_NEW(I_HISTR,0)
	%W    (IN_INT)
	%W    (N_HISTR)
	%W    (0)

%OID_NEW(I_HISTL,0)
	%W    (IN_INT)
	%W    (N_HISTL)
	%W    (0)

%OID_NEW(I_HISTH,0)
	%W    (IN_INT)
	%W    (N_HISTH)
	%W    (0)

%OID_NEW(I_RDRQ,0)
	%W    (OU_RDRQ)

%OID_NEW(I_C_RES,0)
	%W    (RESET)

%OID_NEW(I_C_DEF,0)
	%W    (ZPINI_D)

%OID_NEW(I_C_INI,0)
	%W    (ZPINI_C)

%OID_NEW(I_C_ZPC,0)
	%W    (ZPC)

%OID_NEW(I_C_ZPT,0)
	%W    (ZPT)

%OID_NEW(I_C_SVP,0)
	%W    (SV_MPAR)

OID_IN  SET   OID_T
)FI

%IF (NOT %FINAL) THEN (
;=================================================================
; Vystupni objekty

OID_T	SET   0

%OID_NEW(I_RPER,0)
	%W    (OU_INT)
	%W    (RPER)
	%W    (0)

%OID_NEW(I_MSPD,0)
	%W    (OU_INT)
	%W    (MSPD)
	%W    (0)

%OID_NEW(I_PMES,0)
	%W    (OU_INT)
	%W    (PMES)
	%W    (0)

%OID_NEW(I_PFRST,0)
	%W    (OU_INT)
	%W    (PFIRST)
	%W    (0)

%OID_NEW(I_PNEXT,0)
	%W    (OU_INT)
	%W    (PNEXT)
	%W    (0)

%OID_NEW(I_PSHFT,0)
	%W    (OU_INT)
	%W    (PSHIFT)
	%W    (0)

%OID_NEW(I_PDEL,0)
	%W    (OU_INT)
	%W    (PDELAY)
	%W    (0)

%OID_NEW(I_PBLCK,0)
	%W    (OU_INT)
	%W    (PBLOCK)
	%W    (0)

%OID_NEW(I_CCNT,0)
	%W    (OU_INT)
	%W    (CCNT)
	%W    (0)

%OID_NEW(I_CCNTP,0)
	%W    (OU_INT)
	%W    (CCNTP)
	%W    (0)

%OID_NEW(I_RPM,0)
	%W    (OU_INT)
	%W    (RPM)
	%W    (0)

%OID_NEW(I_PRES,0)
	%W    (OU_INT)
	%W    (AD_PRE)
	%W    (0)

%OID_NEW(I_TEMP,0)
	%W    (OU_INT)
	%W    (AD_TEMP)
	%W    (0)

%OID_NEW(I_FREQ,0)
	%W    (OU_INT)
	%W    (X_FREQ)
	%W    (0)

%OID_NEW(I_ROT,0)
	%W    (OU_INT)
	%W    (N_ROT)
	%W    (0)

%OID_NEW(I_FLS,0)
	%W    (OU_INT)
	%W    (N_FLS)
	%W    (0)

%OID_NEW(I_TRPM,0)
	%W    (OU_INT)
	%W    (NT_RPM)
	%W    (0)

%OID_NEW(I_TPRE,0)
	%W    (OU_INT)
	%W    (NT_PRE)
	%W    (0)

%OID_NEW(I_TRPMS,0)
	%W    (OU_INT)
	%W    (NT_RPMS)
	%W    (0)

%OID_NEW(I_TPREM,0)
	%W    (OU_INT)
	%W    (NT_PREM)
	%W    (0)

%OID_NEW(I_TPREO,0)
	%W    (OU_INT)
	%W    (NT_PREO)
	%W    (0)

%OID_NEW(I_ROTDS,0)
	%W    (OU_INT)
	%W    (N_ROTDS)
	%W    (0)

%OID_NEW(I_TADR,0)
	%W    (OU_INT)
	%W    (TAB_ADR)
	%W    (0)

%OID_NEW(I_SHIFT,0)
	%W    (OU_INT)
	%W    (N_SHIFT)
	%W    (0)

%OID_NEW(I_HISTR,0)
	%W    (OU_INT)
	%W    (N_HISTR)
	%W    (0)

%OID_NEW(I_HISTL,0)
	%W    (OU_INT)
	%W    (N_HISTL)
	%W    (0)

%OID_NEW(I_HISTH,0)
	%W    (OU_INT)
	%W    (N_HISTH)
	%W    (0)

%OID_NEW(I_RDRQ,0)
	%W    (OU_RDRQ)

OID_OUT SET   OID_T

;=================================================================
)FI

	END