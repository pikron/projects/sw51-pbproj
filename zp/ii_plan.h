;********************************************************************
;*                    ID 2050 - PLAN.H                              *
;*     Hlavicka pro vyuziti komunikacnich sluzeb PLAN               *
;*                  Stav ke dni 27. 3.1995                          *
;*                      (C) Pisoft 1994                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

; Vnejsi rutina vyslani statusu musi volat ACK_CMD, SND_BEB,
; n.SND_CHR, SND_END a skocit na S_WAITD nebo skok na NAK_CMD
; musi byt publikovana uL_SNST

; Rutiny volane z uL_SNST
EXTRN   CODE (ACK_CMD,SND_BEB,SND_CHC,SND_END,S_WAITD,NAK_CMD)
EXTRN   CODE (	      REC_BEG,REC_CHR,REC_END,REC_CME)
EXTRN   CODE (REC_Bi, SND_Bi, SND_Bc)
EXTRN   DATA (uL_ADR,uL_CMD,uL_SA)
EXTRN   IDATA(BEG_PB,END_PB,BEG_OB,END_OB,BEG_IB,END_IB)

; Rutiny pro spolupraci s PLAN mino preruseni
EXTRN   CODE (uL_STR,uL_INIT)
EXTRN   BIT  (uLF_ERR,uLF_SN,uLF_RS,uLF_NB)