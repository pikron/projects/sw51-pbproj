;********************************************************************
;*                    LCP 4000 - PB_AI.ASM                          *
;*     Aritmetika v pevne radove carce - Include file               *
;*                  Stav ke dni 18.06.1995                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

EXTRN   CODE(ADDi,SUBi,NEGi,CMPi,MULi,SMULi,DIVi,DIVi1,MODi,DIVihf)
EXTRN   CODE(MULsi,TSTMULO)
EXTRN   CODE(SHRi,SHR1i)
EXTRN   CODE(xLDR23i,xLDR45i,xSVR45i,cLDR23i,cLDR45i,DECDPTR,MR45R67)
EXTRN   CODE(O10E4i,rLDR23i,xLDR123,xSVR23i,xSVR123)
EXTRN   CODE(PRINTi,PRINTiP,OUTi)
EXTRN   CODE(INPUTi)

EXTRN   DATA(Xi)

; Vyzaduje LP_TTY.H include file- definice spoluprace s displejem
;
; Rutiny : LCDWR,LCDWR1,LCDWCO1,LCDNBUS,INPUTc
