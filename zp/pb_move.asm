;********************************************************************
;*                    LCP 4000 - PB_MOVE.ASM                        *
;*                       Pametove presuny                           *
;*                  Stav ke dni 11.11.1995                          *
;*                      (C) Pisoft 1995                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

PUBLIC  cMDPDP,xMDPDPA,xMDPDP
PUBLIC  ADDATDP
PUBLIC  cxMOVEt,cxMOVE,cxMOVE1
PUBLIC  xxMOVEt,xxMOVE,xxMOVE1
PUBLIC  XCDPR01

PB_MV_C SEGMENT CODE

RSEG	PB_MV_C

cMDPDP: CLR    A              ; DPTR = c:[DPTR]
	MOVC   A,@A+DPTR
	MOV    R0,A
	INC    DPTR
	CLR    A
	MOVC   A,@A+DPTR
	SJMP   xMDPDPR

xMDPDPA:MOV    R0,A           ; DPTR = A + x:[DPTR]
	MOVX   A,@DPTR
	ADD    A,R0
	MOV    R0,A
	INC    DPTR
	MOVX   A,@DPTR
	ADDC   A,#0
	SJMP   xMDPDPR

xMDPDP:	MOVX   A,@DPTR        ; DPTR = x:[DPTR]
	MOV    R0,A
	INC    DPTR
	MOVX   A,@DPTR
xMDPDPR:MOV    DPH,A
	MOV    DPL,R0
cxMOVER:RET

cxMOVEt:MOV    R1,#000H
cxMOVE: MOV    DPL,R2         ; R01 x x:[R45] = c:[R23]
	MOV    DPH,R3
cxMOVE1:MOV    A,R0           ; R01 x x:[R45] = c:[DPTR]
	ORL    A,R1
	JZ     cxMOVER
	CLR    A
	MOVC   A,@A+DPTR
	INC    DPTR
	MOV    R2,DPL
	MOV    R3,DPH
	MOV    DPL,R4
	MOV    DPH,R5
	MOVX   @DPTR,A
	INC    DPTR
	MOV    R4,DPL
	MOV    R5,DPH
	MOV    A,R0
	DEC    R0
	JNZ    cxMOVE
	DEC    R1
	SJMP   cxMOVE

xxMOVEt:MOV    R1,#000H
xxMOVE: MOV    DPL,R2         ; R01 x x:[R45] = x:[R23]
	MOV    DPH,R3
xxMOVE1:MOV    A,R0           ; R01 x x:[R45] = x:[DPTR]
	ORL    A,R1
	JZ     cxMOVER
	MOVX   A,@DPTR
	INC    DPTR
	MOV    R2,DPL
	MOV    R3,DPH
	MOV    DPL,R4
	MOV    DPH,R5
	MOVX   @DPTR,A
	INC    DPTR
	MOV    R4,DPL
	MOV    R5,DPH
	MOV    A,R0
	DEC    R0
	JNZ    xxMOVE
	DEC    R1
	SJMP   xxMOVE

XCDPR01:XCH    A,R0           ; DPTR <-> R01
	XCH    A,DPL
	XCH    A,R0
	XCH    A,R1
	XCH    A,DPH
	XCH    A,R1
	RET

ADDATDP:ADD    A,DPL          ; DPTR = DPTR + A
	MOV    DPL,A
	CLR    A
	ADDC   A,DPH
	MOV    DPH,A
	RET

	END