;********************************************************************
;*                    LCP 4000 - PB_MOVE.ASM                        *
;*                       Pametove presuny                           *
;*                  Stav ke dni 11.11.1995                          *
;*                      (C) Pisoft 1995                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

EXTRN   CODE(cMDPDP,xMDPDPA,xMDPDP)
EXTRN   CODE(ADDATDP)
EXTRN   CODE(cxMOVEt,cxMOVE,cxMOVE1)
EXTRN   CODE(xxMOVEt,xxMOVE,xxMOVE1)
EXTRN   CODE(XCDPR01)
