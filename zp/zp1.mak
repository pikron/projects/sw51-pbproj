#   Project file pro Zapalovani ZAP1
#         (C) Pisoft 1995

zp.obj    : zp.asm pb_ai.h ulan.h pb_iic.h pb_move.h
	a51 zp.asm $(par) debug

zp_ai.obj : zp_ai.asm pb_tty.h
	a51 zp_ai.asm  $(par)

pb_move.obj : pb_move.asm
	a51 pb_move.asm  $(par)

pb_vec.obj: pb_vec.asm
	a51 pb_vec.asm $(par)

ulan.obj     : ulan.asm
	a51    ulan.asm $(par)

pb_iic.obj: pb_iic.asm
	a51 pb_iic.asm $(par)

          : zp.obj
	del zp.

zp.       : zp.obj zp_ai.obj pb_move.obj ulan.obj pb_iic.obj pb_vec.obj
	l51 zp.obj,zp_ai.obj,pb_move.obj,ulan.obj,pb_iic.obj,pb_vec.obj xdata(8800H) ramsize(100h) stack(STACK) ixref

zp1.hex   : zp.
	del  zp1.
        ren zp. zp1.
	ohs51 zp1
	del zp1.

