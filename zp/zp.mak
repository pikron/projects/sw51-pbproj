#   Project file pro Zapalovani ZAP1
#         (C) Pisoft 1995

zp.obj    : zp.asm pb_ai.h ulan.h pb_iic.h pb_move.h
	a51 zp.asm $(par) debug

zp_ai.obj : zp_ai.asm pb_tty.h
	a51 zp_ai.asm  $(par)

pb_move.obj : pb_move.asm
	a51 pb_move.asm  $(par)

pb_vec.obj: pb_vec.asm
	a51 pb_vec.asm $(par)

ulan.obj     : ulan.asm
	a51    ulan.asm $(par)

pb_iic.obj: pb_iic.asm
	a51 pb_iic.asm $(par)

zp.       : zp.obj zp_ai.obj pb_move.obj ulan.obj pb_iic.obj pb_vec.obj
	l51 zp.obj,zp_ai.obj,pb_move.obj,ulan.obj,pb_iic.obj,pb_vec.obj code(9000H) xdata(8800H) ramsize(100h) stack(STACK) ixref

zp.hex    : zp.
	ohs51 zp

	  : zp.hex
	sendhex zp.hex  /p3 /m3 /t2 /g36864 /B9600
#	sendhex tab.hex /q /p3 /m3 /t2
