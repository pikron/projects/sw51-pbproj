$NOMOD51
;********************************************************************
;*                    PODAVAC  - AA_POD.ASM                         *
;*                       Hlavni modul                               *
;*                  Stav ke dni 10.08.2003                          *
;*                      (C) Pisoft 1996-2003                        *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_AL)
$INCLUDE(%INCH_ADR)
;$INCLUDE(%INCH_UF)
$INCLUDE(%INCH_ULAN)
$INCLUDE(%INCH_UL_OI)
$INCLUDE(%INCH_REGS)
$INCLUDE(%INCH_MR_DEFS)
$INCLUDE(%INCH_UI)
$INCLUDE(%INCH_BREAK)
$LIST

EXTRN	CODE(PRINThb,PRINThw,SEL_FNC,INPUThw)
EXTRN	CODE(MONITOR)
EXTRN	CODE(cxMOVE,xMDPDP,ADDATDP)
PUBLIC	INPUTc,KBDBEEP,ERRBEEP,RES_STAR

%IF (0) THEN (
  %DEFINE (IRC_WHEEL) (10)	; Pocet znacek na otacku motoru
)ELSE(
  %DEFINE (IRC_WHEEL) (16)	; Pocet znacek na otacku motoru
)FI

%DEFINE (STP_8_PHASE) (1)	; Osmitaktni rizeni krokace


AA_PO_C SEGMENT CODE
AA_PO_D SEGMENT DATA
AA_PO_B SEGMENT DATA BITADDRESSABLE
AA_PO_X SEGMENT XDATA

RSEG	AA_PO_B

LEB_FLG:DS    1		; Blikani ledek

HW_FLG: DS    1
ITIM_RF BIT   HW_FLG.7
FL_ASH0	BIT   HW_FLG.6
STP_ERR	BIT   HW_FLG.5
SEK_ERR	BIT   HW_FLG.4
STP_WCH	BIT   HW_FLG.3
LEB_PHA	BIT   HW_FLG.2
FL_WINJ	BIT   HW_FLG.1	; Pri davkovani pockej na I_INJECT
FL_DINJ	BIT   HW_FLG.0	; Prisel I_INJECT, bude se davkovat

HW_FLG1:DS    1
FL_LPS1	BIT   HW_FLG1.7	; Kapalina v cidle 1
FL_LPS2	BIT   HW_FLG1.6	; Kapalina v cidle 2
FL_25Hz	BIT   HW_FLG1.5
FL_ENLPS BIT  HW_FLG1.4	; Povoleni rizeni podle cidel
FL_DIPR	BIT   HW_FLG1.3	; Displej pripojen
STP_APD BIT   HW_FLG1.2	; Automaticky power down pro krokac
; Bity 0 a 1 jsou pouzity pro LPS

RSEG	AA_PO_X

TMP:	DS    16

C_R_PER	EQU   30
REF_PER:DS    1

RSEG	AA_PO_C

RES_STAR:
	MOV   IEN0,#0
	MOV   IEN1,#0
	MOV   IP0,#0
	MOV   IP1,#0
	MOV   SP,#80H
	MOV   PCON,#10000000B ; Bd = OSC/12/16/(256-TH1)
	MOV   TM2CON,#10000001B; timer 2 CLK, TR2 disabled, 16 bit OV
	%VECTOR(T2CMP2,I_TIME1); Realny cas z komparatoru CM2
	SETB  ECM2	       ; povoleni casu od preruseni od CM2
	MOV   P4,#0FFH

%IF(1)THEN(
	MOV   DPTR,#8080H
STRT10:	CLR   A
	MOV   R0,A
STRT11:	MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,STRT11
	%WATCHDOG
	MOV   A,DPH
	XRL   A,#HIGH STRT10
	JZ    STRT12
	MOV   A,DPH
	CJNE  A,#0F0H,STRT11
STRT12:
)FI

	CALL  I_TIMRI
	CALL  I_TIMRI
	MOV   CINT25,#10
	CLR   A
	MOV   DPTR,#TIMRI
	MOVX  @DPTR,A
	MOV   DPTR,#TIME
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A

	MOV   HW_FLG,#080H
	MOV   HW_FLG1,#0
	MOV   LEB_FLG,#0

	MOV   A,#018H
	MOV   DPTR,#APWM0
	MOVX  @DPTR,A
	CLR   A
	MOV   DPTR,#TC_ACL
	MOVX  @DPTR,A
	MOV   DPTR,#TMC1_RT
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#TMC1_FLG
	MOVX  @DPTR,A
	MOV   DPTR,#APWM1
	MOVX  @DPTR,A
	MOV   DPTR,#STP_ST
	MOVX  @DPTR,A
	MOV   DPTR,#INV_ST
	MOVX  @DPTR,A
	MOV   DPTR,#SEK_ST
	MOVX  @DPTR,A
	MOV   A,#MSEK_LPS	; Musi se pouzivat cidla
	MOV   DPTR,#SEK_CFG
	MOVX  @DPTR,A
	%LDMXi(SAMPNUM,1)

	%LDMXi(P_VOL1,150)	; Objem sani za 1 cidlo
	%LDMXi(P_INJT,200)	; Doba otoceni ventilu
	%LDMXi(P_IRC_W,%IRC_WHEEL); Pocet impulsu na IRC kolecku

	%LDMXi(LPS_TD1,5)	; Doba pro rozhodnuti o zmene LPS
	%LDMXi(LPS_TD2,5)	; Doba pro rozhodnuti o zmene LPS

	MOV   DPTR,#REG_A+OMR_FLG
	CLR   A
	MOVX  @DPTR,A

	CALL  LCDINST
	JNZ   STRT30
	SETB  FL_DIPR
	CALL  LEDWR
STRT30:

	MOV   A,#7
	CALL  I_U_LAN
	CALL  uL_OINI

	MOV   DPTR,#REF_PER
	MOV   A,#C_R_PER
	MOVX  @DPTR,A

	JMP   L0

INPUTc:	CALL  SCANKEY
	JZ    INPUTc
	RET

; Pipnuti na klavese klavesnice
;KBDBEEP:JMP   KBDSTDB
KBDBEEP:MOV   A,#2
BEEP:	MOV   DPTR,#BEEPTIM
	MOVX  @DPTR,A
	SETB  %BEEP_FL
	MOV   DPTR,#LED       ; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
	MOV   A,R2
	RET
ERRBEEP:MOV   A,#8
	JMP   BEEP

I_U_LAN:PUSH  ACC
;	CLR   A
;	MOV   A,#10	; 9600 pro krystal 18432 kHz
	MOV   A,#5	; 19200 pro krystal 18432 kHz
	MOV   R0,#1
	CALL  uL_FNC	; Rychlost
	POP   ACC
	MOV   R0,#2
	CALL  uL_FNC	; Adresa
	MOV   R2,#0
	MOV   R0,#3
	CALL  uL_FNC	; Delka IB OB
	MOV   R2,#0
	MOV   R0,#4
	CALL  uL_FNC	; Rychle bloky
	MOV   R0,#0
	CALL  uL_FNC	; Start
	RET

; *******************************************************************
;
; Casove preruseni

PUBLIC  KBDTIMR

RSEG	AA_PO_D

;DINT600 EQU   1536 ; Deleni CLK/12 na 600 Hz pro X 11.0592 MHz
DINT600 EQU   2560 ; Deleni CLK/12 na 600 Hz pro X 18.432 MHz
DINT25  EQU   24   ; Delitel EXINT1 na 25 Hz
CINT25: DS    1

RSEG	AA_PO_X

BEEPTIM:DS    1	   ; Timer delky pipani

N_OF_T  EQU   6    ; Pocet timeru
TIMR1:  DS    1    ; Timery jsou decrementovany
TIMR2:  DS    1    ; s frekvenci 25 Hz
TIMR_WAIT:
TIMR3:  DS    1
REF_TIM:DS    1	   ; Refresh timr
KBDTIMR:DS    1
TIMRI:  DS    1
TIME:   DS    2    ; Cas v 0.01 min              =====

RSEG	AA_PO_C

USING   3
I_TIME1:PUSH  ACC	; Cast s pruchodem 600 Hz
	PUSH  PSW
	;SETB  P4.7	;*!!!!!!!!!!!!!!!
	MOV   A,CML2
	ADD   A,#LOW  DINT600
	MOV   CML2,A
	MOV   A,CMH2
	ADDC  A,#HIGH DINT600
	MOV   CMH2,A
	CLR   CMI2
	MOV   PSW,#AR0
	PUSH  B
	PUSH  DPL
	PUSH  DPH

	CLR   FL_25Hz
	DJNZ  CINT25,I_TIM10
	SETB  FL_25Hz
I_TIM10:CALL  DO_REG		; Regulace DC motoru
	CALL  ADC_D		; Cteni AD prevodniku
	CALL  SEK		; Sekvencer davkovani

	JNB   FL_25Hz,I_TIMR1	; Konec casti spruchodem 600 Hz
	MOV   CINT25,#DINT25	; Pruchod s frekvenci 25 Hz

	CALL  ADC_S		; Spusteni cyklu prevodu ADC
	CALL  uL_STR
	CALL  TMC_REG
	%WATCHDOG
	MOV   DPTR,#BEEPTIM
	MOVX  A,@DPTR
	JZ    I_TIM80
	DEC   A
	MOVX  @DPTR,A
	JNZ   I_TIM80
	CLR   %BEEP_FL
	MOV   DPTR,#LED       ; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
I_TIM80:MOV   DPTR,#TIMR1
	MOV   B,#N_OF_T-1
I_TIME2:MOVX  A,@DPTR
	JZ    I_TIME3
	DEC   A
	MOVX  @DPTR,A
I_TIME3:INC   DPTR
	DJNZ  B,I_TIME2
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	JNB   ITIM_RF,I_TIMR1
	JB    ACC.7,I_TIME4
I_TIMR1:POP   DPH
	POP   DPL
	POP   B
	POP   PSW
	POP   ACC
	SETB  EA
I_TIMRI:
	;CLR   P4.7	;*!!!!!!!!!!!!!!!
	RETI

I_TIME4:CLR   ITIM_RF	      ; Pruchod 0.6 sec
;	CALL  I_TIMRI
;	MOV   PSW,#AR0        ; Banka 2
	ADD   A,#15
	MOVX  @DPTR,A

	MOV   A,LEB_FLG
	JBC   LEB_PHA,I_TIM42
	ORL   LED_FLG,A
	SETB  LEB_PHA
	SJMP  I_TIM43
I_TIM42:CPL   A
	ANL   LED_FLG,A
I_TIM43:SETB  ITIM_RF
	JMP   I_TIMR1

; *******************************************************************
; Promenne multiregulatoru

%DEFINE (MR_REG_TYPE) (MR_PID)

EXTRN	CODE(%MR_REG_TYPE)
PUBLIC	MR_BAS

RSEG	AA_PO_B

MR_FLG:	DS    1
MR_FLGA:DS    1

RSEG	AA_PO_D

MR_BAS:	DS    2		; ukazatel na struktury regulatoru

OMR_AIR	EQU   OMR_LEN	;1B ; spodni cast adresy IRC snimace
OMR_APW	EQU   OMR_AIR+1	;1B ; spodni cast adresy PWM vystupu
OMR_VGJ	EQU   OMR_APW+1	;1B ; instrukce JMP
OMR_VG	EQU   OMR_VGJ+1	;2B ; adresa rutiny generatoru, koncit VR_RET
OMR_GST	EQU   OMR_VG+2	;1B ; soucasny stav generovani polohy
OMR_GEP	EQU   OMR_GST+1	;4B ; koncova poloha
OMR_LEN	SET   OMR_GEP+4

RSEG	AA_PO_X

REG_N	EQU   6
REG_LEN	EQU   OMR_LEN
REG_A:	DS    REG_N*REG_LEN+1

OREG_A	EQU   0
OREG_B	EQU   REG_LEN

; *******************************************************************
; Cteni polohy z CF32006

AIR_CF0	XDATA 0FF20H	; Baze prvniho CF32006
AIR_CF1	XDATA 0FF40H	; Baze druheho CF32006

			;	       7   6   5   4   3   2   1   0
AIR_CC0	XDATA 0FF80H    ; Konfigurace KL0 R0  M22 M12 M02 M21 M11 M01
AIR_CC1	XDATA 0FFA0H	; Konfigurace KL1 R1  0   0   0   M23 M13 M03
; mody:	16 bit counter	- 0
;	single IRC	- 1 on raisign Ua1n, 2 on raising Ua2n
;	double IRC	- 3 on Ua1n edges , 4 on Ua2n edges
;	quadruple IRC	- 5 an all edges
;	PWM measure	- 6 Ua1n gate, Ua2n=1 count up on CLK, 0=down
;	frequncy measure- 7 Ua1n frequncy signal, Ua2n gate

CIR_C_N	EQU   11101101B ; zakladni konfigurace
CIR_RES EQU   10111111B ; reset

RSEG	AA_PO_C

CF_INIT:MOV   DPTR,#AIR_CC0
	MOV   A,#CIR_C_N AND CIR_RES
	MOVX  @DPTR,A
	MOV   A,#CIR_C_N
	MOVX  @DPTR,A
	MOV   DPTR,#AIR_CC1
	MOV   A,#CIR_C_N AND CIR_RES
	MOVX  @DPTR,A
	MOV   A,#CIR_C_N
	MOVX  @DPTR,A
	RET


; Nacte polohu do OMR_AP (3B) a vypocita OMR_AS (2B)
IRC_RD:
CF_RD:  MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
IRC_RDP:
CF_RDP: MOV   A,#OMR_AIR
	MOVC  A,@A+DPTR
	MOV   DPL,A
	MOV   DPH,#HIGH AIR_CF0
CF_RDDP:MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A	; R45 = IRC z CF32006
	%MR_OFS2DP(OMR_AP)	; OMR_AP
	MOVX  A,@DPTR
	MOV   R2,A
	MOV   A,R4
	MOVX  @DPTR,A
	CLR   C
	SUBB  A,R2
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   A,R5
	MOVX  @DPTR,A
	SUBB  A,R3
	MOV   R3,A
	INC   DPTR
	JNC   CF_RD4
	CPL   A
CF_RD4:	JNB   ACC.7,CF_RD6	; Prodlouzeni OMR_AP
	JNC   CF_RD5
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	SJMP  CF_RD7
CF_RD5: MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	SJMP  CF_RD7
CF_RD6: MOVX  A,@DPTR
CF_RD7:	MOV   R7,A
	INC   DPTR
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	RET

; Pozadavek na kalibraci polohy na R567
IRC_WR:
CF_WR:	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
IRC_WRP:
CF_WRP: MOV   A,#OMR_AIR
	MOVC  A,@A+DPTR		; OMR_AIR
	MOV   DPL,A
	MOV   DPH,#HIGH AIR_CF0
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	RET

; *******************************************************************
; Vystup energii na motory

APWM0	XDATA 0FFE0H
APWM1	XDATA 0FFE1H

RSEG	AA_PO_C

MR_SENE:MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
MR_SENEP:MOV  A,#OMR_APW
	MOVC  A,@A+DPTR
	MOV   DPL,A
	MOV   DPH,#HIGH APWM0
	MOV   A,R5
	MOVX  @DPTR,A
	RET

; *******************************************************************
; Zakladni smycka multi regulatoru

RSEG	AA_PO_C

DO_REGR:;CLR   P4.7	;*!!!!!!!!!!!!!!!
	RET

; Prochazi vsechny regulatory az do OMR_FLG=0

DO_REG: ;SETB  P4.7	;*!!!!!!!!!!!!!!!
	ANL   MR_FLG,#MMR_ERR
	MOV   MR_BAS,#LOW REG_A
	MOV   MR_BAS+1,#HIGH REG_A
DO_REG1:CLR   F0
	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOVX  A,@DPTR		; OMR_FLG
	JZ    DO_REGR
	MOV   MR_FLGA,A
	JNB   MR_FLGA.BMR_ENI,DO_REG5
	CALL  CF_RD
DO_REG5:JNB   MR_FLGA.BMR_ENG,DO_REG6
	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,#OMR_VGJ
	JMP   @A+DPTR  		; skok na vektor generatoru
DO_REG6:
VR_REG: JNB   MR_FLGA.BMR_ENR,DO_REG9
	CALL  %MR_REG_TYPE
DO_REG7:
VR_REG1:JNB   F0,DO_REG8
	ORL   MR_FLGA,#MMR_ERR
	CLR   A
	MOV   R5,A
	MOV   R4,A
DO_REG8:CALL  MR_SENE
DO_REG9:
VR_RET:	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,MR_FLGA
	ORL   MR_FLG,A
	MOVX  @DPTR,A		; OMR_FLG
	MOV   A,MR_BAS
	ADD   A,#OMR_LEN
	MOV   MR_BAS,A
	JNC   DO_REG1
	INC   MR_BAS+1
	JMP   DO_REG1

MR_ZER:	MOV   DPTR,#REG_A
	%LDR23i (REG_N*REG_LEN)
MR_ZER1:CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R2
	DEC   R2
	JNZ   MR_ZER1
	MOV   A,R3
	DEC   R3
	JNZ   MR_ZER1
	MOV   A,#LOW (AIR_CF0+6)
	MOV   DPTR,#REG_A+OREG_A+OMR_AIR
	MOVX  @DPTR,A
	MOV   A,#LOW (AIR_CF0+4)
	MOV   DPTR,#REG_A+OREG_B+OMR_AIR
	MOVX  @DPTR,A
	MOV   A,#LOW APWM0
	MOV   DPTR,#REG_A+OREG_A+OMR_APW
	MOVX  @DPTR,A
	MOV   A,#LOW APWM1
	MOV   DPTR,#REG_A+OREG_B+OMR_APW
	MOVX  @DPTR,A
	MOV   MR_FLG,#0
	RET

; *******************************************************************
; Generatory pozadovane polohy

; R4567=OMR_RS+OMR_RP , meni R3

MR_GPSC:%MR_OFS2DP(OMR_RS)
	MOVX  A,@DPTR	; OMR_RS
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR	; OMR_RS+1
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR	; OMR_RS+2
	MOV   R3,A
	SJMP  MR_GPS5

; R4567=R45+OMR_RP , meni R3

MR_GPSV:%MR_OFS2DP(OMR_RS)
	MOV   A,R4
	MOVX  @DPTR,A	; OMR_RS
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A	; OMR_RS+1
	INC   DPTR
	MOV   R3,#0
	JNB   ACC.7,MR_GPS2
	DEC   R3
MR_GPS2:MOV   A,R3
	MOVX  @DPTR,A	; OMR_RS+2
MR_GPS5:MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,#OMR_RP
	MOVC  A,@A+DPTR	; OMR_RP
	ADD   A,R4
	MOV   R4,A
	MOV   A,#OMR_RP+1
	MOVC  A,@A+DPTR
	ADDC  A,R5
	MOV   R5,A
	MOV   A,#OMR_RP+2
	MOVC  A,@A+DPTR
	ADDC  A,R3
	MOV   R6,A
	MOV   A,#OMR_RP+3
	MOVC  A,@A+DPTR
	ADDC  A,R3
	MOV   R7,A
	RET

; Provede komparaci R4567 a OMR_GEP

MR_CMEP:MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	CLR   C
	MOV   A,#OMR_GEP
	MOVC  A,@A+DPTR
	SUBB  A,R4
	MOV   B,A
	MOV   A,#OMR_GEP+1
	MOVC  A,@A+DPTR
	SUBB  A,R5
	ORL   B,A
	MOV   A,#OMR_GEP+2
	MOVC  A,@A+DPTR
	SUBB  A,R6
	ORL   B,A
	MOV   A,#OMR_GEP+3
	MOVC  A,@A+DPTR
	SUBB  A,R7
	ORL   B,A
	INC   B
	DJNZ  B,MR_CME3
	RET
MR_CME3:CPL   C
	CPL   A
	ORL   A,#1
	RET

; Nacte do R4567 OMR_RP

MR_RDRP:MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,#OMR_RP
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_RP+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   A,#OMR_RP+2
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   A,#OMR_RP+3
	MOVC  A,@A+DPTR
	MOV   R7,A
	RET

; Ulozi R4567 do OMR_RP

MR_WRRP:%MR_OFS2DP(OMR_RP)
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	RET

; Nacte do R4567 OMR_AP

MR_RDAP:MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   R4,#0
	MOV   A,#OMR_AP+0
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   A,#OMR_AP+1
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   A,#OMR_AP+2
	MOVC  A,@A+DPTR
	MOV   R7,A
	RET

; Inicializuje pohyb konstantni rychlosti na polohu OMR_GEP

CI_GEP:	CALL  MR_RDRP
	CALL  MR_CMEP
	JZ    CI_GEP8
	MOV   C,OV
	XRL   A,PSW
	MOV   C,ACC.7
	SETB  MR_FLGA.BMR_BSY
	MOV   A,#OMR_MS
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_MS+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	JC    CI_GEP4
	CALL  NEGi
CI_GEP4:CALL  MR_GPSV
	%MR_OFS2DP(OMR_VG)	; dale pobezi CD_GEP
	MOV   A,#HIGH CD_GEP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW  CD_GEP
	MOVX  @DPTR,A
	JMP   CD_GEP2
CI_GEP8:JMP   CD_GEPE

; Pohyb konstantni rychlosti

CD_GEP:	JNB   MR_FLGA.BMR_BSY,CD_GEP9
	CALL  MR_GPSC
CD_GEP2:CALL  MR_CMEP
	JZ    CD_GEPE
	MOV   C,OV
	XRL   A,PSW
	XRL   A,R3
	JB    ACC.7,CD_GEP8
CD_GEPE:CLR   MR_FLGA.BMR_BSY
	MOV   A,#OMR_GEP
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_GEP+1
	MOVC  A,@A+DPTR
	MOV   R5,A
	MOV   A,#OMR_GEP+2
	MOVC  A,@A+DPTR
	MOV   R6,A
	MOV   A,#OMR_GEP+3
	MOVC  A,@A+DPTR
	MOV   R7,A
	%MR_OFS2DP(OMR_RS)
	CLR   A
	MOVX  @DPTR,A	; OMR_RS
	INC   DPTR
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	%MR_OFS2DP(OMR_VG); dale pobezi jen regulator
	MOV   A,#HIGH VR_REG
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW  VR_REG
	MOVX  @DPTR,A
CD_GEP8:%MR_OFS2DP(OMR_RP)
	MOV   A,R4	; OMR_RP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
CD_GEP9:JMP   VR_REG

; Kompletni vynulovani regulatoru
CD_HHCL:CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R7,A
CD_HHC0:MOV   DPL,MR_BAS	; vstupni bod
	MOV   DPH,MR_BAS+1
CD_HHC1:INC   DPTR		; vstupni bod
	MOV   R0,#1		; DPTR = OMR_AP
CD_HHC2:MOV   A,R5 		; OMR_AP, OMR_RPI
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A		; OMR_AS, OMR_RS
	INC   DPTR
	MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,CD_HHC3
	MOV   A,R4		; OMR_RP
	MOVX  @DPTR,A
	INC   DPTR
	SJMP  CD_HHC2
CD_HHC3:MOVX  @DPTR,A		; OMR_RS+1
	INC   DPTR
	MOV   A,DPL
	ADD   A,#OMR_FOI-OMR_P
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#0
	MOV   DPH,A
	MOV   R0,#4
	CLR   A
CD_HHC4:MOVX  @DPTR,A		; OMR_FOI, OMR_FOD
	INC   DPTR
	DJNZ  R0,CD_HHC4
	MOV   A,DPL
	ADD   A,#OMR_ERC-OMR_FOI-4
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#0
	MOV   DPH,A
	CLR   A
	MOVX  @DPTR,A		; OMR_ERC
	MOV   A,DPL
	ADD   A,#-OMR_ERC
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#-1
	MOV   DPH,A
	JMP   IRC_WRP

; *******************************************************************
; Rutiny pro ramenko autosampleru

%DEFINE (WITH_ASH_VIP) (1)	; Kontrolovat odkloneni ramenka

ASH_ZF	BIT   P1.0	; Zakladni poloha ramenka nahore
ASH_VIP	BIT   P1.1	; Poloha pro propichnuti (ma vyznam je )

CASH_ZP	EQU   820*%IRC_WHEEL	; Poloha koncove znacky znacky
CASH_HP	EQU   CASH_ZP+4		; Poloha pro vyplach jehly
CASH_HR	EQU   8*%IRC_WHEEL ;(6)	; Pocet IRC otaceni ramenka
CASH_SL	EQU   3*%IRC_WHEEL	; Pomala rychlost
CASH_SH	EQU   45*%IRC_WHEEL	; Vysoka rychlost
CASH_MT	EQU   10*%IRC_WHEEL	; Minimalni poloha pri testu

	; Pozadovana poloha pro dojeti na dno zkumavky
ASH1_BOT SET  REG_A+OREG_A+OMR_GEP+1

; Vraci CY=1 pokud doslo k prejeti hrany

ASH_TZ:	MOV   C,FL_ASH0
	MOV   B.7,C
	CLR   A
	MOV   C,ASH_ZF
	ADDC  A,#0
	MOV   C,ASH_ZF
	ADDC  A,#0
	MOV   C,ASH_ZF
	ADDC  A,#-2
	MOV   FL_ASH0,C
	XRL   A,B
	MOV   C,ACC.7
	CPL   C
	RET

; Nastavi polohu odpovidajici hrane

ASH_ZER:MOV   DPTR,#AIR_CF0+6
	MOV   A,#HIGH CASH_ZP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW CASH_ZP
	MOVX  @DPTR,A
	MOV   DPTR,#AIR_CF0+6
	MOV   A,#HIGH CASH_ZP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW CASH_ZP
	MOVX  @DPTR,A
	%MR_OFS2DP(OMR_AP)	; OMR_AP
	MOV   A,#LOW CASH_ZP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH CASH_ZP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#0
	MOVX  @DPTR,A
	INC   DPTR
	INC   DPTR
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW CASH_ZP	; OMR_RP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH CASH_ZP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#0
	MOVX  @DPTR,A
	RET

%IF(%WITH_ASH_VIP)THEN(
; Nastavi CY=1, pokud je aktivni vstup ASH_VIP
ASH_TVIP:
	CLR   A
	MOV   C,ASH_VIP
	ADDC  A,#0
	MOV   C,ASH_VIP
	ADDC  A,#0
	MOV   C,ASH_VIP
	ADDC  A,#-2
	RET
)FI

; Generovani polohy a regulace ramenka autosampleru

VG_ASH: JB    MR_FLGA.BMR_ERR,ASH_090
	CLR   F0
	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,#OMR_GST
	MOVC  A,@A+DPTR
	MOV   R0,A
	ANL   A,#0F0H
	JNZ   ASH_100
ASH_090:CLR   MR_FLGA.BMR_BSY
	MOV   R4,#00H
	MOV   R5,#1CH		; Stabilni pridrzna energie
	JMP   ASH_990

; Kalibrace nulove polohy ramenka
ASH_100:CJNE  A,#10H,ASH_200
	XRL   A,R0
	JNZ   ASH_110
	SETB  MR_FLGA.BMR_BSY
	CALL  ASH_ZER
	SETB  FL_ASH0
	MOV   R0,#11H
	%LDR45i(-CASH_SL)
	JMP   ASH_880
ASH_110:DJNZ  ACC,ASH_120
	CALL  ASH_TZ
	JNC   ASH_111
	MOV   R0,#12H
	%LDR45i(CASH_SL*2)
	JMP   ASH_880
ASH_111:CALL  MR_GPSC
	MOV   A,R5
	SUBB  A,#LOW  (CASH_ZP-CASH_MT)
	MOV   A,R6
	SUBB  A,#HIGH (CASH_ZP-CASH_MT)
	JNC   ASH_127
ASH_10E:SETB  MR_FLGA.BMR_ERR
	JMP   ASH_090
ASH_120:DJNZ  ACC,ASH_130
	CALL  ASH_TZ
	JNC   ASH_122
	CALL  ASH_ZER
ASH_10S:MOV   R0,#0H
	%LDR45i(0)
	JMP   ASH_880
ASH_122:CALL  MR_GPSC
	MOV   A,R5
	SUBB  A,#LOW  (2*CASH_ZP)
	MOV   A,R6
	SUBB  A,#HIGH (2*CASH_ZP)
	JNC   ASH_10E
ASH_127:JMP   ASH_870
ASH_130:JMP   ASH_10E

; Pohyb ramenka nahoru
ASH_200:CJNE  A,#20H,ASH_300
	XRL   A,R0
	JNZ   ASH_210
	SETB  MR_FLGA.BMR_BSY
	CALL  MR_RDAP
	CALL  MR_WRRP
	CALL  ASH_TZ
	JB    FL_ASH0,ASH_229
	MOV   R0,#21H
	%LDR45i(CASH_SH)
	JMP   ASH_880
ASH_210:DJNZ  ACC,ASH_220
	CALL  ASH_TZ
	JB    FL_ASH0,ASH_10S
	CALL  MR_GPSC
	MOV   A,R5
	SUBB  A,#LOW  (CASH_ZP-CASH_HR)
	MOV   A,R6
	SUBB  A,#HIGH (CASH_ZP-CASH_HR)
	JC    ASH_227
	MOV   R0,#22H
	%LDR45i(CASH_SL)
	JMP   ASH_880
ASH_220:DJNZ  ACC,ASH_230
	CALL  ASH_TZ
	JB    FL_ASH0,ASH_228
	CALL  MR_GPSC
	MOV   A,R5
	SUBB  A,#LOW  (CASH_ZP+20)
	MOV   A,R6
	SUBB  A,#HIGH (CASH_ZP+20)
	JNC   ASH_20E
ASH_227:JMP   ASH_870
ASH_228:CALL  ASH_ZER
ASH_229:MOV   R0,#40H
	%LDR45i(0)
	JMP   ASH_880
ASH_230:
ASH_20E:SETB  MR_FLGA.BMR_ERR
	JMP   ASH_090

; Pohyb ramenka dolu
ASH_300:CJNE  A,#30H,ASH_400
	XRL   A,R0
	JNZ   ASH_310
	SETB  MR_FLGA.BMR_BSY
	CALL  MR_RDAP
	CALL  MR_WRRP
	CALL  ASH_TZ
	JNB   FL_ASH0,ASH_30E
	MOV   R0,#31H
	%LDR45i(-CASH_SL/4)
	JMP   ASH_880
ASH_310:DJNZ  ACC,ASH_320
	CALL  ASH_TZ
	JNC   ASH_314
;	CALL  ASH_ZER		; !!!!!!!!!!!!!!!!
	MOV   R0,#32H
	%LDR45i(-CASH_SL)
	JMP   ASH_880
ASH_314:CALL  MR_GPSC
	MOV   A,R5
	SUBB  A,#LOW  (CASH_ZP-CASH_MT)
	MOV   A,R6
	SUBB  A,#HIGH (CASH_ZP-CASH_MT)
	JC    ASH_30E
ASH_317:JMP   ASH_870
ASH_320:DJNZ  ACC,ASH_330
	CALL  MR_GPSC
	MOV   A,R5
	SUBB  A,#LOW  (CASH_ZP-CASH_HR)
	MOV   A,R6
	SUBB  A,#HIGH (CASH_ZP-CASH_HR)
	JNC   ASH_317
    %IF(%WITH_ASH_VIP)THEN(
	CALL  ASH_TVIP
	JNC   ASH_30E
    )FI
	MOV   R0,#33H
	%LDR45i(-CASH_SH)
	JMP   ASH_880
ASH_330:DJNZ  ACC,ASH_340
	CALL  MR_GPSC
	CALL  MR_CMEP
	JNC   ASH_317
	MOV   R0,#34H
	%LDR45i(0)
	JMP   ASH_880
ASH_340:DJNZ  ACC,ASH_350
	CLR   MR_FLGA.BMR_BSY
	JMP   ASH_900
ASH_350:
ASH_30E:SETB  MR_FLGA.BMR_ERR
	JMP   ASH_090

; Pridrzeni ramenka v horni poloze
ASH_400:CJNE  A,#40H,ASH_500
	XRL   A,R0
	JNZ   ASH_410
	MOV   R0,#41H
	%LDR45i(CASH_SL/4)
	JMP   ASH_880
ASH_410:DJNZ  ACC,ASH_420
	CALL  ASH_TZ
;	JNB   FL_ASH0,ASH_40E	; !!!!!!!!!
	CALL  MR_GPSC
	MOV   A,R5
	SUBB  A,#LOW  (CASH_HP+2)
	MOV   A,R6
	SUBB  A,#HIGH (CASH_HP+2)
	JC    ASH_415
	CLR   MR_FLGA.BMR_BSY
	MOV   R0,#42H
	%LDR45i(0)
	JMP   ASH_880
ASH_415:JMP   ASH_870
ASH_420:ADD   A,#-10
	JC    ASH_430
	%MR_OFS2DP(OMR_GST)
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	JMP   ASH_435
ASH_430:JNZ   ASH_440
	CALL  ASH_TZ
	JC    ASH_431		; Chyba az podruhe
	JNB   FL_ASH0,ASH_40E	; !!!!!!!!!!!!!!!!
ASH_431:MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	CLR   C
	MOV   A,#OMR_AP
	MOVC  A,@A+DPTR
	SUBB  A,#LOW  (CASH_HP)
	MOV   R4,A
	MOV   A,#OMR_AP+1
	MOVC  A,@A+DPTR
	SUBB  A,#HIGH (CASH_HP)
	MOV   R5,A
	JC    ASH_435
	ORL   A,R4
	JZ    ASH_434
	MOV   R4,#00H
	MOV   R5,#1CH		; Nizsi energie
	JMP   ASH_990
ASH_434:MOV   R4,#00H
	MOV   R5,#1CH		; Stabilni energie
	JMP   ASH_990
ASH_435:MOV   R4,#00H
	MOV   R5,#28H		; Vyssi energie
	JMP   ASH_990

ASH_440:
ASH_40E:SETB  MR_FLGA.BMR_ERR
	JMP   ASH_090

ASH_500:
	JMP   ASH_990

ASH_870:CALL MR_WRRP		; Nova pozadovana poloha R4567
	JMP   ASH_900
ASH_880:%MR_OFS2DP(OMR_RS)	; Nova rychlost R45 a stav R0
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   R1,#0
	JNB   ACC.7,ASH_881
	DEC   R1
ASH_881:MOV   A,R1
	MOVX  @DPTR,A
ASH_890:%MR_OFS2DP(OMR_GST)	; Prechod na dalsi stav R0
	MOV   A,R0
	MOVX  @DPTR,A
ASH_900:CALL  %MR_REG_TYPE
	JNB   FL_ASH0,ASH_990
	MOV   A,R5
	JB    ACC.7,ASH_990
	ADD   A,#-030H
	JNC   ASH_990
	MOV   R5,#030H
ASH_990:JMP   VR_REG1

; *******************************************************************
; Nastaveni pro AAA_POD

MR_INIS:CALL  MR_ZER
	%LDR45i (REG_A)
	%LDR23i (STDR_A)
	%LDR01i (STDR_AE-STDR_A)
	CALL  cxMOVE
	MOV   A,#MMR_ENI 		; OR MMR_ENR
	MOV   DPTR,#REG_A+OREG_A+OMR_FLG
	MOVX  @DPTR,A
	MOV   DPTR,#REG_A+OREG_B+OMR_FLG
	MOVX  @DPTR,A
	RET

STDR_A:	DB    0, 0,0,0, 0,0
	DB    0
	DB    0,0   ; 	(CASH_ZP)	; RP
	DB    0
	DB    0,0,0			; RS
	DB    080H,0, 002H,0, 0FFH,0	; P I D
	DB    000H,0, 000H,0, 0,060H	; 1 2 ME
	DB    080H,0, 010H,0		; MS MA
	DB    0,0,0,0,0,0,0
	DB    LOW (AIR_CF0+6),LOW APWM0
	DB    2				; JMP
	DW    VG_ASH
	DB    10			; GST = inicializace
	DB    0				; GEP
	%W    (1250)
	DB    0

	DS    STDR_A+REG_LEN-$
	DB    0, 0,0,0, 0,0, 0,0,0,0, 0,0,0
	DB    014H,0, 002H,0, 070H,0	; P I D
	DB    000H,0, 000H,0, 0,060H	; 1 2 ME
	%W    (80)			; MS
	%W    (010H)			; MA
	DB    0,0,0,0,0,0,0
	DB    LOW (AIR_CF0+4),LOW APWM1
	DB    2				; JMP
	DW    VR_REG
STDR_AE:

; pro regulator R1 vypocte adresu parametru v ACC

MR_GPA1:ADD   A,#LOW  REG_A
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH REG_A
	MOV   DPH,A
	MOV   A,R1
	MOV   B,#REG_LEN
	MUL   AB
	ADD   A,DPL
	MOV   DPL,A
	MOV   A,B
	ADDC  A,DPH
	MOV   DPH,A
	RET

TM_GEP: MOV   DPTR,#TM_GEPT
	CALL  cPRINT
	MOV   R6,#8
	MOV   R7,#0C0H
	CALL  INPUTi
	JB    F0,GO_GEPR
	MOV   A,R5
	MOV   R6,A
	MOV   A,R4
	MOV   R5,A
	CLR   A
	MOV   R4,A
	MOV   R7,A
	MOV   R1,#0		; motor A

; Najede motorem R1 na polohu R4567
GO_GEP:	MOV   A,#OMR_FLG	; odpojit generator
	CALL  MR_GPA1
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	JB    ACC.7,GO_GEPE
	ANL   A,#NOT MMR_ENG
	ORL   A,#080H
	MOVX  @DPTR,A
	MOV   EA,C
	MOV   A,#OMR_GEP	; koncova poloha OMR_GPE
	CALL  MR_GPA1
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	%LDR45i(CI_GEP)
; Nastavi vektor a spusti regulator R1
GO_VG:  MOV   A,#OMR_VGJ
	CALL  MR_GPA1
	MOV   A,#2
	MOVX  @DPTR,A           ; instrukce LJMP
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A           ; OMR_VG
	INC   DPTR
	MOV   A,R4
	MOVX  @DPTR,A
	MOV   A,#OMR_FLG        ; spusteni rizeni
	CALL  MR_GPA1
        MOVX  A,@DPTR
        ANL   A,#MMR_DBG
        ORL   A,#MMR_ENI OR MMR_ENR OR MMR_ENG
        MOVX  @DPTR,A
        SETB  MR_FLG.BMR_BSY
; !!!!  CALL  GO_NOTIFY         ; upozorneni na start pohybu
GO_GEPR:RET
GO_GEPE:MOV   EA,C
        SETB  F0
	RET

; Priprava na zmenu generatoru regulatoru R1
; vraci:  ACC = [DPTR]  .. OMR_FLG
;         F0=1          .. pokud je reentrance zmeny
GO_PRPC:MOV   A,R1              ; odpojit generator
        MOV   B,#REG_LEN
        MUL   AB
        ADD   A,#LOW  REG_A
        MOV   DPL,A
        MOV   A,B
        ADDC  A,#HIGH REG_A
        MOV   DPH,A
        MOV   C,EA
        CLR   EA
        MOVX  A,@DPTR
        JNB   ACC.7,GO_PRP1
        MOV   EA,C
        SETB  F0
        RET
GO_PRP1:ANL   A,#NOT MMR_ENG
	ORL   A,#080H
        MOVX  @DPTR,A
        MOV   EA,C
        CLR   F0
        RET

; Vynuluje polohu regulatoru R1
CLR_GEP:MOV   A,#OMR_FLG        ; odpojit generator
        CALL  MR_GPA1
        MOV   C,EA
        CLR   EA
        MOVX  A,@DPTR
        JB    ACC.7,GO_GEPE
        ANL   A,#NOT (MMR_ENG OR MMR_ENI OR MMR_ENR)
        ORL   A,#080H
        MOVX  @DPTR,A
        MOV   EA,C
        CLR   A
        MOV   R4,A
        MOV   R5,A
	MOV   R6,A
        MOV   R7,A
        MOV   A,#0
        CALL  MR_GPA1
        CALL  CD_HHC1           ; nulovani polohy
        CLR   A                 ; nulova vystupni energie
        MOV   R4,A
        MOV   R5,A
        CALL  MR_GPA1
        CALL  MR_SENEP
        MOV   A,#OMR_FLG        ; spusteni odmeru IRC
        CALL  MR_GPA1
	MOV   A,#MMR_ENI
	MOVX  @DPTR,A
	RET

; Zastavi regulator R1
STP_GEP:MOV   A,#OMR_FLG	; odpojit generator
	CALL  MR_GPA1
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	JB    ACC.7,GO_GEPE
	ANL   A,#NOT MMR_ENG
	ORL   A,#080H
	MOVX  @DPTR,A
	MOV   EA,C
	MOV   A,#OMR_RS
	CALL  MR_GPA1
	CLR   A
	MOV   R0,#OMR_P-OMR_RS
STP_GE1:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,STP_GE1
	MOV   A,#OMR_FLG
	CALL  MR_GPA1
	MOVX  A,@DPTR
	ANL   A,#NOT (080H OR MMR_BSY)
	MOVX  @DPTR,A
	RET

; -----------------------------------
; Ramenko

TM_ASH_UP:
	MOV   R2,#20H
	SJMP  TM_ASH

TM_ASH_DOWN:
	MOV   R2,#30H
	MOV   A,P5
	ANL   A,#STP_MSA
	JZ    TM_ASH
	RET

TM_ASH_HOME:
	MOV   R2,#10H
	SJMP  TM_ASH

TM_ASH:	MOV   DPTR,#REG_A+OREG_A+OMR_FLG
	MOV   A,#80H
	MOVX  @DPTR,A
	MOV   DPTR,#REG_A+OREG_A+OMR_VG
	MOV   A,#HIGH VG_ASH
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#LOW  VG_ASH
	MOVX  @DPTR,A
	MOV   DPTR,#REG_A+OREG_A+OMR_GST
	MOV   A,R2
	MOVX  @DPTR,A
	MOV   DPTR,#REG_A+OREG_A+OMR_FLG
	MOV   A,#MMR_ENI OR MMR_ENR OR MMR_ENG
	MOVX  @DPTR,A
	RET


TM_GEPT:DB    LCD_CLR,'Kam :',0

; -----------------------------------
; Peristaltika

RSEG    AA_PO_X

P_IRC_W:DS    2         ; Pocet impulsu na IRC kolecku

RSEG    AA_PO_C

; Odcerpani objemu v R45 rychlosti R23
GO_PER: MOV   DPTR,#P_IRC_W
	MOVX  A,@DPTR
	MOV   R7,A
	JNB   ACC.7,GO_PER1
	CPL   A
	INC   A
	MOV   R7,A
	CLR   C
	CLR   A
	SUBB  A,R4
	MOV   R4,A
	CLR   A
	SUBB  A,R5
	MOV   R5,A
GO_PER1:MOV   A,#OMR_FLG        ; odpojit generator
	CALL  MR_GPA1
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	JB    ACC.7,GO_PERE
	ANL   A,#NOT (MMR_ENG OR MMR_ENI OR MMR_ENR)
	ORL   A,#080H
	MOVX  @DPTR,A
	MOV   EA,C
	MOV   A,DPL             ; OMR_GEP
	ADD   A,#LOW(OMR_GEP-OMR_FLG)
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#HIGH(OMR_GEP-OMR_FLG)
	MOV   DPH,A
	CLR   A                 ; koncova poloha OMR_GEP
	MOVX  @DPTR,A           ; bude R45*R7
	INC   DPTR
	MOV   A,R4
	MOV   B,R7
	MUL   AB
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,B
	XCH   A,R5
	MOV   B,R7
	MOV   R0,B
	JB    ACC.7,GO_PER2
	MOV   R0,#0
GO_PER2:MUL   AB
	ADD   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,B
	ADDC  A,#0
	SUBB  A,R0
	MOVX  @DPTR,A
	MOV   A,DPL             ; OMR_MS
	ADD   A,#LOW(OMR_MS-OMR_GEP-3)
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#HIGH(OMR_MS-OMR_GEP-3)
	MOV   DPH,A
	MOV   A,R2              ; maximalni rychlost OMR_MS
	MOV   B,R7              ; bude R23*R7
	MUL   AB
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,B
	XCH   A,R3
	MOV   B,R7
	MUL   AB
	ADD   A,R3
	MOVX  @DPTR,A
	CLR   A                 ; nulovani polohy
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R7,A
	MOV   A,#0
	CALL  MR_GPA1
	CALL  CD_HHC1
	%LDR45i(CI_GEP)         ; start generatoru a regulatoru
	JMP   GO_VG
GO_PERE:MOV   EA,C
	SETB  F0
	RET

; *******************************************************************
; Krokovy motorek

; Cidla jsou na brane P5 a pri zarezu je na nich nizka uroven
STP_BSB	EQU   7		; 1.
STP_BSA	EQU   6		; 2.
STP_BSC	EQU   5		; 3.
STP_MSB	EQU   1 SHL STP_BSB
STP_MSA	EQU   1 SHL STP_BSA
STP_MSC	EQU   1 SHL STP_BSC

M_STP	EQU   1111B

RSEG	AA_PO_X

STP_ST:	DS    1		; stav automatu krokace
SAMPNUM:DS    2		; cislo zkumavky k najeti
STP_WT:	DS    1		; typ kotouce
STP_WN:	DS    1		; cislo kotouce
STP_RP:	DS    1		; pozadovana poloha
STP_AP:	DS    1		; aktualni poloha
STP_FRC:DS    1		; delicka frekvence
STP_SPD:DS    1		; rychlost
STP_PHA:DS    1		; faze krokace
STP_PDCNT:DS  2		; citac pro automaticky power down

STP_TMP:DS    1		; !!!!!!!!!!

RSEG	AA_PO_C

STP:	MOV   DPTR,#STP_ST
	MOVX  A,@DPTR
	JNZ   STP_004
	JNB   STP_APD,STP_003
	MOV   DPTR,#STP_PDCNT	; Automaticky power down
	MOVX  A,@DPTR
	JNZ   STP_002
	DEC   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	JNZ   STP_002
	ORL   P4,#M_STP		; Uvolneni krokace
	CLR   STP_APD
	RET
STP_002:DEC   A
	MOVX  @DPTR,A
STP_003:RET
STP_004:CLR   STP_APD
	JB    STP_ERR,STP_003
	MOV   R7,A
	MOV   DPTR,#STP_FRC
	MOVX  A,@DPTR
	JZ    STP_005
	DEC   A
	MOVX  @DPTR,A
	MOV   A,#080H
	RET
STP_20V:JMP   STP_200
STP_005:MOV   A,R7
	ANL   A,#0F0H

	CJNE  A,#10H,STP_20V
	XRL   A,R7
	JNZ   STP_110
	CLR   A				; Rozbeh motoru
	MOV   DPTR,#STP_SPD
	MOVX  @DPTR,A
	MOV   DPTR,#STP_AP
	MOVX  @DPTR,A
	MOV   DPTR,#STP_RP
	MOV   A,#250
	MOVX  @DPTR,A
	CALL  STP_GEO
	MOV   DPTR,#STP_FRC		; Doba na sfazovani motoru
	MOV   A,#240
	MOVX  @DPTR,A
	SJMP  STP_NST
STP_110:DJNZ  ACC,STP_120
	CALL  STP_GS
	JZ    STP_10E
	CJNE  R2,#20,STP_112		; Prvnich 20 kroku rozjezd
STP_112:JC    STP_114
	MOV   A,P5
	CPL   A
	ANL   A,#STP_MSA OR STP_MSB	; Tma na cidlech
	JZ    STP_NST
STP_114:RET
STP_120:DJNZ  ACC,STP_130
	MOV   A,P5
	ANL   A,#STP_MSA OR STP_MSB
	JNZ   STP_121
	MOV   DPTR,#STP_PHA
	MOVX  A,@DPTR		; Znacka musi byt na fazi 0
    %IF(NOT %STP_8_PHASE)THEN(
	ANL   A,#3
    )ELSE(
	ANL   A,#7
    )FI
	JZ    STP_122
STP_121:CALL  STP_GS			; Neni znacka
	JNZ   STP_114
STP_10E:SETB  STP_ERR
	ORL   P4,#M_STP
	CLR   A
	RET
STP_122:MOV   DPTR,#STP_AP		; Nalezena pocatecni znacka
	MOV   A,#0		; Offset pocatku
	MOVX  @DPTR,A
	MOV   DPTR,#STP_RP
	MOV   A,#50
	MOVX  @DPTR,A
	CALL  STP_GS
STP_NST:MOV   A,R7
	INC   A
	MOV   DPTR,#STP_ST
	MOVX  @DPTR,A
STP_10R:RET
STP_130:DJNZ  ACC,STP_140
	CALL  STP_GS
	JZ    STP_10E
	MOV   A,P5
	CPL   A
	ANL   A,#STP_MSA OR STP_MSB
	JZ    STP_NST
	RET
STP_140:DJNZ  ACC,STP_150
	MOV   A,P5
	ANL   A,#STP_MSA
	JZ    STP_145
	CALL  STP_GS
	JZ    STP_10E
	RET
STP_145:MOV   DPTR,#STP_AP		; Urceni typu kotouce
	MOVX  A,@DPTR
	CALL  STP_SWT
	JZ    STP_10E
	MOV   B,A
	MOV   DPTR,#SAMPNUM
	MOVX  A,@DPTR
	JZ    STP_147
	DEC   A
STP_147:MUL   AB
	ADD   A,#40
	MOV   DPTR,#STP_RP
	MOVX  @DPTR,A
	CALL  STP_GS
STP_NSV:JMP   STP_NST
STP_150:DJNZ  ACC,STP_160
	CALL  STP_GS
	JZ    STP_155
	RET
STP_155:MOV   DPTR,#STP_FRC
	MOV   A,#100
	MOVX  @DPTR,A
	JMP   STP_NSV
STP_160:DJNZ  ACC,STP_170
	MOV   A,#20H
	MOV   DPTR,#STP_ST
	MOVX  @DPTR,A
	RET
STP_170:

	JMP   STP_10E

STP_200:CJNE  A,#20H,STP_300
	CLR   A
	RET

STP_300:RET

STP_OFF:CLR   A
	MOV   DPTR,#STP_ST
	MOVX  @DPTR,A
	ORL   P4,#M_STP
	CLR   STP_ERR
	CLR   STP_APD
	RET


; Krokuje dokud STP_AP rovno STP_RP
; =================================
;stara se o rychlosti rozjezdu a brzdeni STP_SPD a STP_FRC
;vraci: R2 .. STP_AP
;	ACC = 0 .. konec pohybu
;rusi:	R1
STP_GS:
    %IF(%STP_8_PHASE)THEN(
	MOV   DPTR,#STP_PHA
	MOVX  A,@DPTR
	JNB   ACC.0,STP_GS0
	MOV   DPTR,#STP_AP
	MOVX  A,@DPTR
	MOV   R2,A
	MOV   DPTR,#STP_SPD
	MOVX  A,@DPTR
	JMP   STP_GS9
STP_GS0:
    )FI
	MOV   DPTR,#STP_RP
	MOVX  A,@DPTR
	MOV   R1,A
	MOV   DPTR,#STP_AP
	MOVX  A,@DPTR
	MOV   R2,A		; R2=STP_AP
	XCH   A,R1
	CLR   C
	SUBB  A,R1
	JZ    STP_GSR
	DEC   A
	XCH   A,R1		; R1=STP_RP-STP_AP-1
	INC   A
	MOVX  @DPTR,A		; STP_AP+=1
STP_GS1:MOV   DPTR,#STP_SPD
	MOVX  A,@DPTR
	SUBB  A,R1
	JC    STP_GS3
	MOV   A,R1
	SJMP  STP_GS8
STP_GS3:MOVX  A,@DPTR
	INC   A
	CJNE  A,#STP_PRN,STP_GS8
	DEC   A
STP_GS8:MOVX  @DPTR,A		; STP_SPD
STP_GS9:ADD   A,#STP_PRT-STP_Gb1
	MOVC  A,@A+PC
STP_Gb1:MOV   DPTR,#STP_FRC
	MOVX  @DPTR,A
STP_GEO:MOV   DPTR,#STP_PHA
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
STP_GEN:CLR   STP_APD
    %IF(NOT %STP_8_PHASE)THEN(
	ANL   A,#3
    )ELSE(
	ANL   A,#7
    )FI
	ADD   A,#STP_TAB-STP_Gb2
	MOVC  A,@A+PC
STP_Gb2:ORL   P4,#M_STP
	ANL   P4,A
STP_GSR:RET

%IF(NOT %STP_8_PHASE)THEN(
STP_TAB:DB    11110011B
	DB    11111001B
	DB    11111100B
	DB    11110110B

	DB    11110111B
	DB    11111011B
	DB    11111101B
	DB    11111110B
)ELSE(
STP_TAB:DB    11110011B
	DB    11111011B
	DB    11111001B
	DB    11111101B
	DB    11111100B
	DB    11111110B
	DB    11110110B
	DB    11110111B
)FI

STP_PRT:
    %IF(NOT %STP_8_PHASE)THEN(
	DB   200
    )FI
	DB    80
	DB    40
	DB    20
	DB    10
	DB    7
	DB    5
    %IF(%STP_8_PHASE)THEN(
	DB    3
	DB    2
    )FI
STP_PRN	SET   $-STP_PRT

; Try find wheel type  8, 5, (zatim ne 4 )
STP_SWT:MOV   DPTR,#STP_TMP	; !!!!!!!!!!
	MOVX  @DPTR,A		; !!!!!!!!!!!
	MOV   R0,#1
	ADD   A,#-10
	JC    STP_SWE
	ADD   A,#-7+10
	JC    STP_SW1		; 7 az 9
	INC   R0
	ADD   A,#-7+7
	JC    STP_SWE
	ADD   A,#-4+7
	JC    STP_SW1		; 4 az 6
STP_SWE:CLR   A
	MOV   DPTR,#STP_WT
	MOVX  @DPTR,A
	RET
STP_SW1:MOV   DPTR,#STP_WT
	MOVX  A,@DPTR
	XRL   A,R0
	JZ    STP_SW2
	SETB  STP_WCH
STP_SW2:MOV   A,R0
	MOVX  @DPTR,A
STP_GWD:MOV   DPTR,#STP_WT
	MOVX  A,@DPTR
	ANL   A,#3
	ADD   A,#STP_WTT-STP_GW8
	MOVC  A,@A+PC
STP_GW8:RET
STP_WTT:DB    0, 8, 5, 0

; Cteni optocidel polohy kotouce
STP_PSR:CLR   A
	MOV   R5,A
	MOV   B,P5
	MOV   C,B.STP_BSC
	RLC   A
	MOV   C,B.STP_BSB
	RLC   A
	MOV   C,B.STP_BSA
	RLC   A
	MOV   R4,A
	MOV   A,#1
	RET

TM_STP:	MOV   DPTR,#TM_STPT
	CALL  cPRINT
	MOV   R6,#8
	MOV   R7,#0C0H
	CALL  INPUTi
	JB    F0,TM_STPR
TM_STP1:MOV   A,R4
	MOV   DPTR,#SAMPNUM
	MOVX  @DPTR,A
	JNZ   TM_STP2
	JMP   STP_OFF
TM_STP2:MOV   A,#10H
	MOV   DPTR,#STP_ST
	MOVX  @DPTR,A
TM_STPR:RET

TM_STPT:DB    LCD_CLR,'Sample :',0

TM_STPSER:
	MOV   DPTR,#STP_ST
	MOVX  A,@DPTR
	JNZ   TM_STPSER2
	DJNZ  R4,TM_STPSER1
	SJMP  TM_SUP
TM_STPSER1:
	DJNZ  R4,TM_STPSER2
	SJMP  TM_SDO
TM_STPSER2:
	RET

TM_SUP:	MOV   DPTR,#STP_AP
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	MOV   DPTR,#STP_PHA
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	JMP   STP_GEN

TM_SDO:	MOV   DPTR,#STP_AP
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	MOV   DPTR,#STP_PHA
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	JMP   STP_GEN

; Drzeni krokace na fazi 0
TM_STPHLD:CLR A
	MOV   DPTR,#STP_PHA
	MOVX  @DPTR,A
	JMP   STP_GEN

; Pridrzeni kotouce pro obsluhu
TM_STPHLDT:
	%LDR45i(600*10)
	CLR   EA
	MOV   DPTR,#STP_ST
	MOVX  A,@DPTR
	JNZ   TM_STPHLDT9
    %IF(0)THEN(
	JB    MR_FLG.BMR_BSY,TM_STPHLDT9
	MOV   DPTR,#SEK_ST
	MOVX  A,@DPTR
	JNZ   TM_STPHLDT9
    )FI
	MOV   DPTR,#STP_PDCNT
	MOV   A,R4
	MOVX  @DPTR,A		; Doba pridrzeni
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   DPTR,#STP_PHA
	MOVX  A,@DPTR
	CALL  STP_GEN
	SETB  STP_APD
TM_STPHLDT9:
	SETB  EA
	RET

; *******************************************************************
; System prevodniku

RSEG	AA_PO_X

ADC0:	DS    2
ADC1:	DS    2
ADC2:	DS    2
ADC3:	DS    2
ADC4:	DS    2
ADC5:	DS    2
ADC6:	DS    2
ADC7:	DS    2

RSEG	AA_PO_C

ADC_D:	MOV   B,ADCON
	JNB   B.4,ADC_DR
	MOV   A,B
	ANL   A,#7
	RL    A
	ADD   A,#LOW  ADC0
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH ADC0
	MOV   DPH,A
	MOV   A,B
	ANL   A,#0C0H
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,ADCH
	MOVX  @DPTR,A
	MOV   A,B
	ANL   A,#7
	INC   A
	JNB   ACC.3,ADC_S2
	ANL   ADCON,#NOT 10H
ADC_DR:	RET

ADC_S:  CLR   A
	MOV   B,ADCON
	JB    B.3,ADC_DR
ADC_S2:	ANL   A,#07H
	MOV   ADCON,A
	SETB  ACC.3
	MOV   ADCON,A
	RET

; *******************************************************************
; Davkovaci ventil

INV_P1	BIT   P3.4
INV_P2	BIT   P1.4
INV_E	BIT   P4.4

RSEG	AA_PO_X

INV_ST:	DS    1		; pozadovany a skutecny stav ventilu
BINV_P1 EQU   0		; aktualni poloha 1
BINV_P2 EQU   1		; aktualni poloha 2
BINV_SF EQU   2		; filtr senzoru
BINV_IP EQU   3		; v pohybu
BINV_G1 EQU   4		; pozadavek na polohu 1
BINV_G2 EQU   5		; pozadavek na polohu 2
BINV_ERR EQU  7		; chyba

INV_TIM:DS    1
INV_TIML EQU  50	; casovy limit pohybu v 1/25 s

RSEG	AA_PO_C

; Prikaz pro pohyb ventilu do PO1 a PO2
INV_GP1:MOV   R3,#(1 SHL BINV_IP)OR(1 SHL BINV_G1)
	SJMP  INV_G01
; Druha poloha
INV_GP2:MOV   R3,#(1 SHL BINV_IP)OR(1 SHL BINV_G2)
INV_G01:MOV   DPTR,#INV_TIM
	MOV   A,#INV_TIML
	MOVX  @DPTR,A
	MOV   A,R3
	MOV   DPTR,#INV_ST
	MOVX  @DPTR,A		; prikaz pro ventil
INV_G05:RET

; Rizeni ventilu podle stavu INV_ST
INV_G:	MOV   DPTR,#INV_ST
	MOVX  A,@DPTR
	JB    ACC.BINV_IP,INV_G08
	CLR   C
	CLR   A
	RET
INV_G08:JB    ACC.BINV_G1,INV_G10
	JB    ACC.BINV_G2,INV_G20
	RET
INV_G10:JB    INV_P1,INV_G12	; Presun do polohy 1
	CLR   INV_E
	CLR   ACC.BINV_SF
	MOVX  @DPTR,A
	JB    FL_25Hz,INV_G40
	RET
INV_G12:JB    INV_P2,INV_G40
	JB    ACC.BINV_SF,INV_G14
	SETB  ACC.BINV_SF
	MOVX  @DPTR,A
	RET
INV_G14:SETB  INV_E
	ANL   A,#0
	ORL   A,#(1 SHL BINV_P1)
	MOVX  @DPTR,A
	CLR   A
	CLR   C
	RET
INV_G20:JB    INV_P2,INV_G22	; Presun do polohy 2
	CLR   INV_E
	CLR   ACC.BINV_SF
	MOVX  @DPTR,A
	JB    FL_25Hz,INV_G40
	RET
INV_G22:JB    INV_P1,INV_G40
	JB    ACC.BINV_SF,INV_G24
	SETB  ACC.BINV_SF
	MOVX  @DPTR,A
	RET
INV_G24:SETB  INV_E
	ANL   A,#0
	ORL   A,#(1 SHL BINV_P2)
	MOVX  @DPTR,A
	CLR   A
	CLR   C
	RET
INV_G40:CLR   ACC.BINV_SF
	MOV   DPTR,#INV_TIM	; Kontrola cas limitu
	MOVX  A,@DPTR
	DJNZ  ACC,INV_G42
INV_G41:SETB  INV_E		; Cyba ve snimacich nebo motoru
	MOV   DPTR,#INV_ST
	MOV   A,#(1 SHL BINV_ERR)
	MOVX  @DPTR,A
	SETB  C
	CLR   A
	RET
INV_G42:MOVX  @DPTR,A
	RET

TM_INV:	MOV   DPTR,#INV_TIM
	MOV   A,#INV_TIML
	MOVX  @DPTR,A		; casovy limit
	MOV   A,R2
	MOV   DPTR,#INV_ST
	ORL   A,#1 SHL BINV_IP
	MOVX  @DPTR,A		; prikaz pro ventil
	RET

TM_MAN:	MOV   DPTR,#SEK_ST
	MOVX  A,@DPTR
	JNZ   TM_MAN9
	MOV   DPTR,#INV_ST
	MOVX  A,@DPTR
	JB    ACC.BINV_IP,TM_MAN9
	JB    ACC.BINV_P2,TM_MAN7
	JMP   INV_GP2
TM_MAN7:JMP   INV_GP1
TM_MAN9:RET

; *******************************************************************
; Cidla kapaliny ve smycce

%DEFINE (LPS_NEW_CAL) (1)

%IF (%LPS_NEW_CAL) THEN (
LPS_FILTC SET 64	; 64 odpovida pomeru 1
)ELSE(
LPS_FILTC SET 32	; 32 odpovida pomeru 1/2
)FI

LPS_AD1	XDATA ADC3
LPS_AD2	XDATA ADC4

LPS_FLG	DATA  HW_FLG1	; FL_LPS1, FL_LPS2
LPS_FLGB0 BIT LPS_FLG.0
B_LPS1	SET   FL_LPS1-LPS_FLGB0
B_LPS2	SET   FL_LPS2-LPS_FLGB0

RSEG	AA_PO_X

LPS_LEN EQU   7

LPS_BAS:
LPS_CN1:DS    1         ; citac zmeny cidla 1
LPS_TR1:DS    2         ; komparacni uroven cidla 1
LPS_SM1:DS    2         ; pro kalibraci
LPS_TD1:DS    2         ; doba pro preklopeni cidla 1

LPS_CN2:DS    1         ; citac zmeny cidla 2
LPS_TR2:DS    2         ; komparacni uroven cidla 2
LPS_SM2:DS    2         ; pro kalibraci
LPS_TD2:DS    2         ; doba pro preklopeni cidla 2

LPS_END	SET   $

RSEG	AA_PO_C

LPS_TST:JNB   FL_25Hz,LPS_T99
	CALL  LPS_TS1
	CALL  LPS_TS2
LPS_T99:RET

LPS_TS1:MOV   DPTR,#LPS_AD1	; ADC
	MOV   R1,#LPS_CN1-LPS_BAS ; posun baze
	MOV   R2,#1 SHL B_LPS1	; maska bitu
	SJMP  LPS_T10
LPS_TS2:MOV   DPTR,#LPS_AD2
	MOV   R1,#LPS_CN2-LPS_BAS
	MOV   R2,#1 SHL B_LPS2
LPS_T10:MOVX  A,@DPTR		; Nacteni ADC do R45
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   A,R1
	ADD   A,#LOW  LPS_BAS
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH LPS_BAS
	MOV   DPH,A
	CLR   C			; porovnani R45 s LPS_TR?
	MOV   A,#1
	MOVC  A,@A+DPTR		; LPS_TRx
	SUBB  A,R4
	MOV   A,#2
	MOVC  A,@A+DPTR
	SUBB  A,R5
	MOV   A,R2
	JNC   LPS_T20
	CLR   A
LPS_T20:XRL   A,LPS_FLG
	ANL   A,R2
	JZ    LPS_T39		; Beze zmeny
	MOV   A,#5		; LPS_TDx
	MOVC  A,@A+DPTR
	MOV   R3,A
	MOVX  A,@DPTR
	CLR   C
	SUBB  A,R3
	ADDC  A,R3
	JC    LPS_T39
	MOV   A,R2		; Skutecne zmena
	XRL   LPS_FLG,A
	CLR   A
LPS_T39:MOVX  @DPTR,A
	RET

; Priprava kalibrace cidel
LPS_CS: MOV   DPTR,#LPS_CN1
	MOV   R0,#LPS_TD1-LPS_CN1
	CLR   A
LPS_CS1:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,LPS_CS1
	MOV   DPTR,#LPS_CN2
	MOV   R0,#LPS_TD2-LPS_CN2
	CLR   A
LPS_CS2:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,LPS_CS2
	RET

; Prvni kalibrace - kalibrace vody
LPS_CW: JNB   FL_25Hz,LPS_CW9
	MOV   DPTR,#LPS_AD1	; ADC
	MOV   R1,#LPS_TR1-LPS_BAS ; posun baze
	CALL  LPS_ADD
	MOV   DPTR,#LPS_AD2	; ADC
	MOV   R1,#LPS_TR2-LPS_BAS ; posun baze
	CALL  LPS_ADD
	MOV   DPTR,#LPS_CN1
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	CJNE  A,#LPS_FILTC,LPS_CW9
	CLR   A
	MOVX  @DPTR,A
LPS_CW9:RET

; Druha kalibrace - kalibrace vzduchu
LPS_CA: JNB   FL_25Hz,LPS_CA9
	MOV   DPTR,#LPS_AD1	; ADC
	MOV   R1,#LPS_SM1-LPS_BAS ; posun baze
	CALL  LPS_ADD
	MOV   DPTR,#LPS_AD2	; ADC
	MOV   R1,#LPS_SM2-LPS_BAS ; posun baze
	CALL  LPS_ADD
	MOV   DPTR,#LPS_CN1
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	CJNE  A,#LPS_FILTC,LPS_CA9
	CLR   A
	MOVX  @DPTR,A
	CLR   F0
	MOV   DPTR,#LPS_TR1
	CALL  LPS_CTR
	MOV   DPTR,#LPS_TR2
	CALL  LPS_CTR
	CLR   A
	RET
LPS_CA9:MOV   A,#1
	RET

LPS_ADD:MOVX  A,@DPTR		; Nacteni ADC/64 do R45
	INC   DPTR
	RL    A
	RL    A
	ANL   A,#3
	MOV   R4,A
	MOVX  A,@DPTR
	RL    A
	RL    A
	MOV   R5,A
	ANL   A,#3
	XCH   A,R5
	ANL   A,#NOT 3
	ORL   A,R4
	MOV   R4,A
	MOV   A,R1		; LPS polozka do DPTR
	ADD   A,#LOW  LPS_BAS
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH LPS_BAS   ; [DPTR] += R45
	MOV   DPH,A
	MOVX  A,@DPTR
	ADD   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOVX  @DPTR,A
	RET

%IF (%LPS_NEW_CAL) THEN (

; LPS_TRx = Voda + (Vzduch - Voda) / 4
LPS_CTR:MOVX  A,@DPTR		; LPS_TRx
	MOV   R4,A
	MOV   A,#1
	MOVC  A,@A+DPTR         ; LPS_TRx+1
	MOV   R5,A             	; R45 = LPS_TRx
	MOV   A,#2
	MOVC  A,@A+DPTR		; LPS_SMx
	CLR   C
	SUBB  A,R4
	MOV   R6,A
	MOV   A,#3
	MOVC  A,@A+DPTR		; LPS_SMx+1
	SUBB  A,R5
	MOV   R7,A		; R67 = LPS_SMx - LPS_TRx
	JC    LPS_CT6
	ADD   A,#-3H	; max 80h
	JC    LPS_CT9
LPS_CT6:SETB  F0
LPS_CT9:CLR   C			; R67 >>= 1
	MOV   A,R7
	RRC   A
	MOV   R7,A
	MOV   A,R6
	RRC   A
	MOV   R6,A
	CLR   C			; R67 >>= 1
	MOV   A,R7
	RRC   A
	MOV   R7,A
	MOV   A,R6
	RRC   A
	MOV   R6,A
	MOV   A,R6		; R45 + R67
        ADD   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	ADDC  A,R5
	MOVX  @DPTR,A
	INC   DPTR
	RET

)ELSE(

; LPS_TRx = (Vzduch + Voda) /2
LPS_CTR:MOVX  A,@DPTR		; LPS_TRx
	MOV   R4,A
	MOV   A,#1
	MOVC  A,@A+DPTR         ; LPS_TRx+1
	MOV   R5,A
	MOV   A,#2
	MOVC  A,@A+DPTR		; LPS_SMx
	MOV   R6,A
	ADD   A,R4
	MOVX  @DPTR,A
	MOV   A,#3
	MOVC  A,@A+DPTR		; LPS_SMx+1
	MOV   R7,A
	ADDC  A,R5
	INC   DPTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	SUBB  A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	SUBB  A,R5
	MOVX  @DPTR,A
	JC    LPS_CT6
	ADD   A,#-3H	; max 80h
	JC    LPS_CT9
LPS_CT6:SETB  F0
LPS_CT9:RET

)FI

; Kalibrace cidel
TM_LPS:	MOV   DPTR,#SEK_ST
	MOVX  A,@DPTR
	JNZ   TM_LPS9
	MOV   A,#SEK_ST_LPSC
	MOVX  @DPTR,A
	MOV   DPTR,#SEK_CFG
	MOVX  A,@DPTR
	ORL   A,#MSEK_LPS
	MOVX  @DPTR,A
TM_LPS9:RET

; *******************************************************************
; Sekvencer davkovani

RSEG	AA_PO_X

SEK_ST:	DS    1		; stav automatu davkovani

SEK_CFG:DS    1		; konfigurace sekvenceru
BSEK_LPS SET  0		; pouzivaji se cidla kapaliny
BSEK_NOV SET  1		; je pritomen dusikovy ventil
BSEK_REV SET  2		; kalibrace po davkovani
MSEK_LPS SET  1 SHL BSEK_LPS
MSEK_NOV SET  1 SHL BSEK_NOV
MSEK_REV SET  1 SHL BSEK_REV

SEK_TIM:DS    2		; Timer sekvenceru
P_VOL1: DS    2         ; Pevny objem sani vzorku po 1 cidlu
P_INJT: DS    2         ; Doba po kterou je otoceny davk. ventil

PER_SPD_HI EQU  10      ; Vysoka rychlost peristaltiky
PER_SPD_LO EQU  7       ; Nizka rychlost peristaltiky
PER_SPD_ZM EQU  5       ; Rychlost slimak

RSEG	AA_PO_C

; Cekani na dobehnuti citace SEK_TIM
SEK_WFT:JNB   FL_25Hz,SEK_WF8
	MOV   DPTR,#SEK_TIM
	MOVX  A,@DPTR
	MOV   R0,A
	MOV   A,#1
	MOVC  A,@A+DPTR
	MOV   R1,A
	ORL   A,R0
	JZ    SEK_WF9
	MOV   A,R0
	ADD   A,#0FFH
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	ADDC  A,#0FFH
	MOVX  @DPTR,A
SEK_WF8:MOV   A,#1
SEK_WF9:RET

SEK_E:	SETB  SEK_ERR
	CALL  STP_OFF
	MOV   R1,#1
	CALL  CLR_GEP
	SETB  INV_E
SEK_E1:	RET

SEK:	JB    SEK_ERR,SEK_E1
	MOV   DPTR,#SEK_ST
	MOVX  A,@DPTR
	CJNE  A,#SEK_STN,SEK1
SEK1:	JNC   SEK_E
	RL    A
	MOV   DPTR,#SEK_TAB
	MOV   R0,A
	MOVC  A,@A+DPTR
	PUSH  ACC
	MOV   A,R0
	INC   A
	MOVC  A,@A+DPTR
	PUSH  ACC
	RET
; Tabulka stavu sekvenceru
SEK_TAB:%W    (SEK_00)
	%W    (SEK_01)
	%W    (SEK_02)
SEK_ST_DOFCLEAN SET ($-SEK_TAB)/2 ; Cekani na vodu pri cisteni
	%W    (SEK_03)
	%W    (SEK_04)
SEK_ST_FAIR SET ($-SEK_TAB)/2	; Cekani na vzduch
	%W    (SEK_05)
	%W    (SEK_06)
	%W    (SEK_07)
SEK_ST_STPS SET	($-SEK_TAB)/2	; Rozjeti krokace
	%W    (SEK_08)
	%W    (SEK_09)
SEK_ST_LPLA SET	($-SEK_TAB)/2	; Sani do cidla A
	%W    (SEK_10)
	%W    (SEK_11)
	%W    (SEK_12)
SEK_ST_LPLB SET	($-SEK_TAB)/2	; Sani do cidla B
	%W    (SEK_13)
	%W    (SEK_14)
SEK_ST_WINJ SET	($-SEK_TAB)/2	; Stav cekani na nadavkovani
	%W    (SEK_15)
	%W    (SEK_16)
	%W    (SEK_17)
	%W    (SEK_18)
	%W    (SEK_19)
SEK_ST_FCLN SET	($-SEK_TAB)/2	; Vyplach ve volnem case
	%W    (SEK_20)
SEK_ST_LPSC SET	($-SEK_TAB)/2	; Stav pocatku kalibrace cidel
	%W    (SEK_21)
	%W    (SEK_22)
	%W    (SEK_23)
	%W    (SEK_24)
	%W    (SEK_25)
	%W    (SEK_26)
SEK_ST_LPEC SET	($-SEK_TAB)/2	; Konec kalibrace cidel
	%W    (SEK_27)
	%W    (SEK_28)
SEK_STN	SET   ($-SEK_TAB)/2

; Klidovy stav
SEK_00:	CALL  INV_G
	JMP   STP

; Stav 1 - zavrit ventil zkontrolovat ramenko
SEK_01: JB    MR_FLG.BMR_ERR,SEK_02E
	JB    MR_FLG.BMR_BSY,SEK_R
	MOV   DPTR,#REG_A+OREG_A+OMR_GST
	MOV   A,#20H
	MOVX  @DPTR,A

SEK_NST:MOV   DPTR,#SEK_ST
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
SEK_R:	RET

; Stav 2 - cekat na vytazeni ramenka pak cisteni
SEK_02:	JB    MR_FLG.BMR_ERR,SEK_02E
	JB    MR_FLG.BMR_BSY,SEK_02R
	JNB   ASH_ZF,SEK_02E
	MOV   DPTR,#SEK_CFG
	MOVX  A,@DPTR
	JNB   ACC.BSEK_REV,SEK_025
	JMP   SEK_STPS
SEK_025:CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_HI)
	%LDR45i(-1024)		; Vyplach peristaltiky
	CALL  GO_PER		; do cidel
	JB    F0,SEK_02E
	CLR   FL_LPS1
	CLR   FL_LPS2
	MOV   DPTR,#SEK_ST
	MOV   A,#SEK_ST_DOFCLEAN
	MOVX  @DPTR,A
SEK_02R:RET
SEK_02E:JMP   SEK_E


; Stav 3 - cekat na vodu v obou cidlech, pak vyplach objemem
SEK_03: JB    MR_FLG.BMR_ERR,SEK_02E
	JNB   FL_ENLPS,SEK_032
	JNB   MR_FLG.BMR_BSY,SEK_02E
	CALL  LPS_TST			; Test cidel kapaliny
	JNB   FL_LPS1,SEK_02R
	JNB   FL_LPS2,SEK_02R		; V 1 nebo 2 neni kapalina
	SJMP  SEK_033
SEK_032:JB    MR_FLG.BMR_BSY,SEK_02R
SEK_033:CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_HI)
	%LDR45i(-410)		; Vyplach peristaltiky
	CALL  GO_PER		; pevny objem
        JB    F0,SEK_02E
        JMP   SEK_NST

; Stav 4 - vyplach urcitym objemem, pak oddeleni vzduchem
SEK_04: JB    MR_FLG.BMR_ERR,SEK_02E
	JNB   FL_ENLPS,SEK_044
	CALL  LPS_TST			; Test cidel kapaliny
	JNB   FL_LPS1,SEK_042	; V 1 nebo 2 neni kapalina
	JB    FL_LPS2,SEK_044
SEK_042:SJMP  SEK_025
SEK_044:JB    MR_FLG.BMR_BSY,SEK_02R
SEK_045:CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_HI)
	%LDR45i(1250)		; Oddeleni vzduchem
	CALL  GO_PER		; do cidel
	JB    F0,SEK_02E
	MOV   DPTR,#SEK_ST
	MOV   A,#SEK_ST_FAIR
	MOVX  @DPTR,A
SEK_04R:RET

; Stav 5 - cekat vzduch v cidlech, pak pevny objem vzduchu
SEK_05:	JB    MR_FLG.BMR_ERR,SEK_02E
	JNB   FL_ENLPS,SEK_052
	JNB   MR_FLG.BMR_BSY,SEK_02E
	CALL  LPS_TST			; Test cidel kapaliny
	JB    FL_LPS1,SEK_04R
	JB    FL_LPS2,SEK_04R		; V 1 a 2 musi byt sucho
	SJMP  SEK_053
SEK_052:JB    MR_FLG.BMR_BSY,SEK_04R
SEK_053:CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_HI)
	%LDR45i(160)		; Oddeleni vzduchem
	CALL  GO_PER		; pevny objem
SEK_058:JB    F0,SEK_09E
	JMP   SEK_NST

; Stav 6 - natazeni pevneho objemu vzduchu
SEK_06:	JB    MR_FLG.BMR_ERR,SEK_09E
	JNB   FL_ENLPS,SEK_062
	CALL  LPS_TST			; Test cidel kapaliny
	JB    FL_LPS1,SEK_061
	JNB   FL_LPS2,SEK_062		; V 1 a 2 musi byt sucho
SEK_061:SJMP  SEK_045
SEK_062:JB    MR_FLG.BMR_BSY,SEK_04R
SEK_063:MOV   R1,#1
	CALL  STP_GEP
	JMP   SEK_STPS

; Stav 7 - rezerva
SEK_07:	JMP   SEK_STPS

; Zacatek vlasni davkovaci sekvence
SEK_STPS:MOV  DPTR,#STP_ST
	MOV   A,#10H			; Start krokace
	MOVX  @DPTR,A
	MOV   DPTR,#SEK_ST
	MOV   A,#SEK_ST_STPS
	MOVX  @DPTR,A
	RET

; Stav 8 - cekani na krokac pak start ramenka dolu
SEK_08: JB    STP_ERR,SEK_09E
	CALL  STP
	JNZ   SEK_10R
	JB    STP_ERR,SEK_09E
	MOV   A,P5
	ANL   A,#STP_MSA
	JNZ   SEK_09E
	MOV   DPTR,#REG_A+OREG_A+OMR_GST
	MOV   A,#30H
	MOVX  @DPTR,A
	JMP   SEK_NST

; Stav 9 - cekani na ramenko a start sani
SEK_09:	JB    MR_FLG.BMR_ERR,SEK_09E
	JB    MR_FLG.BMR_BSY,SEK_10R
SEK_09N:CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_LO)
	%LDR45i(410)		; Objem do ktereho musi prijit
	CALL  GO_PER		; kapalina do 1 cidla
	JB    F0,SEK_09E
	CLR   FL_LPS1
	CLR   FL_LPS2
	MOV   DPTR,#SEK_ST
	MOV   A,#SEK_ST_LPLA
	MOVX  @DPTR,A
	RET
SEK_09E:JMP   SEK_E

; Stav 10 - sani do cidla 1 pak sani urciteho objemu
SEK_10:	JB    MR_FLG.BMR_ERR,SEK_09E
	JNB   FL_ENLPS,SEK_101
	CALL  LPS_TST			; Test cidel kapaliny
	JB    FL_LPS1,SEK_104		; V 1 je kapalina
	JNB   MR_FLG.BMR_BSY,SEK_09E
	RET
SEK_101:JNB   MR_FLG.BMR_BSY,SEK_104	; Na objem bez cidla
SEK_10R:RET
SEK_104:
;!!!!!!!JB    FL_LPS2,SEK_09E		; V 2 je kapalina - error
	CLR   F0
	MOV   R1,#1
	MOV   DPTR,#P_VOL1	; Dosati objemu vzorku
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	%LDR23i(PER_SPD_LO)
	CALL  GO_PER
	JB    F0,SEK_09E
	%LDMXi(SEK_TIM,50)
	JMP   SEK_NST

; Stav 11 - dosati objemu pak ramenko nahoru
SEK_11: JNB   FL_ENLPS,SEK_112
	CALL  LPS_TST
	JB    FL_LPS1,SEK_112
	MOV   R1,#1		; Vzduch v cidle 1
	MOV   A,#OMR_AP+1
	CALL  MR_GPA1
	MOVX  A,@DPTR		; Ignorovat bubliny na zacatku
	JNZ   SEK_09E		; jinak ERROR
SEK_112:JNB   FL_LPS2,SEK_114
	CALL  SEK_WFT           ; Kapalina i jiz v druhem cidle
	JZ    SEK_118
	SJMP  SEK_116
SEK_114:%LDMXi(SEK_TIM,50)
SEK_116:JB    MR_FLG.BMR_ERR,SEK_12E
	JB    MR_FLG.BMR_BSY,SEK_10R
SEK_118:CLR   F0
        MOV   R1,#1             ; Zastavit peristaltiku
        CALL  STP_GEP
	MOV   DPTR,#REG_A+OREG_A+OMR_GST
	MOV   A,#20H		; Pohyb ramenka nahoru
	MOVX  @DPTR,A
	JMP   SEK_NST

; Stav 12 - jizda ramenka nahoru pak sani do cidla 2
SEK_12:	JB    MR_FLG.BMR_ERR,SEK_12E
	JB    MR_FLG.BMR_BSY,SEK_10R
	CALL  STP_OFF
SEK_125:CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_ZM)
	%LDR45i(300)		; Objem do ktereho musi prijit
	CALL  GO_PER		; kapalina do 2 cidla
	JB    F0,SEK_12E	; bylo 204
	MOV   DPTR,#SEK_ST
	MOV   A,#SEK_ST_LPLB
	MOVX  @DPTR,A
SEK_12R:RET
SEK_12E:JMP   SEK_E

; Stav 13 - sani do cidla 2 pak casova synchronizace
SEK_13: JB    MR_FLG.BMR_ERR,SEK_12E
	JNB   FL_ENLPS,SEK_132
	CALL  LPS_TST			; Test cidel
	JB    FL_LPS2,SEK_134		; Kapalina v cidle 2
	JNB   FL_LPS1,SEK_12E		; Bublina v cidle 1
	JNB   MR_FLG.BMR_BSY,SEK_12E
	RET
SEK_132:JNB   MR_FLG.BMR_BSY,SEK_134	; Jede se bez cidel
	RET
SEK_134:CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_ZM)
	%LDR45i(12)		; Objem presati vzorku
	CALL  GO_PER		; za 2 cidlo (bylo 25)
	JB    F0,SEK_12E
	JMP   SEK_NST

; Stav 14 - sani vzorku kousek za cidlo 2
SEK_14:	JB    MR_FLG.BMR_ERR,SEK_12E
	JNB   FL_ENLPS,SEK_142
	CALL  LPS_TST			; Test cidel
	JNB   FL_LPS2,SEK_125		; Bublina v cidle 2
	JNB   FL_LPS1,SEK_12E		; Bublina v cidle 1
SEK_142:JNB   MR_FLG.BMR_BSY,SEK_144	; Jede se bez cidel
	RET
SEK_144:
%IF(0)THEN(
	MOV   R1,#1		; Zastavit peristaltiku
	CALL  STP_GEP
)ELSE(
	MOV   R1,#1		; Vypmout rizeni peristaltiky
	CALL  CLR_GEP		; !!!!!!!!!!!!!!!!!
)FI
%IF(1)THEN(
	CLR   A			; ukoncit drzeni ramenka
	MOV   DPTR,#REG_A+OREG_A+OMR_GST
	MOVX  @DPTR,A		; !!!!!!!!!!!!!!!!!
)FI
	JMP   SEK_NST

; Stav 15 - casova syncronizace pak davkovani do 2
SEK_15: JNB   FL_WINJ,SEK_158
	JB    FL_DINJ,SEK_158
SEK_15R:RET
SEK_158:CLR   FL_DINJ
	CALL  INV_GP2		; Otocit do 2
	JMP   SEK_NST

; Stav 16 - dokonceni pohybu davkovace do 2
SEK_16: CALL  INV_G
	JNZ   SEK_15R
	JC    SEK_12E		; Chyba, ventil se neotocil
	MOV   DPTR,#P_INJT      ; Doba otoceni ventilu - DAVKA
	MOVX  A,@DPTR
	MOV   B,#15
	MUL   AB
	MOV   R0,A
	MOV   R1,B
	INC   DPTR
	MOVX  A,@DPTR
	MOV   B,#15
	MUL   AB
	ADD   A,R1
	XCH   A,R0
	MOV   DPTR,#SEK_TIM
	MOVX  @DPTR,A
	INC   DPTR
	XCH   A,R0
	MOVX  @DPTR,A
	JMP   SEK_NST

; Stav 17 - cekani po urcitou dobu pak davkovac do 1
SEK_17:	CALL  SEK_WFT
	JNZ   SEK_15R
	CALL  INV_GP1		; Otocit do 1
%IF(1)THEN(
	MOV   A,#20H		; pridrzet ramenko
	MOV   DPTR,#REG_A+OREG_A+OMR_GST
	MOVX  @DPTR,A		; !!!!!!!!!!!!!!!!!
)FI
	JMP   SEK_NST

; Stav 18 - davkovaci kohout do 1 pak vyplach smycky
SEK_18: JB    MR_FLG.BMR_ERR,SEK_22E
	CALL  INV_G
	JNZ   SEK_15R
	JC    SEK_22E		; Chyba, ventil se neotocil
	JB    MR_FLG.BMR_BSY,SEK_15R ; Hybe se ramenko
	CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_HI)
	%LDR45i(-500)		; Vyplach smycky
	CALL  GO_PER
	JB    F0,SEK_22E
	JMP   SEK_NST

; Stav 19 - vyplach smycky - pak konec nebo kalibrace
SEK_19:	JB    MR_FLG.BMR_ERR,SEK_22E
	JB    MR_FLG.BMR_BSY,SEK_23R
	CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_HI)
	%LDR45i(-3000)		; Vyplach ve volnem case
	CALL  GO_PER
	JB    F0,SEK_22E
	JMP   SEK_NST

; Stav 20 - vyplach ve volnem case
SEK_20: JB    MR_FLG.BMR_ERR,SEK_22E
	JB    MR_FLG.BMR_BSY,SEK_23R
	MOV   DPTR,#SEK_CFG
	MOVX  A,@DPTR
	JB    ACC.BSEK_REV,SEK_20N
	JMP   SEK_END
SEK_20N:JMP   SEK_NST

; Stav 21 - pocatek kalibrace - rezerva
SEK_21:	JB    MR_FLG.BMR_ERR,SEK_22E
	CALL  INV_G
	JNZ   SEK_21R
	JNB   MR_FLG.BMR_BSY,SEK_212
SEK_21R:RET
SEK_212:MOV   DPTR,#REG_A+OREG_A+OMR_GST       ; Kontrola ramenka
	MOV   A,#20H
	MOVX  @DPTR,A
	JMP   SEK_NST

; Stav 22 - pocatek vyplachy peristaltiky
SEK_22:	JB    MR_FLG.BMR_ERR,SEK_22E
	JB    MR_FLG.BMR_BSY,SEK_21R
	CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_HI)
	%LDR45i(-2500)		; Vyplach peristaltiky
	CALL  GO_PER
	JB    F0,SEK_22E
	JMP   SEK_NST
SEK_22E:JMP   SEK_E

; Stav 23 - peristaltika vyplachuje pak priprava kalibrace cidel
SEK_23: JB    MR_FLG.BMR_ERR,SEK_22E
	JNB   MR_FLG.BMR_BSY,SEK_232
SEK_23R:RET
SEK_232:MOV   DPTR,#SEK_CFG
	MOVX  A,@DPTR
	JB    ACC.BSEK_LPS,SEK_235
	CALL  SEK_NST		; preskocit jeden stav
	SJMP  SEK_242
SEK_235:CALL  LPS_CS		; pocatek kalibrace cidel
	JMP   SEK_NST

; Stav 24 - kalibrace cidel na vodu pak suseni
SEK_24: CALL  LPS_CW
	JNZ   SEK_23R
SEK_242:MOV   DPTR,#SEK_CFG
	MOVX  A,@DPTR
	JNB   ACC.BSEK_NOV,SEK_245
	; Susici ventil ON
	JMP   SEK_NST
SEK_245:CLR   F0		; Neni ventil
	MOV   R1,#1
	%LDR23i(PER_SPD_HI)
	%LDR45i(1250)		; Objem pro vysusebi trubicky
	CALL  GO_PER
	JB    F0,SEK_22E
	CALL  SEK_NST
	JMP   SEK_NST

; Stav 25 - doba sepnuti susiciho
SEK_25:
	; Susici ventil OFF
	CALL  SEK_NST
	SJMP  SEK_265

; Stav 26 - suseni peristaltikou pak kalibrace vzduchu
SEK_26: JB    MR_FLG.BMR_ERR,SEK_27E
	JNB   MR_FLG.BMR_BSY,SEK_265
SEK_26R:RET
SEK_265:MOV   DPTR,#SEK_CFG
	MOVX  A,@DPTR
	JNB   ACC.BSEK_LPS,SEK_268
	JMP   SEK_NST
SEK_268:CALL  SEK_NST		; cidla nejsou
	SJMP  SEK_275

; Stav 27 - kalibrace vzduchu, pak naplneni vodou
SEK_27:	CALL  LPS_CA
	JNZ   SEK_26R
	CLR   FL_ENLPS
	JB    F0,SEK_27E
	SETB  FL_ENLPS		; Nakalibrovana cidla
SEK_275:MOV   DPTR,#SEK_CFG
	MOVX  A,@DPTR
	JB    ACC.BSEK_REV,SEK_END
	CLR   F0
	MOV   R1,#1
	%LDR23i(PER_SPD_HI)
	%LDR45i(-2400)		; Vyplach peristaltiky
	CALL  GO_PER
	JB    F0,SEK_27E
	CLR   FL_LPS1
	CLR   FL_LPS2
	JMP   SEK_NST
SEK_27E:JMP   SEK_E

; Stav 28 - dokonceni plneni vodou
SEK_28: JB    MR_FLG.BMR_ERR,SEK_27E
	JNB   MR_FLG.BMR_BSY,SEK_27E
	CALL  LPS_TST
	JNB   FL_LPS1,SEK_26R
	JNB   FL_LPS2,SEK_26R
SEK_END:MOV   R1,#1		; ukoncit rizeni peristaltiky
	CALL  CLR_GEP
	CLR   A			; ukoncit drzeni ramenka
	MOV   DPTR,#REG_A+OREG_A+OMR_GST
	MOVX  @DPTR,A
	MOV   DPTR,#SEK_ST
	MOVX  @DPTR,A
	RET

; Dokonceni pripraveneho davkovani
TM_INJ:	JNB   FL_WINJ,TM_INJ9
	MOV   DPTR,#SEK_ST
	MOVX  A,@DPTR
	CJNE  A,#SEK_ST_WINJ,TM_INJE
	SETB  FL_DINJ		; Dokonci davkovani
TM_INJ9:RET
TM_INJE:JMP   SEK_E

; Manualni spusteni davkovani
TM_LOAD:JB    FL_WINJ,TM_INJ
	JMP   TM_SEK

; Davkovani s cekanim na TM_INJ
TM_PREP:CLR   FL_DINJ
	SETB  FL_WINJ
	SJMP  TM_SEK1

; Davkovani bez cekanim TM_INJ
TM_SEK:	CLR   FL_WINJ
	CLR   FL_DINJ
TM_SEK1:MOV   DPTR,#SEK_CFG
	MOVX  A,@DPTR
	JNB   ACC.BSEK_LPS,TM_SEK2
	JNB   FL_ENLPS,TM_SEKE
TM_SEK2:MOV   DPTR,#SEK_ST
	MOVX  A,@DPTR
	JZ    TM_SEK3
	CJNE  A,#SEK_ST_FCLN,TM_SEKE
	CLR   F0
	MOV   R1,#1             ; Zastavit peristaltiku
	CALL  STP_GEP
TM_SEK3:MOV   DPTR,#SEK_ST
	MOV   A,#1
	MOVX  @DPTR,A
	CLR   SEK_ERR
	RET
TM_SEKE:JMP   SEK_E

; Rekonfigurace a inicializace vcetne kalibrace cidel
; R45 nova konfigurace
TM_INICFG:
	MOV   A,R4
	JNZ   TM_ICF1
	MOV   A,#MSEK_LPS	; Musi se pouzivat cidla
TM_ICF1:MOV   DPTR,#SEK_CFG
	MOVX  @DPTR,A
	CALL  SEK_INI
	CALL  TM_LPS
	RET

; Pouze zastavani a navrat ramenka
SEK_INI:CLR   A
	CLR   FL_WINJ
	CLR   FL_DINJ
	MOV   DPTR,#SEK_ST
	MOVX  @DPTR,A
	CLR   SEK_ERR
	MOV   R1,#0		; Ramenko
	MOV   A,#OMR_FLG
	CALL  MR_GPA1
	MOV   A,#MMR_ENI
	MOVX  @DPTR,A
	MOV   A,#OMR_ERC
	CALL  MR_GPA1
	CLR   A
	MOVX  @DPTR,A
	MOV   R1,#1		; Peristaltika
	MOV   A,#OMR_FLG
	CALL  MR_GPA1
	MOV   A,#MMR_ENI
	MOVX  @DPTR,A
	CALL  CLR_GEP
	MOV   MR_FLG,#0		; nulovat chyby
	CALL  STP_OFF		; vypnout krokac
	CALL  TM_ASH_HOME
	CALL  INV_GP1		; ventil do polohy 1
	RET

G_SEK_ST:
	JNB   STP_ERR,G_SEKS1
	MOV   R5,#0FCH		; Chyba krokoveho motoru
	MOV   DPTR,#STP_ST
	MOVX  A,@DPTR
	MOV   R4,A
	SJMP  G_SEKS9
G_SEKS1:JNB   MR_FLG.BMR_ERR,G_SEKS3
	MOV   R4,#090H		; Chyba stejnosmerneho motoru
	MOV   R5,#0FEH
	MOV   R2,#REG_N
	MOV   DPTR,#REG_A+OREG_A+OMR_FLG
G_SEKS2:MOVX  A,@DPTR
	JB    ACC.BMR_ERR,G_SEKS9
	MOV   A,#OMR_LEN
	CALL  ADDATDP
	INC   R4
	DJNZ  R2,G_SEKS2
	MOV   R4,#080H
	SJMP  G_SEKS9
G_SEKS3:JNB   SEK_ERR,G_SEKS5
	MOV   R5,#0FDH		; Chyba pri cinnosti sekvenceru
	SJMP  G_SEKS7
G_SEKS5:
G_SEKS6:MOV   R5,#0
G_SEKS7:MOV   DPTR,#SEK_ST
	MOVX  A,@DPTR
	CJNE  A,#SEK_ST_WINJ,G_SEKS81
	MOV   A,#81H
G_SEKS81:CJNE A,#SEK_ST_LPEC,G_SEKS82
	MOV   A,#88H
G_SEKS82:CJNE A,#SEK_ST_LPLA,G_SEKS83
	MOV   A,#89H
G_SEKS83:CJNE A,#SEK_ST_LPLB,G_SEKS84
	MOV   A,#8AH
G_SEKS84:CJNE A,#SEK_ST_FCLN,G_SEKS85
	MOV   A,#90H
G_SEKS85:
G_SEKS88:MOV R4,A
G_SEKS9:CLR  F0
	MOV  A,#1
	RET

; *******************************************************************
; Rizeni teploty

EXTRN	CODE(MR_PIDP)

TMC_OUT	BIT   P4.5
TC_ACL	XDATA 0FFE3H
TC_MCL	EQU   10H
TC_ADC	XDATA ADC0

RSEG	AA_PO_X

TMC1:	DS    OMR_LEN
TMC1_FLG XDATA TMC1+OMR_FLG
TMC1_AT	XDATA TMC1+OMR_AP
TMC1_AG	XDATA TMC1+OMR_AS
TMC1_RT	XDATA TMC1+OMR_RPI
TMC1_EN	XDATA TMC1+OMR_ENE
TMC1_FL:DS    2
TMC1_RD:DS    2

TMC1_OC:DS    2
TMC1_MC:DS    2

TMC_PWC:DS    1

RSEG	AA_PO_C

TMC_REG:MOV   DPTR,#TMC1_FLG
	MOVX  A,@DPTR
	JNB   ACC.BMR_ENI,TMC_39	; mereni vypnuto
	MOV   R7,A
; Mereni
	MOV   DPTR,#TC_ADC
	MOVX  A,@DPTR
	INC   DPTR
TMC_R15:RL    A
	RL    A
	ANL   A,#3
	MOV   R4,A
	MOVX  A,@DPTR
	RL    A
	RL    A
	MOV   R5,A
	ANL   A,#3
	XCH   A,R5
	ANL   A,#NOT 3
	ORL   A,R4
	MOV   R4,A
	MOV   DPTR,#TMC1_FL
	MOVX  A,@DPTR
	ADD   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOVX  @DPTR,A
; Generovani PWM
	MOV   DPTR,#TMC_PWC
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	ANL   A,#1FH
	MOV   R4,A
	MOV   A,R7
	JNB   ACC.BMR_ENR,TMC_38	; energie vypnuta
	MOV   DPTR,#TMC1_EN+1
	MOVX  A,@DPTR
%IF(0) THEN (                           ; pro chlazeni
        CPL   A
        INC   A
)FI
	JNB   ACC.7,TMC_33
	SETB  TMC_OUT
	ADD   A,#10H		; Posun vetraku
	JC    TMC_34
	MOV   A,#TC_MCL
	SJMP  TMC_35
TMC_33: CLR   C
	RRC   A
	CLR   C
	RRC   A
	ADDC  A,#0
	SETB  C
	SUBB  A,R4
	MOV   TMC_OUT,C
TMC_34:
%IF(0) THEN(
	MOV   A,#TC_MCL		; Pro chlazeni
)ELSE(
	CLR   A
)FI
TMC_35:	MOV   DPTR,#TC_ACL	; Spinani vetraku
	MOVX  @DPTR,A
TMC_38:	MOV   DPTR,#TMC_PWC
	MOVX  A,@DPTR
	ANL   A,#1FH
	JZ    TMC_R50
TMC_39:	RET
; Nacteni filtru a prepocet
TMC_R50:MOV   DPTR,#TMC1_FL
	MOVX  A,@DPTR
	MOV   R4,A
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A		; R45 = TMC1_FL
	CLR   A
	MOVX  @DPTR,A
	MOV   DPTR,#TMC1_RD	; Ulozeni raw data
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   DPTR,#TMC1_MC	; R45=R45*TMC1_MC/2^16
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	CALL  MULsi
	MOV   A,R6
	MOV   R4,A
	MOV   A,R7
	MOV   R5,A
	MOV   DPTR,#TMC1_OC	; R45=R45+TMC1_OC
	MOVX  A,@DPTR
	ADD   A,R4
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOV   R5,A
	JNB   OV,TMC_R53
	%LDR45i(7FFFH)
	JB    ACC.7,TMC_R53
	%LDR45i(-7FFFH)
TMC_R53:
; Rozdil
TMC_R55:CLR   C
	MOV   DPTR,#TMC1_AT	; TMC1_AT=R45
	MOVX  A,@DPTR           ; R45-=old TMC1_AT
	XCH   A,R4
	MOVX  @DPTR,A
	SUBB  A,R4
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R5
	MOVX  @DPTR,A
	MOV   R3,#0
	JNB   ACC.7,TMC_R56	; znamenkove rozsireni
	DEC   R3
TMC_R56:SUBB  A,R5
	MOV   R5,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	MOV   DPTR,#TMC1_AG
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
; Vlastni regulator
TMC_R60:MOV   DPTR,#TMC1+OMR_ERC
	CLR   A
	MOVX  @DPTR,A
	MOV   DPTR,#TMC1
	CALL  MR_PIDP
	MOV   DPTR,#TMC1_EN
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	RET

TMC_INI:MOV   DPTR,#TMC1_FLG
	MOV   A,#80H
	MOVX  @DPTR,A
	MOV   DPTR,#TC_ACL
	CLR   A
	MOVX  @DPTR,A
	%LDR45i (TMC1)
	%LDR23i (TMC1PPR)
	%LDR01i (OMR_LEN)
	CALL  cxMOVE
	MOV   DPTR,#TMC1_FL
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
       ; Pro starsi desky
       ;%LDMXi (TMC1_OC,350)	; T2
       ;%LDMXi (TMC1_MC,18570)	; ((T2-T1)*100*10000h)/7FE0h
       ; Pro nove desky
	%LDMXi (TMC1_OC,9600)	; T2
	%LDMXi (TMC1_MC,-16600)	; ((T2-T1)*100*10000h)/7FE0h
; Vypne topeni a vetrak kolony
TMC_OFF:MOV   DPTR,#TMC1_FLG
	MOV   A,#MMR_ENI
	MOVX  @DPTR,A
	SETB  TMC_OUT		; Vypnout topeni
	CLR   A
	MOV   DPTR,#TC_ACL	; Spinani vetraku
	MOVX  @DPTR,A
	MOV   A,#1
	RET

; Zapne regulaci teploty kolony
TMC_ON:	MOV   DPTR,#TMC1_FLG
	MOV   A,#MMR_ENI OR MMR_ENR
	MOVX  @DPTR,A
	RET

G_TMC_ST:MOV  DPTR,#TMC1_FLG
	MOVX  A,@DPTR
	JNB   ACC.BMR_ERR,TMC_ST1
	%LDR45i(0FFF1H)
	RET
TMC_ST1:MOV   R5,#0
	JNB   ACC.BMR_ENR,TMC_ST3
	MOV   A,#OMR_ENE+1
	MOVC  A,@A+DPTR
	MOV   R4,#1
	JZ    TMC_ST2
	MOV   R4,#2
	JNB   ACC.7,TMC_ST2
	MOV   R4,#3
TMC_ST2:RET
TMC_ST3:MOV   R4,#0
	RET

TMC1PPR:DB    0, 0,0,0, 0,0
	DB    0
	DB    0,0   			; RP
	DB    0
	DB    0,0,0			; RS
	DB    018H,0, 002H,0, 20H,0	; P I D
	DB    000H,0, 000H,0, 0,07FH	; 1 2 ME
	DB    080H,0, 010H,0		; MS MA
	DB    0,0,0,0,0,0,0

UR_TEMP:MOV   A,#OU_DP
	CALL  ADDATDP
G_TEMP:	%LDR23i(10)
	CALL  xMDPDP
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   EA,C
	JB    ACC.7,G_TEMP1
	CALL  DIVi	; R45=R45/R23
	MOV   A,R4
	ADDC  A,#0
	MOV   R4,A
	MOV   A,R5
	ADDC  A,#0
	MOV   R5,A
	ORL   A,#1
	RET
G_TEMP1:CALL  NEGi
	CALL  DIVi	; R45=R45/R23
	CLR   A
	SUBB  A,R4
	MOV   R4,A
	CLR   A
	SUBB  A,R5
	MOV   R5,A
	ORL   A,#1
	RET

UW_TEMP:MOV   A,#OU_DPSI
	CALL  ADDATDP
S_TEMP:	%LDR23i(10)
	CALL  MULsi
	CALL  xMDPDP
	MOV   C,EA
	CLR   EA
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   EA,C
	JMP   TMC_ON

; *******************************************************************
; Prace s pameti EEPROM 8582

EEP_ADR	EQU   0A0H		; Adresa EEPROM na IIC sbernici
C_S1CON EQU   11000000B ; 11000001B ; Pocatecni stav S1CON
			; 11000000B ; S1CON s delenim 960
			; 01000000B ; S1CON pro X 24.000 MHz

EEA_RD	EQU   1		; akce cteni
EEA_WR	EQU   2		; akce zapisu

RSEG	AA_PO_D

RSEG	AA_PO_X

EE_PTR:	DS    2		; ukazatel do MEM_BUF na data
EEP_TAB:DS    2		; popis dat ulozenych v bloku EEPROM

MEM_BUF:DS    100H	; Buffer pameti EEPROM

RSEG	AA_PO_C

EE_WAIT:JNB   SI,$
	MOV   A,S1STA
	RET

EE_WRA:	MOV   S1CON,#C_S1CON	; OR 20H
	SETB  STO
	CLR   SI
	SETB  STA		; START
	CALL  EE_WAIT
	CLR   STA
	CJNE  A,#008H,EE_WRA
	MOV   A,R5
	MOV   S1DAT,A		; SLA and W
	CLR   SI
	CALL  EE_WAIT
	XRL   A,#018H
	JNZ   EE_WRA9
	MOV   A,R4
	MOV   S1DAT,A		; Adresa v EEPROM
	CLR   SI
	CALL  EE_WAIT
	XRL   A,#028H
EE_WRA9:RET

; Cteni pameti EEPROM
;	DPTR .. pointer na buffer
;	R2   .. pocet ctenych byte
;	R4   .. adresa, od ktere se cte

EE_RD:	MOV   R5,#EEP_ADR
	MOV   R1,#0
EE_RD10:CLR   ES1
	CALL  EE_WRA
	JNZ   EE_ERR
	SETB  STA		; repeated START
	CLR   SI
	CALL  EE_WAIT
	CLR   STA
	CJNE  A,#010H,EE_ERR
	MOV   A,R5
	ORL   A,#1
	MOV   S1DAT,A		; SLA and R
	CLR   SI
	CALL  EE_WAIT
	CJNE  A,#040H,EE_ERR
	SETB  AA
EE_RD30:CLR   SI
	CALL  EE_WAIT
	CJNE  A,#050H,EE_ERR
	MOV   A,S1DAT		; DATA
	MOVX  @DPTR,A
	XRL   A,R1
	INC   A
	MOV   R1,A
	INC   DPTR
	DJNZ  R2,EE_RD30
	CLR   AA
	CLR   SI
	CALL  EE_WAIT
	CJNE  A,#058H,EE_ERR
	MOV   A,S1DAT		; XORSUM
	XRL   A,R1
	SETB  STO		; STOP
	CLR   SI
	RET

EE_ERR:	SETB  STO
	CLR   SI
	SETB  F0
	ORL   A,#07H
	RET

; Zapis do pameti EEPROM
;	DPTR .. pointer na buffer
;	R2   .. pocet zapisovanych byte
;	R4   .. adresa, od ktere se zapisuje

EE_WR:	MOV   R5,#EEP_ADR
	MOV   R1,#0
EE_WR10:CLR   ES1
EE_WR20:MOV   R0,#0
EE_WR21:NOP
	DJNZ  R0,EE_WR21
	MOV   R0,#100
	MOV   A,R2
	JZ    EE_WR30
EE_WR22:CALL  EE_WRA
	JZ    EE_WR23
	DJNZ  R0,EE_WR22
	SJMP  EE_ERR
EE_WR23:MOV   R0,#4
EE_WR24:MOVX  A,@DPTR
	MOV   S1DAT,A
	CLR   SI
	XRL   A,R1
	INC   A
	MOV   R1,A
	INC   DPTR
	DEC   R2
	INC   R4
	CALL  EE_WAIT
	CJNE  A,#028H,EE_ERR
	MOV   A,R2
	JZ    EE_WR26
	DJNZ  R0,EE_WR24
EE_WR26:SETB  STO
	CLR   SI
	SJMP  EE_WR20
EE_WR30:CALL  EE_WRA
	JZ    EE_WR34
	DJNZ  R0,EE_WR30
	SJMP  EE_ERR
EE_WR34:MOV   A,R1
	MOV   S1DAT,A
	CLR   SI
	CALL  EE_WAIT
	CJNE  A,#028H,EE_ERR
	SETB  STO
	CLR   SI
	RET

; R45 := [EE_PTR++]
EEA_RDi:MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOV   R0,DPH
	MOV   A,DPL
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	RET

; [EE_PTR++] := R45
EEA_WRi:MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   R0,DPH
	MOV   A,DPL
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	RET

; provadi EEA_RD, EEA_WR pro iteger cislo
EEA_Mi:	CJNE  R0,#EEA_RD,EEA_Mi5
	CALL  EEA_RDi
	MOV   DPL,R2
	MOV   DPH,R3
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	RET
EEA_Mi5:CJNE  R0,#EEA_WR,EEA_Mi9
	MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  EEA_WRi
EEA_Mi9:RET

EEA_PRO:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#EE_PTR
	MOV   A,#LOW MEM_BUF
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH MEM_BUF
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
EEA_PR2:MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	ORL   A,R4
	JZ    EEA_Mi9
	PUSH  DPL
	PUSH  DPH
	MOV   A,R0
	PUSH  ACC
	CALL  JMPR45
	POP   ACC
	MOV   R0,A
	POP   DPH
	POP   DPL
	JMP   EEA_PR2

JMPR45:	MOV   A,R4
	PUSH  ACC
	MOV   A,R5
	PUSH  ACC
	RET

; Nastavi EEP_TAB a DPTR na R23
EEP_PTS:MOV   DPTR,#EEP_TAB
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	MOV   DPL,R2
	MOV   DPH,R3
	RET

; Ulozit data podle tabulky R23
EEP_WRS:CALL  EEP_PTS
	INC   DPTR
	INC   DPTR	; popis akci
	MOV   R0,#EEA_WR
	CALL  EEA_PRO
	MOV   DPTR,#EEP_TAB
	CALL  xMDPDP
	MOVX  A,@DPTR
	MOV   R2,A	; pocet prenasenych byte
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A	; pocatecni adresa v EEPROM
	MOV   DPTR,#MEM_BUF
	JMP   EE_WR

; Nacist data podle tabulky R23
EEP_RDS:CALL  EEP_PTS
	MOVX  A,@DPTR
	MOV   R2,A	; pocet prenasenych byte
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A	; pocatecni adresa v EEPROM
	MOV   DPTR,#MEM_BUF
	CALL  EE_RD
	JNZ   EEP_RD9
	MOV   DPTR,#EEP_TAB
	CALL  xMDPDP
	INC   DPTR
	INC   DPTR	; popis akci
	MOV   R0,#EEA_RD
	JMP   EEA_PRO
EEP_RD9:RET

; *******************************************************************
; Ukladani a cteni nastaveni z EEPROM

; Akce TMC ON/OFF
; R2 cislo regulatoru
EEA_TMC:CJNE  R0,#EEA_RD,EEA_TMC5
	CALL  EEA_RDi
	MOV   A,R2
	MOV   R1,A
	MOV   A,R4
	JZ    EEA_TMC2
	JMP   TMC_ON
EEA_TMC2:JMP  TMC_OFF
EEA_TMC5:CJNE R0,#EEA_WR,EEA_TMC9
	MOV   A,R2
	MOV   R1,A
	CALL  G_TMC_ST
	CALL  EEA_WRi
EEA_TMC9:RET

; Tabulky popisu akci pro ulozeni a cteni dat

; Tabulka pro SERVICE
EEC_SER:DB    030H	; pocet byte ukladanych dat
	DB    010H	; pocatecni adresa v EEPROM

	%W    (TMC1_OC)
	%W    (EEA_Mi)

	%W    (TMC1_MC)
	%W    (EEA_Mi)

	%W    (ASH1_BOT)
	%W    (EEA_Mi)

	%W    (LPS_TD1)
	%W    (EEA_Mi)

	%W    (LPS_TD2)
	%W    (EEA_Mi)

	%W    (P_IRC_W)
	%W    (EEA_Mi)

	%W    (P_INJT)
	%W    (EEA_Mi)

	%W    (P_VOL1)
	%W    (EEA_Mi)

	%W    (0)
	%W    (0)

; *******************************************************************

xPR_AD: MOV   A,#' '
	CALL  LCDWR
	MOV   R1,#3
	SJMP  xPRThl1

xPRThl:	MOV   R1,#4
xPRThl1:MOV   A,R1
	DEC   A
	MOVC  A,@A+DPTR
	CALL  PRINThb
	DJNZ  R1,xPRThl1
	RET

DB_W_10:MOV   R0,#10H
	SJMP  DB_WAI1

DB_WAIT:MOV   R0,#0H
DB_WAI1:%WATCHDOG
	DJNZ  R1,DB_WAI1
	DJNZ  R0,DB_WAI1
	RET

L0:	MOV   SP,#80H
	SETB  EA
	JNB   FL_DIPR,L007
	MOV   DPTR,#DEVER_T
	CALL  cPRINT
	CALL  DB_WAIT

	MOV   A,#LCD_CLR
	CALL  LCDWCOM
L007:
	CALL  CF_INIT
	CALL  MR_INIS
	CALL  SEK_INI
	CALL  TMC_INI

	%LDR23i(EEC_SER); Cteni parametru z EEPROM
	CALL  EEP_RDS

	JB    FL_DIPR,L090
L080:	CALL  UD_OI	; Bez displeje
	JMP   L080

L090:	MOV   A,#K_PROG
	CALL  TESTKEY
	JZ    L1
	JMP   UT

L1:
	MOV   A,#LCD_HOM
	CALL  LCDWCOM

	MOV   DPTR,#REG_A+OREG_A+OMR_AP
	CALL  xPR_AD

	MOV   DPTR,#REG_A+OREG_B+OMR_AP
	CALL  xPR_AD

	MOV   A,#LCD_HOM+40H
	CALL  LCDWCOM

	MOV   DPTR,#SEK_ST		; Sekvencer
	MOVX  A,@DPTR
	CALL  PRINThb

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#STP_ST		; Krokac
	MOVX  A,@DPTR
	CALL  PRINThb

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#STP_SPD
	MOVX  A,@DPTR
	CALL  PRINThb

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#STP_AP
	MOVX  A,@DPTR
	CALL  PRINThb

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#STP_TMP
	MOVX  A,@DPTR
	CALL  PRINThb

	MOV   A,#'>'
	CALL  LCDWR

	MOV   DPTR,#STP_WT
	MOVX  A,@DPTR
	CALL  PRINThb

	MOV   A,#' '
	CALL  LCDWR

	MOV   A,P5
	CALL  PRINThb

	MOV   A,#' '
	CALL  LCDWR

	MOV   A,#LCD_HOM+14H
	CALL  LCDWCOM

	MOV   DPTR,#ADC0		; Prevodniky
	CALL  xLDR45i
	CALL  PRINThw

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#ADC6
	CALL  xLDR45i
	CALL  PRINThw

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#ADC7
	CALL  xLDR45i
	CALL  PRINThw

	MOV   A,#' '
	CALL  LCDWR

	MOV   A,#LCD_HOM+54H
	CALL  LCDWCOM

	MOV   DPTR,#REG_A+OREG_A+OMR_GST	; Ramenko
	MOVX  A,@DPTR
	CALL  PRINThb

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#REG_A+OREG_A+OMR_FOD
	CALL  xLDR45i
	CALL  PRINThw

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#REG_A+OREG_A+OMR_FOI
	CALL  xLDR45i
	CALL  PRINThw

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#REG_A+OREG_A+OMR_RP
	CALL  xLDR45i
	CALL  PRINThw

L2:	CALL  SCANKEY
	JZ    L3
	MOV   R7,A
	MOV   DPTR,#SFT_D1
	CALL  SEL_FNC
L3:

	MOV   R0,#40H
	CALL  uL_FNC

	JMP   L1


T_RDVAL:MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR
	PUSH  ACC
        INC   DPTR
	MOVX  A,@DPTR
	PUSH  ACC
	INC   DPTR
	CALL  cPRINT
	MOV   R6,#8
	MOV   R7,#40H
	CALL  INPUTi
	POP   DPH
	POP   DPL
	JB    F0,T_RDV99
	CALL  xSVR45i
T_RDV99:RET


T_DISP:	MOV   A,#LCD_CLR
	CALL  LCDWCOM
T_DISP0:MOV   R6,#8
	MOV   R7,#40H
	CALL  INPUThw
	MOV   A,R4
	JZ    T_DISP9
	CALL  LCDWCOM
	MOV   A,R5
	MOV   R1,#'A'
	MOV   R0,#5
T_DISP1:MOV   A,R1
	CALL  LCDWR
	MOV   A,#0F0H
	CALL  LCDWR
	MOV   R2,#14
T_DISP2:MOV   A,#' '
	CALL  LCDWR
	DJNZ  R2,T_DISP2
	INC   R1
	DJNZ  R0,T_DISP1
	SJMP  T_DISP0
T_DISP9:RET

T_COFF:	SETB  TMC_OUT
	RET

T_CON:	CLR   TMC_OUT
	RET

SFT_D1:	DB    K_END
	%W    (0)
	%W    (L0)

	DB    K_RUN
	%W    (0)
	%W    (TM_SEK)

	DB    K_SAMPL
	%W    (0)
	%W    (TM_STP)

	DB    K_PUMP1
	%W    (0)
	%W    (TM_GEP)

	DB    K_H_A
	%W    (T_PWM0)
	%W    (T_RDVAL)

	DB    K_H_B
	%W    (T_PWM1)
	%W    (T_RDVAL)

	DB    K_0
	%W    (0)
	%W    (TM_STPHLD)

	DB    K_1
	%W    (0)
	%W    (T_DISP)

	DB    K_3
	%W    (10H)
	%W    (TM_ASH)

	DB    K_4
	%W    (20H)
	%W    (TM_ASH)

	DB    K_5
	%W    (30H)
	%W    (TM_ASH)

	DB    K_6
	%W    (0)
	%W    (UT)

	DB    K_7
	%W    (0)
	%W    (T_CON)

	DB    K_8
	%W    (0)
	%W    (T_COFF)

	DB    K_DP
	%W    (0)
	%W    (MONITOR)

	DB    K_UP
	%W    (0)
	%W    (TM_SUP)

	DB    K_DOWN
	%W    (0)
	%W    (TM_SDO)

	DB    0

T_PWM0:	%W    (0FFE0H)
	DB    LCD_CLR,'PWM0 :',0

T_PWM1:	%W    (0FFE1H)
	DB    LCD_CLR,'PWM1 :',0

DEVER_T:DB    LCD_CLR,'%VERSION'
	DB    C_LIN2 ,' (c) PiKRON 1996',0

; *******************************************************************
; Testovaci rutiny a pamet

RSEG	AA_PO_X

UT_UIAD:DS    40
UT_DATA:DS    40

RSEG	AA_PO_C

UT_INIT:SETB  D4LINE
	SETB  FL_CMAV
	MOV   DPTR,#UI_MV_SX
	MOV   A,#20
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#4
	MOVX  @DPTR,A
	MOV   DPTR,#UT_UIAD
	MOV   UI_AD,DPL
	MOV   UI_AD+1,DPH
	CLR   A
	MOV   DPTR,#EV_BUF
	MOVX  @DPTR,A
	MOV   DPTR,#GR_ACT
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#REF_TIM
	MOVX  @DPTR,A
	RET

UT_TREF:MOV   DPTR,#REF_TIM
	MOVX  A,@DPTR
	JNZ   UT_TRE1
	MOV   DPTR,#REF_PER
	MOVX  A,@DPTR
	MOV   DPTR,#REF_TIM
	MOVX  @DPTR,A
	MOV   A,#1
	RET
UT_TRE1:CLR   A
	RET

UT:     CALL  UT_INIT
	MOV   R7,#ET_RQGR
	%LDR45i(UT_GR15)
	CALL  EV_POST

UT_ML:  CALL  EV_GET
	JZ    UT_ML50
	CALL  EV_DO
	JMP   UT_ML
UT_ML50:CALL  UD_OI
	JB    uLF_INE,UT_ML55
	JB    UDF_RDP,UT_ML57
UT_ML55:CALL  UT_TREF
	JZ    UT_ML60
	CALL  UD_REFR
UT_ML57:CLR   UDF_RDP
	SETB  FL_REFR
	SJMP  UT_ML65
UT_ML60:CALL  UT_ULED
UT_ML65:JMP   UT_ML

UT_ULED:MOV   DPTR,#IP1_ST
	MOV   R2,#1 SHL LFB_PUMP1
	CALL  UT_USTS
	MOV   DPTR,#IP2_ST
	MOV   R2,#1 SHL LFB_PUMP2
	CALL  UT_USTS
	MOV   DPTR,#ID1_ST
	MOV   R2,#1 SHL LFB_DETEC
	CALL  UT_USTS
	CALL  G_SEK_ST
	MOV   R2,#1 SHL LFB_SAMPL
	CALL  UT_UST1
	CALL  G_TMC_ST
	MOV   R2,#1 SHL LFB_TEMPER
	CALL  UT_UST1
	JMP   LEDWR

UT_USTS:MOVX  A,@DPTR	; kontrola aktualnosti stavu
	ANL   A,#MUP_DOK
	JZ    UT_UST7
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
UT_UST1:MOV   A,R5
	JB    ACC.7,UT_UST7
	ORL   A,R4
UT_UST2:JZ    UT_UST5
UT_UST3:MOV   A,R2		; On
	CPL   A
	ANL   LEB_FLG,A
	CPL   A
	ORL   LED_FLG,A
	RET
UT_UST5:MOV   A,R2		; Off
	CPL   A
	ANL   LEB_FLG,A
	ANL   LED_FLG,A
	RET
UT_UST7:MOV   A,R2		; Error
	ORL   LEB_FLG,A
	RET

UT_G1K3:MOV   R7,#0
UT_G001:MOV   A,#'*'
	CALL  UI_WR
	DJNZ  R7,UT_G001
	SETB  FL_DRAW
	RET

UT_G1K2:CALL  UI_CLR
UT_G1K1:SETB  FL_DRAW
	RET

UT_GR1:	DS    UT_GR1+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR1+OGR_BTXT-$
	%W    (0)
	DS    UT_GR1+OGR_STXT-$
	%W    (0)
	DS    UT_GR1+OGR_HLP-$
	%W    (0)
	DS    UT_GR1+OGR_SFT-$
	%W    (UT_SF1)
	DS    UT_GR1+OGR_PU-$
	%W    (UT_U1)
	%W    (UT_U2)
	%W    (UT_U3)
	%W    (UT_U4)
	%W    (UT_U5)
	%W    (UT_U6)
	%W    (UT_U7)
	%W    (0)

UT_GR2:	DS    UT_GR2+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR2+OGR_BTXT-$
	%W    (UT_GT2)
	DS    UT_GR2+OGR_STXT-$
	%W    (UT_GS2)
	DS    UT_GR2+OGR_HLP-$
	%W    (0)
	DS    UT_GR2+OGR_SFT-$
	%W    (UT_SF1)
	DS    UT_GR2+OGR_PU-$
	%W    (UT_U3)
	%W    (UT_U4)
	%W    (UT_U5)
	%W    (UT_U6)
	%W    (0)

UT_GT2:	DB    'Tohle je druha skupina',C_NL
	DB    '1 radka',C_NL
	DB    '2 radka',C_NL
	DB    '3 radka',C_NL
	DB    '4 radka',C_NL
	DB    '5 radka',C_NL
	DB    '6 radka',0

UT_GS2:	DB    '=== === === === === ',0

UT_U1:	DS    UT_U1+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1+OU_X-$
	DB    2,1,7,1
	DS    UT_U1+OU_HLP-$
	%W    (0)
	DS    UT_U1+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1+OU_A_RD-$
	%W    (NULL_A)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U1+OU_I_F-$
	DB    82H		; format I_F
	%W    (-1000)		; I_L
	%W    (5000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2:	DS    UT_U2+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2+OU_X-$
	DB    7,0,6,1
	DS    UT_U2+OU_HLP-$
	%W    (0)
	DS    UT_U2+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U2+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UT_DATA)		; DP
	DS    UT_U2+OU_I_F-$
	DB    80H		; format I_F
	%W    (-1000)		; I_L
	%W    (5000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U3:	DS    UT_U3+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U3+OU_MSK-$
	DB    0
	DS    UT_U3+OU_X-$
	DB    10,1,2,1
	DS    UT_U3+OU_HLP-$
	%W    (0)
	DS    UT_U3+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U3+OU_A_RD-$
	%W    (NULL_A)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U3+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U3+OU_I_F-$
	DB    82H		; format I_F
	%W    (-1000)		; I_L
	%W    (5000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U4:	DS    UT_U4+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U4+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U4+OU_X-$
	DB    14,1,2,1
	DS    UT_U4+OU_HLP-$
	%W    (0)
	DS    UT_U4+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U4+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U4+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UT_DATA+2)	; DP
	DS    UT_U4+OU_I_F-$
	DB    00H		; format I_F
	%W    (10)		; I_L
	%W    (50)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U5:	DS    UT_U5+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U5+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U5+OU_X-$
	DB    3,2,7,1
	DS    UT_U5+OU_HLP-$
	%W    (0)
	DS    UT_U5+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U5+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U5+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UT_DATA+4)	; DP
	DS    UT_U5+OU_I_F-$
	DB    83H		; format I_F
	%W    (10)		; I_L
	%W    (50)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U6:	DS    UT_U6+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U6+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U6+OU_X-$
	DB    0,3,7,1
	DS    UT_U6+OU_HLP-$
	%W    (0)
	DS    UT_U6+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U6+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U6+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UT_DATA+4)	; DP
	DS    UT_U6+OU_I_F-$
	DB    80H		; format I_F
	%W    (10)		; I_L
	%W    (50)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U7:	DS    UT_U7+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U7+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7+OU_X-$
	DB    8,3,7,1
	DS    UT_U7+OU_HLP-$
	%W    (0)
	DS    UT_U7+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U7+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U7+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UT_DATA+4)	; DP
	DS    UT_U7+OU_M_S-$
	%W    (0)
	DS    UT_U7+OU_M_P-$
	%W    (0)
	DS    UT_U7+OU_M_F-$
	%W    (TM_SEK)
	DS    UT_U7+OU_M_T-$
	%W    (3)
	%W    (1)
	%W    (UT_U7T1)
	%W    (3)
	%W    (2)
	%W    (UT_U7T2)
	%W    (8000H)
	%W    (8000H)
	%W    (UT_U7T3)
	%W    (0)
UT_U7T1:DB    '+ 1',0
UT_U7T2:DB    '+ 2',0
UT_U7T3:DB    '- ?',0

UT_SF1:	DB    K_PUMP1
	%W    (UT_GR13)
	%W    (GR_RQ23)

	DB    K_PUMP2
	%W    (UT_GR14)
	%W    (GR_RQ23)

	DB    K_SAMPL
	%W    (UT_GR15)
	%W    (GR_RQ23)

	DB    K_DETEC
	%W    (UT_GR16)
	%W    (GR_RQ23)

	DB    K_TEMPER
	%W    (UT_GR17)
	%W    (GR_RQ23)

	DB    K_VALVES
	%W    (UT_GR18)
	%W    (GR_RQ23)

	DB    K_H_C
	%W    (UT_GR81)
	%W    (GR_RQ23)

%IF(0) THEN (
	DB    K_END
	%W    (0)
	%W    (UT_G1K2)

	DB    K_RUN
	%W    (0)
	%W    (UT_G1K1)

	DB    K_INS
	%W    (UT_GR1)
	%W    (GR_RQ23)

	DB    K_DEL
	%W    (UT_GR2)
	%W    (GR_RQ23)

) FI

UT_SFTN:DB    K_RIGHT
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    K_LEFT
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    K_UP
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    K_DOWN
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

UT_SF0:	DB    0

; *******************************************************************
; Komunikace s ostatnimi jednotkami

%OID_ADES(AI_STATUS,STATUS,u2)
%OID_ADES(AI_ERRCLR,ERRCLR,e)
I_PREPSAMP EQU  401
%OID_ADES(AI_PREPSAMP,PREPSAMP,u2)
I_INJECT  EQU   402
%OID_ADES(AI_INJECT,INJECT,e)
I_SINICFG EQU   403
I_CALLPS  EQU   404
%OID_ADES(AI_CALLPS,CALLPS,e)
I_ASPREPARE EQU 405
%OID_ADES(AI_ASPREPARE,ASPREPARE,e)
I_ASLOAD  EQU   406
%OID_ADES(AI_ASLOAD,ASLOAD,e)
I_MANINJ  EQU   407
I_SAMPNUM EQU   408
%OID_ADES(AI_SAMPNUM,SAMPNUM,u2)
I_WHELLSTP EQU  421
I_WHELLPSR EQU  422
I_WHELLHLDT EQU 423
I_WHELLHLD EQU  424
I_WHELLGO EQU   425
I_ASH_UP  EQU   426
I_ASH_DOWN EQU  427
I_ASH_HOME EQU  428
I_ASH_BOT EQU   429
I_LPS_AD1 EQU   431
I_LPS_AD2 EQU   432
I_LPS_TD1 EQU   433
I_LPS_TD2 EQU   434
I_PER_IRC_W EQU 435
I_PER_INJT EQU  436
I_PER_VOL1 EQU  437
I_SAVECFG EQU   451
I_TEMP1	  EQU   301
%OID_ADES(AI_TEMP1,TEMP1,s2/.1)
I_TEMP1RQ EQU   311
%OID_ADES(AI_TEMP1RQ,TEMP1RQ,s2/.1)
I_TEMP_OFF EQU  334
%OID_ADES(AI_TEMP_OFF,TEMP_OFF,e)
I_TEMP_ON EQU   335
%OID_ADES(AI_TEMP_ON,TEMP_ON,e)
I_TEMP_ST EQU	336
%OID_ADES(AI_TEMP_ST,TEMP_ST,u2)
I_TEMP1MC EQU   351
I_TEMP1OC EQU   361
I_TEMP1RD EQU   371
; -----------------
I_FLOW	  EQU   220
I_PCFG	  EQU   224
I_PRESS	  EQU   225
I_PRESS_H EQU   226
I_PRESS_L EQU   227
I_GRAD_B  EQU   231
I_GRAD_C  EQU   232
I_GRADDIR EQU   239
I_AUXUAL  EQU	240
I_AUXVALV EQU	241
I_STOP	  EQU   250
I_START	  EQU   251
I_PURGE	  EQU   252
; -----------------
I_OFF	  EQU   250
%OID_ADES(AI_OFF,OFF,e)
I_ON	  EQU   251
%OID_ADES(AI_ON,ON,e)
I_ZERRO	  EQU   255
I_FIC	  EQU   256
I_CHAi	  EQU   230
I_CHBi	  EQU   231

RSEG	AA_PO_X

; Jednotlive buffery hodnot z jinych jednotek
; prvni byte obsahuje priznaky

IP1_FLOW: DS   3
IP1_PH:	  DS   3
IP1_PL:	  DS   3
IP1_PRESS:DS   3
IP1_ST:	  DS   3
IP1_VALV: DS   3
; --------------
IP2_FLOW: DS   3
IP2_PH:	  DS   3
IP2_PL:	  DS   3
IP2_PRESS:DS   3
IP2_ST:	  DS   3
IP2_GR_B: DS   3
; --------------
ID1_ST:	  DS   3
ID1_CHAi: DS   3
ID1_CHBi: DS   3
ID1_TEMP1:DS   3

RSEG	AA_PO_C

; definice struktury pro popis prenasenych parametru
OUP_FLG	EQU   0		; definicni priznaky
OUP_DL	EQU   1		; delka datove casti bufferu
OUP_DP	EQU   2		; ukazatel na pocatek bufferu (OBP_FLG)
OUP_IID	EQU   4		; identifikacni cislo pristroje
OUP_UC	EQU   5		; delka a prikaz k ziskani a zapisu dat

;popis priznaku v 1.byte bufferu dat OBP_FLG
MUP_DOK	EQU   003H	; nenulova hodnota => data jsou cerstva
MUP_CWR	EQU   004H	; data byla prave zmenena a vyslana

; Pumpa 1 ----------------
UP_P1START:
	DB    4,2	; OUP_IID, OUP_UC
	%W    (I_START)
UP_P1STOP:
	DB    4,4
	%W    (I_STOP)
	%W    (I_ERRCLR)
UP_P1PURGE:
	DB    4,2
	%W    (I_PURGE)
UP_P1FLOW:DB  1,2	; OUP_FLG, OUP_DL
	%W    (IP1_FLOW); OUP_DP
	DB    4,2	; OUP_IID, OUP_UC
	%W    (I_FLOW)
UP_P1PH:DB    1,2
	%W    (IP1_PH)
	DB    4,2
	%W    (I_PRESS_H)
UP_P1PL:DB    1,2
	%W    (IP1_PL)
	DB    4,2
	%W    (I_PRESS_L)
UP_P1PRESS:DB 1,2
	%W    (IP1_PRESS)
	DB    4,2
	%W    (I_PRESS)
UP_P1ST:DB    1,2
	%W    (IP1_ST)
	DB    4,2
	%W    (I_STATUS)
UP_P1VALV:DB  1,2	; Ovladani ventilu pres AUX
	%W    (IP1_VALV)
	DB    4,2
	%W    (I_AUXVALV)

; Pumpa 2 ----------------
UP_P2START:
	DB    5,2
	%W    (I_START)
UP_P2STOP:
	DB    5,4
	%W    (I_STOP)
	%W    (I_ERRCLR)
UP_P2PURGE:
	DB    5,2
	%W    (I_PURGE)
UP_P2FLOW:DB  1,2
	%W    (IP2_FLOW)
	DB    5,2
	%W    (I_FLOW)
UP_P2PH:DB    1,2
	%W    (IP2_PH)
	DB    5,2
	%W    (I_PRESS_H)
UP_P2PL:DB    1,2
	%W    (IP2_PL)
	DB    5,2
	%W    (I_PRESS_L)
UP_P2PRESS:DB 1,2
	%W    (IP2_PRESS)
	DB    5,2
	%W    (I_PRESS)
UP_P2ST:DB    1,2
	%W    (IP2_ST)
	DB    5,2
	%W    (I_STATUS)
UP_P2DIRGA:DB 5,4	; Prime ovladani gradientu
	%W    (I_GRADDIR)
	%W    (0C0H)
UP_P2DIRGB:DB 5,4
	%W    (I_GRADDIR)
	%W    (0C1H)
UP_P2DIRGC:DB 5,4
	%W    (I_GRADDIR)
	%W    (0C2H)
UP_P2GR_B:DB  1,2
	%W    (IP2_GR_B)
	DB    5,2
	%W    (I_GRAD_B)

; Detektor ----------------
UP_D1ON:DB    6,2
	%W    (I_ON)
UP_D1OFF:
	DB    6,4
	%W    (I_OFF)
	%W    (I_ERRCLR)
UP_D1ZER:
	DB    6,2
	%W    (I_ZERRO)
UP_D1FIC:
	DB    6,2
	%W    (I_FIC)
UP_D1ST:DB    1,2
	%W    (ID1_ST)
	DB    6,2
	%W    (I_STATUS)
UP_D1CHAi:
	DB    1,2
	%W    (ID1_CHAi)
	DB    6,2
	%W    (I_CHAi)
UP_D1CHBi:
	DB    1,2
	%W    (ID1_CHBi)
	DB    6,2
	%W    (I_CHBi)
UP_D1T1:DB    1,2
	%W    (ID1_TEMP1)
	DB    6,2
	%W    (I_TEMP1)

; Dotazovy cyklus
UPP_1:	%W    (UP_P1FLOW)
	%W    (UP_P1PH)
	%W    (UP_P1PL)
	%W    (UP_P1PRESS)
	%W    (UP_P1ST)
	%W    (UP_P1VALV)
	;-----------------
	%W    (UP_P2FLOW)
	%W    (UP_P2PH)
	%W    (UP_P2PL)
	%W    (UP_P2PRESS)
	%W    (UP_P2ST)
	%W    (UP_P2GR_B)
	;-----------------
	%W    (UP_D1ST)
	%W    (UP_D1CHAi)
	%W    (UP_D1CHBi)
	%W    (UP_D1T1)
	%W    (0)

UC_HSND:DB    11H,0,0

; Otevre pocatek objektove zpravy R5 pro R4
UC_OP:  CLR   F0
	CALL  uL_O_OP
	MOV   DPTR,#UC_HSND
	%LDR45i(3)
	CALL  uL_WR
	RET

; vysle pascalsky string
UC_WRS:	MOVX  A,@DPTR
	INC   DPTR
	MOV   R4,A
	MOV   R5,#0
	JMP   uL_WR

; vysle zpravu s prikazem definovanym na adrese R23
UC_SND:	MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#10H
	INC   DPTR
	PUSH  DPL
	PUSH  DPH
	CALL  UC_OP
	POP   DPH
	POP   DPL
	CALL  UC_WRS
	CALL  uL_O_CL
	RET

UI_A_DP:MOV   A,#OU_DP
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OU_DP+1
	MOVC  A,@A+DPTR
	MOV   DPL,R0
	MOV   DPH,A
	RET

UDS_DP:	MOV   A,#OUP_DP
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OUP_DP+1
	MOVC  A,@A+DPTR
	MOV   DPL,R0
	MOV   DPH,A
	RET

; Nacte integer hodnotu z bufferu do R45
UR_ULi:	CALL  UI_A_DP
	CALL  UDS_DP
	MOVX  A,@DPTR	; Priznaky
	ANL   A,#MUP_DOK
	JZ    UR_ULi1
	INC   DPTR
	CALL  xLDR45i
	MOV   A,#1
	RET
UR_ULi1:SETB  F0
	RET

; Zapise hodnotu R45 do bufferu a vysle data do jednotky
UW_ULi: CALL  UI_A_DP
	PUSH  DPL
	PUSH  DPH
	MOV   A,#OUP_IID
	MOVC  A,@A+DPTR
	MOV   R1,A
	CALL  UDS_DP
	; Priznaky
	INC   DPTR
	CALL  xSVR45i
UW_ULiP:MOV   A,R1
	MOV   R4,A
	MOV   R5,#10H
	CALL  UC_OP
	POP   DPH
	POP   DPL
	PUSH  DPL
	PUSH  DPH
	MOV   A,DPL
	ADD   A,#OUP_UC
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#0
	MOV   DPH,A
	CALL  UC_WRS
	POP   DPH
	POP   DPL
	MOV   A,#OUP_DL
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   R5,#0
	CALL  UDS_DP
	MOVX  A,@DPTR	; Priznaky
	ORL   A,#MUP_CWR; data byla zmenena a vyslana
	MOVX  @DPTR,A
	INC   DPTR
	CALL  uL_WR
	CALL  uL_O_CL
	RET

RSEG	AA_PO_B

UD_FLG:	DS    1
UDF_RIP	BIT   UD_FLG.0	; Jiz zapsano I_RDRQ
UDF_RDP	BIT   UD_FLG.1	; Prijmuta zprava s I_RDRQ

RSEG	AA_PO_X

UD_ADR:	DS    1		; adresat prave otevrene zpravy nebo -1
UD_TMP:	DS    16
UD_AUPP:DS    2		; ukazatel na dotazovaci seznam
UD_NUPP:DS    2		; ukazatel do seznamu pri zpracovavani
UD_NUP:	DS    2		; ukazatel na zpracovavany popis parametru UP

RSEG	AA_PO_C

UDC_RE1:%W    (I_RDRQ)
UDC_RE2:%W    (0)

; Hleda k objetku OID jeho popis UP
; vstup:  R45   ... OID objektu
; vystup: DPTR  ... UP popis objektu
;	  ACC=0 ... OID nenalezen
FND_NUP:MOV   DPTR,#UD_ADR
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   DPTR,#UD_AUPP
	CALL  xMDPDP
FND_NU1:MOVX  A,@DPTR
	INC   DPTR
	MOV   R0,A
	MOVX  A,@DPTR
	INC   DPTR
	MOV   R1,A
	ORL   A,R0
	JZ    FND_NU9
	PUSH  DPL
	PUSH  DPH
	MOV   DPL,R0
	MOV   DPH,R1
	MOV   A,#OUP_IID
	MOVC  A,@A+DPTR
	XRL   A,R3
	JNZ   FND_NU8
	MOV   A,#OUP_UC+1
	MOVC  A,@A+DPTR
	XRL   A,R4
	JNZ   FND_NU8
	MOV   A,#OUP_UC+2
	MOVC  A,@A+DPTR
	XRL   A,R5
	JNZ   FND_NU8
	DEC   SP
	DEC   SP
	MOV   A,#1
	RET
FND_NU8:POP   DPH
	POP   DPL
	JMP   FND_NU1
FND_NU9:CLR   A
	RET

; Uzavre prave generovanou zpravu pro UD_ADR
; spravne zakonci I_RDRQ
UD_RECL:MOV   DPTR,#UD_ADR
	MOVX  A,@DPTR
	INC   A
	JZ    UD_REC2
	JNB   UDF_RIP,UD_REC1
	CLR   UDF_RIP
	MOV   DPTR,#UDC_RE2
	%LDR45i(2)
	CALL  UL_WR
UD_REC1:JMP   UL_O_CL
UD_REC2:RET

; Otevre novou objektovou zpravu pro R4, je-li to potreba
UD_REOP:MOV   DPTR,#UD_NUP
	CALL  xMDPDP
	MOV   A,#OUP_IID
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   DPTR,#UD_ADR
	MOVX  A,@DPTR
	XRL   A,R4
	JZ    UD_REO9
	MOV   A,R4
	PUSH  ACC
	CALL  UD_RECL
	POP   ACC
	MOV   DPTR,#UD_ADR
	MOVX  @DPTR,A
	MOV   R4,A
	MOV   R5,#10H
	JMP   UC_OP
UD_REO9:RET

; Vytvori dotazy podle seznamu UD_AUPP
UD_REFR:CLR   UDF_RIP
	MOV   DPTR,#UD_ADR
	MOV   A,#-1
	MOVX  @DPTR,A
	MOV   DPTR,#UD_AUPP
	CALL  xLDR45i
	MOV   DPTR,#UD_NUPP
	CALL  xSVR45i
UD_RE10:MOV   DPTR,#UD_NUPP
	CALL  xLDR45i
	MOV   A,R4
	ADD   A,#2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	ADDC  A,#0
	MOVX  @DPTR,A
	MOV   DPL,R4
	MOV   DPH,R5
	CALL  xLDR45i
	ORL   A,R4
	JNZ   UD_RE20
	JMP   UD_RECL
UD_RE20:MOV   DPTR,#UD_NUP
	CALL  xSVR45i
	MOV   DPL,R4
	MOV   DPH,R5
	MOVX  A,@DPTR
	JNB   ACC.0,UD_RE10
	MOV   A,#OUP_DP
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OUP_DP+1
	MOVC  A,@A+DPTR
	MOV   DPL,R0
	MOV   DPH,A
	MOVX  A,@DPTR	;  Priznaky bufferu dat
	ANL   A,#MUP_DOK
	JZ    UD_RE23
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A	; informuje o starnuti dat v bufferu
UD_RE23:CALL  UD_REOP
	JB    UDF_RIP,UD_RE25
	MOV   DPTR,#UDC_RE1
	%LDR45i(2)
	CALL  uL_WR
	SETB  UDF_RIP
UD_RE25:MOV   DPTR,#UD_NUP
	MOVX  A,@DPTR
	ADD   A,#OUP_UC
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#0
	MOV   DPL,R0
	MOV   DPH,A
	CALL  UC_WRS
	JMP   UD_RE10

; Zpracovavani prijatych zprav
UD_OI:  JBC   uLF_INE,UD_OI04
	RET
UD_OI04:CLR   F0
	CALL  uL_I_OP	; vraci R4 Adr a R5 Com
	JNB   F0,UD_OI10
	RET
UD_OI10:SETB  uLF_INE
	CJNE  R5,#11H,UD_OI50
	MOV   DPTR,#UD_ADR
	MOV   A,R4
	MOVX  @DPTR,A
	CLR   UDF_RIP
	MOV   DPTR,#UD_TMP
	%LDR45i(3)
	CALL  uL_RD
	MOV   DPTR,#UD_TMP
	%LDR45i(2)
	CALL  uL_RD
	MOV   DPTR,#UD_TMP
	MOVX  A,@DPTR
	CJNE  A,#I_RDRQ+1,UD_OI49
	INC   DPTR
	MOVX  A,@DPTR
	JNZ   UD_OI49
	SETB  UDF_RDP
UD_OI20:MOV   DPTR,#UD_TMP
	%LDR45i(2)
	CALL  uL_RD
	MOV   DPTR,#UD_TMP
	CALL  xLDR45i
	ORL   A,R4
	JZ    UD_OI49		; Konec RDRQ
	CALL  FND_NUP
	JZ    UD_OI49		; Input error
	MOV   A,#OUP_DL
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   R5,#0
	CALL  UDS_DP
	MOVX  A,@DPTR		; Priznaky bufferu dat
	ORL   A,#MUP_DOK
	MOVX  @DPTR,A		; nova data prisla
	INC   DPTR
	CALL  uL_RD
	JMP   UD_OI20
UD_OI49:CALL  uL_I_CL
	RET
UD_OI50:JMP   UI_PRAO

; inicializace objektovych komunikaci
uL_OINI:%LDR45i (UPP_1)
	MOV   DPTR,#UD_AUPP	; seznam parametru ostatnich jednotek
	CALL  xSVR45i
	%LDR45i (OID_1IN)	; seznam prijimanych prikazu
	%LDR67i (OID_1OUT)	; seznam vysilanych prikazu
	JMP    US_INIT

; Identifikace typu pristroje
PUBLIC	uL_IDB,uL_IDE
uL_IDB: DB    '.mt %VERSION .uP 51x',0
uL_IDE:

; *******************************************************************
; prikazy pro AA_POD

G_STATUS:JMP  G_SEK_ST

ERRCLR_U:JMP  SEK_INI

OFF_U:	JMP   SEK_INI

PREP_U: MOV   DPTR,#SEK_ST
	; zpracovat predchozi stav davkovace
	MOV   DPTR,#SAMPNUM
	CALL  xSVR45i
	JMP   TM_PREP

INJ_U:	JMP   TM_INJ

SAVECFG_U:%LDR23i(EEC_SER); Ulozeni parametru do EEPROM
	JMP   EEP_WRS


; Prijimane hodnoty

OID_T	SET   $
	%W    (I_ERRCLR)
	%W    (OID_ISTD)
	%W    (AI_ERRCLR)
	%W    (ERRCLR_U)

%OID_NEW(I_SAVECFG ,0)
	%W    (SAVECFG_U)

%OID_NEW(I_WHELLSTP,0)
	%W    (UI_INT)
	%W    (0)
	%W    (TM_STPSER)

%OID_NEW(I_WHELLHLDT,0)
	%W    (TM_STPHLDT)

%OID_NEW(I_WHELLHLD,0)
	%W    (TM_STPHLD)

%OID_NEW(I_WHELLGO,0)
	%W    (TM_STP2)

%OID_NEW(I_ASH_UP,0)
	%W    (TM_ASH_UP)

%OID_NEW(I_ASH_DOWN,0)
	%W    (TM_ASH_DOWN)

%OID_NEW(I_ASH_HOME,0)
	%W    (TM_ASH_HOME)

%OID_NEW(I_OFF,AI_OFF)
	%W    (OFF_U)

%OID_NEW(I_TEMP1,AI_TEMP1)
	%W    (UI_INT)
	%W    (0)
	%W    (S_TEMP)
	%W    (TMC1_RT)

%OID_NEW(I_TEMP1RQ,AI_TEMP1RQ)
	%W    (UI_INT)
	%W    (0)
	%W    (S_TEMP)
	%W    (TMC1_RT)

%OID_NEW(I_TEMP_OFF,AI_TEMP_OFF)
	%W    (TMC_OFF)

%OID_NEW(I_TEMP_ON,AI_TEMP_ON)
	%W    (TMC_ON)

%OID_NEW(I_TEMP1MC,0)
	%W    (UI_INT)
	%W    (TMC1_MC)
	%W    (0)

%OID_NEW(I_TEMP1OC,0)
	%W    (UI_INT)
	%W    (TMC1_OC)
	%W    (0)

%OID_NEW(I_INJECT,AI_INJECT)
	%W    (INJ_U)

%OID_NEW(I_CALLPS,AI_CALLPS)
	%W    (TM_LPS)

%OID_NEW(I_ASPREPARE,AI_ASPREPARE)
	%W    (TM_PREP)

%OID_NEW(I_ASLOAD,AI_ASLOAD)
	%W    (TM_LOAD)

%OID_NEW(I_MANINJ ,0)
	%W    (TM_MAN)

%OID_NEW(I_ASH_BOT,0)
	%W    (UI_INT)
	%W    (ASH1_BOT)
	%W    (0)

%OID_NEW(I_LPS_TD1,0)
	%W    (UI_INT)
	%W    (LPS_TD1)
	%W    (0)

%OID_NEW(I_LPS_TD2,0)
	%W    (UI_INT)
	%W    (LPS_TD2)
	%W    (0)

%OID_NEW(I_PER_INJT,0)
	%W    (UI_INT)
	%W    (P_INJT)
	%W    (0)

%OID_NEW(I_PER_VOL1,0)
	%W    (UI_INT)
	%W    (P_VOL1)
	%W    (0)

%OID_NEW(I_PER_IRC_W,0)
	%W    (UI_INT)
	%W    (P_IRC_W)
	%W    (0)

%OID_NEW(I_SAMPNUM,AI_SAMPNUM)
	%W    (UI_INT)
	%W    (SAMPNUM)
	%W    (0)

%OID_NEW(I_PREPSAMP,AI_PREPSAMP)
	%W    (UI_INT)
	%W    (0)
	%W    (PREP_U)

%OID_NEW(I_SINICFG,0)
	%W    (UI_INT)
	%W    (0)
	%W    (TM_INICFG)

OID_1IN SET   OID_T

; Vysilane hodnoty

OID_T	SET   0

%OID_NEW(I_WHELLPSR,0)
	%W    (UO_INT)
	%W    (0)
	%W    (STP_PSR)

%OID_NEW(I_LPS_TD1,0)
	%W    (UO_INT)
	%W    (LPS_TD1)
	%W    (0)

%OID_NEW(I_LPS_TD2,0)
	%W    (UO_INT)
	%W    (LPS_TD2)
	%W    (0)

%OID_NEW(I_LPS_AD1,0)
	%W    (UO_INT)
	%W    (LPS_AD1)
	%W    (0)

%OID_NEW(I_LPS_AD2,0)
	%W    (UO_INT)
	%W    (LPS_AD2)
	%W    (0)

%OID_NEW(I_STATUS,AI_STATUS)
	%W    (UO_INT)
	%W    (0)
	%W    (G_STATUS)

%OID_NEW(I_SAMPNUM,AI_SAMPNUM)
	%W    (UO_INT)
	%W    (SAMPNUM)
	%W    (0)

%OID_NEW(I_ASH_BOT,0)
	%W    (UO_INT)
	%W    (ASH1_BOT)
	%W    (0)

%OID_NEW(I_PER_INJT,0)
	%W    (UO_INT)
	%W    (P_INJT)
	%W    (0)

%OID_NEW(I_PER_VOL1,0)
	%W    (UO_INT)
	%W    (P_VOL1)
	%W    (0)

%OID_NEW(I_PER_IRC_W,0)
	%W    (UO_INT)
	%W    (P_IRC_W)
	%W    (0)

%OID_NEW(I_TEMP1,AI_TEMP1)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TEMP)
	%W    (TMC1_AT)

%OID_NEW(I_TEMP1RQ,AI_TEMP1RQ)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TEMP)
	%W    (TMC1_RT)

%OID_NEW(I_TEMP_ST,AI_TEMP_ST)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TMC_ST)

%OID_NEW(I_TEMP1MC,0)
	%W    (UO_INT)
	%W    (TMC1_MC)
	%W    (0)

%OID_NEW(I_TEMP1OC,0)
	%W    (UO_INT)
	%W    (TMC1_OC)
	%W    (0)

%OID_NEW(I_TEMP1RD,0)
	%W    (UO_INT)
	%W    (TMC1_RD)
	%W    (0)

OID_1OUT SET  OID_T


; *******************************************************************
; Manualni rezim rezim

; ---------------------------------
; Pumpa 1
UT_GR13:DS    UT_GR13+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR13+OGR_BTXT-$
	%W    (UT_GT13)
	DS    UT_GR13+OGR_STXT-$
	%W    (UT_GS13)
	DS    UT_GR13+OGR_HLP-$
	%W    (0)
	DS    UT_GR13+OGR_SFT-$
	%W    (UT_SF13)
	DS    UT_GR13+OGR_PU-$
	%W    (UT_U1301)
	%W    (UT_U1302)
	%W    (UT_U1303)
	%W    (UT_U1304)
	%W    (UT_U1305)
	%W    (0)

UT_GT13:DB    '  Pump 1',C_NL
	DB    'FLOW P-H P-L P MPa',0

UT_GS13:DB    '=== === Purg On  Off',0

UT_SF13:DB    K_F5
	%W    (UP_P1STOP)
	%W    (UC_SND)

	DB    K_F4
	%W    (UP_P1START)
	%W    (UC_SND)

	DB    K_F3
	%W    (UP_P1PURGE)
	%W    (UC_SND)

	DB    -1
	%W    (UT_SF1)

UT_U1301:
	DS    UT_U1301+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1301+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1301+OU_X-$
	DB    0,2,4,1
	DS    UT_U1301+OU_HLP-$
	%W    (0)
	DS    UT_U1301+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1301+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1301+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P1FLOW)	; DP
	DS    UT_U1301+OU_I_F-$
	DB    03H		; format I_F
	%W    (0)		; I_L
	%W    (10000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1302:
	DS    UT_U1302+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1302+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1302+OU_X-$
	DB    5,2,3,1
	DS    UT_U1302+OU_HLP-$
	%W    (0)
	DS    UT_U1302+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1302+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1302+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P1PH)		; DP
	DS    UT_U1302+OU_I_F-$
	DB    01H		; format I_F
	%W    (0)		; I_L
	%W    (400)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1303:
	DS    UT_U1303+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1303+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1303+OU_X-$
	DB    9,2,3,1
	DS    UT_U1303+OU_HLP-$
	%W    (0)
	DS    UT_U1303+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1303+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1303+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P1PL)		; DP
	DS    UT_U1303+OU_I_F-$
	DB    01H		; format I_F
	%W    (0)		; I_L
	%W    (400)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1304:
	DS    UT_U1304+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1304+OU_MSK-$
	DB    0
	DS    UT_U1304+OU_X-$
	DB    14,2,4,1
	DS    UT_U1304+OU_HLP-$
	%W    (0)
	DS    UT_U1304+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1304+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1304+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P1PRESS)	; DP
	DS    UT_U1304+OU_I_F-$
	DB    01H		; format I_F
	%W    (0)		; I_L
	%W    (400)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1305:
	DS    UT_U1305+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1305+OU_MSK-$
	DB    0
	DS    UT_U1305+OU_X-$
	DB    9,0,7,1
	DS    UT_U1305+OU_HLP-$
	%W    (0)
	DS    UT_U1305+OU_SFT-$
	%W    (0)
	DS    UT_U1305+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1305+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P1ST)		; DP
	DS    UT_U1305+OU_M_S-$
	%W    (0)
	DS    UT_U1305+OU_M_P-$
	%W    (0)
	DS    UT_U1305+OU_M_F-$
	%W    (0)
	DS    UT_U1305+OU_M_T-$
	%W    (0FF00H)
	%W    (100H)
	%W    (UT_U1305TR)
	%W    (0FFFFH)
	%W    (0)
	%W    (UT_U1305T0)
	%W    (0FFFFH)
	%W    (2)
	%W    (UT_U1305T2)
	%W    (08000H)
	%W    (0)
	%W    (UT_U1305T1)
	%W    (8000H)
	%W    (8000H)
	%W    (UT_U1305TE)
	%W    (0)
UT_U1305T0:DB    'Off',0
UT_U1305T1:DB    'On',0
UT_U1305T2:DB    'Purge',0
UT_U1305TR:DB    'Running',0
UT_U1305TE:DB    'Error',0

; ---------------------------------
; Pumpa 2
UT_GR14:DS    UT_GR14+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR14+OGR_BTXT-$
	%W    (UT_GT14)
	DS    UT_GR14+OGR_STXT-$
	%W    (UT_GS13)
	DS    UT_GR14+OGR_HLP-$
	%W    (0)
	DS    UT_GR14+OGR_SFT-$
	%W    (UT_SF14)
	DS    UT_GR14+OGR_PU-$
	%W    (UT_U1401)
	%W    (UT_U1402)
	%W    (UT_U1403)
	%W    (UT_U1404)
	%W    (UT_U1405)
	%W    (0)

UT_GT14:DB    '  Pump 2',C_NL
	DB    'FLOW P-H P-L P MPa',0

UT_SF14:DB    K_F5
	%W    (UP_P2STOP)
	%W    (UC_SND)

	DB    K_F4
	%W    (UP_P2START)
	%W    (UC_SND)

	DB    K_F3
	%W    (UP_P2PURGE)
	%W    (UC_SND)

	DB    -1
	%W    (UT_SF1)

UT_U1401:
	DS    UT_U1401+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1401+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1401+OU_X-$
	DB    0,2,4,1
	DS    UT_U1401+OU_HLP-$
	%W    (0)
	DS    UT_U1401+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1401+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1401+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P2FLOW)	; DP
	DS    UT_U1401+OU_I_F-$
	DB    03H		; format I_F
	%W    (0)		; I_L
	%W    (10000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1402:
	DS    UT_U1402+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1402+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1402+OU_X-$
	DB    5,2,3,1
	DS    UT_U1402+OU_HLP-$
	%W    (0)
	DS    UT_U1402+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1402+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1402+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P2PH)		; DP
	DS    UT_U1402+OU_I_F-$
	DB    01H		; format I_F
	%W    (0)		; I_L
	%W    (400)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1403:
	DS    UT_U1403+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1403+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1403+OU_X-$
	DB    9,2,3,1
	DS    UT_U1403+OU_HLP-$
	%W    (0)
	DS    UT_U1403+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1403+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1403+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P2PL)		; DP
	DS    UT_U1403+OU_I_F-$
	DB    01H		; format I_F
	%W    (0)		; I_L
	%W    (400)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1404:
	DS    UT_U1404+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1404+OU_MSK-$
	DB    0
	DS    UT_U1404+OU_X-$
	DB    14,2,4,1
	DS    UT_U1404+OU_HLP-$
	%W    (0)
	DS    UT_U1404+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1404+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1404+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P2PRESS)	; DP
	DS    UT_U1404+OU_I_F-$
	DB    01H		; format I_F
	%W    (0)		; I_L
	%W    (400)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1405:
	DS    UT_U1405+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1405+OU_MSK-$
	DB    0
	DS    UT_U1405+OU_X-$
	DB    9,0,7,1
	DS    UT_U1405+OU_HLP-$
	%W    (0)
	DS    UT_U1405+OU_SFT-$
	%W    (0)
	DS    UT_U1405+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1405+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P2ST)		; DP
	DS    UT_U1405+OU_M_S-$
	%W    (0)
	DS    UT_U1405+OU_M_P-$
	%W    (0)
	DS    UT_U1405+OU_M_F-$
	%W    (0)
	DS    UT_U1405+OU_M_T-$
	%W    (0FF00H)
	%W    (100H)
	%W    (UT_U1305TR)
	%W    (0FFFFH)
	%W    (0)
	%W    (UT_U1305T0)
	%W    (0FFFFH)
	%W    (2)
	%W    (UT_U1305T2)
	%W    (08000H)
	%W    (0)
	%W    (UT_U1305T1)
	%W    (8000H)
	%W    (8000H)
	%W    (UT_U1305TE)
	%W    (0)

; ---------------------------------
; Sampler
UT_GR15:DS    UT_GR15+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR15+OGR_BTXT-$
	%W    (UT_GT15)
	DS    UT_GR15+OGR_STXT-$
	%W    (UT_GS15)
	DS    UT_GR15+OGR_HLP-$
	%W    (0)
	DS    UT_GR15+OGR_SFT-$
	%W    (UT_SF15)
	DS    UT_GR15+OGR_PU-$
	%W    (UT_U1501)
	%W    (UT_U1502)
	%W    (0)

UT_GT15:DB    '  Sampler',C_NL
	DB    'Number ',0

UT_GS15:DB    'Cal Pre Load Man Off',0

UT_SF15:DB    K_F1
	%W    (0)
	%W    (TM_LPS)

	DB    K_F2
	%W    (0)
	%W    (TM_PREP)

	DB    K_F3
	%W    (0)
	%W    (TM_LOAD)

	DB    K_F4
	%W    (0)
	%W    (TM_MAN)

	DB    K_F5
	%W    (0)
	%W    (SEK_INI)

	DB    -1
	%W    (UT_SF1)

UT_U1501:
	DS    UT_U1501+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1501+OU_MSK-$
	DB    0
	DS    UT_U1501+OU_X-$
	DB    11,0,7,1
	DS    UT_U1501+OU_HLP-$
	%W    (0)
	DS    UT_U1501+OU_SFT-$
	%W    (0)
	DS    UT_U1501+OU_A_RD-$
	%W    (G_SEK_ST)	; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1501+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U1501+OU_M_S-$
	%W    (0)
	DS    UT_U1501+OU_M_P-$
	%W    (0)
	DS    UT_U1501+OU_M_F-$
	%W    (0)
	DS    UT_U1501+OU_M_T-$
	%W    (0FF00H)
	%W    (100H)
	%W    (UT_U1501TR)
	%W    (0FFFFH)
	%W    (0)
	%W    (UT_U1501T0)
	%W    (0FFFFH)
	%W    (00081H)
	%W    (UT_U1501TP)
	%W    (08000H)
	%W    (0)
	%W    (UT_U1501T1)
	%W    (8000H)
	%W    (8000H)
	%W    (UT_U1501TE)
	%W    (0)
UT_U1501T0:DB    'Off',0
UT_U1501T1:DB    'On',0
UT_U1501TP:DB    'Prepared',0
UT_U1501TR:DB    'Running',0
UT_U1501TE:DB    'Error',0

UT_U1502:
	DS    UT_U1502+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1502+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1502+OU_X-$
	DB    0,2,5,1
	DS    UT_U1502+OU_HLP-$
	%W    (0)
	DS    UT_U1502+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1502+OU_A_RD-$
	%W    (UR_Mi)		; A_RD !!!!!!!!!!!!!!!!!
	%W    (UW_Mi)		; A_WR
	DS    UT_U1502+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (SAMPNUM)		; DP
	DS    UT_U1502+OU_I_F-$
	DB    00H		; format I_F
	%W    (1)		; I_L
	%W    (25)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Detector
UT_GR16:DS    UT_GR16+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR16+OGR_BTXT-$
	%W    (UT_GT16)
	DS    UT_GR16+OGR_STXT-$
	%W    (UT_GS16)
	DS    UT_GR16+OGR_HLP-$
	%W    (0)
	DS    UT_GR16+OGR_SFT-$
	%W    (UT_SF16)
	DS    UT_GR16+OGR_PU-$
	%w    (UT_U1601)
	%w    (UT_U1602)
	%w    (UT_U1603)
	%W    (0)

UT_GT16:DB    '  Detector',C_NL
	DB    ' Green   Blue',0

UT_GS16:DB    '=== FIC Zer  On  Off',0

UT_SF16:DB    K_F5
	%W    (UP_D1OFF)
	%W    (UC_SND)

	DB    K_F4
	%W    (UP_D1ON)
	%W    (UC_SND)

	DB    K_F3
	%W    (UP_D1ZER)
	%W    (UC_SND)

	DB    K_F2
	%W    (UP_D1FIC)
	%W    (UC_SND)

	DB    -1
	%W    (UT_SF1)

UT_U1601:
	DS    UT_U1601+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1601+OU_MSK-$
	DB    0
	DS    UT_U1601+OU_X-$
	DB    11,0,7,1
	DS    UT_U1601+OU_HLP-$
	%W    (0)
	DS    UT_U1601+OU_SFT-$
	%W    (0)
	DS    UT_U1601+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1601+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_D1ST)		; DP
	DS    UT_U1601+OU_M_S-$
	%W    (0)
	DS    UT_U1601+OU_M_P-$
	%W    (0)
	DS    UT_U1601+OU_M_F-$
	%W    (0)
	DS    UT_U1601+OU_M_T-$
	%W    (0FF00H)
	%W    (100H)
	%W    (UT_U1601TR)
	%W    (0FFFFH)
	%W    (0)
	%W    (UT_U1601T0)
	%W    (08000H)
	%W    (0)
	%W    (UT_U1601T1)
	%W    (8000H)
	%W    (8000H)
	%W    (UT_U1601TE)
	%W    (0)
UT_U1601T0:DB    'Off',0
UT_U1601T1:DB    'On',0
UT_U1601TR:DB    'Running',0
UT_U1601TE:DB    'Error',0

UT_U1602:
	DS    UT_U1602+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1602+OU_MSK-$
	DB    0
	DS    UT_U1602+OU_X-$
	DB    0,2,7,1
	DS    UT_U1602+OU_HLP-$
	%W    (0)
	DS    UT_U1602+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1602+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1602+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_D1CHBi)	; DP
	DS    UT_U1602+OU_I_F-$
	DB    084H		; format I_F
	%W    (8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1603:
	DS    UT_U1603+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1603+OU_MSK-$
	DB    0
	DS    UT_U1603+OU_X-$
	DB    8,2,7,1
	DS    UT_U1603+OU_HLP-$
	%W    (0)
	DS    UT_U1603+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1603+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1603+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_D1CHAi)	; DP
	DS    UT_U1603+OU_I_F-$
	DB    084H		; format I_F
	%W    (8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Temperatures
UT_GR17:DS    UT_GR17+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR17+OGR_BTXT-$
	%W    (UT_GT17)
	DS    UT_GR17+OGR_STXT-$
	%W    (UT_GS17)
	DS    UT_GR17+OGR_HLP-$
	%W    (0)
	DS    UT_GR17+OGR_SFT-$
	%W    (UT_SF17)
	DS    UT_GR17+OGR_PU-$
	%W    (UT_U1701)
	%W    (UT_U1702)
	%W    (UT_U1703)
	%W    (UT_U1704)
	%W    (0)

UT_GT17:DB    '  Temperatures',C_NL
	DB    'Col       tol   ',25H,C_NL
	DB    'Re        tol   ',25H,0

UT_GS17:DB    '=== === ===  COn COf',0

UT_SF17:DB    K_F5
	%W    (0)
	%W    (TMC_OFF)

	DB    K_F4
	%W    (0)
	%W    (TMC_ON)

	DB    -1
	%W    (UT_SF1)

UT_U1701:
	DS    UT_U1701+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1701+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1701+OU_X-$
	DB    4,1,5,1
	DS    UT_U1701+OU_HLP-$
	%W    (0)
	DS    UT_U1701+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1701+OU_A_RD-$
	%W    (UR_TEMP)		; A_RD
	%W    (UW_TEMP)		; A_WR
	DS    UT_U1701+OU_DPSI-$
	%W    (TMC1_RT)         ; DPSI
	%W    (TMC1_AT)		; DP
	DS    UT_U1701+OU_I_F-$
	DB    01H		; format I_F
	%W    (350)		; I_L
	%W    (1000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1702:
	DS    UT_U1702+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1702+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1702+OU_X-$
	DB    13,1,5,1
	DS    UT_U1702+OU_HLP-$
	%W    (0)
	DS    UT_U1702+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1702+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1702+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TMC1_EN)		; DP
	DS    UT_U1702+OU_I_F-$
	DB    80H		; format I_F
	%W    (0)		; I_L
	%W    (100)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1703:
	DS    UT_U1703+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1703+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1703+OU_X-$
	DB    4,2,5,1
	DS    UT_U1703+OU_HLP-$
	%W    (0)
	DS    UT_U1703+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1703+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1703+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_D1T1)		; DP
	DS    UT_U1703+OU_I_F-$
	DB    01H		; format I_F
	%W    (350)		; I_L
	%W    (1500)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1704:
	DS    UT_U1704+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1704+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1704+OU_X-$
	DB    13,2,3,1
	DS    UT_U1704+OU_HLP-$
	%W    (0)
	DS    UT_U1704+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1704+OU_A_RD-$
	%W    (NULL_A)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1704+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U1704+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (100)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Ventily
UT_GR18:DS    UT_GR18+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR18+OGR_BTXT-$
	%W    (UT_GT18)
	DS    UT_GR18+OGR_STXT-$
	%W    (UT_GS18)
	DS    UT_GR18+OGR_HLP-$
	%W    (0)
	DS    UT_GR18+OGR_SFT-$
	%W    (UT_SF18)
	DS    UT_GR18+OGR_PU-$
	%W    (UT_U1801)
	%W    (UT_U1802)
	%W    (0)

UT_GT18:DB    '  Valves',C_NL
	DB    'Buff  Reagenc',0

UT_GS18:DB    '=== === ==== NHD H2O',0

UT_SF18:DB    K_F5
	%W    (UP_P2DIRGA)
	%W    (UC_SND)

	DB    K_F4
	%W    (UP_P2DIRGB)
	%W    (UC_SND)

	DB    -1
	%W    (UT_SF1)

UT_U1801:
	DS    UT_U1801+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1801+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1801+OU_X-$
	DB    0,2,4,1
	DS    UT_U1801+OU_HLP-$
	%W    (0)
	DS    UT_U1801+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1801+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1801+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P1VALV)	; DP
	DS    UT_U1801+OU_I_F-$
	DB    00H		; format I_F
	%W    (1)		; I_L
	%W    (8)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1802:
	DS    UT_U1802+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1802+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1802+OU_X-$
	DB    6,2,6,1
	DS    UT_U1802+OU_HLP-$
	%W    (0)
	DS    UT_U1802+OU_SFT-$
	%W    (0)
	DS    UT_U1802+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1802+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P2GR_B)	; DP
	DS    UT_U1802+OU_M_S-$
	%W    (0)
	DS    UT_U1802+OU_M_P-$
	%W    (0)
	DS    UT_U1802+OU_M_F-$
	%W    (0)
	DS    UT_U1802+OU_M_T-$
	%W    (0FFFFH)
	%W    (0)
	%W    (UT_U1802T0)
	%W    (08000H)
	%W    (0)
	%W    (UT_U1802T1)
	%W    (0)
UT_U1802T0:DB    '   H2O',0
UT_U1802T1:DB    '   NHD',0

; *******************************************************************
; Servisni rezim

UT_GR81:DS    UT_GR81+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR81+OGR_BTXT-$
	%W    (UT_GT81)
	DS    UT_GR81+OGR_STXT-$
	%W    (UT_GS81)
	DS    UT_GR81+OGR_HLP-$
	%W    (0)
	DS    UT_GR81+OGR_SFT-$
	%W    (UT_SF1)
	DS    UT_GR81+OGR_PU-$
	%W    (UT_U8101)	; Exit
	%W    (UT_U8116)	; LPS_AD1
	%W    (UT_U8117)	; LPS_AD2
	%W    (UT_U8118)	; LPS_TD1
	%W    (UT_U8119)	; LPS_TD2
	%W    (UT_U8121)	; Handler
	%W    (UT_U8122)	; ASH1_BOT
	%W    (UT_U8123)	; Inject
	%W    (UT_U8124)	; SAMPNUM
	%W    (UT_U8151)	; TMC1 ADC
	%W    (UT_U8152)	; TMC1_OC
	%W    (UT_U8153)	; TMC1_MC
	%W    (UT_U8161)	; P_IRC_W
	%W    (UT_U8162)	; P_INJT
	%W    (UT_U8163)	; P_VOL1
	%W    (UT_U8190)	; Save
	%W    (0)

UT_GT81:DB    '  Service',C_NL
	DB    'Sens',C_NL
	DB    'Sens td',C_NL
	DB    C_NL
	DB    C_NL
	DB    'Temp ADC',C_NL
	DB    'Temp Offs',C_NL
	DB    'Temp Slop',C_NL
	DB    'Per IRC',C_NL
	DB    'Inject T',C_NL
	DB    'Fill Vol',0

UT_GS81:DB    '=== === ==== === ===',0

UT_U8101: ; Exit
	DS    UT_U8101+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U8101+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8101+OU_X-$
	DB    12,0,4,1
	DS    UT_U8101+OU_HLP-$
	%W    (0)
	DS    UT_U8101+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8101+OU_B_S-$
	%W    (UT_US8101)
	DS    UT_U8101+OU_B_P-$
	%W    (UT_GR15)
	DS    UT_U8101+OU_B_F-$
	%W    (GR_RQ23)
	DS    UT_U8101+OU_B_T-$
	DB    'Exit',0

UT_US8101:DB  '=== === ==== === ===',0

; ---------------------------------
; Snimace bublin

UT_U8116: ; LPS_AD1
	DS    UT_U8116+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8116+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8116+OU_X-$
	DB    5,1,5,1
	DS    UT_U8116+OU_HLP-$
	%W    (0)
	DS    UT_U8116+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8116+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U8116+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (LPS_AD1)		; DP
	DS    UT_U8116+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8117: ; LPS_AD2
	DS    UT_U8117+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8117+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8117+OU_X-$
	DB    11,1,5,1
	DS    UT_U8117+OU_HLP-$
	%W    (0)
	DS    UT_U8117+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8117+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U8117+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (LPS_AD2)		; DP
	DS    UT_U8117+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8118: ; LPS_TD1
	DS    UT_U8118+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8118+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8118+OU_X-$
	DB    8,2,2,1
	DS    UT_U8118+OU_HLP-$
	%W    (0)
	DS    UT_U8118+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8118+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U8118+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (LPS_TD1)		; DP
	DS    UT_U8118+OU_I_F-$
	DB    000H		; format I_F
	%W    (2)		; I_L
	%W    (99)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8119: ; LPS_TD2
	DS    UT_U8119+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8119+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8119+OU_X-$
	DB    14,2,2,1
	DS    UT_U8119+OU_HLP-$
	%W    (0)
	DS    UT_U8119+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8119+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U8119+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (LPS_TD2)		; DP
	DS    UT_U8119+OU_I_F-$
	DB    000H		; format I_F
	%W    (2)		; I_L
	%W    (99)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Ramenko, Inject, Krokac

UT_U8121: ; Handler
	DS    UT_U8121+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U8121+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8121+OU_X-$
	DB    0,3,7,1
	DS    UT_U8121+OU_HLP-$
	%W    (0)
	DS    UT_U8121+OU_SFT-$
	%W    (UT_SF8121)
	DS    UT_U8121+OU_B_S-$
	%W    (UT_US8121)
	DS    UT_U8121+OU_B_P-$
	%W    (0)
	DS    UT_U8121+OU_B_F-$
	%W    (TM_SEK)
	DS    UT_U8121+OU_B_T-$
	DB    'Handler',0

UT_US8121:DB  '=== Nah Dolu Ini ===',0

UT_SF8121:
	DB    K_F2
	%W    (20H)
	%W    (TM_ASH)

	DB    K_F3
	%W    (30H)
	%W    (TM_ASH)

	DB    K_F4
	%W    (10H)
	%W    (TM_ASH)

	DB    0

UT_U8122: ; ASH1_BOT
	DS    UT_U8122+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8122+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8122+OU_X-$
	DB    9,3,7,1
	DS    UT_U8122+OU_HLP-$
	%W    (0)
	DS    UT_U8122+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8122+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U8122+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (ASH1_BOT)	; DP
	DS    UT_U8122+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (CASH_ZP-CASH_HR)	; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8123: ; Inject
	DS    UT_U8123+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U8123+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8123+OU_X-$
	DB    0,4,6,1
	DS    UT_U8123+OU_HLP-$
	%W    (0)
	DS    UT_U8123+OU_SFT-$
	%W    (UT_SF8123)
	DS    UT_U8123+OU_B_S-$
	%W    (UT_US8123)
	DS    UT_U8123+OU_B_P-$
	%W    (0)
	DS    UT_U8123+OU_B_F-$
	%W    (0)
	DS    UT_U8123+OU_B_T-$
	DB    'Inject',0

UT_US8123:DB  '=== Po1 Po2  === ===',0

UT_SF8123:
	DB    K_F2
	%W    (1 SHL BINV_G1)
	%W    (TM_INV)

	DB    K_F3
	%W    (1 SHL BINV_G2)
	%W    (TM_INV)

	DB    0

UT_U8124: ; SAMPNUM
	DS    UT_U8124+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8124+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8124+OU_X-$
	DB    9,4,7,1
	DS    UT_U8124+OU_HLP-$
	%W    (0)
	DS    UT_U8124+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8124+OU_A_RD-$
	%W    (UR_Mi)		; A_RD !!!!!!!!!!!!!!!!!
	%W    (TM_STP1)		; A_WR
	DS    UT_U8124+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (SAMPNUM)		; DP
	DS    UT_U8124+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (50)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Teplota

UT_U8151: ; TMC1 ADC
	DS    UT_U8151+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8151+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8151+OU_X-$
	DB    9,5,7,1
	DS    UT_U8151+OU_HLP-$
	%W    (0)
	DS    UT_U8151+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8151+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U8151+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TMC1_RD)		; DP
	DS    UT_U8151+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8152: ; TMC1_OC
	DS    UT_U8152+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8152+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8152+OU_X-$
	DB    9,6,7,1
	DS    UT_U8152+OU_HLP-$
	%W    (0)
	DS    UT_U8152+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8152+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U8152+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TMC1_OC)		; DP
	DS    UT_U8152+OU_I_F-$
	DB    82H		; format I_F
	%W    (-30000)		; I_L
	%W    (30000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8153: ; TMC1_MC
	DS    UT_U8153+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8153+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8153+OU_X-$
	DB    9,7,7,1
	DS    UT_U8153+OU_HLP-$
	%W    (0)
	DS    UT_U8153+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8153+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U8153+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TMC1_MC)		; DP
	DS    UT_U8153+OU_I_F-$
	DB    82H		; format I_F
	%W    (-30000)		; I_L
	%W    (30000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Konfigurace IRC kotoucku

UT_U8161: ; P_IRC_W
	DS    UT_U8161+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8161+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8161+OU_X-$
	DB    9,8,7,1
	DS    UT_U8161+OU_HLP-$
	%W    (0)
	DS    UT_U8161+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8161+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U8161+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (P_IRC_W)		; DP
	DS    UT_U8161+OU_I_F-$
	DB    80H		; format I_F
	%W    (-127)		; I_L
	%W    (127)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8162: ; P_INJT
	DS    UT_U8162+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8162+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8162+OU_X-$
	DB    11,9,5,1
	DS    UT_U8162+OU_HLP-$
	%W    (0)
	DS    UT_U8162+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8162+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U8162+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (P_INJT)		; DP
	DS    UT_U8162+OU_I_F-$
	DB    02H		; format I_F
	%W    (0)		; I_L
	%W    (2000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8163: ; P_VOL1
	DS    UT_U8163+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8163+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8163+OU_X-$
	DB    11,10,5,1
	DS    UT_U8163+OU_HLP-$
	%W    (0)
	DS    UT_U8163+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8163+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U8163+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (P_VOL1)		; DP
	DS    UT_U8163+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (2000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Ulozeni do EEPROM

UT_U8190:
	DS    UT_U8190+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U8190+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8190+OU_X-$
	DB    0,11,12,1
	DS    UT_U8190+OU_HLP-$
	%W    (0)
	DS    UT_U8190+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U8190+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U8190+OU_B_P-$
	%W    (EEC_SER)		; Servisni nastaveni
	DS    UT_U8190+OU_B_F-$
	%W    (EEP_WRS)		; Zapis do EEPROM
	DS    UT_U8190+OU_B_T-$
	DB    'Save service',0

END