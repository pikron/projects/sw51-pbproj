#   Project file pro detektor AAA
#         (C) Pisoft 1996

aa_ikb.obj: aa_ikb.asm
	a51 aa_ikb.asm $(par) debug

	  : aa_ikb.obj
	del aa_ikb.

aa_ikb.   : aa_ikb.obj ..\pblib\pb.lib
	l51 aa_ikb.obj,..\pblib\pb.lib xdata(8000H) ramsize(100h) ixref

aa_ikb.hex : aa_ikb.
	ohs51 aa_ikb
	del  aa_ikb.
