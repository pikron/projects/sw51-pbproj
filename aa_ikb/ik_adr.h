;********************************************************************
;*                    IC_ADR.H                                      *
;*     Soubor definic fyzickych adres pro KLV                       *
;*                  Stav ke dni 24.03.1991                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************


LCD_INST    XDATA 0FF80H    ; zapis instrukce
LCD_STAT    XDATA 0FF81H    ; cteni statusu
LCD_WDATA   XDATA 0FF82H    ; zapis dat
LCD_RDATA   XDATA 0FF83H    ; cteni dat

;------------------ Ovladani LED diod -------------------------------

LED0	    XDATA 0FF40H
LED1	    XDATA 0FF20H
LED         XDATA LED0

; P3.4	..  podsvetleni displeje

;------------------ Cteni/zapis do klavesnice -----------------------

; KBDWR     P5.2 az P5.7
KBDRD       XDATA 0FF60H

%DEFINE (KBDWR) (
	RL    A
	RL    A
	ANL   A,#0FCH
	ANL   0F8H,#03H
	ORL   0F8H,A
)

%DEFINE (KBDRD) (
	MOV   DPTR,#KBDRD
	MOVX  A,@DPTR
)
