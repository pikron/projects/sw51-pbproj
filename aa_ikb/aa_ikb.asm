$NOMOD51
;********************************************************************
;*           Inteligentni klavesnice - AA_IKB.ASM                   *
;*                       Hlavni modul                               *
;*                  Stav ke dni 10.07.1996                          *
;*                      (C) Pisoft 1996                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_ADR)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_UI)
$INCLUDE(%INCH_PRG)
$INCLUDE(%INCH_ULAN)
$INCLUDE(%INCH_UL_OI)
$INCLUDE(%INCH_UL_OC)
$INCLUDE(%INCH_REGS)
$INCLUDE(%INCH_RS232)
$LIST

EXTRN	CODE(PRINThb,PRINThw,INPUThw,SEL_FNC)
EXTRN	CODE(MONITOR)
EXTRN	CODE(xMDPDP)
EXTRN	CODE(BFL_EV)
PUBLIC	INPUTc,KBDBEEP,ERRBEEP,RES_STAR,DPLOCK


BACK_LIGHT BIT P3.4
%DEFINE (POWER_WDG) (XRL P5,#1)
%DEFINE (POWER_OFF) (ORL P5,#1)

AA_IK_C SEGMENT CODE
AA_IK_D SEGMENT DATA
AA_IK_B SEGMENT DATA BITADDRESSABLE
AA_IK_X SEGMENT XDATA

STACK	DATA  80H

RSEG	AA_IK_B

HW_FLG: DS    1

LEB_FLG:DS    1			; Blikani ledek

ITIM_RF BIT   HW_FLG.7
FL_DIPR	BIT   HW_FLG.6		; Displej pripojen
LEB_PHA	BIT   HW_FLG.5		; Pro blikani ledek
FL_PWRON BIT  HW_FLG.4		; Sepnuti napajeni zbytku pristroje

RSEG	AA_IK_D

DPLOCK: DS    1			; Maximalni pouzivane DPTR

RSEG	AA_IK_X

C_R_PER	EQU   30
REF_PER:DS    1

TMP:	DS    16
TMP1:	DS    16

RSEG	AA_IK_C

RES_STAR:
	MOV   IEN0,#0
	MOV   IEN1,#0
	MOV   IEN2,#0
	MOV   IP0,#00010001B  ; priorita 3 pro S0INT a S1INT
	MOV   IP1,#00010001B  ;
	MOV   SP,#STACK
	MOV   DPSEL,#0
	MOV   DPLOCK,#0
	MOV   PCON,#10000000B ; Bd = OSC/12/16/(256-TH1)
	MOV   TMOD,#00100001B ; 16 bit timer 0;
	MOV   TCON,#01010101B ; citac 0 a 1 cita ; interapy hranou
	%VECTOR(TIMER0,I_TIME); Realny cas z citace 0
	MOV   CINT25,#1
	SETB  ET0
	MOV   HW_FLG,#080H
	MOV   LEB_FLG,#0
	SETB  BACK_LIGHT
	CLR   %BEEP_FL
	SETB  %AUX_OUT_BIT0
	SETB  %AUX_OUT_BIT1

	CALL  ADC_INI

	CLR   A
	MOV   DPTR,#EV_WDGT
	MOVX  @DPTR,A

	CALL  I_TIMRI
	CALL  I_TIMRI

	CALL  LCDINST
	JNZ   STRT30
	SETB  FL_DIPR
	CALL  LEDWR
STRT30:
	MOV   A,#3
	CALL  I_U_LAN
	ANL   IEN1,#NOT 10H
	CALL  uL_OINI

	MOV   R7,#6
	CALL  RS232_INI

	MOV   DPTR,#REF_PER
	MOV   A,#C_R_PER
	MOVX  @DPTR,A

	JMP   L0

INPUTc:	CALL  SCANKEY
	JZ    INPUTc
	RET

; Pipnuti na klavese klavesnice

;KBDBEEP:JMP   KBDSTDB
KBDBEEP:MOV   A,#2
BEEP:	MOV   DPTR,#BEEPTIM
	MOVX  @DPTR,A
	SETB  %BEEP_FL       ; Pipnuti
%IF(%BEEP_FL LE 080H) THEN (
	MOV   DPTR,#LED
	MOV   A,LED_FLG
	MOVX  @DPTR,A
)FI
	MOV   A,R2
	RET
ERRBEEP:MOV   A,#8
	JMP   BEEP

I_U_LAN:PUSH  ACC
;	CLR   A
;	MOV   A,#10	; 9600 pro krystal 18432 kHz
	MOV   A,#3	; 19200 pro krystal 110592 
	MOV   R0,#1
	CALL  uL_FNC	; Rychlost
	POP   ACC
	MOV   R0,#2
	CALL  uL_FNC	; Adresa
	MOV   R2,#0
	MOV   R0,#3
	CALL  uL_FNC	; Delka IB OB
	MOV   R2,#0
	MOV   R0,#4
	CALL  uL_FNC	; Rychle bloky
	MOV   R0,#0
	CALL  uL_FNC	; Start
	RET


; *******************************************************************
; Interni ADC v 80C517A

RSEG	AA_IK_X

ADC_CNT	EQU   8		; Pocet pouzivanych prevodniku

ADC0:	DS    2
ADC1:	DS    2
ADC2:	DS    2
ADC3:	DS    2
ADC4:	DS    2
ADC5:	DS    2
ADC6:	DS    2
ADC7:	DS    2

LS_ADRF	SET   ADC2	; Prime mereni reference

RSEG	AA_IK_C

; Cteci cyklus ADC
ADC_D:	MOV   B,ADCON0
	JB    B.4,ADC_DR	; BSY
	MOV   A,ADCON1
	ANL   A,#0FH
	ADD   A,#-ADC_CNT
	JC    ADC_DR		; Neni co dal prevadet
	ADD   A,#ADC_CNT
	RL    A
	ADD   A,#LOW  ADC0
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH ADC0
	MOV   DPH,A
	MOV   A,ADDATL
	ANL   A,#0C0H
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,ADDATH
	MOVX  @DPTR,A
	MOV   A,B
	ANL   A,#0FH
	INC   A
	SJMP  ADC_S2

; Odstartovani dalsiho cykly
ADC_S:  CLR   A
	MOV   B,ADCON0
	JB    B.4,ADC_DR
ADC_S2:	ANL   A,#0FH
	MOV   ADCON1,A
	MOV   DAPR,#0F0H
ADC_DR:	RET

; Inicializace ADC
ADC_INI:ANL   ADCON0,#NOT 03FH
	MOV   ADCON1,#ADC_CNT
	MOV   DAPR,#0F0H
	RET

; *******************************************************************
;
; Casove preruseni

PUBLIC  KBDTIMR

RSEG	AA_IK_D

DTINT	EQU   2048 ; Prevod XTAL/12 na 450 Hz pro X 11.0592 MHz
DINT25  EQU   18   ; Delitel na 25 Hz
CINT25: DS    1

RSEG	AA_IK_X

BEEPTIM:DS    1	   ; Timer delky pipani

N_OF_T  EQU   6    ; Pocet timeru
TIMR1:  DS    1    ; Timery jsou decrementovany
TIMR2:  DS    1    ; s frekvenci 25 Hz
TIMR_WAIT:
TIMR3:  DS    1
REF_TIM:DS    1	   ; Refresh timr
KBDTIMR:DS    1
TIMRI:  DS    1
TIME:   DS    2    ; Cas v 0.01 min              =====

DELAY_WDG EQU 100  ; Cas V 0.01 min po kterych dojde k vypnti
TIMR_WDG:DS   2	   ; Watchdog kontrolujici cinnost PC
EV_WDGT:DS    1	   ; Typ global event vyslany watchdogem

RSEG	AA_IK_C

USING   3
I_TIME:	PUSH  ACC		; Cast s pruchodem 675 Hz
	PUSH  PSW
	MOV   PSW,#AR0
	PUSH  B
	PUSH  DPL
	PUSH  DPH

	CLR   TF0
	CLR   C
	CLR   TR0
S_T1_D  SET   7               ; zpozdeni rutiny
	MOV   A,TL0           ; 7 Cyklu
	SUBB  A,#LOW  (DTINT-S_T1_D)
	MOV   TL0,A
	MOV   A,TH0
	SUBB  A,#HIGH (DTINT-S_T1_D)
	MOV   TH0,A
	SETB  TR0

	DJNZ  CINT25,I_TIMR1	; Konec casti spruchodem 450 Hz
	MOV   CINT25,#DINT25	; Pruchod s frekvenci 25 Hz

	JNB   FL_PWRON,I_TIM65
	%POWER_WDG		; Pristroj zapnuty
I_TIM65:

	CALL  ADC_D             ; Postupny sber vysledku

	MOV   DPTR,#BEEPTIM
	MOVX  A,@DPTR
	JZ    I_TIM80
	DEC   A
	MOVX  @DPTR,A
	JNZ   I_TIM80
	CLR   %BEEP_FL
%IF(%BEEP_FL LE 080H) THEN (
	MOV   DPTR,#LED       ; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
)FI
I_TIM80:%WATCHDOG

	CALL  uL_STR		; Vybuzeni uLan komunikace

	MOV   DPTR,#TIMR1
	MOV   B,#N_OF_T-1
I_TIME2:MOVX  A,@DPTR
	JZ    I_TIME3
	DEC   A
	MOVX  @DPTR,A
I_TIME3:INC   DPTR
	DJNZ  B,I_TIME2
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	JNB   ITIM_RF,I_TIMR1
	JB    ACC.7,I_TIME4
I_TIMR1:POP   DPH
	POP   DPL
	POP   B
	POP   PSW
	POP   ACC
	SETB  EA
I_TIMRI:RETI

I_TIME4:CLR   ITIM_RF	      ; Pruchod 0.6 sec
;	CALL  I_TIMRI
;	MOV   PSW,#AR0		; Banka 2
	ADD   A,#15
	MOVX  @DPTR,A

	MOV   A,LEB_FLG
	JBC   LEB_PHA,I_TIM42
	ORL   LED_FLG,A
	SETB  LEB_PHA
	SJMP  I_TIM43
I_TIM42:CPL   A
	ANL   LED_FLG,A
I_TIM43:SETB  ITIM_RF

	MOV   DPTR,#TIMR_WDG	; Watchdog nadrizeneho systemu
	MOVX  A,@DPTR
	ADD   A,#-1
	MOV   R1,A
	MOV   A,#1
	MOVC  A,@A+DPTR
	ADDC  A,#-1
	JNC   I_TIM45
	XCH   A,R1
	MOVX  @DPTR,A
	INC   DPTR
	XCH   A,R1
	MOVX  @DPTR,A
I_TIM45:

	CALL  ADC_S            ; Start dalsiho cyklu prevodu
	
	JMP   I_TIMR1

; *******************************************************************
;

DB_W_10:MOV   R0,#10H
	SJMP  DB_WAI1

DB_WAIT:MOV   R0,#0H
DB_WAI1:%WATCHDOG
	DJNZ  R1,DB_WAI1
	DJNZ  R0,DB_WAI1
	RET

L0:	MOV   SP,#80H
	SETB  EA
	JNB   FL_DIPR,L007
	MOV   DPTR,#DEVER_T
	CALL  cPRINT
L007:	CALL  DB_WAIT

	JB    FL_DIPR,L090
				; Bez displeje
L080:	CALL  UC_OI
	JMP   L080

L090:	MOV   A,#LCD_CLR
	CALL  LCDWCOM

	CALL  SCANKEY
	JNZ   L1
	JMP   UT

;	CALL  IHEXLD		; Intelhex loader

L1:

L2:	CALL  SCANKEY

	JZ    L3
	MOV   R7,A
	MOV   DPTR,#SFT_D1
	CALL  SEL_FNC

L3:	CALL  UC_OI

	JMP   L1

T_RDVAL:MOV   DPL,R2
	MOV   DPH,R3
T_RDVA1:MOVX  A,@DPTR
	MOV   R0,A
	PUSH  ACC
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R1,A
	PUSH  ACC
	INC   DPTR
	CALL  cPRINT
	MOV   A,#LCD_HOM+008H
	CALL  LCDWCOM
	MOV   DPL,R0
	MOV   DPH,R1
	CALL  xLDR45i
	MOV   R7,#40H
	CALL  PRINThw
	MOV   R6,#8
	MOV   R7,#40H
	CALL  INPUThw
	POP   DPH
	POP   DPL
	JB    F0,T_RDV99
	CALL  xSVR45i
T_RDV99:RET

RSEG	AA_IK_C

SFT_D1:	DB    K_RUN
	%W    (0)
	%W    (L0)

	DB    K_DP
	%W    (0)
	%W    (MONITOR)

	DB    0

DEVER_T:DB    C_CLR,'%VERSION'
	DB    C_LIN2 ,' (c) PiKRON 1996',0

; *******************************************************************
; Komunikace s ostatnimi jednotkami pres uLan

; Definice jmenprikazu

%OID_ADES(AI_STATUS,STATUS,s2)
I_OFF	  EQU   250
%OID_ADES(AI_ON,ON,e)
I_ON	  EQU   251
%OID_ADES(AI_OFF,OFF,e)
I_WDG	  EQU   254
%OID_ADES(AI_WDG,WDG,e)

I_AUX_OUT0 EQU   610
%OID_ADES(AI_AUX_OUT0,AUX_OUT0,u2)
I_AUX_OUT1 EQU   611
%OID_ADES(AI_AUX_OUT1,AUX_OUT1,u2)
I_COOLING_EN EQU   619
%OID_ADES(AI_COOLING_EN,COOLING_EN,u2)
I_ADC_RAW0 EQU   620
%OID_ADES(AI_ADC_RAW0,ADC_RAW0,u2)
I_ADC_RAW1 EQU   621
%OID_ADES(AI_ADC_RAW1,ADC_RAW1,u2)


; -----------------
I_PREPSAMP EQU  401
I_INJECT  EQU   402
I_SINICFG EQU   403

I_CALLPS  EQU   404
I_ASPREPARE EQU 405
I_ASLOAD  EQU   406
I_MANINJ  EQU   407
I_SAMPNUM EQU   408
I_WHELLSTP EQU  421
I_WHELLPSR EQU  422
I_WHELLHLDT EQU 423
I_WHELLHLD EQU  424
I_WHELLGO EQU   425
I_ASH_UP  EQU   426
I_ASH_DOWN EQU  427
I_ASH_HOME EQU  428
I_ASH_BOT EQU   429
I_ASH_BOT2 EQU  430
I_LPS_AD1 EQU   431
I_LPS_AD2 EQU   432
I_LPS_TD1 EQU   433
I_LPS_TD2 EQU   434
I_PER_IRC_W EQU 435
I_PER_INJT EQU  436
I_PER_VOL1 EQU  437
I_ASH_ROT EQU   441	; Manualni rizeni rotace
I_ASH_RGH EQU   442	; Prikaz home
I_ASH_RP1 EQU   443	; Pozice pro 25 kotouc
I_ASH_RP2 EQU   444	; Pozice pro 80 kotouc, 1-40
I_ASH_RP3 EQU   445	; Pozice pro 80 kotouc, 41-80
I_ASH_WASH EQU  446	; Hloubka sjeti pro cisteni
I_SAVECFG EQU   451

I_TEMP1	  EQU   301
I_TEMP1RQ EQU   311
I_TEMP_OFF EQU  334
I_TEMP_ON EQU   335
I_TEMP_ST EQU	336
I_TEMP1MC EQU   351
I_TEMP1OC EQU   361
I_TEMP1RD EQU   371
; -----------------
I_FLOW	  EQU   220
I_PCFG	  EQU   224
I_PRESS	  EQU   225
I_PRESS_H EQU   226
I_PRESS_L EQU   227
I_GRAD_B  EQU   231
I_GRAD_C  EQU   232
I_GRADDIR EQU   239
I_AUXUAL  EQU	240
I_AUXVALV EQU	241
I_STOP	  EQU   250
I_START	  EQU   251
I_PURGE	  EQU   252
; -----------------
I_ZERRO	  EQU   255
I_FIC	  EQU   256
I_ADCFILT EQU   208
I_ADCAl	  EQU   210	; prevodnik long
I_ADCBl	  EQU   211
I_CHA	  EQU   220	; absorbance float
I_CHB	  EQU   221
I_CHAi	  EQU   230	; absorbance integer
I_CHBi	  EQU   231
I_CHA_GAIN EQU  244
I_CHB_GAIN EQU  245

RSEG	AA_IK_X

; Jednotlive buffery hodnot z jinych jednotek
; prvni byte obsahuje priznaky

IP_MEM_BEG:
IP1_FLOW: DS   3
IP1_PH:	  DS   3
IP1_PL:	  DS   3
IP1_PRESS:DS   3
IP1_ST:	  DS   3
IP1_VALV: DS   3
; --------------
IP2_FLOW: DS   3
IP2_PH:	  DS   3
IP2_PL:	  DS   3
IP2_PRESS:DS   3
IP2_ST:	  DS   3
IP2_GR_B: DS   3
; --------------
ID1_ST:	  DS   3
ID1_CHAi: DS   3
ID1_CHBi: DS   3
ID1_ADCAl:DS   5
ID1_ADCBl:DS   5
ID1_TEMP1:DS   3
ID1_TEMP1RQ:DS 3
ID1_TMCST:DS   3
ID1_TEMP1RD:DS 3
ID1_TEMP1MC:DS 3
ID1_TEMP1OC:DS 3
ID1_CHAGAIN:DS 3
ID1_CHBGAIN:DS 3
; --------------
IS1_ST:	  DS   3
IS1_TEMP1:DS   3
IS1_TEMP1RQ:DS 3
IS1_TEMP1RD:DS 3
IS1_TEMP1MC:DS 3
IS1_TEMP1OC:DS 3
IS1_SNUM: DS   3
IS1_ASHBOT:DS  3
IS1_ASHBOT2:DS 3
IS1_PERIRCW:DS 3
IS1_TMCST:DS   3
IS1_LPS_AD1:DS 3
IS1_LPS_AD2:DS 3
IS1_LPS_TD1:DS 3
IS1_LPS_TD2:DS 3
IS1_P_INJT:DS  3
IS1_P_VOL1:DS  3
IS1_WHELLPSR:DS 3
IS1_ASH_ROT:DS 3	; Manualni rizeni rotace
IS1_ASH_RP1:DS 3	; Pozice pro 25 kotouc
IS1_ASH_RP2:DS 3	; Pozice pro 80 kotouc, 1-40
IS1_ASH_RP3:DS 3	; Pozice pro 80 kotouc, 41-80
IS1_ASH_WASH:DS 3	; Hloubka sjeti pro cisteni
IP_MEM_END:


RSEG	AA_IK_C

; Pumpa 1 ----------------
UP_P1START:
	DB    4,2	; OUP_IID, OUP_UC
	%W    (I_START)
UP_P1STOP:
	DB    4,4
	%W    (I_STOP)
	%W    (I_ERRCLR)
UP_P1PURGE:
	DB    4,2
	%W    (I_PURGE)

UP_P1FLOW:%UCINT(MCF_REF,4,I_FLOW,IP1_FLOW)
UP_P1PH:%UCINT(MCF_REF,4,I_PRESS_H,IP1_PH)
UP_P1PL:%UCINT(MCF_REF,4,I_PRESS_L,IP1_PL)
UP_P1PRESS:%UCINT(MCF_REF,4,I_PRESS,IP1_PRESS)
UP_P1ST:%UCINT(MCF_REF,4,I_STATUS,IP1_ST)
; Ovladani ventilu pres AUX
UP_P1VALV:%UCINT(MCF_REF,4,I_AUXVALV,IP1_VALV)

; Pumpa 2 ----------------
UP_P2START:
	DB    5,2
	%W    (I_START)
UP_P2STOP:
	DB    5,4
	%W    (I_STOP)
	%W    (I_ERRCLR)
UP_P2PURGE:
	DB    5,2
	%W    (I_PURGE)

UP_P2FLOW:%UCINT(MCF_REF,5,I_FLOW,IP2_FLOW)
UP_P2PH:%UCINT(MCF_REF,5,I_PRESS_H,IP2_PH)
UP_P2PL:%UCINT(MCF_REF,5,I_PRESS_L,IP2_PL)
UP_P2PRESS:%UCINT(MCF_REF,5,I_PRESS,IP2_PRESS)
UP_P2ST:%UCINT(MCF_REF,5,I_STATUS,IP2_ST)

UP_P2DIRGA:DB 5,4	; Prime ovladani gradientu
	%W    (I_GRADDIR)
	%W    (0C0H)
UP_P2DIRGB:DB 5,4
	%W    (I_GRADDIR)
	%W    (0C1H)
UP_P2DIRGC:DB 5,4
	%W    (I_GRADDIR)
	%W    (0C2H)

UP_P2GR_B:%UCINT(MCF_REF,5,I_GRAD_B,IP2_GR_B)

; Detektor ----------------
UP_D1ON:DB    6,2
	%W    (I_ON)
UP_D1OFF:
	DB    6,4
	%W    (I_OFF)
	%W    (I_ERRCLR)
UP_D1ZER:
	DB    6,2
	%W    (I_ZERRO)
UP_D1FIC:
	DB    6,2
	%W    (I_FIC)
UP_D1TOFF:
	DB    6,2
	%W    (I_TEMP_OFF)
UP_D1TON:
	DB    6,2
	%W    (I_TEMP_ON)
UP_D1SAVECFG:
	DB    6,2
	%W    (I_SAVECFG)
UP_D1ST:%UCINT(MCF_REF,6,I_STATUS,ID1_ST)
UP_D1CHAi:%UCINT(MCF_REF,6,I_CHAi,ID1_CHAi)
UP_D1CHBi:%UCINT(MCF_REF,6,I_CHBi,ID1_CHBi)
UP_D1T1:%UCINT(MCF_REF,6,I_TEMP1,ID1_TEMP1)
UP_D1T1RQ:%UCINT(MCF_REF,6,I_TEMP1RQ,ID1_TEMP1RQ)
UP_D1TMCST:%UCINT(MCF_REF,6,I_TEMP_ST,ID1_TMCST)
UP_D1ADCAl:%UCLONG(0,6,I_ADCAl,ID1_ADCAl)
UP_D1ADCBl:%UCLONG(0,6,I_ADCBl,ID1_ADCBl)
UP_D1T1RD:%UCINT(0,6,I_TEMP1RD,ID1_TEMP1RD)
UP_D1T1MC:%UCINT(0,6,I_TEMP1MC,ID1_TEMP1MC)
UP_D1T1OC:%UCINT(0,6,I_TEMP1OC,ID1_TEMP1OC)
UP_D1CHAG:%UCINT(0,6,I_CHA_GAIN,ID1_CHAGAIN)
UP_D1CHBG:%UCINT(0,6,I_CHB_GAIN,ID1_CHBGAIN)

; Sampler -----------------
UP_S1OFF:
	DB    7,4
	%W    (I_OFF)
	%W    (I_ERRCLR)
UP_S1TOFF:
	DB    7,2
	%W    (I_TEMP_OFF)
UP_S1TON:
	DB    7,2
	%W    (I_TEMP_ON)
UP_S1INJECT:
	DB    7,2
	%W    (I_INJECT)
UP_S1CALLPS:
	DB    7,2
	%W    (I_CALLPS)
UP_S1PREP:
	DB    7,2
	%W    (I_ASPREPARE)
UP_S1LOAD:
	DB    7,2
	%W    (I_ASLOAD)
UP_S1MANINJ:
	DB    7,2
	%W    (I_MANINJ)
UP_S1WHELLSTPP:
	DB    7,4
	%W    (I_WHELLSTP)
	%W    (1)
UP_S1WHELLSTPM:
	DB    7,4
	%W    (I_WHELLSTP)
	%W    (2)
UP_S1WHELLHLDT:
	DB    7,2
	%W    (I_WHELLHLDT)
UP_S1WHELLHLD:
	DB    7,2
	%W    (I_WHELLHLD)
UP_S1WHELLGO:
	DB    7,2
	%W    (I_WHELLGO)
UP_S1ASH_UP:
	DB    7,2
	%W    (I_ASH_UP)
UP_S1ASH_DOWN:
	DB    7,2
	%W    (I_ASH_DOWN)
UP_S1ASH_HOME:
	DB    7,2
	%W    (I_ASH_HOME)
UP_S1ASH_RGH:
	DB    7,2
	%W    (I_ASH_RGH)
UP_S1SAVECFG:
	DB    7,2
	%W    (I_SAVECFG)

; I_PREPSAMP,I_SINICFG

UP_S1ST:%UCINT(MCF_REF,7,I_STATUS,IS1_ST)
UP_S1T1:%UCINT(MCF_REF,7,I_TEMP1,IS1_TEMP1)
UP_S1T1RQ:%UCINT(MCF_REF,7,I_TEMP1RQ,IS1_TEMP1RQ)
UP_S1T1RD:%UCINT(0,7,I_TEMP1RD,IS1_TEMP1RD)
UP_S1T1MC:%UCINT(0,7,I_TEMP1MC,IS1_TEMP1MC)
UP_S1T1OC:%UCINT(0,7,I_TEMP1OC,IS1_TEMP1OC)
UP_S1SAMPNUM:%UCINT(MCF_REF,7,I_SAMPNUM,IS1_SNUM)
UP_S1ASHBOT:%UCINT(0,7,I_ASH_BOT,IS1_ASHBOT)
UP_S1ASHBOT2:%UCINT(0,7,I_ASH_BOT2,IS1_ASHBOT2)
UP_S1PERIRCW:%UCINT(0,7,I_PER_IRC_W,IS1_PERIRCW)
UP_S1TMCST:%UCINT(MCF_REF,7,I_TEMP_ST,IS1_TMCST)
UP_S1LPS_AD1:%UCINT(0,7,I_LPS_AD1,IS1_LPS_AD1)
UP_S1LPS_AD2:%UCINT(0,7,I_LPS_AD2,IS1_LPS_AD2)
UP_S1LPS_TD1:%UCINT(0,7,I_LPS_TD1,IS1_LPS_TD1)
UP_S1LPS_TD2:%UCINT(0,7,I_LPS_TD2,IS1_LPS_TD2)
UP_S1PER_INJT:%UCINT(0,7,I_PER_INJT,IS1_P_INJT)
UP_S1PER_VOL1:%UCINT(0,7,I_PER_VOL1,IS1_P_VOL1)
UP_S1WHELLPSR:%UCINT(0,7,I_WHELLPSR,IS1_WHELLPSR)
; pro 80 kotouc
UP_S1ASH_ROT:%UCINT(0,7,I_ASH_ROT,IS1_ASH_ROT)
UP_S1ASH_RP1:%UCINT(0,7,I_ASH_RP1,IS1_ASH_RP1)
UP_S1ASH_RP2:%UCINT(0,7,I_ASH_RP2,IS1_ASH_RP2)
UP_S1ASH_RP3:%UCINT(0,7,I_ASH_RP3,IS1_ASH_RP3)
UP_S1ASH_WASH:%UCINT(0,7,I_ASH_WASH,IS1_ASH_WASH)

; -------------------------
; Dotazovy cyklus
UPP_1:	%W    (UP_P1FLOW)
	%W    (UP_P1PH)
	%W    (UP_P1PL)
	%W    (UP_P1PRESS)
	%W    (UP_P1ST)
	%W    (UP_P1VALV)
	;-----------------
	%W    (UP_P2FLOW)
	%W    (UP_P2PH)
	%W    (UP_P2PL)
	%W    (UP_P2PRESS)
	%W    (UP_P2ST)
	%W    (UP_P2GR_B)
	;--- detector ----
	%W    (UP_D1ST)
	%W    (UP_D1CHAi)
	%W    (UP_D1CHBi)
	%W    (UP_D1T1)
	%W    (UP_D1T1RQ)
	%W    (UP_D1TMCST)
	%W    (UP_D1ADCAl)
	%W    (UP_D1ADCBl)
	%W    (UP_D1T1RD)
	%W    (UP_D1T1MC)
	%W    (UP_D1T1OC)
	%W    (UP_D1CHAG)
	%W    (UP_D1CHBG)
	;--- sampler -----
	%W    (UP_S1ST)
	%W    (UP_S1T1)
	%W    (UP_S1T1RQ)
	%W    (UP_S1T1RD)
	%W    (UP_S1T1MC)
	%W    (UP_S1T1OC)
	%W    (UP_S1SAMPNUM)
	%W    (UP_S1ASHBOT)
	%W    (UP_S1ASHBOT2)
	%W    (UP_S1PERIRCW)
	%W    (UP_S1TMCST)
	%W    (UP_S1LPS_AD1)
	%W    (UP_S1LPS_AD2)
	%W    (UP_S1LPS_TD1)
	%W    (UP_S1LPS_TD2)
	%W    (UP_S1PER_INJT)
	%W    (UP_S1PER_VOL1)
	%W    (UP_S1WHELLPSR)
	%W    (UP_S1ASH_ROT)
	%W    (UP_S1ASH_RP1)
	%W    (UP_S1ASH_RP2)
	%W    (UP_S1ASH_RP3)
	%W    (UP_S1ASH_WASH)
UPP_0:	%W    (0)

; Vynulovani bufferu parametru
NUL_IP:	MOV   DPTR,#IP_MEM_BEG
	%LDR23i(IP_MEM_END-IP_MEM_BEG)
NUL_IP1:CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R2
	DEC   R2
	JNZ   NUL_IP1
	MOV   A,R3
	DEC   R3
	JNZ   NUL_IP1
	RET

; *******************************************************************
; Komunikace pres uLan - cast slave

; inicializace objektovych komunikaci
uL_OINI:CALL  NUL_IP
	%LDR45i (UPP_0)
	MOV   DPTR,#UC_AUPP	; seznam parametru ostatnich jednotek
	CALL  xSVR45i
	%LDR45i (OID_1IN)	; seznam prijimanych prikazu
	%LDR67i (OID_1OUT)	; seznam vysilanych prikazu
	JMP    US_INIT

; Identifikace typu pristroje
PUBLIC	uL_IDB,uL_IDE
uL_IDB: DB    '.mt %VERSION .uP 51x',0
uL_IDE:

; Subsystem komunikace

; Status

G_STATUS:
	%LDR45i (0)
	RET

S_AUX_OUT0:
	MOV   A,R4
	MOV   C,ACC.0
	MOV   %AUX_OUT_BIT0,C
	JMP   LEDWR

S_COOLING_ON:
	MOV   R4,#1
S_COOLING_EN:
S_AUX_OUT1:
	MOV   A,R4
	MOV   C,ACC.0
	MOV   %AUX_OUT_BIT1,C
	JMP   LEDWR

G_AUX_OUT0:
        CLR   A
	MOV   R5,A
	MOV   C,%AUX_OUT_BIT0
	MOV   ACC.0,C
	MOV   R4,A
	INC   A
	RET

G_COOLING_EN:
G_AUX_OUT1:
        CLR   A
	MOV   R5,A
	MOV   C,%AUX_OUT_BIT1
	MOV   ACC.0,C
	MOV   R4,A
	INC   A
	RET

ERRCLR_U:RET

ON_U:   %LDR23i(GL_PWRON)
	JMP   GLOB_RQ23

OFF_U:	%LDR23i(GL_STBY)
	JMP   GLOB_RQ23

; Periodicky watchdog z nadrizene jednotky
WDG_U:  %LDR45i(DELAY_WDG)
	MOV   A,#GL_PCWDG
	JNB   FL_PWRON,WDG_SE9
WDG_SET:MOV   DPTR,#EV_WDGT
	MOVX  @DPTR,A
	MOV   DPTR,#TIMR_WDG
	MOV   C,EA
	CLR   EA
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   EA,C
WDG_SE9:RET

; Prijimane prikazy

OID_T	SET   $
	%W    (I_ERRCLR)
	%W    (OID_ISTD)
	%W    (0)
	%W    (ERRCLR_U)

%OID_NEW(I_ON,AI_ON)
	%W    (ON_U)

%OID_NEW(I_OFF,AI_OFF)
	%W    (OFF_U)

%OID_NEW(I_WDG,AI_WDG)
	%W    (WDG_U)

%OID_NEW(I_AUX_OUT0,AI_AUX_OUT0)
	%W    (UI_INT)
	%W    (0)
	%W    (S_AUX_OUT0)

%OID_NEW(I_AUX_OUT1,AI_AUX_OUT1)
	%W    (UI_INT)
	%W    (0)
	%W    (S_AUX_OUT1)

%OID_NEW(I_COOLING_EN,AI_COOLING_EN)
	%W    (UI_INT)
	%W    (0)
	%W    (S_COOLING_EN)

OID_1IN SET   OID_T

; Vysilane hodnoty

OID_T	SET   0

%OID_NEW(I_STATUS,AI_STATUS)
	%W    (UO_INT)
	%W    (0)
	%W    (G_STATUS)

%OID_NEW(I_AUX_OUT0,AI_AUX_OUT0)
	%W    (UO_INT)
	%W    (0)
	%W    (G_AUX_OUT0)

%OID_NEW(I_AUX_OUT1,AI_AUX_OUT1)
	%W    (UO_INT)
	%W    (0)
	%W    (G_AUX_OUT1)

%OID_NEW(I_ADC_RAW0,AI_ADC_RAW0)
	%W    (UO_INT)
	%W    (ADC0)
	%W    (0)

%OID_NEW(I_ADC_RAW1,AI_ADC_RAW1)
	%W    (UO_INT)
	%W    (ADC1)
	%W    (0)

%OID_NEW(I_COOLING_EN,AI_COOLING_EN)
	%W    (UO_INT)
	%W    (0)
	%W    (G_COOLING_EN)

OID_1OUT SET  OID_T

; *******************************************************************
; Propojeni komunikace a user interface

UI_A_DP:MOV   A,#OU_DP
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OU_DP+1
	MOVC  A,@A+DPTR
	MOV   DPL,R0
	MOV   DPH,A
	RET

UR_ULi:	CALL  UI_A_DP
	JMP   UC_RD

UW_ULi: CALL  UI_A_DP
	JMP   UC_WR

UC_SN23:MOV   DPL,R2
	MOV   DPH,R3
	JMP   UC_SND	; !!!! prozatimni

; *******************************************************************
; User interface

RSEG	AA_IK_X

UT_UIAD:DS    40
UT_DATA:DS    40

RSEG	AA_IK_C

UT_INIT:SETB  D4LINE
	SETB  FL_CMAV
	MOV   DPTR,#UI_MV_SX
	MOV   A,#20
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#4
	MOVX  @DPTR,A
	MOV   DPTR,#UT_UIAD
	MOV   UI_AD,DPL
	MOV   UI_AD+1,DPH
	CLR   A
	MOV   DPTR,#EV_BUF
	MOVX  @DPTR,A
	MOV   DPTR,#GR_ACT
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#REF_TIM
	MOVX  @DPTR,A
	RET

UT_TREF:MOV   DPTR,#REF_TIM
	MOVX  A,@DPTR
	JNZ   UT_TRE1
	MOV   DPTR,#REF_PER
	MOVX  A,@DPTR
	MOV   DPTR,#REF_TIM
	MOVX  @DPTR,A
	MOV   A,#1
	RET
UT_TRE1:CLR   A
	RET

UT:	CALL  UT_INIT
	%LDR23i(UT_GR91)
	CALL  GR_RQ23

UT_ML:  CALL  EV_GET
	JZ    UT_ML50
	CJNE  R7,#ET_KEY,UT_ML44
	CLR   A
	MOV   DPTR,#EV_WDGT
	MOVX  @DPTR,A
UT_ML44:CJNE  R7,#ET_GLOB,UT_ML45
	CALL  GLOB_DO		; Globalni udalosti
	SJMP  UT_ML
UT_ML45:CALL  EV_DO
	JMP   UT_ML
UT_ML50:CALL  UC_OI
	JB    uLF_INE,UT_ML55
	JB    UCF_RDP,UT_ML57
UT_ML55:MOV   DPTR,#EV_WDGT	; Watchdog nadrizeneho systemu
	MOVX  A,@DPTR
	JZ    UT_ML56
	MOV   R2,A
	MOV   DPTR,#TIMR_WDG
	MOVX  A,@DPTR
	JNZ   UT_ML56
	INC   DPTR
	MOVX  A,@DPTR
	JNZ   UT_ML56
	MOV   R3,#0		; Watchdog poklada analyzator
	CALL  GLOB_RQ23
	JMP   UT_ML
UT_ML56:CALL  UT_TREF		; Casovy periodicky refresh
	JZ    UT_ML60
	CALL  UC_REFR
UT_ML57:CLR   UCF_RDP
	SETB  FL_REFR
	SJMP  UT_ML65
UT_ML60:CALL  UT_ULED
UT_ML65:JMP   UT_ML

UT_ULED:MOV   C,FL_PWRON
	CPL   C
	MOV   %STBY_FL,C	; STAND-BY
UT_ULE1:JNB   FL_PWRON,UT_ULE9
	MOV   DPTR,#IP1_ST
	MOV   R2,#1 SHL LFB_PUMP1
	CALL  UT_USTS
	MOV   DPTR,#IP2_ST
	MOV   R2,#1 SHL LFB_PUMP2
	CALL  UT_USTS
	MOV   DPTR,#ID1_ST
	MOV   R2,#1 SHL LFB_DETEC
	CALL  UT_USTS
	MOV   DPTR,#IS1_ST
	MOV   R2,#1 SHL LFB_SAMPL
	CALL  UT_USTS
	MOV   R2,#1 SHL LFB_TEMPER
	CALL  UT_USTSTMC
	JMP   LEDWR
UT_ULE9:MOV   A,#(1 SHL LFB_PUMP1)OR(1 SHL LFB_PUMP2)OR(1 SHL LFB_DETEC)
	ORL   A,#(1 SHL LFB_SAMPL)OR(1 SHL LFB_TEMPER)
	CPL   A
	ANL   LED_FLG,A
	ANL   LEB_FLG,A
	JMP   LEDWR

UT_USTS:MOVX  A,@DPTR	; kontrola aktualnosti stavu
	ANL   A,#MCD_DOK
	JZ    UT_UST7
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
UT_UST1:MOV   A,R5
	JB    ACC.7,UT_UST7
	ORL   A,R4
UT_UST2:JZ    UT_UST5
UT_UST3:MOV   A,R2		; On
	CPL   A
	ANL   LEB_FLG,A
	CPL   A
	ORL   LED_FLG,A
	RET
UT_UST5:MOV   A,R2		; Off
	CPL   A
	ANL   LEB_FLG,A
	ANL   LED_FLG,A
	RET
UT_UST7:MOV   A,R2		; Error
	ORL   LEB_FLG,A
UT_UST9:RET

; Provadi pridani dalsi informace na jednu led
UT_USTSTMC:
	MOV   DPTR,#IS1_TMCST
	MOVX  A,@DPTR	; kontrola aktualnosti stavu
	ANL   A,#MCD_DOK
	JZ    UT_UST7
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   DPTR,#ID1_TMCST
	MOVX  A,@DPTR	; kontrola aktualnosti stavu
	ANL   A,#MCD_DOK
	JZ    UT_UST7
	INC   DPTR
	MOVX  A,@DPTR
	ORL   A,R4
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	ORL   A,R5
	MOV   R5,A
	JMP   UT_UST1

; *******************************************************************
; Globalni udalosti

; Posle globalni udalost
GLOB_RQ23:MOV A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	MOV   R7,#ET_GLOB
	JMP   EV_POST

; Zpracovani globalnich udalosti
GLOB_DO:MOV   A,R4
	MOV   R7,A
	MOV   A,R5
	MOV   R4,A
	MOV   DPTR,#GLOB_SF1
	JMP   SEL_FNC

; Globalni udalosti

GL_STBY  EQU  1
GL_PWRON EQU  2
GL_PCWDG EQU  3
GL_PCWDG1 EQU 4

; Tabulka globalnich udalosti

GLOB_SF1:DB   GL_PWRON
	%W    (0)
	%W    (GO_PWRON)

	DB    GL_STBY
	%W    (0)
	%W    (GO_STBY)

	DB    GL_PCWDG
	%W    (0)
	%W    (PCWDG_ERR)

	DB    GL_PCWDG1
	%W    (0)
	%W    (PCWDG_ERR1)

	DB    0

GO_PWRON:MOV  SP,#STACK
	CALL  UI_ABORT
	SETB  FL_PWRON
	CALL  S_COOLING_ON	; Pro jistotu zapnout chlazeni
	%LDR45i (UPP_1)
	MOV   DPTR,#UC_AUPP	; seznam parametru ostatnich jednotek
	CALL  xSVR45i
	CALL  NUL_IP
	CLR   BACK_LIGHT
	%LDR23i(UT_GR15)
	CALL  GR_RQ23
	JMP   UT_ML

GO_STBY:MOV   SP,#STACK
	CALL  UI_ABORT

	%LDR45i (UPP_0)
	MOV   DPTR,#UC_AUPP	; nedotazovat se
	CALL  xSVR45i

	CLR   A
	MOV   DPTR,#EV_WDGT
	MOVX  @DPTR,A
	CALL  NUL_IP
	CLR   FL_PWRON
	%POWER_OFF
	SETB  BACK_LIGHT
	%LDR23i(UT_GR91)
	CALL  GR_RQ23
	JMP   UT_ML

; PC se nestara o AAA
PCWDG_ERR:
	%LDR23i(UP_P2DIRGA)	; H2O
	CALL  UC_SN23
	%LDR45i(1)		; Pufr 1
	MOV   DPTR,#UP_P1VALV
	CALL  UC_WR
	%LDR23i(UP_D1TOFF)	; Vypnuti reaktoru
	CALL  UC_SN23
	%LDR23i(UP_D1OFF)	; Vypnuti detektoru
	CALL  UC_SN23
	MOV   A,#GL_PCWDG1	; Cekat 5 minut pak vypnout zbytek
	CALL  S_COOLING_ON	; Pro jistotu zapnout chlazeni
	%LDR45i(500)
	JMP   WDG_SET

; PC se nestara o AAA
PCWDG_ERR1:
	%LDR23i(UP_P1STOP)	; Vypnuti pumpy 1
	CALL  UC_SN23
	%LDR23i(UP_P2STOP)	; Vypnuti pumpy 2
	CALL  UC_SN23
	%LDR23i(UP_S1TOFF)	; Vypnuti kolony
	CALL  UC_SN23
	MOV   A,#GL_STBY
	%LDR45i(10)
	JMP   WDG_SET

; *******************************************************************
; Standardni mapovani klaves

UT_SF1:
    %IF(0)THEN(
	DB    K_STBY
	%W    (GL_STBY)
	%W    (GLOB_RQ23)
    )ELSE(
	DB    K_STBY
	%W    (UT_GR92)
	%W    (GR_RQ23)
    )FI

	DB    K_PUMP1
	%W    (UT_GR13)
	%W    (GR_RQ23)

	DB    K_PUMP2
	%W    (UT_GR14)
	%W    (GR_RQ23)

	DB    K_SAMPL
	%W    (UT_GR15)
	%W    (GR_RQ23)

	DB    K_DETEC
	%W    (UT_GR16)
	%W    (GR_RQ23)

	DB    K_TEMPER
	%W    (UT_GR17)
	%W    (GR_RQ23)

	DB    K_VALVES
	%W    (UT_GR18)
	%W    (GR_RQ23)

	DB    K_MENU
	%W    (UT_GR70)
	%W    (GR_RQ23)

UT_SFTN:DB    K_RIGHT
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    K_LEFT
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    K_UP
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    K_DOWN
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

UT_SF0:	DB    0

; *******************************************************************
; Manualni rezim rezim

; ---------------------------------
; Pumpa 1
UT_GR13:DS    UT_GR13+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR13+OGR_BTXT-$
	%W    (UT_GT13)
	DS    UT_GR13+OGR_STXT-$
	%W    (UT_GS13)
	DS    UT_GR13+OGR_HLP-$
	%W    (0)
	DS    UT_GR13+OGR_SFT-$
	%W    (UT_SF13)
	DS    UT_GR13+OGR_PU-$
	%W    (UT_U1301)
	%W    (UT_U1302)
	%W    (UT_U1303)
	%W    (UT_U1304)
	%W    (UT_U1305)
	%W    (0)

UT_GT13:DB    '  Pump 1',C_NL
	DB    'FLOW P-H P-L P MPa',0

UT_GS13:DB    '=== === Purg On  Off',0

UT_SF13:DB    K_F5
	%W    (UP_P1STOP)
	%W    (UC_SN23)

	DB    K_F4
	%W    (UP_P1START)
	%W    (UC_SN23)

	DB    K_F3
	%W    (UP_P1PURGE)
	%W    (UC_SN23)

	DB    -1
	%W    (UT_SF1)

UT_U1301:
	DS    UT_U1301+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1301+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1301+OU_X-$
	DB    0,2,4,1
	DS    UT_U1301+OU_HLP-$
	%W    (0)
	DS    UT_U1301+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1301+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1301+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P1FLOW)	; DP
	DS    UT_U1301+OU_I_F-$
	DB    03H		; format I_F
	%W    (0)		; I_L
	%W    (10000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1302:
	DS    UT_U1302+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1302+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1302+OU_X-$
	DB    5,2,3,1
	DS    UT_U1302+OU_HLP-$
	%W    (0)
	DS    UT_U1302+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1302+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1302+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P1PH)		; DP
	DS    UT_U1302+OU_I_F-$
	DB    01H		; format I_F
	%W    (0)		; I_L
	%W    (400)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1303:
	DS    UT_U1303+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1303+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1303+OU_X-$
	DB    9,2,3,1
	DS    UT_U1303+OU_HLP-$
	%W    (0)
	DS    UT_U1303+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1303+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1303+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P1PL)		; DP
	DS    UT_U1303+OU_I_F-$
	DB    01H		; format I_F
	%W    (0)		; I_L
	%W    (400)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1304:
	DS    UT_U1304+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1304+OU_MSK-$
	DB    0
	DS    UT_U1304+OU_X-$
	DB    14,2,4,1
	DS    UT_U1304+OU_HLP-$
	%W    (0)
	DS    UT_U1304+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1304+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1304+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P1PRESS)	; DP
	DS    UT_U1304+OU_I_F-$
	DB    01H		; format I_F
	%W    (0)		; I_L
	%W    (400)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1305:
	DS    UT_U1305+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1305+OU_MSK-$
	DB    0
	DS    UT_U1305+OU_X-$
	DB    9,0,7,1
	DS    UT_U1305+OU_HLP-$
	%W    (0)
	DS    UT_U1305+OU_SFT-$
	%W    (0)
	DS    UT_U1305+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1305+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P1ST)		; DP
	DS    UT_U1305+OU_M_S-$
	%W    (0)
	DS    UT_U1305+OU_M_P-$
	%W    (0)
	DS    UT_U1305+OU_M_F-$
	%W    (0)
	DS    UT_U1305+OU_M_T-$
	%W    (0FF00H)
	%W    (100H)
	%W    (UT_U1305TR)
	%W    (0FFFFH)
	%W    (0)
	%W    (UT_U1305T0)
	%W    (0FFFFH)
	%W    (2)
	%W    (UT_U1305T2)
	%W    (08000H)
	%W    (0)
	%W    (UT_U1305T1)
	%W    (8000H)
	%W    (8000H)
	%W    (UT_U1305TE)
	%W    (0)
UT_U1305T0:DB    'Off',0
UT_U1305T1:DB    'On',0
UT_U1305T2:DB    'Purge',0
UT_U1305TR:DB    'Running',0
UT_U1305TE:DB    'Error',0

; ---------------------------------
; Pumpa 2
UT_GR14:DS    UT_GR14+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR14+OGR_BTXT-$
	%W    (UT_GT14)
	DS    UT_GR14+OGR_STXT-$
	%W    (UT_GS13)
	DS    UT_GR14+OGR_HLP-$
	%W    (0)
	DS    UT_GR14+OGR_SFT-$
	%W    (UT_SF14)
	DS    UT_GR14+OGR_PU-$
	%W    (UT_U1401)
	%W    (UT_U1402)
	%W    (UT_U1403)
	%W    (UT_U1404)
	%W    (UT_U1405)
	%W    (0)

UT_GT14:DB    '  Pump 2',C_NL
	DB    'FLOW P-H P-L P MPa',0

UT_SF14:DB    K_F5
	%W    (UP_P2STOP)
	%W    (UC_SN23)

	DB    K_F4
	%W    (UP_P2START)
	%W    (UC_SN23)

	DB    K_F3
	%W    (UP_P2PURGE)
	%W    (UC_SN23)

	DB    -1
	%W    (UT_SF1)

UT_U1401:
	DS    UT_U1401+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1401+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1401+OU_X-$
	DB    0,2,4,1
	DS    UT_U1401+OU_HLP-$
	%W    (0)
	DS    UT_U1401+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1401+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1401+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P2FLOW)	; DP
	DS    UT_U1401+OU_I_F-$
	DB    03H		; format I_F
	%W    (0)		; I_L
	%W    (10000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1402:
	DS    UT_U1402+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1402+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1402+OU_X-$
	DB    5,2,3,1
	DS    UT_U1402+OU_HLP-$
	%W    (0)
	DS    UT_U1402+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1402+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1402+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P2PH)		; DP
	DS    UT_U1402+OU_I_F-$
	DB    01H		; format I_F
	%W    (0)		; I_L
	%W    (400)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1403:
	DS    UT_U1403+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1403+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1403+OU_X-$
	DB    9,2,3,1
	DS    UT_U1403+OU_HLP-$
	%W    (0)
	DS    UT_U1403+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1403+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1403+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P2PL)		; DP
	DS    UT_U1403+OU_I_F-$
	DB    01H		; format I_F
	%W    (0)		; I_L
	%W    (400)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1404:
	DS    UT_U1404+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1404+OU_MSK-$
	DB    0
	DS    UT_U1404+OU_X-$
	DB    14,2,4,1
	DS    UT_U1404+OU_HLP-$
	%W    (0)
	DS    UT_U1404+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1404+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1404+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P2PRESS)	; DP
	DS    UT_U1404+OU_I_F-$
	DB    01H		; format I_F
	%W    (0)		; I_L
	%W    (400)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1405:
	DS    UT_U1405+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1405+OU_MSK-$
	DB    0
	DS    UT_U1405+OU_X-$
	DB    9,0,7,1
	DS    UT_U1405+OU_HLP-$
	%W    (0)
	DS    UT_U1405+OU_SFT-$
	%W    (0)
	DS    UT_U1405+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1405+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P2ST)		; DP
	DS    UT_U1405+OU_M_S-$
	%W    (0)
	DS    UT_U1405+OU_M_P-$
	%W    (0)
	DS    UT_U1405+OU_M_F-$
	%W    (0)
	DS    UT_U1405+OU_M_T-$
	%W    (0FF00H)
	%W    (100H)
	%W    (UT_U1305TR)
	%W    (0FFFFH)
	%W    (0)
	%W    (UT_U1305T0)
	%W    (0FFFFH)
	%W    (2)
	%W    (UT_U1305T2)
	%W    (08000H)
	%W    (0)
	%W    (UT_U1305T1)
	%W    (8000H)
	%W    (8000H)
	%W    (UT_U1305TE)
	%W    (0)

; ---------------------------------
; Sampler
UT_GR15:DS    UT_GR15+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR15+OGR_BTXT-$
	%W    (UT_GT15)
	DS    UT_GR15+OGR_STXT-$
	%W    (UT_GS15)
	DS    UT_GR15+OGR_HLP-$
	%W    (0)
	DS    UT_GR15+OGR_SFT-$
	%W    (UT_SF15)
	DS    UT_GR15+OGR_PU-$
	%W    (UT_U1501)
	%W    (UT_U1502)
	%W    (0)

UT_GT15:DB    '  Sampler',C_NL
	DB    'Number ',0

UT_GS15:DB    'Cal Pre Load Man Off',0

UT_SF15:DB    K_F1
	%W    (UP_S1CALLPS)
	%W    (UC_SN23)

	DB    K_F2
	%W    (UP_S1PREP)
	%W    (UC_SN23)

	DB    K_F3
	%W    (UP_S1LOAD)
	%W    (UC_SN23)

	DB    K_F4
	%W    (UP_S1MANINJ)
	%W    (UC_SN23)

	DB    K_F5
	%W    (UP_S1OFF)
	%W    (UC_SN23)

	DB    K_SAMPL
	%W    (UP_S1WHELLHLDT)
	%W    (UC_SN23)

	DB    -1
	%W    (UT_SF1)

UT_U1501:
	DS    UT_U1501+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1501+OU_MSK-$
	DB    0
	DS    UT_U1501+OU_X-$
	DB    11,0,7,1
	DS    UT_U1501+OU_HLP-$
	%W    (0)
	DS    UT_U1501+OU_SFT-$
	%W    (0)
	DS    UT_U1501+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1501+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1ST)		; DP
	DS    UT_U1501+OU_M_S-$
	%W    (0)
	DS    UT_U1501+OU_M_P-$
	%W    (0)
	DS    UT_U1501+OU_M_F-$
	%W    (0)
	DS    UT_U1501+OU_M_T-$
	%W    (0FF00H)
	%W    (100H)
	%W    (UT_U1501TR)
	%W    (0FFFFH)
	%W    (0)
	%W    (UT_U1501T0)
	%W    (0FFFFH)
	%W    (00081H)
	%W    (UT_U1501TP)
	%W    (08000H)
	%W    (0)
	%W    (UT_U1501T1)
	%W    (8000H)
	%W    (8000H)
	%W    (UT_U1501TE)
	%W    (0)
UT_U1501T0:DB    'Off',0
UT_U1501T1:DB    'On',0
UT_U1501TP:DB    'Prepared',0
UT_U1501TR:DB    'Running',0
UT_U1501TE:DB    'Error',0

UT_U1502:
	DS    UT_U1502+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1502+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1502+OU_X-$
	DB    0,2,5,1
	DS    UT_U1502+OU_HLP-$
	%W    (0)
	DS    UT_U1502+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1502+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1502+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1SAMPNUM)	; DP
	DS    UT_U1502+OU_I_F-$
	DB    00H		; format I_F
	%W    (1)		; I_L
	%W    (80) ; (25)	; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Detector
UT_GR16:DS    UT_GR16+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR16+OGR_BTXT-$
	%W    (UT_GT16)
	DS    UT_GR16+OGR_STXT-$
	%W    (UT_GS16)
	DS    UT_GR16+OGR_HLP-$
	%W    (0)
	DS    UT_GR16+OGR_SFT-$
	%W    (UT_SF16)
	DS    UT_GR16+OGR_PU-$
	%w    (UT_U1601)
	%w    (UT_U1602)
	%w    (UT_U1603)
	%W    (0)

UT_GT16:DB    '  Detector',C_NL
	DB    ' Green   Blue',0

UT_GS16:DB    '=== FIC Zer  On  Off',0

UT_SF16:DB    K_F5
	%W    (UP_D1OFF)
	%W    (UC_SN23)

	DB    K_F4
	%W    (UP_D1ON)
	%W    (UC_SN23)

	DB    K_F3
	%W    (UP_D1ZER)
	%W    (UC_SN23)

	DB    K_F2
	%W    (UP_D1FIC)
	%W    (UC_SN23)

	DB    -1
	%W    (UT_SF1)

UT_U1601:
	DS    UT_U1601+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1601+OU_MSK-$
	DB    0
	DS    UT_U1601+OU_X-$
	DB    11,0,7,1
	DS    UT_U1601+OU_HLP-$
	%W    (0)
	DS    UT_U1601+OU_SFT-$
	%W    (0)
	DS    UT_U1601+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1601+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_D1ST)		; DP
	DS    UT_U1601+OU_M_S-$
	%W    (0)
	DS    UT_U1601+OU_M_P-$
	%W    (0)
	DS    UT_U1601+OU_M_F-$
	%W    (0)
	DS    UT_U1601+OU_M_T-$
	%W    (0FF00H)
	%W    (100H)
	%W    (UT_U1601TR)
	%W    (0FFFFH)
	%W    (0)
	%W    (UT_U1601T0)
	%W    (08000H)
	%W    (0)
	%W    (UT_U1601T1)
	%W    (8000H)
	%W    (8000H)
	%W    (UT_U1601TE)
	%W    (0)
UT_U1601T0:DB    'Off',0
UT_U1601T1:DB    'On',0
UT_U1601TR:DB    'Running',0
UT_U1601TE:DB    'Error',0

UT_U1602:
	DS    UT_U1602+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1602+OU_MSK-$
	DB    0
	DS    UT_U1602+OU_X-$
	DB    0,2,7,1
	DS    UT_U1602+OU_HLP-$
	%W    (0)
	DS    UT_U1602+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1602+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1602+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_D1CHBi)	; DP
	DS    UT_U1602+OU_I_F-$
	DB    084H		; format I_F
	%W    (8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1603:
	DS    UT_U1603+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1603+OU_MSK-$
	DB    0
	DS    UT_U1603+OU_X-$
	DB    8,2,7,1
	DS    UT_U1603+OU_HLP-$
	%W    (0)
	DS    UT_U1603+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1603+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1603+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_D1CHAi)	; DP
	DS    UT_U1603+OU_I_F-$
	DB    084H		; format I_F
	%W    (8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Temperatures
UT_GR17:DS    UT_GR17+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR17+OGR_BTXT-$
	%W    (UT_GT17)
	DS    UT_GR17+OGR_STXT-$
	%W    (UT_GS17)
	DS    UT_GR17+OGR_HLP-$
	%W    (0)
	DS    UT_GR17+OGR_SFT-$
	%W    (UT_SF17)
	DS    UT_GR17+OGR_PU-$
	%W    (UT_U1701)
	%W    (UT_U1702)
	%W    (UT_U1703)
	%W    (UT_U1704)
	%W    (UT_U1705)
	%W    (UT_U1706)
	%W    (0)

UT_GT17:DB    '    Column  Reactor',C_NL
	DB    'Act',C_NL
	DB    'Fin',0

UT_GS17:DB    '=== On  Off  On  Off',0

UT_SF17:DB    K_F5
	%W    (UP_D1TOFF)
	%W    (UC_SN23)

	DB    K_F4
	%W    (UP_D1TON)
	%W    (UC_SN23)

	DB    K_F3
	%W    (UP_S1TOFF)
	%W    (UC_SN23)

	DB    K_F2
	%W    (UP_S1TON)
	%W    (UC_SN23)

	DB    -1
	%W    (UT_SF1)

UT_U1701:
	DS    UT_U1701+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1701+OU_MSK-$
	DB    0
	DS    UT_U1701+OU_X-$
	DB    4,1,5,1
	DS    UT_U1701+OU_HLP-$
	%W    (0)
	DS    UT_U1701+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1701+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1701+OU_DPSI-$
	%W    (0)		; DPSI
	%W    (UP_S1T1)		; DP
	DS    UT_U1701+OU_I_F-$
	DB    081H		; format I_F
	%W    (350)		; I_L
	%W    (1000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1702:
	DS    UT_U1702+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1702+OU_MSK-$
	DB    0
	DS    UT_U1702+OU_X-$
	DB    12,1,5,1
	DS    UT_U1702+OU_HLP-$
	%W    (0)
	DS    UT_U1702+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1702+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1702+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_D1T1)		; DP
	DS    UT_U1702+OU_I_F-$
	DB    081H		; format I_F
	%W    (350)		; I_L
	%W    (1500)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1703:
	DS    UT_U1703+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1703+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1703+OU_X-$
	DB    4,2,5,1
	DS    UT_U1703+OU_HLP-$
	%W    (0)
	DS    UT_U1703+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1703+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1703+OU_DPSI-$
	%W    (0)		; DPSI
	%W    (UP_S1T1RQ)		; DP
	DS    UT_U1703+OU_I_F-$
	DB    001H		; format I_F
	%W    (350)		; I_L
	%W    (1000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1704:
	DS    UT_U1704+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1704+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1704+OU_X-$
	DB    12,2,5,1
	DS    UT_U1704+OU_HLP-$
	%W    (0)
	DS    UT_U1704+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1704+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1704+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_D1T1RQ)	; DP
	DS    UT_U1704+OU_I_F-$
	DB    001H		; format I_F
	%W    (350)		; I_L
	%W    (1500)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1705:
	DS    UT_U1705+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1705+OU_MSK-$
	DB    0
	DS    UT_U1705+OU_X-$
	DB    10,2,1,1
	DS    UT_U1705+OU_HLP-$
	%W    (0)
	DS    UT_U1705+OU_SFT-$
	%W    (0)
	DS    UT_U1705+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1705+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1TMCST)	; DP
	DS    UT_U1705+OU_M_S-$
	%W    (0)
	DS    UT_U1705+OU_M_P-$
	%W    (0)
	DS    UT_U1705+OU_M_F-$
	%W    (0)
	DS    UT_U1705+OU_M_T-$
	%W    (0FFFFH)
	%W    (0)
	%W    (UT_U1705T0)
	%W    (08000H)
	%W    (0)
	%W    (UT_U1705T1)
	%W    (8000H)
	%W    (8000H)
	%W    (UT_U1705TE)
	%W    (0)
UT_U1705T0:DB    ' ',0
UT_U1705T1:DB    0FFH,'++++',0
UT_U1705TE:DB    'E',0

UT_U1706:
	DS    UT_U1706+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1706+OU_MSK-$
	DB    0
	DS    UT_U1706+OU_X-$
	DB    18,2,1,1
	DS    UT_U1706+OU_HLP-$
	%W    (0)
	DS    UT_U1706+OU_SFT-$
	%W    (0)
	DS    UT_U1706+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1706+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_D1TMCST)	; DP
	DS    UT_U1706+OU_M_S-$
	%W    (0)
	DS    UT_U1706+OU_M_P-$
	%W    (0)
	DS    UT_U1706+OU_M_F-$
	%W    (0)
	DS    UT_U1706+OU_M_T-$
	%W    (0FFFFH)
	%W    (0)
	%W    (UT_U1705T0)
	%W    (08000H)
	%W    (0)
	%W    (UT_U1705T1)
	%W    (8000H)
	%W    (8000H)
	%W    (UT_U1705TE)
	%W    (0)

; ---------------------------------
; Ventily
UT_GR18:DS    UT_GR18+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR18+OGR_BTXT-$
	%W    (UT_GT18)
	DS    UT_GR18+OGR_STXT-$
	%W    (UT_GS18)
	DS    UT_GR18+OGR_HLP-$
	%W    (0)
	DS    UT_GR18+OGR_SFT-$
	%W    (UT_SF18)
	DS    UT_GR18+OGR_PU-$
	%W    (UT_U1801)
	%W    (UT_U1802)
	%W    (0)

UT_GT18:DB    '  Valves',C_NL
	DB    'Buff  Reagenc',0

UT_GS18:DB    '=== === ==== NHD H2O',0

UT_SF18:DB    K_F5
	%W    (UP_P2DIRGA)
	%W    (UC_SN23)

	DB    K_F4
	%W    (UP_P2DIRGB)
	%W    (UC_SN23)

	DB    -1
	%W    (UT_SF1)

UT_U1801:
	DS    UT_U1801+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1801+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1801+OU_X-$
	DB    0,2,4,1
	DS    UT_U1801+OU_HLP-$
	%W    (0)
	DS    UT_U1801+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1801+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U1801+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P1VALV)	; DP
	DS    UT_U1801+OU_I_F-$
	DB    00H		; format I_F
	%W    (1)		; I_L
	%W    (8)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1802:
	DS    UT_U1802+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1802+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1802+OU_X-$
	DB    6,2,6,1
	DS    UT_U1802+OU_HLP-$
	%W    (0)
	DS    UT_U1802+OU_SFT-$
	%W    (0)
	DS    UT_U1802+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1802+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_P2GR_B)	; DP
	DS    UT_U1802+OU_M_S-$
	%W    (0)
	DS    UT_U1802+OU_M_P-$
	%W    (0)
	DS    UT_U1802+OU_M_F-$
	%W    (0)
	DS    UT_U1802+OU_M_T-$
	%W    (0FFFFH)
	%W    (0)
	%W    (UT_U1802T0)
	%W    (08000H)
	%W    (0)
	%W    (UT_U1802T1)
	%W    (0)
UT_U1802T0:DB    '   H2O',0
UT_U1802T1:DB    '   NHD',0

; *******************************************************************
; Main menu

UT_GR70:DS    UT_GR70+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR70+OGR_BTXT-$
	%W    (UT_GT70)
	DS    UT_GR70+OGR_STXT-$
	%W    (0)		; UT_GS70
	DS    UT_GR70+OGR_HLP-$
	%W    (0)
	DS    UT_GR70+OGR_SFT-$
	%W    (UT_SF70)
	DS    UT_GR70+OGR_PU-$
	%W    (UT_U7001)
	%W    (UT_U7002)
	%W    (0)

UT_GT70:DB    '  Main Menu',C_NL
	DB    'Service :',0

UT_SF70:DB    -1
	%W    (UT_SF1)

UT_U7001:
	DS    UT_U7001+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U7001+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7001+OU_X-$
	DB    2,2,14,1
	DS    UT_U7001+OU_HLP-$
	%W    (0)
	DS    UT_U7001+OU_SFT-$
	%W    (0)		; UT_SF7101
	DS    UT_U7001+OU_B_S-$
	%W    (0)		; UT_US7101
	DS    UT_U7001+OU_B_P-$
	%W    (UT_GR81)
	DS    UT_U7001+OU_B_F-$
	%W    (GR_RQ23)
	DS    UT_U7001+OU_B_T-$
	DB    'Sampler/Column',0

UT_U7002:
	DS    UT_U7002+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U7002+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7002+OU_X-$
	DB    2,3,16,1
	DS    UT_U7002+OU_HLP-$
	%W    (0)
	DS    UT_U7002+OU_SFT-$
	%W    (0)		; UT_SF7101
	DS    UT_U7002+OU_B_S-$
	%W    (0)		; UT_US7101
	DS    UT_U7002+OU_B_P-$
	%W    (UT_GR82)
	DS    UT_U7002+OU_B_F-$
	%W    (GR_RQ23)
	DS    UT_U7002+OU_B_T-$
	DB    'Detector/Reactor',0

; *******************************************************************
; Servisni rezim davkovace

UT_GR81:DS    UT_GR81+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR81+OGR_BTXT-$
	%W    (UT_GT81)
	DS    UT_GR81+OGR_STXT-$
	%W    (UT_GS81)
	DS    UT_GR81+OGR_HLP-$
	%W    (0)
	DS    UT_GR81+OGR_SFT-$
	%W    (UT_SF81)
	DS    UT_GR81+OGR_PU-$
	%W    (UT_U8101)
	%W    (UT_U8116)
	%W    (UT_U8117)
	%W    (UT_U8118)
	%W    (UT_U8119)
	%W    (UT_U8120)
	%W    (UT_U8121)
	%W    (UT_U8122)
	%W    (UT_U8131)
	%W    (UT_U8132)
	%W    (UT_U8135)
	%W    (UT_U8151)
	%W    (UT_U8152)
	%W    (UT_U8153)
	%W    (UT_U8161)
	%W    (UT_U8162)
	%W    (UT_U8163)
	%W    (UT_U8169)
	%W    (UT_U8190)
	%W    (0)

UT_GT81:DB    '  Service',C_NL
	DB    'Lq Sen 1 ----- td',C_NL
	DB    'Lq Sen 2 ----- td',C_NL
	DB    'Samp.        No',C_NL
	DB    '',C_NL
	DB    'Wheel sens.',C_NL
	DB    'Temp ADC',C_NL
	DB    'Temp Offs',C_NL
	DB    'Temp Slop',C_NL
	DB    'Per IRC',C_NL
	DB    'Inject T',C_NL
	DB    'Fill Vol',0

UT_GS81:DB    '=== === ==== === ===',0

UT_SF81:DB    -1
	%W    (UT_SF1)

UT_U8101:
	DS    UT_U8101+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U8101+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8101+OU_X-$
	DB    16,0,4,1
	DS    UT_U8101+OU_HLP-$
	%W    (0)
	DS    UT_U8101+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8101+OU_B_S-$
	%W    (UT_US8101)
	DS    UT_U8101+OU_B_P-$
	%W    (UT_GR70)
	DS    UT_U8101+OU_B_F-$
	%W    (GR_RQ23)
	DS    UT_U8101+OU_B_T-$
	DB    'Exit',0

UT_US8101:DB  '=== === ==== === ===',0

; ---------------------------------
; Snimace bublin

UT_U8116:
	DS    UT_U8116+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8116+OU_MSK-$
	DB    0
	DS    UT_U8116+OU_X-$
	DB    8,1,6,1
	DS    UT_U8116+OU_HLP-$
	%W    (0)
	DS    UT_U8116+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8116+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U8116+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1LPS_AD1)	; DP
	DS    UT_U8116+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8117:
	DS    UT_U8117+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8117+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8117+OU_X-$
	DB    17,1,3,1
	DS    UT_U8117+OU_HLP-$
	%W    (0)
	DS    UT_U8117+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8117+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U8117+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1LPS_TD1)	; DP
	DS    UT_U8117+OU_I_F-$
	DB    000H		; format I_F
	%W    (2)		; I_L
	%W    (99)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8118:
	DS    UT_U8118+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8118+OU_MSK-$
	DB    0
	DS    UT_U8118+OU_X-$
	DB    8,2,6,1
	DS    UT_U8118+OU_HLP-$
	%W    (0)
	DS    UT_U8118+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8118+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U8118+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1LPS_AD2)	; DP
	DS    UT_U8118+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8119:
	DS    UT_U8119+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8119+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8119+OU_X-$
	DB    17,2,3,1
	DS    UT_U8119+OU_HLP-$
	%W    (0)
	DS    UT_U8119+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8119+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U8119+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1LPS_TD2)	; DP
	DS    UT_U8119+OU_I_F-$
	DB    000H		; format I_F
	%W    (2)		; I_L
	%W    (99)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Sampler

UT_U8120: ; Ovladani davkovace
	DS    UT_U8120+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U8120+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8120+OU_X-$
	DB    0,3,4,1
	DS    UT_U8120+OU_HLP-$
	%W    (0)
	DS    UT_U8120+OU_SFT-$
	%W    (UT_SF8120)
	DS    UT_U8120+OU_B_S-$
	%W    (UT_US8120)
	DS    UT_U8120+OU_B_P-$
	%W    (UP_S1LOAD)
	DS    UT_U8120+OU_B_F-$
	%W    (UC_SN23)
	DS    UT_U8120+OU_B_T-$
	DB    'Samp',0

UT_US8120:DB  'Cal Pre Load Man Off',0

UT_SF8120:
	DB    K_F1
	%W    (UP_S1CALLPS)
	%W    (UC_SN23)

	DB    K_F2
	%W    (UP_S1PREP)
	%W    (UC_SN23)

	DB    K_F3
	%W    (UP_S1LOAD)
	%W    (UC_SN23)

	DB    K_F4
	%W    (UP_S1MANINJ)
	%W    (UC_SN23)

	DB    K_F5
	%W    (UP_S1OFF)
	%W    (UC_SN23)

	DB    0

UT_U8121: ; Stav sekvenceru
	DS    UT_U8121+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8121+OU_MSK-$
	DB    0
	DS    UT_U8121+OU_X-$
	DB    6,3,6,1
	DS    UT_U8121+OU_HLP-$
	%W    (0)
	DS    UT_U8121+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8121+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U8121+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1ST)	; DP
	DS    UT_U8121+OU_I_F-$
	DB    80H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8122: ; Najizdeni na polohu kotouce
	DS    UT_U8122+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8122+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8122+OU_X-$
	DB    16,3,2,1
	DS    UT_U8122+OU_HLP-$
	%W    (0)
	DS    UT_U8122+OU_SFT-$
	%W    (UT_SF8122)
	DS    UT_U8122+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U8122+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1SAMPNUM)	; DP
	DS    UT_U8122+OU_I_F-$
	DB    00H		; format I_F
	%W    (1)		; I_L
	%W    (80) ; (25)	; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_SF8122:
	DB    KV_V_OK
	%W    (UP_S1WHELLGO)
	%W    (UC_SN23)

	DB    K_ENTER
	%W    (UP_S1WHELLGO)
	%W    (UC_SN23)

	DB    0

UT_U8131: ; Rizeni ramenka
	DS    UT_U8131+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U8131+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8131+OU_X-$
	DB    0,4,7,1
	DS    UT_U8131+OU_HLP-$
	%W    (0)
	DS    UT_U8131+OU_SFT-$
	%W    (UT_SF8131)
	DS    UT_U8131+OU_B_S-$
	%W    (UT_US8131)
	DS    UT_U8131+OU_B_P-$
	%W    (0)
	DS    UT_U8131+OU_B_F-$
	%W    (0)
	DS    UT_U8131+OU_B_T-$
	DB    'Handler',0

UT_US8131:DB  'Sam Hld Down Up  Ini',0

UT_SF8131:
	DB    K_F1
	%W    (UP_S1WHELLGO)
	%W    (UC_SN23)

	DB    K_F2
	%W    (UP_S1WHELLHLD)
	%W    (UC_SN23)

	DB    K_F3
	%W    (UP_S1ASH_DOWN)
	%W    (UC_SN23)

	DB    K_F4
	%W    (UP_S1ASH_UP)
	%W    (UC_SN23)

	DB    K_F5
	%W    (UP_S1ASH_HOME)
	%W    (UC_SN23)

	DB    0

UT_U8132: ; Nastaveni hloubky ramenka
	DS    UT_U8132+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8132+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8132+OU_X-$
	DB    9,4,7,1
	DS    UT_U8132+OU_HLP-$
	%W    (0)
	DS    UT_U8132+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8132+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U8132+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1ASHBOT)	; DP
	DS    UT_U8132+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (10000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8133: ; Manualni ovladani davkovaciho ventilu
	DS    UT_U8133+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U8133+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8133+OU_X-$
	DB    0,2,7,1
	DS    UT_U8133+OU_HLP-$
	%W    (0)
	DS    UT_U8133+OU_SFT-$
	%W    (UT_SF8133)
	DS    UT_U8133+OU_B_S-$
	%W    (UT_US8133)
	DS    UT_U8133+OU_B_P-$
	%W    (UP_S1MANINJ)
	DS    UT_U8133+OU_B_F-$
	%W    (UC_SN23)
	DS    UT_U8133+OU_B_T-$
	DB    'Inject valve',0

UT_US8133:DB  '=== === ==== === ===',0

UT_SF8133:
	DB    0

%IF(0) THEN (
UT_U8134:
	DS    UT_U8134+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8134+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8134+OU_X-$
	DB    8,2,7,1
	DS    UT_U8134+OU_HLP-$
	%W    (0)
	DS    UT_U8134+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8134+OU_A_RD-$
	%W    (UR_Mi)		; A_RD !!!!!!!!!!!!!!!!!
	%W    (TM_STP1)		; A_WR
	DS    UT_U8134+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (STP_NUM)		; DP
	DS    UT_U8134+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (50)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru
)FI

UT_U8135: ; Stav optocidel kotouce
	DS    UT_U8135+OU_VEVJ-$
	DB    2
	DW    BFL_EV
	DS    UT_U8135+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8135+OU_X-$
	DB    13,5,3,1
	DS    UT_U8135+OU_HLP-$
	%W    (0)
	DS    UT_U8135+OU_SFT-$
	%W    (UT_SF8135)
	DS    UT_U8135+OU_A_RD-$
	%W    (UR_ULi)		; A_RD - STP_PSR
	%W    (NULL_A)		; A_WR
	DS    UT_U8135+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1WHELLPSR)	; DP
	DS    UT_U8135+OU_BF_S-$
	%W    (UT_US8135)
	DS    UT_U8135+OU_BF_P-$
	%W    (0)
	DS    UT_U8135+OU_BF_F-$
	%W    (0)
	DS    UT_U8135+OU_BF_T-$
	DB    0
	DB    '_',0
	DB    'A',0
	DB    1
	DB    '_',0
	DB    'B',0
	DB    2
	DB    '_',0
	DB    'C',0
	DB    -1

UT_US8135:DB  'St+ St-  === ===',0

UT_SF8135:
	DB    K_F1
	%W    (UP_S1WHELLSTPP)
	%W    (UC_SN23)

	DB    K_F2
	%W    (UP_S1WHELLSTPM)
	%W    (UC_SN23)

	DB    0

; ---------------------------------
; Teplota

UT_U8151:
	DS    UT_U8151+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8151+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8151+OU_X-$
	DB    9,6,7,1
	DS    UT_U8151+OU_HLP-$
	%W    (0)
	DS    UT_U8151+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8151+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U8151+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1T1RD)	; DP
	DS    UT_U8151+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8152:
	DS    UT_U8152+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8152+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8152+OU_X-$
	DB    9,7,7,1
	DS    UT_U8152+OU_HLP-$
	%W    (0)
	DS    UT_U8152+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8152+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U8152+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1T1OC)	; DP
	DS    UT_U8152+OU_I_F-$
	DB    82H		; format I_F
	%W    (-30000)		; I_L
	%W    (30000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8153:
	DS    UT_U8153+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8153+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8153+OU_X-$
	DB    9,8,7,1
	DS    UT_U8153+OU_HLP-$
	%W    (0)
	DS    UT_U8153+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8153+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U8153+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1T1MC)	; DP
	DS    UT_U8153+OU_I_F-$
	DB    82H		; format I_F
	%W    (-30000)		; I_L
	%W    (30000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Nastaveni IRC kotoucku davkovace

UT_U8161:
	DS    UT_U8161+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8161+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8161+OU_X-$
	DB    9,9,7,1
	DS    UT_U8161+OU_HLP-$
	%W    (0)
	DS    UT_U8161+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8161+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U8161+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1PERIRCW)	; DP
	DS    UT_U8161+OU_I_F-$
	DB    80H		; format I_F
	%W    (-127)		; I_L
	%W    (127)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8162:
	DS    UT_U8162+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8162+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8162+OU_X-$
	DB    9,10,7,1
	DS    UT_U8162+OU_HLP-$
	%W    (0)
	DS    UT_U8162+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8162+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U8162+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1PER_INJT)	; DP
	DS    UT_U8162+OU_I_F-$
	DB    02H		; format I_F
	%W    (0)		; I_L
	%W    (2000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8163:
	DS    UT_U8163+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8163+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8163+OU_X-$
	DB    9,11,7,1
	DS    UT_U8163+OU_HLP-$
	%W    (0)
	DS    UT_U8163+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8163+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U8163+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1PER_VOL1)	; DP
	DS    UT_U8163+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (2000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Rozsirene nastaveni davkovace

UT_U8169:
	DS    UT_U8169+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U8169+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8169+OU_X-$
	DB    0,12,11,1
	DS    UT_U8169+OU_HLP-$
	%W    (0)
	DS    UT_U8169+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U8169+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U8169+OU_B_P-$
	%W    (UT_GR83)
	DS    UT_U8169+OU_B_F-$
	%W    (GR_RQ23)
	DS    UT_U8169+OU_B_T-$
	DB    'New Handler',0

; ---------------------------------
; Ulozeni do EEPROM

UT_U8190:
	DS    UT_U8190+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U8190+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8190+OU_X-$
	DB    0,13,12,1
	DS    UT_U8190+OU_HLP-$
	%W    (0)
	DS    UT_U8190+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U8190+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U8190+OU_B_P-$
	%W    (UP_S1SAVECFG)	; Nastaveni
	DS    UT_U8190+OU_B_F-$
	%W    (UC_SN23)		; Zapis do EEPROM
	DS    UT_U8190+OU_B_T-$
	DB    'Sampler save',0

; *******************************************************************
; Servisni rezim detektoru

UT_GR82:DS    UT_GR82+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR82+OGR_BTXT-$
	%W    (UT_GT82)
	DS    UT_GR82+OGR_STXT-$
	%W    (UT_GS82)
	DS    UT_GR82+OGR_HLP-$
	%W    (0)
	DS    UT_GR82+OGR_SFT-$
	%W    (UT_SF82)
	DS    UT_GR82+OGR_PU-$
	%W    (UT_U8101)
	%W    (UT_U8211)
	%W    (UT_U8212)
	%W    (UT_U8215)
	%W    (UT_U8216)
	%W    (UT_U8251)
	%W    (UT_U8252)
	%W    (UT_U8253)
	%W    (UT_U8290)
	%W    (0)

UT_GT82:DB    '  Service',C_NL
	DB    'Raw Green',C_NL
	DB    'Raw Blue',C_NL
	DB    'Gain Green',C_NL
	DB    'Gain Blue',C_NL
	DB    'Reac ADC',C_NL
	DB    'Reac Offs',C_NL
	DB    'Reac Slop',0

UT_GS82:DB    '=== === ==== === ===',0

UT_SF82:DB    -1
	%W    (UT_SF1)

; ---------------------------------
; Detector

UT_U8211:
	DS    UT_U8211+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8211+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8211+OU_X-$
	DB    10,1,10,1
	DS    UT_U8211+OU_HLP-$
	%W    (0)
	DS    UT_U8211+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8211+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U8211+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_D1ADCBl)	; DP
	DS    UT_U8211+OU_I_F-$
	DB    0C7H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8212:
	DS    UT_U8212+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8212+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8212+OU_X-$
	DB    10,2,10,1
	DS    UT_U8212+OU_HLP-$
	%W    (0)
	DS    UT_U8212+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8212+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U8212+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_D1ADCAl)	; DP
	DS    UT_U8212+OU_I_F-$
	DB    0C7H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8215: ; Zesileni zelene
	DS    UT_U8215+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8215+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8215+OU_X-$
	DB    11,3,5,1
	DS    UT_U8215+OU_HLP-$
	%W    (0)
	DS    UT_U8215+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8215+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U8215+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_D1CHBG)	; DP
	DS    UT_U8215+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (7)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8216: ; Zesileni modre
	DS    UT_U8216+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8216+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8216+OU_X-$
	DB    11,4,5,1
	DS    UT_U8216+OU_HLP-$
	%W    (0)
	DS    UT_U8216+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8216+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U8216+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_D1CHAG)	; DP
	DS    UT_U8216+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (7)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Teplota

UT_U8251:
	DS    UT_U8251+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8251+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8251+OU_X-$
	DB    9,5,7,1
	DS    UT_U8251+OU_HLP-$
	%W    (0)
	DS    UT_U8251+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8251+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U8251+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_D1T1RD)	; DP
	DS    UT_U8251+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8252:
	DS    UT_U8252+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8252+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8252+OU_X-$
	DB    9,6,7,1
	DS    UT_U8252+OU_HLP-$
	%W    (0)
	DS    UT_U8252+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8252+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U8252+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_D1T1OC)	; DP
	DS    UT_U8252+OU_I_F-$
	DB    82H		; format I_F
	%W    (-30000)		; I_L
	%W    (30000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8253:
	DS    UT_U8253+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8253+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8253+OU_X-$
	DB    9,7,7,1
	DS    UT_U8253+OU_HLP-$
	%W    (0)
	DS    UT_U8253+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8253+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U8253+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_D1T1MC)	; DP
	DS    UT_U8253+OU_I_F-$
	DB    82H		; format I_F
	%W    (-32700)		; I_L
	%W    (32700)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Ulozeni do EEPROM

UT_U8290:
	DS    UT_U8290+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U8290+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8290+OU_X-$
	DB    0,8,13,1
	DS    UT_U8290+OU_HLP-$
	%W    (0)
	DS    UT_U8290+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U8290+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U8290+OU_B_P-$
	%W    (UP_D1SAVECFG)	; Nastaveni
	DS    UT_U8290+OU_B_F-$
	%W    (UC_SN23)		; Zapis do EEPROM
	DS    UT_U8290+OU_B_T-$
	DB    'Detector save',0

; *******************************************************************
; Rozsireny rezim ramenka

UT_GR83:DS    UT_GR83+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR83+OGR_BTXT-$
	%W    (UT_GT83)
	DS    UT_GR83+OGR_STXT-$
	%W    (UT_GS83)
	DS    UT_GR83+OGR_HLP-$
	%W    (0)
	DS    UT_GR83+OGR_SFT-$
	%W    (UT_SF83)
	DS    UT_GR83+OGR_PU-$
	%W    (UT_U8301)
	%W    (UT_U8311)
	%W    (UT_U8312)
	%W    (UT_U8313)
	%W    (UT_U8314)
	%W    (UT_U8315)
	%W    (UT_U8316)
	%W    (UT_U8317)
	%W    (UT_U8390)
	%W    (0)

UT_GT83:DB    '  New handler',C_NL
	DB    'Rot act',C_NL
	DB    'Rot pos1',C_NL
	DB    'Rot pos2',C_NL
	DB    'Rot pos3',C_NL
	DB    'Wash depth',C_NL
	DB    'Bottom 25',C_NL
	DB    'Bottom 80',0

;UT_GS83:DB    '=== === ==== === ===',0
UT_GS83:DB    'Sam Hld Down Up  Ini',0

UT_SF83:

	DB    K_F1
	%W    (UP_S1WHELLGO)
	%W    (UC_SN23)

	DB    K_F2
	%W    (UP_S1WHELLHLD)
	%W    (UC_SN23)

	DB    K_F3
	%W    (UP_S1ASH_DOWN)
	%W    (UC_SN23)

	DB    K_F4
	%W    (UP_S1ASH_UP)
	%W    (UC_SN23)

	DB    K_F5
	%W    (UP_S1ASH_HOME)
	%W    (UC_SN23)

	DB    -1
	%W    (UT_SF1)

; ---------------------------------
; Zpet

UT_U8301:
	DS    UT_U8301+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U8301+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8301+OU_X-$
	DB    16,0,4,1
	DS    UT_U8301+OU_HLP-$
	%W    (0)
	DS    UT_U8301+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8301+OU_B_S-$
	%W    (UT_US8301)
	DS    UT_U8301+OU_B_P-$
	%W    (UT_GR81)
	DS    UT_U8301+OU_B_F-$
	%W    (GR_RQ23)
	DS    UT_U8301+OU_B_T-$
	DB    'Back',0

UT_US8301:DB  '=== === ==== === ===',0

; ---------------------------------
; Otaceni ramenka

UT_U8311:
	DS    UT_U8311+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8311+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8311+OU_X-$
	DB    14,1,6,1
	DS    UT_U8311+OU_HLP-$
	%W    (0)
	DS    UT_U8311+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8311+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U8311+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1ASH_ROT)	; DP
	DS    UT_U8311+OU_I_F-$
	DB    080H		; format I_F
	%W    (-7FFFH)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8312:
	DS    UT_U8312+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8312+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8312+OU_X-$
	DB    14,2,6,1
	DS    UT_U8312+OU_HLP-$
	%W    (0)
	DS    UT_U8312+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8312+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U8312+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1ASH_RP1)	; DP
	DS    UT_U8312+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (6000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8313:
	DS    UT_U8313+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8313+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8313+OU_X-$
	DB    14,3,6,1
	DS    UT_U8313+OU_HLP-$
	%W    (0)
	DS    UT_U8313+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8313+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U8313+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1ASH_RP2)	; DP
	DS    UT_U8313+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (6000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8314:
	DS    UT_U8314+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8314+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8314+OU_X-$
	DB    14,4,6,1
	DS    UT_U8314+OU_HLP-$
	%W    (0)
	DS    UT_U8314+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8314+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U8314+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1ASH_RP3)	; DP
	DS    UT_U8314+OU_I_F-$
	DB    000H		; format I_F
	%W    (0)		; I_L
	%W    (6000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U8315:
	DS    UT_U8315+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8315+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8315+OU_X-$
	DB    14,5,6,1
	DS    UT_U8315+OU_HLP-$
	%W    (0)
	DS    UT_U8315+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8315+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U8315+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1ASH_WASH)	; DP
	DS    UT_U8315+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (10000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru


UT_U8316: ; Nastaveni hloubky ramenka pro 25 kotouc
	DS    UT_U8316+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8316+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8316+OU_X-$
	DB    14,6,6,1
	DS    UT_U8316+OU_HLP-$
	%W    (0)
	DS    UT_U8316+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8316+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U8316+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1ASHBOT)	; DP
	DS    UT_U8316+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (10000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru


UT_U8317: ; Nastaveni hloubky ramenka pro 80 kotouc
	DS    UT_U8317+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U8317+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8317+OU_X-$
	DB    14,7,6,1
	DS    UT_U8317+OU_HLP-$
	%W    (0)
	DS    UT_U8317+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U8317+OU_A_RD-$
	%W    (UR_ULi)		; A_RD
	%W    (UW_ULi)		; A_WR
	DS    UT_U8317+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (UP_S1ASHBOT2)	; DP
	DS    UT_U8317+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (10000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Ulozeni do EEPROM

UT_U8390:
	DS    UT_U8390+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U8390+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U8390+OU_X-$
	DB    0,8,12,1
	DS    UT_U8390+OU_HLP-$
	%W    (0)
	DS    UT_U8390+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U8390+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U8390+OU_B_P-$
	%W    (UP_S1SAVECFG)	; Nastaveni
	DS    UT_U8390+OU_B_F-$
	%W    (UC_SN23)		; Zapis do EEPROM
	DS    UT_U8390+OU_B_T-$
	DB    'Sampler save',0



; *******************************************************************
; STAND-BY rezim

UT_GR91:DS    UT_GR91+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR91+OGR_BTXT-$
	%W    (UT_GT91)
	DS    UT_GR91+OGR_STXT-$
	%W    (0)
	DS    UT_GR91+OGR_HLP-$
	%W    (0)
	DS    UT_GR91+OGR_SFT-$
	%W    (UT_SF91)
	DS    UT_GR91+OGR_PU-$
	%W    (0)

UT_GT91:DB    '*** AAA400  %AAA_VERSION ***',C_NL
	DB    'Developed by PiKRON',C_NL
	DB    C_NL
	DB    'Stand-by mode',0

UT_SF91:DB    K_STBY
	%W    (GL_PWRON)
	%W    (GLOB_RQ23)

	DB    0

UT_GR92:DS    UT_GR92+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR92+OGR_BTXT-$
	%W    (UT_GT92)
	DS    UT_GR92+OGR_STXT-$
	%W    (0)
	DS    UT_GR92+OGR_HLP-$
	%W    (0)
	DS    UT_GR92+OGR_SFT-$
	%W    (UT_SF92)
	DS    UT_GR92+OGR_PU-$
	%W    (UT_U9210)
	%W    (UT_U9220)
	%W    (0)

UT_GT92:DB    'Are you sure about',C_NL
	DB    'switching off?',0

UT_SF92:DB    K_STBY
	%W    (UT_GR15)
	%W    (GR_RQ23)

	DB    K_F4
	%W    (UT_GR15)
	%W    (GR_RQ23)

	DB    K_F5
	%W    (GL_STBY)
	%W    (GLOB_RQ23)

	DB    -1
	%W    (UT_SF1)

UT_U9210:
	DS    UT_U9210+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U9210+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U9210+OU_X-$
	DB    13,3,2,1
	DS    UT_U9210+OU_HLP-$
	%W    (0)
	DS    UT_U9210+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U9210+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U9210+OU_B_P-$
	%W    (UT_GR15)	;
	DS    UT_U9210+OU_B_F-$
	%W    (GR_RQ23)		;
	DS    UT_U9210+OU_B_T-$
	DB    'NO',0

UT_U9220:
	DS    UT_U9220+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U9220+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U9220+OU_X-$
	DB    17,3,2,1
	DS    UT_U9220+OU_HLP-$
	%W    (0)
	DS    UT_U9220+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U9220+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U9220+OU_B_P-$
	%W    (GL_STBY)	;
	DS    UT_U9220+OU_B_F-$
	%W    (GLOB_RQ23)		;
	DS    UT_U9220+OU_B_T-$
	DB    'YES',0

END
