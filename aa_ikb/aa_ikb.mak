#   Project file pro detektor AAA
#         (C) Pisoft 1996

aa_ikb.obj: aa_ikb.asm
	a51 aa_ikb.asm $(par) debug

ik_tty.obj: ik_tty.asm
	a51 ik_tty.asm $(par) debug

ik_vec.obj: ik_vec.asm
	a51 ik_vec.asm $(par)

rs232_1.obj: rs232_1.asm
	a51 rs232_1.asm $(par) debug

pb_prg.obj: pb_prg.asm
	a51 pb_prg.asm $(par)

codeadr=09000H

aa_ikb.   : aa_ikb.obj ik_tty.obj ik_vec.obj rs232_1.obj pb_prg.obj ..\pblib\pb.lib
	l51 aa_ikb.obj,ik_tty.obj,ik_vec.obj,rs232_1.obj,pb_prg.obj,..\pblib\pb.lib code($(codeadr)) xdata(8000H) ramsize(100h) ixref

aa_ikb.hex : aa_ikb.
	ohs51 aa_ikb

	  : aa_ikb.hex
#	sendhex aa_ikb.hex /p4 /m3 /t2 /g40960 /b19200
#	sendhex aa_ikb.hex /p3 /m3 /t2 /g40960
	unixcmd -d ul_sendhex -m 3 -g 0
	unixcmd -d ul_sendhex -m 3 -g 0x9000 aa_ikb.hex
