#   Project file pro detektor AAA
#         (C) Pisoft 1996

aa_ikb.obj: aa_ikb.asm
	a51 aa_ikb.asm $(par) debug

ik_tty.obj: ik_tty.asm
	a51 ik_tty.asm $(par)

ik_vec.obj: ik_vec.asm
	a51 ik_vec.asm $(par)

rs232_1.obj: rs232_1.asm
	a51 rs232_1.asm $(par) debug

pb_prg.obj: pb_prg.asm
	a51 pb_prg.asm $(par)

          : aa_ikb.
	del aa_ikb.

aa_ikb.   : aa_ikb.obj ik_tty.obj ik_vec.obj rs232_1.obj pb_prg.obj ..\pblib\pb.lib
	l51 aa_ikb.obj,ik_tty.obj,ik_vec.obj,rs232_1.obj,pb_prg.obj,..\pblib\pb.lib xdata(8000H) ramsize(100h) ixref

aa_ikb.hex : aa_ikb.
	ohs51 aa_ikb
	del  aa_ikb.

