#   Project file pro detektor AAA
#         (C) Pisoft 1996

ds.obj    : ds.asm config.h
	a51 ds.asm $(par) debug

ds_vec.obj: ds_vec.asm config.h
	a51 ds_vec.asm $(par)

pb_prg.obj: pb_prg.asm
	a51 pb_prg.asm $(par)

rs232_1.obj: rs232_1.asm config.h
	a51 rs232_1.asm $(par) debug

ds.	  : ds.obj ds_vec.obj rs232_1.obj pb_prg.obj ..\pblib\pb.lib
	l51 ds.obj,ds_vec.obj,rs232_1.obj,pb_prg.obj,..\pblib\pb.lib code(00000H) xdata(8000H) ramsize(100h) ixref
#								     code(09000H)

ds.hex    : ds.
	ohs51 ds
	copy ds.hex ds.he1
	ihexcsum -o ds.hex -l 0x8000 -L 0x7f00 -A 0x9 -F lp ds.hex ds3r.hex

	  : ds.hex
#	sendhex ds.hex /p4 /m3 /t2 /g40960 /b11520
#	sendhex ds.hex /p4 /m3 /t2 /g40960 /b16666
#	sendhex ds.hex /p4 /m3 /t2 /g36864 /b9600
#	sendhex ds.hex /p3 /m3 /t2 /g36864 /b9600
#	unixcmd -d ul_sendhex -m 3 -g 0
#	unixcmd -d ul_sendhex ds.hex -m 3 -g 0x9000

	: ds2r.mcs
#	sendhex ds2r.mcs /p4 /m3 /t2 /b19200
