;********************************************************************
;*           Modul casoveho programu - PB_PRG.ASM                   *
;*                     Knihovni modul                               *
;*                  Stav ke dni 10.02.1997                          *
;*                      (C) Pisoft 1997                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_UI)
$INCLUDE(%INCH_ULAN)
$INCLUDE(%INCH_UL_OI)
$INCLUDE(%INCH_UL_OC)
$LIST

; Struktura popisu programove radky
OPR_LEN	SET   0
%STRUCTM(OPR,VPRJ,1)	; instrukce jmp
%STRUCTM(OPR,VPR ,2)	; rutina zpracovani udalosti !!! DW
%STRUCTM(OPR,IID ,1)	; identifikacni cislo pristroje - 0 .. tento
%STRUCTM(OPR,OID ,2)	; identifikacni cislo objektu
%STRUCTM(OPR,LLEN,1)	; pocatecni delka radky po insertu
%STRUCTM(OPR,GR  ,2)	; ukazatel na skupinu pro standartni editaci
%STRUCTM(OPR,DESC,2)	; ukazatel textovy popis radky, i pro UL_OI

; Sluzby radky
PRT_DO	EQU   1		; proved radek

; *******************************************************************

EXTRN	CODE(ADDATDP,xxMOVE,xMDPDP)

PB_PR_C SEGMENT CODE
;PB_PR_D SEGMENT DATA
;PB_PR_B SEGMENT DATA BITADDRESSABLE
PB_PR_X SEGMENT XDATA

RSEG	PB_PR_X

; Kazda radka zacina 1B delky, dale 1B typ a 2B time

PR_AOT:	DS    2		; ukazatel na pole popisu typu radek
PR_SF:	DS    2		; bezpecnostni priznak
PR_MBEG:DS    2		; pocatek pameti pro programy
PR_MEND:DS    2		; konec pameti pro programy
PR_PNUM:DS    2		; cislo aktualniho programu
PR_PBEG:DS    2		; pocatek aktualniho programu
PR_PEND:DS    2		; konec posledniho programu
PR_LAD:	DS    2		; adresa aktualni radky
PR_CHF:	DS    1		; zmena programu

RSEG	PB_PR_C

; Ulozi DPTR do [PR_LAD], meni pouze R0
SPR_LAD:MOV   A,DPL
	MOV   R0,DPH
	MOV   DPTR,#PR_LAD
	MOVX  @DPTR,A
	INC   DPTR
	XCH   A,R0
	MOVX  @DPTR,A
	MOV   DPL,R0
	MOV   DPH,A
	RET

; Nacte do DPTR [PR_LAD]
GPR_LAD:MOV   DPTR,#PR_LAD
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R0
	MOV   DPH,A
	RET

; Vrati popis programove radky [DPTR] v registru DPTR
GPR_LDS:MOV   R4,DPL
	MOV   R5,DPH
	MOVX  A,@DPTR
	JZ    GPR_TD9
	INC   DPTR
	MOVX  A,@DPTR
; Nalezne popis programove radky typu ACC
GPR_TDS:JZ    GPR_TD9
	ANL   A,#03FH
	RL    A
	MOV   R0,A
	MOV   DPTR,#PR_AOT
	MOVX  A,@DPTR
	ADD   A,R0
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#0
	MOV   DPL,R0		; DP=[PR_AOT]+typ*2
	MOV   DPH,A
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R0
	MOV   DPH,A
	MOV   A,#1
GPR_TD9:RET

; proved radku na adrese R45 a s popisem [DPTR]
PR_LNDO:MOV   R0,#PRT_DO
; Skok na rutinu radky, R0 sluzba, R45 adresa radky
JMP_VPR:CLR   A
	JMP   @A+DPTR

; Vloz R2 bytu do programu
; F0=1 pri preplneni pameti programu

PR_SINS:MOV   DPTR,#PR_LAD
	CALL  xLDR45i
PR_SIN0:MOV   DPTR,#PR_PEND
	MOVX  A,@DPTR
	MOV   R0,A
	ADD   A,R2
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R1,A	; R01 = PR_END
	ADDC  A,#0
	MOV   R7,A	; R67 = PR_END+R2
	MOV   DPTR,#PR_MEND
	MOVX  A,@DPTR
	SETB  C
	SUBB  A,R6
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,R7	; ? PR_MEND > R67
	JNC   PR_SIN1
	SETB  F0
	RET
PR_SIN1:CLR   F0
	MOV   DPTR,#PR_CHF
	MOV   A,#2
	MOVX  @DPTR,A
	MOV   DPTR,#PR_PEND
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	; copy from R01 to R67 until R45 = R01
PR_SIN2:MOV   DPL,R0
	MOV   DPH,R1
	MOVX  A,@DPTR
	MOV   DPL,R6
	MOV   DPH,R7
	MOVX  @DPTR,A
	MOV   A,R0
	XRL   A,R4
	JNZ   PR_SIN3
	MOV   A,R1
	XRL   A,R5
	JZ    PR_SIN6
PR_SIN3:DEC   R0
	CJNE  R0,#-1,PR_SIN4
	DEC   R1
PR_SIN4:DEC   R6
	CJNE  R6,#-1,PR_SIN2
	DEC   R7
PR_SIN5:JMP   PR_SIN2
	; end of move
PR_SIN6:MOV   DPL,R4
	MOV   DPH,R5
	MOV   A,R2
NUL_SPC:MOV   R0,A
	PUSH  DPL
	PUSH  DPH
PR_SIN7:MOV   A,R0
	JZ    PR_SIN8
	DEC   R0
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	JMP   PR_SIN7
PR_SIN8:POP   DPH
	POP   DPL
PR_SIN9:RET

; Smaze aktualni radku

PR_LDEL:CALL  PR_LDE0
	JMP   PR_LCH

PR_LDE0:MOV   DPTR,#PR_CHF
	MOV   A,#2
	MOVX  @DPTR,A
	MOV   DPTR,#PR_PEND
	CALL  xLDR23i
	CALL  GPR_LAD
	MOV   A,#1
	MOVC  A,@A+DPTR
	ANL   A,#03FH
	JZ    PR_SIN9
PR_SDEL:MOVX  A,@DPTR
	MOV   R4,DPL
	MOV   R5,DPH
	CALL  ADDATDP
PR_SDE1:CLR   C
	MOV   A,DPL
	XCH   A,R2
	SUBB  A,R2
	MOV   R0,A
	MOV   A,DPH
	XCH   A,R3
	SUBB  A,R3
	MOV   R1,A
	CALL  xxMOVE
	MOV   DPTR,#PR_PEND
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   DPL,R4
	MOV   DPH,R5
	CLR   A
	MOVX  @DPTR,A
	RET

; Vloz novou radku typu R3

PR_LINS:MOV   A,R3
	CALL  GPR_TDS
	MOV   A,#OPR_LLEN
	MOVC  A,@A+DPTR
	MOV   R2,A
	CALL  PR_SINS
	JNB   F0,PR_LINS1
PR_LINSE:			; *** memory overflow error ***
	MOV   R7,#ET_ERR	; Error
	%LDR45i(0)
	JMP   EV_POST
PR_LINS1:CALL xSVR23i
	PUSH  DPL
	PUSH  DPH
	CALL  PR_PRLN
	INC   DPTR
	INC   DPTR
	CALL  xLDR23i
	POP   DPH
	POP   DPL
	CALL  xSVR23i
	JMP   PR_LCH

; Najde predchazejici radku

PR_PRLN:MOV   DPTR,#PR_LAD
	CALL  xLDR23i
	MOV   DPTR,#PR_PBEG
	CALL  xMDPDP
	MOV   A,#01
	MOVC  A,@A+DPTR
	CJNE  A,#040H,PR_PRL0
	CALL  PR_NXL0
PR_PRL0:MOV   R6,DPL
	MOV   R7,DPH
PR_PRL1:MOV   A,R2
	CJNE  A,DPL,PR_PRL3
	MOV   A,R3
	XRL   A,DPH
	JZ    PR_PRL9
PR_PRL3:MOV   R6,DPL
	MOV   R7,DPH
	CALL  PR_NXL
	JNZ   PR_PRL1
PR_PRL9:MOV   DPL,R6
	MOV   DPH,R7
	RET

; Prechod na nasledujici radku
; Vystup:DP .. dalsi radka pri A<>0
;         A =0 konec programu
;         F0=1 prusvih

PR_NXLN:CALL  GPR_LAD
PR_NXL: MOV   A,#001
	MOVC  A,@A+DPTR
	ANL   A,#03FH
	JZ    PR_NXL1
PR_NXL0:MOVX  A,@DPTR
	JZ    PR_NXL1
	ADD   A,DPL
	MOV   DPL,A
	JNC   PR_NXL1
	INC   DPH
	MOV   A,DPH
	INC   A
	JNZ   PR_NXL1	; !!!!!!! spatna kontrola
	SETB  F0
PR_NXL1:RET

; Vyber programu podle [PR_PNUM]
SPR_BEG:MOV  DPTR,#PR_PNUM
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   R2,#0
	MOV   DPTR,#PR_MBEG
	CALL  xMDPDP
	CALL  PRP_PR1
SPR_BEG1:INC  R2
	MOV   A,R2
	CLR   C
	SUBB  A,R3
	JNC   SPR_BEG9
	MOV   A,#1
	MOVC  A,@A+DPTR
	JZ    SPR_BEG9
	CALL  PR_NXL0
	JZ    SPR_BEG9
SPR_BEG2:CALL PR_NXL
	JNZ   SPR_BEG2
	JMP   SPR_BEG1
SPR_BEG9:CALL SPR_LAD
	MOV   A,DPL
	MOV   R0,DPH
	MOV   DPTR,#PR_PBEG
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	MOV   A,R2
	MOV   DPTR,#PR_PNUM
	MOVX  @DPTR,A
	RET

; Priprava na praci s programem

PRP_PRG:MOV   DPTR,#PR_PBEG
	CALL  xMDPDP
PRP_PR1:CALL  SPR_LAD
	MOV   DPTR,#PR_SF
	MOV   A,#0ABH
	MOVX  @DPTR,A
	SETB  FL_DRAW
	CALL  GPR_LAD
PR_TST9:RET

; Kontrola programu

PR_TST:	CLR   F0
	CALL  PRP_PRG
	MOV   A,#1
	MOVC  A,@A+DPTR
	XRL   A,#040H
	JZ    PR_TST9

; Zalozeni noveho programu

PR_PINS:CALL  PRP_PRG
PR_PINS1:MOV  R2,#010H
	MOV   R3,#040H
	CALL  PR_SINS
	JNB   F0,PR_PINS2
	JMP   PR_LINSE
PR_PINS2:CALL xSVR23i
	MOV   R0,#8
	MOV   A,#' '
PR_PINS3:MOVX @DPTR,A
	INC   DPTR
	DJNZ  R0,PR_PINS3
PR_PDEL9:JMP  PR_LCH

; Smazani programu

PR_PDEL:;MOV   DPTR,#T_DL_PR	!!!!!!!!
	;CALL  QUES_YN		!!!!!!!!
	;JZ    PR_PDEL9
PR_PDEL0:MOV  DPTR,#PR_CHF
	MOV   A,#2
	MOVX  @DPTR,A
	MOV   DPTR,#PR_PEND
	CALL  xLDR23i
	CALL  PRP_PRG
	MOV   R4,DPL
	MOV   R5,DPH
	CALL  PR_NXL0
PR_PDEL1:CALL PR_NXL
	JNZ   PR_PDEL1
	CALL  PR_SDE1
	JMP   PR_LCH

; Kontrola zadavaneho casu v posloupnosti

PR_TIMT:CALL  PR_PRLN
	CALL  PR_TIMTS
	JZ    PR_TIMT1
	JC    PR_TIMT2
PR_TIMT1:CALL PR_NXLN
	JZ    PR_TIMT2
	CALL  PR_TIMTS
	JZ    PR_TIMT9
	JC    PR_TIMT9
PR_TIMT2:CALL GPR_LAD
	INC   DPTR
	INC   DPTR
	CALL  xSVR23i
				; *** time sekq. error ***
	MOV   R7,#ET_ERR	; Error
	%LDR45i(0)
	JMP   EV_POST

PR_TIMTS:CALL xLDR23i
	ANL   A,#03FH
	JZ    PR_TIMT9
	MOV   A,R2
	JZ    PR_TIMT9
	CALL  xLDR23i
	CALL  CMPi
PR_TIMT9:RET

; Nastavi PR_PEND
PR_FEND:MOV   DPTR,#PR_MEND
	CALL  xLDR23i
	MOV   DPTR,#PR_MBEG
	CALL  xMDPDP
PR_FEND1:MOV  R4,DPL
	MOV   R5,DPH
	CALL  PR_NXL0
	JZ    PR_FEND2
	MOV   A,#01
	MOVC  A,@A+DPTR
	JZ    PR_FEND2
	CLR   C		; kontrola konce pameti programu
	MOV   A,DPL
	SUBB  A,R2
	MOV   A,DPH
	SUBB  A,R3
	JC    PR_FEND1
	MOV   DPL,R4
	MOV   DPH,R5
PR_FEND2:CLR  A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   R4,DPL
	MOV   R5,DPH
	MOV   DPTR,#PR_PEND
	CALL  xSVR45i
	RET

; Orovani R2 xorovani R3 typu radky

SPR_IMP:CALL  GPR_LAD
	MOVX  A,@DPTR
	JZ    SPR_IMP9
	INC   DPTR
	MOVX  A,@DPTR
	ORL   A,R2
	XRL   A,R3
	MOVX  @DPTR,A
	SETB  FL_DRAW
SPR_IMP9:RET

PR_LTST0:MOV  R1,#0

; Testuje radek na R3 s maskou R2
; R23=Time
; DPTR = [PR_LAD]+R1+4

PR_LTST:CALL  GPR_LAD
	MOVX  A,@DPTR
	JZ    PR_LTST1
	INC   DPTR
	MOVX  A,@DPTR
	ANL   A,R2
	XRL   A,R3
	MOV   R0,A
	INC   DPTR
	CALL  xLDR23i
	MOV   A,R1
	CALL  ADDATDP
	MOV   A,R0
	RET
PR_LTST1:SETB F0
	MOV   A,#-1
	RET

PR_LCH:	SETB  FL_DRAW
	RET

; *******************************************************************

RSEG	PB_PR_X

%IF (%STATIC_UI_AD) THEN (
UPR_LFOC:DS   1		; cislo zpracovavane polozky
GR_UFOC:DS    1		; cislo focusovane polozky
GR_UFFL:DS    1		; priznaky focusovane polozky
GR_TMP:	DS    4		; pomocny buffer
UPR_TLAD:DS   2		;
)FI

RSEG	PB_PR_C

; Adresu do radky programu
; OU_DP   .. kolik radek za PR_LAD
; OU_DP+1 .. offset v radce
UI_PRLA:MOV   A,#OU_DP
	MOVC  A,@A+DPTR
	MOV   R2,A		; Pocet radek
	MOV   A,#OU_DP+1
	MOVC  A,@A+DPTR
	MOV   R3,A		; Offset
UI_PRLR:MOV   DPTR,#PR_LAD
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R0
	MOV   DPH,A
	INC   R2
	SJMP  UI_PRL3
UI_PRL2:MOV   A,R0
	ADD   A,DPL
	MOV   DPL,A
	CLR   A
	ADDC  A,DPH
	MOV   DPH,A
UI_PRL3:MOVX  A,@DPTR		; delka radky
	MOV   R0,A
	JZ    UI_PRL9
	MOV   A,#1
	MOVC  A,@A+DPTR		; typ radky
	ANL   A,#3FH
	JZ    UI_PRL9
	DJNZ  R2,UI_PRL2
	MOV   A,R3
	ADD   A,DPL
	MOV   DPL,A
	CLR   A
	ADDC  A,DPH
	MOV   DPH,A
	MOV   A,R0
	RET
UI_PRL9:SETB  F0
	RET

; Nacte R45 z radky programu
; OU_DP   .. kolik radek za PR_LAD
; OU_DP+1 .. offset v radce
UR_PRi:	CALL  UI_PRLA
	JB    F0,UR_PRi9
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   A,#1
UR_PRi9:RET

; Ulozi R45 do radky programu
; OU_DP   .. kolik radek za PR_LAD
; OU_DP+1 .. offset v radce
UW_PRi:	CALL  UI_PRLA
	JB    F0,UW_PRi9
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   A,#1
UW_PRi9:RET

; Zpracovani udalosti editorem programu
PRG_EV:

			; UPR_TLAD = PR_LAD

			; ulozit UI_AV_X,UI_AV_Y,UI_AV_SX,UI_AV_SY
			; UI_AV_X+=UI_AU.OU_X, UI_AV_Y+=UI_AU.OU_Y



			; PR_LAD = UPR_TLAD
	RET

; *******************************************************************

%IF (0) THEN (

; Jdi o radku nahoru

SET_PRE:CALL PR_PRLN
	JMP   SET_NE1

; Jdi o radku dolu

SET_NEX:CALL  PR_NXLN
SET_NE1:CALL  SET_LAD
	JMP   PR_LCH

; Pomocna akce pri ENTER

ENT_PRG:CALL  SEL_LT
	INC   DPTR
	MOVX  A,@DPTR
	DEC   A
ENT_PR1:MOV   R0,A
	MOV   DPTR,#LINE_PO
	MOVX  A,@DPTR
	XRL   A,R0
	JZ    SET_NEX
	MOV   R2,#1

; Modifikuje LINE_PO podle R2
; POZOR : sp=sp-2

MOD_LNP:MOV   DPTR,#LINE_PO
	MOVX  A,@DPTR
	ADD   A,R2
	MOVX  @DPTR,A
	JMP   SET_LNP

; Jdi na zacatek programu

GO_PRBE:MOV   DPTR,#PROG_BE
	CALL  xMDPDP
	MOVX  A,@DPTR
	JZ    SET_LAD
	MOV   A,#1
	MOVC  A,@A+DPTR
	CJNE  A,#040H,SET_LAD
	CALL  PR_NXL0

; Nastavi radku na DPTR

SET_LAD:MOV   R4,DPL
	MOV   R5,DPH
	MOV   DPTR,#LINE_P
	CALL  xSVR45i
PROG_IR:RET

; Prejde do modu editovani programu
; POZOR : sp=sp-2

PROG_IN:CALL  PR_TST
	JB    F0,PROG_IR
	MOV   DPTR,#PR_SF
	MOV   A,#0ABH
	MOVX  @DPTR,A
	CALL  GO_PRBE
	SETB  %PROG_FL
	CALL  LEDWR
	CALL  PR_FEND

; Nastaveni radku programu
; nastavi DIT a POT
; POZOR : sp=sp-2

SET_LN: MOV   DPTR,#LINE_PO
	CLR   A
	MOVX  @DPTR,A
	CALL  SEL_LT
	INC   DPTR
	INC   DPTR
	CALL  cLDR23i
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#F_LINE
	CALL  xSVR23i
	MOV   A,#LOW  WRT_1
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH WRT_1
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
	CALL  cLDR23i
	MOV   DPTR,#DP_FLEX
	CALL  xSVR23i
	SETB  FL_DRAW

; Nastavi POT podle LNT a LINE_PO
; POZOR : sp=sp-2

SET_LNP:MOV   DPTR,#LINE_PO
	MOVX  A,@DPTR
	MOV   R7,A
	CALL  SEL_LT
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R6,A
	MOV   A,R7
	CLR   C
	SUBB  A,R6
	MOV   A,R7
	JC    SET_LN2
	JNB   ACC.7,SET_LN1
	MOV   R6,#1
SET_LN1:MOV   DPTR,#LINE_PO
	MOV   A,R6
	DEC   A
	MOVX  @DPTR,A
	JMP   SET_LNP
SET_LN2:RL    A
	ADD   A,#5
	CALL  ADDATDP
	CALL  cLDR23i
	JMP   SET_POT

; DPTR naplni ukazatelem na LNT
; zpracovavane radky

SEL_LT: CALL  GET_LAD
	MOVX  A,@DPTR
	INC   DPTR
	JNZ   SEL_LT2
SEL_LT1:CLR   A
	MOVX  @DPTR,A
SEL_LT2:MOVX  A,@DPTR
	ANL   A,#03FH
	CJNE  A,#LNT_MAX,SEL_LT3
SEL_LT3:JNC   SEL_LT1
SEL_LT4:ANL   A,#03FH
	RL    A
	MOV   DPTR,#LNT_DIR
	CALL  ADDATDP
	JMP   cMDPDP
)FI

END