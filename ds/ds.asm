$NOMOD51
;********************************************************************
;*         Spektrofotometricky detektor - DS.ASM                    *
;*                       Hlavni modul                               *
;*                  Stav ke dni 10.10.1997                          *
;*                      (C) Pisoft 1997                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_ADR)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_AL)
$INCLUDE(%INCH_AF)
$INCLUDE(%INCH_UI)
$INCLUDE(%INCH_ULAN)
$INCLUDE(%INCH_UL_OI)
;$INCLUDE(%INCH_UL_OC)
$INCLUDE(%INCH_REGS)
$INCLUDE(%INCH_RS232)
$LIST

%DEFINE (WITH_UL_DY) (1)
%DEFINE (WITH_UL_DY_EEP) (1)	; vyrobnim cislem v EEPROM
%DEFINE (WITH_LCD_CG) (1)	; pouzit definovane znaky pro LCD

EXTRN	CODE(PRINThb,PRINThw,INPUThw,SEL_FNC,PRINTli)
EXTRN	CODE(MONITOR)
EXTRN	CODE(xMDPDP,ADDATDP)

%IF(%WITH_UL_DY) THEN (
EXTRN	CODE(UD_INIT,UD_RQ)
)FI

EXTRN   CODE(IHEXLD)

PUBLIC	INPUTc,KBDBEEP,ERRBEEP,RES_STAR,DPLOCK

%IF(%WITH_UL_DY_EEP) THEN (
PUBLIC	SER_NUM
CSEG AT	80A0H
SER_NUM:DS    10H	; Instrument unigue serial number
XSEG AT	80A0H
	DS    10H	; XDATA overlay
)FI

%IF(%WITH_LCD_CG) THEN (
EXTRN	CODE(LCDWRCG1)
CG_LAM	EQU   010H	; Symbol lambda
CG_TAU	EQU   011H	; Symbol tau
)ELSE(
CG_LAM	EQU   0BDH	; Symbol lambda
CG_TAU	EQU   0B2H	; Symbol tau
)FI

CSEG    AT    00009H
EPRCHK1:DW    00000H          ; Kontrolni soucet eprom
CSEG    AT    00019H
EPRCHK2:DW    0FFFFH          ; CPL kontrolniho soucetu eprom

DS____C SEGMENT CODE
DS____D SEGMENT DATA
DS____B SEGMENT DATA BITADDRESSABLE
DS____X SEGMENT XDATA

STACK	DATA  80H

RSEG	DS____B

HW_FLG: DS    1

LEB_FLG:DS    1			; Blikani ledek

ITIM_RF BIT   HW_FLG.7
FL_DIPR	BIT   HW_FLG.6		; Displej pripojen
LEB_PHA	BIT   HW_FLG.5		; Pro blikani ledek
FL_SPILCK BIT HW_FLG.4		; SPI communication locked

RSEG	DS____D

DPLOCK: DS    1			; Maximalni pouzivane DPTR

RSEG	DS____X

C_R_PER	EQU   5
REF_PER:DS    1

TMP:	DS    16
TMP1:	DS    16

RSEG	DS____C

RES_STAR:
	MOV   IEN0,#0
	MOV   IEN1,#0
	MOV   IEN2,#0
	MOV   IP0,#00010010B  ; S0INT pri 3, TF0 pri 1
	MOV   IP1,#00010000B  ; 0 .. S1/IE0/IADC, 1 .. TF0/IEX2
			      ; 2 .. IE1/IEX3, 3 .. TF1/CTF/IEX4
			      ; 4 .. S0/IEX5, 5 .. TF2/EXTF2/IEX6
	MOV   SP,#STACK
	MOV   DPSEL,#0
	MOV   DPLOCK,#0
	MOV   PSW,#0
	MOV   PCON,#10000000B ; Bd = OSC/12/16/(256-TH1)
	MOV   TMOD,#00100001B ; 16 bit timer 0;
	MOV   TCON,#01010101B ; citac 0 a 1 cita ; interapy hranou
	%VECTOR(TIMER0,I_TIME); Realny cas z citace 0
	SETB  ET0
	MOV   HW_FLG,#080H
	MOV   LEB_FLG,#0
	CLR   %BEEP_FL
	%VECTOR(EXTI0,I_XAD)	; Preruseni z prevodniku INT0
	MOV   R4,#000H		; Offset XAD
	MOV   R5,#060H
	MOV   R6,#0CBH
	MOV   R7,#07CH
	MOV   DPTR,#XAD_OFS
	CALL  xSVl
	MOV   FL_ABS,#ZER_MSK	; Vynulovat
	MOV   WL_FLG,#WLF_INIC	; Kalibrovat ATMEL

	CLR   A
	MOV   DPTR,#EV_WDGT
	MOVX  @DPTR,A
	MOV   DPTR,#TIME
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#uL_SBPO
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#LS_TDEL
	MOVX  @DPTR,A
	MOV   DPTR,#LS_DLST
	MOVX  @DPTR,A
	MOV   DPTR,#WL_OFS
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#AUX_BUF
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	CALL  AUX_ICH

	CALL  I_TIMRI
	CALL  I_TIMRI

	CALL  LCDINST
	JNZ   STRT30
	SETB  FL_DIPR
   %IF(%WITH_LCD_CG) THEN (
	CALL  LCDCGINIT
   )FI
	CALL  LEDWR
STRT30:
	MOV   DPTR,#REF_PER
	MOV   A,#C_R_PER
	MOVX  @DPTR,A

	CALL  ADC_INI

	JMP   L0

INPUTc:	CALL  SCANKEY
	JZ    INPUTc
	RET

; Pipnuti na klavese klavesnice

;KBDBEEP:JMP   KBDSTDB
KBDBEEP:MOV   A,#2
BEEP:	MOV   DPTR,#BEEPTIM
	MOVX  @DPTR,A
	SETB  %BEEP_FL       ; Pipnuti
%IF(1) THEN (
	MOV   DPTR,#LED
	MOV   A,LED_FLG
	MOVX  @DPTR,A
)FI
	MOV   A,R2
	RET
ERRBEEP:MOV   A,#8
	JMP   BEEP

%IF(%WITH_LCD_CG) THEN (
LCDCGINIT:
	MOV   DPTR,#LCDCGTAB
	MOV   R2,#8*2
LCDCGSET:
	MOV   A,#LCD_CGA
	CALL  LCDWCOM
LCDCGS1:CALL  LCDNBUS
	MOVX  A,@DPTR
	CALL  LCDWRCG1
	INC   DPTR
	DJNZ  R2,LCDCGS1
	MOV   A,#LCD_HOM
	CALL  LCDWCOM
	RET

LCDCGTAB:
	DB    000H	; Lambda
	DB    01FH
	DB    001H
	DB    002H
	DB    004H
	DB    00AH
	DB    011H
	DB    000H
	DB    000H	; Tau
	DB    000H
	DB    01FH
	DB    014H
	DB    004H
	DB    004H
	DB    006H
	DB    000H

)FI

; *******************************************************************
; Konfigurace komunikaci RS232 a uLAN

RSEG	DS____X

COM_ADR:DS    2		; Adresa jednotky
COM_SPD:DS    2		; Rychlost komunikace
COM_GRP:DS    2		; Skupina pristroje

RSEG	DS____C

BAUDDIV_9600 EQU  52	; new baudgen; 52 pro 9600 a 16 MHz
			; new baudgen; 36 pro 19200 a 11.0592 MHz

COM_DIVT:
	DB    BAUDDIV_9600*4	;  2400
	DB    BAUDDIV_9600*2	;  4800
	DB    BAUDDIV_9600	;  9600
	DB    BAUDDIV_9600/2	; 19200
	DB    0

; Vraci v ACC divisor pro danou rychlost
COM_DIV:MOV   DPTR,#COM_SPD
	MOVX  A,@DPTR
	MOV   DPTR,#COM_DIVT
	MOVC  A,@A+DPTR
	RET

; Nastavi promennou a restartuje komunikaci
UW_COMi:CALL  UW_Mi
	JMP   I_U_LAN

COM_WR23:
	MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	JMP   UB_A_WR

; Nastaveni komunikace uLan
I_U_LAN:CLR   ES
	CLR   A
	MOV   DPTR,#uL_SBPO
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
%IF (0) THEN (
;	CLR   A
;	MOV   A,#10	; 9600 pro krystal 18432 kHz
	MOV   A,#3	; 19200 pro krystal 110592
;	MOV   A,#5	; 19200 pro krystal 18432 kHz
	MOV   R0,#1
	CALL  uL_FNC	; Rychlost
)FI
	MOV   DPTR,#COM_ADR
	MOVX  A,@DPTR
	MOV   R0,#2
	CALL  uL_FNC	; Adresa
	MOV   R2,#0
	MOV   R0,#3
	CALL  uL_FNC	; Delka IB OB
	MOV   R2,#0
	MOV   R0,#4
	CALL  uL_FNC	; Rychle bloky
%IF (1) THEN (
	SETB  BD	; new baudgen; -26 pro 19200 a 16 MHz
	ORL   PCON,#80H	; new baudgen; -18 pro 19200 a 11.0592 MHz
	CALL  COM_DIV
	CPL   A
	INC   A
	MOV   S0RELL,A
	MOV   S0RELH,#0FFH
	MOV   R0,#6
	CALL  uL_FNC	; Start s uzivatelskym baud
)ELSE(
	MOV   R0,#0
	CALL  uL_FNC	; Start
)FI
%IF(%WITH_UL_DY) THEN (
	%LDR45i(0)
	MOV   R6,#0
	CALL  UD_INIT
)FI
	MOV   DPTR,#COM_GRP
	MOVX  A,@DPTR
	MOV   R0,#5
	CALL  uL_FNC	; Nastavit uL_GRP
	MOV   A,#1
	RET

%IF(%WITH_UL_DY_EEP) THEN (
	DB    -'U',-'L',-'D',-'Y'
	%W   (0)
	%W   (SER_NUM)
	%W   (WR_SERNUM)
INI_SERNUM:
	MOV   R2,#010H	; pocet prenasenych byte
	MOV   R4,#070H	; pocatecni adresa v EEPROM
	MOV   DPTR,#SER_NUM
	CALL  EE_RD
	JZ    INI_SERNUM9
INI_SERNUM7:
	MOV   R2,#8
	MOV   DPTR,#SER_NUM
	MOV   A,#0FFH
INI_SERNUM8:
	MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R2,INI_SERNUM8
INI_SERNUM9:
	RET
WR_SERNUM:
	CLR   EA
	MOV   PSW,#0
	MOV   SP,#80H
	CALL  I_TIMRI
	CALL  I_TIMRI
	CALL  WR_SERNUM1
	JMP   $
WR_SERNUM1:
	MOV   R2,#010H	; pocet prenasenych byte
	MOV   R4,#070H	; pocatecni adresa v EEPROM
	MOV   DPTR,#SER_NUM
	CALL  EE_WR
	RET
)FI

; *******************************************************************
;
; Casove preruseni

PUBLIC  KBDTIMR

RSEG	DS____D

;DTINT	EQU   2048 ; Prevod XTAL/12 na 450 Hz pro X 11.0592 MHz
DTINT	EQU   2807 ; Prevod XTAL/12 na 475 Hz pro X 16.0000 MHz
DINT25  EQU   19   ; 18 ; Delitel na 25 Hz
CINT25: DS    1

RSEG	DS____X

BEEPTIM:DS    1	   ; Timer delky pipani

N_OF_T  EQU   6    ; Pocet timeru
TIMR1:  DS    1    ; Timery jsou decrementovany
TIMR2:  DS    1    ; s frekvenci 25 Hz
TIMR_WAIT:
TIMR3:  DS    1
REF_TIM:DS    1	   ; Refresh timr
KBDTIMR:DS    1
TIMRI:  DS    1
TIME:   DS    2    ; Cas v 0.01 min              =====

DELAY_WDG EQU 100  ; Cas V 0.01 min po kterych dojde k vypnti
TIMR_WDG:DS   2	   ; Watchdog kontrolujici cinnost PC
EV_WDGT:DS    1	   ; Typ global event vyslany watchdogem

RSEG	DS____C

USING   3
I_TIME:	PUSH  ACC		; Cast s pruchodem 475 Hz
	PUSH  PSW
	MOV   PSW,#AR0
	PUSH  B
	PUSH  DPL
	PUSH  DPH

	CLR   TF0
	CLR   C
	CLR   TR0
S_T1_D  SET   7               ; zpozdeni rutiny
	MOV   A,TL0           ; 7 Cyklu
	SUBB  A,#LOW  (DTINT-S_T1_D)
	MOV   TL0,A
	MOV   A,TH0
	SUBB  A,#HIGH (DTINT-S_T1_D)
	MOV   TH0,A
	SETB  TR0
	CALL  ADC_D		; Cteni AD prevodniku

	DJNZ  CINT25,I_TIMR1	; Konec casti spruchodem 475 Hz
	MOV   CINT25,#DINT25	; Pruchod s frekvenci 25 Hz

	CALL  ADC_S		; Spusteni cyklu prevodu ADC
	MOV   DPTR,#BEEPTIM
	MOVX  A,@DPTR
	JZ    I_TIM80
	DEC   A
	MOVX  @DPTR,A
	JNZ   I_TIM80
	CLR   %BEEP_FL
%IF(1) THEN (
	MOV   DPTR,#LED       ; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
)FI
I_TIM80:%WATCHDOG

	CALL  uL_STR		; Vybuzeni uLan komunikace

	MOV   DPTR,#TIMR1
	MOV   B,#N_OF_T-1
I_TIME2:MOVX  A,@DPTR
	JZ    I_TIME3
	DEC   A
	MOVX  @DPTR,A
I_TIME3:INC   DPTR
	DJNZ  B,I_TIME2
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	JNB   ITIM_RF,I_TIMR1
	JB    ACC.7,I_TIME4
I_TIMR1:POP   DPH
	POP   DPL
	POP   B
	POP   PSW
	POP   ACC
	SETB  EA
I_TIMRI:RETI

I_TIME4:CLR   ITIM_RF	      ; Pruchod 0.6 sec
;	CALL  I_TIMRI
;	MOV   PSW,#AR0		; Banka 2
	ADD   A,#15
	MOVX  @DPTR,A

	MOV   A,LEB_FLG
	JBC   LEB_PHA,I_TIM42
	ORL   LED_FLG,A
	SETB  LEB_PHA
	SJMP  I_TIM43
I_TIM42:CPL   A
	ANL   LED_FLG,A
I_TIM43:SETB  ITIM_RF

	MOV   DPTR,#TIMR_WDG	; Watchdog nadrizeneho systemu
	MOVX  A,@DPTR
	ADD   A,#-1
	MOV   R1,A
	MOV   A,#1
	MOVC  A,@A+DPTR
	ADDC  A,#-1
	JNC   I_TIM45
	XCH   A,R1
	MOVX  @DPTR,A
	INC   DPTR
	XCH   A,R1
	MOVX  @DPTR,A
I_TIM45:

	CALL  LS_INT		; Rizeni zhaveni

	CALL  AUX_ICH		; Zmena na vstupech AUX
	JZ    I_TIM48
	;SETB  GS_BUF.BGS_AUX	; Doslo ke zmene
	MOV   R5,A
	MOV   DPTR,#ZR_MASK	; Vyhodnoceni nulovani
	MOVX  A,@DPTR
	ANL   A,R5
	ANL   A,R4
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	ANL   A,R5
	ORL   A,R4
	XRL   A,R4	
	ORL   A,R2
	JZ    I_TIM47
	CALL  ZER_ABS
I_TIM47:MOV   DPTR,#LM_MASK	; Vyhodnoceni vyslani marky
	MOVX  A,@DPTR
	ANL   A,R5
	ANL   A,R4
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	ANL   A,R5
	ORL   A,R4
	XRL   A,R4	
	ORL   A,R2
	JZ    I_TIM48
	CALL  LAN_MRK		; Vyslani znacky
I_TIM48:

	JMP   I_TIMR1

; *******************************************************************
; Download do obvodu XILINX XC3164A

XC_BASE	XDATA 0FFC0H	; bazova adresa portu obvodu
XC_PROG	XDATA 0FFC2H	; konfiguracni port

XC_DP	BIT   P3.4	; L .. programovani
XC_RDY	BIT   P3.3	; H .. ready for data


RSEG	DS____C

; Nacte data z adresy [DPTR] do obvodu XILINX

XC_LOAD:CLR   F0
	SETB  XC_DP
	CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   R0,A
XC_LD12:DJNZ  R0,XC_LD12
	CLR   XC_DP		; inicializace downloadu
XC_LD14:DJNZ  R0,XC_LD14
	SETB  XC_DP
XC_LD16:DJNZ  R0,XC_LD16
XC_LD20:JB    XC_DP,XC_LD30	; konec programu
	MOV   R0,#20
XC_LD24:DJNZ  R0,XC_LD26
	SETB  F0
	RET
XC_LD26:JNB   XC_RDY,XC_LD24	; cekani na ready
	MOVX  A,@DPTR
	INC   DPTR
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#XC_PROG
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
	INC   R4
	CJNE  R4,#0,XC_LD20
	INC   R5
	CJNE  R5,#0,XC_LD20
	SETB  F0
XC_LD30:RET


XC_LDT: MOV   DPTR,#XC_LDTt
	CALL  cPRINT
	MOV   R6,#0CH
	MOV   R7,#40H
	CALL  INPUThw
	JB    F0,XC_LDT9
	MOV   DPL,R4
	MOV   DPH,R5
	CALL  XC_LOAD
	MOV   A,#LCD_HOM+4CH
	CALL  LCDWCOM
	CALL  PRINThw
	CALL  INPUTc
XC_LDT9:RET
XC_LDTt:DB    LCD_CLR,'ld XC from'
	DB    C_LIN2 ,'status',0

; *******************************************************************
; ADC prevodnik z obvodu XILINX XC3164A

XAD_IN1	BIT   P1.0	; 0 .. pripojuje vstup 1
XAD_IN2	BIT   P1.1	; 0 .. pripojuje vstup 2

; XAD_R1_2    P5.2	; rizeni referenci
%DEFINE	(XAD_R1) (ORL P5,#04H)		; prime mereni
%DEFINE	(XAD_R2) (ANL P5,#NOT 04H)	; mereni proti referencni

XAD_EI	BIT   INT0	; P3.2 = 0 .. hlasi intr od XAD
XAD_IEN	BIT   EX0	; povoleni preruseni
XAD_IFL	BIT   IE0	; priznak preruseni

; Write
XAD_PP	XDATA XC_BASE+0	; 2B ; Period preset
XAD_ORP	XDATA XC_BASE+2	; 1B ; Output rate preset

; Read
XAD_RC	XDATA XC_BASE+0	; 3B ; Output reference quantums
XAD_PC	XDATA XC_BASE+3	; 2B ; Corection quantums

RSEG	DS____C

XAD_INI:

;	%LDR45i (3200)	; PP pro XTAL 32MHz a 10kHz beh prevodu
;	MOV   R6,#200	; ORP pro 50hz

	%LDR45i (6400)	; PP pro XTAL 32MHz a 5kHz beh prevodu
	MOV   R6,#200	; ORP pro 25hz

XAD_INI1:MOV  DPTR,#XAD_PP
	CLR   C
	MOV   A,#1
	SUBB  A,R4
	MOVX  @DPTR,A	; 1-R45
	INC   DPTR
	CLR   A
	SUBB  A,R5
	MOVX  @DPTR,A
	MOV   DPTR,#XAD_ORP
	MOV   A,R6
	CPL   A
	INC   A
	MOVX  @DPTR,A
	MOV   DPTR,#FI_CORC
	MOV   B,R6
	MOV   A,R4
	MUL   AB
	MOVX  @DPTR,A		; FI_CORC := R45*R6
	INC   DPTR
	MOV   R7,B
	MOV   B,R6
	MOV   A,R5
	MUL   AB
	ADD   A,R7
	MOVX  @DPTR,A
	INC   DPTR
	CLR   A
	ADDC  A,B
	MOVX  @DPTR,A
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A
	ORL   FL_ABS,#ZER_MSK
	RET

XAD_TST_T:DB  LCD_CLR,'RC'
	DB    C_LIN2 ,'PC',0

XAD_TST_T1:DB LCD_CLR,'ABS'
	DB    C_LIN2 ,'',0

XAD_TST:CLR   XAD_IEN
	MOV   DPTR,#06000H
	CALL  XC_LOAD
	JNB   F0,XAD_T05
	CALL  ERRBEEP
XAD_T05:

XAD_TST_NL:
	SETB  XAD_IN2		; Vyber kanalu 1
	CLR   XAD_IN1
	%XAD_R1

	CALL  XAD_INI

	MOV   DPTR,#XAD_TST_T
	CALL  cPRINT

	CLR   A
	MOV   DPTR,#TMP
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A

XAD_T20:JB    XAD_IEN,XAD_T40
	JB    XAD_EI,XAD_T39
	MOV   DPTR,#XAD_RC
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A

	MOV   A,#LCD_HOM+03H
	CALL  LCDWCOM

	MOV   A,R7
	CALL  PRINThb
	MOV   A,R6
	CALL  PRINThb
	MOV   A,R5
	CALL  PRINThb

	MOV   DPTR,#XAD_PC
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A

	MOV   A,#LCD_HOM+43H
	CALL  LCDWCOM
	CALL  PRINThw

	XRL   LED_FLG,#1
	CALL  LEDWR

	MOV   DPTR,#TMP
	MOVX  A,@DPTR
	ADD   A,#1
	MOVX  @DPTR,A
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#0
	MOVX  @DPTR,A
	MOV   R5,A

	MOV   A,#LCD_HOM+48H
	CALL  LCDWCOM
	CALL  PRINThw
XAD_T39:JMP   XAD_T80

XAD_T40:MOV   A,#LCD_HOM+40H
	CALL  LCDWCOM
	MOV   DPTR,#XAD_RAW+4
	MOVX  A,@DPTR
	CPL   A
	ADD   A,#084H+1
	MOV   R3,A
	ANL   A,#3
	XCH   A,R3
	RR    A
	RR    A
	ANL   A,#3FH
	JZ    XAD_T44
	MOV   R0,A
	ADD   A,#-16
	JC    XAD_T49
XAD_T43:MOV   A,#' '
	CALL  LCDWR
	DJNZ  R0,XAD_T43
XAD_T44:MOV   DPTR,#XAD_RAW
	CALL  xLDl
	MOV   A,R3
	CALL  SHRl
	CALL  PRINThl
	MOV   A,#' '
	CALL  LCDWR
	MOV   A,#LCD_HOM+04H
	CALL  LCDWCOM
	CALL  G_ABSl
	MOV   R3,#0C6H
	MOV   R2,#9
	CALL  PRINTli
XAD_T49:

XAD_T80:CALL  SCANKEY
	JZ    XAD_T99V
	CJNE  A,#K_DP,XAD_T81
	RET
XAD_T99V:JMP  XAD_T99
XAD_T81:CJNE  A,#K_1,XAD_T82
	SETB  XAD_IN2		; Vyber kanalu 1
	CLR   XAD_IN1
	%XAD_R1
	CLR   FL_RAW
	JMP   XAD_T20
XAD_T82:CJNE  A,#K_2,XAD_T83
	SETB  XAD_IN1		; Vyber kanalu 2
	CLR   XAD_IN2
	%XAD_R1
	CLR   FL_RAW
	JMP   XAD_T20
XAD_T83:CJNE  A,#K_3,XAD_T84
	SETB  XAD_IN2		; Vyber kanalu 1 proti 2
	CLR   XAD_IN1
	%XAD_R2
	CLR   FL_RAW
	JMP   XAD_T20
XAD_T84:CJNE  A,#K_0,XAD_T85
	SETB  XAD_IN1		; Odpojeni vstupu
	SETB  XAD_IN2
	%XAD_R1
	JMP   XAD_T20
XAD_T85:CJNE  A,#K_RUN,XAD_T86
	MOV   DPTR,#ABS_FI
	MOV   A,#64
	MOVX  @DPTR,A
	ORL   FL_ABS,#ZER_MSK
	SETB  XAD_IEN
	MOVX  @DPTR,A
	MOV   DPTR,#XAD_TST_T1
	CALL  cPRINT
	JMP   XAD_T20
XAD_T86:CJNE  A,#K_4,XAD_T87
	SETB  XAD_IN2		; Vyber kanalu 1
	CLR   XAD_IN1
	SETB  FL_RAW
	%XAD_R1
	JMP   XAD_T20
XAD_T87:CJNE  A,#K_5,XAD_T88
	SETB  XAD_IN1		; Vyber kanalu 2
	CLR   XAD_IN2
	SETB  FL_RAW
	%XAD_R1
	JMP   XAD_T20
XAD_T88:CJNE  A,#K_6,XAD_T89
	SETB  XAD_IN2		; Vyber kanalu 1 proti 2
	CLR   XAD_IN1
	SETB  FL_RAW
	%XAD_R2
	JMP   XAD_T20
XAD_T89:CJNE  A,#K_7,XAD_T90
	MOV   DPTR,#ABS_FI
	MOV   A,#1
	MOVX  @DPTR,A
	SETB  FL_FICH
	JMP   XAD_T20
XAD_T90:CJNE  A,#K_8,XAD_T91
	MOV   DPTR,#ABS_FI
	MOV   A,#64
	MOVX  @DPTR,A
	SETB  FL_FICH
	JMP   XAD_T20
XAD_T91:

XAD_T99:JMP   XAD_T20

; Meni R0 a A
PRINThl:MOV   A,R4
	XCH   A,R6
	MOV   R4,A
	MOV   A,R5
	XCH   A,R7
	MOV   R5,A
	CALL  PRINThw
	MOV   A,R4
	XCH   A,R6
	MOV   R4,A
	MOV   A,R5
	XCH   A,R7
	MOV   R5,A
	JMP   PRINThw

; *******************************************************************
; Pomocna aritmetika

; Posun nebo normovani [R0] pomoci MDU s prikazem v ACC
; volani: R0  .. ukazatel na data
; 	  ACC .. prikaz do ARCON (ACC.5 = 1 .. right, 0 .. left)
; vraci:  ACC .. vystup ARCON
MDU_SH:	MOV   MD0,@R0
	INC   R0
	MOV   MD1,@R0
	INC   R0
	MOV   MD2,@R0
	INC   R0
	MOV   MD3,@R0
	MOV   ARCON,A
	INC   R0	; musi byt alespon 6 cyklu
	NOP
	DEC   R0
	DEC   R0
	DEC   R0
	DEC   R0
	MOV   A,ARCON
	MOV   @R0,MD0
	INC   R0
	MOV   @R0,MD1
	INC   R0
	MOV   @R0,MD2
	INC   R0
	MOV   @R0,MD3
	INC   R0
	RET

; Poloplovouci deleni R4567 := ACUM/R4567, R3 vraci exponent + 80H
MDU_Dhf:MOV   R0,#AKUM
	CLR   A		; Normalize ACUM to left
	CALL  MDU_SH
;	XRL   A,#40H
;	JZ		; deleni nuloveho delence
	ANL   A,#1FH
	MOV   R3,A
	MOV   A,PSW
	ANL   A,#18H
	XRL   A,#04H
	MOV   R0,A
	CLR   A		; Normalize R4567 to left
	CALL  MDU_SH
;	XRL   A,#40H
;	JZ		; deleni nulou
	ANL   A,#1FH
	ADD   A,#80H
	SUBB  A,R3
	MOV   R3,A	; exponent ve float formatu
	MOV   A,R7
	CJNE  A,AKUM+3,MDU_Dh0
	MOV   A,R6
	CJNE  A,AKUM+2,MDU_Dh0
	MOV   A,R5
	CJNE  A,AKUM+1,MDU_Dh0
	MOV   A,R4
	CJNE  A,AKUM+0,MDU_Dh0
	CLR   A		; AKUM == R4567
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R7,#080H
	RET
MDU_Dh0:JNC   MDU_Dh1	; AKUM < R4567 ?
	CLR   C
	MOV   A,AKUM+3
	RRC   A
	MOV   AKUM+3,A
	MOV   A,AKUM+2
	RRC   A
	MOV   AKUM+2,A
	MOV   A,AKUM+1
	RRC   A
	MOV   AKUM+1,A
	MOV   A,AKUM+0
	RRC   A
	MOV   AKUM+0,A	; AKUM =>> 1
	INC   R3
MDU_Dh1:MOV   MD0,AKUM	; 0.q = nn / d0
	MOV   MD1,AKUM+1
	MOV   MD2,AKUM+2
	MOV   MD3,AKUM+3
	MOV   MD4,R6
	MOV   MD5,R7
	NOP		; 6 cyklu
	NOP
	NOP
	NOP
	NOP
	NOP
	MOV   R0,MD0
	MOV   R1,MD1
	MOV   A,MD2
	MOV   R2,MD5	; dummy
	JZ    MDU_Dh2
	MOV   R0,#0FFH
	MOV   R1,#0FFH
MDU_Dh2:MOV   MD0,R6	; nn.0 - d0 * 0.q
	MOV   MD4,R0
	MOV   MD1,R7
	MOV   MD5,R1
	NOP		; 4 cykly
	MOV   R2,AKUM+2
	CLR   C
	MOV   A,AKUM
	SUBB  A,MD0
	MOV   AKUM+2,A
	MOV   A,AKUM+1
	SUBB  A,MD1
	MOV   AKUM+3,A
	MOV   A,R2
	SUBB  A,MD2
	MOV   R2,A
	MOV   A,MD3	; dummy
	MOV   MD0,R4	; nn.0 - dd * 0.q
	MOV   MD4,R0
	MOV   MD1,R5
	MOV   MD5,R1
	NOP		; 4 cykly
	NOP
	CLR   C
	CLR   A
	SUBB  A,MD0
	MOV   AKUM,A
	CLR   A
	SUBB  A,MD1
	MOV   AKUM+1,A
	MOV   A,AKUM+2
	SUBB  A,MD2
	MOV   AKUM+2,A
	MOV   A,AKUM+3
	SUBB  A,MD3
	MOV   AKUM+3,A
	MOV   A,R2
	SUBB  A,#0
	JNB   ACC.7,MDU_Dh5
MDU_Dh3:MOV   R2,A	; korekce pro rozsireni d0 na dd
	MOV   A,R0
	DEC   R0
	JNZ   MDU_Dh4
	DEC   R1
MDU_Dh4:MOV   A,AKUM
	ADD   A,R4
	MOV   AKUM,A
	MOV   A,AKUM+1
	ADDC  A,R5
	MOV   AKUM+1,A
	MOV   A,AKUM+2
	ADDC  A,R6
	MOV   AKUM+2,A
	MOV   A,AKUM+3
	ADDC  A,R7
	MOV   AKUM+3,A
	CLR   A
	ADDC  A,R2	; !!!!!!!!!!!!!!!!!!!!!
	JNC   MDU_Dh3
MDU_Dh5:MOV   MD0,AKUM	; 0.0q = nn-0.q*dd / d0
	MOV   MD1,AKUM+1
	MOV   MD2,AKUM+2
	MOV   MD3,AKUM+3
	MOV   MD4,R6
	MOV   MD5,R7
	NOP		; 6 cyklu
	NOP
	NOP
	NOP
	NOP
	NOP
	MOV   R4,MD0
	MOV   R5,MD1
	MOV   A,MD2
	MOV   R2,MD5	; dummy
	JZ    MDU_Dh6
	MOV   R4,#0FFH
	MOV   R5,#0FFH
MDU_Dh6:MOV   A,R0
	MOV   R6,A
	MOV   A,R1
	MOV   R7,A
	RET

%IF (1) THEN (

RSEG	DS____X
MDU_DHFTX:DS  1+4+4+5


RSEG	DS____C

MDU_DHFT:
	MOV   DPTR,#MDU_DHFTX
	MOVX  A,@DPTR
	JZ    MDU_DHFT
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR	; AKUM
	MOV   AKUM,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   AKUM+1,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   AKUM+2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   AKUM+3,A
	INC   DPTR
	MOVX  A,@DPTR	; R4567
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
	INC   DPTR

	CALL  MDU_Dhf

	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	JMP   MDU_DHFT

) FI

; *******************************************************************
; Plovouci filtr

RSEG	DS____X

FI_MLEN	EQU   64	; Maximalni delka plovouciho filtru

FI1_DAT:
FI1_LEN:DS    1		; Aktualni delka filtru
FI1_POS:DS    1		; Aktualni pozice
FI1_TRD:DS    1		; Pocet prevodu za ktere bude filtr redy
FI1_SUM:DS    4		; Suma prvniho filtru
FI1_BUF:DS    3*FI_MLEN	; Buffer prvniho filtru

FI2_DAT:
FI2_LEN:DS    1		; Druhy filtr
FI2_POS:DS    1		; Aktualni pozice
FI2_TRD:DS    1
FI2_SUM:DS    4
FI2_BUF:DS    3*FI_MLEN

FI_CORC:DS    4		; Konstanta nacitana pri plneni korekcniho
			; filtru

RSEG	DS____C

; Nastvi delku filtru [DPTR] na hodnotu v ACC

FI_SETL:MOV   R1,A
	ADD   A,#-FI_MLEN
	JNC   FI_SL10
	MOV   R1,#FI_MLEN
FI_SL10:MOV   A,R1
	MOVX  @DPTR,A		; FI_LEN
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A		; FI_POS
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A		; FI_TRD
	INC   DPTR
	MOV   R0,#4		; FI_SUM
	CLR   A
FI_SL20:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,FI_SL20
	RET

; Provede filtraci pro filtr [DPTR], vstup R456 NEW, vystup R4567
; pokud nastaveno F0 nedoslo jeste k naplneni filtru

FI_DO:	MOVX  A,@DPTR	; FI_LEN
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR	; FI_POS
	INC   A
	MOV   R1,A
	XRL   A,R0
	JNZ   FI_DO10
	MOV   R1,#0
FI_DO10:MOV   A,R1
	MOVX  @DPTR,A
	RL    A		; FI_POS*3
	ADD   A,R1
	ADD   A,#4
	MOV   R1,A
	INC   DPTR
	MOVX  A,@DPTR	; FI_TRD
	JZ    FI_DO15
	DEC   A
	MOVX  @DPTR,A
	SETB  F0
	INC   DPTR
	MOV   A,DPL
	MOV   R2,A
	ADD   A,R1
	MOV   DPL,A
	MOV   A,DPH
	MOV   R3,A
	ADDC  A,#0
	MOV   DPH,A
	MOV   A,R4	; Ulozit NEW
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	MOV   R7,#0     ; R4567 = NEW
	SJMP  FI_DO20
FI_DO15:CLR   F0
	INC   DPTR
	MOV   A,DPL
	MOV   R2,A
	ADD   A,R1
	MOV   DPL,A
	MOV   A,DPH
	MOV   R3,A
	ADDC  A,#0
	MOV   DPH,A
	CLR   C		; Ulozit NEW
	MOVX  A,@DPTR	; R4567 = NEW - OLD
	XCH   A,R4
	MOVX  @DPTR,A
	SUBB  A,R4
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R5
	MOVX  @DPTR,A
	SUBB  A,R5
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R6
	MOVX  @DPTR,A
	SUBB  A,R6
	MOV   R6,A
	CLR   A
	SUBB  A,#0
	MOV   R7,A
FI_DO20:MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR	; FI_SUM = FI_SUM + NEW - OLD
	ADD   A,R4	; R4567 = FI_SUM
	MOV   R4,A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOV   R5,A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R6
	MOV   R6,A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R7
	MOV   R7,A
	MOVX  @DPTR,A
	RET

; Vypocet korekce s filtracni historii [DPTR],
; vstup R456 NEW, vystup R4567
; pri naplnovani vyuziva FI_CORC
; vysledna funkce R4567=FI_LEN*FI_CORC+NEW-OLD

FI_COR:	MOVX  A,@DPTR	; FI_LEN
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR	; FI_POS
	INC   A
	MOV   R1,A
	XRL   A,R0
	JNZ   FI_COR1
	MOV   R1,#0
FI_COR1:MOV   A,R1
	MOVX  @DPTR,A
	RL    A		; FI_POS*3
	ADD   A,R1
	ADD   A,#4
	MOV   R1,A
	INC   DPTR
	MOVX  A,@DPTR	; FI_TRD
	JZ    FI_COR3
	DEC   A
	MOVX  @DPTR,A
	SETB  F0
	INC   DPTR
	MOV   A,DPL
	MOV   R2,A
	ADD   A,R1
	MOV   DPL,A
	MOV   A,DPH
	MOV   R3,A
	ADDC  A,#0
	MOV   DPH,A
	MOV   A,R4	; Ulozit NEW
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	MOV   DPTR,#FI_CORC
	MOVX  A,@DPTR	; R4567 = FI_CORC
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
	SJMP  FI_DO20
FI_COR3:CLR   F0
	INC   DPTR
	MOV   A,DPL
	MOV   R2,A
	ADD   A,R1
	MOV   DPL,A
	MOV   A,DPH
	MOV   R3,A
	ADDC  A,#0
	MOV   DPH,A
	CLR   C		; Ulozit NEW
	MOVX  A,@DPTR	; R4567 = NEW - OLD
	XCH   A,R4
	MOVX  @DPTR,A
	SUBB  A,R4
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R5
	MOVX  @DPTR,A
	SUBB  A,R5
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R6
	MOVX  @DPTR,A
	SUBB  A,R6
	MOV   R6,A
	CLR   A
	SUBB  A,#0
	MOV   R7,A
FI_COR4:MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR	; R4567 = FI_SUM + NEW - OLD
	ADD   A,R4
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R6
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R7
	MOV   R7,A
	RET

; *******************************************************************
; Zpracovani dat z prevodniku
;
; pocet impulsu po filtraci	FI_LEN*period
;
; hodnota na prevodniku		RC(n)/(FI_LEN*period-PC(n-1)+PC(n))
;

RSEG	DS____B

FL_ABS:	DS    1
FL_ZER	BIT   FL_ABS.7	; Vynuluj absorbanci
FL_FICH	BIT   FL_ABS.6	; Zmen rad filtru
FL_RAW	BIT   FL_ABS.5	; Vystup dat bez logaritmu
ZER_MSK SET   0C0H
RAW_MSK	SET   020H

RSEG	DS____X

ABS_FI:	DS    1		; Hodnota filtru

XAD_RAW:DS    5		; Korigovana a filtrovana hodnota z ADC
XAD_OFS:DS    4		; Offset prevodniku XAD

ABS:	DS    4		; absorbance
ABS_Z:	DS    4		; nula absorbance

XAD_SAV:DS    14	; Pro ulozeni AKUM z knihovny PB_AF

XAD_MOD:DS    1		; Merena velicin 0 ABS, 1 CUV, 2 REF, 3 RAT

RSEG	DS____C

USING   2
I_XAD:  CLR   XAD_IFL
	PUSH  ACC
	PUSH  PSW
	MOV   PSW,#AR0
	PUSH  B
	PUSH  DPL
	PUSH  DPH
	MOV   R0,#AKUM		; Ulozeni stavu PB_AF
	MOV   R2,#14
	MOV   DPTR,#XAD_SAV
	CALL  xiSVs

	JNB   FL_FICH,I_XAD15
	CLR   FL_FICH		; Zmena radu filtru
	MOV   DPTR,#ABS_FI
	MOVX  A,@DPTR
	MOV   R7,A
	MOV   DPTR,#FI1_DAT
	CALL  FI_SETL
	MOV   A,R7
	MOV   DPTR,#FI2_DAT
	CALL  FI_SETL
I_XAD15:

I_XAD20:MOV   DPTR,#XAD_RC	; Nacteni dat z prevodniku
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R6,A
	MOV   DPTR,#FI1_DAT	; Filtrace
	CALL  FI_DO
	JB    F0,I_XAD29
	MOV   AKUM,R4
	MOV   AKUM+1,R5
	MOV   AKUM+2,R6
	MOV   AKUM+3,R7
I_XAD29:

	MOV   DPTR,#XAD_PC	; Nacteni korekce z prevodniku
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   R6,#0FFH
	MOV   DPTR,#FI2_DAT	; Filtrace
	CALL  FI_COR
	JB    F0,I_XAD39

	CALL  MDU_Dhf		; Pomer kladnych kvant ku periode
	MOV   DPTR,#XAD_RAW	; vysledek R4567 a exponent R3
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   C,ACC.7		; Zaokrouhleni vysledku na float
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	ADDC  A,#0
	MOV   R4,A
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	ADDC  A,#0
	MOV   R5,A
	MOV   A,R7
	MOVX  @DPTR,A
	INC   DPTR
	ADDC  A,#0
	MOV   R6,A
	MOV   A,R3
	JZ    I_XAD31
	INC   A			; Vynasobeni vysledku 2x
I_XAD31:MOVX  @DPTR,A
	MOV   R7,A
	JNC   I_XAD33
	MOV   A,R6		; preteceni pri zaokrouhleni
	RRC   A
	MOV   R6,A
	MOV   A,R5
	RRC   A
	MOV   R5,A
	MOV   A,R4
	RRC   A
	MOV   R4,A
	INC   R7
I_XAD33:MOV   A,R6
	ANL   A,#07FH
	MOV   R6,A		; R4567 float z prevodniku
	MOV   DPTR,#XAD_OFS
	MOV   R0,#AKUM
	CALL  xiLDl		; Odecteni nuly
	CALL  ADDf		; Odecteni offsetu ADC
	JB    FL_RAW,I_XAD37
    %IF(1)THEN(			; Limitace vystupu pro
	MOV   A,R6		; zaporny ci nulovy pomer
	JB    ACC.7,I_XAD34
	MOV   A,R7
	ADD   A,#-072H		;  3.05E-5
	JC    I_XAD35
I_XAD34:CLR   A			; Limitace pomeru
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R7,#072H
    )FI
I_XAD35:CALL  LOGfRf		; Logaritmus
	JBC   FL_ZER,I_XAD38
	MOV   DPTR,#ABS_Z
	MOV   R0,#AKUM
	CALL  xiLDl		; Odecteni nuly
	CALL  SUBf
I_XAD37:MOV   DPTR,#ABS
	CALL  xSVl
    %IF(1)THEN(			; Vystup na DA prevodnik
	CALL  DAC_ABS
    )FI
	SJMP  I_XAD39
I_XAD38:MOV   DPTR,#ABS_Z	; Vynulovani absorbance
	CALL  xSVl
I_XAD39:

I_XAD80:MOV   DPTR,#TMP		; !!!!!!!!!!!!!!!!!!!!!
	MOVX  A,@DPTR
	ADD   A,#1
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#0
	MOVX  @DPTR,A

	CALL  LAN_TM		; !!!!!!!!!!!!!!!!!!!!!

I_XADR:	MOV   R0,#AKUM
	MOV   R2,#14
	MOV   DPTR,#XAD_SAV
	CALL  xiLDs
	POP   DPH
	POP   DPL
	POP   B
	POP   PSW
	POP   ACC
	RETI

; Snulovani offsetu prevodniku XAD_OFS
ZER_XADO:
	MOV   DPTR,#XAD_RAW+1
	MOV   C,XAD_IEN
	CLR   XAD_IEN
	CALL  xLDl
	MOV   XAD_IEN,C
	MOV   A,R6
	ORL   A,#080H
	MOV   R6,A
	MOV   DPTR,#XAD_OFS
	MOV   C,XAD_IEN
	CLR   XAD_IEN
	CALL  xSVl
	MOV   XAD_IEN,C
	RET

; Nacte offset prevoniku
G_XADOl:MOV   DPTR,#XAD_OFS
	SJMP  G_ABS10

; Nacte do R4567 absorbanici *10e6
G_ABSl:	MOV   DPTR,#ABS
G_ABS10:MOV   R4,#000H	; 1e6 = 00 24 74 94
	MOV   R5,#024H	; 1e5 = 00 50 43 91
	MOV   R6,#074H	; 1e4 = 00 40 1C 8E
	MOV   R7,#094H	; 1e3 = 00 00 7A 8A
	MOV   R0,#AKUM
	MOV   C,XAD_IEN
	CLR   XAD_IEN
	CALL  xiLDl
	MOV   XAD_IEN,C
	CALL  MULf
	MOV   A,R6
	MOV   R2,A
	ORL   A,#80H
	MOV   R6,A
	MOV   A,R7
	JB    ACC.7,G_ABS30
	MOV   R4,#0
	MOV   R5,#0
	MOV   R6,#0
	MOV   R7,#0
	CLR   F0
	MOV   A,#1
	RET
G_ABS30:ADD   A,#-81H-31
	JNC   G_ABS40
	MOV   R4,#0FFH
	MOV   R5,#0FFH
	MOV   R6,#0FFH
	MOV   R7,#07FH
	SJMP  G_ABS80
G_ABS40:MOV   R7,#0
	CPL   A
	ADD   A,#-8+1
	JC    G_ABS50
	ADD   A,#8
	XCH   A,R4
	XCH   A,R5
	XCH   A,R6
	XCH   A,R7
	XCH   A,R4
G_ABS50:CALL  SHRl
G_ABS80:MOV   A,R2
	JNB   ACC.7,G_ABS90
	CALL  NEGl
G_ABS90:CLR   F0
	MOV   A,#1
	RET

; Nulovani absorbance
ZER_ABS:SETB  FL_ZER
	RET

; Cte casovou konstantu do R45
G_TIMCO:MOV   DPTR,#ABS_FI
	MOVX  A,@DPTR
	CLR   C
	RLC   A
	MOV   R4,A
	CLR   A
	RLC   A
	XCH   A,R4
	CLR   C
	RLC   A
	XCH   A,R4
	RLC   A
	MOV   R5,A
	MOV   A,#1
	RET

; Zmeni casovou konstantu podle R45
S_TIMCO:MOV   A,R5
	RRC   A
	MOV   A,R4
	RRC   A
	CLR   C
	RRC   A
	ADDC  A,#0
	JNZ   S_TIMC1
	INC   A
S_TIMC1:CJNE  A,#FI_MLEN,S_TIMC2
S_TIMC2:JC    S_TIMC3
	MOV   A,#FI_MLEN
S_TIMC3:MOV   DPTR,#ABS_FI
	MOVX  @DPTR,A
	SETB  FL_FICH
	RET

XAD_CHMOD:
	MOV   DPTR,#XAD_MOD
	MOVX  A,@DPTR
	INC   A
XAD_SMOD:
	MOV   DPTR,#XAD_MOD
	MOVX  @DPTR,A
	ADD   A,#-5
	JNC   XAD_SM3
	CLR   A
	MOVX  @DPTR,A
XAD_SM3:MOVX  A,@DPTR
	ADD   A,#XAD_SMt-XAD_SM5
	MOVC  A,@A+PC
XAD_SM5:SETB  XAD_IN1
	SETB  XAD_IN2
	RRC   A
	CPL   C
	MOV   XAD_IN1,C		; 0 povoluje IN1
	RRC   A
	CPL   C
	MOV   XAD_IN2,C		; 0 povoluje IN2
	RRC   A
	JC    XAD_SM6
	%XAD_R1			; Prime mereni
	SJMP  XAD_SM7
XAD_SM6:%XAD_R2			; Mereni proti referenci
XAD_SM7:RRC   A
	MOV   FL_RAW,C		; 1 .. Raw data jinak LOG
	SETB  FL_FICH
	RET

	;         DR21
XAD_SMt:DB    00000101B	; ABS = LOG (M/R)
	DB    00001001B	; M
	DB    00001010B	; R
	DB    00001101B	; M/R
	DB    00001000B	; 0

XAD_IAt1:DB   LCD_CLR,'Fatal XC error'
	DB    C_LIN2 ,'call service.',0

XAD_INIABS:
	CLR   XAD_IEN		; Konfigurace XC3164A
	MOV   DPTR,#06000H
	CALL  XC_LOAD
	JNB   F0,XAD_IA4
	CALL  ERRBEEP		; Hardawarova chyba v XC3164A
	MOV   DPTR,#XAD_IAt1
	CALL  cPRINT
XAD_IA3:CALL  SCANKEY
	JZ    XAD_IA3
	RET
XAD_IA4:
	MOV   DPTR,#ABS_FI	; Kontrola aby >=1 a <=FI_MLEN
	MOVX  A,@DPTR
	JZ    XAD_IA6
	ADD   A,#-FI_MLEN
	JNC   XAD_IA7
XAD_IA6:MOV   A,#FI_MLEN
	MOVX  @DPTR,A
XAD_IA7:
	CALL  XAD_INI
	CLR   A
	CALL  XAD_SMOD		; Meri se ABS jako LOG (M/R)
	SETB  FL_ZER
	SETB  XAD_IEN
	RET

; *******************************************************************

RSEG	DS____X

uL_SBPO:DS    2
LAN_TMB:DS    15

LM_MASK:DS    2		; Maska zmen vstupu pro vyslani znacky

RSEG	DS____C

XC_SBPO:MOV   DPTR,#uL_SBPO
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R1,A
	MOV   DPTR,#uL_SBP
	MOVX  A,@DPTR
	XCH   A,R0
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R1
	MOVX  @DPTR,A
	MOV   DPTR,#uL_SBPO
	MOV   A,R0
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A
	RET

; Pozatimni vysilani absorbance a znacky
LAN_TM:	MOV   DPTR,#EPRCHK1-5
	MOV   A,#5
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#EPRCHK2-EPRCHK1+5
	MOVC  A,@A+DPTR
	CPL   A
	XRL   A,R4
        JNZ   LAN_TME

	JNB   ES,LAN_TME
	CALL  XC_SBPO
	JMP   LAN_A

LAN_TMR:CALL  XC_SBPO
LAN_TME:RET

LAN_A1:	MOV   DPTR,#uL_GRP
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#4FH
	CLR   F0
	CALL  uL_S_OP
	JNB   F0,LAN_A
	; informovat o chybe
	JMP   LAN_TMR

LAN_A:  MOV   DPTR,#ABS
LAN_A2:	CALL  xLDl

LAN_A3:	MOV   A,R6
	RLC   A
	MOV   A,R7
	JZ    LAN_A4
	RRC   A
	DEC   A
	MOV   R7,A
	MOV   A,R6
	MOV   ACC.7,C
LAN_A4:	MOV   R6,A
	MOV   DPTR,#LAN_TMB
	CALL  xSVl

	MOV   DPTR,#LAN_TMB
	MOV   R4,#4
	MOV   R5,#0
	CLR   F0
	CALL  uL_S_WR
	JB    F0,LAN_A1
	JMP   LAN_TMR


; Vyslani znacky zacatku analyzy
LAN_MRK:JNB   ES,L_MRKR
	;MOV   DPTR,#uL_FORM
	;MOVX  A,@DPTR
	;CJNE  A,#2,L_MRKR

	MOV   DPTR,#uL_SBP
	MOVX  A,@DPTR
	PUSH  ACC
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	PUSH  ACC
	CLR   A
	MOVX  @DPTR,A

	MOV   DPTR,#uL_GRP
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#4EH
	CLR   F0
	CALL  uL_S_OP
	MOV   DPTR,#L_MRKD
	MOV   R4,#4
	MOV   R5,#0
	CALL  uL_S_WR
	CALL  uL_S_CL

	JNB   F0,L_MRK1
	; informovat o chybe
	;SETB  uLE_ABS
L_MRK1:
	MOV   DPTR,#uL_SBP+1
	POP   ACC
	MOVX  @DPTR,A
	CALL  DECDPTR
	POP   ACC
	MOVX  @DPTR,A
L_MRKR:	MOV   A,#1
	RET

L_MRKD: DB    0,7,0,0

; *******************************************************************
; Komunikace s krokovym motorem

; Pouzite vyvody
SC_DI	BIT   P1.6
SC_CI	BIT   P1.7
SC_DO	BIT   P4.0
SC_CO	BIT   P4.1

RSEG	DS____B

WL_FLG:	DS    1		; Priznaky rizeni vlnove delky
WLF_ERR	BIT   WL_FLG.7	; Chyba v poloze motorku
WLF_CAL	BIT   WL_FLG.6	; Pozadavek na novou kalibraci
WLF_POS	BIT   WL_FLG.5	; Pozadavek na najeti na polohu
WLF_BSY	BIT   WL_FLG.4	; Na pozadavku se pracuje

WLF_INIC SET  060H
WLF_BSYM SET  070H

RSEG	DS____X

SC_BUF:	DS    6		; Buffer pro komunikaci

WLEN:	DS    2		; Vlnova delka
WL_OFS:	DS    2         ; Posun vlnove delky

RSEG	DS____C

; Vysila postupne bity z R0, citac v R3
; na zacatek je treba nastavit R3=X8
SC_DSH:	CLR   A
	MOV   C,SC_DI
	ADDC  A,#0
	MOV   C,SC_DI
	ADDC  A,#0
	MOV   C,SC_DI
	ADDC  A,#0
	MOV   C,ACC.1
	MOV   A,R3
	ANL   A,#0FH
	JNZ   SC_DSH5
	MOV   A,#1	; zakonceni bytu
	RET
SC_DSH5:MOV   A,R0
	RRC   A
	MOV   R0,A
	MOV   SC_DO,C
	DEC   R3	; citani bitu
	CLR   A
	CLR   C
	RET

; Vygeneruje hodinovy impuls a ceka na odpoved zmenou SC_CI
; rusi R1 a F1
SC_GCL:	SETB  SC_CO
	CLR   A
	MOV   C,SC_CI
	ADDC  A,#0
	MOV   C,SC_CI
	ADDC  A,#0
	MOV   C,SC_CI
	ADDC  A,#-2
	MOV   PSW.1,C
	CLR   SC_CO
	MOV   R1,#0FFH
SC_GCL2:CLR   A
	MOV   C,SC_CI
	ADDC  A,#0
	MOV   C,SC_CI
	ADDC  A,#0
	MOV   C,SC_CI
	ADDC  A,#0
	XRL   A,PSW
	JB    ACC.1,SC_GCL9
	DJNZ  R1,SC_GCL2
	SETB  C
SC_GCL9:SETB  SC_CO
	RET

; Prenos R2 bytu dat z/do [DPTR]
SC_TR:	MOV   R3,#9H
	SETB  SC_DO
SC_TR10:CALL  SC_GCL		; Generovani 9 jednicek
	JC    SC_TR9E
	DJNZ  R3,SC_TR10
	CLR   SC_DO
	CALL  SC_GCL		; Uvodni nula
	JC    SC_TR9E
	INC   R2
SC_TR70:DJNZ  R2,SC_TR80
	JB    SC_DI,SC_TR9E
	SETB  SC_DO
	CALL  SC_GCL		; Zakoncovaci jednicka
	RET
SC_TR80:JB    SC_DI,SC_TR9E
	MOVX  A,@DPTR
	MOV   R0,A
	MOV   R3,#8H
SC_TR82:MOV   A,R0
	RRC   A
	MOV   R0,A
	MOV   SC_DO,C
	CALL  SC_GCL
	JC    SC_TR9E
	CLR   A			; Filtr cteni bitu
	MOV   C,SC_DI
	ADDC  A,#0
	MOV   C,SC_DI
	ADDC  A,#0
	MOV   C,SC_DI
	ADDC  A,#-2
	DJNZ  R3,SC_TR82	; Citani bitu
	MOV   A,R0
	RRC   A
	MOVX  @DPTR,A
	INC   DPTR
	CLR   SC_DO
	CALL  SC_GCL
	JNC   SC_TR70
SC_TR9E:SETB  C
	RET

; Test bitove komunikace
SC_TST:	CALL  INPUTc
	CJNE  A,#K_DP,SC_TST1
	RET
SC_TST1:CJNE  A,#K_0,SC_TST2
	CLR   SC_DO
	SJMP  SC_TST21
SC_TST2:CJNE  A,#K_1,SC_TST3
	SETB  SC_DO
SC_TST21:CALL SC_GCL
	JNC   SC_TST22
	CALL  ERRBEEP
SC_TST22:MOV  C,SC_DI
	CLR   A
	ADDC  A,#'0'
	CALL  LCDWR
	JMP   SC_TST
SC_TST3:JMP   SC_TST


WL_SMT_T:DB   LCD_CLR,'Go To'
	DB    C_LIN2 ,'Act',0

; Test krokace
WL_SMT:	MOV   DPTR,#WL_SMT_T
	CALL  cPRINT
	MOV   R6,#05H
	MOV   R7,#60H
	CALL  INPUTi
	JB    F0,WL_SMT4
	MOV   DPTR,#SC_BUF
	MOV   A,#2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   DPTR,#SC_BUF
	MOV   R2,#3
	CALL  SC_TR
	JNC   WL_SMT4
	CALL  ERRBEEP
WL_SMT4:MOV   DPTR,#SC_BUF
	MOV   A,#1
	MOVX  @DPTR,A
	MOV   R2,#5
	CALL  SC_TR
	JNC   WL_SMT5
	CALL  ERRBEEP
WL_SMT5:MOV   DPTR,#SC_BUF+1
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   A,#LCD_HOM+45H
	CALL  LCDWCOM
	MOV   R7,#60H
	CALL  PRINTi
	MOV   A,#LCD_HOM+4CH
	CALL  LCDWCOM
	MOV   DPTR,#SC_BUF+1+2
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  PRINThw
	JMP   INPUTc

WL_SMU_T:DB   LCD_CLR,'speed',0

WL_SMU:	MOV   DPTR,#WL_SMU_T
	CALL  cPRINT
	MOV   R6,#05H
	MOV   R7,#60H
	CALL  INPUTi
	JB    F0,WL_SMU9
	MOV   DPTR,#SC_BUF
	MOV   A,#4
WL_SMUS:MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   DPTR,#SC_BUF
	MOV   R2,#3
	CALL  SC_TR
WL_SMU9:RET

WL_SMV_T:DB   LCD_CLR,'cal.ph.',0

WL_SMV:	MOV   DPTR,#WL_SMV_T
	CALL  cPRINT
	MOV   R6,#08H
	MOV   R7,#60H
	CALL  INPUTi
	JB    F0,WL_SMU9
	MOV   DPTR,#SC_BUF
	MOV   A,#3
	SJMP  WL_SMUS

; Cteni polohy motoru + statusu
UR_WLEN:MOV   R4,#0FFH
	MOV   R5,#080H
	JB    WLF_ERR,UR_WLE9
	INC   R4
	MOV   A,WL_FLG
	ANL   A,#WLF_BSYM
	JNZ   UR_WLE9
	MOV   DPTR,#WLEN
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
UR_WLE9:MOV   A,#1
	RET

; Pozadavek na novou polohu
UW_WLEN:JNB   WLF_ERR,UW_WLE2
	SETB  WLF_CAL
UW_WLE2:MOV   DPTR,#WLEN
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	SETB  WLF_POS
	RET

; Vykonna rutina rizeni motorku
WL_DO:  JNB   WLF_BSY,WL_DO05
	JNB   SC_DI,WL_DO10
	RET
WL_DO05:JB    WLF_CAL,WL_DO60
	JB    WLF_POS,WL_DO20
	RET
; Kontrola stavu
WL_DO10:MOV   DPTR,#SC_BUF
	MOV   A,#1
	MOVX  @DPTR,A
	MOV   R2,#5
	CALL  SC_TR
	JC    WL_DO48
	MOV   DPTR,#SC_BUF+1+3
	MOVX  A,@DPTR
	MOV   C,ACC.7		; ATMEL FL_BSY
	MOV   WLF_BSY,C
	MOV   C,ACC.6		; ATMEL FL_ERR
	MOV   WLF_ERR,C
	RET
; Najeti na polohu
WL_DO20:SETB  WLF_BSY
	CLR   WLF_POS
	MOV   DPTR,#WLEN
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	%LDR23i(-10)
	CALL  CMPi
	MOV   C,OV
	XRL   A,PSW
	JB    ACC.7,WL_DO48
	%LDR23i(710)
	CALL  CMPi
	MOV   C,OV
	XRL   A,PSW
	JNB   ACC.7,WL_DO48
WL_DO30:MOV   A,R4
	MOV   B,#6
	MUL   AB
	MOV   R4,A
	MOV   A,B
	XCH   A,R5
	MOV   B,#6
	MUL   AB
	ADD   A,R5
	MOV   R5,A
	MOV   DPTR,#WL_OFS
	MOVX  A,@DPTR
	ADD   A,R4
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOV   R5,A
	MOV   A,#2
; Vyslani prikazu v ACC a R45
WL_DO40:MOV   DPTR,#SC_BUF
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   DPTR,#SC_BUF
	MOV   R2,#3
	CALL  SC_TR
	JNC   WL_DO49
WL_DO48:SETB  WLF_ERR
	CLR   WLF_BSY
	CALL  ERRBEEP
WL_DO49:RET
; Provedeni kalibrace
WL_DO60:SETB  WLF_BSY
	CLR   WLF_CAL
	MOV   A,#3
	MOV   R4,#0		; referencni faze
	MOV   R5,#0
	JMP   WL_DO40

; Pozadavek na nastaveni offsetu vlnove delky
UW_WOFS:JNB   WLF_ERR,UW_WOF1
	SETB  WLF_CAL
UW_WOF1:MOV   DPTR,#WL_OFS
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	SETB  WLF_POS
	RET

; *******************************************************************
; Rizeni zdroju svetla

; P6.7 .. 0 zapina zhaveni
; P6.6 .. 1 vyssi uroven zhaveni

; P4.7 .. 0 zapina zarovku
; P4.5 .. 0 zvetsuje napeti na zarovce

; P4.6 .. 0 zapina vybojku
; P4.4 .. 0 zvetsuje proud vybojkou (bylo i P4.5)

; P7.7 ..   proud vybojkou v poradku

LS_TD1	EQU   10	; Cas pro sepnuti vyssiho zhaveni
LS_TD2	EQU   23	; Caz zapaleni vybojky

RSEG	DS____X

LS_TDEL:DS    1		; Casovani zhaveni
LS_DLST:DS    1		; Stav vybojky

RSEG	DS____C

; Volano s frekvenci 0.6 Hz
LS_INT:	MOV   DPTR,#LS_TDEL
	MOVX  A,@DPTR
	JZ    LS_INT8
	DEC   A
	MOVX  @DPTR,A
	JNZ   LS_INT3
	ANL   P6,#NOT 040H	; P6.6 = 0 .. nizsi zhaveni
	ANL   P4,#NOT 040H	; P4.6 = 0 .. zapnout vybojku
	MOV   DPTR,#LS_DLST
	MOVX  A,@DPTR
	ANL   A,#03FH		; Neni chyba ani start
	MOVX  @DPTR,A
	RET
LS_INT3:CJNE  A,#LS_TD2,LS_INT9
	ORL   P6,#040H		; P6.6 = 1 .. vyssi zhaveni
	RET
LS_INT8:MOV   DPTR,#LS_DLST
	MOVX  A,@DPTR
	JZ    LS_INT9
	MOV   B,P7
	JNB   B.7,LS_INT9	; Kontrola proudu vybojkou
	SETB  ACC.7
	MOVX  @DPTR,A
LS_INT9:RET


LS_UV0	EQU   008H
LS_UV1	EQU   040H
LS_UV2	EQU   050H
LS_UV3	EQU   060H
LS_UV4	EQU   070H

LS_VIS0	EQU   004H
LS_VIS1	EQU   006H
LS_VIS2	EQU   007H
LS_VISC	EQU   005H

; Zdroj podle R23
LS_SET:	MOV   A,R2
	JNB   ACC.2,LS_SET5
	JNB   ACC.1,LS_SET2	; rizeni zarovky
LS_SET1:CLR   P4.7		; zapnout zarovku
	MOV   C,ACC.0
	CPL   C
	MOV   P4.5,C		; P4.5 hodnota napeti
	SETB  %LVIS_FL
	SJMP  LS_SET5
LS_SET2:JNB   ACC.0,LS_SET3
	JNB   %LVIS_FL,LS_SET1
%IF(0)THEN(			; prepinani napeti
	XRL   P4,#1 SHL 5	; P4.5
	SJMP  LS_SET5
)ELSE(				; prepinani zap/vyp
	SJMP  LS_SET3
)FI
LS_SET3:SETB  P4.7		; vypnout zarovku
	CLR   %LVIS_FL
LS_SET5:MOV   A,R2
	JNB   ACC.3,LS_SET6
	ORL   P4,#040H		; vypnuti vybojky
	ORL   P6,#080H		; P6.7 = 1 .. vypnout zhaveni
	CLR   A
	JMP   LS_SET8
LS_SET6:JNB   ACC.6,LS_SET9	; zapnuti vybojky
    %IF(1)THEN(
	MOV   C,ACC.4
	CPL   C
	MOV   P4.4,C		; P4.4 hodnota proudu
    )ELSE(
	CPL   A
	ORL   A,#NOT 030H
	ORL   P4,#030H
	ANL   P4,A		; P4.4, P4.5 hodnota proudu
    )FI
	MOV   DPTR,#LS_DLST
	MOVX  A,@DPTR
	JZ    LS_SET7		; Prvni zapnuti
	ANL   A,#0C0H
	JNZ   LS_SET7		; Hori spatne
	MOV   A,R3
	SJMP  LS_SET8
LS_SET7:ANL   P6,#NOT 040H	; P6.6 = 0 .. nizsi zhaveni
	ANL   P6,#NOT 080H	; P6.7 = 0 .. zapnout zhaveni
	MOV   A,#LS_TD1+LS_TD2
	MOV   DPTR,#LS_TDEL
	MOVX  @DPTR,A
	MOV   A,R3
	SETB  ACC.6
LS_SET8:MOV   DPTR,#LS_DLST
	MOVX  @DPTR,A
LS_SET9:RET

G_LAMPC_U:
	CLR   A
	MOV   R5,A
	JNB   %LVIS_FL,GLPC_U1
	INC   A
	JB    P4.5,GLPC_U1
	INC   A
GLPC_U1:SWAP  A
	MOV   R4,A
	MOV   DPTR,#LS_DLST
	MOVX  A,@DPTR
	ANL   A,#00FH
	ORL   A,R4
	MOV   R4,A
	JZ    GLPC_U9
	MOVX  A,@DPTR
	ANL   A,#0C0H
	MOV   R5,A		
GLPC_U9:RET

; *******************************************************************
; Interni ADC v 80C517A

RSEG	DS____X

ADC_CNT	EQU   8		; Pocet pouzivanych prevodniku

ADC0:	DS    2
ADC1:	DS    2
ADC2:	DS    2
ADC3:	DS    2
ADC4:	DS    2
ADC5:	DS    2
ADC6:	DS    2
ADC7:	DS    2

LS_ADRF	SET   ADC2	; Prime mereni reference

RSEG	DS____C

; Cteci cyklus ADC
ADC_D:	MOV   B,ADCON0
	JB    B.4,ADC_DR	; BSY
	MOV   A,ADCON1
	ANL   A,#0FH
	ADD   A,#-ADC_CNT
	JC    ADC_DR		; Neni co dal prevadet
	ADD   A,#ADC_CNT
	RL    A
	ADD   A,#LOW  ADC0
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH ADC0
	MOV   DPH,A
	MOV   A,ADDATL
	ANL   A,#0C0H
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,ADDATH
	MOVX  @DPTR,A
	MOV   A,B
	ANL   A,#0FH
	INC   A
	SJMP  ADC_S2

; Odstartovani dalsiho cykly
ADC_S:  CLR   A
	MOV   B,ADCON0
	JB    B.4,ADC_DR
ADC_S2:	ANL   A,#0FH
	MOV   ADCON1,A
	MOV   DAPR,#0F0H
ADC_DR:	RET

; Inicializace ADC
ADC_INI:ANL   ADCON0,#NOT 03FH
	MOV   ADCON1,#ADC_CNT
	MOV   DAPR,#0F0H
	RET

; *******************************************************************
; Emulace SPI

SPI_PORT DATA P5	; Brana, ke ktere je pripojena pamet
SPI_DO	EQU   020H	; DI periferie
SPI_DI	EQU   040H	; DO periferie
SPI_CL	EQU   080H	; CL

; Pristup na port musi byt chranen FL_SPILCK proti reentranci

; Nacte 8 bitu z periferie do ACC
SPI8IN:	MOV   A,#1
SPI8IN1:ORL   SPI_PORT,#SPI_CL		; Clock
	NOP
	ANL   SPI_PORT,#NOT SPI_CL
	PUSH  ACC
	MOV   A,SPI_PORT
	ANL   A,#SPI_DI
	ADD   A,#-1
	POP   ACC
	RLC   A
	JNC   SPI8IN1
	RET

; Vysle 8 bitu v ACC do periferie
SPI8OUT:SETB  C
SPI8OU0:RLC   A
SPI8OU1:JNC   SPI8OU2
	ORL   SPI_PORT,#SPI_DO
	SJMP  SPI8OU3
SPI8OU2:ANL   SPI_PORT,#NOT SPI_DO
SPI8OU3:ORL   SPI_PORT,#SPI_CL		; Clock
	NOP
	ANL   SPI_PORT,#NOT SPI_CL
	CLR   C
	RLC   A
	JNZ   SPI8OU1
	RET

SPI3OUT:ANL   A,#7
	SWAP  A
	RL    A
	ORL   A,#10H
	CLR   C
	SJMP  SPI8OU0

SPI7OUT:RL    A
	ORL   A,#1
	CLR   C
	SJMP  SPI8OU0

SPI6OUT:ANL   A,#03FH
	RL    A
	RL    A
	ORL   A,#2
	CLR   C
	SJMP  SPI8OU0

; *******************************************************************
; Prace s pameti EEPROM 9346

EEA_RD	EQU   1		; akce cteni
EEA_WR	EQU   2		; akce zapisu

EE_SEL	EQU   010H	; 1 .. select pameti

EE_WR_ATO EQU 1		; Pocet bytu zapisovanych naraz
EE_RD_ATO EQU 1		; Pocet bytu ctenych naraz
%DEFINE (EE_ORG16) (0)  ; Organizace po 16 bitech, jinak 8

; Prikazy v 1. trech bitech
EE_CMRD	EQU   110B	; read
EE_CMWR	EQU   101B	; write
EE_CMER	EQU   111B	; erase one byte
EE_CMEW	EQU   100B	; write/erase enable  .. ADR=11xxxxx
			; write/erase disable .. ADR=00xxxxx
			; erase all ..           ADR=10xxxxx
			; write all by data ..   ADR=01xxxxx

RSEG	DS____X

EE_PTR:	DS    2		; ukazatel do MEM_BUF na data
EEP_TAB:DS    2		; popis dat ulozenych v bloku EEPROM

MEM_BUF:DS    40H	; Buffer pameti EEPROM

RSEG	DS____C

; Vysle prikaz v ACC do pameti
EE_SCMD:ANL   SPI_PORT,#NOT EE_SEL
	ANL   SPI_PORT,#NOT SPI_CL
	NOP
	ORL   SPI_PORT,#SPI_CL
	NOP
	ANL   SPI_PORT,#NOT SPI_CL
	NOP
	ORL   SPI_PORT,#EE_SEL
	JMP   SPI3OUT


; Cteni pameti EEPROM
;	DPTR .. pointer na buffer
;	R2   .. pocet ctenych byte
;	R4   .. adresa, od ktere se cte

EE_RD:  MOV   R1,#0		; Inicializace XOR sum
	SETB  FL_SPILCK
EE_RD10:MOV   A,R2		; Priprava bloku 1-3 byte
	JZ    EE_RD40
	ADD   A,#-EE_RD_ATO
	MOV   R0,#EE_RD_ATO
	MOV   R2,A
	JC    EE_RD12
	ADD   A,#EE_RD_ATO
	MOV   R0,A
	MOV   R2,#0
EE_RD12:MOV   A,#EE_CMRD
	CALL  EE_SCMD
	MOV   A,R4
%IF(%EE_ORG16)THEN(
	RR    A
	CALL  SPI6OUT		; Organizace EEPROM 64*16
)ELSE(
	CALL  SPI7OUT		; Organizace EEPROM 128*8
)FI
EE_RD30:CALL  SPI8IN
	MOVX  @DPTR,A
	XRL   A,R1
	INC   A
	MOV   R1,A
	INC   DPTR
	INC   R4
	DJNZ  R0,EE_RD30
	SJMP  EE_RD10
EE_RD40:MOV   A,#EE_CMRD
	CALL  EE_SCMD
	MOV   A,R4
%IF(%EE_ORG16)THEN(
	RR    A
	CALL  SPI6OUT		; Organizace EEPROM 64*16
)ELSE(
	CALL  SPI7OUT		; Organizace EEPROM 128*8
)FI
	CALL  SPI8IN
	ANL   SPI_PORT,#NOT EE_SEL
	CLR   FL_SPILCK
	XRL   A,R1
	RET

EE_ERR:	SETB  F0
	ORL   A,#07H
	CLR   FL_SPILCK
	RET

; Zapis do pameti EEPROM
;	DPTR .. pointer na buffer
;	R2   .. pocet zapisovanych byte
;	R4   .. adresa, od ktere se zapisuje

EE_WR:	MOV   R1,#0		; Povoleni zapisu
	SETB  FL_SPILCK
	MOV   A,#EE_CMEW
	CALL  EE_SCMD
	MOV   A,#0FFH
	CALL  SPI7OUT
EE_WR10:MOV   A,R2		; Priprava bloku 1-3 byte
	JZ    EE_WR40
	ADD   A,#-EE_WR_ATO
	MOV   R0,#EE_WR_ATO
	MOV   R2,A
	JC    EE_WR12
	ADD   A,#EE_WR_ATO
	MOV   R0,A
	MOV   R2,#0
EE_WR12:MOV   A,#EE_CMWR	; Vyslani prikazu a adresy
	CALL  EE_SCMD
	MOV   A,R4
%IF(%EE_ORG16)THEN(
	RR    A
	CALL  SPI6OUT		; Organizace EEPROM 64*16
)ELSE(
	CALL  SPI7OUT		; Organizace EEPROM 128*8
)FI
EE_WR22:MOVX  A,@DPTR		; Vysilani bytu
	INC   DPTR
	PUSH  ACC
	XRL   A,R1
	INC   A
	MOV   R1,A
	POP   ACC
	CALL  SPI8OUT
	INC   R4
	DJNZ  R0,EE_WR22
	ANL   SPI_PORT,#NOT EE_SEL	;  Konec zapisu
	NOP
	ORL   SPI_PORT,#EE_SEL
EE_WR26:ORL   SPI_PORT,#SPI_CL
	NOP
	ANL   SPI_PORT,#NOT SPI_CL
	MOV   A,SPI_PORT
	ANL   A,#SPI_DI
	JZ    EE_WR26		; Cekat na SPI_CL=1
	SJMP  EE_WR10
EE_WR40:MOV   A,#EE_CMWR	; Vyslani prikazu a adresy
	CALL  EE_SCMD
	MOV   A,R4
%IF(%EE_ORG16)THEN(
	RR    A
	CALL  SPI6OUT		; Organizace EEPROM 64*16
)ELSE(
	CALL  SPI7OUT		; Organizace EEPROM 128*8
)FI
	MOV   A,R1		; Kontrolni byte
	CALL  SPI8OUT
	MOV   A,#EE_CMEW
	CALL  EE_SCMD
	MOV   A,#000H
	CALL  SPI7OUT
	ANL   SPI_PORT,#NOT EE_SEL	;  Konec zapisu
	CLR   FL_SPILCK
	CLR   A
	RET

EE_TSRD:MOV   R4,#10H
	MOV   R2,#22H
	MOV   DPTR,#MEM_BUF
	JMP   EE_RD

EE_TSWR:MOV   R4,#10H
	MOV   R2,#22H
	MOV   DPTR,#MEM_BUF
	JMP   EE_WR



; R45 := [EE_PTR++]
EEA_RDi:MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOV   R0,DPH
	MOV   A,DPL
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	RET

; [EE_PTR++] := R45
EEA_WRi:MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   R0,DPH
	MOV   A,DPL
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	RET

; provadi EEA_RD, EEA_WR pro iteger cislo
EEA_Mi:	CJNE  R0,#EEA_RD,EEA_Mi5
	CALL  EEA_RDi
	MOV   DPL,R2
	MOV   DPH,R3
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	RET
EEA_Mi5:CJNE  R0,#EEA_WR,EEA_Mi9
	MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  EEA_WRi
EEA_Mi9:RET

EEA_PRO:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#EE_PTR
	MOV   A,#LOW MEM_BUF
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH MEM_BUF
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
EEA_PR2:MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	ORL   A,R4
	JZ    EEA_Mi9
	PUSH  DPL
	PUSH  DPH
	MOV   A,R0
	PUSH  ACC
	CALL  JMPR45
	POP   ACC
	MOV   R0,A
	POP   DPH
	POP   DPL
	JMP   EEA_PR2

JMPR45:	MOV   A,R4
	PUSH  ACC
	MOV   A,R5
	PUSH  ACC
	RET

; Nastavi EEP_TAB a DPTR na R23
EEP_PTS:MOV   DPTR,#EEP_TAB
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	MOV   DPL,R2
	MOV   DPH,R3
	RET

; Ulozit data podle tabulky R23
EEP_WRS:CALL  EEP_PTS
	INC   DPTR
	INC   DPTR	; popis akci
	MOV   R0,#EEA_WR
	CALL  EEA_PRO
	MOV   DPTR,#EEP_TAB
	CALL  xMDPDP
	MOVX  A,@DPTR
	MOV   R2,A	; pocet prenasenych byte
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A	; pocatecni adresa v EEPROM
	MOV   DPTR,#MEM_BUF
	JMP   EE_WR

; Nacist data podle tabulky R23
EEP_RDS:CALL  EEP_PTS
	MOVX  A,@DPTR
	MOV   R2,A	; pocet prenasenych byte
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A	; pocatecni adresa v EEPROM
	MOV   DPTR,#MEM_BUF
	CALL  EE_RD
	JNZ   EEP_RD9
	MOV   DPTR,#EEP_TAB
	CALL  xMDPDP
	INC   DPTR
	INC   DPTR	; popis akci
	MOV   R0,#EEA_RD
	JMP   EEA_PRO
EEP_RD9:RET

; *******************************************************************
; Ukladani a cteni nastaveni z EEPROM

; Tabulky popisu akci pro ulozeni a cteni dat

; Tabulka pro SERVICE
EEC_SER:DB    020H	; pocet byte ukladanych dat
	DB    010H	; pocatecni adresa v EEPROM

	%W    (XAD_OFS)
	%W    (EEA_Mi)

	%W    (XAD_OFS+2)
	%W    (EEA_Mi)

	%W    (WL_OFS)
	%W    (EEA_Mi)

	%W    (COM_ADR)
	%W    (EEA_Mi)

	%W    (COM_SPD)
	%W    (EEA_Mi)

	%W    (COM_GRP)
	%W    (EEA_Mi)

	%W    (0)
	%W    (0)

; *******************************************************************
; Vystup na prevodnik AD1856

DAC_SEL	EQU   008H	; sestupna hrana zapis dat

DAC_OUT:CLR   A
	JB    FL_SPILCK,DAC_OUT9
	SETB  FL_SPILCK
	ANL   SPI_PORT,#NOT (EE_SEL OR SPI_CL)
	ORL   SPI_PORT,#DAC_SEL
	MOV   A,R5
	CALL  SPI8OUT
	MOV   A,R4
	CALL  SPI8OUT
	ANL   SPI_PORT,#NOT DAC_SEL
	ORL   SPI_PORT,#SPI_CL
	NOP
	ANL   SPI_PORT,#NOT SPI_CL
	ORL   SPI_PORT,#DAC_SEL
	CLR   FL_SPILCK
	MOV   A,#1
DAC_OUT9:RET

; *******************************************************************
; Vystup na absorbance na prevodnik

RSEG	DS____X

DAC_ATN:DS    2		; Stupen atenuatoru
DAC_MUL:DS    4		; Nasobeni vystupu na DAC

RSEG	DS____C

DAC_ABS:
    %IF(0)THEN(
	MOV   R7,#081H
	MOV   R6,#0
	MOV   R5,#0
    )FI
	MOV   DPTR,#DAC_MUL
	MOV   R0,#AKUM
	CALL  xiLDl
	CALL  MULf
	MOV   A,R6
	MOV   R2,A
	ORL   A,#80H
	MOV   R6,A
	MOV   A,R7
	ADD   A,#-080H+15
	JC    DAC_AB3
	MOV   R4,#0
	MOV   R5,#0
	SJMP  DAC_AB9
DAC_AB3:CPL   A
	ADD   A,#15+1
	JNB   ACC.7,DAC_AB4
	MOV   R4,#0FFH
	MOV   R5,#07FH
	SJMP  DAC_AB8
DAC_AB4:INC   A
	JBC   ACC.3,DAC_AB5
	XCH   A,R6
	XCH   A,R5
	MOV   R4,A
	SJMP  DAC_AB6
DAC_AB5:XCH   A,R6
	MOV   R4,A
	MOV   R5,#0
DAC_AB6:MOV   A,R6
	CALL  SHRi
DAC_AB8:MOV   A,R2
	JNB   ACC.7,DAC_AB9
	CALL  NEGi
DAC_AB9:JMP   DAC_OUT

; Nacte nasobitel DAC
G_DAMUL:MOV   DPTR,#DAC_MUL
	JMP   G_ABS10

S_DAMUL:CLR   A
	MOV   DPTR,#DAC_ATN
	MOVX  @DPTR,A
S_DAMU0:MOV   A,R7
	JNB   ACC.7,S_DAMU1
	CALL  NEGl
	CALL  CONVlRf
	MOV   A,R6
	ORL   A,#80H
	MOV   R6,A
	SJMP  S_DAMU2
S_DAMU1:CALL  CONVlRf
	MOV   A,R6
	ANL   A,#7FH
	MOV   R6,A
S_DAMU2:MOV   AKUM+0,R4
	MOV   AKUM+1,R5
	MOV   AKUM+2,R6
	MOV   AKUM+3,R7
	MOV   R4,#0BDH	; 1e-6 = BD 37 06 6D
	MOV   R5,#037H
	MOV   R6,#006H
	MOV   R7,#06DH
	CALL  MULf
	MOV   DPTR,#DAC_MUL
	CALL  xSVl
	MOV   A,#1
	RET

; Nastavi atenuator podle R4
S_DACAT23:
	MOV   A,R2
	MOV   R4,A
S_DACATN:
	CLR   A
	MOV   DPTR,#DAC_ATN+1
	MOVX  @DPTR,A
	MOV   A,R4
	MOV   DPTR,#DAC_ATN
	MOVX  @DPTR,A
	JZ    S_DACAT9
	DEC   A
	RL    A
	RL    A
	MOV   DPTR,#DAC_ATNt
	CALL  ADDATDP
	CALL  xLDl
	MOV   DPTR,#DAC_MUL
	CALL  xSVl
S_DACAT9:MOV  A,#1
	RET

DAC_ATNt:
	DB    000H,000H,000H,000H	;  0
	DB    000H,000H,000H,080H	;  2
	DB    000H,000H,000H,081H	;  1
	DB    000H,000H,000H,082H	; 0.5
	DB    000H,000H,020H,083H	; 0.2
	DB    000H,000H,020H,084H	; 0.1
	DB    000H,000H,020H,085H	; .05
	DB    000H,000H,048H,086H	; .02
	DB    000H,000H,048H,087H	; .01


; *******************************************************************
; Pomocne vstupy vystupy

AUX_POR	XDATA XC_BASE+7

AUX_IN_MSK EQU 030H

RSEG	DS____X

AUX_BUF:DS    2
AUX_INO:DS    1
ZR_MASK:DS    2		; Maska zmen vstupu pro nulovani

RSEG	DS____C

; Nastaveni AUX podle R4
; ======================
S_AUX:  MOV   A,R4
	ANL   A,#0FH
	MOV   R4,A
	MOV   DPTR,#AUX_BUF
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	ANL   A,#0F0H
	ORL   A,R4
	MOVX  @DPTR,A
	MOV   DPTR,#AUX_POR
	MOV   EA,C
	MOVX  @DPTR,A
	RET

; Nacte AUX do R4
; ======================
G_AUX:  MOV   DPTR,#AUX_BUF
	MOVX  A,@DPTR
	ANL   A,#0FH
G_AUX1:
	MOV   C,P1.3
	MOV   ACC.4,C
	MOV   C,P1.2
	MOV   ACC.5,C
	MOV   R4,A
	MOV   R5,#0
	MOV   A,#1
	RET

; Nastavi ACC pokud doslo ke zmene vstupu
; =======================================
AUX_ICH:
    %IF(0)THEN(
	CLR   A
	CALL  G_AUX1
    )ELSE(
	CALL  G_AUX	; Sledovat i zmeny vystupu
    )FI
	MOV   DPTR,#AUX_INO
	MOVX  A,@DPTR
	XCH   A,R4
	MOVX  @DPTR,A
	XRL   A,R4
	RET

; *******************************************************************
;

DB_W_10:MOV   R0,#10H
	SJMP  DB_WAI1

DB_WAIT:MOV   R0,#0H
DB_WAI1:%WATCHDOG
	DJNZ  R1,DB_WAI1
	DJNZ  R0,DB_WAI1
	RET

RAMT:	MOV   DPTR,#08000H

RAMT10:	MOV   C,EA
	PUSH  PSW
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#80H
RAMT11:	MOV   A,R5
	RL    A
	MOV   R5,A
	MOVX  @DPTR,A
	CLR   A
	MOVC  A,@A+DPTR
	XRL   A,R5
	JNZ   RAMTERR
	CJNE  R5,#80H,RAMT11
	MOV   A,R4
	MOVX  @DPTR,A
	POP   PSW
	MOV   EA,C

;	MOV   A,#LCD_HOM
;	CALL  LCDWCOM
;	MOV   R4,DPL
;	MOV   R5,DPH
;	CALL  PRINThw

	INC   DPTR
	MOV   A,DPH
	CJNE  A,#HIGH RAMT,RAMT14
	INC   DPH
	INC   DPH
RAMT14:	CJNE  A,#0FFH,RAMT10

	MOV   DPTR,#08000H

RAMT20:	MOV   C,EA
	PUSH  PSW
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#7FH
RAMT21:	MOV   A,R5
	RL    A
	MOV   R5,A
	MOVX  @DPTR,A
	CLR   A
	MOVC  A,@A+DPTR
	XRL   A,R5
	JNZ   RAMTERR
	CJNE  R5,#7FH,RAMT21
	MOV   A,R4
	MOVX  @DPTR,A
	POP   PSW
	MOV   EA,C

;	MOV   A,#LCD_HOM
;	CALL  LCDWCOM
;	MOV   R4,DPL
;	MOV   R5,DPH
;	CALL  PRINThw

	INC   DPTR
	MOV   A,DPH
	CJNE  A,#HIGH RAMT,RAMT24
	INC   DPH
	INC   DPH
RAMT24:	CJNE  A,#0FFH,RAMT20

	RET

RAMTERR:PUSH  ACC
	MOV   A,#LCD_HOM+8
	CALL  LCDWCOM
	POP   ACC
	CALL  PRINThb
	MOV   A,#':'
	CALL  LCDWR
	MOV   R4,DPL
	MOV   R5,DPH
	CALL  PRINThw
RAMEHLD:SJMP  RAMEHLD

L0:	MOV   SP,#STACK
	SETB  EA
	JNB   FL_DIPR,L007
	MOV   DPTR,#DEVER_T
	CALL  cPRINT
L007:

	CLR   A
	MOV   DPL,A
	MOV   DPH,A
	MOV   R4,A
	MOV   R5,A
	MOV   R3,A
	MOV   R0,A
	MOV   R1,#07FH
L010:	MOVX  A,@DPTR
	MOV   R2,A
	CALL  ADDi
	INC   DPTR
	DJNZ  R0,L010
	; may be needs %WATCHDOG
	DJNZ  R1,L010
	MOV   DPTR,#EPRCHK1
	CALL  cLDR23i
	CALL  CMPi
	PUSH  ACC

	%LDMXi(COM_ADR,3)	; Adresa 3
	%LDMXi(COM_SPD,3)	; Rychlost 19200
	%LDMXi(COM_GRP,2)	; Skupina 2

	%LDR23i(EEC_SER)	; Cteni parametru z EEPROM
	CALL  EEP_RDS

    %IF(%WITH_UL_DY_EEP) THEN (
	CALL  INI_SERNUM	; Initialize serial number
    )FI
	CALL  I_U_LAN		; Start komunikace uLan
	CALL  uL_OINI

	MOV   R7,#0FFH		; Start komunikace RS232
	MOV   R6,#-26*2
	CALL  RS232_INI

	POP   ACC
	JZ    L020
	MOV   DPTR,#ERR_EPR
	CALL  cPRINT
	MOV   A,#LCD_HOM+040H
	CALL  LCDWCOM
	MOV   R7,#50H
	CALL  PRINTi
L019:	CALL  SCANKEY
	JZ    L019

L020:
	;CALL  DB_WAIT
	CALL  RAMT		; Kontrola pameti RAM

	JB    FL_DIPR,L090
				; Bez displeje
L080:   CALL  XAD_INIABS
L081:	;CALL  UC_OI
	CALL  UI_PR		; uL slave OI
	CALL  WL_DO		; rizeni vlnove delky
    %IF(%WITH_UL_DY) THEN (
	CALL  UD_RQ		; dynamicaka adresace
    )FI
	JMP   L081

L090:	MOV   A,#LCD_CLR
	CALL  LCDWCOM

	MOV   A,#K_PROG
	CALL  TESTKEY
	JZ    L_IHEX

	MOV   A,#K_DP
	CALL  TESTKEY
	JZ    L1
L092:	CALL  XAD_INIABS
	JMP   UT

L_IHEX:	CALL  IHEXLD
	SJMP  L_IHEX

SER_ENT:MOV   DPTR,#SER_ENTT
	CALL  cPRINT
	CALL  INPUTc
	XRL   A,#K_9
	JZ    L1
	SETB  FL_DRAW
	RET

L1:	MOV   SP,#STACK
%IF (0) THEN (
	MOV   A,#LCD_HOM+8
	CALL  LCDWCOM
	MOV   DPTR,#TMP
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	INC   DPTR
	JNZ   L1001
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
L1001:	MOV   DPTR,#TMP
	CALL  xLDR45i
	MOV   R7,#60H
	CALL  PRINTi
	JMP   L1
)FI

L2:	CALL  SCANKEY
	JZ    L3
	MOV   R7,A
	MOV   DPTR,#SFT_D1
	CALL  SEL_FNC
	MOV   A,#LCD_CLR
	CALL  LCDWCOM
L3:
	MOV   C,SC_DI
	MOV   %RUN_FL,C
	CALL  LEDWR

	JMP   L1

SFT_D1:	DB    K_DP
	%W    (0)
	%W    (MONITOR)

	DB    K_1
	%W    (0)
	%W    (XC_LDT)		; Download do XC3164A

	DB    K_2
	%W    (0)
	%W    (SC_TST)		; Test bitove komunikace

	DB    K_3
	%W    (0)
	%W    (WL_SMT)		; Najeti na vlnovou delku

	DB    K_4
	%W    (0)
	%W    (WL_SMU)		; Nastaveni rychlosti krokace

	DB    K_5
	%W    (0)
	%W    (XAD_TST)		; Test AD prevodniku

	DB    K_MODE
	%W    (0)
	%W    (XAD_TST_NL)	; Test AD bez nahrati

	DB    K_6
	%W    (0)
	%W    (WL_SMV)		; Kalibrace nulove polohy

	DB    K_LOFF
	DB    LS_UV0 OR LS_VIS0,0
	%W    (LS_SET)		; Rizeni zdroju svetla

	DB    K_UVL
	DB    LS_UV1,1
	%W    (LS_SET)		; Rizeni zdroju svetla

	DB    K_UVH
	DB    LS_UV2,2
	%W    (LS_SET)		; Rizeni zdroju svetla

	DB    K_VIS
	DB    LS_VISC,0
	%W    (LS_SET)		; Rizeni zdroju svetla

	DB    K_7
	%W    (0)
	%W    (EE_TSWR)

	DB    K_8
	%W    (0)
	%W    (EE_TSRD)

	DB    K_9
	%W    (LS_UV4)
	%W    (UT)		; Spusteni user interface

	DB    0

DEVER_T:DB    LCD_CLR,'%VERSION'
	DB    C_LIN2 ,' (c) PiKRON 2010',0

SER_ENTT:DB   LCD_CLR,'Entering service'
	DB    C_LIN2 ,'mode (Enter=End)',0

ERR_EPR:DB    LCD_CLR,'EPROM error !',0

; *******************************************************************
; Komunikace s ostatnimi jednotkami pres uLan

; Definice jmenprikazu

%OID_ADES(AI_STATUS,STATUS,u2)
%OID_ADES(AI_ERRCLR,ERRCLR,e)
I_WLEN	  EQU	204
%OID_ADES(AI_WLEN,WLEN,u2)
I_LAMPC	  EQU   207
%OID_ADES(AI_LAMPC,LAMPC,u2)
I_ADCFILT EQU   208
%OID_ADES(AI_ADCFILT,ADCFILT,u2/.2)
I_ADCMODE EQU   209
%OID_ADES(AI_ADCMODE,ADCMODE,u2)
I_ABS	  EQU   220
%OID_ADES(AI_ABS,ABS,f4)
I_AUXUAL  EQU	240
%OID_ADES(AI_AUXUAL,AUXUAL,u2)
I_OFF	  EQU   250
%OID_ADES(AI_OFF,OFF,e)
I_ON	  EQU   251
%OID_ADES(AI_ON,ON,e)
I_ZERO	  EQU   255
%OID_ADES(AI_ZERO,ZERO,e)
I_MARK_MASK EQU 462
%OID_ADES(AI_MARK_MASK,MARK_MASK,u2)
I_ZERO_MASK EQU 463
%OID_ADES(AI_ZERO_MASK,ZERO_MASK,u2)

; *******************************************************************
; Komunikace pres uLan - cast slave

RSEG	DS____X

TMP_U:	DS    16

RSEG	DS____C

; inicializace objektovych komunikaci
uL_OINI:
%IF(0) THEN (
	CALL  NUL_IP
	%LDR45i (UPP_0)
	MOV   DPTR,#UC_AUPP	; seznam parametru ostatnich jednotek
	CALL  xSVR45i
)FI
	%LDR45i (OID_1IN)	; seznam prijimanych prikazu
	%LDR67i (OID_1OUT)	; seznam vysilanych prikazu
	JMP    US_INIT

; Identifikace typu pristroje
PUBLIC	uL_IDB,uL_IDE
uL_IDB: DB    '.mt %VERSION .uP 51x'
    %IF(%WITH_UL_DY) THEN (
	DB    ' .dy'
    )FI
	DB    0
uL_IDE:

; Subsystem komunikace

; Status

G_STATUS:
	%LDR45i (0)
	RET

ERRCLR_U:RET

ON_U:   RET
	;LDR23i(GL_PWRON)
	;JMP   GLOB_RQ23

OFF_U:	RET
	;LDR23i(GL_STBY)
	;JMP   GLOB_RQ23

; Cteni absorbance/hodnoty v pohyblive radove carce

UO_ABSf:MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R0
	MOV   DPH,A
	MOV   C,EA
	CLR   EA
	CALL  xLDl	; R4567 = absorbance float
	MOV   EA,C
        CALL  f2IEEE
	MOV   DPTR,#TMP_U
	CALL  xSVl
	MOV   DPTR,#TMP_U
	%LDR45i(4)
	%VJMP (UV_WR)

; Nastaveni rezimu prevodniku

ADCMOD_U:
	MOV   A,R4
	MOV   DPTR,#XAD_MOD
	MOVX  @DPTR,A 
	JMP   XAD_SMOD

; Nastaveni zdroje svetla

LAMPC_U:
	MOV   R3,#0
	MOV   A,R4
	SWAP  A
	INC   A
	ANL   A,#0FH
	JZ    LAMPC_U3
	DEC   A
	MOV   R2,#LS_VIS0
	DJNZ  ACC,LAMPC_U1
	MOV   R2,#LS_VIS1
LAMPC_U1:
	DJNZ  ACC,LAMPC_U2
	MOV   R2,#LS_VIS2
LAMPC_U2:
	CALL  LS_SET
LAMPC_U3:
	MOV   A,R4
	INC   A
	ANL   A,#0FH
	JZ    LAMPC_U6
	DEC   A
	MOV   R2,#LS_UV0
	DJNZ  ACC,LAMPC_U4
	MOV   R2,#LS_UV1
	MOV   R3,#1
LAMPC_U4:
	DJNZ  ACC,LAMPC_U5
	MOV   R2,#LS_UV2
	MOV   R3,#2
LAMPC_U5:
	CALL  LS_SET
LAMPC_U6:
	RET

; Prijimane prikazy

OID_T	SET   $
	%W    (I_ERRCLR)
	%W    (OID_ISTD)
	%W    (AI_ERRCLR)
	%W    (ERRCLR_U)

%OID_NEW(I_ON,AI_ON)
	%W    (ON_U)

%OID_NEW(I_OFF,AI_OFF)
	%W    (OFF_U)

%OID_NEW(I_ZERO,AI_ZERO)
	%W    (ZER_ABS)

%OID_NEW(I_LAMPC,AI_LAMPC)
	%W    (UI_INT)
	%W    (0)
	%W    (LAMPC_U)

%OID_NEW(I_ADCFILT,AI_ADCFILT)
	%W    (UI_INT)
	%W    (0)
	%W    (S_TIMCO)

%OID_NEW(I_ADCMODE,AI_ADCMODE)
	%W    (UI_INT)
	%W    (0)
	%W    (ADCMOD_U)

%OID_NEW(I_MARK_MASK,AI_MARK_MASK)
	%W    (UI_INT)
	%W    (LM_MASK)
	%W    (0)

%OID_NEW(I_ZERO_MASK,AI_ZERO_MASK)
	%W    (UI_INT)
	%W    (ZR_MASK)
	%W    (0)

%OID_NEW(I_AUXUAL,AI_AUXUAL)
	%W    (UI_INT)
	%W    (0)
	%W    (S_AUX)

%OID_NEW(I_WLEN,AI_WLEN)
	%W    (UI_INT)
	%W    (0)
	%W    (UW_WLEN)

OID_1IN SET   OID_T

; Vysilane hodnoty

OID_T	SET   0

%OID_NEW(I_LAMPC,AI_LAMPC)
	%W    (UO_INT)
	%W    (0)
	%W    (G_LAMPC_U)

%OID_NEW(I_ADCFILT,AI_ADCFILT)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TIMCO)

%OID_NEW(I_MARK_MASK,AI_MARK_MASK)
	%W    (UO_INT)
	%W    (LM_MASK)
	%W    (0)

%OID_NEW(I_ZERO_MASK,AI_ZERO_MASK)
	%W    (UO_INT)
	%W    (ZR_MASK)
	%W    (0)

%OID_NEW(I_AUXUAL,AI_AUXUAL)
	%W    (UO_INT)
	%W    (0)
	%W    (G_AUX)

%OID_NEW(I_WLEN,AI_WLEN)
	%W    (UO_INT)
	%W    (0)
	%W    (UR_WLEN)

%OID_NEW(I_STATUS,AI_STATUS)
	%W    (UO_INT)
	%W    (0)
	%W    (G_STATUS)

%OID_NEW(I_ABS,AI_ABS)
	%W    (UO_ABSf)
	%W    (ABS)

OID_1OUT SET  OID_T

; *******************************************************************
; User interface

RSEG	DS____X

UT_UIAD:DS    40
UT_DATA:DS    40

RSEG	DS____C

UT_INIT:CLR   D4LINE
	SETB  FL_CMAV
	MOV   DPTR,#UI_MV_SX
	MOV   A,#16
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#2
	MOVX  @DPTR,A
	MOV   DPTR,#UT_UIAD
	MOV   UI_AD,DPL
	MOV   UI_AD+1,DPH
	CLR   A
	MOV   DPTR,#EV_BUF
	MOVX  @DPTR,A
	MOV   DPTR,#GR_ACT
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#REF_TIM
	MOVX  @DPTR,A
	RET

UT_TREF:MOV   DPTR,#REF_TIM
	MOVX  A,@DPTR
	JNZ   UT_TRE1
	MOV   DPTR,#REF_PER
	MOVX  A,@DPTR
	MOV   DPTR,#REF_TIM
	MOVX  @DPTR,A
	MOV   A,#1
	RET
UT_TRE1:CLR   A
	RET

UT:     CALL  UT_INIT
	MOV   R7,#ET_RQGR
	%LDR45i(UT_GR11)
	CALL  EV_POST

UT_ML:  CALL  EV_GET
	JZ    UT_ML50
UT_ML44:CJNE  R7,#ET_GLOB,UT_ML45
	;CALL  GLOB_DO		; Globalni udalosti
	SJMP  UT_ML
UT_ML45:CALL  EV_DO
	JMP   UT_ML
UT_ML50:CALL  WL_DO		; rizeni vlnove delky
	;CALL  GS_DO		; vyvola handler signalu

	;JNB   FL_ECRS,UT_ML53
	;CALL  RS_POOL		; Komunikace RS232
	;JNZ   UT_ML53
	;MOV   DPTR,#STATUS+1
	;MOVX  A,@DPTR
	;CALL  RS_RDYSND
    %IF(1)THEN(
	CALL   RS_RD
	JZ     UT_ML53
	MOV    A,R0
	CALL   RS_WR
    )FI

UT_ML53:;JNB   FL_ECUL,UT_ML55
	;CALL  UC_OI		; uL master OC
	CALL  UI_PR		; uL slave OI
	JB    uLF_INE,UT_ML55
	;JB    UCF_RDP,UT_ML57
UT_ML55:CALL  UT_TREF		; Urceni okamziku refrese od casu
	JZ    UT_ML60
	;JNB   FL_ECUL,UT_ML57
	;CALL  UC_REFR		; uL master OC
UT_ML57:;CLR   UCF_RDP
	SETB  FL_REFR
	SJMP  UT_ML65
UT_ML60:CALL  UT_ULED
    %IF(%WITH_UL_DY) THEN (
	CALL  UD_RQ		; uLan dynamic addressing
    )FI
UT_ML65:JMP   UT_ML

UT_ULED:CALL  UT_USLS
	JMP   LEDWR

UT_USLS:MOV   DPTR,#LS_DLST
	MOVX  A,@DPTR
	MOV   B,A
	CLR   A
	MOV   C,B.0
	MOV   ACC.LFB_LLOW,C
	MOV   C,B.1
	MOV   ACC.LFB_LHIG,C
	MOV   R2,A
	XRL   A,#NOT ((1 SHL LFB_LHIG)OR(1 SHL LFB_LLOW))
	ANL   LEB_FLG,A
	ANL   LED_FLG,A		; Off
	MOV   A,B
	ANL   A,#0C0H
	JZ    UT_UST3		; On
	SJMP  UT_UST7		; Error

; Stav R45 na ledku s maskou R2
UT_UST1:MOV   A,R5
	JB    ACC.7,UT_UST7
	ORL   A,R4
UT_UST2:JZ    UT_UST5
UT_UST3:MOV   A,R2		; On
	CPL   A
	ANL   LEB_FLG,A
	CPL   A
	ORL   LED_FLG,A
	RET
UT_UST5:MOV   A,R2		; Off
	CPL   A
	ANL   LEB_FLG,A
	ANL   LED_FLG,A
	RET
UT_UST7:MOV   A,R2		; Error
	ORL   LEB_FLG,A
UT_UST9:RET

; *******************************************************************
; Podminene metody objektu a rozsireni UI

EXTRN	DATA(EV_TYPE)
EXTRN	CODE(FND_MINT,UB_GFLG,UB_DRAW1,UB_A_RD,UB_WRDPN)
EXTRN	CODE(BFL_EV,BFL_XOR,BFL_3ST,BFL_3HL,UB_A_WR)

OU_IMUT_T SET OU_I_E+3

; UIN nebo MUT
UIN_MUT_EV:
	MOV   A,EV_TYPE
	CJNE  A,#ET_DRAW,UIN_MUT_EV1
	SJMP  UIN_MUT_EV2
UIN_MUT_EV1:
	CJNE  A,#ET_REFR,UIN_MUT_EV8
UIN_MUT_EV2:
	CALL  UB_GFLG
	JB    ACC.UFB_EDV,UIN_MUT_EV8
	CALL  GOTO_A
	JZ    UIN_MUT_EV9

	MOV   R1,EV_TYPE
	CALL  UB_A_RD
	JB    F0,UIN_MUT_EV6
	JZ    UIN_MUT_EV5
	MOV   DPL,UI_AU
	MOV   DPH,UI_AU+1
	MOV   A,#OU_SX
	MOVC  A,@A+DPTR		; OU_SX
	MOV   R6,A
	%UI_OU2DP(OU_IMUT_T)
	CALL  FND_MINT
	JZ    UIN_MUT_EV7
	JMP   UB_WRDPN
UIN_MUT_EV5:
	MOV   A,EV_TYPE
	XRL   A,#ET_REFR
	JZ    UIN_MUT_EV9
UIN_MUT_EV6:
	MOV   A,#'-'
	JMP   UB_DRAW1
UIN_MUT_EV7:
	MOV   DPL,UI_AU
	MOV   DPH,UI_AU+1
	MOV   A,#OU_I_F
	MOVC  A,@A+DPTR
	MOV   R3,A
	MOV   A,#OU_SX
	MOVC  A,@A+DPTR
	MOV   R2,A
	JMP   PRINTli
UIN_MUT_EV8:
	JMP   UIN_EV
UIN_MUT_EV9:
	RET

; Prechod mezi displeji s navratem k absorbanci
GR_RQ23_ABS:
	MOV   DPTR,#XAD_MOD
	MOVX  A,@DPTR
	JZ    GR_RQ23_ABS1
	CLR   A
	CALL  XAD_SMOD
GR_RQ23_ABS1:
	JMP   GR_RQ23

GR_EV_PRMT:
	MOV   A,EV_TYPE
	CJNE  A,#ET_DONE,GR_EV_PRMT1
	MOV   DPTR,#XAD_MOD
	MOVX  A,@DPTR
	JZ    GR_EV_PRMT1
	CLR   A
	CALL  XAD_SMOD
GR_EV_PRMT1:
	JMP   GR_EV

; AUX nastavit jen mimo program
BFL_EV_NR:
	MOV   A,EV_TYPE
	CJNE  A,#ET_GETF,BFL_EV_NR1
	JB    %RUN_FL,BFL_EV_NR9
BFL_EV_NR1:
	JMP   BFL_EV
BFL_EV_NR9:
	RET

; INT nastavit jen mimo program
UIN_EV_NR:
	MOV   A,EV_TYPE
	CJNE  A,#ET_GETF,UIN_EV_NR1
	JB    %RUN_FL,UIN_EV_NR9
UIN_EV_NR1:
	JMP   UIN_EV
UIN_EV_NR9:
	RET

; Nacte hodnotu, pricte R2, a kontroluje max R3
MUT_AD23:
	MOV   A,R2
	PUSH  ACC
	MOV   A,R3
	PUSH  ACC
	CALL  UB_A_RD
	POP   B
	POP   ACC
	ADD   A,R4
	JB    ACC.7,MUT_AD23N
	MOV   R4,A
	CLR   C
	JBC   B.7,MUT_AD23K
	SUBB  A,B
	JC    MUT_AD23W
	MOV   R4,B
	DEC   R4
	SJMP  MUT_AD23W
MUT_AD23K:
	SUBB  A,B
	JC    MUT_AD23W
	MOV   R4,A
	SJMP  MUT_AD23W
MUT_AD23N:
	JNB   B.7,MUT_AD23M
	ADD   A,B
	MOV   R4,A
	DB    074H	; MOV  A,#n
MUT_AD23M:
	MOV   R4,#0
MUT_AD23W:
	MOV   R5,#0
	JMP   UB_A_WR

; *******************************************************************

UT_SF1:
	DB    K_PROG
	%W    (0)
	%W    (SER_ENT)		; Pomocny rezim

	DB    K_ABS
	%W    (UT_GR11)
	%W    (GR_RQ23_ABS)

	DB    K_ATTEN
	%W    (UT_GR12)
	%W    (GR_RQ23)

	DB    K_AUX
	%W    (UT_GR13)
	%W    (GR_RQ23)

	DB    K_PRET
	%W    (UT_GR18)
	%W    (GR_RQ23)

	DB    K_MODE
	%W    (UT_GR70)
	%W    (GR_RQ23)

	DB    K_ZERO
	%W    (0)
	%W    (ZER_ABS)		; Nulovani absorbance

	DB    K_LOFF
	DB    LS_UV0 OR LS_VIS0,0
	%W    (LS_SET)		; Rizeni zdroju svetla

	DB    K_UVL
	DB    LS_UV1,1
	%W    (LS_SET)		; Rizeni zdroju svetla

	DB    K_UVH
	DB    LS_UV2,2
	%W    (LS_SET)		; Rizeni zdroju svetla

	DB    K_VIS
	DB    LS_VISC,0
	%W    (LS_SET)		; Rizeni zdroju svetla

	DB    K_MARK
	DB    0,0
	%W    (LAN_MRK)		; Vyslani znacky

UT_SFTN:DB    K_RIGHT
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    K_LEFT
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    K_UP
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    K_DOWN
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

UT_SF0:	DB    0

; *******************************************************************
; Manualni rezim rezim

; ---------------------------------
; Zobrazeni absorbance

UT_GR11:DS    UT_GR11+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR11+OGR_BTXT-$
	%W    (UT_GT11)
	DS    UT_GR11+OGR_STXT-$
	%W    (0)
	DS    UT_GR11+OGR_HLP-$
	%W    (0)
	DS    UT_GR11+OGR_SFT-$
	%W    (UT_SF11)
	DS    UT_GR11+OGR_PU-$
	%W    (UT_U1101)
	%W    (UT_U1102)
	%W    (UT_U1103)
	%W    (UT_U1104)
	%W    (UT_U1105)
	%W    (0)

UT_GT11:DB    C_NL
	DB    CG_LAM,' +++nm  ',CG_TAU,' +.++s',0
;	DB    CG_LAM,0BDH,'+++nm  ',CG_TAU,0B2H,'+.++s',0

UT_SF11:DB    -1
	%W    (UT_SF1)

UT_U1101:	; Merena velicina
	DS    UT_U1101+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1101+OU_MSK-$
	DB    0
	DS    UT_U1101+OU_X-$
	DB    0,0,3,1
	DS    UT_U1101+OU_HLP-$
	%W    (0)
	DS    UT_U1101+OU_SFT-$
	%W    (0)
	DS    UT_U1101+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1101+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (XAD_MOD)		; DP
	DS    UT_U1101+OU_M_S-$
	%W    (0)
	DS    UT_U1101+OU_M_P-$
	%W    (0)
	DS    UT_U1101+OU_M_F-$
	%W    (XAD_CHMOD)
	DS    UT_U1101+OU_M_T-$
	%W    (0FFH)
	%W    (000H)
	%W    (UT_U1101T0)
	%W    (0FFH)
	%W    (001H)
	%W    (UT_U1101T1)
	%W    (0FFH)
	%W    (002H)
	%W    (UT_U1101T2)
	%W    (0FFH)
	%W    (003H)
	%W    (UT_U1101T3)
	%W    (0FFH)
	%W    (004H)
	%W    (UT_U1101T4)
	%W    (0)
UT_U1101T0:DB	'ABS',0
UT_U1101T1:DB	'CUV',0
UT_U1101T2:DB	'REF',0
UT_U1101T3:DB	'RAT',0
UT_U1101T4:DB	'NUL',0

UT_U1102:	; Absorbance/vystup
	DS    UT_U1102+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1102+OU_MSK-$
	DB    0
	DS    UT_U1102+OU_X-$
	DB    4,0,9,1
	DS    UT_U1102+OU_HLP-$
	%W    (0)
	DS    UT_U1102+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1102+OU_A_RD-$
	%W    (G_ABSl)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1102+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U1102+OU_I_F-$
	DB    0C6H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1103:	; Stav
	DS    UT_U1103+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1103+OU_MSK-$
	DB    0
	DS    UT_U1103+OU_X-$
	DB    14,0,2,1
	DS    UT_U1103+OU_HLP-$
	%W    (0)
	DS    UT_U1103+OU_SFT-$
	%W    (0)
	DS    UT_U1103+OU_A_RD-$
	%W    (NULL_A)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1103+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U1103+OU_M_S-$
	%W    (0)
	DS    UT_U1103+OU_M_P-$
	%W    (0)
	DS    UT_U1103+OU_M_F-$
	%W    (0)
	DS    UT_U1103+OU_M_T-$
	%W    (0FF00H)
	%W    (100H)
	%W    (UT_U1103TR)
	%W    (0FFFFH)
	%W    (0)
	%W    (UT_U1103T0)
	%W    (08000H)
	%W    (08000H)
	%W    (UT_U1103TE)
	%W    (0)
UT_U1103T0:DB	'  ',0
UT_U1103TR:DB	'On',0
UT_U1103TE:DB	'Error',0

UT_U1104:	; Vlnova delka
	DS    UT_U1104+OU_VEVJ-$
	DB    2
	DW    UIN_MUT_EV
	DS    UT_U1104+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1104+OU_X-$
	DB    2,1,3,1
	DS    UT_U1104+OU_HLP-$
	%W    (0)
	DS    UT_U1104+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1104+OU_A_RD-$
	%W    (UR_WLEN)		; A_RD
	%W    (UW_WLEN)		; A_WR
	DS    UT_U1104+OU_DPSI-$
	%W    (0)		; DPSI
	%W    (WLEN)		; DP
	DS    UT_U1104+OU_I_F-$
	DB    080H		; format I_F
	%W    (-10)		; I_L
	%W    (700)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru
	DS    UT_U1104+OU_IMUT_T-$
	%W    (0FFFFH)
	%W    (080FFH)
	%W    (UT_U1104T2)
	%W    (0FF00H)
	%W    (08000H)
	%W    (UT_U1104T1)
	%W    (0)
UT_U1104T1:DB	'WRK',0
UT_U1104T2:DB	'ERR',0

UT_U1105:	; Casova konstanta
	DS    UT_U1105+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1105+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1105+OU_X-$
	DB    11,1,4,1
	DS    UT_U1105+OU_HLP-$
	%W    (0)
	DS    UT_U1105+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1105+OU_A_RD-$
	%W    (G_TIMCO)		; A_RD
	%W    (S_TIMCO)		; A_WR
	DS    UT_U1105+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U1105+OU_I_F-$
	DB    02H		; format I_F
	%W    (4)		; I_L
	%W    (FI_MLEN*4)	; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Attenuator

UT_GR12:DS    UT_GR12+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR12+OGR_BTXT-$
	%W    (UT_GT12)
	DS    UT_GR12+OGR_STXT-$
	%W    (0)
	DS    UT_GR12+OGR_HLP-$
	%W    (0)
	DS    UT_GR12+OGR_SFT-$
	%W    (UT_SF12)
	DS    UT_GR12+OGR_PU-$
	%W    (UT_U1101)
	%W    (UT_U1102)
	%W    (UT_U1201)
	%W    (UT_U1211)
	%W    (0)

UT_GT12:DB    '              AU',C_NL
	DB    '    AUFS :',0

UT_SF12:DB    -1
	%W    (UT_SF1)

UT_U1201:
	DS    UT_U1201+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1201+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1201+OU_X-$
	DB    0,1,3,1
	DS    UT_U1201+OU_HLP-$
	%W    (0)
	DS    UT_U1201+OU_SFT-$
	%W    (UT_SF1201)
	DS    UT_U1201+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (S_DACATN)	; A_WR
	DS    UT_U1201+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (DAC_ATN)		; DP
	DS    UT_U1201+OU_M_S-$
	%W    (0)
	DS    UT_U1201+OU_M_P-$
	DB    1,10+080H
	DS    UT_U1201+OU_M_F-$
	%W    (MUT_AD23)
	DS    UT_U1201+OU_M_T-$
	%W    (000FFH)
	%W    (00000H)
	%W    (UT_U1201T0)
	%W    (000FFH)
	%W    (00001H)
	%W    (UT_U1201T1)
	%W    (000FFH)
	%W    (00002H)
	%W    (UT_U1201T2)
	%W    (000FFH)
	%W    (00003H)
	%W    (UT_U1201T3)
	%W    (000FFH)
	%W    (00004H)
	%W    (UT_U1201T4)
	%W    (000FFH)
	%W    (00005H)
	%W    (UT_U1201T5)
	%W    (000FFH)
	%W    (00006H)
	%W    (UT_U1201T6)
	%W    (000FFH)
	%W    (00007H)
	%W    (UT_U1201T7)
	%W    (000FFH)
	%W    (00008H)
	%W    (UT_U1201T8)
	%W    (000FFH)
	%W    (00009H)
	%W    (UT_U1201T9)
	%W    (0)
UT_U1201T0:DB    'Usr',0
UT_U1201T1:DB    'Off',0
UT_U1201T2:DB    ' 2 ',0
UT_U1201T3:DB    ' 1 ',0
UT_U1201T4:DB    '0.5',0
UT_U1201T5:DB    '0.2',0
UT_U1201T6:DB    '0.1',0
UT_U1201T7:DB    '.05',0
UT_U1201T8:DB    '.02',0
UT_U1201T9:DB    '.01',0

UT_SF1201:
	DB    K_0
	%W    (1)
	%W    (S_DACAT23)

	DB    K_2
	%W    (2)
	%W    (S_DACAT23)

	DB    K_1
	%W    (3)
	%W    (S_DACAT23)

	DB    K_5
	%W    (4)
	%W    (S_DACAT23)

	DB    K_UP
	DB    -1,10
	%W    (MUT_AD23)

	DB    K_DOWN
	DB    1,10
	%W    (MUT_AD23)

	DB    0

UT_U1211:	; Attenuator
	DS    UT_U1211+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1211+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1211+OU_X-$
	DB    12,1,4,1
	DS    UT_U1211+OU_HLP-$
	%W    (0)
	DS    UT_U1211+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1211+OU_A_RD-$
	%W    (G_DAMUL)		; A_RD
	%W    (S_DAMUL)		; A_WR
	DS    UT_U1211+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U1211+OU_I_F-$
	DB    0C6H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Pomocne vstupy vystupy

UT_GR13:DS    UT_GR13+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR13+OGR_BTXT-$
	%W    (UT_GT13)
	DS    UT_GR13+OGR_STXT-$
	%W    (0)
	DS    UT_GR13+OGR_HLP-$
	%W    (0)
	DS    UT_GR13+OGR_SFT-$
	%W    (UT_SF13)
	DS    UT_GR13+OGR_PU-$
	%W    (UT_U1301)
	%W    (UT_U1302)
	%W    (UT_U1303)
	%W    (UT_U1304)
	%W    (0)

UT_GT13:DB    'Aux     -S-  -R-',C_NL
	DB    'output',C_NL
	DB    'Sig. no 1234 56',C_NL
	DB    'Zero on',C_NL
	DB    'Mark on',0

UT_SF13:DB    -1
	%W    (UT_SF1)

UT_U1301:
	DS    UT_U1301+OU_VEVJ-$
	DB    2
	DW    BFL_EV_NR
	DS    UT_U1301+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1301+OU_X-$
	DB    8,1,4,1
	DS    UT_U1301+OU_HLP-$
	%W    (0)
	DS    UT_U1301+OU_SFT-$
	%W    (UT_SFAUX1)
	DS    UT_U1301+OU_A_RD-$
	%W    (G_AUX)		; A_RD
	%W    (S_AUX)		; A_WR
	DS    UT_U1301+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U1301+OU_BF_S-$
	%W    (0)
	DS    UT_U1301+OU_BF_P-$
	%W    (0)
	DS    UT_U1301+OU_BF_F-$
	%W    (0)
	DS    UT_U1301+OU_BF_T-$
	DB    0
	DB    '_',0
	DB    '1',0
	DB    1
	DB    '_',0
	DB    '2',0
	DB    2
	DB    '_',0
	DB    '3',0
	DB    3
	DB    '_',0
	DB    '4',0
	DB    -1

UT_SFAUX2:
	DB    K_5
	%W    (1 SHL 4)
	%W    (BFL_3ST)

	DB    K_6
	%W    (1 SHL 5)
	%W    (BFL_3ST)

UT_SFAUX1:
	DB    K_1
	%W    (1 SHL 0)
	%W    (BFL_XOR)

	DB    K_2
	%W    (1 SHL 1)
	%W    (BFL_XOR)

	DB    K_3
	%W    (1 SHL 2)
	%W    (BFL_XOR)

	DB    K_4
	%W    (1 SHL 3)
	%W    (BFL_XOR)

	DB    0

UT_U1302:
	DS    UT_U1302+OU_VEVJ-$
	DB    2
	DW    BFL_EV
	DS    UT_U1302+OU_MSK-$
	DB    0
	DS    UT_U1302+OU_X-$
	DB    13,1,3,1
	DS    UT_U1302+OU_HLP-$
	%W    (0)
	DS    UT_U1302+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1302+OU_A_RD-$
	%W    (G_AUX)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1302+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U1302+OU_BF_S-$
	%W    (0)
	DS    UT_U1302+OU_BF_P-$
	%W    (0)
	DS    UT_U1302+OU_BF_F-$
	%W    (0)
	DS    UT_U1302+OU_BF_T-$
	DB    4
	DB    '_',0
	DB    '5',0
	DB    5
	DB    '_',0
	DB    '6',0
	DB    -1

UT_U1303:
	DS    UT_U1303+OU_VEVJ-$
	DB    2
	DW    BFL_EV
	DS    UT_U1303+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1303+OU_X-$
	DB    8,3,7,1
	DS    UT_U1303+OU_HLP-$
	%W    (0)
	DS    UT_U1303+OU_SFT-$
	%W    (UT_SFAUX3)
	DS    UT_U1303+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1303+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (ZR_MASK)		; DP
	DS    UT_U1303+OU_BF_S-$
	%W    (0)
	DS    UT_U1303+OU_BF_P-$
	%W    (0)
	DS    UT_U1303+OU_BF_F-$
	%W    (0)
	DS    UT_U1303+OU_BF_T-$
	DB    8+0
	DB    '',0
	DB    'H',C_BS,0
	DB    0
	DB    'x',0
	DB    'L',0
	DB    8+1
	DB    '',0
	DB    'H',C_BS,0
	DB    1
	DB    'x',0
	DB    'L',0
	DB    8+2
	DB    '',0
	DB    'H',C_BS,0
	DB    2
	DB    'x',0
	DB    'L',0
	DB    8+3
	DB    '',0
	DB    'H',C_BS,0
	DB    3
	DB    'x',0
	DB    'L',0
	DB    8+4
	DB    '',0
	DB    ' H',C_BS,0
	DB    4
	DB    ' x',0
	DB    ' L',0
	DB    8+5
	DB    '',0
	DB    'H',C_BS,0
	DB    5
	DB    'x',0
	DB    'L',0
	DB    -1

UT_U1304:
	DS    UT_U1304+OU_VEVJ-$
	DB    2
	DW    BFL_EV
	DS    UT_U1304+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1304+OU_X-$
	DB    8,4,7,1
	DS    UT_U1304+OU_HLP-$
	%W    (0)
	DS    UT_U1304+OU_SFT-$
	%W    (UT_SFAUX3)
	DS    UT_U1304+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1304+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (LM_MASK)		; DP
	DS    UT_U1304+OU_BF_S-$
	%W    (0)
	DS    UT_U1304+OU_BF_P-$
	%W    (0)
	DS    UT_U1304+OU_BF_F-$
	%W    (0)
	DS    UT_U1304+OU_BF_T-$
	DB    8+0
	DB    '',0
	DB    'H',C_BS,0
	DB    0
	DB    'x',0
	DB    'L',0
	DB    8+1
	DB    '',0
	DB    'H',C_BS,0
	DB    1
	DB    'x',0
	DB    'L',0
	DB    8+2
	DB    '',0
	DB    'H',C_BS,0
	DB    2
	DB    'x',0
	DB    'L',0
	DB    8+3
	DB    '',0
	DB    'H',C_BS,0
	DB    3
	DB    'x',0
	DB    'L',0
	DB    8+4
	DB    '',0
	DB    ' H',C_BS,0
	DB    4
	DB    ' x',0
	DB    ' L',0
	DB    8+5
	DB    '',0
	DB    'H',C_BS,0
	DB    5
	DB    'x',0
	DB    'L',0
	DB    -1

UT_SFAUX3:
	DB    K_1
	%W    (1 SHL 0)
	%W    (BFL_3HL)

	DB    K_2
	%W    (1 SHL 1)
	%W    (BFL_3HL)

	DB    K_3
	%W    (1 SHL 2)
	%W    (BFL_3HL)

	DB    K_4
	%W    (1 SHL 3)
	%W    (BFL_3HL)

	DB    K_5
	%W    (1 SHL 4)
	%W    (BFL_3HL)

	DB    K_6
	%W    (1 SHL 5)
	%W    (BFL_3HL)

	DB    0


; ---------------------------------
; Preamp test

UT_GR18:DS    UT_GR18+OGR_VEVJ-$
	DB    2
	DW    GR_EV_PRMT
	DS    UT_GR18+OGR_BTXT-$
	%W    (UT_GT18)
	DS    UT_GR18+OGR_STXT-$
	%W    (0)
	DS    UT_GR18+OGR_HLP-$
	%W    (0)
	DS    UT_GR18+OGR_SFT-$
	%W    (UT_SF18)
	DS    UT_GR18+OGR_PU-$
	%W    (UT_U1800)
	%W    (UT_U1801)
	%W    (UT_U1802)
	%W    (UT_U1803)
	%W    (UT_U1804)
	%W    (UT_U1805)
	%W    (UT_U1810)
	%W    (UT_U1811)
	%W    (UT_U1812)
	%W    (UT_U1890)
	%W    (0)

UT_GT18:DB    'PreampTest',C_NL
	DB    C_NL
	DB    CG_LAM,' +++nm  ',CG_TAU,' +.++s',C_NL
	DB    'Wlen Offs',C_NL
	DB    'Offs',C_NL
	DB    'Ref ADC',0

UT_SF18:DB    -1
	%W    (UT_SF1)

UT_U1800:	; Exit
	DS    UT_U1800+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U1800+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1800+OU_X-$
	DB    12,0,4,1
	DS    UT_U1800+OU_HLP-$
	%W    (0)
	DS    UT_U1800+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U1800+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U1800+OU_B_P-$
	%W    (UT_GR11)		; R23
	DS    UT_U1800+OU_B_F-$
	%W    (GR_RQ23_ABS)	; Funkce
	DS    UT_U1800+OU_B_T-$
	DB    'Exit',0

UT_U1801:	; Merena velicina
	DS    UT_U1801+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1801+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1801+OU_X-$
	DB    0,1,3,1
	DS    UT_U1801+OU_HLP-$
	%W    (0)
	DS    UT_U1801+OU_SFT-$
	%W    (0)
	DS    UT_U1801+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1801+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (XAD_MOD)		; DP
	DS    UT_U1801+OU_M_S-$
	%W    (0)
	DS    UT_U1801+OU_M_P-$
	%W    (0)
	DS    UT_U1801+OU_M_F-$
	%W    (XAD_CHMOD)
	DS    UT_U1801+OU_M_T-$
	%W    (0FFH)
	%W    (000H)
	%W    (UT_U1101T0)
	%W    (0FFH)
	%W    (001H)
	%W    (UT_U1101T1)
	%W    (0FFH)
	%W    (002H)
	%W    (UT_U1101T2)
	%W    (0FFH)
	%W    (003H)
	%W    (UT_U1101T3)
	%W    (0FFH)
	%W    (004H)
	%W    (UT_U1101T4)
	%W    (0)
	; Vyuziva texty z UT_U1101

UT_U1802:	; Absorbance/vystup
	DS    UT_U1802+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1802+OU_MSK-$
	DB    0
	DS    UT_U1802+OU_X-$
	DB    4,1,9,1
	DS    UT_U1802+OU_HLP-$
	%W    (0)
	DS    UT_U1802+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1802+OU_A_RD-$
	%W    (G_ABSl)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1802+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U1802+OU_I_F-$
	DB    0C6H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1803:	; Stav
	DS    UT_U1803+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U1803+OU_MSK-$
	DB    0
	DS    UT_U1803+OU_X-$
	DB    14,1,2,1
	DS    UT_U1803+OU_HLP-$
	%W    (0)
	DS    UT_U1803+OU_SFT-$
	%W    (0)
	DS    UT_U1803+OU_A_RD-$
	%W    (NULL_A)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1803+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U1803+OU_M_S-$
	%W    (0)
	DS    UT_U1803+OU_M_P-$
	%W    (0)
	DS    UT_U1803+OU_M_F-$
	%W    (0)
	DS    UT_U1803+OU_M_T-$
	%W    (0FF00H)
	%W    (100H)
	%W    (UT_U1103TR)
	%W    (0FFFFH)
	%W    (0)
	%W    (UT_U1103T0)
	%W    (08000H)
	%W    (08000H)
	%W    (UT_U1103TE)
	%W    (0)
	; Vyuziva texty z UT_U1103

UT_U1804:	; Vlnova delka
	DS    UT_U1804+OU_VEVJ-$
	DB    2
	DW    UIN_MUT_EV
	DS    UT_U1804+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1804+OU_X-$
	DB    2,2,3,1
	DS    UT_U1804+OU_HLP-$
	%W    (0)
	DS    UT_U1804+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1804+OU_A_RD-$
	%W    (UR_WLEN)		; A_RD
	%W    (UW_WLEN)		; A_WR
	DS    UT_U1804+OU_DPSI-$
	%W    (0)		; DPSI
	%W    (WLEN)		; DP
	DS    UT_U1804+OU_I_F-$
	DB    080H		; format I_F
	%W    (-10)		; I_L
	%W    (700)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru
	DS    UT_U1804+OU_IMUT_T-$
	%W    (0FFFFH)
	%W    (080FFH)
	%W    (UT_U1104T2)
	%W    (0FF00H)
	%W    (08000H)
	%W    (UT_U1104T1)
	%W    (0)
	; Vyuziva texty z UT_U1104

UT_U1805:	; Casova konstanta
	DS    UT_U1805+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1805+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1805+OU_X-$
	DB    11,2,4,1
	DS    UT_U1805+OU_HLP-$
	%W    (0)
	DS    UT_U1805+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1805+OU_A_RD-$
	%W    (G_TIMCO)		; A_RD
	%W    (S_TIMCO)		; A_WR
	DS    UT_U1805+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U1805+OU_I_F-$
	DB    02H		; format I_F
	%W    (4)		; I_L
	%W    (FI_MLEN*4)	; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1810:	; Offset vlnove delky
	DS    UT_U1810+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1810+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1810+OU_X-$
	DB    10,3,5,1
	DS    UT_U1810+OU_HLP-$
	%W    (0)
	DS    UT_U1810+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1810+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_WOFS)		; A_WR
	DS    UT_U1810+OU_DPSI-$
	%W    (0)		; DPSI
	%W    (WL_OFS)		; DP
	DS    UT_U1810+OU_I_F-$
	DB    080H		; format I_F
	%W    (-7FFFH)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1811:	; Offset prevodniku XAD_OFS
	DS    UT_U1811+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1811+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1811+OU_X-$
	DB    6,4,9,1
	DS    UT_U1811+OU_HLP-$
	%W    (0)
	DS    UT_U1811+OU_SFT-$
	%W    (UT_SF1811)
	DS    UT_U1811+OU_A_RD-$
	%W    (G_XADOl)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1811+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U1811+OU_I_F-$
	DB    0C6H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_SF1811:
	DB    K_ZERO
	DB    0,0
	%W    (ZER_XADO)

	DB    0

UT_U1812:	; Prime mereni reference
	DS    UT_U1812+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1812+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1812+OU_X-$
	DB    10,5,5,1
	DS    UT_U1812+OU_HLP-$
	%W    (0)
	DS    UT_U1812+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1812+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1812+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (LS_ADRF)		; DP
	DS    UT_U1812+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru


UT_U1890:	; Ulozeni do EEPROM
	DS    UT_U1890+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U1890+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1890+OU_X-$
	DB    0,14,12,1
	DS    UT_U1890+OU_HLP-$
	%W    (0)
	DS    UT_U1890+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U1890+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U1890+OU_B_P-$
	%W    (EEC_SER)		; Servisni nastaveni
	DS    UT_U1890+OU_B_F-$
	%W    (EEP_WRS)		; Zapis do EEPROM
	DS    UT_U1890+OU_B_T-$
	DB    'Save service',0

; *******************************************************************
; Mode menu

UT_GR70:DS    UT_GR70+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR70+OGR_BTXT-$
	%W    (UT_GT70)
	DS    UT_GR70+OGR_STXT-$
	%W    (0)		; UT_GS70
	DS    UT_GR70+OGR_HLP-$
	%W    (0)
	DS    UT_GR70+OGR_SFT-$
	%W    (UT_SF70)
	DS    UT_GR70+OGR_PU-$
	%W    (UT_U7001)
	%W    (UT_U7002)
	%W    (0)

UT_GT70:DB    '  Mode menu',0

UT_SF70:DB    -1
	%W    (UT_SF1)

UT_U7001:
	DS    UT_U7001+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U7001+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7001+OU_X-$
	DB    0,1,13,1
	DS    UT_U7001+OU_HLP-$
	%W    (0)
	DS    UT_U7001+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U7001+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U7001+OU_B_P-$
	%W    (EEC_SER)		; Uzivatelske nastaveni
	DS    UT_U7001+OU_B_F-$
	%W    (EEP_WRS)		; Zapis do EEPROM
	DS    UT_U7001+OU_B_T-$
	DB    'Save settings',0

UT_U7002:
	DS    UT_U7002+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U7002+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7002+OU_X-$
	DB    0,2,12,1
	DS    UT_U7002+OU_HLP-$
	%W    (0)
	DS    UT_U7002+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U7002+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U7002+OU_B_P-$
	%W    (UT_GR72)
	DS    UT_U7002+OU_B_F-$
	%W    (GR_RQ23)
	DS    UT_U7002+OU_B_T-$
	DB    'Comunication',0

; ---------------------------------
; Nastaveni parametru komunikace
UT_GR72:DS    UT_GR72+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR72+OGR_BTXT-$
	%W    (UT_GT72)
	DS    UT_GR72+OGR_STXT-$
	%W    (0)		; UT_GS72
	DS    UT_GR72+OGR_HLP-$
	%W    (0)
	DS    UT_GR72+OGR_SFT-$
	%W    (UT_SF72)
	DS    UT_GR72+OGR_PU-$
	%W    (UT_U7202)
	%W    (UT_U7203)
	%W    (UT_U7204)
	%W    (0)

UT_GT72:DB    'Adr kBd Mode Grp',0

UT_SF72:DB    -1
	%W    (UT_SF1)

UT_U7202:
	DS    UT_U7202+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U7202+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7202+OU_X-$
	DB    0,1,2,1
	DS    UT_U7202+OU_HLP-$
	%W    (0)
	DS    UT_U7202+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U7202+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_COMi)	; A_WR
	DS    UT_U7202+OU_DPSI-$
	%W    (0)		; DPSI
	%W    (COM_ADR)		; DP
	DS    UT_U7202+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (99)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U7203:
	DS    UT_U7203+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U7203+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7203+OU_X-$
	DB    3,1,5,1
	DS    UT_U7203+OU_HLP-$
	%W    (0)
	DS    UT_U7203+OU_SFT-$
	%W    (UT_SF7203)
	DS    UT_U7203+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_COMi)		; A_WR
	DS    UT_U7203+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (COM_SPD)		; DP
	DS    UT_U7203+OU_M_S-$
	%W    (0)
	DS    UT_U7203+OU_M_P-$
	DB    1,4+080H
	DS    UT_U7203+OU_M_F-$
	%W    (MUT_AD23)
	DS    UT_U7203+OU_M_T-$
	%W    (0FFFFH)
	%W    (00000H)
	%W    (UT_U7203T0)
	%W    (0FFFFH)
	%W    (00001H)
	%W    (UT_U7203T1)
	%W    (0FFFFH)
	%W    (00002H)
	%W    (UT_U7203T2)
	%W    (0FFFFH)
	%W    (00003H)
	%W    (UT_U7203T3)
	%W    (0)
UT_U7203T0:DB    ' 2400',0
UT_U7203T1:DB    ' 4800',0
UT_U7203T2:DB    ' 9600',0
UT_U7203T3:DB    '19200',0

UT_SF7203:
	DB    K_9
	%W    (2)
	%W    (COM_WR23)

	DB    K_1
	%W    (3)
	%W    (COM_WR23)

	DB    0

UT_U7204:
	DS    UT_U7204+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U7204+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U7204+OU_X-$
	DB    13,1,2,1
	DS    UT_U7204+OU_HLP-$
	%W    (0)
	DS    UT_U7204+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U7204+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_COMi)	; A_WR
	DS    UT_U7204+OU_DPSI-$
	%W    (0)		; DPSI
	%W    (COM_GRP)		; DP
	DS    UT_U7204+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (99)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru


	END
