;********************************************************************
;*         Spektrofotometricky detektor - DS_SM.ASM                 *
;*               Rizeni krokoveho motoru - ATMEL 89C2051            *
;*                  Stav ke dni 10.10.1997                          *
;*                      (C) Pisoft 1997                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

; motor pripojen
; 1 vinuti   proud P1.2, P1.3 znameko P1.4
; 2 vinuti   proud P1.5, P1.6 znameko P1.7

M_STP	EQU   11111100B

; ohlasy kotouce
; P1.0, P1.1, P3.7, P3.3/INT1

; komunikace
; P3.0/RxD, P3.1/TxD, P3.2/INT0, P3.4

%*DEFINE (W (WO)) (
	DB   LOW (%WO),HIGH (%WO)
)

%*DEFINE (LDR45i (WO)) (
	MOV  R4,#LOW  (%WO)
	MOV  R5,#HIGH (%WO)
)

%DEFINE	(SC_BSY_IFO) (1)

DS_SM_C SEGMENT CODE
DS_SM_D SEGMENT DATA
DS_SM_B SEGMENT DATA BITADDRESSABLE

STACK	DATA  60H

CSEG	AT    RESET
	JMP   RES_START

CSEG	AT    TIMER0
	JMP   I_TIME

RSEG	DS_SM_B
HW_FLG:	DS    1		; Priznaky
FL_BSY	BIT   HW_FLG.7	; Motor v pohybu
FL_ERR	BIT   HW_FLG.6	; Chyba
FL_BSY1	BIT   HW_FLG.5	; Konec pohybu pro kalibraci

RSEG	DS_SM_C

RES_START:
	MOV   IE,  #00000011B ; ET0 EX0
;	MOV   IP,  #00000001B ; PX0
	MOV   IP,  #00000000B ;
	MOV   TMOD,#00100001B ; 16 bit timer 0;
	MOV   TCON,#00000101B ; interapy hranou
	MOV   SP,#STACK
	CLR   A
	MOV   HW_FLG,A
	MOV   SC_ST,A
	MOV   SC_FLG,A

	MOV   STP_FRC,#00H
	MOV   STP_FRC+1,#00H
	MOV   STP_PHA,#00H
	MOV   STP_SPD,#00H
	MOV   STP_RP,#00H
	MOV   STP_RP+1,#00H
	MOV   STP_AP,#00H
	MOV   STP_AP+1,#00H
	MOV   R4,#5		; maximalni rychlost < STP_PRN
	CALL  STP_SSPM
	MOV   WL_CPHA,#0	; referencni faze
	MOV   WL_CST,#1		; priznak kalibrace
	MOV   WL_NMRK,#0
	MOV   WL_NMRK+1,#0
	SETB  TR0

;	CALL  STP_T

	SETB  EA

L1:     CALL  WL_TST		; Kontrola
	JMP   L1

	%LDR45i(-400H)
	CLR   EA
	MOV   STP_RP,R4
	MOV   STP_RP+1,R5
	SETB  EA
	JMP   L1


STP_T:	CALL  STP_GEO
	INC   STP_PHA
	MOV   A,STP_PHA
	CJNE  A,#STP_TAN,STP_T2
	MOV   STP_PHA,#0
STP_T2: JNB   P3.4,STP_TR
	JB    P3.7,STP_T2
STP_T3: JNB   P3.4,STP_TR
	JB    P3.3,STP_T3
	XRL   P3,#2
	JMP   STP_T
STP_TR:	RET


; *******************************************************************
; Rizeni krokace

RSEG	DS_SM_D

STP_RP:	DS    2		; pozadovana poloha
STP_AP:	DS    2		; aktualni poloha
STP_SPD:DS    1		; rychlost
STP_SPM:DS    1		; maximalni rychlost
STP_PHA:DS    1		; faze krokace

RSEG	DS_SM_C



; Krokuje dokud STP_AP rovno STP_RP
; =================================
;stara se o rychlosti rozjezdu a brzdeni STP_SPD a STP_FRC
;vraci: R2 .. STP_AP
;	ACC = 0 .. konec pohybu
;rusi:	R1
STP_GS: CLR   C
	MOV   A,STP_RP
	SUBB  A,STP_AP
	MOV   R1,A
	MOV   A,STP_RP+1
	SUBB  A,STP_AP+1	; STP_AP-STP_RP ?
	JB    ACC.7,STP_G16
STP_G10:JNZ   STP_G12
	MOV   A,R1
	JNZ   STP_G14
	MOV   A,STP_SPD		; Chceme zastavit
	JB    ACC.7,STP_G1I
	JNZ   STP_G1D
	RET
; potreba pohybu nahoru
STP_G12:MOV   R1,#0FFH
STP_G14:MOV   A,STP_SPD
	JB    ACC.7,STP_G1I	; STP_SPD<0?  STP_SPD++
	CLR   C
	SUBB  A,R1
	JZ    STP_G20		; STP_SPD=R1?  STP_SPD
	JNC   STP_G1D		; STP_SPD>R1?  STP_SPD--
	MOV   A,STP_SPD
	CLR   C
	SUBB  A,STP_SPM
	JNC   STP_G20		; STP_SPD>=STP_SPM? STP_SPD
STP_G1I:INC   STP_SPD		; STP_SPD++
	SJMP  STP_G20
; potreba pohybu dolu
STP_G16:INC   A
	JZ    STP_G18
	MOV   R1,#000H
STP_G18:MOV   A,STP_SPD
	JNB   ACC.7,STP_G1D	; STP_SPD>=0?  STP_SPD--
	CLR   C
	SUBB  A,R1
	JZ    STP_G20		; STP_SPD=R1?  STP_SPD
	JC    STP_G1I		; STP_SPD<R1?  STP_SPD++
	MOV   A,STP_SPD
	DEC   A
	ADD   A,STP_SPM
	JNC   STP_G20		; STP_SPD<=-STP_SPM?  STP_SPD
STP_G1D:DEC   STP_SPD		; STP_SPD--
; Generovani polohy
STP_G20:MOV   A,STP_SPD
	JZ    STP_G30
	JB    ACC.7,STP_G25
; Pohyb nahoru
	INC   STP_PHA
	MOV   A,STP_PHA
	CJNE  A,#STP_TAN,STP_G22
	MOV   STP_PHA,#0
STP_G22:INC   STP_AP		; STP_AP+=1
	MOV   A,STP_AP
	JNZ   STP_G30
	INC   STP_AP+1
	SJMP  STP_G30
; Pohyb dolu
STP_G25:MOV   A,STP_PHA
	JNZ   STP_G27
	MOV   STP_PHA,#STP_TAN
STP_G27:DEC   STP_PHA
	MOV   A,STP_AP		; STP_AP-=1
	DEC   STP_AP
	JNZ   STP_G30
	DEC   STP_AP+1
; Nastaveni rychlosti
STP_G30:MOV   A,STP_SPD
	JNB   ACC.7,STP_G33
	CPL   A
	INC   A
STP_G33:RL    A
	MOV   R1,A
	ADD   A,#STP_PRT-STP_Gb1
	MOVC  A,@A+PC
STP_Gb1:MOV   STP_FRC,A
	MOV   A,R1
	ADD   A,#STP_PRT-STP_Gb2+1
	MOVC  A,@A+PC
STP_Gb2:MOV   STP_FRC+1,A
STP_GEO:MOV   A,STP_PHA
STP_GEN:ADD   A,#STP_TAB-STP_Gb3
	MOVC  A,@A+PC
STP_Gb3:ORL   P1,#M_STP
	XRL   P1,A
STP_GSR:RET

;	      PIIPII
;	      H10H10
;             222111
STP_TAB:DB    11100000B
	DB    11000100B
	DB    10101000B
	DB    00001100B
	DB    00101000B
	DB    01000100B
	DB    01110000B
	DB    01010100B
	DB    00111000B
	DB    10011100B
	DB    10111000B
	DB    11010100B
STP_TAN	SET   $-STP_TAB

STP_PRT:
	%W    (10000)
	%W    (5000)
	%W    (2500)
	%W    (1667)
	%W    (1250)
	%W    (1000)
	%W    (833)
	%W    (714)
	%W    (625)
	%W    (556)
	%W    (500)	; zlom
	%W    (476)
	%W    (455)
	%W    (435)
	%W    (417)
	%W    (400)
	%W    (385)
	%W    (370)
	%W    (357)
	%W    (345)
	%W    (333)
	%W    (323)
	%W    (313)
	%W    (303)
STP_PRN	SET   ($-STP_PRT)/2

; *******************************************************************
; Kalibrace polohy a hlidani ohlasu

RSEG	DS_SM_D

WL_ROT	EQU   600	; pocet impulsu na otacku

WL_CST:	DS    1		; stav kalibrace polohy
WL_CPHA:DS    1		; faze krokace pro hledani znacky
WL_NMRK:DS    2		; nejblizsi oblast znacky

RSEG	DS_SM_C

; Cte ACC poloha ze snimace, CY=1 znacka
WL_OSR:	MOV   A,P1
	ANL   A,#3
	MOV   C,P3.7
	MOV   ACC.2,C
	MOV   C,P3.3
	CPL   C
	RET

WL_MOV:	CALL  WL_OSR
	MOV   R0,WL_CST
	INC   R0
	DJNZ  R0,WL_C10
				; Stav 0
	JMP   STP_GS
WL_C10:	DJNZ  R0,WL_C20
	JB    FL_BSY1,WL_C98	; Stav 1
	CJNE  A,#7,WL_C95
	%LDR45i(-WL_ROT)     	; ze 700nm o otocku zpet
	SJMP  WL_C90
WL_C20:	DJNZ  R0,WL_C30
	JB    FL_BSY1,WL_C98	; Stav 2
	%LDR45i((3*WL_ROT)/2)	; 1.5 otacky vpred
	SJMP  WL_C90
WL_C30:	DJNZ  R0,WL_C40
	JNB   FL_BSY1,WL_C3E	; Stav 3
	JC    WL_C98		; jet mimo znacku
	SJMP  WL_C95
WL_C3E:	SETB  FL_ERR
	MOV   WL_CST,#0
	SJMP  WL_C98
WL_C40:	DJNZ  R0,WL_C50
	JNB   FL_BSY1,WL_C3E	; Stav 3
	JNC   WL_C98		; najit zacatek  znacky
	MOV   R7,A
	MOV   A,STP_PHA
	CJNE  A,WL_CPHA,WL_C98
	MOV   A,#LOW  WL_ROT
	MOV   B,R7
	MUL   AB
	MOV   R4,A
	MOV   R5,B
	MOV   A,#HIGH WL_ROT
	MOV   B,R7
	MUL   AB
	ADD   A,R5
	MOV   R5,A
	MOV   STP_RP,R4
	MOV   STP_RP+1,R5
	MOV   STP_AP,R4
	MOV   STP_AP+1,R5
	SJMP  WL_C95
WL_C50:	JNC   WL_C3E
	MOV   WL_CST,#0
	SJMP  WL_C98
WL_C90:	MOV   A,STP_RP		; Reletivni pohyb o R45
	ADD   A,R4
	MOV   STP_RP,A
	MOV   A,STP_RP+1
	ADDC  A,R5
	MOV   STP_RP+1,A
	SETB  FL_BSY1
WL_C95:	INC   WL_CST		; Prechod na dalsi stav
WL_C98: CALL  STP_GS
	JNZ   WL_C99
	CLR   FL_BSY1
WL_C99:	MOV   A,#1
	RET

; Kontrola spravnosti ohlasu
WL_TST:	MOV   A,WL_CST
	JNZ   WL_T99
WL_T10:	MOV   R4,STP_AP
	MOV   R5,STP_AP+1
	CALL  WL_OSR
	MOV   ACC.7,C
	MOV   R1,A
	MOV   A,R4
	CJNE  A,STP_AP,WL_T10
	MOV   A,R5
	CJNE  A,STP_AP+1,WL_T10
	MOV   A,R1
	JBC   ACC.7,WL_T70
	CLR   C
	MOV   A,R4
	SUBB  A,WL_NMRK
	ANL   A,#NOT 1
	MOV   R1,A
	MOV   A,R5
	SUBB  A,WL_NMRK+1
	MOV   C,ACC.7
	ORL   A,R1
	JZ    WL_T95
	JC    WL_T45
	MOV   A,#LOW  WL_ROT
	ADD   A,WL_NMRK
	MOV   WL_NMRK,A
	MOV   A,#HIGH WL_ROT
	ADDC  A,WL_NMRK+1
	MOV   WL_NMRK+1,A
	SJMP  WL_T46
WL_T45:	CLR   C
	MOV   A,#LOW  WL_ROT
	SUBB  A,WL_NMRK
	MOV   WL_NMRK,A
	MOV   A,#HIGH WL_ROT
	SUBB  A,WL_NMRK+1
	MOV   WL_NMRK+1,A
WL_T46:	SJMP  WL_T99
WL_T70: MOV   R1,A
	MOV   B,#LOW  WL_ROT
	MUL   AB
	MOV   R6,A
	MOV   R7,B
	MOV   A,#HIGH WL_ROT
	MOV   B,R1
	MUL   AB
	ADD   A,R7
	MOV   R7,A
	CLR   C
	MOV   A,R6
	SUBB  A,R4
	MOV   R6,A
	MOV   A,R7
	SUBB  A,R5
	MOV   R7,A
	JNB   ACC.7,WL_T73
	CLR   C
	CLR   A
	SUBB  A,R6
	MOV   R6,A
	CLR   A
	SUBB  A,R7
	MOV   R7,A
WL_T73: JNZ   WL_T95
	MOV   A,R6
	ADD   A,#-11
	JNC   WL_T99
WL_T95:	CLR   EX0
	MOV   A,WL_CST
	JNZ   WL_T99
	SETB  FL_ERR
WL_T99:	SETB  EX0
	RET

; *******************************************************************
; Casove preruseni

USING   1

RSEG	DS_SM_D

TIM_DEL:DS    2
STP_FRC SET   TIM_DEL   ; delicka frekvence

RSEG	DS_SM_C

I_TIME: PUSH  ACC
	PUSH  PSW
	PUSH  B
	MOV   PSW,#AR0

	CALL  WL_MOV	; STP_GS
	JNZ   I_TIM20
	CLR   FL_BSY
%IF (%SC_BSY_IFO) THEN (
	JB    FLS_BSY,I_TIM18
	CLR   SC_DO
I_TIM18:
)FI
I_TIM20:

	CLR   TF0
	CLR   C
	CLR   TR0
S_T0_D  SET   7               ; zpozdeni rutiny
	MOV   A,TL0           ; 7 Cyklu
	SUBB  A,TIM_DEL
	MOV   TL0,A
	MOV   A,TH0
	SUBB  A,TIM_DEL+1
	MOV   TH0,A
	SETB  TR0
	POP   B
	POP   PSW
	POP   ACC
	RETI

; *******************************************************************
; Komunikace

USING   2

; Vystupni hodiny staticke
%DEFINE (SC_CO_STAT) (1)

; Pouzite vyvody
SC_DI	BIT   P3.0	; RxD
SC_CI	BIT   P3.2	; INT0
SC_DO	BIT   P3.1	; TxD
SC_CO	BIT   P3.4

RSEG	DS_SM_B
SC_FLG:	DS    1		; Priznaky komunikace
FLS_SONY BIT  SC_FLG.7	; Pouze vysilat a neprijimat
FLS_BSY BIT   SC_FLG.6	; Probiha komunikace

;Vyuziti registru
; R3	stav/cislo bitu/citac bytu
; R0	prijimana/vysilana data+pomocny reg
;pro prenos bloku
; R1	adresa
; R2	prikaz
; R4567	lze vyuzit pro ulozeni dat


SC_ST	DATA  AR3

CSEG	AT    EXTI0
	JMP   I_SC

RSEG	DS_SM_C

I_SC:	CLR   IE0
	PUSH  ACC
	PUSH  PSW
	MOV   PSW,#AR0

      %IF(NOT %SC_CO_STAT) THEN (
	CLR   SC_CO
      )FI

	MOV   A,R3
	SWAP  A
	ANL   A,#0FH
	JNZ   I_SC10
	CALL  SC_DSH	; === stav 0 - cekat
	JC    I_SC90
	SETB  FLS_BSY
	MOV   R3,#18H	; prijmout prikaz
	MOV   R0,HW_FLG	; vyslat stav
	CLR   SC_DO
	SJMP  I_SCRET
I_SC10:	DJNZ  ACC,I_SC20
	CALL  SC_DSH	; === stav 1 - prijmout prikaz
	JC    I_SCERR	; spatne zakonceni => chyba
	JZ    I_SCRET   ; pokracovat v prenosu
	MOV   R2,AR0
	MOV   R1,#AR4
	CLR   FLS_SONY
	CALL  SC_FNV	; volani podle prikazu v R2
	JB    ACC.7,I_SCERR
	JZ    I_SCEND
	SWAP  A
	ADD   A,#28H	; prijmout/vyslat data
	MOV   R3,A
	MOV   A,@R1	; 1. vysilany byte
	MOV   R0,A
	CLR   SC_DO
	SJMP  I_SCRET
I_SC20:	CALL  SC_DSH	; === stav 2..14 - prijimat/vysilat data
	JC    I_SCERR	; spatne zakonceni => chyba
	JZ    I_SCRET   ; pokracovat v prenosu
	JB    FLS_SONY,I_SC21
	MOV   A,R0
	MOV   @R1,A
I_SC21:	INC   R1
	MOV   A,@R1
	MOV   R0,A
	CLR   SC_DO
	MOV   A,R3
	ADD   A,#-8
	MOV   R3,A
	CJNE  A,#28H,I_SCRET; dalsi byte

	MOV   R1,#AR4
	CALL  SC_FNV	; volani podle prikazu v R2
	JB    ACC.7,I_SCERR

I_SCEND:MOV   R3,#0
	CLR   SC_DO
	SJMP  I_SCRET

I_SC30:	SJMP  I_SCERR

I_SC90:	CLR   FLS_BSY	; Iddle
%IF (%SC_BSY_IFO) THEN (
	MOV   C,FL_BSY	; Informace o busy
	MOV   SC_DO,C
)FI
I_SCRET:
      %IF(NOT %SC_CO_STAT) THEN (
	SETB  SC_CO
      )ELSE(
	XRL   SC_CO AND 0F8H ,#1 SHL (SC_CO AND 07H)
      )FI
	POP   PSW
	POP   ACC
	RETI

I_SCERR:MOV   R3,#0
	SETB  SC_DO
	SJMP  I_SCRET

; Vysila postupne bity z R0, citac v R3
; na zacatek je treba nastavit R3=X8
SC_DSH:	CLR   A
	MOV   C,SC_DI
	ADDC  A,#0
	MOV   C,SC_DI
	ADDC  A,#0
	MOV   C,SC_DI
	ADDC  A,#0
	MOV   C,ACC.1
	MOV   A,R3
	ANL   A,#0FH
	JNZ   SC_DSH5
	MOV   A,#1	; zakonceni bytu
	RET
SC_DSH5:MOV   A,R0
	RRC   A
	MOV   R0,A
	MOV   SC_DO,C
	DEC   R3	; citani bitu
	CLR   A
	CLR   C
	RET

; vola funkci podle prikazu v R2
; R3=10H pro prvni volavi
; R3=28H pro druhe
SC_FNV:	MOV   A,R2
	ADD   A,#-SC_FNN
	JC    SC_FNE
	MOV   A,R2
	RL    A
	ADD   A,#SC_FNT-SC_FNb1
	MOVC  A,@A+PC
SC_FNb1:PUSH  ACC
	MOV   A,R2
	RL    A
	ADD   A,#SC_FNT-SC_FNb2+1
	MOVC  A,@A+PC
SC_FNb2:PUSH  ACC
	RET

SC_FNE:	MOV   A,#-1
	RET

SC_FNT:	%W    (SC_F0)
	%W    (SC_F1)
	%W    (SC_F2)
	%W    (SC_F3)
	%W    (SC_F4)
SC_FNN	SET   ($-SC_FNT)/2

; ---------------------------
; jednotlive funkce

; Identifikace
SC_F0:	MOV   R4,#'D'
	MOV   R5,#'S'
	MOV   R6,#'0'
	MOV   R7,#'1'
	MOV   A,#4
	RET

; Stav
SC_F1:	MOV   R4,STP_AP
	MOV   R5,STP_AP+1
	;MOV   R6,STP_SPD
	CALL  WL_OSR
	MOV   ACC.7,C
	MOV   R6,A
	MOV   A,HW_FLG
	ANL   A,#0C0H
	ORL   A,STP_PHA
	MOV   R7,A
	MOV   A,#4
	RET

; Najeti na polohu
SC_F2:  CJNE  R3,#10H,SC_F210
	MOV   A,#2
	MOV   R4,STP_AP
	MOV   R5,STP_AP+1
	RET
SC_F210:MOV   STP_RP,R4
	MOV   STP_RP+1,R5
	SETB  FL_BSY
	CLR   A
	RET

; Spusteni kalibrace
SC_F3:  CJNE  R3,#10H,SC_F310
	MOV   A,#2
	RET
SC_F310:MOV   WL_CPHA,R4	; referencni faze
	MOV   WL_CST,#1
	CLR   FL_ERR
	SETB  FL_BSY
	CLR   A
	RET

; Nastaveni max rychlosti
SC_F4:  CJNE  R3,#10H,SC_F410
	MOV   R4,#STP_PRN-1
	MOV   A,#2
	RET
STP_SSPM:			; nastavi maximalni
SC_F410:MOV   A,R4		; rychlost podle R4
	ADD   A,#-STP_PRN
	JNC   SC_F420
	MOV   R4,#STP_PRN-1
SC_F420:MOV   STP_SPM,R4	; maximalni rychlost
	CLR   A
	RET


END