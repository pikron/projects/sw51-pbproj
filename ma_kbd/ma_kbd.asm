$NOMOD51
;********************************************************************
;*              Mala klavesnice  - MA_KBD.ASM                       *
;*                       Hlavni modul                               *
;*                  Stav ke dni 10. 1.1998                          *
;*                      (C) Pisoft 1996                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
;INCLUDE(%INCH_LCD)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_REGS)
$INCLUDE(%INCH_IIC)
$INCLUDE(PLAN.H)
$LIST

%DEFINE (DEBUG_FL)   (0)	; Povoleni vlozeni debug rutin
%DEFINE (WITH_VECTOR)(0)	; Pridani vektoru

%IF(%DEBUG_FL)THEN(
  EXTRN	CODE(MONITOR)
)FI

EXTRN	CODE(RESKEYFL,SKEYREP)
PUBLIC	INPUTc,KBDBEEP,ERRBEEP,RES_STAR,KBDTIMR

MA_KB_C	SEGMENT CODE
MA_KB_D	SEGMENT DATA
MA_KB_B	SEGMENT DATA BITADDRESSABLE
MA_KB_I	SEGMENT IDATA

STACK	EQU   0C0H

CSEG    AT    RESET
	JMP   RES_STAR

%SVECTOR(TIMER0,I_TIME)	  ; Realny cas z casovace 0

CSEG    AT    1FF0H
SER_NUM:%W    (7)
	%W    (100H)

RSEG MA_KB_B

TIME_CN:DS    1    ; Delici citac pro odvozeni 25 Hz
HW_FLG: DS    1
LEB_PHA	BIT   HW_FLG.4	; Pro blikani led


LEB_FLG:DS    1		; Blikani ledek

RSEG MA_KB_D

M_TIMED	EQU   2048 ; Pocet impulsu na casove preruseni
C_TIMED	EQU   18   ; Deleni na 25 Hz pro MCB1 8051
C_TIM06	EQU   15   ; Deleni z 25 Hz na 0.6 s (0.01 min)
C_TMIN	EQU   100  ; Deleni na 1 min
N_OF_T	EQU   3    ; Pocet timeru
TIMR1:	DS    1    ; Timery jsou decrementovany
KBDTIMR:DS    1    ; Timery jsou decrementovany
TIMRI:	DS    1    ; Delicka 0.01 min
TIME:	DS    2    ; Cas v 0.01 min              =====

BEEPTIM:DS    1	   ; Citac delky pipnuti

TMP:	DS    4	   ; Pomocny

RSEG    MA_KB_I

;=================================================================
; Programovy segment

RSEG MA_KB_C

RES_STAR:
	MOV   IEN0,#00000000B	; zakaz preruseni
	MOV   IEN1,#00000000B
	MOV   IP0, #00000000B	; priority preruseni
	MOV   IP1, #00000000B	;
	%WATCHDOG		; Nulovani watch-dogu
	MOV   TMOD,#00010001B	; timer 1 mod 1; timer 0 mod 1
	MOV   TCON,#01010101B	; citac 0 a 1 cita ; interapy hranou
	MOV   SCON,#11011000B	; dva stopbity
	MOV   PCON,#10000000B	; Bd = OSC/12/16/(256-TH1)
	MOV   TH1,#0FAH		; 9600Bd
	MOV   PSW,#0		; banka registru 0
	MOV   SP,#STACK		; inicializace zasobniku
	MOV   P3,#0FFH
	MOV   P1,#0FFH

STARTC:	MOV   LEB_FLG,#0
	CALL  LCDINST
	MOV   BEEPTIM,#0
	CALL  LEDWR
	CLR   TR0		; Priprava casoveho preruseni
	MOV   TH0,#0FFH
	MOV   TL0,#0FFH
	SETB  TR0
	%VECTOR(TIMER0,I_TIME)

	ORL   IEN0,#10000010B	; Spusteni casoveho preruseni

	MOV   DPTR,#DEVER_T
	CALL  cPRINT

	MOV   R5,#6		; Rychlost 9600 pro 11.0592 MHz
	;CALL  uL_INIT		; Zpusteni uLan komunikace

	CALL  INI_IIC		; Zpusteni komunikace IIC

	MOV   DPTR,#MA_T01
	CALL  cPRINT

LKTEST:	MOV   A,KB_MADR		; Smycka testu klavesnice
	JNZ   LKTEST9
	CALL  COMPOOL
	CALL  SCANKEY
     %IF(%DEBUG_FL)THEN(
	CJNE  A,#K_PROG,LKTEST3
	CALL  MONITOR
	MOV   A,#LCD_CLR
	CALL  LCDWCOM
	JMP   LKTEST
    )FI
LKTEST3:JZ    LKTEST
	MOV   A,#LCD_HOM+48H
	CALL  LCDWCOM
	MOV   A,R2
	MOV   R4,A
	MOV   R5,#0
	MOV   R7,#040H
	CALL  PRINTi
	INC   TMP
	MOV   R4,TMP
	MOV   R5,#0
	MOV   R7,#040H
	CALL  PRINTi
	JMP   LKTEST
LKTEST9:CALL  RESKEYFL

L1:	CALL  SCANKEY		; Hlavni smycka
    %IF(%DEBUG_FL)THEN(
	CJNE  A,#K_PROG,L110
	CALL  MONITOR
	MOV   A,#LCD_CLR
	CALL  LCDWCOM
	JMP   L1
    )FI
L110:   JZ    L190
	MOV   R7,A		; Klavesa
	MOV   A,KB_MADR		; Adresa mastera IIC KBD
	JZ    L1
	CALL  TE_SNKB
	JMP   L1
L190:   CALL  COMPOOL
	JMP   L1

COMPOOL:CALL  UREC		; Zpracovani zprav z uLanu
	CALL  SNRQ		; Zadost o adresu
	CALL  SL_REC		; Zpracovani udalosti IIC
	RET

DEVER_T:DB    LCD_CLR,'%VERSION'
	DB    C_LIN2 ,' (c) PiKRON 1998',0

MA_T01:	DB    C_LIN2 ,'Waiting master ...',0

INPUTc:	CALL  SCANKEY
	JZ    INPUTc
	RET

; Pipnuti na klavese klavesnice
%IF (1) THEN (
KBDBEEP:MOV   A,#2
BEEP:	MOV   BEEPTIM,A
	SETB  %BEEP_FL		; Pipnuti
	CALL  LEDWR		; Zmenit LED
	MOV   A,R2
	RET
ERRBEEP:MOV   A,#8
	JMP   BEEP
)ELSE(
ERRBEEP:
KBDBEEP:JMP   KBDSTDB
)FI

;********************************************************************
; Casove preruseni

USING   2

I_TIME: PUSH  ACC		; Pruchod s frekvenci 450Hz
	PUSH  PSW
	PUSH  B
	MOV   PSW,#AR0
	CLR   TF0
	CLR   C
	CLR   TR0
S_T1_D  SET   7			; zpozdeni rutiny
	MOV   A,TL0		; 7 Cyklu
	SUBB  A,#LOW (M_TIMED-S_T1_D)
	MOV   TL0,A
	MOV   A,TH0
	SUBB  A,#HIGH (M_TIMED-S_T1_D)
	MOV   TH0,A
	SETB  TR0
	JMP   I_TIME1

I_TIME1:DJNZ  TIME_CN,ITIMEQ
	MOV   TIME_CN,#C_TIMED	; Pruchod s frekvenci 25Hz
	MOV   A,BEEPTIM
	JZ    I_TIM12
	DEC   BEEPTIM
	DEC   A
	JNZ   I_TIM12
	CLR   %BEEP_FL		; Konec pipnuti
	PUSH  DPL
	PUSH  DPH
	CALL  LEDWR		; Zmenit LED
	POP   DPH
	POP   DPL
I_TIM12:MOV   R0,#TIMR1
	MOV   B,#N_OF_T
I_TIME2:MOV   A,@R0
	JZ    I_TIME3
	DEC   A
	MOV   @R0,A
I_TIME3:INC   R0
	DJNZ  B,I_TIME2
	MOV   A,TIMRI
	JNZ   I_TIMR1
	MOV   TIMRI,#C_TIM06
	CALL  uL_STR		; Pruchod s periodou 0.01 min
I_TIM33:INC   TIME
	MOV   A,TIME
	JNZ   I_TIM52
	INC   TIME+1
I_TIM52:MOV   A,LEB_FLG
	JZ    I_TIM44
	JBC   LEB_PHA,I_TIM42	; Blikani LED
	ORL   LED_FLG,A
	SETB  LEB_PHA
	SJMP  I_TIM43
I_TIM42:CPL   A
	ANL   LED_FLG,A
I_TIM43:PUSH  DPL
	PUSH  DPH
	CALL  LEDWR
	POP   DPH
	POP   DPL
I_TIM44:
I_TIMR1:MOV   R7,#C_TIMED
ITIMEQ: %WATCHDOG
	POP   B
	POP   PSW
	POP   ACC
	RETI


;********************************************************************
; Utility

iiMOVs:	MOV   A,@R1           ; @R0:=@R1 .. delka R2
	MOV   @R0,A
	INC   R0
	INC   R1
	DJNZ  R2,iiMOVs
	RET

ciMOVs:	CLR   A        	      ; @R0:=@DPTR .. delka R2
	MOVC  A,@A+DPTR
	MOV   @R0,A
	INC   R0
	INC   DPTR
	DJNZ  R2,ciMOVs
	RET

CMP_SN: MOV   DPTR,#SER_NUM
	MOV   R2,#4
ciCMPs:	CLR   A        	      ; @R0 ?= @DPTR .. delka R2
	MOVC  A,@A+DPTR       ; vrati ACC=0 pri rovnosti
	XRL   A,@R0
	JNZ   ciCMPsR
	INC   R0
	INC   DPTR
	DJNZ  R2,ciCMPs
ciCMPsR:RET

;********************************************************************
; Pomocne rutiny terminalu terminal

RSEG	MA_KB_D

TE_CP_T:DS    1		; Typ cursoru
TE_CP_X:DS    1		; Poloha cursoru
TE_CP_Y:DS    1

RSEG	MA_KB_C

; Presune ukazatel na X=R4, Y=R5
GOTOXY: CALL  CLR_CP
GOTOXYC:CALL  LCDNBUS
GOTOXY1:MOV   A,R5
	ADD   A,#GXY_D4L-GOTOXY2
	MOVC  A,@A+PC
GOTOXY2:ADD   A,R4
	ORL   A,#LCD_HOM
	JMP   LCDWCO1

GXY_D4L:DB    0
	DB    40H
	DB    14H
	DB    54H


; Zobrazi cursor
SHOW_CP:MOV   A,TE_CP_T
	JZ    CLR_CP
SHOW_C1:CALL  LCDNBUS
	MOV   R4,TE_CP_X
	MOV   R5,TE_CP_Y
	CALL  GOTOXY1
	CALL  LCDNBUS
	MOV   A,#LCD_DON OR LCD_CON
	JMP   LCDWCO1

; Smaze cursor
CLR_CP:	CALL  LCDNBUS
	MOV   A,#LCD_DON
	JMP   LCDWCO1

;********************************************************************
; IIC komunikace a IIC terminal

; Adresa jednotky
IIC_ADR EQU   7AH
;IIC_ADR EQU   7CH

; Typ, prvni byte prijete zpravy
IIC_TEC	EQU   51H	; Rizeni IIC klavesnice a displaye
IIC_TEK	EQU   52H	; Informace o stisnutych klavesach
IIC_TED	EQU   53H	; Zobrazovani dat na display

; Jednotlive prikazy IIC_TED
TED_CLR EQU   1		; Smazat display
TED_CP	EQU   2		; Nastavit TE_CP_T,TE_CP_X,TE_CP_Y
TED_BEEP EQU  3		; Pipne po prijmutou dobu
TED_LED	EQU   4		; Vypsat na led LED BLINK LED_HI
; 80H+L  vypsani radky L

; Jednotlive prikazy IIC_TEC
TEC_CONN EQU  1		; Spojeni klavesnice
TEC_SREP EQU  2		; Nastaveni PUS a REP prodlevy

RSEG	MA_KB_D

KB_MADR:DS    1		; Adresa pripojeneho mastera

RSEG	MA_KB_I

SLAV_BL EQU   22
IIC_INP:DS    SLAV_BL	; Prijmuta zprava
IIC_OUT	IDATA IIC_INP	; Vyzadana zprava
IIC_BUF:DS    SLAV_BL	; Master buffer

RSEG	MA_KB_C

INI_IIC:MOV   S1ADR,#IIC_ADR
	CALL  IIC_PRE
	MOV   S1CON,#01000000B	; S1CON pro X 24.000 MHz
	MOV   TE_CP_T,#0	; Inicializace terminalu
	MOV   KB_MADR,#0
	%LDR45i(IIC_INP)
	%LDR67i(SL_CMIC)
	MOV   R2,#SLAV_BL
	CALL  IIC_SLI
	RET

SL_CM_R:JMP   SL_JRET

; Zpracovavani prikazu z IIC pod prerusenim
; registry  R0  .. funkce
;           R12 .. ukazatel na data
;           R3  .. pocet byte do konce S_BLEN

SL_CMIC:CJNE  R0,#SJ_REND,SL_CM_R
	MOV   R1,S_DP
	MOV   A,@R1		; Prijmuty prikaz
	INC   R1
	CJNE  A,#IIC_TEC,SL_CM20
	MOV   A,S_RLEN		; Delka dat prikazu
	DEC   A
	MOV   R3,A
	JZ    SL_CM19
	MOV   A,@R1
	INC   R1
	DEC   R3
	CJNE  A,#TEC_CONN,SL_CM12
	MOV   A,@R1		; Pripojeni klavesnice
	MOV   KB_MADR,A

	JMP   SL_CM_A
SL_CM12:CJNE  A,#TEC_SREP,SL_CM15
	MOV   A,R3		; Nastaveni repeatu
	JZ    SL_CM13
	MOV   A,@R1
	INC   R1
	DEC   R3
SL_CM13:MOV   R4,A
	MOV   A,R3
	JZ    SL_CM14
	MOV   A,@R1
	INC   R1
SL_CM14:MOV   R5,A
	PUSH  DPL
	PUSH  DPH
	CALL  SKEYREP
	POP   DPH
	POP   DPL
	JMP   SL_CM_A
SL_CM15:
SL_CM19:JMP   SL_CM_A
SL_CM60v:JMP  SL_CM60
SL_CM20:CJNE  A,#IIC_TED,SL_CM60v
	MOV   A,S_RLEN		; Delka dat prikazu
	DEC   A
	MOV   R3,A
	JZ    SL_CM29
	MOV   A,@R1
	INC   R1
	DEC   R3
	JNB   ACC.7,SL_CM30
	ANL   A,#7FH
	MOV   R5,A		; Radka
	; Vypsani radky
SL_CM25:MOV   R4,#0
	CALL  CLR_CP		; Zrusit cursor
	CALL  GOTOXYC		; Najet na polohu
	MOV   A,R3
	JZ    SL_CM27
SL_CM26:CALL  LCDNBUS		; Vypsani znaku
	MOV   A,@R1
	CALL  LCDWR1
	INC   R1
	DJNZ  R3,SL_CM26
SL_CM27:
SL_CM29:CALL  SHOW_CP		; Zobrazeni kurzoru
	JMP   SL_CM_A
SL_CM30:CJNE  A,#TED_CLR,SL_CM31
	CALL  LCDNBUS		; Smazani displeje
	MOV   A,#LCD_CLR
	CALL  LCDWR1
	JMP   SL_CM_A
SL_CM31:CJNE  A,#TED_CP,SL_CM32
	MOV   TE_CP_T,@R1	; Nastavit typ cursoru
	INC   R1
	MOV   TE_CP_X,@R1
	INC   R1
	MOV   TE_CP_Y,@R1
	SJMP  SL_CM29
SL_CM32:CJNE  A,#TED_BEEP,SL_CM35
	MOV   A,R3		; Pipnuti dodane delky
	JZ    SL_CM33
	MOV   A,@R1
	JNZ   SL_CM34
SL_CM33:MOV   BEEPTIM,#1	; Mute
	JMP   SL_CM_A
SL_CM34:PUSH  DPL
	PUSH  DPH
	CALL  BEEP
	POP   DPH
	POP   DPL
	JMP   SL_CM_A
SL_CM35:CJNE  A,#TED_LED,SL_CM40
	MOV   A,R3		; Led
	JZ    SL_CM36
	MOV   A,@R1
	DEC   R3
SL_CM36:INC   R1
	MOV   R4,A
	MOV   A,R3		; Led
	JZ    SL_CM37
	MOV   A,@R1
	DEC   R3
SL_CM37:INC   R1
	MOV   LEB_FLG,A
	ORL   A,#080H		; BEEP_FL !!!!!!
	ANL   LED_FLG,A
	CPL   A
	ANL   A,R4
	ORL   LED_FLG,A
	PUSH  DPL
	PUSH  DPH
	CALL  LEDWR
	POP   DPH
	POP   DPL
	JMP   SL_CM_A

SL_CM40:JMP   SL_CM_A

SL_CM60:
	JMP   SL_JRET
SL_CM_A:CLR   ICF_SRC
	SETB  AA
	MOV   R0,#SJ_REND
	JMP   SL_JRET

; Vysle klavesu v R7
TE_SNKB:CALL  IIC_WME
	MOV   R1,#IIC_BUF
	MOV   @R1,#IIC_TEK
	INC   R1
	MOV   A,R7
	MOV   @R1,A
	MOV   R4,#IIC_BUF
	MOV   R2,#2
	MOV   R3,#0
	MOV   R6,KB_MADR
	CALL  IIC_RQI
	CALL  IIC_WME
	RET

; Zpracovani zprav mimo preruseni
SL_REC: JNB   ICF_SRC,SL_REC9
	CLR   ICF_SRC
SL_REC9:RET

;********************************************************************
; uLAN komunikace

PUBLIC	uL_SNST
PUBLIC	UL_IDB,UL_IDE

RSEG    MA_KB_B

uL_DYFL:DS    1
uLD_RQA	BIT   uL_DYFL.2       ; Pozadavek na pripojeni do site

RSEG    MA_KB_D

uL_DYSA:DS    1

RSEG    MA_KB_C

uL_IDB: DB    '.mt %VERSION .uP 51i .dy',0
uL_IDE:

; Rutina vyslani zadosti o prideleni dynamicke adresy
SNRQ:   JNB   uLD_RQA,SNRQ01
	MOV   R0,#BEG_OB	; Buffer pro vystupni zpravu
	MOV   A,@R0
	JZ    SNRQ02
SNRQ01:	RET
SNRQ02: DEC   uL_DYFL
	MOV   @R0,uL_DYSA	; Adresa dynamickeho serveru
	INC   R0
	MOV   @R0,#07FH		; Sluzebni zprava
	INC   R0
	INC   R0
	MOV   @R0,#0C0H		; Zadost o adresu
	INC   R0
	MOV   @R0,uL_ADR	; Soucasna adresa
	MOV   R2,#4
	MOV   DPTR,#SER_NUM	; Seriove cislo
	CALL  ciMOVs
; Uzavreni vystupni zpravy a jeji odeslani
; vstup : R0 .. ukazuje za konec zpravy
CL_OB:  MOV   A,R0
	MOV   R0,#BEG_OB+2
	MOV   @R0,A
	MOV   R0,#BEG_OB
	MOV   A,@R0
	ORL   A,#080H		; Zprava pripravena k odeslani
	MOV   @R0,A
	SETB  uLF_RS
	JNB   uLF_NB,CL_OB9
	JNB   ES,CL_OB9
	MOV   C,EA
	CLR   EA
	JNB   uLF_NB,CL_OB8
	SETB  TI		; Aktivace uLan preruseni
CL_OB8:	MOV   EA,C
CL_OB9: RET

; Rutina vysilani statusu CMD=0C1H
uL_SNST:MOV   R1,#BEG_PB
	MOV   A,@R1
	JNZ   SNSTA10
	JB    uLD_RQA,SNSTA03 ; Snaha o zviditelneni
	INC   uL_DYFL
	JNB   uLD_RQA,SNSTA03
SNSTA02:ORL   uL_DYFL,#7
	MOV   uL_DYSA,uL_SA   ; Server dynamickych adres
	MOV   uL_ADR,#0
SNSTA03:JMP   SNSTAR

SNSTA10:MOV   R7,A
	ANL   A,#0F0H
	CJNE  A,#010H,SNSTA03
	MOV   A,R6            ; Prikaz cteni udaju
	CLR   C
	SUBB  A,#BEG_PB+1+4   ; Neni test serioveho cisla
	JC    SNSTA13
	INC   R1
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#SER_NUM   ; Kontrola serioveho cisla
	MOV   R2,#4
SNSTA11:CLR   A
	MOVC  A,@A+DPTR
	XRL   A,@R1
	JNZ   SNSTA12
	INC   R1
	INC   DPTR
	DJNZ  R2,SNSTA11
SNSTA12:POP   DPH
	POP   DPL
	JNZ   SNSTA02         ; Nesouhlasi cislo
SNSTA13:CALL  ACK_CMD
	ANL   uL_DYFL,#NOT 7
SNSTA20:CALL  SND_BEB
	PUSH  DPL
	PUSH  DPH
	MOV   R2,#4           ; Vysli seriove cislo
	MOV   DPTR,#SER_NUM
SNSTA21:CLR   A
	MOVC  A,@A+DPTR
	CALL  SND_CHC
	INC   DPTR
	DJNZ  R2,SNSTA21
	POP   DPH
	POP   DPL
%IF (0) THEN (
	CJNE  R7,#010H,SNSTA30
	MOV   A,MOD_II        ; Vyslani zakladnich udaju
	CALL  SND_CHC
	MOV   A,ERRNUM
	CALL  SND_CHC
	MOV   R0,#FLOW
	CALL  SND_IDi
	MOV   R0,#MAX_TIM
	MOV   R2,#6
SNSTA25:CALL  SND_IDi
	DJNZ  R2,SNSTA25
	MOV   A,SPRTYP
	CALL  SND_CHC
	MOV   R0,#VOL_IN
	CALL  SND_IDi
	SJMP  SNSTA50
SNSTA30:CJNE  R7,#011H,SNSTA50
	MOV   R0,#CAP_ACT     ; Vyslani servisnich udaju
	CALL  SND_IDi
	MOV   R0,#IRC_ACT
	CALL  SND_IDi
	MOV   R2,#3
	MOV   R0,#AD_DCIN
	SJMP  SNSTA25
)FI
SNSTA50:CALL  SND_END
SNSTAR: JMP   S_WAITD		; Cekani na dalsi zpravu
	JMP   NAK_CMD

SND_IDi:MOV   A,@R0
	MOV   R4,A
	INC   R0
	MOV   A,@R0
	MOV   R5,A
	INC   R0
SNDR45i:MOV   A,R4
	CALL  SND_CHC
	MOV   A,R5
	JMP   SND_CHC

; Hlavni rutina pro zpracovani zprav mimo preruseni
UREC:   MOV   R0,#BEG_IB
	MOV   A,@R0
	JNB   ACC.7,URECNO
	INC   R0              ; Zpracovani zprav uLan
	MOV   A,@R0
	CJNE  A,#07FH,UREC10
	INC   R0
	MOV   A,@R0
	CLR   C
	SUBB  A,#BEG_IB+3+1+4+1
	JC    URECRV1
	INC   R0
	MOV   A,@R0
	CJNE  A,#0C1H,URECRV1
	INC   R0
	CALL  CMP_SN
	JNZ   URECRV1
	MOV   uL_ADR,@R0
	ANL   uL_DYFL,#NOT 7
URECRV1:JMP   URECR
UREC10: CJNE  A,#024H,UREC20
%IF (0) THEN (
	MOV   A,MOD_II
	XRL   A,#M_COMRD
	JNZ   URECRV1
	INC   R0
	INC   R0
	MOV   FLOW,@R0
	INC   R0
	MOV   FLOW+1,@R0
	INC   R0
	MOV   A,R0
	MOV   R1,A
	MOV   R0,#MAX_TIM
	MOV   R2,#6
	CALL  MOVsi
	SETB  FL_SETL
	JMP   URECR
)FI
UREC20:

URECR:	MOV   R0,#BEG_IB
	MOV   @R0,#0
URECNO:	RET

	END