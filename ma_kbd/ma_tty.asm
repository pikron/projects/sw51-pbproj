$NOMOD51
;********************************************************************
;*                    MA_TTY.ASM                                    *
;*     Obsluha displaye a klavesnice - primo pripojenych na 552     *
;*                  Stav ke dni 10.01.1998                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_LCD)
$INCLUDE(%INCH_REGS)
$LIST

%DEFINE	(X_TIMR_FL) (0) ; Umisteni KBDTIMR v XDATA/DATA

%IF(%X_TIMR_FL) THEN (
  EXTRN	XDATA(KBDTIMR)	; softwareovy timer v XDATA
  %DEFINE (GKBDTIMR) (
	MOV   DPTR,#KBDTIMR
	MOVX  A,@DPTR )
  %DEFINE (SKBDTIMR) (
	MOV   DPTR,#KBDTIMR
	MOVX  @DPTR,A )
)ELSE(
  EXTRN	DATA(KBDTIMR)	; softwareovy timer v DATA
  %DEFINE (GKBDTIMR) (
	MOV   A,KBDTIMR)
  %DEFINE (SKBDTIMR) (
	MOV   KBDTIMR,A)
)FI

EXTRN	CODE(KBDBEEP)	; Pipnuti podle ACC a pak ACC=R2

PUBLIC	LCDINST,LCDINSM,LCDNBUS,LCDWCOM,LCDWCO1,LCDWR,LCDWR1
PUBLIC	PRINT,PRINTH,xPRINT,cPRINT

PUBLIC	SCANKEY,TESTKEY,RESKEYFL,SKEYREP
PUBLIC	KBDSTDB

PUBLIC	LEDWR,LED_FLG,LED_FLH,KBD_FLG


LCD_DPORT  DATA  P4	; Port pripojeny na LCD display
LCD_CPORT  DATA  P3	; Port emulujici ridici signaly
LCD_CMASK  EQU   018H	; Maska ridicich signalu
LCD_INST   EQU   000H	; Zapis instrukci
LCD_STAT   EQU   008H   ; Cteni statusu
LCD_WDATA  EQU   010H   ; Zapis dat
LCD_RDATA  EQU   018H	; Cteni dat
LCD_ENABLE EQU   004H	; Potvrzeni adresy H

KBD_PORT   DATA  P5	; Port s tlacitky klavesnice

TTY___B SEGMENT DATA BITADDRESSABLE
RSEG TTY___B
LED_FLG:DS    1		; priznaky LED diod
LED_FLH:DS    1

KBD_FLG:DS    1		; priznaky klavesnice
KBD_CHG:DS    1		; detekovane zmeny oproti minulemu stavu
KBD_KEY:DS    1		; akceptovany stav klaves
KBD_PUS:DS    1		; prodleva po stisku
KBD_REP:DS    1		; prodleva pri opakovani

TTY___C SEGMENT CODE
RSEG TTY___C

LCDINST:MOV   A,#LCD_MOD
LCDINSM:MOV   PWM0,#040H	; Rozsviceni displaye
	ANL   LCD_CPORT,#NOT LCD_ENABLE
	ORL   LCD_CPORT,#LCD_CMASK
	ANL   LCD_CPORT,#LCD_INST OR NOT LCD_CMASK
	CALL  LCDWAIT
	CALL  LCDWAIT
	CLR   A
	MOV   KBD_FLG,A
	MOV   KBD_KEY,A
	MOV   KBD_PUS,#TIM_PUS
	MOV   KBD_REP,#TIM_REP
	MOV   LED_FLG,A
	MOV   LED_FLH,A
	%SKBDTIMR
	MOV   A,#LCD_CLR
	CALL  LCDWAIT
	ORL   LCD_CPORT,#LCD_CMASK
	ANL   LCD_CPORT,#LCD_STAT OR NOT LCD_CMASK ; Neni treba
	MOV   LCD_DPORT,#0FFH
	ORL   LCD_CPORT,#LCD_ENABLE
	MOV   A,LCD_DPORT
	ANL   LCD_CPORT,#NOT LCD_ENABLE
	JNZ   LCDINSR
	MOV   PWM0,#080H	; Rozsviceni displaye
	ORL   LCD_CPORT,#LCD_CMASK
	ANL   LCD_CPORT,#LCD_WDATA OR NOT LCD_CMASK
	MOV   A,#055H
	CALL  LCDWAIT
	MOV   A,#LCD_HOM
	ORL   LCD_CPORT,#LCD_CMASK
	ANL   LCD_CPORT,#LCD_INST OR NOT LCD_CMASK
	CALL  LCDWAIT
	ORL   LCD_CPORT,#LCD_CMASK
	ANL   LCD_CPORT,#LCD_RDATA OR NOT LCD_CMASK
	MOV   LCD_DPORT,#0FFH
	ORL   LCD_CPORT,#LCD_ENABLE
	MOV   A,LCD_DPORT
	ANL   LCD_CPORT,#NOT LCD_ENABLE
	XRL   A,#055H
	JNZ   LCDINSR
	MOV   PWM0,#0C0H	; Rozsviceni displaye
	MOV   A,#LCD_CLR
	CALL  LCDWCOM
	MOV   A,#LCD_NROL
	CALL  LCDWCOM
	MOV   A,#LCD_DON OR LCD_CON
	CALL  LCDWCOM
	MOV   A,#LCD_NSH
	CALL  LCDWCOM
	MOV   A,#LCD_CLR
	CALL  LCDWCOM
	MOV   PWM0,#0FFH	; Rozsviceni displaye
	CLR   A
LCDINSR:RET

LCDWAIT:MOV   LCD_DPORT,A
	ORL   LCD_CPORT,#LCD_ENABLE
	ANL   LCD_CPORT,#NOT LCD_ENABLE
	MOV   R0,#10
LCDWAI1:DJNZ  R1,LCDWAI1
	DJNZ  R0,LCDWAI1
	RET

LCDNBUS:ORL   LCD_CPORT,#LCD_CMASK
	ANL   LCD_CPORT,#LCD_STAT OR NOT LCD_CMASK ; neni treba
LCDNBU1:MOV   LCD_DPORT,#0FFH
	ORL   LCD_CPORT,#LCD_ENABLE
	MOV   A,LCD_DPORT
	ANL   LCD_CPORT,#NOT LCD_ENABLE
	JB    ACC.7,LCDNBU1	; #LCD_BF
	ORL   LCD_CPORT,#LCD_CMASK
	RET

LCDWCOM:PUSH  ACC
	CALL  LCDNBUS
	POP   ACC
LCDWCO1:ANL   LCD_CPORT,#LCD_INST OR NOT LCD_CMASK
	MOV   LCD_DPORT,A
	ORL   LCD_CPORT,#LCD_ENABLE
	ANL   LCD_CPORT,#NOT LCD_ENABLE
	RET

LCDWR:  PUSH  ACC
	CALL  LCDNBUS
	POP   ACC
LCDWR1: CJNE  A,#C_LIN2,LCDWR2
	MOV   A,#LCD_HOM+040H
	SJMP  LCDWCO1
LCDWR2: JC    LCDWCO1
	ANL   LCD_CPORT,#LCD_WDATA OR NOT LCD_CMASK
	MOV   LCD_DPORT,A
	ORL   LCD_CPORT,#LCD_ENABLE
	ANL   LCD_CPORT,#NOT LCD_ENABLE
PRINTRE:RET

PRINTH: CALL  LCDNBUS
	MOV   A,#LCD_HOM
	CALL  LCDWCO1
PRINT:  MOV   DPL,R1
	MOV   DPH,R2
        CJNE  R3,#002H,cPRINT  ; NEDORESENE - NAVAZNOST NA C

xPRINT: CALL  LCDNBUS
	MOVX  A,@DPTR
        INC   DPTR
	JZ    PRINTRE
	CALL  LCDWR1
	SJMP  xPRINT

cPRINT: CALL  LCDNBUS
	CLR   A
	MOVC  A,@A+DPTR
	INC   DPTR
	JZ    PRINTRE
	CALL  LCDWR1
	SJMP  cPRINT

; Vynulovani priznaku klavesnice
RESKEYFL:CLR  A
	MOV   KBD_FLG,A
	MOV   KBD_KEY,A
	%SKBDTIMR
	RET

; Nastavi opakovani klavesnice na R45
SKEYREP:MOV   KBD_PUS,R4
	MOV   KBD_REP,R5
	RET

; Test kodu klavesy v ACC
; =======================
; vraci: A ..  0 je stisknuta jinak neni
; rusi:  DP,R0

TESTKEY:
	RET

; Cteni klavesnice
; ================
; vraci: A .. kod stisknute klavesy nebo 0
; meni : A, DP, R2, R3

TIM_REP SET   5  ; Pocet taktu repeatu
TIM_PUS SET   20 ; Cekani po stisku
TIM_OFF SET   2  ; Delka uvolneni

SCANKEY:MOV   A,KBD_PORT
	CPL   A
	XRL   A,KBD_KEY
	XCH   A,KBD_CHG
	ANL   A,KBD_CHG
	JZ    SCANKR1
	MOV   R3,#7
SCANKE2:RLC   A
	JC    SCANKE3
	DJNZ  R3,SCANKE2
SCANKE3:MOV   A,R3		; Test stisku klavesy
	XRL   A,KBD_FLG
	ANL   A,#07H
	JNZ   SCANKE4
	%GKBDTIMR
	JNZ   SCANKR6
SCANKE4:MOV   A,R3
	ADD   A,#SCANKEt-SCANKEb
	MOVC  A,@A+PC
SCANKEb:XRL   KBD_CHG,A		; Registrace zmeny
	XRL   KBD_KEY,A
	ANL   A,KBD_KEY
	JZ    SCANKE7

SCANKE5:MOV   KBD_FLG,R3	; Stisk klavesy
	ORL   KBD_FLG,#0C0H
	MOV   A,KBD_PUS
SCANKE6:%SKBDTIMR
	MOV   A,R3
	ADD   A,#040H
	MOV   R2,A
	JMP   KBDBEEP

SCANKE7:MOV   KBD_FLG,R3	; Uvolneni klavesy
	MOV   A,#TIM_OFF
	%SKBDTIMR
	MOV   A,R3
	ADD   A,#0C0H
	MOV   R2,A
	RET

SCANKR1:JNB   KBD_FLG.7,SCANKR9
	JNB   KBD_FLG.6,SCANKR4
	%GKBDTIMR
	JNZ   SCANKR9
	MOV   A,KBD_FLG
	ANL   A,#07H
	MOV   R3,A
	MOV   A,KBD_REP
	JZ    SCANKR9
	SJMP  SCANKE6
SCANKR4:MOV   A,KBD_PUS
	SETB  KBD_FLG.6
	SJMP  SCANKR8
SCANKR6:JNB   KBD_FLG.6,SCANKR9
	CLR   KBD_FLG.6
	MOV   A,#TIM_OFF
SCANKR8:%SKBDTIMR
SCANKR9:CLR   A
	MOV   R2,A
	RET



SCANKEt:DB    00000001B
	DB    00000010B
	DB    00000100B
	DB    00001000B
	DB    00010000B
	DB    00100000B
	DB    01000000B
	DB    10000000B

KBDSTDB:; Nedefinovano		; Pipnuti
	MOV   A,R2
	RET

; Nastaveni indikacnich LED podle A
; =================================

LEDWR:	CLR   A
	JNB   LED_FLG.7,LEDWR1
	CPL   A
LEDWR1:	MOV   PWM1,A
	MOV   A,LED_FLG
	ORL   P1,#03FH
	ORL   A,#0C0H
	ANL   P1,A
	RET

END
