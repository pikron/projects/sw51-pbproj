;********************************************************************
;*                    MA_TTY.ASM                                    *
;*     Obsluha displaye a klavesnice                                *
;*                  Stav ke dni 10.01.1998                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_LCD)
$INCLUDE(%INCH_ADR)
$LIST

%DEFINE	(X_TIMR_FL) (0) ; Umisteni KBDTIMR v XDATA/DATA

%IF(%X_TIMR_FL) THEN (
  EXTRN	XDATA(KBDTIMR)	; softwareovy timer v XDATA
  %DEFINE (GKBDTIMR) (
	MOV   DPTR,#KBDTIMR
	MOVX  A,@DPTR )
  %DEFINE (SKBDTIMR) (
	MOV   DPTR,#KBDTIMR
	MOVX  @DPTR,A )
)ELSE(
  EXTRN	DATA(KBDTIMR)	; softwareovy timer v DATA
  %DEFINE (GKBDTIMR) (
	MOV   A,KBDTIMR)
  %DEFINE (SKBDTIMR) (
	MOV   KBDTIMR,A)
)FI

EXTRN	CODE(KBDBEEP)	; Pipnuti podle ACC a pak ACC=R2

PUBLIC	LCDINST,LCDINSM,LCDNBUS,LCDWCOM,LCDWCO1,LCDWR,LCDWR1
PUBLIC	PRINT,PRINTH,xPRINT,cPRINT

PUBLIC	SCANKEY,TESTKEY,RESKEYFL,SKEYREP
PUBLIC	KBDSTDB

PUBLIC	LEDWR,LED_FLG,LED_FLH,KBD_FLG

TTY___B SEGMENT DATA BITADDRESSABLE
RSEG TTY___B
LED_FLG:DS    1
LED_FLH:DS    1

KBD_FLG:DS    1		; priznaky klavesnice
KBD_FL_MSK  EQU 07FH
KBD_PUS:DS    1		; prodleva po stisku
KBD_REP:DS    1		; prodleva pri opakovani

TTY___C SEGMENT CODE
RSEG TTY___C

LCDINST:MOV   A,#LCD_MOD
LCDINSM:MOV   DPTR,#LCD_INST
	CALL  LCDWAIT
	CALL  LCDWAIT
	CLR   A
	MOV   KBD_FLG,A
	MOV   KBD_PUS,#TIM_PUS
	MOV   KBD_REP,#TIM_REP
	MOV   LED_FLG,A
	MOV   LED_FLH,A
	MOV   A,#LCD_CLR
	CALL  LCDWAIT
	MOV   DPTR,#LCD_STAT
	MOVX  A,@DPTR
	JNZ   LCDINSR
	MOV   DPTR,#LCD_WDATA
	MOV   A,#055H
	CALL  LCDWAIT
	MOV   A,#LCD_HOM
	MOV   DPTR,#LCD_INST
	CALL  LCDWAIT
	MOV   DPTR,#LCD_RDATA
	MOVX  A,@DPTR
	XRL   A,#055H
	JNZ   LCDINSR
	MOV   A,#LCD_CLR
	CALL  LCDWCOM
	MOV   A,#LCD_NROL
	CALL  LCDWCOM
	MOV   A,#LCD_DON OR LCD_CON
	CALL  LCDWCOM
	MOV   A,#LCD_NSH
	CALL  LCDWCOM
	MOV   A,#LCD_CLR
	CALL  LCDWCOM
	CLR   A
LCDINSR:RET

LCDWAIT:MOVX  @DPTR,A
	MOV   R0,#10
LCDWAI1:DJNZ  R1,LCDWAI1
	DJNZ  R0,LCDWAI1
	RET

LCDNBUS:PUSH  DPH
	PUSH  DPL
LCDNBU1:MOV   DPTR,#LCD_STAT
        MOVX  A,@DPTR
        ANL   A,#LCD_BF
        JNZ   LCDNBU1
        MOVX  A,@DPTR
	POP   DPL
        POP   DPH
	RET

LCDWCOM:PUSH  ACC
	CALL  LCDNBUS
        POP   ACC
LCDWCO1:PUSH  DPH
        PUSH  DPL
        MOV   DPTR,#LCD_INST
	MOVX  @DPTR,A
        POP   DPL
	POP   DPH
        RET

LCDWR:  PUSH  ACC
        CALL  LCDNBUS
        POP   ACC
LCDWR1: CJNE  A,#C_LIN2,LCDWR2
        MOV   A,#LCD_HOM+040H
	SJMP  LCDWCO1
LCDWR2: JC    LCDWCO1
	PUSH  DPH
        PUSH  DPL
        MOV   DPTR,#LCD_WDATA
	MOVX  @DPTR,A
        POP   DPL
	POP   DPH
PRINTRE:RET

PRINTH: CALL  LCDNBUS
        MOV   A,#LCD_HOM
	CALL  LCDWCO1
PRINT:  MOV   DPL,R1
	MOV   DPH,R2
        CJNE  R3,#002H,cPRINT  ; NEDORESENE - NAVAZNOST NA C

xPRINT: CALL  LCDNBUS
        MOVX  A,@DPTR
        INC   DPTR
	JZ    PRINTRE
        CALL  LCDWR1
	SJMP  xPRINT

cPRINT: CALL  LCDNBUS
	CLR   A
	MOVC  A,@A+DPTR
	INC   DPTR
	JZ    PRINTRE
	CALL  LCDWR1
	SJMP  cPRINT

; Vynulovani priznaku klavesnice
RESKEYFL:CLR  A
	ANL   KBD_FLG,#NOT KBD_FL_MSK
	%SKBDTIMR
	RET

; Nastavi opakovani klavesnice na R45
SKEYREP:MOV   KBD_PUS,R4
	MOV   KBD_REP,R5
	RET

; Test kodu klavesy v ACC
; =======================
; vraci: A ..  0 je stisknuta jinak neni
; rusi:  DP,R0

TESTKEY:DEC   A
	ANL   A,#3FH
	MOV   R0,B
	MOV   B,#6
	DIV   AB
	XCH   A,R0
	XCH   A,B
	ADD   A,#TESTKET-TESTKE1
	MOVC  A,@A+PC
TESTKE1:MOV   DPTR,#KBDWR
	MOV   C,LED_FLH.7
	MOV   ACC.7,C
	MOV   C,LED_FLH.6
	MOV   ACC.6,C
	MOVX  @DPTR,A
	MOV   DPTR,#KBDRD
	MOVX  A,@DPTR
	XCH   A,R0
	ADD   A,#TESTKET-TESTKE2
	MOVC  A,@A+PC
TESTKE2:XRL   A,R0
	ANL   A,#3FH
	RET

TESTKET:DB    11111110B
	DB    11111101B
	DB    11111011B
	DB    11110111B
	DB    11101111B
	DB    11011111B

; Cteni klavesnice
; ================
; vraci: A .. kod stisknute klavesy nebo 0
; meni : A, DP, R2, R3

TIM_REP SET   5  ; Pocet taktu repeatu
TIM_PUS SET   20 ; Cekani po stisku
TIM_OFF SET   3  ; Delka uvolneni

SCANKEY:
	%GKBDTIMR	; Nacist KBDTIMR do A
	MOV   R2,A
	MOV   DPTR,#KBDRD
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   A,KBD_FLG
	ANL   A,#KBD_FL_MSK
	JZ    SCANKE1
	XCH   A,R3
	CPL   A
	CJNE  R2,#0,SCANKR1
	XRL   A,R3
	ANL   A,#03FH
	JZ    SCANKE2
SCANKE1:MOV   A,KBD_PUS
	SJMP  SCANKE3
SCANKE2:MOV   A,KBD_REP	; rychlost repeatu
SCANKE3:%SKBDTIMR	; Nastavit KBDTIMR na A
	MOV   R2,#006H
	MOV   R3,#NOT 040H
SCANKE4:MOV   A,R3
	RR    A
	MOV   R3,A
	MOV   DPTR,#KBDWR
	MOV   C,LED_FLH.7
	MOV   ACC.7,C
	MOV   C,LED_FLH.6
	MOV   ACC.6,C
	MOVX  @DPTR,A
	MOV   DPTR,#KBDRD
	MOVX  A,@DPTR
	CPL   A
	ANL   A,#03FH
	JNZ   SCANKE5
	DJNZ  R2,SCANKE4
SCANKE5:MOV   R3,A
	MOV   A,KBD_FLG
	XCH   A,R3
	ANL   A,#KBD_FL_MSK
	ANL   KBD_FLG,#NOT KBD_FL_MSK
	ORL   KBD_FLG,A
	JZ    SCANKR3
	MOV   DPL,R3
	MOV   R3,#0FFH
SCANKE6:INC   R3
	RRC   A
	JNC   SCANKE6
	MOV   A,R3
	RL    A
	ADD   A,R3
	RL    A
	ADD   A,R2
	MOV   R2,A
	MOV   R3,DPL
	MOV   A,#1
	JMP   KBDBEEP

SCANKR1:ORL   A,#040H
	ANL   A,R3
	MOV   R2,#TIM_OFF	; doba uvolneni
	JZ    SCANKR2
	XRL   A,#040H
	JZ    SCANKRE
	JB    ACC.6,SCANKRE
	MOV   R2,KBD_PUS	; penalta na kartac
SCANKR2:MOV   A,R3
	XRL   A,#040H
	ANL   A,#KBD_FL_MSK
	ANL   KBD_FLG,#NOT KBD_FL_MSK
	ORL   KBD_FLG,A
	MOV   A,R2
SCANKR3:%SKBDTIMR	; Nastavit KBDTIMR na A
SCANKRE:CLR   A
	RET

KBDSTDB:MOV   DPTR,#LED       ; Pipnuti
	ORL   LED_FLG,#080H
	MOV   A,LED_FLG
	MOVX  @DPTR,A
	MOV   A,#040H
KBDSTB1:DJNZ  ACC,KBDSTB1
	ANL   LED_FLG,#NOT 080H
	MOV   A,LED_FLG
	MOVX  @DPTR,A
	MOV   A,R2
	RET


; Nastaveni indikacnich LED podle A
; =================================

LEDWR:  MOV   A,LED_FLG
	MOV   DPTR,#LED
	MOVX  @DPTR,A
	RET
	MOV   A,KBD_FLG
	MOV   DPTR,#KBDWR
	ORL   A,#3FH
	MOVX  @DPTR,A
	RET

END
