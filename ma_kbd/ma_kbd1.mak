#   Project file pro malou klavesnici
#         (C) Pisoft 1998

ma_kbd.obj : ma_kbd.asm config.h
	a51  ma_kbd.asm $(par) debug

ma_tty.obj : ma_tty.asm config.h
	a51  ma_tty.asm $(par) debug

plan.obj   : plan.asm config.h
	a51  plan.asm $(par) debug

pb_iic.obj : pb_iic.asm config.h
	a51  pb_iic.asm $(par) debug

pb_ai.obj  : pb_ai.asm config.h
	a51  pb_ai.asm $(par) debug

pb_ut1.obj : pb_ut1.asm config.h
	a51  pb_ut1.asm $(par) debug

ma_kbd.    : ma_kbd.obj ma_tty.obj plan.obj pb_iic.obj pb_ai.obj pb_ut1.obj
	l51  ma_kbd.obj,ma_tty.obj,plan.obj,pb_iic.obj,pb_ai.obj,pb_ut1.obj code(00000H) xdata(8000H) ramsize(100h) ixref

ma_kbd.hex : ma_kbd.
	ohs51 ma_kbd

	  : ma_kbd.hex
#	sendhex ma_kbd.hex /p4 /m4 /t2 /g36864 /b9600
	eprem ma_kbd.hex  -2 -d1 -s30
