$NOMOD51
;********************************************************************
;*                    PB_UT1.ASM                                    *
;*     Pomocne rutiny pro praci s pointry, funkcemi, atd            *
;*                  Stav ke dni 18.06.1995                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_REGS)

%IF (%EQS(%PROCESSOR_TYPE,517)) THEN (
; Vyuzivat pole ukazatelu DPTR
EXTRN	DATA(DPLOCK)
%DEFINE (WITH_DPSEL) (1)
)ELSE(
%DEFINE (WITH_DPSEL) (0)
)FI

PUBLIC	cJMPDPP,xJMPDPP,cMDPDP,xMDPDPA,xMDPDP,cxMOVEt,cxMOVE
PUBLIC  xxMOVEt,xxMOVE,XCDPR01,ADDATDP

UTIL_C	SEGMENT CODE

RSEG	UTIL_C

cJMPDPP:CLR    A	      ; Skok na c:[DPTR]
	MOVC   A,@A+DPTR
	INC    DPTR
	MOV    R1,A
	CLR    A
	MOVC   A,@A+DPTR
	INC    DPTR
	MOV    DPH,A
	MOV    DPL,R1
	CLR    A
	JMP    @A+DPTR

xJMPDPP:MOV    C,EA           ; Skok na x:[DPTR]
	CLR    EA
	MOVX   A,@DPTR
	PUSH   ACC
	INC    DPTR
	MOVX   A,@DPTR
	PUSH   ACC
	INC    DPTR
	MOV    EA,C
	RET

cMDPDP: CLR    A              ; DPTR = c:[DPTR]
	MOVC   A,@A+DPTR
	MOV    R0,A
	INC    DPTR
	CLR    A
	MOVC   A,@A+DPTR
	SJMP   xMDPDPR

xMDPDPA:MOV    R0,A	      ; DPTR = x:[DPTR]+A
	MOVX   A,@DPTR
	ADD    A,R0
	MOV    R0,A
	INC    DPTR
	MOVX   A,@DPTR
	ADDC   A,#0
	SJMP   xMDPDPR

xMDPDP:	MOV    C,EA           ; DPTR = x:[DPTR]
	CLR    EA
	MOVX   A,@DPTR
	MOV    R0,A
	INC    DPTR
	MOVX   A,@DPTR
	MOV    EA,C
xMDPDPR:MOV    DPH,A
	MOV    DPL,R0
	RET

cxMOVEt:MOV    R1,#000H
cxMOVE: INC    R1	      ; R01 x x:[R45] = c:[R23]
	CJNE   R0,#0,cxMOVE3
	DJNZ   R1,cxMOVE3
	RET
%IF (%WITH_DPSEL) THEN (
; Specific SAB 517 code
cxMOVE3:MOV    A,DPLOCK
	INC    DPLOCK
	XCH    A,DPSEL
	XCH    A,R4
	MOV    DPL,A
	MOV    DPH,R5
	INC    DPSEL
	MOV    DPL,R2
	MOV    DPH,R3
cxMOVE4:CLR    A
	MOVC   A,@A+DPTR
	INC    DPTR
	DEC    DPSEL
	MOVX   @DPTR,A
	INC    DPTR
	INC    DPSEL
	DJNZ   R0,cxMOVE4
	DJNZ   R1,cxMOVE4
	MOV    R2,DPL
	MOV    R3,DPH
	DEC    DPSEL
	MOV    A,R4
	MOV    R4,DPL
	MOV    R5,DPH
	MOV    DPSEL,A
	DEC    DPLOCK
	RET
)ELSE(
; Generic 51 code
cxMOVE3:MOV    DPL,R2
	MOV    DPH,R3
	CLR    A
	MOVC   A,@A+DPTR
	INC    DPTR
	MOV    R2,DPL
	MOV    R3,DPH
	MOV    DPL,R4
	MOV    DPH,R5
	MOVX   @DPTR,A
	INC    DPTR
	MOV    R4,DPL
	MOV    R5,DPH
	DJNZ   R0,cxMOVE3
	DJNZ   R1,cxMOVE3
	RET
)FI

xxMOVEt:MOV    R1,#000H
xxMOVE: INC    R1	      ; R01 x x:[R45] = x:[R23]
	CJNE   R0,#0,xxMOVE3
	DJNZ   R1,xxMOVE3
	RET
%IF (%WITH_DPSEL) THEN (
; Specific SAB 517 code
xxMOVE3:MOV    A,DPLOCK
	INC    DPLOCK
	XCH    A,DPSEL
	XCH    A,R4
	MOV    DPL,A
	MOV    DPH,R5
	INC    DPSEL
	MOV    DPL,R2
	MOV    DPH,R3
xxMOVE4:MOVX   A,@DPTR
	INC    DPTR
	DEC    DPSEL
	MOVX   @DPTR,A
	INC    DPTR
	INC    DPSEL
	DJNZ   R0,xxMOVE4
	DJNZ   R1,xxMOVE4
	MOV    R2,DPL
	MOV    R3,DPH
	DEC    DPSEL
	MOV    A,R4
	MOV    R4,DPL
	MOV    R5,DPH
	MOV    DPSEL,A
	DEC    DPLOCK
	RET
)ELSE(
; Generic 51 code
xxMOVE3:MOV    DPL,R2
	MOV    DPH,R3
	MOVX   A,@DPTR
	INC    DPTR
	MOV    R2,DPL
	MOV    R3,DPH
	MOV    DPL,R4
	MOV    DPH,R5
	MOVX   @DPTR,A
	INC    DPTR
	MOV    R4,DPL
	MOV    R5,DPH
	DJNZ   R0,xxMOVE3
	DJNZ   R1,xxMOVE3
	RET
)FI

XCDPR01:XCH    A,R0           ; DPTR <-> R01
	XCH    A,DPL
	XCH    A,R0
	XCH    A,R1
	XCH    A,DPH
	XCH    A,R1
	RET

ADDATDP:ADD    A,DPL          ; DPTR = DPTR + A
	MOV    DPL,A
	CLR    A
	ADDC   A,DPH
	MOV    DPH,A
	RET

END