#   Project file pro malou klavesnici
#         (C) Pisoft 1998

ma_kbd.obj : ma_kbd.asm config.h
	a51  ma_kbd.asm $(par) debug

ma_tty.obj : ma_tty.asm config.h
	a51  ma_tty.asm $(par) debug

plan.obj   : plan.asm config.h
	a51  plan.asm $(par) debug

ma_kbd.    : ma_kbd.obj ma_tty.obj plan.obj ..\pblib\pb.lib
	l51  ma_kbd.obj,ma_tty.obj,plan.obj,..\pblib\pb.lib code(09000H) xdata(8000H) ramsize(100h) ixref

ma_kbd.hex : ma_kbd.
	ohs51 ma_kbd

	  : ma_kbd.hex
#	sendhex ma_kbd.hex /p4 /m3 /t2 /g40960 /b16000
	sendhex ma_kbd.hex /p4 /m4 /t2 /g36864 /b9600
