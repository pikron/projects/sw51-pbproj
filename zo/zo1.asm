;********************************************************************
;*                  Zableskove osvetleni - ZO.ASM                   *
;*                           ATMEL 89C2051                          *
;*                  Stav ke dni 25.04.2000                          *
;*                      (C) Pisoft 1999                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

%DEFINE	(WITH_OUT3) (1)		; P1.7 pouzit jako out 3

%*DEFINE (W (WO)) (
	DB   LOW (%WO),HIGH (%WO)
)

%*DEFINE (LDR45i (WO)) (
	MOV  R4,#LOW  (%WO)
	MOV  R5,#HIGH (%WO)
)

PCON    DATA  087H

ZO____C SEGMENT CODE
ZO____D SEGMENT DATA
ZO_MC_D SEGMENT DATA
ZO____B SEGMENT DATA BITADDRESSABLE

STACK	DATA  60H

CSEG	AT    RESET
	JMP   RES_START

CSEG	AT    TIMER0
	JMP   I_DELAY

CSEG	AT    EXTI0
	JMP   I_FIELD

CSEG	AT    EXTI1
	JMP   I_FIELD

RSEG	ZO____B
HW_FLG:	DS    1		; Priznaky
FL_BSY	BIT   HW_FLG.7
FL_RSCO	BIT   HW_FLG.6	; RS232 controled
FL_MF	BIT   HW_FLG.5	; Microcode flash

DIP1:	DS    1		; Nastaveni na DIP spinacich
DIP2:	DS    1

RSEG	ZO____D

RS_PTR:	DS    1		; Ukazatel do RS_BUF
RS_BUF:	DS    48	; Buffer prijimane zpravy
RS_BEND:
RS_ADR	DATA  HW_FLG

RSEG	ZO____C

RES_START:
	; IE	7..EA,6,5,4..ES,3..ET1,2..EX1,1..ET0,0..EX0
	MOV   IE,  #00000000B	;
	MOV   IP,  #00000000B	;
	; TMOD	7..GATE,6..C/T,5..M1,4..M0,3..GATE,2..C/T,1..M1,0..M0
	MOV   TMOD,#00100101B	; 16 bit cnt 0; 8 rel timr 1
;	MOV   TMOD,#00100001B	; 16 bit timer 0; 8 rel timr 1
	; TCON	7..TF1,6..TR1,5..TF0,4..TR0,3..IE1,2..IT1,1..IE0,0..IT0
	MOV   TCON,#01000101B	; interapy hranou, timer 1 run
	MOV   SCON,#11011000B
	MOV   PCON,#10000000B	; Bd = OSC/12/16/(256-TH1)
	MOV   TH1,#-3		; RS-232 19200 pro 11.0592 MHz
	MOV   SP,#STACK
	CLR   A
	MOV   HW_FLG,A
	MOV   RS_PTR,A

	SETB  EX0		; povolit vnejsi preruseni

	SETB  EA

L10:	CALL  ON_DIP
	MOV   A,DIP1
	ANL   A,#7
	ANL   RS_ADR,#NOT 7
	ORL   RS_ADR,A

L11:	CALL  RS_REC
	JZ    L12
	CALL RS_PROC 
L12:

	JB    FL_RSCO,L11

	JB    DIP2.3,L80

	JMP   L10

L80:	CALL  I_FIELD
	MOV   A,DIP1
	CALL  DELAY2
	CALL  I_DELAY
	MOV   A,DIP1
	CPL   A
	CALL  DELAY2
	JMP   L10

L85:	SETB  P1.2
	SETB  P1.3

	MOV   A,DIP1
	CALL  DELAY2

	CLR   P1.3

	MOV   A,DIP2
	CALL  DELAY2

	JMP   L10


DELAY1:	MOV   A,#10
DELAY2: JZ    DELAY9
	MOV   R1,A
	CLR   A
	MOV   R0,A
DELAY4:	NOP
	;DJNZ  ACC,DELAY4
	DJNZ  R0,DELAY4
	DJNZ  R1,DELAY4
DELAY9:	RET

; *******************************************************************
; Prijem po RS-232

RS_REC:	JBC   RI,RS_REC1
RS_REC0:CLR   A
	RET
RS_REC1:MOV   A,SBUF

	;INC   A
	;MOV   SBUF,A
	;DEC   A

	CJNE  A,#0DH,RS_REC2
	SJMP  RS_REC3
RS_REC2:CJNE  A,#0AH,RS_REC4
RS_REC3:MOV   A,RS_PTR
	MOV   RS_PTR,#RS_BUF
	JZ    RS_REC0
	MOV   R0,A
	CLR   A
	MOV   @R0,A
	MOV   A,R0
	CLR   C
	SUBB  A,#RS_BUF
	RET
RS_REC4:MOV   R0,RS_PTR
	CJNE  R0,#0,RS_REC5
	SJMP  RS_REC0
RS_REC5:MOV   @R0,A
	INC   RS_PTR
	CJNE  R0,#RS_BEND-2,RS_REC0
	CLR   A
	MOV   RS_PTR,A
	RET

RS_SND:	MOV   R0,#RS_BUF
	CLR   TI
RS_SND1:MOV   A,@R0
	JZ    RS_SND5
	MOV   SBUF,A
	INC   R0
RS_SND2:JNB   TI,RS_SND2
	CLR   TI
	SJMP  RS_SND1
RS_SND5:MOV   SBUF,#0DH
RS_SND6:JNB   TI,RS_SND6
	CLR   TI
	MOV   SBUF,#0AH
RS_SND7:JNB   TI,RS_SND7
	CLR   TI
RS_PRR:	RET

; Process lines for @Fx@ device
RS_PROC:MOV   A,RS_BUF
	CJNE  A,#'@',RS_PRR
	MOV   A,RS_BUF+1
	CJNE  A,#'F',RS_PRR
	MOV   A,RS_ADR
	ANL   A,#07H
	XRL   A,RS_BUF+2
	CJNE  A,#'0',RS_PRR
	MOV   A,RS_BUF+3
	CJNE  A,#'@',RS_PRR
	SETB  FL_RSCO

	;MOV   RS_BUF,#'1'
	;MOV   RS_BUF+1,#'2'
	;MOV   RS_BUF+2,#'3'

	MOV   A,RS_BUF+4
	CJNE  A,#'F',RS_PR80v
	MOV   A,RS_BUF+5
	CJNE  A,#'S',RS_PR21	; Stop microcode
	SETB  FL_MF
	CALL  MF_STOP
	SJMP  RS_PR90v
RS_PR21:CJNE  A,#'R',RS_PR22	; Run microcode
	SETB  FL_MF
	CALL  MF_RUN
	SJMP  RS_PR90v
RS_PR80v:JMP  RS_PR80

RS_PR22:CJNE  A,#'P',RS_PR30	; Program microcode
	MOV   R0,#MF_MC
	MOV   R2,#MF_MC_E-MF_MC
	CLR   A
RS_PR23:MOV   @R0,A
	INC   R0
	DJNZ  R2,RS_PR23
	MOV   R0,#MF_MC
	MOV   R1,#RS_BUF+7
	MOV   R3,#0
RS_PR24:MOV   A,@R1
	INC   R1
	JZ    RS_PR29
	CJNE  A,#' ',RS_PR26
	SJMP  RS_PR24
RS_PR26:CALL  NIB2B
	JC    RS_PR80
	CALL  NIB2B2		; R2=A<<4; A=NIB2B(@R1+)
	JC    RS_PR80
	ADD   A,R2
	MOV   @R0,A
	INC   R0
	ADD   A,R3
	MOV   R3,A
	SJMP  RS_PR24
RS_PR29:MOV   RS_BUF+6,#'='
	MOV   RS_BUF+7,#'O'
	MOV   RS_BUF+8,#'K'
	MOV   RS_BUF+9,#' '
	MOV   A,R3
	SWAP  A
	CALL  B2NIB
	MOV   RS_BUF+10,A
	MOV   A,R3
	CALL  B2NIB
	MOV   RS_BUF+11,A
	MOV   RS_BUF+12,#0
	SETB  FL_MF
RS_PR90v:SJMP RS_PR90
RS_PR30:CJNE  A,#'D',RS_PR31	; Set DIP command
	MOV   R1,#RS_BUF+7
	CALL  NIB2B1		; A=NIB2B(@R1+)
	CALL  NIB2B2		; R2=A<<4; A=NIB2B(@R1+)
	ADD   A,R2
	MOV   DIP2,A
	CALL  NIB2B1		; A=NIB2B(@R1+)
	CALL  NIB2B2		; R2=A<<4; A=NIB2B(@R1+)
	ADD   A,R2
	MOV   DIP1,A
	CLR   FL_MF
	MOV   TMOD,#00100101B	; 16 bit cnt 0; 8 rel timr 1
	MOV   IE,  #10000001B	; EA, EX0	
	SJMP  RS_PR90

RS_PR31:CJNE  A,#'M',RS_PR32	; Modify command
	MOV   R1,#RS_BUF+7
	CALL  NIB2B1		; A=NIB2B(@R1+)
	JC    RS_PR80
	CALL  NIB2B2		; R2=A<<4; A=NIB2B(@R1+)
	JC    RS_PR80
	ADD   A,R2
	MOV   R3,A
	ADD   A,#MF_MC
	MOV   R0,A
	SJMP  RS_PR24

RS_PR32:CJNE  A,#'O',RS_PR33	; Output command
	MOV   R1,#RS_BUF+7
	CALL  NIB2B1		; A=NIB2B(@R1+)
	CALL  NIB2B2		; R2=A<<4; A=NIB2B(@R1+)
	ADD   A,R2
	CALL  MF_COUT
	SJMP  RS_PR90

RS_PR33:

RS_PR80:MOV   RS_BUF+4,#'E'
	MOV   RS_BUF+5,#'R'
	MOV   RS_BUF+6,#'R'
	MOV   RS_BUF+7,#0
RS_PR90:CALL  RS_SND
	RET

NIB2B2:	SWAP  A
	MOV   R2,A
NIB2B1:	MOV   A,@R1
	INC   R1
NIB2B:	ADD   A,#-'G'
	JC    NIB2B9
	ADD   A,#'G'-'A'
	JC    NIB2B7
	ADD   A,#'A'-'9'-1
	JC    NIB2B9
	ADD   A,#'9'+1-'0'
	CPL   C
        RET
NIB2B7: ADD   A,#10
NIB2B9:	RET

B2NIB:	ANL   A,#0FH
	ADD   A,#-10
	JC    B2NIB2
	ADD   A,#'0'+10
	RET
B2NIB2:	ADD   A,#'A'
	RET


; *******************************************************************
; Generovani zablesku

; Preruseni od pulsnimku INT0 nebo INT1
; P3.0=0 => zablesk
; P3.5=1 => start snimku
; T0 .. radkova synchronizace

; Konfigurace
; DIP1              DIP2
; 1 2 3 4 5 6 7 8 | 1 2 3 4 5 6 7 8
; zpozdeni zablesku    |O  | delka
; DIP1 123 je addr pri RS232

; Vystupy
; P1.2=0 .. vypnout dobijeni
; P1.3=0 .. zablesk
; P3.1 a P3.7

I_FIELD:JNB   FL_MF,I_FLD00
	CLR   EX0
	CLR   EX1
	PUSH  PSW
	PUSH  ACC
	JMP   MF_CMD0
; Standard version without microcode
I_FLD00:JB    P3.0,I_FLD99
	JNB   DIP2.2,I_FLD10
	JNB   P3.5,I_FLD99
I_FLD10:PUSH  PSW
	PUSH  ACC
	CLR   P3.7	; Vystup
	CLR   TR0
	CLR   TF0
	MOV   A,DIP1
	CPL   A
	MOV   TL0,A
	MOV   A,DIP2
	ANL   A,#3
	CPL   A
	MOV   TH0,A
	SETB  TR0
	SETB  ET0
I_FLD90:POP   ACC
	POP   PSW
I_FLD99:RETI


I_DELAY:JNB   FL_MF,I_DEL0
	CLR   TR0
	CLR   ET0
	PUSH  PSW
	PUSH  ACC
	MOV   A,TL0
	CPL   A
	ANL   A,#0FH
	CLR   C
	RRC   A
	JNC   I_DEL_S		; Synchro to 1us for timer
	NOP
I_DEL_S:DJNZ  ACC,I_DEL_S
	JMP   MF_CMD0
; Standard version without microcode
I_DEL0:	PUSH  PSW
	PUSH  ACC
	CLR   P1.2	; vypnout dobijeni
	CLR   P1.3	; zablesk
	CLR   P3.1	; Ohlas

	MOV   A,DIP2
	ANL   A,#0F0H
	INC   A
I_DEL1:	NOP
	DJNZ  ACC,I_DEL1

	SETB  P3.1	; Ohlas
	SETB  P1.3	; konec zablesku
	SETB  P1.2	; dobijet
	SETB  P3.7	; Vystup
	CLR   ET0
	POP   ACC
	POP   PSW
	RETI


; Vyber spinace na P1.4 az P1.6 pri P1.7=0
; Cteni na P1.0 a P1.1

ON_DIP:	MOV   R5,#1
    %IF(%WITH_OUT3)THEN(
	MOV   R2,#08FH
    )ELSE(
	MOV   R2,#00FH
    )FI
ON_DIP2:ORL   P1,#073H
	MOV   A,R2
	ANL   P1,A
	ADD   A,#10H
	MOV   R2,A
	MOV   C,P1.0
	CPL   C
	MOV   A,R4
	RLC   A
	MOV   R4,A
	MOV   C,P1.1
	CPL   C
	MOV   A,R5
	RLC   A
	MOV   R5,A
	JNC   ON_DIP2
	MOV   DIP1,R5
	MOV   DIP2,R4
	RET

; *******************************************************************
; Microcode flash system

; Microcode operations
; 00000000		.. end of program
; 00000001		.. end of cyclic program
; 00000010 Del		.. blocking delay Del us
; 00000011 Out		.. output data
; 000100ba Out		.. and wait for next trig source ba
; 10oooooo DelH DelL	.. set outputs to o and wait for Del cycles
; 11oooooo DelH DelL	.. set outputs to o and wait for Del pulses
;
; Bit mapping of "Out"	0..flash,	1..charge disable,
;			2..out1(TxD),	3..out2
;
; Trig source		b..HL edge,	a..LH edge of V.sync

; TEST EXAMPLE
;
; @F0@FS:					- STOP PROGRAM
; @F0@FP:800400 0303 0210 800080 0303 01	- LOAD PROGRAM
; @F0@FP:800400 0303 0220 800040 0303 01
; @F0@FP:1100 800400 0303 02FF 01
; @F0@FR:					- RUN PROGRAM
;
; @F0@FM:00 900080 0303 0210 800080 0303 01	- MODIFY PROGRAM
; @F0@FP:0300 0280 0303 02FF 01
; @F0@FD:FFFF					- SET DIP VALUE
; @F0@FO:00					- OUTPUT


DSEG	AT    8		; Register bank 1
MF_MCP:	DS    1		; Microcode pointer in R0 of bank 1

;RSEG	ZO_MC_D

MF_MCLN	SET   32
MF_MC:	DS    MF_MCLN	; Microcode storage
MF_MC_E:

RSEG	ZO____C

MF_CMD:	PUSH  PSW
	PUSH  ACC
MF_CMD0:MOV   PSW,#MF_MCP AND NOT 7
MF_CMD1:MOV   A,@R0
	INC   R0
	JNB   ACC.7,MF_CMD4
	;***  Set output and wait commands
	CLR   TR0
	ANL   TMOD,#NOT 4	; C/T0=0 timer
	JNB   ACC.6,MF_CMD2
	ORL   TMOD,#4		; C/T0=1 counter
MF_CMD2:CALL  MF_COUT
	MOV   A,@R0
	CPL   A
	MOV   TH0,A
	INC   R0
	MOV   A,@R0
	CPL   A
	MOV   TL0,A
	INC   R0
	CLR   TF0		; Clear overflow flag
	SETB  ET0		; Enable T/C int
	SETB  TR0		; Start T/C
MF_RET:	POP   ACC
	POP   PSW
	RETI
MF_CMD4:JNB   ACC.4,MF_CMD6
	; Set output and wait for trig
	CLR   IE0
	CLR   IE1
	MOV   C,ACC.0
	MOV   EX0,C
	MOV   C,ACC.1
	MOV   EX1,C
	MOV   A,@R0
	INC   R0
	CALL  MF_COUT
	SJMP  MF_RET
MF_CMD6:;*** End of micocode
	JZ    MF_RET
	DJNZ  ACC,MF_CMD7
	;*** End of micocode, cycle to start
	MOV   R0,#MF_MC
	MOV   A,SBUF
	CJNE  A,#'r',MF_CMD1
	SJMP  MF_RET
MF_CMD7:DJNZ  ACC,MF_CMD9
	;*** Blocking delay
	MOV   A,@R0
	INC   R0
MF_CMD8:NOP
	DJNZ  ACC,MF_CMD8
	SJMP  MF_CMD1
MF_CMD9:DJNZ  ACC,MF_CM10
	;*** Output
	MOV   A,@R0
	INC   R0
	CALL  MF_COUT
	SJMP  MF_CMD1
MF_CM10:

	SJMP  MF_RET


; Set outputs according to ACC
MF_COUT:MOV   C,ACC.0
	CPL   C
	MOV   P1.3,C	; Flash output
	MOV   C,ACC.1
	CPL   C
	MOV   P1.2,C	; Charge control
	MOV   C,ACC.2
	CPL   C
	MOV   P3.1,C	; Synchro signal / TxD
	MOV   C,ACC.3
	CPL   C
	MOV   P3.7,C	; Aux output 2
    %IF(%WITH_OUT3)THEN(
	MOV   C,ACC.4
	MOV   P1.7,C	; Aux output 3
    )FI
	RET


MF_RUN:	CLR   EA
	MOV   MF_MCP,#MF_MC
	CALL  MF_CMD
	SETB  EA
	RET

MF_STOP:
	CLR   EA
	CLR   EX0
	CLR   EX1
	CLR   ET0
	MOV   MF_MCP,#MF_MC
	SETB  P3.1
	SETB  P1.3
	SETB  EA
	RET


END