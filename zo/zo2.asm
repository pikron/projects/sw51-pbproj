;********************************************************************
;*                  Zableskove osvetleni - ZO.ASM                   *
;*                           ATMEL 89C2051 nebo 8752                *
;*                  Stav ke dni 25.04.2000                          *
;*                      (C) Pisoft 1999                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

%DEFINE (WITH_8752) (1)		; Novy hardware s 8752

%DEFINE	(WITH_OUT3) (1)		; P1.7 pouzit jako out 3

%*DEFINE (W (WO)) (
	DB   LOW (%WO),HIGH (%WO)
)

%*DEFINE (LDR45i (WO)) (
	MOV  R4,#LOW  (%WO)
	MOV  R5,#HIGH (%WO)
)

PCON    DATA  087H

ZO____C SEGMENT CODE
ZO____D SEGMENT DATA
ZO____I SEGMENT IDATA
ZO____B SEGMENT DATA BITADDRESSABLE

%IF(%WITH_8752)THEN(
STACK	DATA  0C0H
)ELSE(
STACK	DATA  60H
)FI

CSEG	AT    RESET
	JMP   RES_START

CSEG	AT    TIMER0
	JMP   I_DELAY

CSEG	AT    EXTI0
	JMP   I_FIELD

CSEG	AT    EXTI1
	JMP   I_FIELD

RSEG	ZO____B
HW_FLG:	DS    1		; Priznaky
FL_BSY	BIT   HW_FLG.7
FL_RSCO	BIT   HW_FLG.6	; RS232 controled
FL_MF	BIT   HW_FLG.5	; Microcode flash
FL_TEST	BIT   HW_FLG.4	; Testovaci rezim

DIP1:	DS    1		; Nastaveni na DIP spinacich
DIP2:	DS    1

RSEG	ZO____D

RS_PTR:	DS    1		; Ukazatel do RS_BUF
%IF(%WITH_8752)THEN(
RS_BUF:	DS    73	; Buffer prijimane zpravy
)ELSE(
RS_BUF:	DS    48	; Buffer prijimane zpravy
)FI
RS_BEND:
RS_ADR	DATA  HW_FLG

RSEG	ZO____C

RES_START:
	; IE	7..EA,6,5,4..ES,3..ET1,2..EX1,1..ET0,0..EX0
	MOV   IE,  #00000000B	;
	MOV   IP,  #00000000B	;
	; TMOD	7..GATE,6..C/T,5..M1,4..M0,3..GATE,2..C/T,1..M1,0..M0
	MOV   TMOD,#00100101B	; 16 bit cnt 0; 8 rel timr 1
;	MOV   TMOD,#00100001B	; 16 bit timer 0; 8 rel timr 1
	; TCON	7..TF1,6..TR1,5..TF0,4..TR0,3..IE1,2..IT1,1..IE0,0..IT0
	MOV   TCON,#01000101B	; interapy hranou, timer 1 run
	MOV   SCON,#11011000B
	MOV   PCON,#10000000B	; Bd = OSC/12/16/(256-TH1)
	MOV   TH1,#-(BAUDDIV_9600/2)
	MOV   SP,#STACK
	CLR   A
	MOV   HW_FLG,A
	MOV   RS_PTR,A

	MOV   P0,#0FFH
	MOV   P1,#0FFH
	MOV   P2,#0FFH
	MOV   P3,#0FFH
	CLR   A
L05:	DJNZ  ACC,L05

	SETB  EX0		; povolit vnejsi preruseni

	SETB  EA

L10:	CALL  ON_DIP
	MOV   A,DIP1
	ANL   A,#7
	ANL   RS_ADR,#NOT 7
	ORL   RS_ADR,A
    %IF(%WITH_8752)THEN(
	CALL  EE_MCRD
    )FI
	MOV   A,DIP2
	RR    A
	RR    A
	ANL   A,#3		; MOD .. operacni mod (0 program, 1 test, 2 DIP)
	JNZ   L15
	SETB  FL_RSCO		; Rizeni pres RS232
	SETB  FL_MF		; Rizeni microcodem
	CALL  RS_SPD		; Setup RS232 speed
	JNB   DIP1.7,L14
	ANL   SCON,#07FH	; Pouze s jednim stopbitem
L14:	MOV   C,DIP2.4
	CPL   C
	MOV   P3.6,C		; Synchro signal
	MOV   C,DIP2.5
	CPL   C
	MOV   P3.7,C		; Aux output 2
    %IF(%WITH_OUT3)THEN(
	MOV   C,DIP2.6
	CPL   C
	MOV   P1.1,C		; Aux output 3
    )FI
	JNB   DIP1.3,L19
	CALL  MF_RUN		; Autostart microcode
	SJMP  L19
L15:
	DJNZ  ACC,L19
	SETB  FL_TEST		; Test
L19:

L20:
L22:	CALL  RS_REC
	JZ    L23
	CALL  RS_PROC
L23:
	JB    FL_RSCO,L22

	CALL  ON_DIP

	JB    FL_TEST,L80

	JMP   L20

L80:	CALL  I_FIELD
	MOV   A,DIP1
	CALL  DELAY2
	CALL  I_DELAY
	MOV   A,DIP1
	CPL   A
	CALL  DELAY2
	JMP   L20

L85:	SETB  P1.2
	SETB  P1.3

	MOV   A,DIP1
	CALL  DELAY2

	CLR   P1.3

	MOV   A,DIP2
	CALL  DELAY2

	JMP   L20


DELAY1:	MOV   A,#10
DELAY2: JZ    DELAY9
	MOV   R1,A
	CLR   A
	MOV   R0,A
DELAY4:	NOP
	;DJNZ  ACC,DELAY4
	DJNZ  R0,DELAY4
	DJNZ  R1,DELAY4
DELAY9:	RET


; *******************************************************************
; Setup RS-232

BAUDDIV_9600 SET 6	; RS-232 9600 pro 11.0592 MHz

RS_DIVT:	; Tabulka delitelu frekvence
	DB    BAUDDIV_9600/2	; 19200
	DB    BAUDDIV_9600*8	;  1200
	DB    BAUDDIV_9600*4	;  2400
	DB    BAUDDIV_9600*2	;  4800
	DB    BAUDDIV_9600	;  9600
	DB    BAUDDIV_9600/2	; 19200
	DB    BAUDDIV_9600/3	; 28800
	DB    BAUDDIV_9600/4	; 38400
	DB    0

; Vraci v ACC divisor pro danou rychlost
RS_SPD:	MOV   A,DIP1
	SWAP  A
	ANL   A,#7
	MOV   DPTR,#RS_DIVT
	MOVC  A,@A+DPTR
	CPL   A
	INC   A
	MOV   TH1,A
	RET

; *******************************************************************
; Prijem po RS-232

RS_REC:	JBC   RI,RS_REC1
RS_REC0:CLR   A
	RET
RS_REC1:MOV   A,SBUF

	;INC   A
	;MOV   SBUF,A
	;DEC   A

	CJNE  A,#0DH,RS_REC2
	SJMP  RS_REC3
RS_REC2:CJNE  A,#0AH,RS_REC4
RS_REC3:MOV   A,RS_PTR
	MOV   RS_PTR,#RS_BUF
	JZ    RS_REC0
	MOV   R0,A
	CLR   A
	MOV   @R0,A
	MOV   A,R0
	CLR   C
	SUBB  A,#RS_BUF
	RET
RS_REC4:MOV   R0,RS_PTR
	CJNE  R0,#0,RS_REC5
	SJMP  RS_REC0
RS_REC5:MOV   @R0,A
	INC   RS_PTR
	CJNE  R0,#RS_BEND-2,RS_REC0
	CLR   A
	MOV   RS_PTR,A
	RET

RS_SND:	CLR   A
RS_SND0:NOP
	DJNZ  ACC,RS_SND0
	MOV   R0,#RS_BUF
	CLR   TI
RS_SND1:MOV   A,@R0
	JZ    RS_SND5
	MOV   SBUF,A
	INC   R0
RS_SND2:JNB   TI,RS_SND2
	CLR   TI
	SJMP  RS_SND1
RS_SND5:MOV   SBUF,#0DH
RS_SND6:JNB   TI,RS_SND6
	CLR   TI
	MOV   SBUF,#0AH
RS_SND7:JNB   TI,RS_SND7
	CLR   TI
RS_PRR:	RET

; Process lines for @Fx@ device
RS_PROC:MOV   A,RS_BUF
	CJNE  A,#'@',RS_PRR
	MOV   A,RS_BUF+1
	CJNE  A,#'F',RS_PRR
	MOV   A,RS_ADR
	ANL   A,#07H
	XRL   A,RS_BUF+2
	CJNE  A,#'0',RS_PRR
	MOV   A,RS_BUF+3
	CJNE  A,#'@',RS_PRR
	SETB  FL_RSCO

	;MOV   RS_BUF,#'1'
	;MOV   RS_BUF+1,#'2'
	;MOV   RS_BUF+2,#'3'

	MOV   A,RS_BUF+4
	CJNE  A,#'F',RS_PR80v
	MOV   A,RS_BUF+5
	CJNE  A,#'S',RS_PR21	; Stop microcode
	SETB  FL_MF
	CALL  MF_STOP
	SJMP  RS_PR90v
RS_PR21:CJNE  A,#'R',RS_PR22	; Run microcode
	SETB  FL_MF
	CALL  MF_RUN
	SJMP  RS_PR90v
RS_PR80v:JMP  RS_PR80

RS_PR22:CJNE  A,#'P',RS_PR30	; Program microcode
	MOV   R0,#MF_MC
	MOV   R2,#MF_MC_E-MF_MC
	CLR   A
RS_PR23:MOV   @R0,A
	INC   R0
	DJNZ  R2,RS_PR23
	MOV   R0,#MF_MC
	MOV   R1,#RS_BUF+7
	MOV   R3,#0
RS_PR24:MOV   A,@R1
	INC   R1
	JZ    RS_PR29
	CJNE  A,#' ',RS_PR26
	SJMP  RS_PR24
RS_PR26:CALL  NIB2B
	JC    RS_PR80v
	CALL  NIB2B2		; R2=A<<4; A=NIB2B(@R1+)
	JC    RS_PR80v
	ADD   A,R2
	MOV   @R0,A
	INC   R0
	ADD   A,R3
	MOV   R3,A
	SJMP  RS_PR24
RS_PR29:MOV   RS_BUF+6,#'='
	MOV   RS_BUF+7,#'O'
	MOV   RS_BUF+8,#'K'
	MOV   RS_BUF+9,#' '
	MOV   A,R3
	SWAP  A
	CALL  B2NIB
	MOV   RS_BUF+10,A
	MOV   A,R3
	CALL  B2NIB
	MOV   RS_BUF+11,A
	MOV   RS_BUF+12,#0
	SETB  FL_MF
RS_PR90v:JMP  RS_PR90
RS_PR30:CJNE  A,#'D',RS_PR31	; Set DIP command
	MOV   R1,#RS_BUF+7
	CALL  NIB2B1		; A=NIB2B(@R1+)
	CALL  NIB2B2		; R2=A<<4; A=NIB2B(@R1+)
	ADD   A,R2
	MOV   DIP2,A
	CALL  NIB2B1		; A=NIB2B(@R1+)
	CALL  NIB2B2		; R2=A<<4; A=NIB2B(@R1+)
	ADD   A,R2
	MOV   DIP1,A
	CLR   FL_MF
	MOV   TMOD,#00100101B	; 16 bit cnt 0; 8 rel timr 1
	MOV   IE,  #10000001B	; EA, EX0	
	SJMP  RS_PR90

RS_PR31:CJNE  A,#'M',RS_PR32	; Modify command
	MOV   R1,#RS_BUF+7
	CALL  NIB2B1		; A=NIB2B(@R1+)
	JC    RS_PR80
	CALL  NIB2B2		; R2=A<<4; A=NIB2B(@R1+)
	JC    RS_PR80
	ADD   A,R2
	MOV   R3,A
	ADD   A,#MF_MC
	MOV   R0,A
	SJMP  RS_PR24

RS_PR32:CJNE  A,#'O',RS_PR33	; Output command
	MOV   R1,#RS_BUF+7
	CALL  NIB2B1		; A=NIB2B(@R1+)
	CALL  NIB2B2		; R2=A<<4; A=NIB2B(@R1+)
	ADD   A,R2
	CALL  MF_COUT
	SJMP  RS_PR90

RS_PR33:
    %IF(%WITH_8752)THEN(
	CJNE  A,#'W',RS_PR34	; Write program to EEPROM
	CALL  EE_MCWR
	MOV   RS_BUF+6,#'='
	MOV   RS_BUF+7,#'O'
	MOV   RS_BUF+8,#'K'
	MOV   RS_BUF+9,#0
	SJMP  RS_PR90
    )FI
RS_PR34:
    %IF(%WITH_8752)THEN(
	CJNE  A,#'C',RS_PR36	; Check program code
	MOV   RS_BUF+6,#'='
	MOV   R1,#RS_BUF+7
	MOV   R0,#MF_MC
	MOV   R2,#MF_MCLN
RS_PR35:MOV   A,@R0
	INC   R0
	MOV   R3,A
	SWAP  A
	CALL  B2NIB
	MOV   @R1,A
	INC   R1
	MOV   A,R3
	CALL  B2NIB
	MOV   @R1,A
	INC   R1
	DJNZ  R2,RS_PR35
	MOV   @R1,#0
	SJMP  RS_PR90
    )FI
RS_PR36:CJNE  A,#'V',RS_PR37	; Version read
	MOV   RS_BUF+6,#'='
	MOV   RS_BUF+7,#'1'
	MOV   RS_BUF+8,#'1'
	MOV   RS_BUF+9,#0
	SJMP  RS_PR90
RS_PR37:

RS_PR80:MOV   RS_BUF+4,#'E'
	MOV   RS_BUF+5,#'R'
	MOV   RS_BUF+6,#'R'
	MOV   RS_BUF+7,#0
RS_PR90:CALL  RS_SND
	RET

NIB2B2:	SWAP  A
	MOV   R2,A
NIB2B1:	MOV   A,@R1
	INC   R1
NIB2B:	ADD   A,#-'G'
	JC    NIB2B9
	ADD   A,#'G'-'A'
	JC    NIB2B7
	ADD   A,#'A'-'9'-1
	JC    NIB2B9
	ADD   A,#'9'+1-'0'
	CPL   C
	RET
NIB2B7: ADD   A,#10
NIB2B9:	RET

B2NIB:	ANL   A,#0FH
	ADD   A,#-10
	JC    B2NIB2
	ADD   A,#'0'+10
	RET
B2NIB2:	ADD   A,#'A'
	RET


; *******************************************************************
; Generovani zablesku

; Preruseni od pulsnimku INT0 nebo INT1
; P1.0=0 => zablesk   byl P3.0
; P3.5=1 => start snimku
; T0 .. radkova synchronizace

; Konfigurace - stara
; DIP1              DIP2
; 1 2 3 4 5 6 7 8 | 1 2 3 4 5 6 7 8
; zpozdeni zablesku    |O T| delka
; Pri T a nastavenem DIP1 4 dojde
; po resetu k rozbehnuti programu
; DIP1 123 je addr pri RS232

; Konfigurace
; DIP1              DIP2
; 1 2 3 4 5 6 7 8 | 1 2 3 4 5 6 7 8
;  ADR |A| Bd  |S |    |MOD| O123|
; ADR  - DIP1 123 addresa pri RS232
; A    - DIP1 4   automaticky start programu
; Bd   - DIP1 567 (0 .. 19200, 1 ..  1200, 2 ..  2400, 3 ..  4800,
;                  4 ..  9600, 5 .. 19200, 6 .. 28800, 7 .. 38400)
; S    - DIP1 8    stop bits (0 .. 2, 1 .. 1)
; O123 - DIP2 567  pocatecni hodnoty OUT1, OUT2, OUT3
; MOD  - DIP2 34   operacni mod (0 .. programove rizeni, pri A autostart,
;                  1 .. test, 2 .. rizeni z DIPu )

; Vystupy
; P1.2=0 .. vypnout dobijeni
; P1.3=0 .. zablesk
; P3.1(P3.6) a P3.7

I_FIELD:JNB   FL_MF,I_FLD00
	CLR   EX0
	CLR   EX1
	PUSH  PSW
	PUSH  ACC
	JMP   MF_CMD0
; Standard version without microcode
I_FLD00:
    %IF(%WITH_8752)THEN(
	JB    P1.0,I_FLD99
    )ELSE(
	JB    P3.0,I_FLD99
    )FI
	JNB   DIP2.2,I_FLD10
	JNB   P3.5,I_FLD99
I_FLD10:PUSH  PSW
	PUSH  ACC
	CLR   P3.7	; Vystup
	CLR   TR0
	CLR   TF0
	MOV   A,DIP1
	CPL   A
	MOV   TL0,A
	MOV   A,DIP2
	ANL   A,#3
	CPL   A
	MOV   TH0,A
	SETB  TR0
	SETB  ET0
I_FLD90:POP   ACC
	POP   PSW
I_FLD99:RETI


I_DELAY:JNB   FL_MF,I_DEL0
	CLR   TR0
	CLR   ET0
	PUSH  PSW
	PUSH  ACC
	MOV   A,TL0
	CPL   A
	ANL   A,#0FH
	CLR   C
	RRC   A
	JNC   I_DEL_S		; Synchro to 1us for timer
	NOP
I_DEL_S:DJNZ  ACC,I_DEL_S
	JMP   MF_CMD0
; Standard version without microcode
I_DEL0:	PUSH  PSW
	PUSH  ACC
	CLR   P1.2	; vypnout dobijeni
	CLR   P1.3	; zablesk
    %IF(%WITH_8752)THEN(
	CLR   P3.6	; Ohlas
    )ELSE(
	CLR   P3.1	; Ohlas
    )FI

	MOV   A,DIP2
	ANL   A,#0F0H
	INC   A
I_DEL1:	NOP
	DJNZ  ACC,I_DEL1

    %IF(%WITH_8752)THEN(
	SETB  P3.6	; Ohlas
    )ELSE(
	SETB  P3.1	; Ohlas
    )fi
	SETB  P1.3	; konec zablesku
	SETB  P1.2	; dobijet
	SETB  P3.7	; Vystup
	CLR   ET0
	POP   ACC
	POP   PSW
	RETI


%IF(%WITH_8752)THEN(
; Cteni DIP spinacu
ON_DIP: MOV   A,P0
	CPL   A
	MOV   DIP1,A
	MOV   A,P2
	CPL   A
	MOV   R2,A
	ANL   A,#0FH
	ADD   A,#ON_DIPt-ON_DIPb
	MOVC  A,@A+PC
ON_DIPb:SWAP  A
	XCH   A,R2
	SWAP  A
	ANL   A,#0FH
	ADD   A,#ON_DIPt-ON_DIPc
	MOVC  A,@A+PC
ON_DIPc:ORL   A,R2
	MOV   DIP2,A
	RET

ON_DIPt:DB    0000B
	DB    1000B
	DB    0100B
	DB    1100B
	DB    0010B
	DB    1010B
	DB    0110B
	DB    1110B
	DB    0001B
	DB    1001B
	DB    0101B
	DB    1101B
	DB    0011B
	DB    1011B
	DB    0111B
	DB    1111B

)ELSE(
; Vyber spinace na P1.4 az P1.6 pri P1.7=0
; Cteni na P1.0 a P1.1
ON_DIP:	MOV   R5,#1
    %IF(%WITH_OUT3)THEN(
	MOV   R2,#08FH
    )ELSE(
	MOV   R2,#00FH
    )FI
ON_DIP2:ORL   P1,#073H
	MOV   A,R2
	ANL   P1,A
	ADD   A,#10H
	MOV   R2,A
	MOV   C,P1.0
	CPL   C
	MOV   A,R4
	RLC   A
	MOV   R4,A
	MOV   C,P1.1
	CPL   C
	MOV   A,R5
	RLC   A
	MOV   R5,A
	JNC   ON_DIP2
	MOV   DIP1,R5
	MOV   DIP2,R4
	RET
)FI

; *******************************************************************
; Emulace SPI

%IF(%WITH_8752)THEN(

SPI_PORT DATA P1	; Brana, ke ktere je pripojena pamet
SPI_DO	EQU   020H	; DI periferie
SPI_DI	EQU   040H	; DO periferie
SPI_CL	EQU   080H	; CL

; Nacte 8 bitu z periferie do ACC
SPI8IN:	MOV   A,#1
SPI8IN1:ORL   SPI_PORT,#SPI_CL		; Clock
	NOP
	ANL   SPI_PORT,#NOT SPI_CL
	PUSH  ACC
	MOV   A,SPI_PORT
	ANL   A,#SPI_DI
	ADD   A,#-1
	POP   ACC
	RLC   A
	JNC   SPI8IN1
	RET

; Vysle 8 bitu v ACC do periferie
SPI8OUT:SETB  C
SPI8OU0:RLC   A
SPI8OU1:JNC   SPI8OU2
	ORL   SPI_PORT,#SPI_DO
	SJMP  SPI8OU3
SPI8OU2:ANL   SPI_PORT,#NOT SPI_DO
SPI8OU3:ORL   SPI_PORT,#SPI_CL		; Clock
	NOP
	ANL   SPI_PORT,#NOT SPI_CL
	CLR   C
	RLC   A
	JNZ   SPI8OU1
	RET

SPI3OUT:ANL   A,#7
	SWAP  A
	RL    A
	ORL   A,#10H
	CLR   C
	SJMP  SPI8OU0

SPI7OUT:RL    A
	ORL   A,#1
	CLR   C
	SJMP  SPI8OU0

SPI6OUT:ANL   A,#03FH
	RL    A
	RL    A
	ORL   A,#2
	CLR   C
	SJMP  SPI8OU0

)FI
; *******************************************************************
; Prace s pameti EEPROM 9346

%IF(%WITH_8752)THEN(

EEA_RD	EQU   1		; akce cteni
EEA_WR	EQU   2		; akce zapisu

EE_SEL	EQU   010H	; 1 .. select pameti

EE_WR_ATO EQU 1		; Pocet bytu zapisovanych naraz
EE_RD_ATO EQU 1		; Pocet bytu ctenych naraz
%DEFINE (EE_ORG16) (0)  ; Organizace po 16 bitech, jinak 8
%DEFINE (EE_XDATA) (0)  ; Ukladat obsah pameti do XDATA

; Prikazy v 1. trech bitech
EE_CMRD	EQU   110B	; read
EE_CMWR	EQU   101B	; write
EE_CMER	EQU   111B	; erase one byte
EE_CMEW	EQU   100B	; write/erase enable  .. ADR=11xxxxx
			; write/erase disable .. ADR=00xxxxx
			; erase all ..           ADR=10xxxxx
			; write all by data ..   ADR=01xxxxx

RSEG	ZO____C

; Vysle prikaz v ACC do pameti
EE_SCMD:ANL   SPI_PORT,#NOT EE_SEL
	ANL   SPI_PORT,#NOT SPI_CL
	NOP
	ORL   SPI_PORT,#SPI_CL
	NOP
	ANL   SPI_PORT,#NOT SPI_CL
	NOP
	ORL   SPI_PORT,#EE_SEL
	JMP   SPI3OUT


; Cteni pameti EEPROM
;	DPTR/R1 .. pointer na buffer (R1 pro DATA)
;	R2   .. pocet ctenych byte
;	R4   .. adresa, od ktere se cte

EE_RD:  MOV   R3,#0		; Inicializace XOR sum
EE_RD10:MOV   A,R2		; Priprava bloku 1-3 byte
	JZ    EE_RD40
	ADD   A,#-EE_RD_ATO
	MOV   R0,#EE_RD_ATO
	MOV   R2,A
	JC    EE_RD12
	ADD   A,#EE_RD_ATO
	MOV   R0,A
	MOV   R2,#0
EE_RD12:MOV   A,#EE_CMRD
	CALL  EE_SCMD
	MOV   A,R4
    %IF(%EE_ORG16)THEN(
	RR    A
	CALL  SPI6OUT		; Organizace EEPROM 64*16
    )ELSE(
	CALL  SPI7OUT		; Organizace EEPROM 128*8
    )FI
EE_RD30:CALL  SPI8IN
    %IF(%EE_XDATA)THEN(
	MOVX  @DPTR,A
	INC   DPTR
    )ELSE(
	MOV   @R1,A
	INC   R1
    )FI
	XRL   A,R3
	INC   A
	MOV   R3,A
	INC   R4
	DJNZ  R0,EE_RD30
	SJMP  EE_RD10
EE_RD40:MOV   A,#EE_CMRD
	CALL  EE_SCMD
	MOV   A,R4
    %IF(%EE_ORG16)THEN(
	RR    A
	CALL  SPI6OUT		; Organizace EEPROM 64*16
    )ELSE(
	CALL  SPI7OUT		; Organizace EEPROM 128*8
    )FI
	CALL  SPI8IN
	ANL   SPI_PORT,#NOT EE_SEL
	XRL   A,R3
	RET

EE_ERR:	SETB  F0
	ORL   A,#07H
	RET

; Zapis do pameti EEPROM
;	DPTR/R1 .. pointer na buffer (R1 pro DATA)
;	R2   .. pocet zapisovanych byte
;	R4   .. adresa, od ktere se zapisuje

EE_WR:	MOV   R3,#0		; Povoleni zapisu
	MOV   A,#EE_CMEW
	CALL  EE_SCMD
	MOV   A,#0FFH
	CALL  SPI7OUT
EE_WR10:MOV   A,R2		; Priprava bloku 1-3 byte
	JZ    EE_WR40
	ADD   A,#-EE_WR_ATO
	MOV   R0,#EE_WR_ATO
	MOV   R2,A
	JC    EE_WR12
	ADD   A,#EE_WR_ATO
	MOV   R0,A
	MOV   R2,#0
EE_WR12:MOV   A,#EE_CMWR	; Vyslani prikazu a adresy
	CALL  EE_SCMD
	MOV   A,R4
    %IF(%EE_ORG16)THEN(
	RR    A
	CALL  SPI6OUT		; Organizace EEPROM 64*16
    )ELSE(
	CALL  SPI7OUT		; Organizace EEPROM 128*8
    )FI
EE_WR22:
    %IF(%EE_XDATA)THEN(
	MOVX  A,@DPTR		; Vysilani bytu
	INC   DPTR
    )ELSE(
	MOV   A,@R1
	INC   R1
    )FI
	PUSH  ACC
	XRL   A,R3
	INC   A
	MOV   R3,A
	POP   ACC
	CALL  SPI8OUT
	INC   R4
	DJNZ  R0,EE_WR22
	ANL   SPI_PORT,#NOT EE_SEL	;  Konec zapisu
	NOP
	ORL   SPI_PORT,#EE_SEL
EE_WR26:ORL   SPI_PORT,#SPI_CL
	NOP
	ANL   SPI_PORT,#NOT SPI_CL
	MOV   A,SPI_PORT
	ANL   A,#SPI_DI
	JZ    EE_WR26		; Cekat na SPI_CL=1
	SJMP  EE_WR10
EE_WR40:MOV   A,#EE_CMWR	; Vyslani prikazu a adresy
	CALL  EE_SCMD
	MOV   A,R4
    %IF(%EE_ORG16)THEN(
	RR    A
	CALL  SPI6OUT		; Organizace EEPROM 64*16
    )ELSE(
	CALL  SPI7OUT		; Organizace EEPROM 128*8
    )FI
	MOV   A,R3		; Kontrolni byte
	CALL  SPI8OUT
	MOV   A,#EE_CMEW
	CALL  EE_SCMD
	MOV   A,#000H
	CALL  SPI7OUT
	ANL   SPI_PORT,#NOT EE_SEL	;  Konec zapisu
	CLR   A
	RET

EE_MCRD:MOV   R4,#10H		; Adresa v EEPROM
	MOV   R2,#MF_MCLN
	MOV   R1,#MF_MC
	JMP   EE_RD

EE_MCWR:MOV   R4,#10H		; Adresa v EEPROM
	MOV   R2,#MF_MCLN
	MOV   R1,#MF_MC
	JMP   EE_WR
)FI
; *******************************************************************
; Microcode flash system

; Microcode operations
; 00000000		.. end of program
; 00000001		.. end of cyclic program
; 00000010 Del		.. blocking delay Del (about us
; 00000011 Out		.. output data
; 00000100 Tgt		.. jump instruction on OD/EVEN
; 00000101 Tgt		.. jump instruction on ENABLE
; 00000110 Flsh		.. short flash for Flsh delay (about 2us step)
; 000100ba Out		.. set Out and wait for next trig source ba
; 10oooooo DelH DelL	.. set outputs to o and wait for Del cycles
; 11oooooo DelH DelL	.. set outputs to o and wait for Del pulses
;
; Bit mapping of "Out"	0..flash,	1..charge disable,
;			2..out1(TxD),	3..out2
;			4..out3
;
; Trig source "ba"	b..HL edge,	a..LH edge of V.sync
;
; Jump target "Tgt"	bit 7	jump on L (bit 7=0), on H (bit 7=1)
;			bit 6	relative to end of JMP ins (bit 6=0)
;				absolute (bit 6=1)
;			bit 0-5	jump offset or absolute address

; TEST EXAMPLE
;
; @F0@FS:					- STOP PROGRAM
; @F0@FP:800400 0303 0210 800080 0303 01	- LOAD PROGRAM
; @F0@FP:800400 0303 0220 800040 0303 01
; @F0@FP:1100 800400 0303 02FF 01
; @F0@FR:					- RUN PROGRAM
;
; @F0@FM:00 900080 0303 0210 800080 0303 01	- MODIFY PROGRAM
; @F0@FP:0300 0280 0303 02FF 01
; @F0@FD:FFFF					- SET DIP VALUE
; @F0@FO:00					- OUTPUT
; @F0@FW:					- WRITE TO EEPROM
; @F0@FC:					- CHECK PROGRAM
; @F0@FV:					- READ VERSION


DSEG	AT    8		; Register bank 1
MF_MCP:	DS    1		; Microcode pointer in R0 of bank 1

%IF(%WITH_8752)THEN(
RSEG	ZO____I
)FI

MF_MCLN	SET   32
MF_MC:	DS    MF_MCLN	; Microcode storage
MF_MC_E:

RSEG	ZO____C

MF_CMD:	PUSH  PSW
	PUSH  ACC
MF_CMD0:MOV   PSW,#MF_MCP AND NOT 7
MF_CMD1:MOV   A,@R0
	INC   R0
	JNB   ACC.7,MF_CMD4
	;***  Set output and wait commands
	CLR   TR0
	ANL   TMOD,#NOT 4	; C/T0=0 timer
	JNB   ACC.6,MF_CMD2
	ORL   TMOD,#4		; C/T0=1 counter
MF_CMD2:CALL  MF_COUT
	MOV   A,@R0
	CPL   A
	MOV   TH0,A
	INC   R0
	MOV   A,@R0
	CPL   A
	MOV   TL0,A
	INC   R0
	CLR   TF0		; Clear overflow flag
	SETB  ET0		; Enable T/C int
	SETB  TR0		; Start T/C
MF_RET:	POP   ACC
	POP   PSW
	RETI
MF_CMD4:JNB   ACC.4,MF_CMD6
	; Set output and wait for trig
	CLR   IE0
	CLR   IE1
	MOV   C,ACC.0
	MOV   EX0,C
	MOV   C,ACC.1
	MOV   EX1,C
	MOV   A,@R0
	INC   R0
	CALL  MF_COUT
	SJMP  MF_RET
MF_CMD6:;*** End of micocode	(0)
	JZ    MF_RET
	DJNZ  ACC,MF_CMD7
	;*** End of micocode, cycle to start (1)
	MOV   R0,#MF_MC
	MOV   A,SBUF
	CJNE  A,#'r',MF_CMD1
	SJMP  MF_RET
MF_CMD7:DJNZ  ACC,MF_CMD9
	;*** Blocking delay	(2)
	MOV   A,@R0
	INC   R0
MF_CMD8:NOP
	DJNZ  ACC,MF_CMD8
	SJMP  MF_CMD1
MF_CMD9:DJNZ  ACC,MF_CM10
	;*** Output		(3)
	MOV   A,@R0
	INC   R0
	CALL  MF_COUT
	SJMP  MF_CMD1
MF_CM10:DJNZ  ACC,MF_CM11
	;*** jump instruction on OD/EVEN (4)
%IF(%WITH_8752)THEN(
	MOV   C,P3.5		; Input OD/EVEN
)FI
	SJMP  MF_CM12
MF_CM11:DJNZ  ACC,MF_CM20
	;*** jump instruction on ENABLE  (5)
%IF(%WITH_8752)THEN(
	MOV   C,P1.0		; Input ENABLE
	CPL   C
)FI
MF_CM12:MOV   A,@R0
	ANL   A,#080H
	XRL   PSW,A
	XRL   A,@R0
	INC   R0
	JC    MF_CMD1
	JBC   ACC.6,MF_CM13
	ADD   A,R0
	MOV   R0,A
	SJMP  MF_CMD1
MF_CM13:ADD   A,#MF_MC
	MOV   R0,A
	JMP   MF_CMD1
MF_CM20:DJNZ  ACC,MF_CM22
	;*** Short Flash	(6)
	MOV   A,@R0
	INC   R0
%IF(%WITH_8752)THEN(
	CLR   P1.3	; Flash output
MF_CM21:DJNZ  ACC,MF_CM21
	SETB  P1.3	; Flash output
)FI
	JMP   MF_CMD1
MF_CM22:
	SJMP  MF_RET

%IF(%WITH_8752)THEN(
; Set outputs according to ACC
MF_COUT:MOV   C,ACC.0
	CPL   C
	MOV   P1.3,C	; Flash output
	MOV   C,ACC.1
	CPL   C
	MOV   P1.2,C	; Charge control
	MOV   C,ACC.2
	CPL   C
	MOV   P3.6,C	; Synchro signal
	MOV   C,ACC.3
	CPL   C
	MOV   P3.7,C	; Aux output 2
    %IF(%WITH_OUT3)THEN(
	MOV   C,ACC.4
	MOV   P1.1,C	; Aux output 3
    )FI
	RET
)ELSE(
; Set outputs according to ACC
MF_COUT:MOV   C,ACC.0
	CPL   C
	MOV   P1.3,C	; Flash output
	MOV   C,ACC.1
	CPL   C
	MOV   P1.2,C	; Charge control
	MOV   C,ACC.2
	CPL   C
	MOV   P3.1,C	; Synchro signal / TxD
	MOV   C,ACC.3
	CPL   C
	MOV   P3.7,C	; Aux output 2
    %IF(%WITH_OUT3)THEN(
	MOV   C,ACC.4
	MOV   P1.7,C	; Aux output 3
    )FI
	RET
)FI

MF_RUN:	CLR   EA
	MOV   MF_MCP,#MF_MC
	CALL  MF_CMD
	SETB  EA
	RET

MF_STOP:
	CLR   EA
	CLR   EX0
	CLR   EX1
	CLR   ET0
	MOV   MF_MCP,#MF_MC
	SETB  P3.1
	SETB  P1.3
	SETB  EA
	RET


END
