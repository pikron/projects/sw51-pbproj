#!/bin/bash
#
# ZO2 FLASH CONTROLLER - RS232 tests
#
# *******************************************************************
# Microcode flash system

# Microcode operations
# 00000000		.. end of program
# 00000001		.. end of cyclic program
# 00000010 Del		.. blocking delay Del us
# 00000011 Out		.. output data
# 00000100 Tgt		.. jump instruction on OD/EVEN
# 00000101 Tgt		.. jump instruction on ENABLE
# 00000110 Del		.. short flash signal
# 000100ba Out		.. and wait for next trig source ba
# 10oooooo DelH DelL	.. set outputs to o and wait for Del cycles
# 11oooooo DelH DelL	.. set outputs to o and wait for Del pulses
#
# Bit mapping of "Out"	0..flash,	1..charge disable,
#			2..out1(TxD),	3..out2
#			4..out3
#
# Trig source		b..HL edge,	a..LH edge of V.sync

# TEST EXAMPLE
#
# @F0@FS:					- STOP PROGRAM
# @F0@FP:800400 0303 0210 800080 0303 01	- LOAD PROGRAM
# @F0@FP:800400 0303 0220 800040 0303 01
# @F0@FP:1100 800400 0303 02FF 01
# @F0@FR:					- RUN PROGRAM
#
# @F0@FM:00 900080 0303 0210 800080 0303 01	- MODIFY PROGRAM
# @F0@FP:0300 0280 0303 02FF 01
# @F0@FD:FFFF					- SET DIP VALUE
# @F0@FO:00					- OUTPUT
# @F0@FW:					- WRITE TO EEPROM
# @F0@FC:					- CHECK PROGRAM
# @F0@FV:					- FIRMWARE VERSION


if [ "$1" = "" ]
then
  TTYS=/dev/ttyS1
else
  TTYS=$1
fi

# set line discipline
if true
then   # high speed version 6 .. 19200, 4 .. 28800
  setserial $TTYS spd_cust divisor 6
  stty raw extb clocal <$TTYS 
  # stty raw 19200 clocal <$TTYS 
else
  stty raw 9600 clocal <$TTYS  # -clocal
fi
stty cs8 -crtscts cstopb -parenb -parodd <$TTYS >$TTYS
stty ignbrk -ignpar -parmrk -inpck -ixon -isig cread <$TTYS >$TTYS
stty -echo -echoe -crterase -icanon <$TTYS >$TTYS
#stty -noflsh <$TTYS >$TTYS
#stty -a <$TTYS

#flush garbage
cat $TTYS &
echo >$TTYS
sleep 1
kill $!

#

echo ZO2 flash controller test
echo set all DIPs to 0

echo "@F0@FS:">$TTYS
echo "@F0@FP:800400 0303 0210 800080 0303 01">$TTYS

sleep 1
ZOREPLY=`cat $TTYS`
echo $ZOREPLY

#ZOREPLY=`cat $TTYS`
#echo $ZOREPLY

echo "@F0@FR:">$TTYS


#flash
echo "@F0@FO:01">$TTYS

#charge disable
echo "@F0@FO:02">$TTYS

#out1
echo "@F0@FO:04">$TTYS

#out2
echo "@F0@FO:08">$TTYS

#out3
echo "@F0@FO:10">$TTYS

return


# TV sync test program
# off 
# HL		12 00
# out1
# LH		11 04
# out2
# 100 pulses	C8 0064	/ 1000 uS 98 03E8
# out3
# 1000 uS	90 03E8
# flash cd	03 03
# 100 block	02 64
# cd		03 02
# 100 block	02 64
# cyklus	01
echo "@F0@FS:">$TTYS
# test pro ZO2 se separatorem
echo "@F0@FP:1100 1204 C80064 9003E8 0303 0264 0302 0264 01">$TTYS
# test pro ZO2 bez separatorem
#echo "@F0@FP:1100 1204 8803E8 9003E8 0303 0264 0302 0264 01">$TTYS
#echo "@F0@FP:110012108803E8030302640302026401">$TTYS
echo "@F0@FR:">$TTYS
#echo "@F0@FW:">$TTYS
#echo "@F0@FC:">$TTYS
#echo "@F0@FV:">$TTYS


# short flash and jumps
# off 
# LH		11 00
# out1
# HL		12 04
# out2
# 1000 uS	88 03E8
# out3
# 1000 uS	90 03E8
# jmp ODD	04 02
# short flash 	06 14
# 100 block	02 64
# cyklus	01
#echo "@F0@FP:1100 1204 8803E8 9003E8 0482 0614 0264 01">$TTYS
#echo "@F0@FP:1100 1204 8803E8 9003E8 04C4 0614 0264 01">$TTYS
# without jmp
#echo "@F0@FP:1100 1204 8803E8 9003E8 0628 0264 01">$TTYS



