;********************************************************************
;*                  Zableskove osvetleni - ZO.ASM                   *
;*                           ATMEL 89C2051                          *
;*                  Stav ke dni 14.04.1999                          *
;*                      (C) Pisoft 1999                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

%*DEFINE (W (WO)) (
	DB   LOW (%WO),HIGH (%WO)
)

%*DEFINE (LDR45i (WO)) (
	MOV  R4,#LOW  (%WO)
	MOV  R5,#HIGH (%WO)
)

ZO____C SEGMENT CODE
ZO____D SEGMENT DATA
ZO____B SEGMENT DATA BITADDRESSABLE

STACK	DATA  60H

CSEG	AT    RESET
	JMP   RES_START

CSEG	AT    TIMER0
	JMP   I_DELAY

CSEG	AT    EXTI0
	JMP   I_FIELD

CSEG	AT    EXTI1
	JMP   I_FIELD

RSEG	ZO____B
HW_FLG:	DS    1		; Priznaky
FL_BSY	BIT   HW_FLG.7

DIP1:	DS    1		; Nastaveni na DIP spinacich
DIP2:	DS    1

RSEG	ZO____C

RES_START:
	MOV   IE,  #00000000B ;
	MOV   IP,  #00000000B ;
;	MOV   TMOD,#00100001B ; 16 bit timer 0;
	MOV   TMOD,#00010101B ; 16 bit cnt 0; 16 bit timr 1
	MOV   TCON,#00000101B ; interapy hranou
	MOV   SP,#STACK
	CLR   A
	MOV   HW_FLG,A

	SETB  EX0	      ; povolit vnejsi preruseni

	SETB  EA

L10:	CALL  ON_DIP

	JB    DIP2.3,L80

	JMP   L10

L80:	CALL  I_FIELD
	MOV   A,DIP1
	CALL  DELAY2
	CALL  I_DELAY
	MOV   A,DIP1
	CPL   A
	CALL  DELAY2
	JMP   L10

L85:	SETB  P1.2
	SETB  P1.3

	MOV   A,DIP1
	CALL  DELAY2

	CLR   P1.3

	MOV   A,DIP2
	CALL  DELAY2

	JMP   L10


DELAY1:	MOV   A,#10
DELAY2: JZ    DELAY9
	MOV   R1,A
	CLR   A
	MOV   R0,A
DELAY4:	NOP
	;DJNZ  ACC,DELAY4
	DJNZ  R0,DELAY4
	DJNZ  R1,DELAY4
DELAY9:	RET

; *******************************************************************
; Generovani zablesku

; Preruseni od pulsnimku INT0 nebo INT1
; P3.0=0 => zablesk
; P3.5=1 => start snimku
; T0 .. radkova synchronizace

; Konfigurace
; DIP1              DIP2
; 1 2 3 4 5 6 7 8 | 1 2 3 4 5 6 7 8
; zpozdeni zablesku    |O  | delka

; Vystupy
; P1.2=0 .. vypnout dobijeni
; P1.3=0 .. zablesk
; P3.1 a P3.7

I_FIELD:JB    P3.0,I_FLD99
	JNB   DIP2.2,I_FLD10
	JNB   P3.5,I_FLD99
I_FLD10:PUSH  PSW
	PUSH  ACC
	CLR   P3.7	; Vystup
	CLR   TR0
	CLR   TF0
	MOV   A,DIP1
	CPL   A
	MOV   TL0,A
	MOV   A,DIP2
	ANL   A,#3
	CPL   A
	MOV   TH0,A
	SETB  TR0
	SETB  ET0
I_FLD90:POP   ACC
	POP   PSW
I_FLD99:RETI


I_DELAY:PUSH  PSW
	PUSH  ACC
	CLR   P1.2	; vypnout dobijeni
	CLR   P1.3	; zablesk
	CLR   P3.1	; Ohlas

	MOV   A,DIP2
	ANL   A,#0F0H
	INC   A
I_DEL1:	NOP
	DJNZ  ACC,I_DEL1

	SETB  P3.1	; Ohlas
	SETB  P1.3	; konec zablesku
	SETB  P1.2	; dobijet
	SETB  P3.7	; Vystup
	CLR   ET0
	POP   ACC
	POP   PSW
	RETI


; Vyber spinace na P1.4 az P1.6 pri P1.7=0
; Cteni na P1.0 a P1.1

ON_DIP:	MOV   R5,#1
	MOV   R2,#00FH
ON_DIP2:ORL   P1,#0F3H
	MOV   A,R2
	ANL   P1,A
	ADD   A,#10H
	MOV   R2,A
	MOV   C,P1.0
	CPL   C
	MOV   A,R4
	RLC   A
	MOV   R4,A
	MOV   C,P1.1
	CPL   C
	MOV   A,R5
	RLC   A
	MOV   R5,A
	JNC   ON_DIP2
	MOV   DIP1,R5
	MOV   DIP2,R4
	RET

END