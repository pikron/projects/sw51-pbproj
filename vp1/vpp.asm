$NOMOD51
;********************************************************************
;*                    VP 1000 - VP.ASM                              *
;*     Hlavni modul software pro procesor kontroly a mereni polohy  *
;*                  Stav ke dni 23.07.2002                          *
;*                      (C) Pisoft 1994                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$INCLUDE(REG552.H)
$INCLUDE(II_AI.H)
$INCLUDE(VP_MSG.H)
$INCLUDE(II_IIC.H)
$INCLUDE(II_PLAN.H)
PUBLIC	uL_SNST,uL_IDB,uL_IDE

%DEFINE  (EP_SOFT_FL)  (0)    ; Software pro EP1000
%DEFINE  (VPP_SOFT_FL) (0)    ; PVC hadicka  VP1000P
%DEFINE  (VPD_SOFT_FL) (0)    ; Detska verze VP1000D
%DEFINE  (DEBUG_FL)    (0)    ; Povoleni vlozeni debug rutin
%DEFINE  (DR2ROT_FL)   (0)    ; Kapky pres dvojnasobek otacek
%DEFINE  (DRFAST_FL)   (1)    ; Kapky prevadet rychleji

%IF (%EP_SOFT_FL) THEN (
CMAX_FL   EQU   600 	      ; Max nastavitelny prutok v 1 ml/h
SOFT_VER  EQU	87H           ; Kompatabilita verze pro  EP1000

)ELSE(
%IF (%VPP_SOFT_FL) THEN (
CMAX_FL   EQU   1000 	      ; Max nastavitelny prutok v 1 ml/h
SOFT_VER  EQU	047H	      ; Kompatabilita verze pro  VP1000P

)ELSE(
%IF (%VPD_SOFT_FL) THEN (
CMAX_FL   EQU   1500 	      ; Max nastavitelny prutok v 1.0 ml/h
SOFT_VER  EQU	0D7H	      ; Kompatabilita verze pro  VP1000D

)ELSE(
CMAX_FL   EQU   1000 	      ; Max nastavitelny prutok v 1 ml/h
SOFT_VER  EQU	07H           ; Kompatabilita verze pro  VP1000
)FI
)FI
)FI

%DEFINE (WATCHDOG) (
	ORL   PCON,#10H
	MOV   T3,#0E0H
)

%*DEFINE (W (WO)) (
	DB   LOW (%WO),HIGH (%WO)
)

ROT_MOT BIT   P1.0    ; Otocka motorku
BATTST	BIT   P4.6    ; Nulovy spina test baterie
SPRTST	BIT   P4.4    ; Testovani zalozene strikacky
MOTOFF	BIT   P4.3    ; Vypnuti motoru
ELED	BIT   P4.2    ; Errorova led
BEEPB   BIT   P4.1    ; Pipani
OFF_PRO BIT   P4.0    ; 0 -> nelze vypnout
SERV_SW BIT   P4.5    ; 0 .. servisni switch je sepnuty
P4_RES  EQU   11110001B
P4_MSK  EQU   01011110B

LED_CLK BIT   P3.5    ; Ovladani indikace strikacky
LED_DAT BIT   P3.4

LED0    EQU   0111111111111111B
LED1    EQU   1011111111111111B
LED2    EQU   1101111111111111B

;=============================================================

; Zpravy v OUT_MSG posilane na druhy procesor

OMS_CAP EQU   080H          ; Skupina zprav pro kalibraci CAP cidla
OMS_SCH EQU   090H          ; Zmenil se typ strikacky
OMS_CCH EQU   094H          ; Zmenila se konfigurace
OMS_CER EQU   0AAH          ; Bude se mazat chyba
OMS_STR EQU   0B5H          ; Start cerpani
OMS_KVO EQU   0BAH          ; Star rezimu KVO

IC____C SEGMENT CODE
IC____D SEGMENT DATA
IC____I SEGMENT IDATA
IC____B SEGMENT DATA BITADDRESSABLE
STACK   SEGMENT IDATA


%IF (%EP_SOFT_FL) THEN (
RSEG    IC____C
uL_IDB: DB    '.mt EP1000 v 0.7 .uP 51i .dy',0
uL_IDE:
CSEG    AT    1FF0H
SER_NUM:%W    (1)
	%W    (2)

)ELSE(
%IF (NOT %VPD_SOFT_FL) THEN (
RSEG    IC____C
uL_IDB: DB    '.mt VP1000 v 0.7 .uP 51i .dy',0
uL_IDE:
CSEG    AT    1FF0H
SER_NUM:%W    (1)
	%W    (1)

)ELSE(
RSEG    IC____C
uL_IDB: DB    '.mt VP1000D v 0.7 .uP 51i .dy',0
uL_IDE:
CSEG    AT    1FF0H
SER_NUM:%W    (0D000H)
	%W    (1)
)FI
)FI

RSEG	STACK
STACK_S EQU   30H
	DS    STACK_S

USING   0

CSEG    AT    RESET
	JMP   START

CSEG    AT    TIMER0          ; Realny cas z casovace 0
	JMP   I_TIME

RSEG IC____B

TIME_CN:DS    1    ; Delici citac pro odvozeni 25 Hz
HW_FLG: DS    1
FL_EREE BIT   HW_FLG.7        ; Chyba EEPROM
FL_EDROP BIT  HW_FLG.6        ; Pozadavek na chybu kapek
FL_MOT  BIT   HW_FLG.5        ; Spusteni motoru
FL_DCIN BIT   HW_FLG.4        ; Je vnejsi napajeni
FL_TRER BIT   HW_FLG.3        ; Pro test chyby ERTRHI
FL_MOV  BIT   HW_FLG.2        ; Motor overload
FL_SETL BIT   HW_FLG.1        ; Nastavit limity podle pocitace
FL_NERR BIT   HW_FLG.0        ; Neodeslana nova chyba


SW_FLG: DS    1               ; Priznaky vyuzite softwarem
FL_BEWA BIT   SW_FLG.7        ; Varovani pipanim
FL_TIME BIT   SW_FLG.6        ; Nastaven casovy limit
FL_PRES BIT   SW_FLG.5        ; Tlakovy limit
FL_VOL  BIT   SW_FLG.4        ; Objemovy limit
FL_BAT  BIT   SW_FLG.3        ; Napajeni z baterie
FL_EDRF BIT   SW_FLG.2        ; Priznak refresovani udaju
FL_STKV BIT   SW_FLG.1	      ; Cerpani v rezimu KVO
FL_SERV BIT   SW_FLG.0        ; Priznak servisniho rezimu

SM_TIME EQU   40H
SM_PRES EQU   20H
SM_VOL  EQU   10H

SW1_FLG:DS    1               ; Priznaky vyuzite softwarem
FL_FROT	BIT   SW1_FLG.7       ; Prvni otacka motoru
FL_ROTM	BIT   SW1_FLG.6	      ; Stara hodnota ROT_MOT
FL_FCYC	BIT   SW1_FLG.5	      ; Prvni cyklus komunikace po startu
FL_FRO1	BIT   SW1_FLG.4	      ; Druhy priznak FL_FROT
FL_ROTE	BIT   SW1_FLG.3	      ; Neprisla otacka motoru
ROT_DCN DATA  SW1_FLG
ROT_DMS EQU   3

CFG_FLG:DS    1               ; Priznaky konfigurace davkovace
FL_UL48 BIT   CFG_FLG.7       ; uLan 4800 / 9600 Bd
FL_HSBU	BIT   CFG_FLG.6       ; Na bubliny spadne okamzite
FL_P100 BIT   CFG_FLG.5       ; Zmena standartni tlakove meze
			      ; pro tenzometr z 50 na 100 kPa
FL_SSBU	BIT   CFG_FLG.4       ; Max 1 Bublina 0.3 ml jinak 0.05 ml
			      ; bylo: FL_IROT Ignorovani otacky motorku
FL_6MBU	BIT   CFG_FLG.3	      ; Povoleni tmave kapaliny
			      ; bylo: Na bubliny spadne po pruchodu 0.6 ml
			      ; jinak po 0.3 ml
FL_PSET	BIT   CFG_FLG.2	      ; PVC hadicka => mene kapek
RAT_MSK	EQU   3		      ; bity 0 a 1 .. prevodovy pomer

PPCNT:  DS    1               ; Ping-Pong counter

RSEG IC____D

PPT_MAX EQU   30   ; Maximalni cas mezi prijmy Ping-Pongu
PPT_SND EQU   4    ; Cas po kterem se zacne vysilat
PPTIMR: DS    1    ; Casovac pro Ping-Pong

BPTIMR: DS    1    ; Citac delky pipnuti

M_TIMED EQU   2048; Pocet impulsu na preruseni od motoru
C_TIMED EQU   18   ; Deleni na 25 Hz pro MCB1 8051
C_TIM06 EQU   15   ; Deleni z 25 Hz na 0.6 s (0.01 min)
C_TMIN  EQU   100  ; Deleni na 1 min
N_OF_T  EQU   2    ; Pocet timeru
TIMR1:  DS    1    ; Timery jsou decrementovany
TIMRI:  DS    1    ; Delicka 0.01 min
TIMRJ:  DS    1    ; Delicka na minuty
TIME:   DS    2    ; Cas v min              =====
TIM_DIF:DS    1    ; Rozdil casu mezi procesory

; Delky jednotlivych pipnuti
BP_OK   EQU   3
BP_WA   EQU   20
BP_ERR  EQU   60

; Jednotlive chybove kody
ERBUB	EQU   01H  ; Bublinu
ERDROP  EQU   02H  ; Kapky
ERINT   EQU   10H  ; Vnitrni chyba
ERINTSW EQU   10H  ;  Vnitrni chyba softu
ERINTSM EQU   11H  ;   Neopravneny rezim
ERINTST EQU   12H  ;   Neopravneny start cerpani
ERINTHW EQU   14H  ;  Vnitrni chyba hardware
ERFLTS  EQU   14H  ;   Rozchazi se davkovani mezi procesory
ERTIME  EQU   15H  ;   Rozchazi se hodiny procesoru
ERTRON  EQU   16H  ;   Chyba spinacich tranzistoru
ERTRHI  EQU   17H  ;   Chyba horniho tranzistoru
ERINTMS EQU   18H  ;   Rychlost motoru neodpovida
ERRCOMM EQU   19H  ;   Chyba komunikace
ERCOMMT EQU   1AH  ;   Chyba komunikace - time out
ERCAPNP EQU   1FH  ;   Neni pripojene CAP cidlo
ERNCMD  EQU   20H  ; Dele nez 2 min bez povelu
ERPRCUR EQU   42H  ; Prekrocen proud motorem => tlak limit
EREND   EQU   50H  ; Konec davky
ERENDTI EQU   51H  ;  Prekrocen casovy limit
ERENDVO EQU   53H  ;  Prekrocen limit objemu
ERENDKV EQU   58H  ;  Prechod do rezimu KVO
ERENDKE EQU   59H  ;  Ukonceni rezimu KVO
ERDOOR	EQU   70H  ; Otevrena dvirka
ERSET	EQU   71H  ;  Nezalozeny set
EREEPR  EQU   88H  ; Chyba pri cteni udaju z EEPROM
ERST    EQU   90H  ; Chyba pri startu
ERSTAD  EQU   91H  ;  Chyby prevodniku
ERSTSYN EQU   99H  ;  Chyba synchronizace s druhym procesorem
ERSTWDT EQU   9AH  ;  Chyba ve watchdogu
ERSTSNU EQU   9BH  ;  Chyba v seriovem cislu
ERSTSNC EQU   9CH  ;  Nekompatabilni procesory

ERRNUM: DS    1    ; Cislo chyby

A_MSK:  DS    2    ; Maska pozadavku na informace
A_PER:  DS    1    ; Perioda vysilani bloku informaci
A_CPOS: DS    1    ; Pozice cteni prikazu
A_OPOS: DS    1    ; Pozice zapisu dat
TIMR_A: DS    1    ; Casovac vysilani A_SYS


MOD_SLV:DS    1
MOD_II: DS    1    	      ; Pracovni mod druheho procesoru
M_MSK	EQU   0F0H
M_SNORM EQU   0C0H            ; Stop
M_COMRD EQU   0C8H            ; Macteni dat z pocitace
M_RNORM EQU   0D0H            ; Cerpani
M_SERV  EQU   0E0H            ; Servisni rezim

C_TRER	EQU   30
CN_TRER:DS    1	   ; Zpozdeni chyby ERTRHI pri vypnuti

SLAV_BL	EQU   8
IIC_INP:DS    SLAV_BL
TMP:
IIC_OUT:DS    SLAV_BL

RSEG    IC____I

MAX_TIM:DS    2    ; Limit casu
MAX_PRE:DS    2    ; Limit tlaku
MAX_VOL:DS    2    ; Limit objemu
II_TIM:	DS    2    ; Aktualni cas
II_PRE:	DS    2    ; Aktualni tlak
II_VOL:	DS    2    ; Aktualni objem

A_BUF:  DS    20H

;=================================================================

RSEG IC____D

FLOW:   DS    2    ; Prutok

IRC_ACT:DS    2    ; Aktualni pozice z IRC cidla druheho procesoru
CRF_IRC0 EQU  2697 ; MAXON 84.3*8*4  - pocet IRC na otacku vacky
CRF_IRC1 EQU   933 ; MAXON 29.16*8*4 - pocet IRC na otacku vacky
CRF_IRC2 EQU  2144 ; ESCAP 67*8*4    - pocet IRC na otacku vacky
CRF_IRC3 EQU   800 ; ESCAP 25*8*4  - pocet IRC na otacku vacky
CRF_VOL EQU   800
RF_VOL: DS    2    ; Objem v 0.1 ml vydavkovany za CRF_IRC/IRC_RAT
		   ; otacek vacky
;CRF_IRC2 EQU  2816 ; ESCAP 88*8*4    - pocet IRC na otacku vacky
;CRF_IRC3 EQU   966 ; ESCAP 30.2*8*4  - pocet IRC na otacku vacky

RSEG    IC____I

ROT_TOL EQU   20; 10 ;5   !!! ; Min presnost jedne otacky v 0.4 procenta
ROT_ABS EQU   20; 20 ;10  !!! ; Max chyba v poctu tiku 450 Hz;
		   ; 375 tiku pro 1000 ml/h
ROT_TIM:DS    3    ; Timer otacky vacky
ROT_PER:DS    3    ; Vypoctena perioda otacky vacky

;=================================================================
; Programovy segment

RSEG IC____C

START:  MOV   IEN0,#00000000B ; zakaz preruseni
	MOV   IEN1,#00000000B
	MOV   IP0, #00000000B ; priority preruseni
	MOV   IP1, #00000000B ;
	%WATCHDOG	      ; Nulovani watch-dogu
	MOV   TMOD,#00010001B ; timer 1 mod 1; timer 0 mod 1
	MOV   TCON,#01010101B ; citac 0 a 1 cita ; interapy hranou
	MOV   TM2CON,#00000000B; timer 2, 16 bit OV
	MOV   CTCON,#001H     ; CTI0 nastaveno pri hrane P1.0
	MOV   SCON,#11011000B ; dva stopbity
	MOV   PCON,#10000000B ; Bd = OSC/12/16/(256-TH1)
	MOV   TH1,#0FAH       ; 9600Bd
	MOV   PSW,#0          ; banka registru 0
	MOV   SP,#STACK       ; inicializace zasobniku
	MOV   P3,#0FFH
	MOV   P1,#0FFH

	MOV   PWMP,#2         ; PWM frekvence 23/3 kHz
	MOV   PWM0,#080H      ; Strida 1:1

	MOV   PSW,#AR0
	MOV   B,#093H         ; Test instrukcniho souboru
	MOV   0,#055H
	MOV   A,#0F0H
	XRL   A,R0    ; A5
	MOV   9,#18H
	SETB  PSW.3
	MOV   @R1,A
	INC   R1
	MOV   @R1,#0C3H
	SETB  PSW.4   ; R0=A5, R1=C3
	ADD   A,B     ; A=38, PSW=9F
	SUBB  A,PSW
	RLC   A
	ANL   A,#NOT 4
	CJNE  A,#031H,V1ERR_H
	MUL   AB
	ADDC  A,B
	MOV   B,R0
	XRL   B,#0A0H
	DIV   AB
	SWAP  A
	XRL   A,B
	XRL   A,R1
	JZ    ST1_0
V1ERR_H:MOV   B,#5
V2ERR_H:JMP   ERR_HL1

ST1_0:	MOV   PSW,#AR0
	CLR   A               ; Test pameti eprom
	MOV   R2,#0EEH
	MOV   R3,#020H
	MOV   R4,A
	MOV   R5,A
	MOV   DPTR,#0
ST1_1:  CLR   A
	MOVC  A,@A+DPTR
	INC   DPTR
	ADD   A,R4
	MOV   R4,A
	MOV   A,R5
	ADDC  A,#0
	MOV   R5,A
	DJNZ  R2,ST1_1
	%WATCHDOG
	DJNZ  R3,ST1_1
	CLR   A
	MOVC  A,@A+DPTR
	ADD   A,R4
	MOV   R4,A
	INC   DPTR
	CLR   A
	MOVC  A,@A+DPTR
	ADDC  A,R5
	ORL   A,R4
	MOV   B,#7
	JNZ   V2ERR_H         ; !!!!!!!!!!!!!!!

ST2_0:  CLR   EA              ; Test pameti RAM
	MOV   R0,#0FFH
	MOV   A,@R0
	MOV   B,A
	MOV   DPL,A
	MOV   SP,R0
ST2_1:  ADD   A,#7
	PUSH  ACC
	CJNE  A,B,ST2_1
	%WATCHDOG             ; Nulovani watch-dogu
ST2_2:  INC   SP
	ADD   A,#7
	POP   B
	CJNE  A,B,V1ERR_H
	XRL   B,#0FFH
	PUSH  B
	CJNE  A,DPL,ST2_2
	%WATCHDOG             ; Nulovani watch-dogu
	MOV   DPH,#0
ST2_3:  INC   SP
	ADD   A,#7
	POP   B
	XRL   B,#0FFH
	CJNE  A,B,V1ERR_H
	PUSH  DPH             ; Nulovani pameti ram
	CJNE  A,DPL,ST2_3
	CLR   A
	CJNE  A,0,V1ERR_H
	MOV   SP,#STACK

	MOV   DPH,#HIGH LED1  ; Test AD prevodniku
	MOV   A,#0CFH
	MOVX  @DPTR,A
	CALL  DB_W_10
	MOV   R1,#ST_ADt-ST_D_Sk
	MOV   R7,#ERSTAD-1
ST_AD_1:INC   R7
	MOV   B,#1
	CALL  ST_D_S0
	JZ    ST_SYN
	CALL  GET_ADN
%IF (1) THEN (
	CALL  ST_D_S
	CALL  CMPi
	JC    ST_AD_E
	CALL  ST_D_S
	CALL  CMPi
	JNC   ST_AD_E
)ELSE(
	CALL  ST_D_S
	CALL  ST_D_S
	PUSH  AR1
	SETB  EA
	SETB  TR1
	SETB  ET1
	CLR   EBP
	CALL  PRTR45
ST_AD_3:CALL  ST_WDC          ; Nulovani watch-dogu
	CALL  SCANKEY
	JZ    ST_AD_3
	POP   AR1
)FI
	SJMP  ST_AD_1

ST_AD_E:MOV   ERRNUM,R7
	SJMP  ST_SYN

ST_D_S: MOV   B,#2
ST_D_S0:MOV   R0,#AR2
ST_D_S1:MOV   A,R1
	INC   R1
	MOVC  A,@A+PC
ST_D_Sk:MOV   @R0,A
	INC   R0
	DJNZ  B,ST_D_S1
	RET

ST_ADt: DB    0

;=============================================================
; Provedeni synchronizace s druhym procesorem

PPSYNSN:MOV   IIC_OUT,A
	JNB   FL_SERV,PPSYNS0
	MOV   A,#055H
PPSYNS0:MOV   IIC_OUT+2,A
	MOV   IIC_OUT+1,ERRNUM
	MOV   IIC_OUT+3,#SOFT_VER
PPSYNS1:CALL  SND_OUT
	JNB   ACC.2,PPSYNCR
	CALL  PPSYNW
	SJMP  PPSYNS1

PPSYNRC:MOV   TMP+2,#6H
PPSYNR1:JB    ICF_SRC,PPSYNR3 ; Cekani na druhy procesor
	CALL  PPSYNW
	SJMP  PPSYNR1
PPSYNR3:MOV   R0,#IIC_INP
	CALL  XOR_SU0         ; Kontrola prijateho xorsumu
	XRL   A,@R0
	JNZ   PPSYNER
	MOV   A,IIC_INP+1
	JNZ   PPSYNE1
	MOV   R7,IIC_INP+2
	MOV   A,R7
	XRL   A,#0AAH
	JZ    PPSYNR4
	CLR   FL_SERV
PPSYNR4:MOV   A,IIC_INP+3
	XRL   A,#SOFT_VER
	JZ    PPSYNR5
	MOV   ERRNUM,#ERSTSNC
PPSYNR5:MOV   A,IIC_INP
PPSYNCR:RET

ST_SYN:	JB    SERV_SW,ST_SYN2 ; !!!!!!!!!!!!!!!!!!!!!
	SETB  FL_SERV
ST_SYN2:MOV   S1ADR,#30H      ; Nastaveni adresy stanice
	MOV   R4,#IIC_INP
	MOV   R2,#SLAV_BL OR 80H
	MOV   R6,#0
	MOV   R7,#0
	CALL  IIC_SLI
	SETB  EA
	CALL  PPSYNRC
	CLR   ICF_SRC
	CJNE  A,#0A3H,ST_SYN3
	MOV   A,#0A4H
	CALL  PPSYNSN
	JMP   STARTC

ST_SYN3:CJNE  A,#0A0H,PPSYNER
	MOV   A,#0A1H
	CALL  PPSYNSN
	CALL  PPSYNRC
	CJNE  A,#0A0H,PPSYNER
	MOV   A,#0A2H
	CALL  PPSYNSN
PPSYNT:	CLR   EA
	CALL  ST_WDC
	MOV   TMP+1,#0B0H
PPSYNT0:DJNZ  TMP,PPSYNT0
	DJNZ  TMP+1,PPSYNT0
	CALL  ST_WDC
	MOV   A,#ERSTWDT
	SJMP  PPSYNE1

PPSYNW: DJNZ  TMP,ST_WDC
	DJNZ  TMP+1,ST_WDC
	DJNZ  TMP+2,ST_WDC
PPSYNER:MOV   A,#ERSTSYN
PPSYNE1:MOV   ERRNUM,A
	JMP   ERR_HLT

; Bezpecne nulovani watchdogu se zastavenim cerpani
ST_WDC: %WATCHDOG
	SETB  ELED
	CLR   FL_MOT
	SETB  MOTOFF
	RET

;=============================================================
; Start komunikace s uzivatelem

STARTC:	MOV   AD_CNT,#80H     ; Zpusteni snimani prevodniku
	MOV   P4,#P4_RES
	CLR   TR0             ; Priprava casoveho preruseni
	MOV   TH0,#0FFH
	MOV   TL0,#0FFH
	SETB  TR0
	ORL   IEN0,#10000010B ; Spusteni casoveho preruseni

	CLR   F0
	MOV   R0,#87H         ; Parametry konfigurace
	CALL  EEPA_RD
	JB    F0,STARTC0
	MOV   CFG_FLG,IIC_OUT+2
STARTC0:
	MOV   RF_VOL+0,#LOW  CRF_VOL
	MOV   RF_VOL+1,#HIGH CRF_VOL

	CLR   F0
	MOV   R0,#80H         ; Kalibrace kapacitniho cidla
	CALL  EEPA_RD
	JB    F0,STARTC1
	MOV   RF_VOL+0,IIC_OUT+1; Nacteni referencniho objemu
	MOV   RF_VOL+1,IIC_OUT+2
STARTC1:CLR   F0
	MOV   R0,#95H         ; Kontrola serioveho cisla
	CALL  EEPA_RD
	JB    F0,STARTC2
	MOV   R0,#IIC_OUT+1
	CALL  CMP_SN
	JZ    STARTC4
STARTC2:JB    FL_SERV,STARTC3
	MOV   A,#ERSTSNU
	CALL  SET_ERR
	SJMP  STARTC4
STARTC3:MOV   R0,#IIC_OUT+1
	MOV   DPTR,#SER_NUM
	MOV   R2,#4
	CALL  MOVsci
	CLR   F0
	MOV   R0,#95H         ; Kontrola serioveho cisla
	CALL  EEPA_WR
STARTC4:CLR   A
	MOV   R0,#AD_MCUR
	MOV   @R0,A
	INC   R0
	MOV   @R0,A
	MOV   R0,#AD_BAT
	MOV   @R0,A
	INC   R0
	MOV   @R0,#30H

	MOV   R5,#6           ; Rychlost 9600
	JNB   FL_UL48,STARTC5
	MOV   R5,#12          ; Rychlost 4800
STARTC5:CALL  uL_INIT	      ; Zpusteni uLan komunikace

	MOV   C,FL_EREE
	ANL   C,/FL_SERV
	JNC   C0
	MOV   A,#EREEPR
STARTCE:MOV   ERRNUM,A
C0:	MOV   MOD_SLV,#0C0H
	MOV   PPCNT,#1
C1:	CALL  CREC
CP1:    MOV   R0,MOD_SLV

%IF (%DEBUG_FL) THEN (
	CJNE  R0,#0,CP2       ; MOD_SLV=0 testovani vysilani na
	MOV   R5,TMP          ; slave 10H
	MOV   R4,TMP+1
	MOV   R6,#18H
	CALL  iPRTLhw
	MOV   R6,#18H
	CALL  OUT_BUF
     ;	%WATCHDOG	      ; Nulovani watch-dogu
	JB    ACC.0,C1

	MOV   A,TMP
	ORL   A,TMP+1
	MOV   DPTR,#LED0
	MOVX  @DPTR,A
	MOV   DPTR,#LED1
	MOVX  @DPTR,A
	MOV   DPTR,#LED2
	MOVX  @DPTR,A

	MOV   R1,#0
C2:     NOP
	DJNZ  R0,C2
     ;	%WATCHDOG	      ; Nulovani watch-dogu
	DJNZ  R1,C2
C3:  ;  %WATCHDOG	      ; Nulovani watch-dogu
	MOV   R6,#10H
	MOV   R4,#TMP
	MOV   R2,#2
	MOV   R3,#0
	CALL  IIC_RQI
	JB    ACC.0,C3
C4:     JBC   ICF_MER,C5
	JB    ICF_MRQ,C4
	INC   TMP
	JMP   C1
C5:	INC   TMP+1
C2_ER:	CLR   ICF_MRQ
	SETB  AA
	JMP   C1

CP2:    MOV   A,R0
	ANL   A,#0F0H
	CJNE  A,#010H,CP3     ; SLV_MOD=10 az 17h vystup prevodniku
	MOV   A,R0            ; na LCD
	ANL   A,#07H
	MOV   ADCON,A
	ORL   A,#08H
	MOV   ADCON,A
	MOV   R1,#10H
CP2_1:	NOP
	DJNZ  R2,CP2_1
      ;	%WATCHDOG	      ; Nulovani watch-dogu
	DJNZ  R1,CP2_1
	MOV   R4,ADCON
	MOV   R5,ADCH
	CLR   EA
	MOV   IIC_OUT,R4
	MOV   IIC_OUT+1,R5
	SETB  EA
	MOV   A,R0
	JB    ACC.3,CP2_2
	MOV   R6,#18H
	CALL  iPRTLhw
	MOV   R6,#18H
	CALL  OUT_BUF
	JMP   C1

CP2_2:	JBC   ICF_MER,C2_ER   ; SLV_MOD=18 az 1Fh Posilani dat z ADC na IIC
	MOV   R6,#10H
	MOV   R4,#IIC_OUT
	MOV   R2,#2
	MOV   R3,#0
	CALL  IIC_RQI
	JMP   C1

CP3:    CJNE  R0,#1,CP4       ; SLV_MOD=1 test cteni IIC_OUT
	MOV   IIC_OUT,#012H
	INC   IIC_OUT+1
	JMP   C1

CP4:    CJNE  R0,#2,CP5       ; SLV_MOD=2  vypis ????
	;R45
	MOV   R6,#18H
	CALL  iPRTLhw
	MOV   R7,#0C4H
;	CALL  iPRTLi
	MOV   R6,#18H
	CALL  OUT_BUF
	MOV   R1,#80H
CP4_1:	NOP
	DJNZ  R2,CP4_1
      ;	%WATCHDOG	      ; Nulovani watch-dogu
	DJNZ  R1,CP4_1
	JMP   C1
)FI

CP5:    MOV   A,R0            ; SLV_MOD=C0 pracovni rezim
	ADD   A,#-0C0H
	ADD   A,#-8
	JC    CP6
	CALL  PPSYS
	JNB   FL_EDRF,CP5_50
	CLR   FL_EDRF
CP5_50:	JMP   C1

CP6:
	JMP   C1

URECNO:	RET

CREC:   JB    ICF_SRC,CREC0
	MOV   R0,#BEG_IB
	MOV   A,@R0
	JNB   ACC.7,URECNO
	INC   R0              ; Zpracovani zprav uLan
	MOV   A,@R0
	CJNE  A,#07FH,UREC10
	INC   R0
	MOV   A,@R0
	CLR   C
	SUBB  A,#BEG_IB+3+1+4+1
	JC    URECRV1
	INC   R0
	MOV   A,@R0
	CJNE  A,#0C1H,URECRV1
	INC   R0
	CALL  CMP_SN
	JNZ   URECRV1
	MOV   uL_ADR,@R0
	ANL   uL_DYFL,#NOT 7
URECRV1:JMP   URECR
UREC10: CJNE  A,#024H,UREC20
	MOV   A,MOD_II
	XRL   A,#M_COMRD
	JNZ   URECRV1
	INC   R0
	INC   R0
	MOV   FLOW,@R0
	INC   R0
	MOV   FLOW+1,@R0
	INC   R0
	MOV   A,R0
	MOV   R1,A
	MOV   R0,#MAX_TIM
	MOV   R2,#6
	CALL  MOVsi
	SETB  FL_SETL
	JMP   URECR
UREC20:

URECR:	MOV   R0,#BEG_IB
	MOV   @R0,#0
	RET

CREC0:	MOV   R0,IIC_INP      ; Zpracovani zprav IIC
	CJNE  R0,#1,CREC1     ; Prikaz 1 nastaveni MOD_SLV
	MOV   MOD_SLV,IIC_INP+1
CREC1:	CJNE  R0,#2,CREC2     ; Prikaz 2 nastaveni LED
	MOV   DPTR,#LED0
	MOV   A,IIC_INP+1
	MOVX  @DPTR,A
	MOV   DPTR,#LED1
	MOV   A,IIC_INP+2
	MOVX  @DPTR,A
	MOV   DPTR,#LED2
	MOV   A,IIC_INP+3
	MOVX  @DPTR,A
CREC2:  CJNE  R0,#3,CREC3     ; Prikaz 3 zkouska citace casu
	MOV   TIMR1,#0FFH
CREC2_1:MOV   R4,TIME_CN
	MOV   R5,TIMR1
	MOV   R6,#18H
	CALL  iPRTLhw
	MOV   R6,#18H
	CALL  OUT_BUF
	MOV   A,TIMR1
	JNZ   CREC2_1
	JMP   CRR
CREC3:  CJNE  R0,#4,CREC4     ; Prikaz 4 nulovani citace polohy
	JMP   CRR

CREC4:  CJNE  R0,#80H,CREC5   ; Vstup z TTY
	MOV   R4,IIC_INP+1
	MOV   R5,#0
	MOV   R6,#18H
	CALL  iPRTLhw
	CALL  IIC_WME
	MOV   R6,#18H
	CALL  OUT_BUF
	CALL  IIC_WME
	MOV   TMP,#'A'
	MOV   TMP+1,#'b'
	MOV   R6,#10H
	MOV   R4,#TMP
	MOV   R2,#2
	MOV   R3,#0
	CALL  IIC_RQI
	CALL  IIC_WME
	CALL  DB_WAIT
	JMP   CRR
CREC5:  CJNE  R0,#0BH,CREC6   ; Prikaz B nastaveni brany P4
	MOV   A,IIC_INP+1
	ANL   P4,#NOT P4_MSK
	ANL   A,#P4_MSK
	ORL   P4,A
	JMP   CRR

CREC6:

CREC10: CJNE  R0,#0AH,CREC11  ; Prikaz A nastaveni A_SYS
	CLR   EA
	MOV   A_PER,IIC_INP+1
	MOV   A_MSK,#1
	MOV   A_MSK+1,IIC_INP+2
	MOV   A_CPOS,#0
	MOV   A,A_PER
	JZ    CREC10R
	CLR   ICF_MER
	CLR   ICF_MRQ
	MOV   A_CPOS,#80H
CREC10R:SETB  EA
	JMP   CRR

CREC11: MOV   A,R0            ; Ping-Pong C0-FF
	XRL   A,#0C0H
	ANL   A,#0C0H
	JNZ   CREC12
	CALL  PPREC
	JMP   CRR
CREC12: CJNE  R0,#0BFH,CREC13 ; Nove nahozeni Pig-Pongu
	CLR   ICF_SRC
	MOV   MOD_SLV,#0C0H
	CALL  PPSTART
	RET

CREC13:

CRR:	CLR   ICF_SRC
	RET

DB_W_10:MOV   R0,#10H
	SJMP  DB_WAI1
DB_WAIT:MOV   R0,#0
DB_WAI1:CALL  ST_WDC
	DJNZ  R1,DB_WAI1
	DJNZ  R0,DB_WAI1
	RET

;=================================================================
; Nastaveni chyby

SET_ERR:SETB  FL_NERR
SET_ERRS:SETB OFF_PRO
	XCH   A,ERRNUM
	JZ    SET_ER1
	XCH   A,ERRNUM
SET_ER1:SETB  ELED
	CLR   FL_MOT
	SETB  MOTOFF
	RET

;=================================================================
; Indikace typu strikacek

%IF (0) THEN (
DI_SPR: MOV   R0,#0FFH
	MOV   A,@R0
	MOV   R0,#8
DI_SPRT:CLR   LED_CLK
	RLC   A
	MOV   LED_DAT,C
	SETB  LED_CLK
	DJNZ  R0,DI_SPRT
	RET
)FI

;=================================================================
; Ping-Pong mezi procesory

PPRECE: MOV   A,#ERRCOMM
	JMP   SET_ERR

PPREC:  CALL  PPINC
	CJNE  A,IIC_INP,PPRECE
	MOV   A,S_RLEN
	CJNE  A,#SLAV_BL,PPRECE
	MOV   R0,#IIC_INP
	CALL  XOR_SU0
	XRL   A,@R0
	JNZ   PPRECE
	MOV   PPTIMR,#PPT_MAX ; Natazeni watchdogu PP
	CALL  PPSW
	RET

PPSTART:MOV   PPCNT,#0
	MOV   PPTIMR,#PPT_MAX ; Natazeni watchdogu PP
	SJMP  PPSEND
PPSYS:  MOV   A,PPTIMR        ; Test casove prodlevy
	ADD   A,#-PPT_MAX+PPT_SND
	JC    PPSRET          ; Nenadesel jeste cas
PPSEND: JB    PPCNT.0,PPSRET
	CALL  PPINC
	MOV   IIC_OUT,A
	CALL  PPSW
SND_OUT:MOV   R0,#IIC_OUT
	CALL  XOR_SU0
	MOV   @R0,A

%IF (%DEBUG_FL) THEN (
	MOV   A,PPCNT
	SWAP  A
	XRL   A,PPCNT
	ANL   A,#03H
	JNZ   SND_OU1
)FI
SND_OU0:
%IF (%DEBUG_FL) THEN (
	CALL  IIC_CER
	MOV   R6,#10H
	MOV   R4,#IIC_OUT
	MOV   R2,#SLAV_BL
	MOV   R3,#0
	CALL  IIC_RQI
	JNZ   SND_OU0
	CALL  IIC_WME
)FI

SND_OU1:CALL  IIC_CER
	MOV   R6,#20H
	MOV   R4,#IIC_OUT
	MOV   R2,#SLAV_BL
	MOV   R3,#0
	CALL  IIC_RQI
	JNZ   SND_OU1
	CALL  IIC_WME
PPSRET: RET

PPINC:  MOV   A,PPCNT
	INC   A
	ANL   A,#03FH
	ANL   PPCNT,#0C0H
	ORL   PPCNT,A
	ORL   A,#0C0H
	RET

XOR_SU0:MOV   R2,#SLAV_BL-1
XOR_SUM:CLR   A
XOR_SU1:MOV   R3,A
	MOV   A,@R0
	INC   R0
	XRL   A,R3
	INC   A
	DJNZ  R2,XOR_SU1
	RET

PPSW:   MOV   A,PPCNT
	ANL   A,#00FH
	RL    A
	MOV   R0,A
	ADD   A,#PPSWT-PPSWk1
	MOVC  A,@A+PC
PPSWk1: PUSH  ACC
	MOV   A,R0
	ADD   A,#PPSWT-PPSWk2+1
	MOVC  A,@A+PC
PPSWk2: PUSH  ACC

PPSWRET:RET

PPSWT:  %W    (PPSW0)
	%W    (PPSW1)
	%W    (PPSW2)
	%W    (PPSW3)
	%W    (PPSW4)
	%W    (PPSW5)
	%W    (PPSW6)
	%W    (PPSWRET)
	%W    (PPSW8)
	%W    (PPSW9)
	%W    (PPSWA)
	%W    (PPSWB)
	%W    (PPSWRET)
	%W    (PPSWD)
	%W    (PPSWE)
	%W    (PPSWRET)

PPSW0:  CLR   EA
	MOV   IRC_ACT,IIC_INP+2
	MOV   IRC_ACT+1,IIC_INP+3
	SETB  EA
	MOV   A,ERRNUM
	JNZ   PPSW010
	MOV   A,IIC_INP+6     ; ERRNUM druheho procesoru
	JZ    PPSW010
	CALL  SET_ERRS
PPSW010:JB    FL_MOT,PPSW012
	MOV   A,MOD_II
	XRL   A,#M_COMRD
	JZ    PPSW012
	MOV   FLOW  ,IIC_INP+4; Prutok z druheho procesoru
	MOV   FLOW+1,IIC_INP+5
PPSW012:RET

PPSW1:  MOV   R4,#LOW  1700H  ; Dostatecna uroven vnejsiho napajeni
	MOV   R5,#HIGH 1700H
	MOV   R0,#AD_DCIN
	CALL  iLDR23i
	CALL  CMPi
	MOV   FL_DCIN,C
	MOV   IIC_OUT+1,HW_FLG
	MOV   IIC_OUT+2,SW_FLG
    %IF(1)THEN(
	JBC   FL_EDROP,PPSW16
	SJMP  PPSW17
PPSW16:	MOV   A,#ERDROP
	CALL  SET_ERR
PPSW17:
    )FI
	CLR   FL_NERR
	MOV   IIC_OUT+3,ERRNUM
	RET

PPSW2E:	MOV   A,#ERINTSM
	CALL  SET_ERR
	JMP   PPSWPPF

PPSW2:  CLR   FL_FCYC
	MOV   A,IIC_INP+6     ; MOD_SLV druheho procesoru
	MOV   R7,A
	ANL   A,#0F0H
	CJNE  A,#M_SERV,PPSW201
	JNB   FL_SERV,PPSW2E
PPSW201:MOV   MOD_II,IIC_INP+6
	MOV   R7,IIC_INP+2    ; OUT_MSG druheho procesoru
PPSW210:CJNE  R7,#OMS_CER,PPSW214 ; Prikaz nulovani chyby
	MOV   A,ERRNUM
	JB    ACC.7,PPSW214
	MOV   A,IIC_INP+3
	CJNE  A,#055H,PPSW214
	CLR   FL_EDROP		; Zrusit priznak chyby kapek
	MOV   ERRNUM,IIC_INP+4
	CLR   ELED
	MOV   R0,#AD_BAT
	MOV   @R0,A
	INC   R0
	MOV   @R0,#30H
PPSW214:CJNE  R7,#OMS_CCH,PPSW230
	JNB   FL_SERV,PPSW2E
	MOV   CFG_FLG,IIC_INP+3
PPSW230:JMP   PPSWPPF

PPSW3:  CLR   EA
	MOV   R5,#0FFH
	MOV   A,ERRNUM
	JNZ   PPSW350
	MOV   R0,#AD_BAT
	CALL  iLDR45i
PPSW350:MOV   IIC_OUT+1,R4
	MOV   IIC_OUT+2,R5
	SETB  EA
	RET

PPSW4:  MOV   R0,#II_TIM
	JMP   PPSW899

PPSW5:	RET

PPSW6:	MOV   A,IIC_INP+5     ; SW_FLG z II
	JB    FL_MOT,PPSW620
	MOV   C,ACC.1	      ; FL_DROP
	MOV   FL_DROP,C
PPSW620:JNB   ACC.0,PPSW621   ; FL_DATA
	MOV   A,ERRNUM
	JNZ   PPSW621
	CLR   OFF_PRO
	SJMP  PPSW622
PPSW621:SETB  OFF_PRO
PPSW622:
PPSWPPF:MOV   B,IIC_INP+1     ; Zpracovani PP_FLG z II
	JB    B.6,PPSW654     ; FL_STKV
	CLR   FL_STKV
PPSW654:JNB   B.0,PPSW655     ; FL_BEOK
	MOV   BPTIMR,#BP_OK
PPSW655:JNB   B.1,PPSW657     ; FL_BEER
	MOV   BPTIMR,#BP_ERR
PPSW657:JNB   B.5,PPSW658     ; FL_PPTI
	INC   TIM_DIF
PPSW658:MOV   A,B
	ANL   A,#01001100B    ; FL_STKV | FL_BALO | FL_NEND
	JZ    PPSW659
	SETB  FL_BEWA
PPSW659:MOV   A,MOD_II
	ANL   A,#0F0H
	MOV   R1,A
	CJNE  R1,#M_SERV,PPSW661
	SETB  FL_MOT
	JMP   PPSW699
PPSW660:JMP   PPSW698	      ; Vypnout motor
PPSW661:MOV   A,ERRNUM
	JNZ   PPSW660
	JB    B.6,PPSW662     ; FL_STKV
	JNB   B.7,PPSW660     ; FL_STRT
PPSW662:JB    FL_MOT,PPSW699
	MOV   A,PPCNT
	ANL   A,#00FH
	CJNE  A,#2,PPSW6ER
	MOV   B,IIC_INP+1     ; Zpracovani PP_FLG z II
	MOV   A,ERRNUM        ; Start cerpani
	JNZ   PPSW698
	SETB  FL_FROT	      ; Prvni otacka se snizenou kontrolou
	SETB  FL_FRO1
%IF(%DR2ROT_FL) THEN (
	CLR   DR_DIV1
	CLR   DR_DIV2
)FI
	CLR   CTI0
	ANL   ROT_DCN,#NOT ROT_DMS
	MOV   R0,#ROT_TIM
	CLR   A
	MOV   @R0,A
	INC   R0
	MOV   @R0,A
	INC   R0
	MOV   @R0,A
	MOV   R0,#DR_CNT
	MOV   @R0,A
	MOV   R0,#DR_IRCN
	MOV   A,#DR_IROT
	MOV   @R0,A
	MOV   C,B.6	      ; FL_STKV
	MOV   FL_STKV,C
	SETB  FL_FCYC
	MOV   R7,IIC_INP+2    ; OUT_MSG druheho procesoru
	CJNE  R7,#OMS_KVO,PPSW675
	MOV   A,FLOW+1	      ; Prutok v rezimu KVO
	JNZ   PPSW672
	MOV   A,FLOW
	MOV   R4,A
      %IF (NOT %VPD_SOFT_FL) THEN (
	ADD   A,#-3
      )ELSE(
	ADD   A,#-30
      )FI
	JNC   PPSW673
PPSW672:
      %IF (NOT %VPD_SOFT_FL) THEN (
	MOV   R4,#3
      )ELSE(
	MOV   R4,#30
      )FI
PPSW673:MOV   R5,#0
	SJMP  PPSW676
PPSW675:CJNE  R7,#OMS_STR,PPSW6ER
	MOV   R4,FLOW	      ; Prutok v rezimu START
	MOV   R5,FLOW+1
PPSW676:CALL  FL2PER	      ; Vypocet periody vacky
	MOV   R0,#ROT_PER
	MOV   A,R4
	MOV   @R0,A
	INC   R0
	MOV   A,R5
	MOV   @R0,A
	INC   R0
	MOV   A,R6
	MOV   @R0,A
	MOV   A,R7
	JNZ   PPSW6ER
	MOV   B,IIC_INP+1     ; Nacteni PP_FLG z II
	SETB  FL_MOT
	CLR   FL_ROTE
	MOV   CN_TRER,#C_TRER   ; Zpozdeni ERTRHI
	CLR   MOTOFF
	CLR   A
	MOV   TIMRJ,A
	MOV   TIME,A
	MOV   TIME+1,A
	SJMP  PPSW699
PPSW6ER:MOV   A,#ERINTST	; Chyba v software
	JMP   SET_ERR
PPSW698:CLR   FL_MOT
PPSW699:JNB   FL_MOT,PPSW69A
	JNB   FL_ROTE,PPSW69A
	MOV   A,#ERFLTS		; Neprisla otacka vacky
	CALL  SET_ERR
PPSW69A:MOV   A,MOD_II
	ANL   A,#0F0H
	CJNE  A,#M_RNORM,PPSW6R1
	JNB   FL_MOT,PPSW6ER
PPSW6R1:RET

PPSW8:  MOV   A,MOD_II
	XRL   A,#M_COMRD
	JZ    PPSW8R
	MOV   R0,#MAX_TIM
PPSW899:MOV   R1,#IIC_INP+1
	MOV   R2,#6
	CLR   EA
	CALL  MOVsi
	SETB  EA
PPSW8R:	RET

PPSW9:  MOV   R1,#MAX_TIM
	MOV   R0,#IIC_OUT+1
	MOV   R2,#6
	JMP   MOVsi

PPSWA:	MOV   R2,#LOW  3000H  ; Test proudu motorem
	MOV   R3,#HIGH 3000H
	MOV   R0,#AD_MCUR
	CLR   EA
	CALL  iLDR45i
	SETB  EA
	CALL  CMPi
	CPL   C
	MOV   FL_MOV,C
	JNC   PPSWA15
	MOV   A,#ERPRCUR
	CALL  SET_ERR
PPSWA15:MOV   A,MOD_II
	ANL   A,#0F0H
	CJNE  A,#M_RNORM,PPSWA50
PPSWA50:MOV   R0,#IIC_INP+2
	MOV   R1,#AD_BUB1
	MOV   R2,#4
	CLR   EA
PPSWA70:MOV   A,@R0
	MOV   @R1,A
	INC   R0
	INC   R1
	DJNZ  R2,PPSWA70
	SETB  EA
	JMP   PPSWPPF

PPSWB:  CLR   A
	JNB   FL_SETL,PPSWB10
	CPL   A
PPSWB10:MOV   IIC_OUT+4,A
	MOV   IIC_OUT+5,FLOW
	MOV   IIC_OUT+6,FLOW+1
	CLR   FL_SETL
	CLR   A
	JNB   FL_NERR,PPSWB20
	CLR   FL_NERR
	MOV   A,ERRNUM
PPSWB20:MOV   IIC_OUT+3,A     ; Zrychleny prenos chyby
	RET

PPSWD:  MOV   A,#80H	      ; Zobrazit kapicku
	JBC   DR_OCFL,PPSWD20
	CLR   A
PPSWD20:MOV   IIC_OUT+6,A
	MOV   A,MOD_II
	XRL   A,#M_RNORM
	ANL   A,#M_MSK
	JNZ   PPSWD40
	JNB   FL_MOT,PPSWD40
	MOV   R4,FLOW	      ; Prutok v rezimu start
	MOV   R5,FLOW+1
	JNB   FL_STKV,PPSWD25
      %IF (NOT %VPD_SOFT_FL) THEN (
	MOV   R2,#3	      ; Prutok v rezimu KVO
	MOV   R3,#0           ; min(3,FLOW)
      )ELSE(
	MOV   R2,#30	      ; Prutok v rezimu KVO
	MOV   R3,#0           ; min(3.0,FLOW)
      )FI
	CALL  CMPi
	JC    PPSWD25
	MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
PPSWD25:CALL  FL2PER	      ; Vypocet periody vacky
	MOV   R0,#ROT_PER
	MOV   A,@R0
	XRL   A,R4
	MOV   R4,A
	INC   R0
	MOV   A,@R0
	XRL   A,R5
	ORL   A,R4
	MOV   R4,A
	INC   R0
	MOV   A,@R0
	XRL   A,R6
	ORL   A,R4
	ORL   A,R7
	JZ    PPSWD30
PPSWD28:MOV   A,#ERINTSW
	CALL  SET_ERR
	SJMP  PPSWD40
PPSWD30:
PPSWD40:RET

PPSWE:  JNB   uLD_RQA,PPSWE05
	MOV   A,PPCNT
	ANL   A,#03FH
	CJNE  A,#00EH,PPSWE05
	CALL  SNRQ
PPSWE05:MOV   A,IIC_INP+6     ; SW1_FLG z II
	JNB   ACC.4,PPSWE20   ; FL_TRTS
	JNB   FL_MOT,PPSWE20
	JMP   PPSWE11         ; !!!!!!!!!!!!!!
	MOV   A,#6
	CALL  GET_ADN
	MOV   A,R5
	MOV   R7,A
	ADD   A,#-30H         ; Kontrola funkce spinacu
	JNC   PPSWE10
	MOV   A,#5
	CALL  GET_ADN
	MOV   A,R5
	SUBB  A,R7
	ADD   A,#8
	ADD   A,#-28H
	JNC   PPSWE11
PPSWE10:MOV   A,#ERTRON
	CALL  SET_ERR
PPSWE11:SETB  AD_EN
PPSWE20:JMP   PPSWPPF

;=================================================================
; Ruzne vypocty

; Prevadi kapacitni cidlo na [256*IRC] podle vztahu
;   (CAP_MAX-CAP_ACT)/CAP_DIV*1024

;CAP2IRC:MOV   R4,CAP_MAX
;	MOV   R5,CAP_MAX+1
	MOV   C,EA
	CLR   EA
;	MOV   R2,CAP_ACT
;	MOV   R3,CAP_ACT+1
	MOV   EA,C
	CALL  SUBi
	JC    CAP2IZ
;	MOV   R2,CAP_DIV
;	MOV   R3,CAP_DIV+1
	CLR   F0
	MOV   R0,#0
	MOV   R1,#10	      ; 2^10=1024
	CALL  DIVi0
	RET
CAP2IZ: CLR   A
	MOV   R4,A
	MOV   R5,A
	RET

; Ulozi do R23 pocet IRC na 1 otacku vacky

GRF_IRCT:MOV  A,#4	      ; R23:=10*CRF_IRC
	DB    07AH	      ; MOV  R2,#nn
GRF_IRC:CLR   A		      ; R23:=CRF_IRC
GRF_IRC1:MOV  R2,A
	MOV   A,CFG_FLG
	ANL   A,#RAT_MSK
	ADD   A,R2
	RL    A
	MOV   R2,A
	ADD   A,#CRF_TAB-GRF_IRC2
	MOVC  A,@A+PC
GRF_IRC2:XCH  A,R2
	ADD   A,#CRF_TAB-GRF_IRC3+1
	MOVC  A,@A+PC
GRF_IRC3:MOV  R3,A
	RET

CRF_TAB:%W    (CRF_IRC0)
	%W    (CRF_IRC1)
	%W    (CRF_IRC2)
	%W    (CRF_IRC3)
	%W    (CRF_IRC0*10)
	%W    (CRF_IRC1*10)
	%W    (CRF_IRC2*10)
	%W    (CRF_IRC3*10)

; Vypocte z prutoku periodu otacky vacky
;
;   Prutok             F  [1 ml/h] vstup z R45
;   Referencni davka   L  [irc]
;   Referencni davka   L1 [256*irc] = CRF_IRC
;   Referencni objem   V  [0.1 ml]  = RF_VOL
;   Frekvence intr.    N  [intr/h]  = 60*60*450 = 50625*2^5
;   Rychlost           S  [irc/int]
;   Perioda vacky      T  [int]
;   Pocet IRC na vacku I  	    = IRC_RAT
;
;                                                       5
;       F*L          I    I*V*N   IRC_RAT*RF_VOL*50625*2
;   S = ---     T = --- = ----- = -----------------------
;       V*N          S     F*L    10*FLOW*CRF_IRC*256
;
;  vstup :  R45   FLOW
;  vystup : R4567 pocet interruptu
;

FL2PER: MOV   A,R4
	MOV   R2,A
	ORL   A,R5
	JNZ   FL2PER1
	MOV   A,#0FFH
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R7,A
	RET
FL2PER1:MOV   A,R5
	MOV   R3,A
	MOV   R4,RF_VOL
	MOV   R5,RF_VOL+1
	MOV   R1,#5-8
	CALL  DIVihf	      ; RF_VOL / FLOW
%IF(0)THEN(
	CALL  GRF_IRC	      ; R23:=CRF_IRC
	CALL  MULi
	CALL  NORMMI1
)FI
	MOV   R2,#LOW  50625
	MOV   R3,#HIGH 50625
	CALL  MULi
	CALL  NORMMI1
%IF(0)THEN(
      %IF (NOT %VPD_SOFT_FL) THEN (
	CALL  GRF_IRCT	      ; R23:=10*CRF_IRC
      )ELSE(
	CALL  GRF_IRC	      ; R23:=CRF_IRC
      )FI
)ELSE(
      %IF (NOT %VPD_SOFT_FL) THEN (
	MOV   R2,#10
	MOV   R3,#0
      )ELSE(
	MOV   R2,#1
	MOV   R3,#0
      )FI
)FI
	CALL  DIVihf	      ; Vysledek vypoctu R45*2^R1
	MOV   A,R4
	MOV   R2,A
	MOV   A,R5
	MOV   R3,A
	JMP   DENOil

; Roztahne R45 exp R1 na R4567

DENOil: CLR   A
	MOV   R6,A
	MOV   R7,A
	MOV   A,R1
	JB    ACC.7,DENOil5   ; Exponent je zaporny
	ANL   A,#7            ; Posun R45 vlevo do R4567
	JZ    DENOil2
	MOV   R0,A            ; Posun o R1 mod 8
DENOil1:CLR   C
	MOV   A,R4
	RLC   A
	MOV   R4,A
	MOV   A,R5
	RLC   A
	MOV   R5,A
	MOV   A,R6
	RLC   A
	MOV   R6,A
	DJNZ  R0,DENOil1
DENOil2:MOV   A,R1
	ANL   A,#NOT 7
	RR    A
	RR    A
	RR    A
	MOV   R0,A            ; Posun o (R1 div 8)*8
	JZ    DENOilR
DENOil3:CLR   A
	XCH   A,R4
	XCH   A,R5
	XCH   A,R6
	XCH   A,R7
	DJNZ  R0,DENOil3
DENOilR:CLR   F0
	RET
DENOil5:CPL   A               ; Posum R45 vpravo a R67=0
	INC   A
	CLR   F0
	JMP   SHRi

;=================================================================

; Rotuje R4567 tak ze se cislo zkrati na R45 pocet posunu ulozi do R1

NORMMI: MOV   R1,#0
NORMMI1:MOV   A,R7
	JZ    NORMMI2
	CLR   A
	XCH   A,R7
	XCH   A,R6
	XCH   A,R5
	XCH   A,R4
	MOV   A,R1
	ADD   A,#8
	MOV   R1,A
NORMMI2:MOV   A,R6
	JZ    NORMMIR
	CLR   C
	MOV   A,R6
	RRC   A
	MOV   R6,A
	MOV   A,R5
	RRC   A
	MOV   R5,A
	MOV   A,R4
	RRC   A
	MOV   R4,A
	INC   R1
	SJMP  NORMMI2
NORMMIR:RET

;=================================================================
; Zborceni programu

ERR_HL1:
ERR_HLT:CLR   EA
	SETB  ELED
	SETB  MOTOFF
	SETB  BATTST
	CLR   AA              ; Neprijima se po IIC
	CLR   SI              ; Uvolneni IIC z prijmu
	SETB  STO             ; Uvolneni IIC z vysilani
	CLR   EA
	%WATCHDOG
	JMP   ERR_HLT

;=================================================================
; Casove preruseni

USING   2

I_TIME: PUSH  ACC	      ; Pruchod s frekvenci 450 Hz
	PUSH  PSW
	PUSH  B
	MOV   PSW,#AR0
	MOV   A,SP            ; Test stavu zasobniku
	CLR   C
	SUBB  A,#STACK
	JC    ERR_HLT
	SUBB  A,#STACK_S-4
	JNC   ERR_HLT
	CLR   TF0
	CLR   C
	CLR   TR0
S_T1_D  SET   7               ; zpozdeni rutiny
	MOV   A,TL0           ; 7 Cyklu
	SUBB  A,#LOW (M_TIMED-S_T1_D)
	MOV   TL0,A
	MOV   A,TH0
	SUBB  A,#HIGH (M_TIMED-S_T1_D)
	MOV   TH0,A
	SETB  TR0
	JMP   A_SYS

MOVsi:  MOV   A,@R1           ; @R0:=@R1 .. delka R2
	MOV   @R0,A
	INC   R0
	INC   R1
	DJNZ  R2,MOVsi
	RET

MOVsci: CLR   A        	      ; @R0:=@DPTR .. delka R2
	MOVC  A,@A+DPTR
	MOV   @R0,A
	INC   R0
	INC   DPTR
	DJNZ  R2,MOVsci
	RET

CMP_SN: MOV   DPTR,#SER_NUM
	MOV   R2,#4
CMPsci: CLR   A        	      ; @R0 ?= @DPTR .. delka R2
	MOVC  A,@A+DPTR       ; vrati ACC=0 pri rovnosti
	XRL   A,@R0
	JNZ   CMPsciR
	INC   R0
	INC   DPTR
	DJNZ  R2,CMPsci
CMPsciR:RET

;=================================================================
; Aquission subsystem

A_SYS:  MOV   A,TIMR_A
	JZ    A_SYS01
	DEC   TIMR_A
A_SYS01:MOV   A,A_CPOS
	JNZ   A_SYS10
A_SYS_R:JMP   I_TIME1
A_SYS10:ANL   A,#07FH
	JNZ   A_SYS20
	MOV   A,TIMR_A
	JNZ   A_SYS_R
	MOV   TIMR_A,A_PER
	MOV   A_CPOS,#80H-1
	MOV   R0,#A_BUF
	MOV   @R0,S1ADR
A_SYS17:INC   R0
A_SYS18:MOV   A_OPOS,R0
A_SYS19:INC   A_CPOS
A_SYS20:MOV   R0,A_OPOS
	MOV   A,A_CPOS
	JNB   ACC.4,A_SYS30
A_SYS21:MOV   R4,#A_BUF       ; Pocatek bufferu
	MOV   R5,#0
	MOV   A,R0
	CLR   C
	SUBB  A,R4
	MOV   R2,A            ; Delka = A_OPOS-A_BUF
	MOV   R3,#0
	MOV   R6,#10H         ; Na slave 10H
	CALL  IIC_RQI         ; Odesli zpravu
	JNZ   A_SYS_R         ; Nepovedlo se
	MOV   A_CPOS,#80H     ; Povedlo se cekej na dalsi TIMR_A=0
	SJMP  A_SYS_R

A_SYS30:MOV   R7,A
	MOV   R4,A_MSK
	MOV   R5,A_MSK+1
	CALL  TSTBR45
	JZ    A_SYS19
	CJNE  R7,#80H,A_SYS31
	MOV   R1,#ROT_TIM     ; Vyslani polohy z kapacitniho cidla
	MOV   R2,#2
	CALL  MOVsi
	CLR   A
	MOV   @R0,A
	INC   R0
	JMP   A_SYS18
A_SYS31:
A_SYS40:MOV   A,R7
	JNB   ACC.3,A_SYS18
	XRL   A,ADCON         ; Vysilnani prevodniku
	ANL   A,#1FH
	CJNE  A,#18H,A_SYS_R  ; Neni ukoncen prevod daneho ADC
	MOV   A,ADCON         ; ADCI=1 a ADCS=0
	CLR   ACC.4
	MOV   @R0,A
	INC   R0
	MOV   ADCON,A
	MOV   @R0,ADCH
	JMP   A_SYS17

TSTBR45:JB    ACC.3,TSTBR5A   ; Testuje bit A registru R45 vysledek v A
TSTBR4A:ANL   A,#7            ; Testuje bit A registru R4 vysledek v A
	ADD   A,#TSTBR_t-TSTBR4t
	MOVC  A,@A+PC
TSTBR4t:ANL   A,R4
	RET
TSTBR5A:ANL   A,#7            ; Testuje bit A registru R5 vysledek v A
	ADD   A,#TSTBR_t-TSTBR5t
	MOVC  A,@A+PC
TSTBR5t:ANL   A,R5
	RET

TSTBR_t:DB    001h
	DB    002h
	DB    004h
	DB    008h
	DB    010h
	DB    020h
	DB    040h
	DB    080h

;=================================================================
; Prime cteni prevodniku pri pocatecnich testech

GET_ADN:MOV   AD_CNT,#0
	MOV   C,EA
	CLR   EA
	CALL  AD_PREC
	MOV   EA,C
	JB    B.3,GET_ADN
GET_AD1:MOV   B,ADCON
	JB    B.3,GET_AD1
	MOV   R5,ADCH
	ANL   A,#7
	MOV   ADCON,A
	MOV   A,B
	ANL   A,#0C0H
	MOV   R4,A
	RET

;=================================================================
; Prubezne cteni dat z prevodniku
;
RSEG    IC____B
AD_CNT: DS    1
AD_EN   BIT   AD_CNT.7	; Povoleny prevody ADC
AD_ST   BIT   AD_CNT.6	; Odstartovan prevod pro AD_RDN
AD_DRFL BIT   AD_CNT.5	; Nastaven po dobu prevodu kapek
AD_CNTB BIT   AD_CNT.0

DR_FLG:	DS    1
DR_ONFL	BIT   DR_FLG.7
DR_OCFL	BIT   DR_FLG.6	; Prosla kapka - mazano po odeslani PP
FL_DROP	BIT   DR_FLG.5  ; Nehlidaji se kapky
DR_DIV2	BIT   DR_FLG.4  ; Pri FL_PSET hlidani kapek pres 2 otacky
%IF(%DR2ROT_FL) THEN (
DR_DIV1	BIT   DR_FLG.3  ; Dvojnasobny pocet otacek
)FI
%IF(%DRFAST_FL)THEN(
DR_BLFL	BIT   DR_FLG.2  ; Blokovani dalsi kapky
DR_SLFL	BIT   DR_FLG.1  ; Zpomaleni zmeny prahovaci urovne
)FI
RSEG    IC____D
DR_DCN:	DS    1		; Casovani snimani kapek

RSEG    IC____I
AD_DCIN:DS    2
AD_MCUR:DS    2
AD_BAT: DS    2
AD_BUB1:DS    2		      ; Preneseno z druheho procesoru
AD_BUB2:DS    2		      ; Preneseno z druheho procesoru
AD_DROP:DS    1		      ; Snimac kapicek
TR_DROP:DS    1		      ; Prahova uroven kapek
%IF(%DR2ROT_FL) THEN (
DR_MIN	SET   4 ; 4	      ; Minimalni pocet kapek na otocku
DR_MAX	SET   23 ; 15	      ; Maximalni pocet kapek na otocku
)ELSE(
DR_MIN	SET   1 ; 3	      ; Minimalni pocet kapek na otocku
DR_MAX	SET   18 ; 10	      ; Maximalni pocet kapek na otocku
)FI
DR_CNT: DS    1		      ; Citac kapek
DR_IROT	SET   3		      ; Pocet ignorovanych otacek
DR_IRCN:DS    1		      ; Citac ignorovanych otacek

RSEG    IC____C

AD_PREP:JNB   AD_EN,AD_PRER   ; Neni povolene prevadeni
	JB    AD_ST,AD_PRER
    %IF(NOT %DRFAST_FL)THEN(
	MOV   A,ADCON	      ; Rizeni prevodu kapek
	JB    ACC.3,AD_PRER
	XRL   AD_CNT,#1 SHL (AD_DRFL-AD_CNTB)
	JNB   AD_DRFL,AD_PREN ; Pristi prevod nejsou kapky
	MOV   A,#AD_DRN       ; Prevodnik kapek
	SJMP  AD_PRED	      ; Konec kapicek
    )ELSE(
	CLR   AD_DRFL
    )FI
AD_PREN:
	MOV   A,AD_CNT
	ANL   A,#0FH
	CALL  AD_PRET
	JZ    AD_PRER
	JB    ACC.7,AD_PRE2
AD_PREC:MOV   B,ADCON
	JB    B.3,AD_PRER     ; ADCS
AD_PRED:ANL   A,#07H
	MOV   ADCON,A
	SETB  ACC.3	      ; ADCS
	MOV   ADCON,A
	SETB  AD_ST
AD_PRER:RET

AD_PRE2:MOV   R7,A            ; Specialni funkce
	MOV   A,TIMRI
	DEC   A
	JZ    AD_PRE4
	INC   AD_CNT
	INC   AD_CNT
AD_PRE3:INC   AD_CNT
	INC   AD_CNT
	RET

AD_PRE4:CLR   BATTST
	MOV   A,R7
	JB    ACC.6,AD_PREC
	SJMP  AD_PRE3

%IF(%DRFAST_FL)THEN(
AD_PREP_DR:
	MOV   A,ADCON	      ; Rizeni prevodu kapek
	JB    ACC.3,AD_PRER
	SETB  AD_DRFL
	MOV   A,#AD_DRN       ; Prevodnik kapek
	SJMP  AD_PRED	      ; Konec kapicek
)FI

AD_RDR1v:JMP  AD_RDR1

AD_RDD0:MOV   A,B	      ; Prevod kapek
	ANL   A,#7
	CJNE  A,#AD_DRN,AD_RDR1v
	ANL   B,#7
	MOV   ADCON,B
	MOV   R1,DR_DCN
	MOV   A,ADCH
	MOV   R0,#AD_DROP
	MOV   @R0,A	; AD_DROP
	INC   R0
	CLR   C
	SUBB  A,@R0	; TR_DROP
	JC    AD_RDD3
	INC   @R0	; TR_DROP
	CJNE  @R0,#0,AD_RDD1
	DEC   @R0
AD_RDD1:JNB   DR_ONFL,AD_RDD2 ; Na vstupu neni kapka
   %IF(%DRFAST_FL)THEN(
	JB    DR_BLFL,AD_RDD8
   )FI
	MOV   PWM1,#020H      ; !!!!!!!!!!!!!!!!!!!!!!!!
	INC   DR_DCN
	CJNE  R1,#DR_TOFF,AD_RDR1
	CLR   DR_ONFL
AD_RDD2:MOV   DR_DCN,#0
	MOV   PWM1,#0H        ; !!!!!!!!!!!!!!!!!!!!!!!!
	SJMP  AD_RDR1
AD_RDD3:
   %IF(%DRFAST_FL)THEN(
	JBC   DR_SLFL,$+3+3
	SETB  DR_SLFL
   )FI
	DEC   @R0	      ; TR_DROP
   %IF(%DRFAST_FL)THEN(
	JB    DR_BLFL,AD_RDD8
   )FI
   %IF(NOT %DRFAST_FL)THEN(
	ADD   A,#10	      ; Odstup kapky od stredni urovne
   )ELSE(
	MOV   R2,#8
	JNB   DR_ONFL,$+3+2
	MOV   R2,#6
	ADD   A,R2	      ; Odstup kapky od stredni urovne
   )FI
	JC    AD_RDD1
	JB    DR_ONFL,AD_RDD4 ; Na vstupu je kapka
	MOV   PWM1,#0E0H      ; !!!!!!!!!!!!!!!!!!!!!!!!
	INC   DR_DCN
	CJNE  R1,#DR_TON,AD_RDR1
	SETB  DR_ONFL
	SETB  DR_OCFL
   %IF(%DRFAST_FL)THEN(
	SETB  DR_BLFL	      ; Blokovani dalsi kapky po DR_TBLK
   )FI
	MOV   R0,#DR_CNT
	INC   @R0	      ; Citani kapky
	MOV   A,@R0
	JNZ   AD_RDD4
	DEC   @R0
AD_RDD4:
	MOV   PWM1,#0FFH      ; !!!!!!!!!!!!!!!!!!!!!!!!
%IF (NOT %EP_SOFT_FL) THEN (
	MOV   C,FL_MOT
	ANL   C,/FL_DROP
	JNC   AD_RDD6
	MOV   A,MOD_II
	XRL   A,#M_SERV
	ANL   A,#M_MSK
	JZ    AD_RDD6
	MOV   R0,#DR_CNT
	MOV   A,@R0
	ADD   A,#-DR_MAX-1
	JNC   AD_RDD6
	MOV   R0,#DR_IRCN
	MOV   A,@R0
	JZ    AD_RDD5
	MOV   R0,#DR_CNT
	MOV   A,@R0
	ADD   A,#-3*DR_MAX	; Pri zacatku cerpani
	JNC   AD_RDD6
AD_RDD5:
    %IF(0)THEN(
        MOV   A,#ERDROP
	CALL  SET_ERR
    )ELSE(
        SETB  FL_EDROP
    )FI
)FI
AD_RDD6:MOV   DR_DCN,#0
AD_RDR1:CLR   AD_ST
AD_RDR:	RET
    %IF(%DRFAST_FL)THEN(
AD_RDD8:INC   DR_DCN
	CJNE  R1,#DR_TBLK,AD_RDR1
	CLR   DR_BLFL
	SJMP  AD_RDD6
    )FI

%IF(%DRFAST_FL)THEN(
AD_RD_DR:
	JNB   AD_DRFL,AD_RD
	MOV   R0,#12
AD_RD0:	MOV   A,ADCON
	JNB   ACC.3,AD_RD     ; ADCS
	DJNZ  R0,AD_RD0
)FI
AD_RD:  JNB   AD_EN,AD_RDR1
	JNB   AD_ST,AD_RDR1
	MOV   B,ADCON
	JB    B.3,AD_RDR      ; ADCS

	JNB   AD_DRFL,AD_RDN  ; Neni prevod kapek
	JMP   AD_RDD0
AD_RDN:
	MOV   A,AD_CNT
	ANL   A,#0FH
	ADD   A,#AD_TAB-AD_RD1
	MOVC  A,@A+PC
AD_RD1:	JZ    AD_RDR1
	MOV   R1,A
	JNB   ACC.6,AD_RD2
			      ; Specialni funkce
	SETB  BATTST
AD_RD2: XRL   A,B
	ANL   A,#7
	JNZ   AD_RDR1
	INC   AD_CNT
	MOV   A,AD_CNT
	ANL   A,#0FH
	ADD   A,#AD_TAB-AD_RD3
	MOVC  A,@A+PC
AD_RD3: INC   AD_CNT
	MOV   R0,A
AD_RDD:	ANL   B,#7
	MOV   ADCON,B
	CJNE  R1,#15H,AD_RD4
	JB    FL_MOT,AD_RDR1
	MOV   A,CN_TRER
	DEC   CN_TRER
	JNZ   AD_RDR1
	INC   CN_TRER
	MOV   A,ADCH
	ANL   A,#0F8H
	MOV   C,FL_TRER
	CLR   FL_TRER
	JZ    AD_RDR1
	SETB  FL_TRER
	JNC   AD_RDR1
	MOV   A,#ERTRHI
	CALL  SET_ERR
	SJMP  AD_RDR1
AD_RD4:	MOV   A,R1      ; Ma se filtrovat ?
	JNB   ACC.5,AD_RD5
	CLR   A
	MOV   R4,A
	SJMP  AD_RD6
AD_RD5:	CALL  iLDR45i
	MOV   R2,#0
	MOV   R3,#0E0H
	CALL  MULi
	MOV   A,R6
	MOV   R4,A
	MOV   A,R7
AD_RD6:	MOV   R5,A
	MOV   A,ADCON
	RL    A
	RL    A
	RL    A
	ANL   A,#6
	MOV   R2,A
	MOV   A,ADCH
	RL    A
	RL    A
	RL    A
	MOV   R3,A
	ANL   A,#7
	XCH   A,R3
	ANL   A,#NOT 7
	ORL   A,R2
	MOV   R2,A
	CALL  ADDi
	CALL  iSVR45i
	CLR   AD_ST	      ; AD_RDR1:CLR   AD_ST
	RET		      ; AD_RDR:	RET

AD_PRET:ADD   A,#AD_TAB-AD_PREU
	MOVC  A,@A+PC
AD_PREU:RET

AD_TAB: DB    007H,AD_DCIN
	DB    002H,AD_MCUR
	DB    080H,0
	DB    0C4H,AD_BAT
	DB    015H,0
	DB    0

AD_DRN  SET   0		      ; Prevodnik kapek AD_DROP
DR_TOFF	SET   15 ; 5	      ; Doba odpadu kapky max MDR_DCN (6)
DR_TON	SET   1		      ; Doba zapocteni kapky
DR_TBLK	SET   20 	      ; Doba blokovani dalsi kapky

;=================================================================
; Casove preruseni

I_TIME1:		      ; Preruseni s frekvenci 450 Hz

A_SYSP:	MOV   A,A_CPOS
	JNB   ACC.3,A_SYSP1   ; Neni potreba pripravit data v prevodniku
	MOV   B,ADCON
	JB    B.3,A_SYSP1     ; ADCS=1 .. prevodnik zamestnan
	ANL   A,#07H
	MOV   ADCON,A
	SETB  ACC.3           ; Odstartovani ADC
	MOV   ADCON,A
A_SYSP1:

	CALL  AD_RD	      ; Precteni ADC
    %IF(NOT %DRFAST_FL)THEN(
	CALL  AD_PREP         ; Start pristiho prevodu ADC
    )ELSE(
	CALL  AD_PREP_DR      ; Pristi prevod pro potrebu kapek
    )FI

	DJNZ  PPTIMR,I_TIM20
	INC   PPTIMR          ; Chyba Ping-Pongu
	MOV   A,#ERCOMMT
	CALL  SET_ERR
I_TIM20:
I_TIM21:MOV   A,ERRNUM        ; Rizeni horniho tanzistoru
	JZ    I_TIM22
	SETB  ELED
	SETB  C
	SETB  MOTOFF
	CLR   FL_MOT
	SJMP  I_TIM25
I_TIM22:MOV   C,FL_MOT
	CPL   C
	JC    I_TIM23
	JNB   FL_SERV,I_TIM24
I_TIM23:MOV   MOTOFF,C
I_TIM24:SETB  C               ; Generovani pipnuti
	DJNZ  BPTIMR,I_TIM25
	CLR   C
	INC   BPTIMR
I_TIM25:MOV   BEEPB,C
	CLR   F0	      ; Zpracovani otacky
	MOV   C,FL_ROTM
	MOV   ACC.7,C
	MOV   C,ROT_MOT
	XRL   A,PSW
	JNB   ACC.7,I_TIM29
	MOV   A,ROT_DCN
	ANL   A,#ROT_DMS
	XRL   A,#3
	JZ    I_TIM28
	INC   ROT_DCN
	SJMP  I_TIM30
I_TIM28:MOV   FL_ROTM,C	      ; Registrovana zmena ROT_MOT
	MOV   F0,C
I_TIM29:ANL   ROT_DCN,#NOT ROT_DMS
I_TIM30:JB    FL_MOT,I_TIM31
	JMP   I_TIM38
I_TIM31:CLR   C
	MOV   R2,#3
	MOV   R0,#ROT_TIM     ; Zvyseni ROT_TIM
I_TIM32:INC   @R0
	MOV   A,@R0
	JNZ   I_TIM33
	INC   R0
	DJNZ  R2,I_TIM32
I_TIM33:MOV   R0,#ROT_PER     ; R567=ROT_PER-ROT_TIM
	MOV   R1,#ROT_TIM
	CLR   C
	MOV   A,@R0
	MOV   R2,A
	SUBB  A,@R1
	MOV   R5,A
	INC   R0
	INC   R1
	MOV   A,@R0
	MOV   R3,A
	SUBB  A,@R1
	MOV   R6,A
	INC   R0
	INC   R1
	MOV   A,@R0
	MOV   R4,A	      ; R234=ROT_PER
	SUBB  A,@R1
	MOV   R7,A

	MOV   B,#ROT_TOL
	MOV   A,R4
	MUL   AB
	MOV   R4,B
	XCH   A,R3
	MOV   B,#ROT_TOL
	MUL   AB
	XCH   A,R2
	XCH   A,B
	ADD   A,R3
	MOV   R3,A
	CLR   A
	ADDC  A,R4
	MOV   R4,A
	MOV   A,#ROT_TOL
	MUL   AB
	MOV   A,B
	ADD   A,#ROT_ABS
	MOV   R1,A
	CLR   A
	RLC   A
	XCH   A,R1
	ADDC  A,R2
	MOV   R2,A
	MOV   A,R1
	ADDC  A,R3
	MOV   R3,A
	CLR   A
	ADDC  A,R4
	MOV   R4,A	; R234=ROT_TIM/256*ROT_TOL+ROT_ABS

	MOV   A,R7
	JB    ACC.7,I_TIM34
	JB    FL_FROT,I_TIM38
	SETB  C		; Kontrola pro ROT_PER - ROT_TIM > 0
	MOV   A,R5
	SUBB  A,R2
	MOV   A,R6
	SUBB  A,R3
	MOV   A,R7
	SUBB  A,R4	; ROT_PER - ROT_TIM  - ROT_PER/256*ROT_TOL
	ADDC  A,@R0
	JC    I_TIM38
	SJMP  I_TIM36

I_TIM34:SETB  C		; Kontrola pro ROT_PER - ROT_TIM < 0
	MOV   A,R5
	ADDC  A,R2
	MOV   A,R6
	ADDC  A,R3
	MOV   A,R7
	ADDC  A,R4	; ROT_TIM - ROT_PER + ROT_PER/256*ROT_TOL
	JC    I_TIM38
I_TIM35:JB    FL_FRO1,I_TIM39
	MOV   A,MOD_II
	XRL   A,#M_SERV	      ; Neprisla znacka = ERROR
	ANL   A,#M_MSK
	JZ    I_TIM38
;	JB    FL_IROT,I_TIM38	; !!!!!!!!!!!!!!
	SETB  MOTOFF
	SETB  FL_ROTE
	SJMP  I_TIM38
I_TIM36:JNB   F0,I_TIM45      ; Nesmi prijit znacka
I_TIM37:MOV   A,MOD_II
	XRL   A,#M_SERV
	ANL   A,#M_MSK
	JZ    I_TIM38
;	JB    FL_IROT,I_TIM38	; !!!!!!!!!!!!!!
	MOV   A,#ERFLTS
	CALL  SET_ERR
I_TIM38:JNB   F0,I_TIM45      ; Mela by prijit znacka
I_TIM39:CLR   A		      ; Prosla znacka vacky
	MOV   R0,#ROT_TIM
	MOV   @R0,A
	INC   R0
	MOV   @R0,A
	INC   R0
	MOV   @R0,A
I_TIM40:CLR   F0	      ; Akce po pruchodu znacky
%IF(%DR2ROT_FL) THEN (
	JBC   DR_DIV1,I_TIM45 ; pocitat pres dvojnasobek otacek
	SETB  DR_DIV1
)FI
	JNB   FL_PSET,I_TIM41
	JBC   DR_DIV2,I_TIM45 ; pro PVC set pocitat pres 2 otacky
	SETB  DR_DIV2
I_TIM41:JBC   FL_FRO1,I_TIM44 ; Ignorovat prvni
	JBC   FL_FROT,I_TIM44 ; a druhou otacku
%IF (NOT %EP_SOFT_FL) THEN (
	MOV   C,FL_MOT
	ANL   C,/FL_DROP
	JNC   I_TIM44
	MOV   R0,#DR_IRCN
	MOV   A,@R0
	JZ    I_TIM42
	DEC   @R0
	SJMP  I_TIM44
I_TIM42:MOV   A,MOD_II
	XRL   A,#M_SERV
	ANL   A,#M_MSK
	JZ    I_TIM44
	MOV   R0,#DR_CNT
	MOV   A,@R0
	ADD   A,#-DR_MIN
	JNC   I_TIM43
	ADD   A,#DR_MIN-DR_MAX-1
	JNC   I_TIM44
I_TIM43:
    %IF(0)THEN(
        MOV   A,#ERDROP
	CALL  SET_ERR
    )ELSE(
        SETB  FL_EDROP
    )FI
)FI
I_TIM44:MOV   R0,#DR_CNT
	MOV   @R0,#0
I_TIM45:
    %IF(%DRFAST_FL)THEN(
	CALL  AD_RD_DR	      ; Zpracovani kapek
	CALL  AD_PREP         ; Start pristiho prevodu ADC
    )FI
	DEC   TIME_CN
	JNB   TIME_CN.7,ITIMEQ
	ANL   AD_CNT,#0E0H    ; Nove odstartovani cteni ADC
	MOV   A,#C_TIMED      ; Pruchod s frekvenci 25Hz
	ADD   A,TIME_CN
	MOV   TIME_CN,A

	MOV   PSW,#AR0
	MOV   R0,#TIMR1
	MOV   B,#N_OF_T
I_TIME2:MOV   A,@R0
	JZ    I_TIME3
	DEC   A
	MOV   @R0,A
I_TIME3:INC   R0
	DJNZ  B,I_TIME2
	MOV   A,TIMRI
	JNZ   I_TIMR1
	SETB  FL_EDRF         ; Pruchod s periodou 0.01 min
	CALL  uL_STR
	MOV   TIMRI,#C_TIM06
	DEC   TIM_DIF
	MOV   A,TIM_DIF
	ADD   A,#10
	ADD   A,#-20
	JNC   I_TIM51
	MOV   A,#ERTIME
	CALL  SET_ERR
I_TIM51:JNB   FL_BEWA,I_TIM52
	CLR   FL_BEWA
	MOV   A,TIMRJ         ; Generovani varovnych pipu
	ANL   A,#003H
	JNZ   I_TIM52
	MOV   A,BPTIMR
	ADD   A,#BP_WA
	MOV   BPTIMR,A
I_TIM52:INC   TIMRJ
	MOV   A,TIMRJ
	ADD   A,#-C_TMIN
	JNC   I_TIMR1
	MOV   TIMRJ,#0        ; Pruchod s periodou 1 min
	MOV   A,TIM_DIF
	DEC   A
	JNB   ACC.7,I_TIM54
	INC   A
	INC   A
I_TIM54:MOV   TIM_DIF,A
	INC   TIME
	MOV   A,TIME
	JNZ   I_TIM55
	INC   TIME+1
I_TIM55:

I_TIMR1:MOV   R7,#C_TIMED
ITIMEQ: %WATCHDOG
	POP   B
	POP   PSW
	POP   ACC
	RETI

;=================================================================
; Ovladani displeje pres 8577

DispLen EQU     4
DispAdr EQU     74H

; Definice segmentu
lcda    SET     02h             ;lcd segment a
lcdb    SET     01h             ;lcd segment b
lcdc    SET     04h             ;lcd segment c
lcdd    SET     10h             ;lcd segment d
lcde    SET     40h             ;lcd segment e
lcdf    SET     08h             ;lcd segment f
lcdg    SET     20h             ;lcd segment g
lcdh    SET     80h             ;lcd segment h colon

RSEG    IC____I

WR_BUF: DS    DispLen+1

RSEG    IC____C

; Vypsani WR_BUF po IIC na DISPLAY
OUT_BUF:MOV   A,R6
	ANL   A,#0F8H
	MOV   R0,#WR_BUF
	MOV   @R0,A
        MOV   R6,#DispAdr
	MOV   R4,#WR_BUF
        MOV   R2,#DispLen+1
	MOV   R3,#0
	JMP   IIC_RQI

;Vystup cisla na LCD display pres prevod LCD_TBL a LCD_POS
;Vstup: R45   vypisovane cislo
;       R7    .. format vystupu cisla
;                  SLLLxADD
;            S   - signed
;            A   - znamenko nebo cislice
;                  jinak znamenko nebo blank
;            LLL - delka vypisu > 0
;            DD  - pocet desetinych mist
;       R6    .. pozice vypisu

iPRTLi: MOV   A,R5
	MOV   C,ACC.7
        MOV   A,R7
        JNB   ACC.7,iPRTLi3
	JC    iPRTLi1
	JB    ACC.2,iPRTLi3
	MOV   A,#lcdBlnk
        JNC   iPRTLi2
iPRTLi1:CALL  NEGi
        MOV   A,#lcdSig
iPRTLi2:CALL  iPRTLc
	MOV   A,R7
	ADD   A,#-10H
	MOV   R7,A
iPRTLi3:MOV   A,R7
	SWAP  A
	ANL   A,#7
	MOV   R1,A
iPRTLi9:DEC   R1
	MOV   A,R1
	RL    A
	CPL   A
	ADD   A,#O10E4i-1+5*2
	MOV   R0,A
	CALL  rLDR23i
iPRTL10:MOV   R0,#-1
iPRTL11:CJNE  R0,#9,iPRTL12
iPRTLiE:MOV   A,#lcd_Err
	CALL  iPRTLc
	MOV   A,R7
	ANL   A,#70H
	ADD   A,#-10H
	MOV   R7,A
	JNZ   iPRTLiE
	RET
iPRTL12:CALL  SUBi
	INC   R0
	JNC   iPRTL11
	CALL  ADDi
	MOV   A,R7
	ANL   A,#3
	DEC   A
	XRL   A,R1
	JZ    iPRTL13
	CLR   C
iPRTL13:MOV   A,R0
	MOV   ACC.7,C
	CALL  iPRTLc
	MOV   A,R7
	CLR   ACC.7
	ADD   A,#-10H
	MOV   R7,A
	ANL   A,#70H
	JNZ   iPRTLi9
	RET

; Rotace vlevo o 4 bity

RL4R45: MOV   A,R4
	SWAP  A
	MOV   R4,A
	ANL   A,#0F0H
	XCH   A,R4
	ANL   A,#00FH
	XCH   A,R5
	SWAP  A
	XCH   A,R4
	XRL   A,R4
	XCH   A,R4
	ANL   A,#0F0H
	XCH   A,R5
	ORL   A,R5
	XCH   A,R5
	XRL   A,R4
	MOV   R4,A
	RET

; Vystup hexa
;Vstup: R45   vypisovane cislo
;       R6    .. pozice vypisu
iPRTLhw:MOV   R7,#4
iPRTLh: MOV   A,R5
	SWAP  A
	ANL   A,#00FH
	CALL  iPRTLc
	CALL  RL4R45
	DJNZ  R7,iPRTLh
	RET

iPRTLc: MOV     R0,#0
	JNB     ACC.7,iPRTLc1
	CLR     ACC.7
	MOV     R0,#lcdH
iPRTLc1:ADD     A,#LCD_TBL-iPRTLck
	MOVC    A,@A+PC
iPRTLck:ORL     A,R0
	MOV     R0,A
	MOV     A,R6
	INC     R6
	ANL     A,#07H
	ADD     A,#LCD_POS-iPRTLcl
	MOVC    A,@A+PC
iPRTLcl:ADD     A,#WR_BUF+1
	XCH     A,R0
	MOV     @R0,A
	RET

lcdBlnk SET     10H
lcdSig  SET     17h
lcd_Err SET     17h ; = '-' dalsi moznost 0Eh = 'E'

LCD_POS:DB      3
	DB      2
	DB      0
	DB      1

LCD_TBL:db      lcda+lcdb+lcdc+lcdd+lcde+lcdf           ;0
	db      lcdb+lcdc                               ;1
	db      lcda+lcdb+lcdg+lcde+lcdd                ;2
	db      lcda+lcdb+lcdg+lcdc+lcdd                ;3
	db      lcdf+lcdg+lcdb+lcdc                     ;4
	db      lcda+lcdf+lcdg+lcdc+lcdd                ;5
	db      lcda+lcdf+lcdg+lcde+lcdd+lcdc           ;6
	db      lcda+lcdb+lcdc                          ;7
	db      lcda+lcdb+lcdc+lcdd+lcde+lcdf+lcdg      ;8
	db      lcda+lcdb+lcdf+lcdg+lcdc+lcdd           ;9
	db      lcda+lcdb+lcdf+lcdg+lcdc+lcde           ;A
	db      lcdc+lcdd+lcde+lcdf+lcdg                ;b
	db      lcda+lcdd+lcde+lcdf                     ;C
	db      lcde+lcdg+lcdd+lcdc+lcdb                ;d
	db      lcda+lcdd+lcde+lcdf+lcdg                ;E
	db      lcda+lcde+lcdf+lcdg                     ;F
	db      0                                       ;blank
	db      lcda                                    ;segment a
	db      lcdb                                    ;segment b
	db      lcdc                                    ;segment c
	db      lcdd                                    ;segment d
	db      lcde                                    ;segment e
	db      lcdf                                    ;segment f
	db      lcdg                                    ;segment g
	db      lcdb+lcdc+lcde+lcdf+lcdg                ;H
	db      lcdd+lcde+lcdf                          ;L
	db      lcda+lcdb+lcdf+lcdg                     ;degree
	db      lcdc+lcdd+lcde+lcdg                     ;lower degree
	db      0                                       ;spare
	db      0                                       ;spare
	db      0                                       ;spare
	db      0                                       ;spare
	db      lcdh                                    ;colon

;=================================================================
; Cteni a zapis do pameti EEPROM 8582

EEPRAdr EQU   0A0H ; IIC adresa pameti EEPROM

; Nacte do bufferu IIC_OUT paket dat z adresy R0 v EEPROM

EEPA_RD:JB    F0,EEPA_RR
	MOV   IIC_OUT,R0
EEPA_R1:CALL  IIC_CER
	MOV   R2,#1
	MOV   R3,#7
	MOV   R4,#IIC_OUT
	MOV   R6,#EEPRAdr
	CALL  IIC_RQI
	JNZ   EEPA_R1
	CALL  IIC_WME
	JNZ   EEPA_RE
	MOV   R0,#IIC_OUT
	MOV   R2,#7
	CALL  XOR_SUM
	XRL   A,@R0
	JZ    EEPA_RR
EEPA_RE:SETB  F0
	SETB  FL_EREE
EEPA_RR:RET

; Nacte do bufferu IIC_OUT paket dat z adresy R0 v EEPROM

EEPA_WR:JB    F0,EEPA_WQ
	MOV   IIC_OUT,R0
	MOV   R0,#IIC_OUT
	MOV   R2,#7
	CALL  XOR_SUM
	MOV   @R0,A
EEPA_W1:CALL  IIC_CER
	MOV   R2,#8
	MOV   R3,#0
	MOV   R6,#EEPRAdr
	MOV   R4,#IIC_OUT
	CALL  IIC_RQI
	JNZ   EEPA_W1
	CALL  IIC_WME
	JZ    EEPA_WQ
EEPA_WE:SETB  F0
	SETB  FL_EREE
EEPA_WQ:RET

;=================================================================
; Vypis na LCD pro ladeni

PRTR23:	MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
PRTR45:	MOV   R7,#0C4H
	MOV   R6,#18H
	CALL  iPRTLhw
;	CALL  iPRTLi
	MOV   R6,#18H
	CALL  OUT_BUF
	RET

;=================================================================
; System dynamicke adresace a vysilani statusu

RSEG    IC____B

uL_DYFL:DS    1
uLD_RQA	BIT   uL_DYFL.2       ; Pozadavek na pripojeni do site

RSEG    IC____D

uL_DYSA:DS    1

RSEG    IC____C

; Rutina vyslani zadosti o prideleni dynamicke adresy

SNRQ:   JNB   uLD_RQA,SNRQ01
	MOV   R0,#BEG_OB
	MOV   A,@R0
	JZ    SNRQ02
SNRQ01:	RET
SNRQ02: DEC   uL_DYFL
	MOV   @R0,uL_DYSA
	INC   R0
	MOV   @R0,#07FH
	INC   R0
	INC   R0
	MOV   @R0,#0C0H
	INC   R0
	MOV   @R0,uL_ADR
	MOV   R2,#4
	MOV   DPTR,#SER_NUM
	CALL  MOVsci

CL_OB:  MOV   A,R0
	MOV   R0,#BEG_OB+2
	MOV   @R0,A
	MOV   R0,#BEG_OB
	MOV   A,@R0
	ORL   A,#080H
	MOV   @R0,A
	SETB  uLF_RS
	JNB   uLF_NB,CL_OB9
	JNB   ES,CL_OB9
	SETB  TI
CL_OB9: RET

; Rutina vysilani statusu z davkovace  CMD=0C1H

uL_SNST:MOV   R1,#BEG_PB
	MOV   A,@R1
	JNZ   SNSTA10
	JB    uLD_RQA,SNSTA03 ; Snaha o zviditelneni
	INC   uL_DYFL
	JNB   uLD_RQA,SNSTA03
SNSTA02:ORL   uL_DYFL,#7
	MOV   uL_DYSA,uL_SA   ; Server dynamickych adres
	MOV   uL_ADR,#0
SNSTA03:JMP   SNSTAR

SNSTA10:MOV   R7,A
	ANL   A,#0F0H
	CJNE  A,#010H,SNSTA03
	MOV   A,R6            ; Prikaz cteni udaju
	CLR   C
	SUBB  A,#BEG_PB+1+4   ; Neni test serioveho cisla
	JC    SNSTA13
	INC   R1
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#SER_NUM   ; Kontrola serioveho cisla
	MOV   R2,#4
SNSTA11:CLR   A
	MOVC  A,@A+DPTR
	XRL   A,@R1
	JNZ   SNSTA12
	INC   R1
	INC   DPTR
	DJNZ  R2,SNSTA11
SNSTA12:POP   DPH
	POP   DPL
	JNZ   SNSTA02         ; Nesouhlasi cislo
SNSTA13:CALL  ACK_CMD
	ANL   uL_DYFL,#NOT 7
SNSTA20:CALL  SND_BEB
	PUSH  DPL
	PUSH  DPH
	MOV   R2,#4           ; Vysli seriove cislo
	MOV   DPTR,#SER_NUM
SNSTA21:CLR   A
	MOVC  A,@A+DPTR
	CALL  SND_CHC
	INC   DPTR
	DJNZ  R2,SNSTA21
	POP   DPH
	POP   DPL
	CJNE  R7,#010H,SNSTA30
	MOV   A,MOD_II        ; Vyslani zakladnich udaju
	CALL  SND_CHC
	MOV   A,ERRNUM
	JNZ   SNSTA24
	JNB   FL_STKV,SNSTA24
	MOV   A,#ERENDKV
SNSTA24:CALL  SND_CHC
	MOV   R0,#FLOW
	CALL  SND_IDi
	MOV   R0,#MAX_TIM
	MOV   R2,#6
SNSTA25:CALL  SND_IDi
	DJNZ  R2,SNSTA25
	MOV   A,#0 ; Volne
	CALL  SND_CHC
	SJMP  SNSTA50
SNSTA30:CJNE  R7,#011H,SNSTA50
	MOV   R0,#ROT_TIM     ; Vyslani servisnich udaju
	CALL  SND_IDi
	MOV   R0,#IRC_ACT
	CALL  SND_IDi
	MOV   R2,#8
	MOV   R0,#AD_DCIN
	SJMP  SNSTA25

SNSTA50:CALL  SND_END
SNSTAR: JMP   S_WAITD
	JMP   NAK_CMD

SND_IDi:MOV   A,@R0
	MOV   R4,A
	INC   R0
	MOV   A,@R0
	MOV   R5,A
	INC   R0
SNDR45i:MOV   A,R4
	CALL  SND_CHC
	MOV   A,R5
	JMP   SND_CHC

	END