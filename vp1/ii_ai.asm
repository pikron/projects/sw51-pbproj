;********************************************************************
;*                    II_AI.ASM                                     *
;*     Aritmetika v pevne radove carce                              *
;*                  Stav ke dni 18.09.1998                          *
;*                      (C) Pisoft 1994                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

%DEFINE (WITH_SMULi) (0)      ; SMULi MULTENi

PUBLIC  ADDi,SUBi,NEGi,CMPi,MULi,DIVi,DIVi0,DIVi1,DIVihf,MODi
PUBLIC  SHRi,SHR1i
PUBLIC  iLDR23i,iLDR45i,iSVR45i,cLDR23i,DECDPTR,ADDATDP,MR45R67
PUBLIC  rLDR23i,O10E4i,iLDR123,iSVR23i,iSVR123

%IF(%WITH_SMULi)THEN(
PUBLIC	SMULi,MULTENi
)FI

INT_A_C SEGMENT CODE

RSEG INT_A_C

ADDi:   MOV   A,R4            ;:R45:=R45+R23
	ADD   A,R2
	MOV   R4,A
	MOV   A,R5
	ADDC  A,R3
	MOV   R5,A
	RET

SUBi:   CLR   C               ;:R45:=R45-R23
	MOV   A,R4
	SUBB  A,R2
	MOV   R4,A
	MOV   A,R5
	SUBB  A,R3
	MOV   R5,A
	ORL   A,R4
	RET

NEGi:   CLR   C               ;:R45:=-R45
	CLR   A
	SUBB  A,R4
	MOV   R4,A
	CLR   A
	SUBB  A,R5
	MOV   R5,A
	RET

CMPi:   CLR   C               ;?R45>=R23
	MOV   A,R4
	SUBB  A,R2
	JZ    CMPi1
	MOV   A,R5
	SUBB  A,R3
	ORL   A,#1
	RET
CMPi1:  MOV   A,R5
	SUBB  A,R3
	RET

MULi:   MOV   A,R5            ;:R4567:=R45*R23
	MOV   R7,A
	MOV   B,R2
	MUL   AB              ; R2*R5
	MOV   R5,A
	MOV   R6,B
	MOV   A,R3
	MOV   B,R4
	MUL   AB              ; R3*R4
	ADD   A,R5
	MOV   R5,A
	MOV   A,B
	ADDC  A,R6
	MOV   R6,A
	CLR   A
	ADDC  A,#0
	XCH   A,R7
	MOV   B,R3
	MUL   AB              ; R3*R5
	ADD   A,R6
	MOV   R6,A
	MOV   A,B
	ADDC  A,R7
	MOV   R7,A
	MOV   A,R2
	MOV   B,R4
	MUL   AB              ; R2*R4
	MOV   R4,A
	MOV   A,B
	ADD   A,R5
	MOV   R5,A
	CLR   A
	ADDC  A,R6
	MOV   R6,A
	CLR   A
	ADDC  A,R7
	MOV   R7,A
	RET

%IF(%WITH_SMULi)THEN(
MULTENi:MOV   A,#00AH         ;:R456:=R45*10
SMULi:  MOV   R6,A            ;:R456:=R45*A
	MOV   B,R4
	MUL   AB              ; R4*A
	MOV   R4,A
	MOV   A,B
	XCH   A,R5
	MOV   B,R6
	MUL   AB              ; R5*A
	ADD   A,R5
	MOV   R5,A
	MOV   A,B
	ADDC  A,#0
	MOV   R6,A
	RET
)FI

DIVihf: MOV   A,R4            ; Poloplovouci deleni
	ORL   A,R5            ; R1 je exponent
	SETB  F0              ; R45*2^R1=R45*2^R1/R23
	JNZ   DIVi0
	CLR   F0
	CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   A,R1
	ADD   A,#-16
	MOV   R1,A
	RET

MODi:   MOV   R0,#080H        ;:R45:=R45 mod R23,R67:=R45/R23
	DB    07FH            ; MOV   R7,#d8

DIVi:   MOV   R0,#000H        ;:R45:=R45/R23
	MOV   R1,#000H
	CLR   F0
DIVi0:	MOV   A,R2
	ORL   A,R3
	JNZ   DIVi1
	SETB  F0
	RET
DIVi1:  MOV   A,R3            ; Vstup s F0=1 pro poloplovouci
	CLR   C               ; deleni, R1 je exponent
DIVi2:  INC   R1              ; R45*2^R1=R45*2^R1/R23
	JB    ACC.7,DIVi3
	MOV   A,R2
	RLC   A
	MOV   R2,A
	MOV   A,R3
	RLC   A
	MOV   R3,A
	SJMP  DIVi2
DIVi3:  CLR   A
	MOV   R6,A
	MOV   R7,A
	MOV   A,R1
	ORL   A,R0
	MOV   R0,A
DIVi4:  JC    DIVi5
	MOV   A,R4
	SUBB  A,R2
	MOV   A,R5
	SUBB  A,R3
	CPL   C
	JNC   DIVi6
DIVi5:  CLR   C
	MOV   A,R4
	SUBB  A,R2
	MOV   R4,A
	MOV   A,R5
	SUBB  A,R3
	MOV   R5,A
	SETB  C
DIVi6:  MOV   A,R6
	RLC   A
	MOV   R6,A
	MOV   A,R7
	RLC   A
	MOV   R7,A
	CLR   C
	MOV   A,R4
	RLC   A
	MOV   R4,A
	MOV   A,R5
	RLC   A
	MOV   R5,A
	JB    F0,DIVi7
	DJNZ  R1,DIVi4
	MOV   A,R0
	XRL   A,#080H
	JNB   ACC.7,SHRi+1
MR45R67:MOV   A,R6
	MOV   R4,A
	MOV   A,R7
	MOV   R5,A
SHRiR:  RET

DIVi7:  DEC   R1
	MOV   A,R7
	JNB   ACC.7,DIVi4
	CLR   F0
	SJMP  MR45R67

SHR1i:  MOV   A,#001H         ; R45:=R45 shr 1
SHRi:   CLR   C               ; R45:=R45 shr A
	JZ    SHRiR
	XCH   A,R5
	RRC   A
	XCH   A,R5
	XCH   A,R4
	RRC   A
	XCH   A,R4
	DEC   A
	SJMP  SHRi

iLDR45i:MOV   A,@R0           ;:R45:=i(R0)
	MOV   R4,A
	INC   R0
	MOV   A,@R0
	MOV   R5,A
	DEC   R0
	RET

DECDPTR:INC   DPL             ;:DPTR--
	DJNZ  DPL,DECDPT1
	DEC   DPH
DECDPT1:DEC   DPL
	RET

ADDATDP:ADD    A,DPL          ;:DPTR:=DPTR+A
        MOV    DPL,A
        CLR    A
        ADDC   A,DPH
        MOV    DPH,A
        RET

iLDR123:MOV   A,@R0           ;:R123:=i(R0)
	MOV   R1,A
	INC   R0
iLDR23i:MOV   A,@R0           ;:R23:=i(R0)
	MOV   R2,A
	INC   R0
	MOV   A,@R0
	MOV   R3,A
	INC   R0
	RET

iSVR45i:MOV   A,R4            ;:i(R0):=R45
	MOV   @R0,A
	INC   R0
	MOV   A,R5
	MOV   @R0,A
	DEC   R0
	RET

iSVR123:MOV   A,R1            ;:i(R0):=R123
	MOV   @R0,A
	INC   R0
iSVR23i:MOV   A,R2            ;:i(R0):=R23
	MOV   @R0,A
	INC   R0
	MOV   A,R3
	MOV   @R0,A
	INC   R0
	RET

cLDR23i:CLR    A              ;:R23:=c(DPTR)
	MOVC   A,@A+DPTR
	MOV    R2,A
	INC    DPTR
	CLR    A
	MOVC   A,@A+DPTR
	MOV    R3,A
	INC    DPTR
	RET

rLDR23i:MOV   R3,#2           ;:R23:=c(A)
cLDR231:MOV   R2,A
	MOV   A,R0
	INC   R0
	MOVC  A,@A+PC
cLDR23K:DJNZ  R3,cLDR231
	MOV   R3,A
	RET

K10E4i: DB    010H,027H
	DB    0E8H,003H
	DB    064H,000H
K10i:   DB    00AH,000H
K1i:    DB    001H,000H
O10E4i  SET   K10E4i-cLDR23K

END
