;********************************************************************
;*                    ID 2050 - II_MSG.ASM                          *
;*     Definice tvaru zprav mezi procesory                          *
;*                  Stav ke dni 14.11.1994                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

; Zprava se zklada z TYPU DAT a XOR1_SUMu

O_TYMSG EQU   0

; Zpravy vysilane spodnim procesorem (S1ADR=30H)

; Vyslani flagy(SW_FLG)+typ strikacky(SPRTYP)+poloha+cas

M3_SFST EQU   31H

