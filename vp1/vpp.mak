#   Project file pro cerpadlo VP1000
#         (C) Pisoft 1992

ii_iic.obj: ii_iic.asm
	a51 ii_iic.asm $(par)

ii_ai.obj : ii_ai.asm
	a51 ii_ai.asm  $(par)

ii_plan.obj : ii_plan.asm
	a51   ii_plan.asm  $(par)

vpp.obj     : vpp.asm ii_ai.h vp_msg.h ii_iic.h ii_plan.h
	a51   vpp.asm     $(par)

vpp.      : vpp.obj ii_ai.obj ii_iic.obj ii_plan.obj
	l51 vpp.obj,ii_ai.obj,ii_iic.obj,ii_plan.obj xdata(8000H) ramsize(100h) stack(STACK) ixref

vpp.hex   : vpp.
	ohs51 vpp
	copy vpp.hex vpp.he1
	ihexcsum -o vpp.hex -l 0x2000 -L 0x1fee -A 0x1fee -F bc vpp.hex
