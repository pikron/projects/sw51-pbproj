#   Project file pro cerpadlo LCP4000
#         (C) Pisoft 1992

ii_iic.obj: ii_iic.asm
	a51 ii_iic.asm $(par)

ii_ai.obj : ii_ai.asm
	a51 ii_ai.asm  $(par)

vpk.obj   : vpk.asm vp_msg.h ii_ai.h ii_iic.h
        a51 vpk.asm $(par) xref

vp_kbd.obj: vp_kbd.asm
	a51 vp_kbd.asm $(par)

vpk.      : vpk.obj ii_ai.obj vp_kbd.obj ii_iic.obj
	l51 vpk.obj,ii_ai.obj,vp_kbd.obj,ii_iic.obj xdata(8000H) ramsize(100h) stack(STACK) ixref

vpk.hex   : vpk.
	ohs51 vpk
	copy vpk.hex vpk.he1
	ihexcsum -o vpk.hex -l 0x4000 -L 0x3ffe -A 0x3ffe -F bc vpk.hex
#	ihexcsum -o vpk.hex -l 0x2000 -L 0x1ffe -A 0x1ffe -F bc vpk.hex
