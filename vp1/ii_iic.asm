$NOMOD51
;********************************************************************
;*                    ID 2050 - II_IIC.ASM                          *
;*     Rutiny pro komunikaci pomoci IIC                             *
;*                  Stav ke dni 14.11.1994                          *
;*                      (C) Pisoft 1994                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$INCLUDE(REG552.H)

USING 3

%DEFINE (ENA_XDA) (0)         ; Compile for XDATA buffers too

%DEFINE (IIC_TEST_FL) (0)     ; Testovani IIC

%IF (%IIC_TEST_FL) THEN (
	EXTRN  XDATA(TEST_IIC_OUT)
)FI

PUBLIC  MSK_MXD,MSK_SXD,SJ_TSTA,SJ_REND,SJ_TEND
PUBLIC  ICF_MRQ,ICF_MER,ICF_SRC,ICF_NA
PUBLIC  IIC_FLG,M_SLA,M_SLEN,M_RLEN,M_DP,S_BLEN,S_RLEN,S_DP,S_CADR
PUBLIC  IIC_RQI,IIC_SLI,IIC_WME,IIC_CER,SL_JRET
%IF (%ENA_XDA) THEN(
PUBLIC	IIC_RQX,IIC_SLX,ICF_MXD,ICF_SXD,ICF_PXD
)FI

C_S1CON EQU   11000001B       ; Pocatecni stav S1CON

IIC___C SEGMENT CODE
IIC___D SEGMENT DATA
IIC___B SEGMENT DATA BITADDRESSABLE
IIC__PC SEGMENT CODE PAGE

CSEG    AT    SIIC
	JMP   I_IIC

RSEG IIC___B

IIC_FLG:DS    1
ICF_MRQ BIT   IIC_FLG.0       ; Master proc request
ICF_MER BIT   IIC_FLG.1       ; Master proc error
%IF (%ENA_XDA) THEN(
ICF_MXD BIT   IIC_FLG.2       ; Master from XDATA
)FI
ICF_SRC BIT   IIC_FLG.4       ; Slave rec data
%IF (%ENA_XDA) THEN(
ICF_SXD BIT   IIC_FLG.5       ; Slave to XDATA
ICF_PXD BIT   IIC_FLG.6       ; Proceed XDATA
)FI
ICF_NA  BIT   IIC_FLG.7       ; IIC not active

MSK_MXD EQU   4               ; Vymaskovani ICF_MXD
MSK_SXD EQU   20H             ; Vymaskovani ICF_SXD

RSEG IIC___D

; Message from master to slave

M_SLA:  DS    1     ; Destignation adr
M_SLEN: DS    1     ; Send cnt
M_RLEN: DS    1     ; Rec cnt
M_DP:   DS    2     ; Pointer to data

; Message for slave

S_BLEN: DS    1     ; Buffer length
S_RLEN: DS    1     ; Bytes received
S_DP:   DS    2     ; Pointer to data
S_CADR: DS    2     ; Address to call when rec or write proc

; Registers assigment
;       R12  address
;       R3   count
;       R0   activity id
;

RSEG IIC___C

%IF (%ENA_XDA) THEN(
  %DEFINE (READ_DAT)  (CALL  R_S1DAT)
  %DEFINE (WRITE_DAT) (CALL  W_S1DAT)
R_S1DAT:JB    ICF_PXD,R_S1DA1
	MOV   @R1,S1DAT
	INC   R1
	RET
R_S1DA1:XCH   A,R1
	XCH   A,DPL
	XCH   A,R1
	XCH   A,R2
	XCH   A,DPH
	XCH   A,R2
	MOV   A,S1DAT
	MOVX  @DPTR,A
	INC   DPTR
	XCH   A,R1
	XCH   A,DPL
	XCH   A,R1
	XCH   A,R2
	XCH   A,DPH
	XCH   A,R2
	RET
W_S1DAT:JB    ICF_PXD,W_S1DA1
	MOV   S1DAT,@R1
	INC   R1
	RET
W_S1DA1:XCH   A,R1
	XCH   A,DPL
	XCH   A,R1
	XCH   A,R2
	XCH   A,DPH
	XCH   A,R2
	MOVX  A,@DPTR
	MOV   S1DAT,A
	INC   DPTR
	XCH   A,R1
	XCH   A,DPL
	XCH   A,R1
	XCH   A,R2
	XCH   A,DPH
	XCH   A,R2
	RET
)ELSE(
  %DEFINE (READ_DAT) (
	MOV   @R1,S1DAT
	INC   R1
  )
  %DEFINE (WRITE_DAT) (
	MOV   S1DAT,@R1
	INC   R1
  )
)FI


; Prikaz pro start mastera
;Vstup: R45   adresa bufferu
;       R2    delka bufferu M_SLEN
;       R3    delka bufferu M_RLEN
;       R6    SLA adresa cile
;Vystup:A = 0 vse v poradku
;           1 probiha minuly prenos
;           2 chyba v minulem prenosu

IIC_RQI:		      ; IDATA
%IF (%ENA_XDA) THEN(
	MOV   R7,#0
	SJMP  IIC_RQ1
IIC_RQX:MOV   R7,#MSK_MXD     ; XDATA
)FI
IIC_RQ1:MOV   C,EA
	CLR   EA
	CALL  IIC_PR1
	MOV   A,#2
	JB    ICF_MER,IIC_RQ4
	MOV   A,#1
	JB    ICF_MRQ,IIC_RQ4
%IF (%ENA_XDA) THEN(
	ANL   IIC_FLG,#NOT MSK_MXD
	MOV   A,R7
	ORL   IIC_FLG,A
)FI
	MOV   M_SLEN,R2
	MOV   M_RLEN,R3
	MOV   M_DP,R4
	MOV   M_DP+1,R5
	MOV   M_SLA,R6
	SETB  ICF_MRQ
	SETB  STA
	CLR   A
IIC_RQ4:MOV   EA,C
	RET

IIC_PR1:JNB   ENS1,IIC_PR2
	JB    ES1,IIC_PR3
IIC_PR2:CLR   A
	MOV   S_BLEN,A
	MOV   S_CADR,A
	MOV   S_CADR+1,A
	MOV   S_RLEN,A
	ANL   IIC_FLG,A
	MOV   S1CON,#C_S1CON
	SETB  ES1
	SETB  STO
IIC_PR3:RET

; Nastaveni bufferu pro slave
;Vstup: R45   adresa bufferu
;       R2    S_BLEN
;             pokud je R2.7=1 pak buffer dat pro vysilani ve slave
;             mode nasleduje za bufferem pro pro prijem a oba maji delku
;             S_BLEN and 7FH jinak je buffer spolecny
;       R6    S_CADR
;       R7    S_CADR+1

IIC_SLI:		      ; IDATA
%IF (%ENA_XDA) THEN(
	MOV   R3,#0
	SJMP  IIC_SL1
IIC_SLX:MOV   R3,#MSK_SXD     ; XDATA
)FI
IIC_SL1:MOV   C,EA
	CLR   EA
	CALL  IIC_PR1
%IF (%ENA_XDA) THEN(
	ANL   IIC_FLG,#NOT MSK_SXD
	MOV   A,R3
	ORL   IIC_FLG,A
)FI
	MOV   S_BLEN,R2
	MOV   S_DP,R4
	MOV   S_DP+1,R5
	MOV   S_CADR,R6
	MOV   S_CADR+1,R7
	CLR   ICF_SRC
	SETB  AA
IIC_SL4:MOV   EA,C
	RET

; Cekani na konec prenosu
; Pri chybe nastavi ACC.2

IIC_WME:
%IF (%IIC_TEST_FL) THEN (     ; Pro ucely kontroly IIC
	PUSH  DPL
	PUSH  DPH
	MOV   A,S1STA
	MOV   DPTR,#TEST_IIC_OUT
	RR    A
	MOV   C,ICF_MRQ
	MOV   ACC.0,C
	MOV   C,ICF_MER
	MOV   ACC.1,C
	MOVX  @DPTR,A
	RL    A
	POP   DPH
	POP   DPL
)FI
	JB    ICF_MER,IIC_WM1
	JB    ICF_MRQ,IIC_WME
	CLR   C
	RET
IIC_WM1:ORL   A,#4
	JMP   IIC_CER

; Smazani chyby vznikle pri vysilani
;Vystup:CY = 0 vse v poradku
;       CY = 1 v minulem prenosu doslo k chybe

IIC_CER:CLR   C
	JNB   ICF_MER,IIC_CE1
	MOV   C,EA
	CLR   EA
	CLR   ICF_MER
	CLR   ICF_MRQ
	MOV   EA,C
	SETB  C
IIC_CE1:RET

I_IIC:  PUSH  ACC
	PUSH  PSW
	MOV   PSW,#AR0
	MOV   A,S1STA
%IF (%IIC_TEST_FL) THEN (     ; Pro ucely kontroly IIC
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#TEST_IIC_OUT
	RR    A
	MOV   C,ICF_MRQ
	MOV   ACC.0,C
	MOV   C,ICF_MER
	MOV   ACC.1,C
	MOVX  @DPTR,A
	RL    A
	POP   DPH
	POP   DPL
)FI
	ANL   A,#0F8H
	PUSH  ACC
	MOV   A,#HIGH IIC_TBL
	PUSH  ACC
	RET

I_IICR: CLR   ICF_NA
	CLR   SI
	POP   PSW
	POP   ACC
	RETI

; Bude se vysilat nebo prijimat
MS_STA: CLR   STA
	SETB  AA
	MOV   R1,M_DP
	MOV   R2,M_DP+1
%IF (%ENA_XDA) THEN(
	MOV   C,ICF_MXD
	MOV   ICF_PXD,C
)FI
	MOV   A,M_SLEN
	JZ    MS_REPS
	MOV   R3,A
	ANL   M_SLA,#0FEH
	MOV   S1DAT,M_SLA
	JMP   I_IICR

; Opakovany start, bude se prijimat
MS_REPS:CLR   STA
	SETB  AA
	MOV   A,M_SLA
	ORL   A,#01H
	MOV   S1DAT,A
	MOV   A,M_RLEN
	MOV   R3,A
	JNZ   I_IICR
	JMP   MS_SSTP

; Konec cinnosti mastera
MS_END: JMP   SL_WAIT

; Zacatek prijmu
SL_PREP:MOV   A,S_BLEN
	ANL   A,#07FH
	JZ    SL_PRE4
	CLR   C
	JB    ICF_SRC,SL_PRE4
	SJMP  SL_PRE2
; Pocatek vysilani
SL_PRE1:MOV   A,S_BLEN
	MOV   C,ACC.7
	ANL   A,#07FH
SL_PRE2:MOV   R3,A
	SETB  AA
	MOV   R1,S_DP
	MOV   R2,S_DP+1
	JNC   SL_PRE3
	MOV   A,R3
	ADD   A,R1
	MOV   R1,A
	JNC   SL_PRE3
	INC   R2
SL_PRE3:
%IF (%ENA_XDA) THEN(
	MOV   C,ICF_SXD
	MOV   ICF_PXD,C
)FI
	RET

SL_PRE4:POP   ACC
	POP   ACC
	CLR   AA
	JMP   I_IICR

SJ_TSTA SET   1     ; Zacatek vysilani
SJ_REND SET   2     ; Konec prijmu
SJ_TEND SET   3     ; Konec vysilani

SL_JMP: CJNE  R0,#SJ_REND,SL_JM1
	SETB  ICF_SRC
	MOV   A,S_BLEN
	ANL   A,#07FH
	CLR   C
	SUBB  A,R3
	MOV   S_RLEN,A
SL_JM1: MOV   A,S_CADR
	ORL   A,S_CADR+1
	JZ    SL_JM2
	PUSH  S_CADR
	PUSH  S_CADR+1
	RET
SL_JM2:
SL_JRET:JMP   I_IICR

RSEG IIC__PC

IIC_TBL:DS    000H+IIC_TBL-$
	SETB  STO
	SJMP  I_IICRV

	DS    008H+IIC_TBL-$
; Povedl se start
	JMP   MS_STA

	DS    010H+IIC_TBL-$
; Povedl se repeated start
	JMP   MS_REPS
MS_SRST:SETB  STA
	JMP   I_IICR

	DS    018H+IIC_TBL-$
; Vyslano SLA W prijato ACK
MS_SDAT:%WRITE_DAT
I_IICRV:JMP   I_IICR

	DS    020H+IIC_TBL-$
; Vyslano SLA W prijato NACK
MS_ERR: SETB  ICF_MER
MS_ENDS:SETB  STO
MS_ENDV:JMP   MS_END

	DS    028H+IIC_TBL-$
; Vyslana DATA prijato ACK
; Cyklicke vysilani
	DJNZ  R3,MS_SDAT
	MOV   A,M_RLEN
	JNZ   MS_SRST
	SJMP  MS_SSTP

	DS    030H+IIC_TBL-$
; Vyslana DATA prijato NACK
	SJMP  MS_ERR

	DS    038H+IIC_TBL-$
; Pri vysilani ztrata arbitrace
	SETB  STA
	SJMP  SL_WAIU

	DS    040H+IIC_TBL-$
; Vyslano SLA R prijato ACK
	DJNZ  R3,I_IICRU
	SJMP  MS_RNAK

	DS    048H+IIC_TBL-$
; Vyslano SLA R prijato NACK
	SJMP  MS_ERR
MS_RNAK:CLR   AA
I_IICRU:JMP   I_IICR

	DS    050H+IIC_TBL-$
; Prijata DATA vyslano ACK
; Cyklicky prijem
	%READ_DAT
	DJNZ  R3,I_IICRU
	SJMP  MS_RNAK

	DS    058H+IIC_TBL-$
; Prijata DATA vyslano NACK
	%READ_DAT
MS_SSTP:CLR   ICF_MRQ
	SJMP  MS_ENDS

	DS    060H+IIC_TBL-$
; Prijato vlastni SLA a W vyslano ACK
SL_STRE:CALL  SL_PREP
	MOV   R0,#SJ_REND
	JMP   I_IICR

	DS    068H+IIC_TBL-$
; Prijato vlastni SLA a W vyslano ACK po ztrate arbitrace
	SETB  STA
	SJMP  SL_STRE

	DS    070H+IIC_TBL-$
; Prijato Generall CALL vyslano ACK
	JMP   I_IICR

	DS    078H+IIC_TBL-$
; Prijato Generall CALL vyslano ACK po ztrate arbitrace
	SETB  STA
	JMP   SL_STRE

	DS    080H+IIC_TBL-$
; SLA W : Prijata DATA vyslano ACK
SL_RDAT:%READ_DAT
	DJNZ  R3,I_IICRU
	SJMP  SL_NACK

	DS    088H+IIC_TBL-$
; SLA W : Prijata DATA vyslano NACK
SL_WAIU:JMP   SL_WAIT
SL_NACK:CLR   AA
	JMP   I_IICR

	DS    090H+IIC_TBL-$
; GCall : Prijata DATA vyslano ACK
	SJMP  SL_RDAT

	DS    098H+IIC_TBL-$
; GCall : Prijata DATA vyslano NACK
	JMP   SL_WAIT

	DS    0A0H+IIC_TBL-$
; Slave : Repeated START nebo STOP
	JMP   SL_JMP

	DS    0A8H+IIC_TBL-$
; Prijato vlastni SLA a R vyslano ACK
SL_STTR:CALL  SL_PRE1
	INC   R3
	MOV   R0,#SJ_TEND
	SJMP  SL_WDAT

	DS    0B0H+IIC_TBL-$
; Prijato vlastni SLA a R vyslano ACK po ztrate arbitrace
	SETB  STA
	SJMP  SL_STTR

	DS    0B8H+IIC_TBL-$
; SLA R : vyslana DATA prijato ACK
SL_WDAT:DJNZ  R3,SL_WDA1
	MOV   S1DAT,#0
	INC   R3
	SJMP  I_IICRX

	DS    0C0H+IIC_TBL-$
; SLA R : vyslana DATA prijato NACK
	SJMP  I_IICRX
SL_WDA1:%WRITE_DAT
I_IICRX:JMP   I_IICR

	DS    0C8H+IIC_TBL-$
; SLA R : vyslana posledni DATA (AA=0) prijato ACK
SL_WAIT:MOV   C,ICF_SRC
	CPL   C
	MOV   AA,C
	JMP   I_IICR

	DS    0D0H+IIC_TBL-$
	JMP   I_IICR
	DS    0D8H+IIC_TBL-$
	JMP   I_IICR
	DS    0E0H+IIC_TBL-$
	JMP   I_IICR
	DS    0E8H+IIC_TBL-$
	JMP   I_IICR
	DS    0F0H+IIC_TBL-$
	JMP   I_IICR

	DS    0F8H+IIC_TBL-$
; Neaktivni stav
	JMP   I_IICR

	END