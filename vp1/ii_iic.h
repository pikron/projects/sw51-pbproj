;********************************************************************
;*                    ID 2050 - II_IIC.H                            *
;*     Rutiny pro komunikaci pomoci IIC                             *
;*                  Stav ke dni 14.11.1994                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

%DEFINE (ENA_XDA) (0)         ; Compile for XDATA buffers too

	EXTRN NUMBER(MSK_MXD,MSK_SXD,SJ_TSTA,SJ_REND,SJ_TEND)
	EXTRN BIT(ICF_MRQ,ICF_MER,ICF_SRC,ICF_NA)
	EXTRN DATA(IIC_FLG,M_SLA,M_SLEN,M_RLEN,M_DP)
	EXTRN DATA(S_BLEN,S_RLEN,S_DP,S_CADR)
	EXTRN CODE(IIC_RQI,IIC_SLI)
	EXTRN CODE(IIC_WME,IIC_CER,SL_JRET)
%IF (%ENA_XDA) THEN(
	EXTRN CODE(ICF_MXD,ICF_SXD,ICF_PXD,IIC_RQX,IIC_SLX)
)FI

