$NOMOD51
;********************************************************************
;*                 micro detector  - MDET.ASM                       *
;*                       Hlavni modul                               *
;*                  Stav ke dni  5. 4.2000                          *
;*                      (C) Pisoft 2000                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_AL)
$INCLUDE(%INCH_AF)
$INCLUDE(%INCH_ADR)
$INCLUDE(%INCH_REGS)
$INCLUDE(%INCH_UI)
$INCLUDE(%INCH_RS232)
$INCLUDE(%INCH_RSOI)
$LIST

; *******************************************************************

%DEFINE (WITH_RS232)	(0)	; s komunikaci RS232
%DEFINE (WITH_ULAN)	(1)	; s komunikaci uLan
%DEFINE (WITH_UL_OI)	(1)	; s objektovou komunikaci uLan
%DEFINE (WITH_UL_DY)	(1)	; s dynamickou adresaci
%DEFINE (WITH_UL_DY_EEP) (1)	; vyrobnim cislem v EEPROM
%DEFINE (WITH_IIC)	(1)	; s komunikaci IIC
%DEFINE (WITH_IICKB)	(1)	; se vzalenou IIC klavesnici
%DEFINE (WITH_IICRVO)	(1)	; se jednoduchou IIC klavesnici
%DEFINE (WITH_41_KL)	(0)	; se lokalni klavesnici
%DEFINE (WITH_EXTDARKC)	(0)	; externi kalibrace ADS nuly
%DEFINE (TIME_INT_CM)   (1)	; Ktery z komparatoru pro preruseni
				; CMx & ECMx -> int T2CMPx, CMIx

DINT600 EQU   2560 ; Deleni CLK/12 na 600 Hz pro X 18.432 MHz
DINT25  EQU   24   ; Delitel EXINT1 na 25 Hz

BAUDDIV_9600	EQU	10	; pro 18.432 MHz

; *******************************************************************

MDET__C SEGMENT CODE
MDET__D SEGMENT DATA
MDET__B SEGMENT DATA BITADDRESSABLE
MDET__X SEGMENT XDATA

; Vyuziti bank registru (USING)
;  0 .. beh v popredi
;  1 .. komunikace uLAN
;  2 .. casove preruseni
;  3 .. komunikace IIC

RSEG	MDET__C
	JMP   RES_STAR		; Skok na zacatek programu

%IF (%WITH_ULAN) THEN (
EXTRN	CODE(uL_FNC,uL_STR)
EXTRN	CODE(uL_S_OP,uL_S_WR,uL_S_CL)
EXTRN	BIT(uLF_INE)
EXTRN	XDATA(uL_SBP)
EXTRN	XDATA(uL_ADR)
%IF (%WITH_UL_OI) THEN (
$INCLUDE(%INCH_UL_OI)
)FI
%IF(%WITH_UL_DY) THEN (
EXTRN	CODE(UD_INIT,UD_RQ)
)FI
)FI


%IF(%WITH_IIC)THEN(
$INCLUDE(%INCH_IIC)
)FI

PUBLIC	LEB_FLG

%IF(%WITH_IICKB)THEN(
EXTRN	CODE(UI_INIHW,UI_BEEP,KB_KPUSH,KB_IICWRLN,IHEXLDND)
EXTRN	BIT(FL_IICKB)
)FI
EXTRN	BIT(FL_DIPR)

EXTRN	CODE(cxMOVE,xxMOVE,xMDPDP,xJMPDPP,SEL_FNC,ADDATDP)
EXTRN	CODE(xLDl_PA,xLDi_PA,xSVl_PA,xSVi_PA)
EXTRN	CODE(UB_A_WR)
EXTRN   CODE(IHEXLD)
;EXTRN	CODE(PRINThb,PRINThw,INPUThw)
;EXTRN	CODE(MONITOR)
MONITOR	CODE  0
PUBLIC	MONITOR

PUBLIC	INPUTc,BEEP,KBDBEEP,ERRBEEP,RES_STAR

%IF(%WITH_UL_DY_EEP) THEN (
PUBLIC	SER_NUM
CSEG AT	8080H
SER_NUM:DS    10H	; Instrument unigue serial number
XSEG AT	8080H
	DS    10H	; XDATA overlay
)FI

RSEG	MDET__B

LEB_FLG:DS    1		; Blikani ledek

HW_FLG: DS    1
ITIM_RF BIT   HW_FLG.7	; Kontrola reentrance preruseni
FL_25Hz	BIT   HW_FLG.4	; Nastaven pri preruseni
LEB_PHA	BIT   HW_FLG.3	; Pro blikani led
FL_DACO	BIT   HW_FLG.2	; Vystup absorbance na DAC
FL_DACB	BIT   HW_FLG.1	; Vystup z CHA/CHB na DAC

HW_FLG1: DS   1
FL_RDYR1 BIT  HW_FLG1.7 ; Vysli pouze jedno READY
FL_ECUL	 BIT  HW_FLG1.6	; Povolit komunikaci uLAN
FL_ECRS	 BIT  HW_FLG1.5	; Povolit komunikaci RS232
FL_SPILCK BIT HW_FLG1.4	; SPI communication locked

RSEG	MDET__X

TMP:	DS    16

C_R_PER	EQU   5
REF_PER:DS    1		; Perioda refrese displeje

RSEG	MDET__C

RES_STAR:
	MOV   IEN0,#0
	MOV   IEN1,#0
	MOV   IP0,#0
	MOV   IP1,#0
	MOV   PSW,#0
	MOV   SP,#80H
	MOV   PCON,#10000000B	; Bd = OSC/12/16/(256-TH1)
	MOV   TM2CON,#10000001B; timer 2 CLK, TR2 disabled, 16 bit OV
	%VECTOR(T2CMP%TIME_INT_CM,I_TIME1); Realny cas z komparatoru
	SETB  ECM%TIME_INT_CM	; povoleni casu od CMx
	MOV   CTCON,#00000010B	; hrana z P1.0
	%VECTOR(T2CAP0,I_ADS)	; Preruseni AD prevodniku

	MOV   PWMP,#0FFH
	MOV   PWM0,#01H
	MOV   PWM1,#00H

	MOV   P4,#0FFH

	CALL  I_TIMRI
	CALL  I_TIMRI
	MOV   CINT25,#10
	CLR   A
	MOV   DPTR,#TIMRI
	MOVX  @DPTR,A
	MOV   DPTR,#TIME
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#STATUS
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	CLR   A
	MOV   HW_FLG,A
	MOV   HW_FLG1,A
	MOV   LEB_FLG,A
	MOV   LED_FLG,A
	SETB  ITIM_RF
	MOV   DPTR,#uL_SBPO
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
				; Nastaveni AD prevodniku
	CLR   EADS
	MOV   FL_ABS,#ZER_MSK
	%LDMXi (ADC_IFI,1954)
	MOV   DPTR,#ADS1
	CALL  ADS_DFLTS
	MOV   DPTR,#ADS2
	CALL  ADS_DFLTS
	MOV   A,#CAL_MPH
	MOV   DPTR,#CAL_PH
	MOVX  @DPTR,A
	MOV   DPTR,#ADS_WDG	; Zrusit ADS watchdog
	MOV   A,#-1
	MOVX  @DPTR,A

	MOV   R4,#3		; Nastaveni DA prevodniku
	CALL  S_DACATN

	CLR  A
	MOV   DPTR,#AUX_INO	; Nulovani AUX
	MOVX  @DPTR,A
	CALL  S_AUXA


    %IF(%WITH_41_KL)THEN(
	CALL  LCDINST
	CLR   FL_DIPR
	JNZ   RES_ST2		; Test pritomnosti LCD displaye
	SETB  FL_DIPR
	CALL  LEDWR
    )ELSE(
	CLR   FL_DIPR
    )FI
RES_ST2:
    %IF(%WITH_IIC)THEN(
	CALL  INI_IIC		; Inicializace IIC komunikace
    )FI
	MOV   DPTR,#REF_PER
	MOV   A,#C_R_PER
	MOVX  @DPTR,A

	SETB  EA		; Povolit preruseni
	JNB   FL_DIPR,L002
	MOV   DPTR,#DEVER_T
	CALL  cPRINT
	CALL  DB_W_10
L002:
	%LDMXi(COM_TYP,1)	; Nastaveni parametru komunikace
	%LDMXi(COM_ADR,3)	; Adresa 3
	;LDMXi(COM_SPD,3)	; Rychlost 9600
	%LDMXi(COM_SPD,4)	; Rychlost 19200

	%LDMXi(LM_MASK,0)	; Maska vysilani znacky
	%LDMXi(LM_DADR,2)	; Cilova adresa vysilane znacky

    %IF(%WITH_IIC)THEN(
	CALL  TEST_SW0
	JNZ   L006
	CALL  MR_EERD		; Nacteni parametru z EEPROM
L006:
    )FI
    %IF(%WITH_UL_DY_EEP) THEN (
	CALL  INI_SERNUM	; Initialize serial number
    )FI
	CALL  COM_INI		; Odstartovani komunikace

	MOV   DPTR,#ADS1
	CALL  ADS_IAD		; Priprava parametru 1. prevodniku
	MOV   DPTR,#ADS2
	CALL  ADS_IAD		; Priprava parametru 2. prevodniku
	CALL  ADS_INI		; Inicializace prevodniku
	SETB  EADS

    %IF(%WITH_IICRVO)THEN(
	CALL  RVK_INI		; Inicializace IIC klavesnice
    )FI
	JMP   UT

DB_W_10:MOV   R0,#10H
	SJMP  DB_WAI1

DB_WAIT:MOV   R0,#0H
DB_WAI1:%WATCHDOG
	DJNZ  R1,DB_WAI1
	DJNZ  R0,DB_WAI1
	RET

DEVER_T:DB    LCD_CLR,'%VERSION'
	DB    C_LIN2 ,' (c) PiKRON 2000',0

; *******************************************************************

INPUTc:	CALL  SCANKEY
	JZ    INPUTc
	RET

; Pipnuti na klavese klavesnice
;KBDBEEP:JMP   KBDSTDB
KBDBEEP:MOV   A,#2
BEEP:	MOV   DPTR,#BEEPTIM
	MOVX  @DPTR,A
	SETB  %BEEP_FL
	MOV   DPTR,#LED       ; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
	MOV   A,R2
	RET
ERRBEEP:MOV   A,#8
    %IF(%WITH_IICKB)THEN(
	JMP   UI_BEEP
    )ELSE(
	JMP   BEEP
    )FI

; ADC4 vstup tlacitka
; PWM0 vystup na LEDku
TEST_SW0:MOV  A,P5
	ORL   A,P5
	ORL   A,P5
	CPL   A
	ANL   A,#010H
	RET

; *******************************************************************
;
; Casove preruseni

PUBLIC  KBDTIMR

RSEG	MDET__D

CINT25: DS    1

RSEG	MDET__X

BEEPTIM:DS    1	   ; Timer delky pipani

N_OF_T  EQU   6    ; Pocet timeru
TIMR1:  DS    1    ; Timery jsou decrementovany
RVK_TIM:
TIMR2:  DS    1    ; s frekvenci 25 Hz
RVK_TUNST:
TIMR_WAIT:
TIMR3:  DS    1
REF_TIM:DS    1	   ; Refresh timr
KBDTIMR:DS    1
TIMRI:  DS    1
TIME:   DS    2    ; Cas v 0.01 min              =====

RSEG	MDET__C

USING   2
I_TIME1:PUSH  ACC		; Cast s pruchodem 600 Hz
	PUSH  PSW
	MOV   PSW,#AR0		; Prepnuti banky registru
	PUSH  B
	PUSH  DPL
	PUSH  DPH
	MOV   A,CML%TIME_INT_CM
	ADD   A,#LOW  DINT600
	MOV   CML%TIME_INT_CM,A
	MOV   A,CMH%TIME_INT_CM
	ADDC  A,#HIGH DINT600
	MOV   CMH%TIME_INT_CM,A
	CLR   CMI%TIME_INT_CM

	CLR   FL_25Hz
	DJNZ  CINT25,I_TIM10
	SETB  FL_25Hz
I_TIM10:


	JNB   FL_25Hz,I_TIMR1	; Konec casti spruchodem 600 Hz
	MOV   CINT25,#DINT25

	MOV   DPTR,#ADS_WDG	; Kontrola spravne cinnosti ADC
	MOVX  A,@DPTR
	JNZ   I_TIM22
	MOV   DPTR,#STATUS	; Chyba v cinnosti ADC
	MOV   A,#01H
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#0FAH
	MOVX  @DPTR,A
	SJMP  I_TIM23
I_TIM22:DEC   A
	MOVX  @DPTR,A
I_TIM23:

    %IF(%WITH_ULAN)THEN(
	JNB   FL_ECUL,I_TIM70
	CALL  uL_STR
    )FI

I_TIM70:%WATCHDOG
	MOV   DPTR,#BEEPTIM
	MOVX  A,@DPTR
	JZ    I_TIM80
	DEC   A
	MOVX  @DPTR,A
	JNZ   I_TIM80
	CLR   %BEEP_FL
	JNB   FL_DIPR,I_TIM80
	MOV   DPTR,#LED		; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
I_TIM80:MOV   DPTR,#TIMR1
	MOV   B,#N_OF_T-1
I_TIME2:MOVX  A,@DPTR
	JZ    I_TIME3
	DEC   A
	MOVX  @DPTR,A
I_TIME3:INC   DPTR
	DJNZ  B,I_TIME2
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	JNB   ITIM_RF,I_TIMR1
	JB    ACC.7,I_TIME4
I_TIMR1:POP   DPH
	POP   DPL
	POP   B
	POP   PSW
	POP   ACC
	SETB  EA
I_TIMRI:RETI

I_TIME4:CLR   ITIM_RF		; Pruchod 0.6 sec
;	CALL  I_TIMRI
;	MOV   PSW,#AR0		; Banka 2
	ADD   A,#15
	MOVX  @DPTR,A
    %IF(%WITH_IIC)THEN(
	CALL  IIC_STR	      ; naprava zboreneho IIC
    )FI
	MOV   A,LEB_FLG
	JBC   LEB_PHA,I_TIM42
	ORL   LED_FLG,A
	SETB  LEB_PHA
	SJMP  I_TIM43
I_TIM42:CPL   A
	ANL   LED_FLG,A
I_TIM43:SETB  ITIM_RF
	CALL  AUX_ICH		; Zmena na vtupech AUX
	JZ    I_TIM48
	;SETB  GS_BUF.BGS_AUX	; Doslo ke zmene
	MOV   R5,A
	MOV   DPTR,#LM_MASK	; Vyhodnoceni vyslani marky
	MOVX  A,@DPTR
	ANL   A,R5
	ANL   A,R4
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	ANL   A,R5
	ORL   A,R4
	XRL   A,R4	
	ORL   A,R2
	JZ    I_TIM48
	CALL  LAN_MRK		; Vyslani znacky
I_TIM48:
	JMP   I_TIMR1

WAIT_T:	MOV   DPTR,#TIMR_WAIT
	MOVX  @DPTR,A
WAIT_T1:MOV   DPTR,#TIMR_WAIT
	MOVX  A,@DPTR
	JNZ   WAIT_T1
	RET

; *******************************************************************
; Spoluprace s AD7710

AD_DR	BIT   P1.0
AD_DI	BIT   P1.1
AD_DO	BIT   P3.2
AD_CLK	BIT   P3.3	; Positive clock impuls
AD_CS	BIT   P3.4

AD_RFS1	EQU   NOT 01H	; Cteni z ADC 1
AD_TFS1	EQU   NOT 02H	; Zapis do ADC 1
AD_RFS2	EQU   NOT 04H	; Cteni z ADC 2
AD_TFS2	EQU   NOT 08H	; Zapis do ADC 2
AD_SYN1	EQU   NOT 10H	; SYNC pro ADC1
AD_SYN2	EQU   NOT 20H	; SYNC pro ADC2
AD_NA0	EQU   NOT 80H	; Vystup 0 na vodici A0

EADS	BIT   ECT0	; int T2CAP0
ADSI	BIT   CTI0

%DEFINE (ADSDELAY) (
	NOP
	NOP
	NOP
)

ADS8IN: CLR   AD_CS
	MOV   A,#1
ADS8IN1:%ADSDELAY
	SETB  AD_CLK
	;ADSDELAY
	PUSH  ACC
	CLR   A
	MOV   C,AD_DI
	ADDC  A,#0
	MOV   C,AD_DI
	ADDC  A,#0
	MOV   C,AD_DI
	ADDC  A,#0
	MOV   C,ACC.1
	POP   ACC
	CLR   AD_CLK
	RLC   A
	JNC   ADS8IN1
ADS8INR:RET

ADS_CTR:SETB  AD_CS
	SETB  C
	SJMP  ADS8OU2

ADS8OUT:CLR   AD_CS
	SETB  C
	SJMP  ADS8OU2
ADS8OU1:MOV   AD_DO,C
	%ADSDELAY
	SETB  AD_CLK
	CLR   C
ADS8OU2:%ADSDELAY
	CLR   AD_CLK
	RLC   A
	JNZ   ADS8OU1
	RET

ADC_RD:	CALL  ADS_CTR
	CLR   AD_CS
	MOV   R7,#0
	CALL  ADS8IN
	MOV   R6,A
	CALL  ADS8IN
	MOV   R5,A
	CALL  ADS8IN
	MOV   R4,A
	SETB  AD_CS
	RET

ADC_WR:	CALL  ADS_CTR
	CLR   AD_CS
	MOV   A,R6
	CALL  ADS8OUT
	MOV   A,R5
	CALL  ADS8OUT
	MOV   A,R4
	CALL  ADS8OUT
	SETB  AD_CS
	RET

ADC_WRC:MOV   A,#AD_TFS1 AND AD_NA0
	CALL  ADC_WR
	MOV   A,#AD_TFS2 AND AD_NA0
	CALL  ADC_WR
	RET

; *******************************************************************
; Plovouci filtr

RSEG	MDET__X

FI_MLEN	EQU   64	; Maximalni delka plovouciho filtru

FI1_DAT:
FI1_LEN:DS    1		; Aktualni delka filtru
FI1_POS:DS    1		; Aktualni pozice
FI1_TRD:DS    1		; Pocet prevodu za ktere bude filtr redy
FI1_SUM:DS    4		; Suma prvniho filtru
FI1_BUF:DS    3*FI_MLEN	; Buffer prvniho filtru

FI2_DAT:
FI2_LEN:DS    1		; Druhy filtr
FI2_POS:DS    1		; Aktualni pozice
FI2_TRD:DS    1
FI2_SUM:DS    4
FI2_BUF:DS    3*FI_MLEN

RSEG	MDET__C

; Nastvi delku filtru [DPTR] na hodnotu v ACC

FI_SETL:MOV   R1,A
	ADD   A,#-FI_MLEN
	JNC   FI_SL10
	MOV   R1,#FI_MLEN
FI_SL10:MOV   A,R1
	MOVX  @DPTR,A		; FI_LEN
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A		; FI_POS
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A		; FI_TRD
	INC   DPTR
	MOV   R0,#4		; FI_SUM
	CLR   A
FI_SL20:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,FI_SL20
	RET

; Provede filtraci pro filtr [DPTR], vstup R456 NEW, vystup R4567

FI_DO:	MOVX  A,@DPTR	; FI_LEN
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR	; FI_POS
	INC   A
	MOV   R1,A
	XRL   A,R0
	JNZ   FI_DO10
	MOV   R1,#0
FI_DO10:MOV   A,R1
	MOVX  @DPTR,A
	RL    A		; FI_POS*3
	ADD   A,R1
	ADD   A,#4
	MOV   R1,A
	INC   DPTR
	MOVX  A,@DPTR	; FI_TRD
	JZ    FI_DO15
	DEC   A
	MOVX  @DPTR,A
	SETB  F0
	INC   DPTR
	MOV   A,DPL
	MOV   R2,A
	ADD   A,R1
	MOV   DPL,A
	MOV   A,DPH
	MOV   R3,A
	ADDC  A,#0
	MOV   DPH,A
	MOV   A,R4	; Ulozit NEW
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	MOV   R7,#0     ; R4567 = NEW
	SJMP  FI_DO20
FI_DO15:CLR   F0
	INC   DPTR
	MOV   A,DPL
	MOV   R2,A
	ADD   A,R1
	MOV   DPL,A
	MOV   A,DPH
	MOV   R3,A
	ADDC  A,#0
	MOV   DPH,A
	CLR   C		; Ulozit NEW
	MOVX  A,@DPTR	; R4567 = NEW - OLD
	XCH   A,R4
	MOVX  @DPTR,A
	SUBB  A,R4
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R5
	MOVX  @DPTR,A
	SUBB  A,R5
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R6
	MOVX  @DPTR,A
	SUBB  A,R6
	MOV   R6,A
	CLR   A
	SUBB  A,#0
	MOV   R7,A
FI_DO20:MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR	; FI_SUM = FI_SUM + NEW - OLD
	ADD   A,R4	; R4567 = FI_SUM
	MOV   R4,A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOV   R5,A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R6
	MOV   R6,A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R7
	MOV   R7,A
	MOVX  @DPTR,A
	RET

; *******************************************************************
; Vypocty namerenych hodnot a absorbanci

; Nastavi prevodnik na (OAD_CTR or R7<<8) a ADC_IFI
; Konfiguracni registr prevodniku
;   MD2 MD1 MD0 G2 G1 G0 CH PD | WL IO BO B/U FS11-8 | FS8-0
; Operating Mode MD2-0
;	0     Normal
;	1     Vnitrni nula a vnitrni scale
;	2     Active step 1 - external zero
;	3     Active step 2 - external full scale
;	4     Vnejsi nula a vnitrni scale
;	5     Active background cal.
;	6     Read/write zero scale coeficient with A0=1
;	7     Read/write full-scale coeficient with A0=1
; PGA Gain G2-G0
;	0->1; 1->2; 2->4; ... 6->64; 7->128
; Channel selection CH
;	0->AIN1; 1->AIN2
; Power down PD
;	1->Power down
; Word length WL
;	0->16-bit; 1->24-bit
; Output compensation current IO
;	0->off; 1->on
; Burn out current BO
;	0->off; 1->on
; Bipolar/Unipolar selection B/U
;	0->bipolar ; 1->unipolar
;
; Spodni ctyri bity OAD_CTR jsou vyuzity nasledovne
; bit 3	0->linear; 1->logaritmic/absorbance
; bit 2	0->nop;    1->zerro
; bit 1	0->nop;    1->change
; bit 0	0->nop;    1->stop

BADS_ABS SET  3		; Absorbance / linear
MADS_ABS SET  1 SHL BADS_ABS

OAD_LEN	SET   0
%STRUCTM(OAD,CTR ,2)	; Nastaveni formatu AD prevodniku
%STRUCTM(OAD,MAFI,2)	; Moving average in 0.1 s
%STRUCTM(OAD,CMUL,4)	; Calibration multiplying factor
%STRUCTM(OAD,CADD,4)	; Calibration additive constant
%STRUCTM(OAD,UMUL,4)	; User multiplying factor
%STRUCTM(OAD,UADD,4)	; User additive constant
%STRUCTM(OAD,RMUL,4)	; Resulting multiplying factor
			;  RMUL = CMUL*UMUL/MAFI
%STRUCTM(OAD,RADD,4)	; Resulting additive constant
			;  RADD = UADD+CADD*UMUL-
			;         bip*0x800000*CMUL*UMUL
%STRUCTM(OAD,RAW ,4)	; Raw longint ADC value
%STRUCTM(OAD,VAL ,4)	; Float ADC nebo ABS hodnota
OAD_ABS	SET   OAD_VAL
%STRUCTM(OAD,ABSZ,4)	; Nula pro vypocet absorbance

RSEG	MDET__C

; Nastavi pocatecni parametry ADC DPTR -> OAD
ADS_DFLTS:
	%LDR45i(16)		; filtrovat ze 16 vysledku
	MOV   A,#OAD_MAFI
	CALL  xSVi_PA
	;LDR45i(0090H)		; 24 bit, unipolar
	%LDR45i(0080H)		; 24 bit, bipolar
	MOV   A,#OAD_CTR
	CALL  xSVi_PA
	CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R7,#81H
	MOV   A,#OAD_CMUL	; Nasobit 1
	CALL  xSVl_PA
	MOV   A,#OAD_UMUL
	CALL  xSVl_PA
	MOV   R7,#0
	MOV   A,#OAD_CADD	; Pricist 0
	CALL  xSVl_PA
	MOV   A,#OAD_UADD
	CALL  xSVl_PA
	RET

; Napocita parametry ADC podle DPTR -> OAD
ADS_IAD:MOV   A,#OAD_CMUL
	CALL  xLDl_PA
	CALL  Rf2AKUM	; AKUM = OAD_CMUL
	MOV   A,#OAD_UMUL
	CALL  xLDl_PA	; Rf = OAD_UMUL
	MOV   A,#OAD_CTR+1
	MOVC  A,@A+DPTR	; Zapocteni zmeny zesileni
	RR    A
	RR    A
	ANL   A,#7
	XCH   A,R7
	CLR   C
	SUBB  A,R7
	JNC   ADS_IAD2
	CLR   A
ADS_IAD2:MOV  R7,A
	CALL  MULf	; OAD_CMUL*OAD_UMUL
	MOV   A,R6
	XRL   A,#080H
	MOV   R6,A	; Odcitat posun
	MOV   A,#OAD_CTR
	MOVC  A,@A+DPTR
	JNB   ACC.4,ADS_IAD3	; Bipolar operation
	MOV   A,AKUM+3
	JZ    ADS_IAD3
	DEC   AKUM+3		; Unipolar has higher resolution
	MOV   R7,#0
ADS_IAD3:MOV  A,#OAD_RADD
	CALL  xSVl_PA	; OAD_RADD = -bip*Rf*OAD_UMUL*OAD_CMUL
	MOV   A,#OAD_MAFI
	MOVC  A,@A+DPTR
	CALL  CONVbRf	; Rf = OAD_MAFI*OAD_UMUL*OAD_CMUL
	CALL  DIVf
	MOV   A,#OAD_RMUL
	CALL  xSVl_PA	; OAD_RMUL = OAD_UMUL*OAD_CMUL/OAD_MAFI
	MOV   A,#OAD_CADD
	CALL  xLDl_PA
	CALL  Rf2AKUM	; AKUM = OAD_CADD
	MOV   A,#OAD_UMUL
	CALL  xLDl_PA	; Rf = OAD_UMUL
	CALL  MULf
	MOV   A,#OAD_UADD
	CALL  xLDl_PA	; Rf = OAD_UADD
	CALL  ADDf
	MOV   A,#OAD_RADD
	CALL  xLDl_PA	; Rf = OAD_RADD (bip*(...))
	CALL  ADDf
	MOV   A,#OAD_RADD
	CALL  xSVl_PA	; OAD_RADD = OAD_CADD*OAD_UMUL+OAD_UADD+bip
	RET

RSEG	MDET__B

FL_ABS:	DS    1
FL_ZER1	BIT   FL_ABS.7	; Vynuluj 1. absorbanci
FL_ZER2	BIT   FL_ABS.6	; Vynuluj 2. absorbanci
FL_FICH1 BIT  FL_ABS.5	; Zmen rad filtru ADS1
FL_FICH2 BIT  FL_ABS.4	; Zmen rad filtru ADS2
FL_CH2	BIT   FL_ABS.3	; Vystup druheho kanalu
ZER1_MSK SET  0A0H
ZER2_MSK SET  050H
ZER_MSK SET   ZER1_MSK OR ZER2_MSK
CH2_MSK	SET   008H

RSEG	MDET__X

CAL_MPH	SET   3+20	; Pocatecni phase interniho nulovani
CAL_PH:	DS    1		; Phase interniho nulovani
ADC_IFI:DS    2		; Rad filtru v AD7710

ADS1:	DS    OAD_LEN	; Paramatry a data z 1. kanalu
ADS2:	DS    OAD_LEN	; Paramatry a data z 2. kanalu

ADS_SAV:DS    14	; Pro ulozeni AKUM z knihovny PB_AF

ADS_WDG:DS    1		; Watchdog zpravne cinnosti ADC
ADS_WDT	EQU   25*3	; 3s

RSEG	MDET__C

USING   2
I_ADS:  CLR   ADSI
	PUSH  ACC
	PUSH  PSW
	MOV   PSW,#AR0
	PUSH  B
	PUSH  DPL
	PUSH  DPH
	MOV   R0,#AKUM		; Ulozeni stvu PB_AF
	MOV   R2,#14
	MOV   DPTR,#ADS_SAV
	CALL  xiSVs
	MOV   DPTR,#ADS_WDG	; ADC v cinnosti
	MOV   A,#ADS_WDT
	MOVX  @DPTR,A

	JB    AD_DR,I_ADS05
	JNB   AD_DR,I_ADS10
I_ADS05:MOV   A,#10		; Prislo ruseni
	CALL  BEEP
	JMP   I_ADSR

I_ADS10:MOV   DPTR,#CAL_PH
	MOVX  A,@DPTR
	JZ    I_ADS11
	JMP   I_ADS50		; Vnitrni nulovani AD7710
I_ADS11:JNB   FL_FICH1,I_ADS12
	CLR   FL_FICH1		; Zmena radu filtru ADS1
	MOV   DPTR,#ADS1+OAD_MAFI
	MOVX  A,@DPTR
	MOV   DPTR,#FI1_DAT
	CALL  FI_SETL
I_ADS12:JNB   FL_FICH2,I_ADS15
	CLR   FL_FICH2		; Zmena radu filtru ADS2
	MOV   DPTR,#ADS2+OAD_MAFI
	MOVX  A,@DPTR
	MOV   DPTR,#FI2_DAT
	CALL  FI_SETL
I_ADS15:
	MOV   A,#AD_RFS1	; Nacteni dat z 1. prevodniku
	CALL  ADC_RD
	MOV   DPTR,#ADS1+OAD_RAW
	CALL  xSVl
	MOV   DPTR,#FI1_DAT	; Filtrace
	CALL  FI_DO
	JB    F0,I_ADS29
	MOV   A,#-23
I_ADS17:CALL  CONVl_S		; Prevod na float
	MOV   A,R6		; na rozsah <0,2)*OAD_MAFI
	ANL   A,#07FH
	MOV   R6,A
	CALL  Rf2AKUM		; Hodnota z ADC do AKUM
	MOV   DPTR,#ADS1	; Prepocet podle OAD_RMUL
	MOV   A,#OAD_RMUL
	CALL  xLDl_PA
	CALL  MULf
	MOV   A,#OAD_RADD	; Pricist OAD_RADD
	CALL  xLDl_PA
	CALL  ADDf
	MOV   A,#OAD_CTR
	MOVC  A,@A+DPTR
	JNB   ACC.BADS_ABS,I_ADS27
	CALL  LOGfRf		; Logaritmus
	JBC   FL_ZER1,I_ADS28
	MOV   DPTR,#ADS1+OAD_ABSZ
	MOV   R0,#AKUM
	CALL  xiLDl		; Odecteni nuly
	CALL  SUBf
I_ADS27:MOV   DPTR,#ADS1+OAD_ABS
	CALL  xSVl
	SJMP  I_ADS29
I_ADS28:MOV   DPTR,#ADS1+OAD_ABSZ ; Vynulovani absorbance
	CALL  xSVl
	JNB   %LVIS_FL,I_ADS29
	MOV   DPTR,#ADS1+OAD_RAW+2
	MOVX  A,@DPTR
	ADD   A,#-10H
	JC    I_ADS29
	MOV   DPTR,#STATUS	; Chyba zarovky
	MOV   A,#01H
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#0FBH
	MOVX  @DPTR,A
I_ADS29:

	MOV   A,#AD_RFS2	; Nacteni dat z 2. prevodniku
	CALL  ADC_RD
	MOV   DPTR,#ADS2+OAD_RAW
	CALL  xSVl
	MOV   DPTR,#FI2_DAT	; Filtrace
	CALL  FI_DO
	JB    F0,I_ADS39
	MOV   A,#-23
I_ADS35:CALL  CONVl_S		; Prevod na float
	MOV   A,R6		; na rozsah <0,2)*OAD_MAFI
	ANL   A,#07FH
	MOV   R6,A
	CALL  Rf2AKUM		; Hodnota z ADC do AKUM
	MOV   DPTR,#ADS2	; Prepocet podle OAD_RMUL
	MOV   A,#OAD_RMUL
	CALL  xLDl_PA
	CALL  MULf
	MOV   A,#OAD_RADD	; Pricist OAD_RADD
	CALL  xLDl_PA
	CALL  ADDf
	MOV   A,#OAD_CTR
	MOVC  A,@A+DPTR
	JNB   ACC.BADS_ABS,I_ADS37
	CALL  LOGfRf		; Logaritmus
	JBC   FL_ZER2,I_ADS38
	MOV   DPTR,#ADS2+OAD_ABSZ
	MOV   R0,#AKUM
	CALL  xiLDl		; Odecteni nuly
	CALL  SUBf
I_ADS37:MOV   DPTR,#ADS2+OAD_ABS
	CALL  xSVl
	SJMP  I_ADS39
I_ADS38:MOV   DPTR,#ADS2+OAD_ABSZ ; Vynulovani absorbance
	CALL  xSVl
	JNB   %LVIS_FL,I_ADS39
	MOV   DPTR,#ADS2+OAD_RAW+2
	MOVX  A,@DPTR
	ADD   A,#-10H
	JC    I_ADS39
	MOV   DPTR,#STATUS	; Chyba zarovky
	MOV   A,#02H
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#0FBH
	MOVX  @DPTR,A
I_ADS39:
	JMP   I_ADS80

I_ADS50:DEC   A			; Vnitrni kalibrace prevodniku
	MOVX  @DPTR,A
	JNZ   I_ADS52
	JNB   %LVIS_FL,I_ADS51	; 1 .. Ukonceni a synchronizace
	MOV   PWM0,#0FFH
    %IF(%WITH_EXTDARKC)THEN(
    )FI
I_ADS51:MOV   A,#AD_SYN1 AND AD_SYN2
	CALL  ADS_CTR
	NOP
	CLR   AD_CS
	NOP
	NOP
	NOP
	NOP
	SETB  AD_CS
	MOV   A,#AD_RFS1
	CALL  ADC_RD
	MOV   A,#AD_RFS2
	CALL  ADC_RD
	SJMP  I_ADS59
I_ADS52:DEC   A
	JNZ   I_ADS53
    %IF(%WITH_EXTDARKC)THEN(
	MOV   R7,#80H		; 2 .. Vnejsi nula a vnitrni scale
    )ELSE(
	MOV   R7,#20H		; 2 .. Vnitrni nula a vnitrni scale
    )FI
	CALL  ADC_SCF
	SJMP  I_ADS59
I_ADS53:DEC   A
	JNZ   I_ADS54
	MOV   PWM0,#000H
    %IF(%WITH_EXTDARKC)THEN(
	MOV   R7,#80H		; 3 .. Vnejsi nula a vnitrni scale
    )ELSE(
	MOV   R7,#20H		; 3 .. Vnitrni nula a vnitrni scale
    )FI
	CALL  ADC_SCF
	SJMP  I_ADS59
I_ADS54:MOV   A,#AD_RFS1	; >=4 .. cekat
	CALL  ADC_RD
	MOV   A,#AD_RFS2
	CALL  ADC_RD
	;MOV   PWM0,#000H	; Vyrazeno
    %IF(%WITH_EXTDARKC)THEN(
    )FI

I_ADS59:

I_ADS80:
    %IF(0)THEN(
	MOV   DPTR,#TMP
	MOVX  A,@DPTR
	ADD   A,#1
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#0
	MOVX  @DPTR,A
    )FI
	JNB   FL_DACO,I_ADS84	; Vystup na DA prevodnik
	MOV   DPTR,#ADS1+OAD_VAL
	JNB   FL_DACB,I_ADS83
	MOV   DPTR,#ADS2+OAD_VAL
I_ADS83:CALL  xLDl
	CALL  DAC_ABS
I_ADS84:

	CALL  LAN_TM		; !!!!!!!!!!!!!!!!!!!!!

I_ADSR:	MOV   R0,#AKUM
	MOV   R2,#14
	MOV   DPTR,#ADS_SAV
	CALL  xiLDs
	POP   DPH
	POP   DPL
	POP   B
	POP   PSW
	POP   ACC
	RETI

ADC_SCF:MOV   DPTR,#ADC_IFI
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   DPTR,#ADS1+OAD_CTR
	MOVX  A,@DPTR
	ANL   A,#0F0H
	XCH   A,R5
	ANL   A,#00FH
	ORL   A,R5
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	ORL   A,R7
	MOV   R6,A
	MOV   A,#AD_TFS1 AND AD_NA0
	CALL  ADC_WR		; Set parameters of 1. ADC
	MOV   DPTR,#ADS2+OAD_CTR
	MOVX  A,@DPTR
	ANL   A,#0F0H
	XCH   A,R5
	ANL   A,#00FH
	ORL   A,R5
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	ORL   A,R7
	MOV   R6,A
	MOV   A,#AD_TFS2 AND AD_NA0
	CALL  ADC_WR		; Set parameters of 2. ADC
	RET

; Ceka na AD_DR nevyuziva-li se preruseni
ADS_WAIT:MOV  DPTR,#ADS_WDG	; Kontrola spravne cinnosti ADC
	MOV   A,#ADS_WDT
	MOVX  @DPTR,A
ADS_WA2:JNB   AD_DR,ADS_WA9
	MOVX  A,@DPTR
	JNZ   ADS_WA2
ADS_WA9:RET

; Inicializace prevodniku
ADS_INI:MOV   C,EADS
	PUSH  PSW
	CLR   EADS
	CALL  ADS_WAIT		; Cekani na AD_DR
	MOV   R7,#20H		; Vnitrni nula
	CALL  ADC_SCF
	CALL  DB_W_10
	CALL  ADS_WAIT		; Cekani na AD_DR

	MOV   R7,#000H		; Normal rezim
	CALL  ADC_SCF

	MOV   A,#AD_SYN1 AND AD_SYN2
	CALL  ADS_CTR
	CLR   AD_CS
	CALL  DB_W_10
	SETB  AD_CS
	POP   PSW
	MOV   EADS,C
	RET

ADS_FLCH:MOV  A,R2
	CPL   A
	ANL   FL_ABS,A
	MOV   A,R3
	ORL   FL_ABS,A
	RET

ADS_CNUL:MOV  A,#CAL_MPH		; Spusteni vnitrniho nulovani
	MOV   DPTR,#CAL_PH
	MOVX  @DPTR,A
	RET

; *******************************************************************
; Emulace SPI

SPI_PORT DATA P4	; Brana, ke ktere je pripojena pamet
SPI_DO	EQU   008H	; DI periferie
SPI_DI	EQU   008H	; DO periferie
SPI_CL	EQU   001H	; CL

; Pristup na port musi byt chranen FL_SPILCK proti reentranci

; Nacte 8 bitu z periferie do ACC
SPI8IN:	MOV   A,#1
SPI8IN1:ORL   SPI_PORT,#SPI_CL		; Clock
	NOP
	ANL   SPI_PORT,#NOT SPI_CL
	PUSH  ACC
	MOV   A,SPI_PORT
	ANL   A,#SPI_DI
	ADD   A,#-1
	POP   ACC
	RLC   A
	JNC   SPI8IN1
	RET

; Vysle 8 bitu v ACC do periferie
SPI8OUT:SETB  C
SPI8OU0:RLC   A
SPI8OU1:JNC   SPI8OU2
	ORL   SPI_PORT,#SPI_DO
	SJMP  SPI8OU3
SPI8OU2:ANL   SPI_PORT,#NOT SPI_DO
SPI8OU3:ORL   SPI_PORT,#SPI_CL		; Clock
	NOP
	ANL   SPI_PORT,#NOT SPI_CL
	CLR   C
	RLC   A
	JNZ   SPI8OU1
	RET

SPI3OUT:ANL   A,#7
	SWAP  A
	RL    A
	ORL   A,#10H
	CLR   C
	SJMP  SPI8OU0

SPI7OUT:RL    A
	ORL   A,#1
	CLR   C
	SJMP  SPI8OU0

SPI6OUT:ANL   A,#03FH
	RL    A
	RL    A
	ORL   A,#2
	CLR   C
	SJMP  SPI8OU0

; *******************************************************************
; Vystup na prevodnik AD1856

DAC_SEL	EQU   002H	; sestupna hrana zapis dat

DAC_OFIX:CLR  FL_DACO
DAC_OUT:CLR   A
	JB    FL_SPILCK,DAC_OUT9
	SETB  FL_SPILCK
	ANL   SPI_PORT,#NOT SPI_CL
	ORL   SPI_PORT,#DAC_SEL
	MOV   A,R5
	CALL  SPI8OUT
	MOV   A,R4
	CALL  SPI8OUT
	ANL   SPI_PORT,#NOT DAC_SEL
	ORL   SPI_PORT,#SPI_CL
	NOP
	ANL   SPI_PORT,#NOT SPI_CL
	ORL   SPI_PORT,#DAC_SEL
	CLR   FL_SPILCK
	MOV   A,#1
DAC_OUT9:RET

; *******************************************************************
; Vystup na absorbance na prevodnik

RSEG	MDET__X

DAC_ATN:DS    2		; Stupen atenuatoru
DAC_MUL:DS    4		; Nasobeni vystupu na DAC

RSEG	MDET__C

DAC_ABS:
    %IF(0)THEN(
	MOV   R7,#081H
	MOV   R6,#0
	MOV   R5,#0
    )FI
	MOV   DPTR,#DAC_MUL
	MOV   R0,#AKUM
	CALL  xiLDl
	CALL  MULf
	MOV   A,R6
	MOV   R2,A
	ORL   A,#80H
	MOV   R6,A
	MOV   A,R7
	ADD   A,#-080H+15
	JC    DAC_AB3
	MOV   R4,#0
	MOV   R5,#0
	SJMP  DAC_AB9
DAC_AB3:CPL   A
	ADD   A,#15+1
	JNB   ACC.7,DAC_AB4
	MOV   R4,#0FFH
	MOV   R5,#07FH
	SJMP  DAC_AB8
DAC_AB4:INC   A
	JBC   ACC.3,DAC_AB5
	XCH   A,R6
	XCH   A,R5
	MOV   R4,A
	SJMP  DAC_AB6
DAC_AB5:XCH   A,R6
	MOV   R4,A
	MOV   R5,#0
DAC_AB6:MOV   A,R6
	CALL  SHRi
DAC_AB8:MOV   A,R2
	JNB   ACC.7,DAC_AB9
	CALL  NEGi
DAC_AB9:JMP   DAC_OUT

; Nastavi atenuator podle R4
S_DACAT23:
	MOV   A,R2
	MOV   R4,A
S_DACATN:
	CLR   A
	MOV   DPTR,#DAC_ATN+1
	MOVX  @DPTR,A
	MOV   A,R4
	MOV   DPTR,#DAC_ATN
	MOVX  @DPTR,A
	JZ    S_DACAT9
	DEC   A
	RL    A
	RL    A
	MOV   DPTR,#DAC_ATNt
	CALL  ADDATDP
	CALL  xLDl
	MOV   DPTR,#DAC_MUL
	CALL  xSVl
S_DACAT9:SETB  FL_DACO
	MOV   A,#1
	RET

DAC_ATNt:
	DB    000H,000H,000H,000H	;  0
	DB    000H,000H,000H,080H	;  2
	DB    000H,000H,000H,081H	;  1
	DB    000H,000H,000H,082H	; 0.5
	DB    000H,000H,020H,083H	; 0.2
	DB    000H,000H,020H,084H	; 0.1
	DB    000H,000H,020H,085H	; .05
	DB    000H,000H,048H,086H	; .02
	DB    000H,000H,048H,087H	; .01

G_DACAUFS:
	MOV   DPTR,#DAC_MUL
	MOV   AKUM+0,#000H
	MOV   AKUM+1,#000H
	MOV   AKUM+2,#07AH
	MOV   AKUM+3,#08AH	; 1000 = 00 00 7A 8A
	CALL  xLDl
	CALL  DIVf		; deleni Af,Rf:=Af/Rf
	JMP   CONVRf2i

%IF(%WITH_IICRVO)THEN(
; Zmena AUFS
RV_EFAUFS:
	MOV   DPTR,#DAC_MUL+3
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   R4,#0
	MOV   DPTR,#DAC_ATNt+3
RV_EFAUFS2:
	INC   R4
	MOVX  A,@DPTR
	INC   DPTR
	INC   DPTR
	INC   DPTR
	INC   DPTR
	CLR   C
	SUBB  A,R5
	JNC   RV_EFAUFS3
	CJNE  R4,#10,RV_EFAUFS2
RV_EFAUFS3:
	CJNE  R2,#RVKL_MORE,RV_EFAUFS4
	INC   R4
RV_EFAUFS4:
	CJNE  R2,#RVKL_LESS,RV_EFAUFS5
	DEC   R4
RV_EFAUFS5:
	MOV   A,R4
	ADD   A,#-10
	JNC   RV_EFAUFS6
	MOV   R4,#0
RV_EFAUFS6:
	JMP   S_DACATN
)FI

; *******************************************************************

RSEG	MDET__X

LM_MASK:DS    2		; Maska zmen vstupu pro vyslani znacky
LM_DADR:DS    2		; Cilova adresa pro vyslani znacky

uL_SBPO:DS    2
LAN_TMB:DS    15

RSEG	MDET__C

XC_SBPO:MOV   DPTR,#uL_SBPO
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R1,A
	MOV   DPTR,#uL_SBP
	MOVX  A,@DPTR
	XCH   A,R0
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R1
	MOVX  @DPTR,A
	MOV   DPTR,#uL_SBPO
	MOV   A,R0
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A
	RET

LAN_TM:	JNB   ES,LAN_TME
	CALL  XC_SBPO
	JMP   LAN_A

LAN_TMR:CALL  XC_SBPO
LAN_TME:RET

LAN_A1:	;MOV   DPTR,#uL_GRP
	;MOVX  A,@DPTR
	MOV   A,#2
	MOV   R4,A
	MOV   R5,#4FH
	CLR   F0
	CALL  uL_S_OP
	JNB   F0,LAN_A
	; informovat o chybe
	JMP   LAN_TMR

LAN_A:  MOV   DPTR,#ADS1+OAD_VAL
	JNB   FL_CH2,LAN_A2
	MOV   DPTR,#ADS2+OAD_VAL
LAN_A2:	CALL  xLDl

LAN_A3:	MOV   A,R6
	RLC   A
	MOV   A,R7
	JZ    LAN_A4
	RRC   A
	DEC   A
	MOV   R7,A
	MOV   A,R6
	MOV   ACC.7,C
LAN_A4:	MOV   R6,A
	MOV   DPTR,#LAN_TMB
	CALL  xSVl

	MOV   DPTR,#LAN_TMB
	MOV   R4,#4
	MOV   R5,#0
	CLR   F0
	CALL  uL_S_WR
	JB    F0,LAN_A1
	JMP   LAN_TMR

; Vysle znacku na adresu v [LM_DADR]
LAN_MRK:JNB   FL_ECUL,L_MRKR
	;MOV   DPTR,#LM_PROT
	;MOVX  A,@DPTR
	;CJNE  A,#1,L_MRKR
	MOV   DPTR,#LM_DADR
	MOVX  A,@DPTR
	MOV   R4,A

	MOV   DPTR,#uL_SBP
	MOVX  A,@DPTR
	PUSH  ACC
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	PUSH  ACC
	CLR   A
	MOVX  @DPTR,A

	;MOV   DPTR,#uL_GRP
	;MOVX  A,@DPTR
	;MOV   A,#2
	;MOV   R4,A
	MOV   R5,#4EH
	CLR   F0
	CALL  uL_S_OP
	MOV   DPTR,#L_MRKD
	MOV   R4,#4
	MOV   R5,#0
	CALL  uL_S_WR
	CALL  uL_S_CL

	JNB   F0,L_MRK1
	; informovat o chybe
	;SETB  uLE_ABS
L_MRK1:
	MOV   DPTR,#uL_SBP+1
	POP   ACC
	MOVX  @DPTR,A
	CALL  DECDPTR
	POP   ACC
	MOVX  @DPTR,A
L_MRKR:	MOV   A,#1
	RET

L_MRKD: DB    0,7,0,0

; *******************************************************************
; Konfigurace komunikaci RS232 a uLAN

;FL_ECUL	Povolit komunikaci uLAN
;FL_ECRS	Povolit komunikaci RS232

RSEG	MDET__X

COM_TYP:DS    2		; Typ komunikace
COM_ADR:DS    2		; Adresa jednotky
COM_SPD:DS    2		; Rychlost komunikace

RSEG	MDET__C

COM_INI:CLR   ES
	CLR   FL_ECUL
	CLR   FL_ECRS
	MOV   DPTR,#COM_TYP
	MOVX  A,@DPTR
	DJNZ  ACC,COM_I50
	; Bude se pouzivat uLAN
%IF(%WITH_ULAN)THEN(
	SETB  FL_ECUL
	CALL  I_U_LAN
	CALL  uL_OINI
)FI
	RET
COM_I50:DJNZ  ACC,COM_I99
	; Bude se pouzivat RS232
%IF(%WITH_RS232)THEN(
	SETB  FL_ECRS
	CALL  COM_DIV
	MOV   R7,A
	CALL  RS232_INI
	SETB  PS
COM_RS_OPL:
	%LDMXi(RS_OPL,RS_OPL1)
)FI
COM_I99:RET

COM_DIVT:	; Tabulka delitelu frekvence
	DB    BAUDDIV_9600*8	;  1200
	DB    BAUDDIV_9600*4	;  2400
	DB    BAUDDIV_9600*2	;  4800
	DB    BAUDDIV_9600	;  9600
	DB    BAUDDIV_9600/2	; 19200
	DB    BAUDDIV_9600/3	; 28800
	DB    0

; Vraci v ACC divisor pro danou rychlost
COM_DIV:MOV   DPTR,#COM_SPD
	MOVX  A,@DPTR
	MOV   DPTR,#COM_DIVT
	MOVC  A,@A+DPTR
	RET

; Zmeni rychlost komunikace
COM_SPDCH:
	MOV   DPTR,#COM_SPD
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	MOV   DPTR,#COM_DIVT
	MOVC  A,@A+DPTR
	MOV   DPTR,#COM_SPD
	JNZ   COM_SC1
	MOVX  @DPTR,A
COM_SC1:INC   DPTR
	CLR   A
	MOVX  @DPTR,A
	JMP   COM_RQINI

UW_COMi:
	CALL  UW_Mi
	JMP   COM_RQINI

COM_WR23:
	MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	JMP   UB_A_WR

COM_TYPCH:
	MOV   DPTR,#COM_TYP
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	ADD   A,#-3
	JNC   COM_TC1
	CLR   A
	MOVX  @DPTR,A
COM_TC1:INC   DPTR
	CLR   A
	MOVX  @DPTR,A
COM_RQINI:
	SETB  FL_REFR
%IF (0) THEN (
	MOV   R2,#GL_COMCH
	MOV   R3,#0
	JMP   GLOB_RQ23
)ELSE(
	JMP   COM_INI
)FI

%IF(%WITH_ULAN)THEN(
I_U_LAN:CLR   A
	MOV   DPTR,#uL_SBPO
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	CALL  COM_DIV
	MOV   R0,#1
	CALL  uL_FNC	; Rychlost
	MOV   DPTR,#COM_ADR
	MOVX  A,@DPTR
	MOV   R0,#2
	CALL  uL_FNC	; Adresa
	MOV   R2,#0
	MOV   R0,#3
	CALL  uL_FNC	; Delka IB OB
	MOV   R2,#0
	MOV   R0,#4
	CALL  uL_FNC	; Rychle bloky
	MOV   R0,#0
	CALL  uL_FNC	; Start
%IF(%WITH_UL_DY) THEN (
	%LDR45i(STATUS)
	MOV   R6,#2
	CALL  UD_INIT
)FI
	MOV   A,#1
	RET

%IF(%WITH_UL_DY_EEP) THEN (
	DB    -'U',-'L',-'D',-'Y'
	%W   (0)
	%W   (SER_NUM)
	%W   (WR_SERNUM)
INI_SERNUM:
	MOV   R2,#010H	; pocet prenasenych byte
	MOV   R4,#0F0H	; pocatecni adresa v EEPROM
	MOV   DPTR,#SER_NUM
	CALL  EE_RD
	JZ    INI_SERNUM9
INI_SERNUM7:
	MOV   R2,#8
	MOV   DPTR,#SER_NUM
	MOV   A,#0FFH
INI_SERNUM8:
	MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R2,INI_SERNUM8
INI_SERNUM9:
	RET
WR_SERNUM:
        CLR   EAD
	CLR   ES
	MOV   PSW,#0
	MOV   SP,#80H
	CALL  I_TIMRI
	CALL  I_TIMRI
	CALL  WR_SERNUM1
	MOV   PWM0,#020H
	JMP   $
WR_SERNUM1:
	MOV   PWM0,#0C0H
	MOV   R2,#010H	; pocet prenasenych byte
	MOV   R4,#0F0H	; pocatecni adresa v EEPROM
	MOV   DPTR,#SER_NUM
	CALL  EE_WR
	RET

PUBLIC	UD_NCS_ADRNVSV

; Dodana funkce pro trvale ulozeni adresy v EEPROM
UD_NCS_ADRNVSV:
	MOV   DPTR,#uL_ADR
	MOVX  A,@DPTR
	MOV   DPTR,#COM_ADR
	MOVX  @DPTR,A
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A
	JMP   MR_EEWR
)FI
)FI

%IF(0)THEN(
EXTRN	CODE(UD_SNST_DEF)
PUBLIC	uL_R_BU,uL_R_CO
uL_R_CO:CJNE  A,#0C1H,uL_R_BU
	POP   ACC
	POP   ACC
	JMP   UD_SNST_DEF
uL_R_BU:RET
)FI

; *******************************************************************
; Komunikace pres uLan - cast slave

%IF (%WITH_ULAN) THEN (

RSEG	MDET__B
UD_BBBB:DS    1
UDF_RDP	BIT   UD_BBBB.7

RSEG	MDET__X

TMP_U:	DS    16

RSEG	MDET__C

%IF (%WITH_UL_OI) THEN (
uL_OINI:%LDR45i (OID_1IN)	; seznam prijimanych prikazu
	%LDR67i (OID_1OUT)	; seznam vysilanych prikazu
	JMP   US_INIT
UD_OI:  CALL  UI_PR
    %IF(%WITH_UL_DY) THEN (
	CALL  UD_RQ		; dynamicaka adresace
    )FI
UD_REFR:RET
)ELSE(
uL_OINI:RET
UD_OI:	RET
UD_REFR:RET
)FI

; Identifikace typu pristroje
PUBLIC	uL_IDB,uL_IDE
uL_IDB: DB    '.mt %VERSION .uP 51x'
    %IF(%WITH_UL_DY) THEN (
	DB    ' .dy'
    )FI
	DB    0
uL_IDE:

)FI

; Cteni stavu
G_STATUS:
	MOV   DPTR,#STATUS
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOV   EA,C
	MOVX  A,@DPTR
	MOV   R5,A
	ORL   A,R4
	JNZ   G_STAT9
	JB    %LVIS_FL,G_STAT1
	%LDR45i (0)
	SJMP  G_STAT9
G_STAT1:%LDR45i (1)
G_STAT9:RET

; Nulovani chyboveho stavu
ERRCLR_U:
	MOV   DPTR,#ADS_WDG
	MOVX  A,@DPTR
	JNZ   ERRCLR_U1
	CALL  ADS_INI		; Revitalizace prevodniku
ERRCLR_U1:
	CLR   A
	MOV   DPTR,#STATUS
	MOV   C,EA
	CLR   EA
	MOVX  @DPTR,A
	INC   DPTR
	MOV   EA,C
	MOVX  @DPTR,A
	RET

; Ukladani konfigurace

SAVECFG_U:%LDR23i(EEC_SER); Ulozeni parametru do EEPROM
	JMP   EEP_WRS

; Nulovani a prikazy pro prevodnik

ADSFL_U:CALL  xLDR23i
	JMP   ADS_FLCH

; Prime cteni longint cisla

UO_INTRl:MOVX A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R0
	MOV   DPH,A
	MOV   C,EA
	CLR   EA
	CALL  xLDl
	MOV   EA,C
	MOV   DPTR,#TMP_U
	CALL  xSVl
	MOV   DPTR,#TMP_U
	%LDR45i(4)
	%VJMP (UV_WR)

; Cteni absorbance/hodnoty v pohyblive radove carce

UO_ABSf:MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R0
	MOV   DPH,A
	MOV   C,EA
	CLR   EA
	CALL  xLDl	; R4567 = absorbance float
	MOV   EA,C
        CALL  f2IEEE
	MOV   DPTR,#TMP_U
	CALL  xSVl
	MOV   DPTR,#TMP_U
	%LDR45i(4)
	%VJMP (UV_WR)

; Cteni kanalu v pevne radove carce
UR_CHi3:MOV   A,#OU_DP
	CALL  ADDATDP
	CALL  xMDPDP
	MOV   R4,#000H
	MOV   R5,#000H
	MOV   R6,#07AH
	MOV   R7,#08AH	;  1000 = 00 00 7A 8A
	SJMP  G_CHA02

; Cteni kanalu v pevne radove carce pro komunikace
G_CHAi_U:MOV  DPTR,#ADS1+OAD_VAL
	SJMP  G_CHA01
G_CHBi_U:MOV  DPTR,#ADS2+OAD_VAL
G_CHA01:MOV   R4,#000H
	MOV   R5,#040H
	MOV   R6,#01CH	;  1000 = 00 00 7A 8A
	MOV   R7,#08EH	; 10000 = 00 40 1C 8E
G_CHA02:MOV   R0,#AKUM
	MOV   C,EA
	CLR   EA
	CALL  xiLDl
	MOV   EA,C
	CALL  MULf
CONVRf2i:MOV  A,R7
	ADD   A,#-81H-15
	JNC   G_CHA40
	MOV   R4,#0FFH
	MOV   R5,#07FH
	MOV   A,R6
	MOV   R2,A
	SJMP  G_CHA80
G_CHA40:MOV   A,R6
	MOV   R2,A
	ORL   A,#80H
	MOV   R6,A
	MOV   A,R7
	JB    ACC.7,G_CHA50
	MOV   R4,#0
	MOV   R5,#0
	MOV   R6,#0
	MOV   R7,#0
	CLR   F0
	RET
G_CHA50:CPL   A
	ADD   A,#98H+1
	MOV   R7,#0
G_CHA70:CALL  SHRl
G_CHA80:MOV   A,R2
	JNB   ACC.7,G_CHA90
	CALL  NEGl
G_CHA90:CLR   F0
	RET

; Nastaveni radu filtru
S_FILT_U:
	MOV   B,#MADP_FILT1 OR MADP_FILT2
	SJMP  S_ADPAR
; Nastaveni modu ADC
S_ADCMODE_U:
	MOV   B,#MADP_MODE1 OR MADP_MODE2
	SJMP  S_ADPAR

; Nastaveni parametru ADC na hodnotu v R45 podle B
; Vyznam bitu v B
;  0 ADS1.CTR, 1 ADS2.CTR, 2 ADS1.MAFI, 3 ADS2.MAFI
;  4 ADS1(FL_FICH), 5 ADS2(FL_FICH), 6 ADS_INI
MADP_MODE1 SET 01010001B
MADP_MODE2 SET 01100010B
MADP_FILT1 SET 00010100B
MADP_FILT2 SET 00101000B

S_ADPAR_U:
	MOVX  A,@DPTR
	MOV   B,A
S_ADPAR:MOV   C,EADS
	PUSH  PSW
	CLR   EADS
	JNB   B.0,S_ADP11
	MOV   DPTR,#ADS1+OAD_CTR
	CALL  xSVR45i		; Set config word ADS1.CTR
S_ADP11:JNB   B.1,S_ADP12
	MOV   DPTR,#ADS2+OAD_CTR
	CALL  xSVR45i		; Set config word ADS2.CTR
S_ADP12:MOV   A,R4
	JNZ   S_ADP21
	INC   A
S_ADP21:CJNE  A,#FI_MLEN,S_ADP22
S_ADP22:JC    S_ADP23
	MOV   A,#FI_MLEN
S_ADP23:JNB   B.2,S_ADP24
	MOV   DPTR,#ADS1+OAD_MAFI
	MOVX  @DPTR,A		; Set filter ADS1.MAFI
	PUSH  ACC
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A
	POP   ACC
S_ADP24:JNB   B.3,S_ADP25
	MOV   DPTR,#ADS2+OAD_MAFI
	MOVX  @DPTR,A		; Set filter ADS2.MAFI
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A
S_ADP25:JNB   B.4,S_ADP31
	SETB  FL_FICH1		; Refill ADS1 filter
S_ADP31:JNB   B.5,S_ADP32
	SETB  FL_FICH2		; Refill ADS2 filter
S_ADP32:JNB   B.6,S_ADP41
	CALL  ADS_INI		; Internal zerro and reinitialize
S_ADP41:
S_ADP80:MOV   DPTR,#ADS1
	CALL  ADS_IAD
	MOV   DPTR,#ADS2
	CALL  ADS_IAD
	POP   PSW
	MOV   EADS,C
	RET

; Zapis prevodnich konstsnt v pohyblive radove carce

UI_ADSf_DAC:
	SETB  FL_DACO
UI_ADSf:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#TMP_U
	%LDR45i(4)
	%VCALL(UV_RD)
	MOV   DPTR,#TMP_U
	CALL  xLDl	; R4567 menena konstanta
	POP   DPH
	POP   DPL
	JB    F0,UI_ADSf9
	CALL  IEEE2f
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R0
	MOV   DPH,A
	MOV   C,EA
	CLR   EA
	CALL  xSVl	; zapis parametru
	MOV   EA,C
	MOV   C,EADS
	PUSH  PSW
	CLR   EADS
	JMP   S_ADP80
UI_ADSf9:RET

LAMPC_TOGLE:
	CLR   A
	MOV   R5,A
	MOV   C,%LVIS_FL
	CPL   C
	MOV   ACC.0,C
	MOV   R4,A
S_LAMPC_U:
	MOV   A,R5
	RLC   A
	MOV   A,R4
	CPL   A
	INC   A
	JC    S_LAMPC3
	MOV   C,%LVIS_FL
	CLR   %LVIS_FL
	JZ    S_LAMPC3
    %IF(NOT %WITH_EXTDARKC)THEN(
	JC    S_LAMPC2
	; Prodleva pro zapnuti lampy a naslednou kalibraci
	MOV   A,#0FFH		; CAL_MPH
	MOV   DPTR,#CAL_PH
	MOVX  @DPTR,A
    )FI
S_LAMPC2:SETB %LVIS_FL
	MOV   A,#0FFH
S_LAMPC3:MOV  PWM1,A
	RET

; *******************************************************************
; Prikazy pres uLan

%IF (%WITH_UL_OI) THEN (


C_ADCSAMPPER:
	%W (100)

; Kody prikazu

%OID_ADES(AI_STATUS,STATUS,u2)
%OID_ADES(AI_ERRCLR,ERRCLR,e)
I_ADCSAMPPER EQU 205
%OID_ADES(AI_ADCSAMPPER,ADCSAMPPER,u2/.3)
I_LAMPC	  EQU   207
%OID_ADES(AI_LAMPC,LAMPC,u2)
I_ADCFILT EQU   208
%OID_ADES(AI_ADCFILT,ADCFILT,u2)
I_ADCMODE EQU   209
%OID_ADES(AI_ADCMODE,ADCMODE,u2)
I_ADCAl	  EQU   210
%OID_ADES(AI_ADCAl,ADCAl,s4)
I_ADCBl	  EQU   211
%OID_ADES(AI_ADCBl,ADCBl,s4)
I_CHA	  EQU   220
%OID_ADES(AI_CHA,CHA,f4)
I_CHB	  EQU   221
%OID_ADES(AI_CHB,CHB,f4)
I_CHAi	  EQU   230
%OID_ADES(AI_CHAi,CHAi,s2)
I_CHBi	  EQU   231
%OID_ADES(AI_CHBi,CHBi,s2)
I_CHA_CM  EQU   232
%OID_ADES(AI_CHA_CM,CHA_CM,f4)
I_CHB_CM  EQU   233
%OID_ADES(AI_CHB_CM,CHB_CM,f4)
I_CHA_CA  EQU   234
%OID_ADES(AI_CHA_CA,CHA_CA,f4)
I_CHB_CA  EQU   235
%OID_ADES(AI_CHB_CA,CHB_CA,f4)
I_CHA_UM  EQU   236
%OID_ADES(AI_CHA_UM,CHA_UM,f4)
I_CHB_UM  EQU   237
%OID_ADES(AI_CHB_UM,CHB_UM,f4)
I_CHA_UA  EQU   238
%OID_ADES(AI_CHA_UA,CHA_UA,f4)
I_CHB_UA  EQU   239
%OID_ADES(AI_CHB_UA,CHB_UA,f4)
I_CHA_MODE EQU  240
%OID_ADES(AI_CHA_MODE,CHA_MODE,u2)
I_CHB_MODE EQU  241
%OID_ADES(AI_CHB_MODE,CHB_MODE,u2)
I_CHA_FILT EQU  242
%OID_ADES(AI_CHA_FILT,CHA_FILT,u2)
I_CHB_FILT EQU  243
%OID_ADES(AI_CHB_FILT,CHB_FILT,u2)
I_OFF	  EQU   250
%OID_ADES(AI_OFF,OFF,e)
I_ON	  EQU   251
%OID_ADES(AI_ON,ON,e)
I_ZERO	  EQU   255
%OID_ADES(AI_ZERO,ZERO,e)
I_FIC	  EQU   256
%OID_ADES(AI_FIC,FIC,e)
I_CHA_ZERO EQU  257
%OID_ADES(AI_CHA_ZERO,CHA_ZERO,e)
I_CHB_ZERO EQU  258
%OID_ADES(AI_CHB_ZERO,CHB_ZERO,e)
I_TEMP1	  EQU   301
I_TEMP1RQ EQU   311
I_TEMP_OFF EQU  334
I_TEMP_ON EQU   335
I_TEMP_ST EQU   336
I_TEMP1MC EQU   351
I_TEMP1OC EQU   361
I_TEMP1RD EQU   371
I_SAVECFG EQU   451
%OID_ADES(AI_SAVECFG,SAVECFG,e)
I_DACOUT  EQU   410
%OID_ADES(AI_DACOUT,DACOUT,s2)
I_DACMUL  EQU   411
%OID_ADES(AI_DACMUL,DACMUL,f4)
I_AUXUAL  EQU	440
%OID_ADES(AI_AUXUAL,AUXUAL,u2)
I_MARK_DADR EQU 461
%OID_ADES(AI_MARK_DADR,MARK_DADR,u2)
I_MARK_MASK EQU 462
%OID_ADES(AI_MARK_MASK,MARK_MASK,u2)

)FI
%IF (%WITH_ULAN) THEN (
; Prijimane hodnoty

OID_T	SET   $
	%W    (I_ERRCLR)
	%W    (OID_ISTD)
	%W    (AI_ERRCLR)
	%W    (ERRCLR_U)

%OID_NEW(I_SAVECFG ,AI_SAVECFG)
	%W    (SAVECFG_U)

%OID_NEW(I_MARK_DADR,AI_MARK_DADR)
	%W    (UI_INT)
	%W    (LM_DADR)
	%W    (0)

%OID_NEW(I_MARK_MASK,AI_MARK_MASK)
	%W    (UI_INT)
	%W    (LM_MASK)
	%W    (0)

%OID_NEW(I_AUXUAL,AI_AUXUAL)
	%W    (UI_INT)
	%W    (0)
	%W    (S_AUX)

%OID_NEW(I_DACMUL,AI_DACMUL)
	%W    (UI_ADSf_DAC)
	%W    (DAC_MUL)

; ADC mode and filter
%OID_NEW(I_ADCFILT,AI_ADCFILT)
	%W    (UI_INT)
	%W    (0)
	%W    (S_FILT_U)

%OID_NEW(I_ADCMODE,AI_ADCMODE)
	%W    (UI_INT)
	%W    (0)
	%W    (S_ADCMODE_U)

%OID_NEW(I_CHA_MODE,AI_CHA_MODE)
	%W    (UI_INT)
	%W    (0)
	%W    (S_ADPAR_U)
	DB    MADP_MODE1

%OID_NEW(I_CHB_MODE,AI_CHB_MODE)
	%W    (UI_INT)
	%W    (0)
	%W    (S_ADPAR_U)
	DB    MADP_MODE2

%OID_NEW(I_CHA_FILT,AI_CHA_FILT)
	%W    (UI_INT)
	%W    (0)
	%W    (S_ADPAR_U)
	DB    MADP_FILT1

%OID_NEW(I_CHB_FILT,AI_CHB_FILT)
	%W    (UI_INT)
	%W    (0)
	%W    (S_ADPAR_U)
	DB    MADP_FILT2

; Callibration/user mull/add
%OID_NEW(I_CHA_CM,AI_CHA_CM)
	%W    (UI_ADSf)
	%W    (ADS1+OAD_CMUL)

%OID_NEW(I_CHB_CM,AI_CHB_CM)
	%W    (UI_ADSf)
	%W    (ADS2+OAD_CMUL)

%OID_NEW(I_CHA_CA,AI_CHA_CA)
	%W    (UI_ADSf)
	%W    (ADS1+OAD_CADD)

%OID_NEW(I_CHB_CA,AI_CHB_CA)
	%W    (UI_ADSf)
	%W    (ADS2+OAD_CADD)

%OID_NEW(I_CHA_UM,AI_CHA_UM)
	%W    (UI_ADSf)
	%W    (ADS1+OAD_UMUL)

%OID_NEW(I_CHB_UM,AI_CHB_UM)
	%W    (UI_ADSf)
	%W    (ADS2+OAD_UMUL)

%OID_NEW(I_CHA_UA,AI_CHA_UA)
	%W    (UI_ADSf)
	%W    (ADS1+OAD_UADD)

%OID_NEW(I_CHB_UA,AI_CHB_UA)
	%W    (UI_ADSf)
	%W    (ADS2+OAD_UADD)

%OID_NEW(I_CHA_ZERO,AI_CHA_ZERO)
	%W    (ADSFL_U)
	DB    0,ZER1_MSK

%OID_NEW(I_CHB_ZERO,AI_CHB_ZERO)
	%W    (ADSFL_U)
	DB    0,ZER2_MSK

%OID_NEW(I_ZERO,AI_ZERO)
	%W    (ADSFL_U)
	DB    0,ZER_MSK

%OID_NEW(I_FIC,AI_FIC)
	%W    (ADS_CNUL)

%OID_NEW(I_LAMPC,AI_LAMPC)
	%W    (UI_INT)
	%W    (0)
	%W    (S_LAMPC_U)

%OID_NEW(I_DACOUT,AI_DACOUT)
	%W    (UI_INT)
	%W    (0)
	%W    (DAC_OFIX)

OID_1IN SET   OID_T

)FI
%IF (%WITH_UL_OI) THEN (

; Vysilane hodnoty

OID_T	SET   0

%OID_NEW(I_STATUS,AI_STATUS)
	%W    (UO_INT)
	%W    (0)
	%W    (G_STATUS)

%OID_NEW(I_MARK_DADR,AI_MARK_DADR)
	%W    (UO_INT)
	%W    (LM_DADR)
	%W    (0)

%OID_NEW(I_MARK_MASK,AI_MARK_MASK)
	%W    (UO_INT)
	%W    (LM_MASK)
	%W    (0)

%OID_NEW(I_AUXUAL,AI_AUXUAL)
	%W    (UO_INT)
	%W    (0)
	%W    (G_AUX)

%OID_NEW(I_DACMUL,AI_DACMUL)
	%W    (UO_ABSf)
	%W    (DAC_MUL)

; ADC mode and filter
%OID_NEW(I_ADCSAMPPER,AI_ADCSAMPPER)
	%W    (UO_INT)
	%W    (C_ADCSAMPPER)
	%W    (0)

%OID_NEW(I_CHA_MODE,AI_CHA_MODE)
	%W    (UO_INT)
	%W    (ADS1+OAD_CTR)
	%W    (0)

%OID_NEW(I_CHB_MODE,AI_CHB_MODE)
	%W    (UO_INT)
	%W    (ADS2+OAD_CTR)
	%W    (0)

%OID_NEW(I_CHA_FILT,AI_CHA_FILT)
	%W    (UO_INT)
	%W    (ADS1+OAD_MAFI)
	%W    (0)

%OID_NEW(I_CHB_FILT,AI_CHB_FILT)
	%W    (UO_INT)
	%W    (ADS2+OAD_MAFI)
	%W    (0)

; Callibration/user mull/add
%OID_NEW(I_CHA_CM,AI_CHA_CM)
	%W    (UO_ABSf)
	%W    (ADS1+OAD_CMUL)

%OID_NEW(I_CHB_CM,AI_CHB_CM)
	%W    (UO_ABSf)
	%W    (ADS2+OAD_CMUL)

%OID_NEW(I_CHA_CA,AI_CHA_CA)
	%W    (UO_ABSf)
	%W    (ADS1+OAD_CADD)

%OID_NEW(I_CHB_CA,AI_CHB_CA)
	%W    (UO_ABSf)
	%W    (ADS2+OAD_CADD)

%OID_NEW(I_CHA_UM,AI_CHA_UM)
	%W    (UO_ABSf)
	%W    (ADS1+OAD_UMUL)

%OID_NEW(I_CHB_UM,AI_CHB_UM)
	%W    (UO_ABSf)
	%W    (ADS2+OAD_UMUL)

%OID_NEW(I_CHA_UA,AI_CHA_UA)
	%W    (UO_ABSf)
	%W    (ADS1+OAD_UADD)

%OID_NEW(I_CHB_UA,AI_CHB_UA)
	%W    (UO_ABSf)
	%W    (ADS2+OAD_UADD)

%OID_NEW(I_ADCAl,AI_ADCAl)
	%W    (UO_INTRl)
	%W    (ADS1+OAD_RAW)

%OID_NEW(I_ADCBl,AI_ADCBl)
	%W    (UO_INTRl)
	%W    (ADS2+OAD_RAW)

%OID_NEW(I_CHA,AI_CHA)
	%W    (UO_ABSf)
	%W    (ADS1+OAD_VAL)

%OID_NEW(I_CHB,AI_CHB)
	%W    (UO_ABSf)
	%W    (ADS2+OAD_VAL)

%OID_NEW(I_CHAi,AI_CHAi)
	%W    (UO_INT)
	%W    (0)
	%W    (G_CHAi_U)

%OID_NEW(I_CHBi,AI_CHBi)
	%W    (UO_INT)
	%W    (0)
	%W    (G_CHBi_U)

OID_1OUT SET  OID_T

)FI

; *******************************************************************
; Komunikace IIC

%IF(%WITH_IIC)THEN(

; Typ v 1. byte IIC zpravy
; zpravy pro terminal
IIC_TEC	EQU   51H	; Rizeni IIC klavesnice a displaye
IIC_TEK	EQU   52H	; Informace o stisnutych klavesach
IIC_TED	EQU   53H	; Zobrazovani dat na display

; Adresa jednotky
IIC_ADR	EQU   10H

RSEG	MDET__X

SLAV_BL EQU   100
IIC_INP:DS    SLAV_BL
IIC_OUT:DS    SLAV_BL
IIC_BUF:DS    SLAV_BL

RSEG	MDET__C

INI_IIC:MOV   S1ADR,#IIC_ADR
	CALL  IIC_PRE		; S1CON 11000000B ; 11000001B
	;MOV   S1CON,#01000000B	; S1CON pro X 24.000 MHz
	MOV   S1CON,#11000000B	; 11000001B pro X 18.432

	%LDR45i(IIC_INP)
	%LDR67i(SL_CMIC)
	MOV   R2,#SLAV_BL OR 080H
	CALL  IIC_SLX
	CLR   A			; Nulovani I2M pridelovace mastera
	MOV   R0,#6
	MOV   DPTR,#I2M_RQP	; a I2M_RQA a I2M_CB
INI_II3:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,INI_II3
	RET

SL_CM_R:JMP   SL_JRET

; Zpracovavani prikazu z IIC pod prerusenim
; registry  R0  .. funkce SJ_TSTA,SJ_TEND,SJ_RSTA,SJ_REND
;           R12 .. ukazatel na data
;           R3  .. pocet byte do konce S_BLEN
;	    S_DP   .. ukazatel na zacatek bufferu
;	    S_RLEN .. pro SJ_REND jiz naplnen poctem prijatych byte

SL_CMIC:CJNE  R0,#SJ_REND,SL_CM_R
	PUSH  DPL
	PUSH  DPH
	MOV   DPL,S_DP		; Bufer s prijmutymi daty
	MOV   DPH,S_DP+1
	MOVX  A,@DPTR		; Prijmuty prikaz
	INC   DPTR
   %IF(%WITH_IICKB)THEN(
	CJNE  A,#IIC_TEK,SL_CM20
	MOV   A,S_RLEN		; Kody stisknutych klaves
	DEC   A			; Delka dat prikazu
	MOV   R3,A
	JZ    SL_CM19
SL_CM15:MOVX  A,@DPTR
	INC   DPTR
	MOV   R4,DPL
	MOV   R5,DPH
	CALL  KB_KPUSH		; Ulozit kod klavesy
	MOV   DPL,R4
	MOV   DPH,R5
	DJNZ  R3,SL_CM15
SL_CM19:JMP   SL_CM_A
   )FI
SL_CM20:
	JMP   SL_CM_N

SL_CM_A:CLR   ICF_SRC
	SETB  AA
SL_CM_N:MOV   R0,#SJ_REND
	POP   DPH
	POP   DPL
	JMP   SL_JRET

)FI

; *******************************************************************
; Pridelovani casu mastera

%IF(%WITH_IIC)THEN(

RSEG	MDET__X

OI2M_LEN SET  0
%STRUCTM(OI2M,NEXT,2)	; Dalsi pozadavek
%STRUCTM(OI2M,FNCP,2)	; Ukazatel na funkci

I2M_RQP:DS    2		; Ukazatel na prvni pozadovanou akci
I2M_RQA:DS    2		; Ukazatel na prave probihajici akci
I2M_CB:	DS    2		; Callback pri ukonceni prenosu

RSEG	MDET__C

; Test ukonceni prenosu
; pri volne sbernici vraci ACC=0
I2M_TBF:CLR   C
	JB    ICF_MER,I2M_TBF1
	JNB   ICF_MRQ,I2M_TBF2
	MOV   A,#1
I2M_RET1:RET
I2M_TBF1:CALL IIC_CER
I2M_TBF2:MOV  DPTR,#I2M_CB	; Konec
	MOVX  A,@DPTR
	MOV   R2,A
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	CLR   A
	MOVX  @DPTR,A
	MOV   A,R2
	ORL   A,R3
	JZ    I2M_RET1		; Je I2M_CB
I2M_TBF4:MOV  DPL,R2
	MOV   DPH,R3
	CLR   A
	JMP   @A+DPTR		; Pri chybe nastaveno CY

; Postupne pracuje podle I2M_RQP
I2M_POOL:CALL I2M_TBF
	JNZ   I2M_RET1
	MOV   DPTR,#I2M_RQA
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R0
	MOV   DPH,A
	ORL   A,R0
	JNZ   I2M_PO1
	MOV   DPTR,#I2M_RQP
I2M_PO1:MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   DPTR,#I2M_RQA
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	ORL   A,R4
	JZ    I2M_RET1
	MOV   DPL,R4
	MOV   DPH,R5
	INC   DPTR
	INC   DPTR
	JMP   xJMPDPP

; Stejne jako I2M_ADD ale nastavi funkci na R67
I2M_ADDF:MOV  DPL,R4
	MOV   DPH,R5
	INC   DPTR
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A

; Prida OI2M v R45 do dotazovaciho cyklu
I2M_ADD:MOV   DPTR,#I2M_RQP
	MOVX  A,@DPTR
	MOV   R6,A
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   DPL,R4
	MOV   DPH,R5
	MOV   A,R6
	MOVX  @DPTR,A		; OI2M_NEXT
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	RET

; Vyjme OI2M v R45 z dotazovaciho cyklu
I2M_REM:MOV   DPTR,#I2M_RQA
	MOVX  A,@DPTR
	XRL   A,R4
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	XRL   A,R5
	ORL   A,R0
	JNZ   I2M_REM2
	CLR   A
	MOV   DPTR,#I2M_RQA
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
I2M_REM2:MOV  DPTR,#I2M_RQP
I2M_REM3:MOV  R2,DPL		; Vyjmuti z linkovaneho listu
	MOV   R3,DPH
	MOVX  A,@DPTR
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
	ORL   A,R6
	JZ    I2M_REM5
	MOV   DPL,R6
	MOV   DPH,R7
	MOV   A,R6
	XRL   A,R4
	JNZ   I2M_REM3
	MOV   A,R7
	XRL   A,R5
	JNZ   I2M_REM3
	MOVX  A,@DPTR
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
	MOV   DPL,R2
	MOV   DPH,R3
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
I2M_REM5:RET

)FI

; *******************************************************************
; Ovladani displeje pres 8577

%IF(%WITH_IICRVO)THEN(

RVLED_A EQU	40H
RVKBD_A EQU	42H
RVDIS_A EQU     74H
RVDIS_L EQU     4

; Definice segmentu
lcda    SET     02h             ;lcd segment a
lcdb    SET     01h             ;lcd segment b
lcdc    SET     04h             ;lcd segment c
lcdd    SET     10h             ;lcd segment d
lcde    SET     40h             ;lcd segment e
lcdf    SET     08h             ;lcd segment f
lcdg    SET     20h             ;lcd segment g
lcdh    SET     80h             ;lcd segment h colon

RSEG    MDET__D

WR_BUF: DS    RVDIS_L+1

RSEG    MDET__C

; Vypsani WR_BUF po IIC na DISPLAY
OUT_BUF:MOV   A,R6
	ANL   A,#0F8H
	MOV   R0,#WR_BUF
	MOV   @R0,A
	MOV   R6,#RVDIS_A
	MOV   R4,#WR_BUF
	MOV   R2,#RVDIS_L+1
	MOV   R3,#0
	JMP   IIC_RQI

;Vystup cisla na LCD display pres prevod LCD_TBL a LCD_POS
;Vstup: R45   vypisovane cislo
;       R7    .. format vystupu cisla
;                  SLLLxADD
;            S   - signed
;            A   - znamenko nebo cislice
;                  jinak znamenko nebo blank
;            LLL - delka vypisu > 0
;            DD  - pocet desetinych mist
;       R6    .. pozice vypisu

iPRTLi: MOV   A,R5
	MOV   C,ACC.7
	MOV   A,R7
	JNB   ACC.7,iPRTLi3
	JC    iPRTLi1
	JB    ACC.2,iPRTLi3
	MOV   A,#lcdBlnk
	JNC   iPRTLi2
iPRTLi1:CALL  NEGi
	MOV   A,#lcdSig
iPRTLi2:CALL  iPRTLc
	MOV   A,R7
	ADD   A,#-10H
	MOV   R7,A
iPRTLi3:MOV   A,R7
	SWAP  A
	ANL   A,#7
	MOV   R1,A
iPRTLi9:DEC   R1
	MOV   A,R1
	RL    A
	CPL   A
	ADD   A,#O10E4i-1+5*2
	MOV   R0,A
	CALL  rLDR23i
iPRTL10:MOV   R0,#-1
iPRTL11:CJNE  R0,#9,iPRTL12
iPRTLiE:MOV   A,#lcd_Err
	CALL  iPRTLc
	MOV   A,R7
	ANL   A,#70H
	ADD   A,#-10H
	MOV   R7,A
	JNZ   iPRTLiE
	RET
iPRTL12:CALL  SUBi
	INC   R0
	JNC   iPRTL11
	CALL  ADDi
	MOV   A,R7
	ANL   A,#3
	DEC   A
	XRL   A,R1
	JZ    iPRTL13
	CLR   C
iPRTL13:MOV   A,R0
	MOV   ACC.7,C
	CALL  iPRTLc
	MOV   A,R7
	CLR   ACC.7
	ADD   A,#-10H
	MOV   R7,A
	ANL   A,#70H
	JNZ   iPRTLi9
	RET

; Rotace vlevo o 4 bity

RL4R45: MOV   A,R4
	SWAP  A
	MOV   R4,A
	ANL   A,#0F0H
	XCH   A,R4
	ANL   A,#00FH
	XCH   A,R5
	SWAP  A
	XCH   A,R4
	XRL   A,R4
	XCH   A,R4
	ANL   A,#0F0H
	XCH   A,R5
	ORL   A,R5
	XCH   A,R5
	XRL   A,R4
	MOV   R4,A
	RET

)FI
%IF(%WITH_IICRVO)THEN(

; Vystup hexa
;Vstup: R45   vypisovane cislo
;       R6    .. pozice vypisu
iPRTLhw:MOV   R7,#4
iPRTLh: MOV   A,R5
	SWAP  A
	ANL   A,#00FH
	CALL  iPRTLc
	CALL  RL4R45
	DJNZ  R7,iPRTLh
	RET

iPRTLc: MOV     R0,#0
	JNB     ACC.7,iPRTLc1
	CLR     ACC.7
	MOV     R0,#lcdH
iPRTLc1:ADD     A,#LCD_TBL-iPRTLck
	MOVC    A,@A+PC
iPRTLck:ORL     A,R0
iPRTLg: MOV     R0,A
	MOV     A,R6
	INC     R6
	ANL     A,#07H
	ADD     A,#LCD_POS-iPRTLcl
	MOVC    A,@A+PC
iPRTLcl:ADD     A,#WR_BUF+1
	XCH     A,R0
	MOV     @R0,A
	RET

lcdBlnk SET     10H
lcdSig  SET     17h
lcd_Err SET     17h ; = '-' dalsi moznost 0Eh = 'E'

LCD_POS:DB      3
	DB      2
	DB      0
	DB      1

LCD_TBL:db      lcda+lcdb+lcdc+lcdd+lcde+lcdf           ;0
	db      lcdb+lcdc                               ;1
	db      lcda+lcdb+lcdg+lcde+lcdd                ;2
	db      lcda+lcdb+lcdg+lcdc+lcdd                ;3
	db      lcdf+lcdg+lcdb+lcdc                     ;4
	db      lcda+lcdf+lcdg+lcdc+lcdd                ;5
	db      lcda+lcdf+lcdg+lcde+lcdd+lcdc           ;6
	db      lcda+lcdb+lcdc                          ;7
	db      lcda+lcdb+lcdc+lcdd+lcde+lcdf+lcdg      ;8
	db      lcda+lcdb+lcdf+lcdg+lcdc+lcdd           ;9
	db      lcda+lcdb+lcdf+lcdg+lcdc+lcde           ;A
	db      lcdc+lcdd+lcde+lcdf+lcdg                ;b
	db      lcda+lcdd+lcde+lcdf                     ;C
	db      lcde+lcdg+lcdd+lcdc+lcdb                ;d
	db      lcda+lcdd+lcde+lcdf+lcdg                ;E
	db      lcda+lcde+lcdf+lcdg                     ;F
	db      0                                       ;blank
	db      lcda                                    ;segment a
	db      lcdb                                    ;segment b
	db      lcdc                                    ;segment c
	db      lcdd                                    ;segment d
	db      lcde                                    ;segment e
	db      lcdf                                    ;segment f
	db      lcdg                                    ;segment g
	db      lcdb+lcdc+lcde+lcdf+lcdg                ;H
	db      lcdd+lcde+lcdf                          ;L
	db      lcda+lcdb+lcdf+lcdg                     ;degree
	db      lcdc+lcdd+lcde+lcdg                     ;lower degree
	db      0                                       ;spare
	db      0                                       ;spare
	db      0                                       ;spare
	db      0                                       ;spare
	db      lcdh                                    ;colon

)FI
%IF(%WITH_IICRVO)THEN(

RVK_TST:
	CALL  IIC_WME		; cekat na ukonceni prenosu
	%LDR45i(1234)
	MOV   R6,#10H
;       CALL  iPRTLhw
	MOV   R7,#0C4H
	CALL  iPRTLi
	MOV   R6,#10H
	CALL  OUT_BUF
	CALL  IIC_WME		; cekat na ukonceni prenosu

	;JNZ
RVK_TST1:
	MOV   A,#055H
RVK_TST2:
	CALL  SCANKEY
	JNZ   RVK_TST9

	MOV   WR_BUF,A
	MOV   R6,#RVLED_A
	MOV   R4,#WR_BUF
	MOV   R2,#1
	MOV   R3,#0
	CALL  IIC_RQI
	CALL  IIC_WME		; cekat na ukonceni prenosu

	MOV   R6,#RVKBD_A
	MOV   R4,#WR_BUF
	MOV   R2,#0
	MOV   R3,#1
	CALL  IIC_RQI
	CALL  IIC_WME		; cekat na ukonceni prenosu
	JNZ   RVK_TST1
	MOV   A,WR_BUF
	CPL   A
	JZ    RVK_TST2

	PUSH  ACC
	MOV   R4,A
	MOV   R5,B
	INC   B
	MOV   R6,#10H
	CALL  iPRTLhw
	MOV   R6,#10H
	CALL  OUT_BUF
	CALL  IIC_WME		; cekat na ukonceni prenosu

	POP   ACC
	JMP   RVK_TST2

RVK_TST9:
	RET

)FI

; *******************************************************************
; Klavesnice na IIC

%IF(%WITH_IICRVO)THEN(

RVKL_BASE EQU 060H
RVKL_UP   EQU 060H	; Klavesnice pro RVO
RVKL_LESS EQU 067H
RVKL_MORE EQU 066H
RVKL_MODE EQU 064H
RVKL_ZERO EQU 061H
RVKL_LAMP EQU 062H
RVKL_MARK EQU 063H
RVKL_12   EQU 065H

BRVL_ABS  EQU 0
BRVL_CHB  EQU 1
BRVL_AUFS EQU 2
BRVL_LAMP EQU 4
BRVL_HEAT EQU 5
BRVL_1    EQU 6
BRVL_2    EQU 7

RSEG    MDET__X

RVK_MLED:DS   4
RVK_MKBD:DS   4
RVK_MDIS:DS   4

RVK_FLG:DS    1		; priznaky klavesnice
RVK_CHG:DS    1		; detekovane zmeny oproti minulemu stavu
RVK_KEY:DS    1		; akceptovany stav klaves
RVK_PUS:DS    1		; prodleva po stisku
RVK_REP:DS    1		; prodleva pri opakovani

RVK_REPC SET  5		; Pocet taktu repeatu
RVK_PUSC SET  20	; Cekani po stisku
RVK_OFFC SET  2		; Delka uvolneni

RSEG    MDET__C

RVK_INI:%LDR45i(RVK_MLED)
	%LDR67i(RVK_FLED)
	CALL  I2M_ADDF
	%LDR45i(RVK_MKBD)
	%LDR67i(RVK_FKBD)
	CALL  I2M_ADDF
	%LDR45i(RVK_MDIS)
	%LDR67i(RVK_FDIS)
	CALL  I2M_ADDF
	MOV   RV_ACT,#LOW RVO_FRSD	; Pocatecni display
	MOV   RV_ACT+1,#HIGH RVO_FRSD
	CLR   A
	MOV   DPTR,#RV_EDREP
	MOVX  @DPTR,A
	MOV   DPTR,#RVK_TIM
	MOVX  @DPTR,A
	MOV   DPTR,#RVK_KEY
	MOVX  @DPTR,A
	MOV   DPTR,#RVK_FLG
	MOVX  @DPTR,A
	MOV   A,#RVK_PUSC
	MOV   DPTR,#RVK_PUS
	MOVX  @DPTR,A
	MOV   A,#RVK_REPC
	MOV   DPTR,#RVK_REP
	MOVX  @DPTR,A
	RET

; Vystup LED

RVK_FLED:
	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_LED
	MOVC  A,@A+DPTR		; LED z modu displaye
	MOV   B,A
	CLR   A			; dalsi pevne led
RVK_FLED1:
    %IF(1)THEN(
	MOV   C,%LVIS_FL
	MOV   B.BRVL_LAMP,C
    )FI
	MOV   A,B
	CPL   A
	MOV   WR_BUF,A
	MOV   R6,#RVLED_A
	MOV   R4,#WR_BUF
	MOV   R2,#1
	MOV   R3,#0
	JMP   IIC_RQI

)FI
%IF(%WITH_IICRVO)THEN(
; Zpracovani stisknutych klaves

RVK_FKBD:MOV  DPTR,#I2M_CB
	MOV   A,#LOW  RVK_CKBD
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH RVK_CKBD
	MOVX  @DPTR,A
	MOV   R6,#RVKBD_A
	MOV   R4,#WR_BUF
	MOV   R2,#0
	MOV   R3,#1
	JMP   IIC_RQI

RVK_CKBD:JNC  RVK_CK1
	RET
RVK_CK1:MOV   A,WR_BUF
	CPL   A
	MOV   R2,A		; Stisknute klavesy
	MOV   DPTR,#RVK_KEY
	MOVX  A,@DPTR
	XRL   A,R2
	MOV   R2,A
	MOV   DPTR,#RVK_CHG
	MOVX  A,@DPTR
	XCH   A,R2
	MOVX  @DPTR,A
	ANL   A,R2		; Zmena
	JZ    RVK_CR1
	MOV   R3,#7
RVK_CK2:RLC   A
	JC    RVK_CK3
	DJNZ  R3,RVK_CK2
RVK_CK3:MOV   DPTR,#RVK_FLG	; Test stisku klavesy
	MOVX  A,@DPTR
	MOV   R2,A
	XRL   A,R3
	ANL   A,#7
	JNZ   RVK_CK4
	MOV   DPTR,#RVK_TIM
	MOVX  A,@DPTR
	JNZ   RVK_CR6
RVK_CK4:MOV   A,R3
	ADD   A,#RVK_CKt-RVK_CKb
	MOVC  A,@A+PC
RVK_CKb:MOV   R2,A
	MOV   DPTR,#RVK_CHG	; Registrace zmeny
	MOVX  A,@DPTR
	XRL   A,R2
	MOVX  @DPTR,A
	MOV   DPTR,#RVK_KEY
	MOVX  A,@DPTR
	XRL   A,R2
	MOVX  @DPTR,A
	ANL   A,R2
	JZ    RVK_CK7
RVK_CK5:MOV   DPTR,#RVK_FLG	; Stisk klavesy
	MOV   A,R3
	ORL   A,#0C0H
	MOVX  @DPTR,A
	MOV   DPTR,#RVK_PUS
	MOVX  A,@DPTR
RVK_CK6:MOV   DPTR,#RVK_TIM	; Cas blokovani
	MOVX  @DPTR,A
	MOV   A,R3
	ADD   A,#RVKL_BASE	; Baze kodu klavesy
	MOV   R2,A
	CALL  KBDBEEP
	SJMP  RVK_CK8
RVK_CK7:MOV   DPTR,#RVK_FLG	; Uvolneni klavesy
	MOV   A,R3
	MOVX  @DPTR,A
	MOV   A,#RVK_OFFC
	MOV   DPTR,#RVK_TIM
	MOVX  @DPTR,A
	MOV   A,R3
	ADD   A,#RVKL_BASE+080H	; Baze uvolnene klavesy
	MOV   R2,A
RVK_CK8:

	MOV   R7,#ET_KEY
	MOV   A,R2
	MOV   R4,A
	MOV   R5,#0
	JMP   EV_POST

RVK_CR1:MOV   DPTR,#RVK_FLG
	MOVX  A,@DPTR
	MOV   R2,A
	JNB   ACC.7,RVK_CR9
	JNB   ACC.6,RVK_CR4
	MOV   DPTR,#RVK_TIM
	MOVX  A,@DPTR
	JNZ   RVK_CR9
	MOV   A,R2		; Repeat
	ANL   A,#07H
	MOV   R3,A
	MOV   DPTR,#RVK_REP
	MOVX  A,@DPTR
	JZ    RVK_CR9
	SJMP  RVK_CK6
RVK_CR4:MOV   A,R2
	SETB  ACC.6
	MOV   R2,A
	MOV   DPTR,#RVK_PUS
	MOVX  A,@DPTR
	SJMP  RVK_CR8
RVK_CR6:MOV   A,R2
	JNB   ACC.6,RVK_CR9
	CLR   ACC.6
	MOV   R2,A
	MOV   A,#RVK_OFFC
RVK_CR8:MOV   DPTR,#RVK_TIM
	MOVX  @DPTR,A
	MOV   A,R2
	MOV   DPTR,#RVK_FLG
	MOVX  @DPTR,A
RVK_CR9:CLR   A
	MOV   R2,A
	RET

RVK_CKt:DB    00000001B
	DB    00000010B
	DB    00000100B
	DB    00001000B
	DB    00010000B
	DB    00100000B
	DB    01000000B
	DB    10000000B

)FI
%IF(%WITH_IICRVO)THEN(
; Numericky display na IIC

ORV_LEN	SET   0
%STRUCTM(ORV,NEXT,2)	; dalsi mod displeje
%STRUCTM(ORV,ALT ,2)	; alternativni mod displeje
%STRUCTM(ORV,DFN ,2)	; zobrazovaci rutina
%STRUCTM(ORV,EFN ,2)	; editacni rutina rutina
%STRUCTM(ORV,LED ,1)	; co ukazat na LED
%STRUCTM(ORV,SFT ,2)	; ukazatel na tabulku klaves
;----------------------   shodne s UI
%STRUCTM(ORV,A_RD,2)	; rutina volana pro nacteni dat
%STRUCTM(ORV,A_WR,2)	; rutina volana pro ulozeni dat
%STRUCTM(ORV,DPSI,2)	; info pro up todate a sit
%STRUCTM(ORV,DP  ,2)	; ukazatel na menena data
;----------------------
%STRUCTM(ORV,F   ,1)	; format cisla
%STRUCTM(ORV,L   ,2)	; spodni limit
%STRUCTM(ORV,H   ,2)	; horni limit

RVK_UNSTC EQU 25*2	; doba pocatecniho zobrazeni unst rezimu
RVK_UNSTE EQU 25*5	; doba pridrzeni pri editaci

RSEG    MDET__D

RV_ACT:	DS    2		; Aktualni popis pro zobrazeni

RSEG    MDET__X

RV_EDREP:DS   1		; Opakovani klavesy => zvetsit zmenu

RSEG    MDET__C

; Nacteni dat pres ORV_A_RD
RV_RD:	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_A_RD
	MOVC  A,@A+DPTR
	PUSH  ACC
	MOV   A,#ORV_A_RD+1
RV_RD1:	MOVC  A,@A+DPTR
	PUSH  ACC
	MOV   R1,#0
	MOV   A,DPL
	ADD   A,#LOW  (ORV_A_RD-OU_A_RD)
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#HIGH (ORV_A_RD-OU_A_RD)
	MOV   DPH,A
	CLR   F0
	RET

; Ulozeni dat pres ORV_A_WR
RV_WR:	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_A_WR
	MOVC  A,@A+DPTR
	PUSH  ACC
	MOV   A,#ORV_A_WR+1
	SJMP  RV_RD1

; Periodicke zobrazovani
RVK_FDIS:
	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_DFN
	MOVC  A,@A+DPTR
	PUSH  ACC
	MOV   A,#ORV_DFN+1
	MOVC  A,@A+DPTR
	PUSH  ACC
	RET

; Casovane zobrazeni cisla integer
RV_DFNiU:
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#RVK_TUNST
	MOVX  A,@DPTR
	POP   DPH
	POP   DPL
	JZ    RVK_ALT
; Zobrazeni cisla integer
RV_DFNi:CALL  RV_RD	; Nacteni dat, pri chybe F0
	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_F
	MOVC  A,@A+DPTR
	MOV   R7,A	; Format, napr 0C4H
	MOV   R6,#10H
	CALL  iPRTLi
	MOV   R6,#10H
	JMP   OUT_BUF

; Nasledujici polozka
RVK_NEXT:
	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_NEXT	; nasledujici polozka
	MOVC  A,@A+DPTR
	MOV   RV_ACT,A
	MOV   A,#ORV_NEXT+1
	MOVC  A,@A+DPTR
	MOV   RV_ACT+1,A
RVK_NEXT1:
	MOV   A,#RVK_UNSTC
RVK_NEXT2:
	MOV   DPTR,#RVK_TUNST	; pro nestabilni rezimy
	MOVX  @DPTR,A
	CLR   A
	MOV   DPTR,#RV_EDREP	; nulovat pocitadlo opakovani
	MOVX  @DPTR,A
RVK_NEXT9:RET

; Alternativni nasledujici polozka
RVK_ALT:MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_ALT	; alternativni nasledujici polozka
	MOVC  A,@A+DPTR
	MOV   RV_ACT,A
	MOV   A,#ORV_ALT+1
	MOVC  A,@A+DPTR
	MOV   RV_ACT+1,A
	MOV   A,#RVK_UNSTE
	SJMP  RVK_NEXT2

; Prechod na ALT display pri editaci
RVK_EFNALT:
	CALL  RVK_ALT
; Editace
RVK_EDIT:
	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_EFN
	MOVC  A,@A+DPTR
	PUSH  ACC
	MOV   A,#ORV_EFN+1
	MOVC  A,@A+DPTR
	PUSH  ACC
	RET

)FI
%IF(%WITH_IICRVO)THEN(

; Editace cisla integer v docasnem rezimu
RV_EFNiU:
	PUSH  DPL
	PUSH  DPH
	MOV   A,#RVK_UNSTE	; podrzeni rezimu
	MOV   DPTR,#RVK_TUNST
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
; Editace cisla integer
RV_EFNi:
	MOV   A,R2
	PUSH  ACC
	CALL  RV_RD	; Nacteni dat, pri chybe F0
	JNB   F0,RV_Ei10
	CLR   A
	MOV   R4,A
	MOV   R5,A
RV_Ei10:POP  ACC
	CJNE  A,#RVKL_MORE,RV_Ei20
	CALL  RV_EPROF
	CALL  ADDi
	MOV   A,#ORV_H
	CALL  RV_ETSTL
	MOV   C,OV
	XRL   A,PSW
	JB    ACC.7,RV_Ei60
	SJMP  RV_Ei50
RV_Ei20:CJNE  A,#RVKL_LESS,RV_Ei80
	CALL  RV_EPROF
	CALL  SUBi
	MOV   A,#ORV_L
	CALL  RV_ETSTL
	MOV   C,OV
	XRL   A,PSW
	JNB   ACC.7,RV_Ei60
RV_Ei50:MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
RV_Ei60:JMP   RV_WR	; Ulozeni dat
RV_Ei80:MOV   DPTR,#RV_EDREP
	CLR   A
	MOVX  @DPTR,A
	RET

; Otestuje R45 proti limitu v ORV_L nebo ORV_H
RV_ETSTL:MOV  DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   R2,A
	MOVC  A,@A+DPTR
	XCH   A,R2
	INC   A
	MOVC  A,@A+DPTR
	MOV   R3,A
	JMP   CMPi

; Profil zrychlovani editace
RV_EPROF:MOV  DPTR,#RV_EDREP
	MOVX  A,@DPTR
	INC   A
	CJNE  A,#8*6,RV_EPR1
	DEC   A
RV_EPR1:MOVX  @DPTR,A
	ANL   A,#NOT 7
	RR    A
	RR    A
	MOV   R2,A
	ADD   A,#RV_EPRt-RV_EPRb1
	MOVC  A,@A+PC
RV_EPRb1:XCH  A,R2
	ADD   A,#RV_EPRt-RV_EPRb2+1
	MOVC  A,@A+PC
RV_EPRb2:MOV  R3,A
	RET

RV_EPRt:%W    (1)
	%W    (10)
	%W    (20)
	%W    (50)
	%W    (100)
	%W    (200)

RVO_FRSD:			; Prvni display pro klavenici
; ----- vypis absorbance
RVO_100:DS    RVO_100+ORV_NEXT-$
	%W    (RVO_110)		; dalsi mod displeje
	DS    RVO_100+ORV_ALT-$
	%W    (0)		; alternativni mod
	DS    RVO_100+ORV_DFN-$
	%W    (RV_DFNi)		; zobrazovaci rutina
	DS    RVO_100+ORV_EFN-$
	%W    (RV_EFNi)		; editacni rutina rutina
	DS    RVO_100+ORV_LED-$
	DB    1 SHL BRVL_ABS	; co ukazat na LED
	DS    RVO_100+ORV_SFT-$
	%W    (0)		; ukazatel na tabulku klaves
	DS    RVO_100+ORV_A_RD-$
	%W    (UR_CHi3)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    RVO_100+ORV_DPSI-$
	%W    (0)		; DPSI
	%W    (ADS1+OAD_VAL)	; DP
	DS    RVO_100+ORV_F-$
	DB     0C7H		; I_F
	%W    (-200)		; I_L
	%W    ( 200)		; I_H

)FI
%IF(%WITH_IICRVO)THEN(
; ----- vypis druheho ADC
RVO_110:DS    RVO_110+ORV_NEXT-$
	%W    (RVO_120)		; dalsi mod displeje
	DS    RVO_110+ORV_ALT-$
	%W    (0)		; alternativni mod
	DS    RVO_110+ORV_DFN-$
	%W    (RV_DFNi)		; zobrazovaci rutina
	DS    RVO_110+ORV_EFN-$
	%W    (RV_EFNi)		; editacni rutina rutina
	DS    RVO_110+ORV_LED-$
	DB    1 SHL BRVL_CHB	; co ukazat na LED
	DS    RVO_110+ORV_SFT-$
	%W    (0)		; ukazatel na tabulku klaves
	DS    RVO_110+ORV_A_RD-$
	%W    (UR_CHi3)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    RVO_110+ORV_DPSI-$
	%W    (0)		; DPSI
	%W    (ADS2+OAD_VAL)	; DP
	DS    RVO_110+ORV_F-$
	DB     0C7H		; I_F
	%W    (-200)		; I_L
	%W    ( 200)		; I_H

; ----- vypis AUFS
RVO_120:DS    RVO_120+ORV_NEXT-$
	%W    (RVO_130)		; dalsi mod displeje
	DS    RVO_120+ORV_ALT-$
	%W    (0)		; alternativni mod
	DS    RVO_120+ORV_DFN-$
	%W    (RV_DFNi)		; zobrazovaci rutina
	DS    RVO_120+ORV_EFN-$
	%W    (RV_EFAUFS)	; editacni rutina rutina
	DS    RVO_120+ORV_LED-$
	DB    1 SHL BRVL_AUFS	; co ukazat na LED
	DS    RVO_120+ORV_SFT-$
	%W    (0)		; ukazatel na tabulku klaves
	DS    RVO_120+ORV_A_RD-$
	%W    (G_DACAUFS)	; A_RD
	%W    (NULL_A)		; A_WR
	DS    RVO_120+ORV_DPSI-$
	%W    (0)		; DPSI
	%W    (0)		; DP
	DS    RVO_120+ORV_F-$
	DB     0C7H		; I_F
	%W    (-200)		; I_L
	%W    ( 200)		; I_H

; ----- zobrazeni statusu
RVO_130:DS    RVO_130+ORV_NEXT-$
	%W    (RVO_100)		; dalsi mod displeje
	DS    RVO_130+ORV_ALT-$
	%W    (0)		; alternativni mod
	DS    RVO_130+ORV_DFN-$
	%W    (RV_DFNi)		; zobrazovaci rutina
	DS    RVO_130+ORV_EFN-$
	%W    (RV_EFNi)		; editacni rutina rutina
	DS    RVO_130+ORV_LED-$
	DB    (1 SHL BRVL_AUFS) OR (1 SHL BRVL_CHB)
				; co ukazat na LED
	DS    RVO_130+ORV_SFT-$
	%W    (0)		; ukazatel na tabulku klaves
	DS    RVO_130+ORV_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    RVO_130+ORV_DPSI-$
	%W    (0)		; DPSI
	%W    (STATUS)		; DP
	DS    RVO_130+ORV_F-$
	DB     0C0H		; I_F
	%W    (-200)		; I_L
	%W    ( 200)		; I_H

)FI

; *******************************************************************
; Prace s pameti EEPROM 8582

%IF(%WITH_IIC)THEN(

EEP_ADR	EQU   0A0H	; Adresa EEPROM na IIC sbernici
C_S1CON EQU   11000000B ; 11000001B ; Pocatecni stav S1CON

EEA_RD	EQU   1		; akce cteni
EEA_WR	EQU   2		; akce zapisu

RSEG	MDET__D

RSEG	MDET__X

EE_CNT:	DS    1		; citac byte pro prenos z/do EEPROM
EE_PTR:	DS    2		; ukazatel do EE_MEM na data
EEP_TAB:DS    2		; popis dat ulozenych v bloku EEPROM

EE_MEM:	DS    100H	; Buffer pameti EEPROM

RSEG	MDET__C

XOR_SUM:CLR   A
XOR_SU1:MOV   R3,A
	MOVX  A,@DPTR
	INC   DPTR
	XRL   A,R3
	INC   A
	DJNZ  R2,XOR_SU1
	RET

; Cteni pameti EEPROM
;	DPTR .. pointer na buffer
;	R2   .. pocet ctenych byte
;	R4   .. adresa, od ktere se cte

EE_RD:	MOV   A,DPL
	MOV   R0,DPH
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A		; cilova adresa
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	MOV   DPTR,#IIC_BUF
	MOV   A,R4		; adresa v pameti EEPROM
	MOVX  @DPTR,A
	MOV   DPTR,#EE_CNT
	MOV   A,R2		; pocet prenasenych byte
	MOVX  @DPTR,A
	CALL  IIC_WME		; cekat na ukonceni prenosu
EE_RD1:	%LDR45i(IIC_BUF)
	MOV   R2,#1
	MOV   R3,#010H
	MOV   R6,#EEP_ADR
	CALL  IIC_RQX		; pozadavek na IIC
	JNZ   EE_RD1
	CALL  IIC_WME		; cekat na ukonceni prenosu
	JNZ   EE_RDR
	MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR		; cilova adresa
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	%LDR23i(IIC_BUF+1)
	%LDR01i(10H)
	CALL  xxMOVE
	MOV   DPTR,#EE_PTR
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   DPTR,#IIC_BUF
	MOVX  A,@DPTR		; nova adresa v EEPROM
	ADD   A,#10H
	MOVX  @DPTR,A
	MOV   DPTR,#EE_CNT
	MOVX  A,@DPTR
	ADD   A,#-10H-1
	INC   A
	MOVX  @DPTR,A
	JC    EE_RD1
	CLR   A
EE_RDR:	RET

; Zapis do pameti EEPROM
;	DPTR .. pointer na buffer
;	R2   .. pocet zapisovanych byte
;	R4   .. adresa, od ktere se zapisuje

EE_WRAO EQU   4

EE_WR:	MOV   A,DPL
	MOV   R0,DPH
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A		; zdrojova adresa
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	MOV   DPTR,#IIC_BUF
	MOV   A,R4		; adresa v pameti EEPROM
	MOVX  @DPTR,A
	MOV   DPTR,#EE_CNT
	MOV   A,R2		; pocet prenasenych byte
	MOVX  @DPTR,A
	CALL  IIC_WME		; cekat na ukonceni prenosu
EE_WR1:	MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR		; zdrojova adresa
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	%LDR45i(IIC_BUF+1)
	%LDR01i(EE_WRAO)
	CALL  xxMOVE
	MOV   DPTR,#EE_PTR
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
EE_WR2:	%LDR45i(IIC_BUF)
	MOV   R2,#EE_WRAO+1
	MOV   R3,#0
	MOV   R6,#EEP_ADR
	CALL  IIC_RQX
	JNZ   EE_WR2
	CALL  IIC_WME
	JNZ   EE_WR2
	MOV   DPTR,#IIC_BUF
	MOVX  A,@DPTR		; adresa v EEPROM
	ADD   A,#EE_WRAO
	MOVX  @DPTR,A
	MOV   DPTR,#EE_CNT
	MOVX  A,@DPTR		; pocitani prenasenych byte
	ADD   A,#-EE_WRAO-1
	INC   A
	MOVX  @DPTR,A
	JC    EE_WR1
EE_WRR:	RET

)FI

%IF(%WITH_IIC)THEN(

; R45 := [EE_PTR++]
EEA_RDi:MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOV   R0,DPH
	MOV   A,DPL
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	RET

; [EE_PTR++] := R45
EEA_WRi:MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   R0,DPH
	MOV   A,DPL
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	RET

; provadi EEA_RD, EEA_WR pro iteger cislo
EEA_Mi:	CJNE  R0,#EEA_RD,EEA_Mi5
	CALL  EEA_RDi
	MOV   DPL,R2
	MOV   DPH,R3
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	RET
EEA_Mi5:CJNE  R0,#EEA_WR,EEA_Mi9
	MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  EEA_WRi
EEA_Mi9:RET

EEA_PRO:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#EE_PTR
	MOV   A,#LOW EE_MEM	; Pocatek dat pro EEPROM do EE_MEM
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH EE_MEM
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
EEA_PR2:MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A		; R23 parametr pro rutinu
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	ORL   A,R4		; R45 volana funkce
	JZ    EEA_Mi9
	PUSH  DPL
	PUSH  DPH
	MOV   A,R0
	PUSH  ACC
	CALL  JMPR45
	POP   ACC
	MOV   R0,A
	POP   DPH
	POP   DPL
	JMP   EEA_PR2

JMPR45:	MOV   A,R4
	PUSH  ACC
	MOV   A,R5
	PUSH  ACC
	RET

; Nastavi EEP_TAB na R23
EEP_PTS:MOV   DPTR,#EEP_TAB
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	RET

; Nastavi
; R2=[EEP_TAB]   .. delka prenasenych dat
; R4=[EEP_TAB+1] .. pocatecni adresa EEPROM
; DPTR=EE_MEM    .. adresa bufferu pameti
EEP_GM:	MOV   DPTR,#EEP_TAB
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R2
	MOV   DPH,A
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   DPTR,#EE_MEM
	RET

; Ulozit data podle tabulky R23
EEP_WRS:CALL  EEP_PTS
%IF(1)THEN(
EEP_WR1:CALL  I2M_TBF		; Uvolneni po I2M_POOL
	JNZ   EEP_WR1
)FI
	MOV   DPTR,#EEP_TAB
	CALL  xMDPDP
	INC   DPTR
	INC   DPTR	; popis akci
	MOV   R0,#EEA_WR
	CALL  EEA_PRO
	CALL  EEP_GM
	DEC   R2
	CALL  XOR_SUM
	MOVX  @DPTR,A		; kontrolni byte
	CALL  EEP_GM
	JMP   EE_WR

; Nacist data podle tabulky R23
EEP_RDS:CALL  EEP_PTS
%IF(1)THEN(
EEP_RD1:CALL  I2M_TBF		; Uvolneni po I2M_POOL
	JNZ   EEP_RD1
)FI
	CALL  EEP_GM
	CALL  EE_RD
	JNZ   EEP_RD9
	CALL  EEP_GM
	DEC   R2
	CALL  XOR_SUM
	MOV   R3,A
	MOVX  A,@DPTR		; kontrolni byte
	XRL   A,R3
	JNZ   EEP_RD9
	MOV   DPTR,#EEP_TAB
	CALL  xMDPDP
	INC   DPTR
	INC   DPTR	; popis akci
	MOV   R0,#EEA_RD
	JMP   EEA_PRO
EEP_RD9:SETB  F0
	RET

)FI

; *******************************************************************
; Ukladani a cteni nastaveni z EEPROM

; provadi EEA_RD, EEA_WR pro jonfiguraci ADC prevodniku OAD (20bytu)
EEA_ADS:CJNE  R0,#EEA_RD,EEA_ADS5
	MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	MOV   DPTR,#EE_PTR
	CALL  xLDR23i
	%LDR01i(OAD_UADD+4)
	CALL  xxMOVE		; R01 x x:[R45++] = x:[R23++]
	MOV   DPTR,#EE_PTR
	CALL  xSVR23i
	RET
EEA_ADS5:CJNE R0,#EEA_WR,EEA_ADS9
	MOV   DPTR,#EE_PTR
	CALL  xLDR45i
	%LDR01i(OAD_UADD+4)
	CALL  xxMOVE		; R01 x x:[R45++] = x:[R23++]
	MOV   DPTR,#EE_PTR
	CALL  xSVR45i
EEA_ADS9:RET


; Tabulka ukladanych parametru
EEC_SER:DB    040H	; pocet byte - musi by delitelny 16
	DB    010H	; pocatecni adresa v EEPROM
    %IF(0)THEN(
	%W    (COM_TYP)
	%W    (EEA_Mi)

	%W    (COM_ADR)
	%W    (EEA_Mi)

	%W    (COM_SPD)
	%W    (EEA_Mi)
    )ELSE(
	%W    (0)
	%W    (EEA_Mi)

	%W    (COM_ADR)
	%W    (EEA_Mi)

	%W    (0)
	%W    (EEA_Mi)
    )FI

	%W    (ADS1)
	%W    (EEA_ADS)

	%W    (ADS2)
	%W    (EEA_ADS)

	%W    (DAC_MUL)
	%W    (EEA_Mi)

	%W    (DAC_MUL+2)
	%W    (EEA_Mi)

	%W    (LM_MASK)
	%W    (EEA_Mi)

	%W    (LM_DADR)
	%W    (EEA_Mi)

	%W    (0)
	%W    (0)


MR_EERD:%LDR23i(EEC_SER)
	JMP   EEP_RDS

MR_EEWR:%LDR23i(EEC_SER)
	JMP   EEP_WRS

; *******************************************************************
; Pomocne vstupy a vystupy

; Vystupy P4.4, P4.5, P4.6, P4.7
; Vstupy  P5.0, P5.1

RSEG	MDET__X

AUX_INO:DS    1

RSEG	MDET__C

; Nastaveni AUX podle R4
; ======================
S_AUX:  MOV   A,R4
S_AUXA:	SWAP  A
	CPL   A
	ANL   A,#0F0H
	ORL   P4,A
	ORL   A,#00FH
	ANL   P4,A
	RET

; Nacte AUX do R4
; ======================
G_AUX:  MOV   A,P4	; Zpetne cteni vystupu
	CPL   A
	ANL   A,#0F0H
G_AUX1:	MOV   R4,A
	MOV   A,P5	; Vstupy P5.0, P5.1
	ANL   A,#3
	ORL   A,R4
	SWAP  A
	MOV   R4,A
    %IF(1)THEN(
	MOV   A,P5
	CPL   A
	MOV   C,ACC.4	; Tlacitko
	MOV   A,R4
	MOV   ACC.6,C
	MOV   R4,A   
    )FI
	MOV   R5,#0
	MOV   A,#1
	RET

; Nastavi ACC pokud doslo ke zmene vstupu
; =======================================
AUX_ICH:
    %IF(0)THEN(
        CLR   A
	CALL  G_AUX1
    )ELSE(
	CALL  G_AUX	; Sledovat i zmeny vystupu
    )FI
	MOV   DPTR,#AUX_INO
	MOVX  A,@DPTR
	XCH   A,R4
	MOVX  @DPTR,A
	XCH   A,R4
	XRL   A,R4
	RET

; *******************************************************************
; Promenne

RSEG	MDET__X

STATUS:	DS    2

; *******************************************************************
; Globalni udalosti

RSEG	MDET__C

; Posle globalni udalost
GLOB_RQ23:MOV A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	MOV   R7,#ET_GLOB
	JMP   EV_POST

; Zpracovani globalnich udalosti
GLOB_DO:MOV   A,R4
	MOV   R7,A
	MOV   A,R5
	MOV   R4,A
	MOV   DPTR,#GLOB_SF1
	JMP   SEL_FNC

; Globalni udalosti

; Tabulka globalnich udalosti

GLOB_SF1:

	DB    0

; *******************************************************************
; Komunikace s uzivatelem

RSEG	MDET__X

UT_UIAD:DS    40
UT_DATA:DS    40

RSEG	MDET__C

UT_INIT:CLR   D4LINE
	SETB  FL_CMAV
	MOV   DPTR,#UI_MV_SX
	MOV   A,#20
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#2
	MOVX  @DPTR,A
	MOV   DPTR,#UT_UIAD
	MOV   UI_AD,DPL
	MOV   UI_AD+1,DPH
	CLR   A
	MOV   DPTR,#EV_BUF
	MOVX  @DPTR,A
	MOV   DPTR,#GR_ACT
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#REF_TIM
	MOVX  @DPTR,A
%IF(%WITH_IICKB)THEN(
	MOV   A,#25
	CALL  WAIT_T
	MOV   R6,#07CH	; Adresa vetsi IIC klavesnice
	CALL  UI_INIHW	; Inicializace hardware
	JB    FL_IICKB,UT_INI5

	MOV   R6,#07AH	; Adresa IIC klavesnice
	CALL  UI_INIHW	; Inicializace hardware
	JNB   FL_IICKB,UT_INI6

UT_INI5:
	MOV   DPTR,#DEVER_IT	; text
	MOV   R6,#1		; 1. radka
	MOV   R7,#DEVER_IE-DEVER_IT ; pocet znaku
	CALL  KB_IICWRLN
	MOV   A,#50
	CALL  WAIT_T

UT_INI6:
)FI
	RET

%IF(%WITH_IICKB)THEN(
DEVER_IT:DB   LCD_CLR,'%VERSION'
	DB    C_LIN2 ,'PiKRON'
DEVER_IE:
)FI

UT_TREF:MOV   DPTR,#REF_TIM
	MOVX  A,@DPTR
	JNZ   UT_TRE1
	MOV   DPTR,#REF_PER
	MOVX  A,@DPTR
	MOV   DPTR,#REF_TIM
	MOVX  @DPTR,A
	MOV   A,#1
	RET
UT_TRE1:CLR   A
	RET

UT:     CALL  UT_INIT
	MOV   R7,#ET_RQGR
	%LDR45i(UT_GR10)
	CALL  EV_POST

UT_ML:  CALL  EV_GET
	JZ    UT_ML50
	CJNE  R7,#ET_GLOB,UT_ML45
	CALL  GLOB_DO
	SJMP  UT_ML
UT_ML45:CALL  EV_DO
	JMP   UT_ML
UT_ML50:JNB   FL_ECRS,UT_ML53
   %IF(%WITH_RS232)THEN(
	CALL  RS_POOL		; Smycka zpracovani prikazu RS232
	JNZ   UT_ML55
   )FI
UT_ML53:
    %IF(%WITH_ULAN)THEN(
	JNB   FL_ECUL,UT_ML55
	CALL  UD_OI
	JB    uLF_INE,UT_ML55
	JB    UDF_RDP,UT_ML57
    )FI
UT_ML55:CALL  UT_TREF
	JZ    UT_ML60
    %IF(%WITH_ULAN)THEN(
	JNB   FL_ECUL,UT_ML57
	CALL  UD_REFR
UT_ML57:CLR   UDF_RDP
    )FI
	SETB  FL_REFR
	SJMP  UT_ML65
UT_ML60:
	CALL  UT_ULED
    %IF(%WITH_IIC)THEN(
	CALL  I2M_POOL
    )FI
UT_ML65:CALL  TEST_SW0
	JZ    UT_ML66
	CALL  ERRCLR_U
	%LDR45i(1)
	CALL  S_LAMPC_U
	MOV   R2,#0
	MOV   R3,#ZER_MSK
	CALL  ADS_FLCH
UT_ML66:JMP   UT_ML

UT_ULED:
;       MOV   DPTR,#IP1_ST+1
;	MOV   R2,#1 SHL LFB_PUMP1
;	CALL  UT_USTS	; CALL  UT_UST1

	CALL  G_STATUS	; Ledka statusu
	MOV   A,R5
	JB    ACC.7,UT_UL12
	ORL   A,R4
	JZ    UT_UL11
	MOV   DPTR,#CAL_PH
	MOVX  A,@DPTR
	JZ    UT_UL10
	MOV   A,#030H
	SJMP  UT_UL11
UT_UL10:MOV   A,#080H
UT_UL11:MOV   PWM0,A
	SJMP  UT_UL13
UT_UL12:JB    LEB_PHA,UT_UL10
	MOV   PWM0,#010H
UT_UL13:
	JMP   LEDWR

UT_USTS:MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
UT_UST1:MOV   A,R5
	JB    ACC.7,UT_UST7
	ORL   A,R4
UT_UST2:JZ    UT_UST5
UT_UST3:MOV   A,R2		; On
	CPL   A
	ANL   LEB_FLG,A
	CPL   A
	ORL   LED_FLG,A
	RET
UT_UST5:MOV   A,R2		; Off
	CPL   A
	ANL   LEB_FLG,A
	ANL   LED_FLG,A
	RET
UT_UST7:MOV   A,R2		; Error
	ORL   LEB_FLG,A
	RET

TST_IICKL:
	MOV   DPTR,#STATUS
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	RET

; ---------------------------------
; Definice klaves

UT_SF1:

UT_SFTN:DB    K_RIGHT
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    K_LEFT
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    K_DOWN
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    K_UP
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

%IF(%WITH_IICRVO)THEN(
RVO_SFT1:
	DB    RVKL_MORE
	DB    RVKL_MORE,0
	%W    (RVK_EDIT)

	DB    RVKL_LESS
	DB    RVKL_LESS,0
	%W    (RVK_EDIT)

	DB    RVKL_MORE OR 80H
	DB    0,0
	%W    (RVK_EDIT)

	DB    RVKL_LESS OR 80H
	DB    0,0
	%W    (RVK_EDIT)

	DB    RVKL_MODE
	%W    (0)
	%W    (RVK_NEXT)

	DB    RVKL_LAMP
	%W    (0)
	%W    (LAMPC_TOGLE)

	DB    RVKL_ZERO
	DB    0,ZER_MSK
	%W    (ADS_FLCH)

	DB    RVKL_MARK
	%W    (0)
	%W    (LAN_MRK)
)FI
%IF(%WITH_IICKB)THEN(
	DB    043H
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    041H
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    042H
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    046H
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    045H
	DB    0,0
	%W    (TST_IICKL)

	DB    047H
	DB    ET_KEY,K_ENTER
	%W    (EV_PO23)
)FI

UT_SF0:	DB    0


; ---------------------------------
; Zakladni display
UT_GR10:DS    UT_GR10+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR10+OGR_BTXT-$
	%W    (UT_GT10)
	DS    UT_GR10+OGR_STXT-$
	%W    (0)
	DS    UT_GR10+OGR_HLP-$
	%W    (0)
	DS    UT_GR10+OGR_SFT-$
	%W    (UT_SF10)
	DS    UT_GR10+OGR_PU-$
	%W    (UT_U1021)
	%W    (UT_U1022)
	%W    (UT_U1041)
	%W    (UT_U1042)
    %IF(%WITH_UL_DY_EEP AND 0) THEN (
	%W    (UT_U1082)
    )FI
	%W    (0)

UT_GT10:DB    'CHA',C_NL
	DB    'CHB',C_NL
	DB    'STAT',C_NL
	DB    'VAL',0

UT_SF10:
	DB    -1
	%W    (UT_SF1)

UT_U1021: ; Chanel A int
	DS    UT_U1021+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1021+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1021+OU_X-$
	DB    6,0,7,1
	DS    UT_U1021+OU_HLP-$
	%W    (0)
	DS    UT_U1021+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1021+OU_A_RD-$
	%W    (G_CHAi_U)	; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1021+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U1021+OU_I_F-$
	DB    84H		; format I_F
	%W    (-8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1022: ; Chanel B int
	DS    UT_U1022+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1022+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1022+OU_X-$
	DB    6,1,7,1
	DS    UT_U1022+OU_HLP-$
	%W    (0)
	DS    UT_U1022+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1022+OU_A_RD-$
	%W    (G_CHBi_U)	; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1022+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (0)		; DP
	DS    UT_U1022+OU_I_F-$
	DB    84H		; format I_F
	%W    (-8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1041: ; Status
	DS    UT_U1041+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1041+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1041+OU_X-$
	DB    6,2,6,1
	DS    UT_U1041+OU_HLP-$
	%W    (0)
	DS    UT_U1041+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1041+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1041+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (STATUS)		; DP
	DS    UT_U1041+OU_I_F-$
	DB    80H		; format I_F
	%W    (-200)		; I_L
	%W    (200)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1042: ; Status
	DS    UT_U1042+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1042+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1042+OU_X-$
	DB    6,3,6,1
	DS    UT_U1042+OU_HLP-$
	%W    (0)
	DS    UT_U1042+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1042+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1042+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (STATUS)		; DP
	DS    UT_U1042+OU_I_F-$
	DB    80H		; format I_F
	%W    (-200)		; I_L
	%W    (200)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

%IF(%WITH_UL_DY_EEP AND 0) THEN (
UT_U1082:
	DS    UT_U1082+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U1082+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1082+OU_X-$
	DB    0,4,13,1
	DS    UT_U1082+OU_HLP-$
	%W    (0)
	DS    UT_U1082+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U1082+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U1082+OU_B_P-$
	%W    (0)		; Servisni nastaveni
	DS    UT_U1082+OU_B_F-$
	%W    (WR_SERNUM1)	; Zapis do EEPROM
	DS    UT_U1082+OU_B_T-$
	DB    'Save SER.NUM.',0
)FI

	END