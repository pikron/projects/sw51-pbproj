#   Project file pro maly detektor
#         (C) Pisoft 2000

mdet.obj  : mdet.asm config.h
	a51 mdet.asm $(par) debug

pb_uihw.obj : pb_uihw.asm config.h
	a51   pb_uihw.asm $(par) debug

codeadr=8800H
#codeadr=0000H
madr=14

mdet.     : mdet.obj pb_uihw.obj ..\pblib\pb.lib
	l51 mdet.obj,pb_uihw.obj,..\pblib\pb.lib code($(codeadr)) xdata(8000H) ramsize(100h) ixref

mdet.hex  : mdet.
	ohs51 mdet

	  : mdet.hex
	#sendhex mdet.hex /p3 /m3 /t2 /g34816 /b19200
	unixcmd -d ul_sendhex -m $(madr) -g 0
	pause
	unixcmd -d ul_sendhex mdet.hex -m $(madr) -g 0x8800
