$NOMOD51
;********************************************************************
;*                        PB_UIHW.ASM                               *
;*                Uzivatelske rozhrani - hardwarove zavisla cast    *
;*                  Stav ke dni 24.04.1997                          *
;*                      (C) Pisoft 1997 Pavel Pisa Praha            *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_REGS)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_UI_DEFS)
$LIST

%DEFINE (WITH_IICKB)(1)
%DEFINE (WITH_KBLED)(1)

%IF(%WITH_IICKB)THEN(
$INCLUDE(%INCH_IIC)
EXTRN	CODE(xxMOVEt)
%IF(%WITH_KBLED)THEN(
EXTRN	DATA(LEB_FLG)
)FI
)FI

EXTRN	CODE(BEEP)

EXTRN	CODE(UI_PV2G,UI_SDSY)
EXTRN	XDATA(UI_CP_T,UI_CP_X,UI_AV_OY)
EXTRN	XDATA(UI_MV_SX,UI_MV_SY)
EXTRN	BIT(FL_DRCP,FL_CMAV,FL_DRAW)

PUBLIC	UI_WR,UI_CLR,UI_GKEY,GOTOXY,GOTO_S,SHOWCP,SHOW_CP,CLR_CP
PUBLIC  UI_BEEP

PUBLIC	K_ENTER,K_DP,K_PM,KV_V_OK
PUBLIC	K_0,K_1,K_2,K_3,K_4,K_5,K_6,K_7,K_8,K_9

PUBLIC	UI_INIHW

%IF(%WITH_IICKB)THEN(
PUBLIC	KB_KPUSH
PUBLIC	FL_DIPR,FL_IICKB
PUBLIC	KB_IICWRLN
)FI

PB_UI_C SEGMENT CODE
PB_UI_B SEGMENT DATA BITADDRESSABLE
PB_UI_X SEGMENT XDATA

%IF(%WITH_IICKB)THEN(

; zpravy pro terminal
IIC_TEC	EQU   51H	; Rizeni IIC klavesnice a displaye
IIC_TEK	EQU   52H	; Informace o stisnutych klavesach
IIC_TED	EQU   53H	; Zobrazovani dat na display

; Jednotlive prikazy IIC_TED
TED_CLR EQU   1		; Smazat display
TED_CP	EQU   2		; Nastavit TE_CP_T,TE_CP_X,TE_CP_Y
TED_BEEP EQU  3		; Pinuti
TED_LED	EQU   4		; Vypsat na led LED BLINK LED_HI

; Jednotlive prikazy IIC_TEC
TEC_CONN EQU  1		; Spojeni klavesnice

RSEG	PB_UI_B
UI_HWFL:DS    1
FL_DIPR	BIT   UI_HWFL.7	; Je lokalni display
FL_IICKB BIT  UI_HWFL.6	; Je IIC display
FKB_CLR	BIT   UI_HWFL.5	; Smazat IIC KB
FKB_CP	BIT   UI_HWFL.4	; Nove nastavit pozici kurzoru
FKB_RF	BIT   UI_HWFL.3	; Refresh IIC KB
FKB_RIP	BIT   UI_HWFL.2	; Refresh prave probiha
FKB_BEEP BIT  UI_HWFL.1	; Vyslat pipnuti

RSEG	PB_UI_X
KB_ADR:	DS    1		; Adresa IIC klavesnice

KB_BEEPLN:DS  1		; Delka pipnuti


KB_BLP:	DS    2		; Aktualni menena radka
KB_BRCN:DS    1		; Pocet byte do konce radky v bufferu
KB_BP:	DS    2		; Pozice vypisu do bufferu IIC
KB_BLRF:DS    1		; Radka, pro kterou probiha refresh
KB_BLRR:DS    1		; Pocet radek jiz refresovanych

; Buffer radek IIC displaye
; kazda radka zabira misto o velikosti 1+UI_MV_SX
; zacina priznakovym bytem

BKB_LRF	EQU   7		; Radka potrebuje byt refresovana
MKB_LRF	EQU   1 SHL BKB_LRF

KB_BUF:	DS    21*6	; Buffer radek

KB_BIIC:DS    32+4	; Buffer pro IIC

; Prijem kodu klaves
KB_KBBL	EQU   20	; Pocet klaves v bufferu klaves

KB_KBUF:DS    2*KB_KBBL	; Buffer nezpracovanych kodu klaves

KB_KIBP:DS    1		; Ukazatel na zacatek prijomaneho bufferu
KB_KIBC:DS    1		; Pocet byte v prijmovem bufferu

KB_KOBP:DS    1		; Ukazatel do cteneho bufferu
KB_KOBC:DS    1		; Pocet zbylych byte v ctenem bufferu

%IF(%WITH_KBLED)THEN(
KB_LEDC:DS    1		; Rizeni led
KB_LEDO:DS    3		; Buffer minulych hodnot LED
)FI

)FI

RSEG	PB_UI_C

; Inicializace hardware
; R6 adresa IIC KBD
UI_INIHW:
    %IF(%WITH_IICKB)THEN(
	CLR   A
	MOV   DPTR,#KB_KIBP	; Nulovani bufferu klavesnice
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#KB_KOBC
	MOVX  @DPTR,A
	ANL   UI_HWFL,#080H
    %IF(%WITH_KBLED)THEN(
	MOV   DPTR,#KB_LEDC
	MOV   R0,#4
	MOV   A,#1
UI_IH05:MOVX  @DPTR,A		; Nulovani LED bufferu
	INC   DPTR
	CLR   A
	DJNZ  R0,UI_IH05
    )FI
	MOV   A,R6
	MOV   DPTR,#KB_ADR	; Adresa IIC KBD
	MOVX  @DPTR,A
	JZ    UI_IH40
	; Pripojeni IIC KBD
	MOV   DPTR,#KB_BRCN
	MOV   A,#31		; Pocet pokusu o spojeni
	MOVX  @DPTR,A
	CALL  IIC_WME
UI_IH10:MOV   DPTR,#KB_BRCN
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	JZ    UI_IH40		; Nepovedlo se
	MOV   DPTR,#KB_BIIC
	MOV   A,#IIC_TEC	; Rizeni klavesnice
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#TEC_CONN	; Pripojit si IIC KB
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,S1ADR		; Kam posilat klavesy
	MOVX  @DPTR,A
	MOV   R2,#3		; Vyslanych byte
	MOV   R3,#0		; Prijmout byte
	%LDR45i(KB_BIIC)
	MOV   DPTR,#KB_ADR
	MOVX  A,@DPTR
	MOV   R6,A		; Adresa IIC klavesnice
	CALL  IIC_RQX
	CALL  IIC_WME
	JB    ACC.2,UI_IH10	; Chyba?

	SETB  FL_IICKB
UI_IH40:MOV   DPTR,#KB_BRCN
	CLR   A
	MOVX  @DPTR,A
    )FI
	RET

%IF(%WITH_IICKB)THEN(
%IF(%WITH_KBLED)THEN(
; Refresh stavu LED
KB_RFLED:
	MOV   DPTR,#KB_LEDC
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,LED_FLG
	MOV   R4,A
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,LEB_FLG
	MOV   R5,A
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,LED_FLH
	MOV   R6,A
	MOVX  @DPTR,A
	MOV   DPTR,#KB_BIIC	; Priprava zpravy
	MOV   A,#IIC_TED
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#TED_LED
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	MOV   R2,#5
	JMP   KB_RF20
)FI

; Refresh IIC displaye
KB_RF:  JBC   FKB_BEEP,KB_RF08
	JBC   FKB_CLR,KB_RF10
	JBC   FKB_CP,KB_RF30
	JBC   FKB_RF,KB_RF40
	JBC   FKB_RIP,KB_RF42
    %IF(%WITH_KBLED)THEN(
	MOV   DPTR,#KB_LEDC
	MOVX  A,@DPTR
	JNZ   KB_RFLED
	INC   DPTR
	MOVX  A,@DPTR
	XRL   A,LED_FLG
	ANL   A,#NOT 080H	; !!!!!!!!! bez pipnuti
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	XRL   A,LEB_FLG
	JNZ   KB_RFLED
	MOV   A,R3
	CPL   A
	ANL   A,R2
	JNZ   KB_RFLED
	INC   DPTR
	MOVX  A,@DPTR
	XRL   A,LED_FLH
	ANL   A,#0C0H		; !!!!!!!!! jen led, ne flagy
	JNZ   KB_RFLED
    )FI
	RET
; Pipnuti
KB_RF08:MOV   DPTR,#KB_BEEPLN
	MOVX  A,@DPTR
	MOV   R7,A
	MOV   DPTR,#KB_BIIC
	MOV   A,#IIC_TED
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#TED_BEEP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	MOV   R2,#3
	JMP   KB_RF20
; Mazani displaye
KB_RF10:MOV   DPTR,#KB_BIIC
	MOV   A,#IIC_TED
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#TED_CLR
	MOVX  @DPTR,A
	MOV   R2,#2
; Vlastni vyslani zpravy
KB_RF20:MOV   R3,#0
	%LDR45i(KB_BIIC)
	MOV   DPTR,#KB_ADR
	MOVX  A,@DPTR
	MOV   R6,A
	JMP   IIC_RQX
; Zmena polohy nebo typu kurzoru
KB_RF30:CALL  DO_CNAV
	MOV   DPTR,#UI_CP_T
	MOVX  A,@DPTR
	MOV   R7,A
	MOV   DPTR,#KB_BIIC
	MOV   A,#IIC_TED
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#TED_CP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   R2,#5
	JMP   KB_RF20
; Refresh displaye
KB_RF40:MOV   DPTR,#KB_BLRR	; Restart refrese
	CLR   A
	MOVX  @DPTR,A
	SETB  FKB_RIP
KB_RF42:MOV   DPTR,#UI_MV_SX	; Beh radek refrese
	MOVX  A,@DPTR
	MOV   R7,A
	INC   A
	MOV   B,A		; Delka radky + priznaky
	INC   DPTR
	MOVX  A,@DPTR		; Pocet radek
	MOV   R1,A
	MOV   DPTR,#KB_BLRR	; Pocet jiz obnovenych radek
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	DEC   A
	XRL   A,R1
	JNZ   KB_RF43
	CLR   FKB_RIP
	RET
KB_RF43:MOV   DPTR,#KB_BLRF	; Radka testovana na refresh
	MOVX  A,@DPTR
	INC   A
	MOV   R6,A
	CLR   C
	SUBB  A,R1
	JC    KB_RF44
	MOV   R6,#0
KB_RF44:MOV   A,R6
	MOVX  @DPTR,A
	MUL   AB
	ADD   A,#LOW  KB_BUF
	MOV   DPL,A
	MOV   A,B
	ADDC  A,#HIGH  KB_BUF
	MOV   DPH,A
	MOVX  A,@DPTR
	JBC   ACC.BKB_LRF,KB_RF46
	RET
KB_RF46:MOVX  @DPTR,A
	INC   DPTR		; Vyslani vlastni radky
KB_IICWRLN:
	MOV   R2,DPL		; DPTR .. text
	MOV   R3,DPH		; R6 .. cislo radky
	MOV   DPTR,#KB_BIIC	; R7 .. pocet znaku
	MOV   A,#IIC_TED	; Obnoveni radky
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6		; Cislo radky
	ORL   A,#80H
	MOVX  @DPTR,A
	INC   DPTR
	MOV   R4,DPL
	MOV   R5,DPH
	MOV   A,R7
	MOV   R0,A
	CALL  xxMOVEt
	MOV   A,R7
	ADD   A,#2		; Delka zpravy
	MOV   R2,A
	JMP   KB_RF20
KB_RF50:RET
)FI

; Smaze cast displeje obsluhovanou user interfacem
UI_CLR:
    %IF(%WITH_IICKB)THEN(
	SETB  FKB_CLR
	JNB   FL_IICKB,UI_CLR6
	MOV   DPTR,#UI_MV_SX
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   DPTR,#KB_BUF
UI_CLR2:CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R4
	MOV   R0,A
	MOV   A,#' '
UI_CLR3:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,UI_CLR3
	DJNZ  R5,UI_CLR2
UI_CLR6:JNB   FL_DIPR,UI_CLR9
    )FI
	MOV   A,#LCD_CLR
	JMP   LCDWCOM
UI_CLR9:RET

; Vypise znak v A na display
; nesmi rusit zadny registr ani DPTR
UI_WR:
    %IF(%WITH_IICKB)THEN(
	JNB   FL_IICKB,UI_WR50
	PUSH  DPL
	PUSH  DPH
	PUSH  B
	MOV   B,A
	MOV   DPTR,#KB_BRCN
	MOVX  A,@DPTR
	JZ    UI_WR49		; Preteceni pozice v bufferu
	DEC   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADD   A,#1		; [++KB_BP] = znak
	MOVX  @DPTR,A
	PUSH  ACC
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#0
	MOVX  @DPTR,A
	POP   DPL
	MOV   DPH,A
	MOVX  A,@DPTR
	XRL   A,B
	JZ    UI_WR49
	MOV   A,B
	MOVX  @DPTR,A
	MOV   DPTR,#KB_BLP
	MOVX  A,@DPTR		; [KB_BLP] |= MKB_LRF
	PUSH  ACC
	INC   DPTR
	MOVX  A,@DPTR
	POP   DPL
	MOV   DPH,A
	MOVX  A,@DPTR
	ORL   A,#MKB_LRF
	MOVX  @DPTR,A
	SETB  FKB_RF
UI_WR49:MOV   A,B
	POP   B
	POP   DPH
	POP   DPL
UI_WR50:JNB   FL_DIPR,UI_WR99
    )FI
	JMP   LCDWR
UI_WR99:RET

%IF(%WITH_IICKB)THEN(
; Ulozi klavesu v ACC do bufferu
; rusi : R0, R2, DPTR
KB_KPUSH:
	JNB   FL_IICKB,KB_KPU2
	MOV   R2,A
	MOV   DPTR,#KB_KIBP
	MOVX  A,@DPTR		; KB_KIBP
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR		; KB_KIBC
	CJNE  A,#KB_KBBL,KB_KPU3
	MOV   A,R2
KB_KPU2:RET
KB_KPU3:INC   A
	MOVX  @DPTR,A		; KB_KIBC
	ADD   A,R0
	ADD   A,#LOW  (KB_KBUF-1)
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH (KB_KBUF-1)
	MOV   DPH,A
	MOV   A,R2
	MOVX  @DPTR,A
	RET

; Pokusi se vybrat jeden kod z bufferu do ACC
; rusi: R2, DPTR
KB_KGET:
	MOV   DPTR,#KB_KOBC
	MOVX  A,@DPTR
	JNZ   KB_KGE5
	MOV   DPTR,#KB_KIBC	; V KBOB nic neni, co je v KBIB ?
	MOVX  A,@DPTR
	JZ    KB_KGE9
	MOV   DPTR,#KB_KIBP	; V KBIB neco je
	MOVX  A,@DPTR
	MOV   R2,A		; R2 = KB_KIBP
	XRL   A,#KB_KBBL	; Prohodit KBOB a KBIB
	MOV   C,EA
	CLR   EA
	MOVX  @DPTR,A		; KB_KIBC ^= KB_KBBL
	INC   DPTR
	MOVX  A,@DPTR		; R3 = KB_KIBC
	MOV   R3,A
	CLR   A
	MOV   EA,C
	MOVX  @DPTR,A		; KB_KIBC = 0
	MOV   DPTR,#KB_KOBP
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3		; Pocet byte v KBOB
KB_KGE5:DEC   A
	MOVX  @DPTR,A
	MOV   DPTR,#KB_KOBP
	MOVX  A,@DPTR		; KB_KOBC
	INC   A
	MOVX  @DPTR,A
	ADD   A,#LOW  (KB_KBUF-1)
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH (KB_KBUF-1)
	MOV   DPH,A
	MOVX  A,@DPTR		; Konecne cteni klavesy
KB_KGE9:RET
)FI

UI_GKEY:
    %IF(%WITH_IICKB)THEN(
	JNB   FL_DIPR,UI_GK10
    )FI
	CALL  SCANKEY
    %IF(%WITH_IICKB)THEN(
	JNZ   UI_GK99
UI_GK10:JNB   FL_IICKB,UI_GK99
	CALL  KB_KGET		; Prijmi kod z bufferu
	JNZ   UI_GK99
	JB    ICF_MRQ,UI_GK85
	CALL  KB_RF		; Svazani displaye
UI_GK85:JNB   ICF_MER,UI_GK89
	CALL  IIC_CER
UI_GK89:CLR   A
    )FI
UI_GK99:RET

; Pipnuti delky v ACC
UI_BEEP:
%IF(%WITH_IICKB)THEN(
	MOV   DPTR,#KB_BEEPLN
	MOVX  @DPTR,A
	SETB  FKB_BEEP
	JNB   FL_DIPR,UI_GK99
)FI
	JMP   BEEP

; Nastavi polohu vypisu na R45 v lokalnich souradnicich,
; je-li treba skryje kurzor
; funkce musi zachovat R4567 vracene UI_PV2G
; ostatni registry a DPTR jsou k dispozici
; vraci A .. 0 je-li bod mimo aktivni oblast

GOTOXY: CALL  CLR_CP
	CALL  UI_PV2G
	JZ    GOTOXY9
GOTOXYG:
    %IF(%WITH_IICKB)THEN(
	CLR   A
	JNB   FL_IICKB,GOTOX09
	PUSH  B
	MOV   DPTR,#UI_MV_SX
	MOVX  A,@DPTR
	INC   A			; Delka radky + priznaky
	MOV   B,R5
	MUL   AB
	MOV   DPTR,#KB_BLP	; Adresa aktualni radky
	ADD   A,#LOW  KB_BUF
	MOVX  @DPTR,A
	INC   DPTR
	XCH   A,B
	ADDC  A,#HIGH KB_BUF
	MOVX  @DPTR,A
	XCH   A,B
	MOV   DPTR,#KB_BP	; Adresa aktualni pozice
	ADD   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	XCH   A,B
	ADDC  A,#0
	MOVX  @DPTR,A
	MOV   A,R6
;	DB    082H	; ANL  C,xx
;GOTOX08:CLR   A
	MOV   DPTR,#KB_BRCN
	MOVX  @DPTR,A
	POP   B
	MOV   A,#1
GOTOX09:
    )FI
GOTOXY1:
    %IF(%WITH_IICKB)THEN(
	JNB   FL_DIPR,GOTOXY9
    )FI
	MOV   A,R5
	ADD   A,#GXY_D4L-GOTOXY2
	MOVC  A,@A+PC
GOTOXY2:
GOTOXY3:ADD   A,R4
	ORL   A,#LCD_HOM
	JMP   LCDWCOM
GOTOXY9:RET

GXY_D4L:DB    0
	DB    40H
	DB    14H
	DB    54H

; Snazi se o posunuti pohledu tak, aby byl cursor viditelny
DO_CNAV:MOV   DPTR,#UI_CP_X
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  UI_PV2G
	JNZ   DO_CNA8
DO_CNA1:JNB   FL_CMAV,DO_CNA9
	JB    F0,DO_CNA9
	MOV   DPTR,#UI_AV_OY
	MOV   A,R5
	JB    ACC.7,DO_CNA3
	MOV   A,R7
	JNB   ACC.7,DO_CNA4
	MOVX  A,@DPTR
	ADD   A,R7
	JB    OV,DO_CNA4
	MOVX  @DPTR,A
	SJMP  DO_CNA4
DO_CNA3:MOVX  A,@DPTR
	CLR   C
	SUBB  A,R5
	JB    OV,DO_CNA4
	MOVX  @DPTR,A
DO_CNA4:MOV   DPTR,#UI_CP_X
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  UI_PV2G
	JZ    DO_CNA8
	SETB  FL_DRAW
DO_CNA8:RET
DO_CNA9:CLR   A
	RET

; Nastavi typ cursoru na R7 a polohu na R45
SHOWCP:	CALL  CLR_CP
	MOV   DPTR,#UI_CP_T
	MOV   A,R7
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
    %IF(%WITH_IICKB)THEN(
	SETB  FKB_CP
    )FI
	RET

; Docasne skryti kurzoru v dobe vypisu na displej
CLR_CP:
    %IF(%WITH_IICKB)THEN(
	JNB   FL_DIPR,CLR_CP9
    )FI
	JNB   FL_DRCP,CLR_CP1
CLR_CP9:RET
CLR_CP1:SETB  FL_DRCP
CLR_CP2:MOV   A,#LCD_DON
	JMP   LCDWCOM

; Provede obnoveni zviditelneni kurzoru
SHOW_CP:
    %IF(%WITH_IICKB)THEN(
	JNB   FL_DIPR,CLR_CP9
    )FI
	CLR   FL_DRCP
	MOV   DPTR,#UI_CP_T
	MOVX  A,@DPTR
	JZ    CLR_CP2
	CALL  DO_CNAV		; V2G a korekce pohledu
	JZ    CLR_CP2
	CALL  GOTOXY1
	MOV   A,#LCD_DON OR LCD_CON
	JMP   LCDWCOM

; Pripravi polohu nasledujiciho vypisu do stavove radky
GOTO_S:	MOV   DPTR,#UI_MV_SY
	MOVX  A,@DPTR
	DEC   A
	MOV   R5,A
	MOV   R4,#0
	CALL  UI_SDSY
	JMP   GOTOXYG

	END