#   Project file pro viceosou regulaci
#         (C) Pisoft 1996

mdet.obj  : mdet.asm config.h
	a51 mdet.asm $(par) debug

pb_uihw.obj : pb_uihw.asm config.h
	a51   pb_uihw.asm $(par) debug

codeadr=8800H
#codeadr=0000H

mdet.     : mdet.obj pb_uihw.obj ..\pblib\pb.lib
	l51 mdet.obj,pb_uihw.obj,..\pblib\pb.lib code($(codeadr)) xdata(8000H) ramsize(100h) ixref

mdet.hex  : mdet.
	ohs51 mdet

	  : mdet.hex
	ul_sendhex -m 3 -g 0
	pause
	ul_sendhex mdet.hex -m 3 -g 0x8800
