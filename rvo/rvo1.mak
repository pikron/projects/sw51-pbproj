#   Project file pro jednoosou regulaci
#         (C) Pisoft 1997

rvo.obj	  : rvo.asm config.h
	a51 rvo.asm $(par) debug

mr_psys.obj : mr_psys.asm config.h
	a51   mr_psys.asm $(par)

pb_pshw.obj : pb_pshw.asm config.h
	a51   pb_pshw.asm $(par)

	  : rvo.obj
	del rvo.

rvo.	  : rvo.obj mr_psys.obj pb_pshw.obj ..\pblib\pb.lib
	l51 rvo.obj,mr_psys.obj,pb_pshw.obj,..\pblib\pb.lib xdata(8000H) ramsize(100h) ixref

rvo.hex	  : rvo.
	ohs51 rvo
	del   rvo.

