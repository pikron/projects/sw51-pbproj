$NOMOD51
;********************************************************************
;*              Multiosa regulace  - MR_PSHW.ASM                    *
;*                    Modul rizeni polohy - casti zavisle na HW     *
;*                  Stav ke dni 10. 8.1997                          *
;*                      (C) Pisoft 1996                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_AL)
$INCLUDE(%INCH_REGS)
$INCLUDE(%INCH_MR_DEFS)
$INCLUDE(%INCH_MR_PDEFS)
$LIST

%DEFINE (USE_MARK_CTI_FL)(1)	; Pouzit zachyty znacky otacky
%DEFINE (PWM_DIR_SLOW)   (0)	; Prodleva ri zmene polarity PWM

EXTRN	NUMBER(REG_LEN,REG_A,REG_N)
EXTRN	BIT(FL_MRCE)
EXTRN	DATA(MR_BAS,MR_FLG,MR_FLGA)
EXTRN	CODE(MR_REG,DO_REGDBG)

PUBLIC	IRC_INIT,IRC_RD,IRC_RDP,IRC_WR,IRC_WRP,HH_MARK
PUBLIC	MR_SENE,MR_SENEP
PUBLIC	DO_REG,VR_REG,VR_REG1,VR_RET

MR____C SEGMENT CODE

; *******************************************************************
; Poloha snimana z T0 a T2

RSEG	MR____C

IRC_INIT:
	SETB  TR0
	ANL   TMOD,#0F0H
	ORL   TMOD,#005H	; counter 0 mod 1
	MOV   TM2CON,#00100011B	; counter 2 from T2, TR2 enabled
%IF (%USE_MARK_CTI_FL) THEN (
	ANL   CTCON,#11110000B	; Zachyt znacky otacky  P1.0 a P1.1
	ORL   CTCON,#00000101B  ; v bitech TM2IR CTI0 a CTI1
)FI
	CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R7,A
	JMP   IRC_WR

; Nacte polohu do OMR_AP (3B) a vypocita OMR_AS (2B)
IRC_RD: MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
IRC_RDP:
	;MOV   A,#OMR_AIR
	;MOVC  A,@A+DPTR

	MOV   R5,TMH2         ; Aktualni pozice motoru T2-T0
	MOV   R4,TML2
	MOV   A,TMH2
	XRL   A,R5
	JZ    IRC_RD2
	MOV   A,R4
	JB    ACC.7,IRC_RD2
	INC   R5
IRC_RD2:MOV   R3,TH0
	MOV   R2,TL0
	MOV   A,TH0
	XRL   A,R3
	JZ    IRC_RD3
	MOV   A,R2
	JB    ACC.7,IRC_RD3
	INC   R3
IRC_RD3:CLR   C
	MOV   A,R4
	SUBB  A,R2
	MOV   R4,A
	MOV   A,R5
	SUBB  A,R3
	MOV   R5,A

	%MR_OFS2DP(OMR_AP)	; OMR_AP
	MOVX  A,@DPTR
	MOV   R2,A
	MOV   A,R4
	MOVX  @DPTR,A
	CLR   C
	SUBB  A,R2
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   A,R5
	MOVX  @DPTR,A
	SUBB  A,R3
	MOV   R3,A
	INC   DPTR
	JNC   IRC_RD4
	CPL   A
IRC_RD4:JNB   ACC.7,IRC_RD6	; Prodlouzeni OMR_AP
	JNC   IRC_RD5
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	SJMP  IRC_RD7
IRC_RD5:MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	SJMP  IRC_RD7
IRC_RD6:MOVX  A,@DPTR
IRC_RD7:MOV   R6,A
	INC   DPTR
	MOV   A,R2		; OMR_AS=R23
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	RET

; Pozadavek na kalibraci polohy na R567
IRC_WR:	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
IRC_WRP:
	;MOV   A,#OMR_AIR
	;MOVC  A,@A+DPTR	; OMR_AIR

	CLR   TR0
	MOV   R3,TMH2
	MOV   R2,TML2
	MOV   A,TMH2
	XRL   A,R3
	JZ    IRC_WR2
	MOV   A,R2
	JB    ACC.7,IRC_WR2
	INC   R3
IRC_WR2:CLR   C
	MOV   A,R2
	SUBB  A,R5
	MOV   TL0,A
	MOV   A,R3
	SUBB  A,R6
	MOV   TH0,A
	SETB  TR0
	RET

; Oznami najeti na znacku  nastavenim ACC != 0, P1.2
HH_MARK:
	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	;MOV   A,#OMR_AIR
	;MOVC  A,@A+DPTR
CF_MAR8:CLR   A
	RET
CF_MAR9:MOV   A,#1
	RET

; *******************************************************************
; Vystup energii na motory



MDIRP   BIT   P4.6
MDIRM   BIT   P4.7

RSEG	MR____C

; Nastavi energii na R45, nesmi menit R1
MR_SENE:MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
MR_SENEP:
	;MOV  A,#OMR_APW
	;MOVC  A,@A+DPTR

S_ENER:	MOV   A,R4
	RLC   A
	MOV   A,R5
	RLC   A
	JZ    S_ENERC
	JC    S_ENER1
%IF(%PWM_DIR_SLOW)THEN(
	JNB   MDIRM,S_ENERC
)ELSE(
	SETB  MDIRM
)FI
	MOV   PWM0,A
	CLR   MDIRP
	RET
S_ENER1:
%IF(%PWM_DIR_SLOW)THEN(
	JNB   MDIRP,S_ENERC
)ELSE(
	SETB  MDIRP
)FI
	MOV   PWM0,A
	CLR   MDIRM
	RET
S_ENERC:SETB  MDIRM
	SETB  MDIRP
;	NOP
;	MOV   PWM0,#0
S_ENER9:RET

; *******************************************************************
; Zakladni smycka multi regulatoru

RSEG	MR____C

DO_REGR:RET

; Prochazi vsechny regulatory az do OMR_FLG=0

DO_REG: JNB   FL_MRCE,DO_REG0
	CLR   FL_MRCE		; smazani chyby pred vyhodnocenim
	CLR   MR_FLG.BMR_ERR
DO_REG0:ANL   MR_FLG,#MMR_ERR
	MOV   MR_BAS,#LOW REG_A
	MOV   MR_BAS+1,#HIGH REG_A
DO_REG1:CLR   F0
	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOVX  A,@DPTR		; OMR_FLG
	JZ    DO_REGR
	MOV   MR_FLGA,A
	JNB   MR_FLGA.BMR_ENI,DO_REG5
	CALL  IRC_RD
DO_REG5:JNB   MR_FLGA.BMR_ENG,DO_REG6
	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,#OMR_VGJ
	JMP   @A+DPTR  		; skok na vektor generatoru
DO_REG6:
VR_REG: JNB   MR_FLGA.BMR_ENR,DO_REG9
	CALL  MR_REG
DO_REG7:
VR_REG1:JB    MR_FLGA.BMR_ERR,VR_REG2
	JNB   F0,DO_REG8
	ORL   MR_FLGA,#MMR_ERR
VR_REG2:CLR   A
	MOV   R5,A
	MOV   R4,A
DO_REG8:CALL  MR_SENE
	JNB   MR_FLGA.BMR_DBG,DO_REG9
	%MR_OFS2DP(OMR_ENE)	; Pro debug ucely ulada energii
	MOV   A,R4
	MOVX  @DPTR,A
	MOV   R6,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   R7,A
	%MR_OFS2DP(OMR_AS)
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  DO_REGDBG
DO_REG9:
VR_RET:	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,MR_FLGA
	ORL   MR_FLG,A
	MOVX  @DPTR,A		; OMR_FLG
	MOV   A,MR_BAS
	ADD   A,#REG_LEN
	MOV   MR_BAS,A
	JNC   DO_REG1
	INC   MR_BAS+1
	JMP   DO_REG1


	END