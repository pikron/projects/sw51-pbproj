#   Project file pro jednoosou regulaci
#         (C) Pisoft 1997

rvo.obj	  : rvo.asm config.h
	a51 rvo.asm $(par) debug

mr_psys.obj : mr_psys.asm config.h
	a51   mr_psys.asm $(par)

pb_pshw.obj : pb_pshw.asm config.h
	a51   pb_pshw.asm $(par)

rvo.	  : rvo.obj mr_psys.obj pb_pshw.obj ..\pblib\pb.lib
	l51 rvo.obj,mr_psys.obj,pb_pshw.obj,..\pblib\pb.lib code(08800H) xdata(8000H) ramsize(100h) ixref

rvo.hex	    : rvo.
	ohs51 rvo

	  : rvo.hex
#	sendhex rvo.hex /p3 /m3 /t2 /g40960 /b16000
#	sendhex rvo.hex /p4 /m3 /t2 /b9600
#	sendhex rvo.hex /p4 /m3 /t2 /g34816 /b9600
#	sendhex rvo.hex /p4 /m3 /t2 /b9600
	unixcmd -d ul_sendhex -m 3 -g 0
	pause
	unixcmd -d ul_sendhex rvo.hex -m 3 -g 0x8800
#	ul_sendhex -m 3 -g 0
#	pause
#	ul_sendhex rvo.hex -m 3 -g 0x8800
