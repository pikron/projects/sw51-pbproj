$NOMOD51
;********************************************************************
;*              Rotacni vakuovy odparovak - RVO.ASM                 *
;*                       Hlavni modul                               *
;*                  Stav ke dni 10.10.1997                          *
;*                      (C) Pisoft 1996                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************
$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_AL)
$INCLUDE(%INCH_ADR)
$INCLUDE(%INCH_REGS)
$INCLUDE(%INCH_MR_DEFS)
$INCLUDE(%INCH_MR_PDEFS)
$INCLUDE(%INCH_MR_PSYS)
$INCLUDE(%INCH_UI)
$LIST

%DEFINE (FOR_BATH)	(0)	; pro vanu
%DEFINE (WITH_TIMER)	(1)	; pro RVO s minutkou
%DEFINE (WITH_TMCCOR)	(1)	; korekce nabehu teploty

%DEFINE (WITH_ULAN)	(1)	; s komunikaci uLAN
%DEFINE (WITH_UL_OI)	(1)	; uLAN object interface
%DEFINE (WITH_UL_DY)	(1)	; s dynamickou adresaci
%DEFINE (WITH_UL_DY_EEP) (1)	; vyrobnim cislem v EEPROM
%DEFINE (WITH_RS232)	(0)	; s komunikaci RS232
%DEFINE (WITH_IIC)	(1)	; s komunikaci IIC
%DEFINE (WITH_IICRVO)	(1)	; s klavesnici pro RVO
%DEFINE (WITH_RAMT)	(0)	; s testem pameti RAM

%IF (NOT %FOR_BATH) THEN (	; Pro RVO
%DEFINE	(MAX_TMC1_RT)	(1800)	; max nastavitelna teplota v 0.1 degC
)ELSE(
%DEFINE	(MAX_TMC1_RT)	(1000)	; max nastavitelna teplota v 0.1 degC
)FI

; Konstanta pro vypocet RPM na motoru (45*4*256.0/1125/60)*
;					*2^(C_RPM2SPD_E)
%IF(1)THEN(
C_RPM2SPD_E	EQU	8	; prepocet pro RVO
C_RPM2SPD	EQU	3*256
%DEFINE (MAX_RSPD)	(200)	; max nastavitelne otacky
;DEFINE (MAX_RSPD)	(250)	; max nastavitelne otacky
)ELSE(%IF(0)THEN(
C_RPM2SPD_E	EQU	15	; prepocet pro otacky na motoru
C_RPM2SPD	EQU	22370
%DEFINE (MAX_RSPD)	(4000)	; max nastavitelne otacky
)ELSE(
C_RPM2SPD_E	EQU	12	; prepocet s krokem 10RPM
C_RPM2SPD	EQU	27962
%DEFINE (MAX_RSPD)	(400)	; max nastavitelne otacky
)FI)FI

%IF (%WITH_RS232) THEN (
$INCLUDE(%INCH_RS232)
$INCLUDE(%INCH_RSOI)
EXTRN   CODE(IHEXLD)
)FI

%IF (%WITH_ULAN) THEN (
;$INCLUDE(%INCH_ULAN)
EXTRN	CODE(uL_FNC,uL_STR)
EXTRN	BIT(uLF_INE)
%IF (%WITH_UL_OI) THEN (
$INCLUDE(%INCH_UL_OI)
)FI
%IF(%WITH_UL_DY) THEN (
EXTRN	CODE(UD_INIT,UD_RQ)
)FI
;$INCLUDE(%INCH_BREAK)
) FI

%IF(%WITH_IIC)THEN(
$INCLUDE(%INCH_IIC)
)FI

%DEFINE (MR_REG_TYPE) (MR_PIDLP); Prednastaveny typ regulatoru
%DEFINE (MR_REG_SEL)    (1)	; Moznost vyberu regulatoru
%DEFINE (REG_COUNT)	(1)	; Pocet pouzitych regulatoru
%DEFINE (TIME_INT_EX)   (1)	; Ktere z preruseni na casovani
				; 1 je pomalejsi, 0 rychlejsi


;BAUDDIV_9600	EQU	6	; pro 11.0592 MHz
BAUDDIV_9600	EQU	10	; pro 18.4321 MHz
;BAUDDIV_9600	EQU	13	; pro 24 MHz

EXTRN	CODE(cxMOVE,xxMOVE,xMDPDP,xJMPDPP,SEL_FNC,ADDATDP)
EXTRN	CODE(SHRls)
EXTRN	CODE(PRINThb,PRINThw,INPUThw)
EXTRN	CODE(MONITOR)
PUBLIC	INPUTc,KBDBEEP,ERRBEEP,RES_STAR

RVO___C SEGMENT CODE
RVO___D SEGMENT DATA
RVO___B SEGMENT DATA BITADDRESSABLE
RVO___X SEGMENT XDATA

%IF(%WITH_UL_DY_EEP) THEN (
PUBLIC	SER_NUM
CSEG AT	8080H
SER_NUM:DS    10H	; Instrument unigue serial number
XSEG AT	8080H
	DS    10H	; XDATA overlay
)FI

RSEG	RVO___B

LEB_FLG:DS    1		; Blikani ledek

HW_FLG: DS    1
ITIM_RF BIT   HW_FLG.7	; Kontrola reentrance preruseni
FL_DIPR BIT   HW_FLG.5	; Pritomnost LCD displaye
FL_25Hz	BIT   HW_FLG.4	; Nastaven pri preruseni
LEB_PHA	BIT   HW_FLG.3	; Pro blikani led
FL_MRMC	BIT   HW_FLG.2	; Povoleni sberu udaju
FL_MRCE	BIT   HW_FLG.1	; Zadost o vynulovani chyby MR_FLG.BMR_ERR
FL_RS232 BIT  HW_FLG.0	; Pouziva se RS232

HW_FLG1: DS   1
FL_RDYR1 BIT  HW_FLG1.7 ; Vysli pouze jedno READY
FL_CONF2 BIT  HW_FLG1.6 ; Prepinani konfigurece 1/2
FL_CONFK BIT  HW_FLG1.5 ; Klavesa konfigurece 1/2
FL_TEMP2 BIT  HW_FLG1.4 ; Jede se pouze na vnitrni cidlo
FL_TLAP  BIT  HW_FLG1.3 ; Jede minutka
FL_ADN_R BIT  HW_FLG1.2 ; Probiha sjizdeni dolu

RSEG	RVO___X

TMP:	DS    16

C_R_PER	EQU   2
REF_PER:DS    1		; Perioda refrese displeje

STATUS:	DS    2

RSEG	RVO___C

RES_STAR:
	MOV   IEN0,#00000000B ; zakaz preruseni
	MOV   IEN1,#00000000B
	MOV   IP0, #00000000B ; priority preruseni
	%WATCHDOG	      ; Nulovani watch-dogu
	MOV   TMOD,#00100101B ; timer 1 mod 2; counter 0 mod 1
	MOV   TCON,#01010101B ; citac 0 a 1 cita ; interapy hranou
	MOV   TM2CON,#00100011B; counter 2 from T2, TR2 enabled
	MOV   SCON,#11011000B ; dva stopbity
	MOV   PCON,#10000000B ; Bd = OSC/12/16/(256-TH1)
	MOV   SP,#80H
	MOV   P1,#0FFH
	MOV   P3,#0FFH
	MOV   P4,#0FFH
	MOV   PWMP,#1         ; PWM frekvence 18 kHz
	MOV   PWM0,#0
	%VECTOR(EXTI%TIME_INT_EX,I_TIME1) ; EXTINT0/1
	SETB  EX%TIME_INT_EX

%IF(1)THEN(
	MOV   DPTR,#8080H
	MOV   R0,#LOW (-8080H)
STRT10:	CLR   A
STRT11:	MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,STRT11
	%WATCHDOG
	MOV   A,DPH
	XRL   A,#HIGH RES_STAR
	JZ    STRT12
	MOV   A,DPH
	CJNE  A,#0F0H,STRT10
STRT12:
)FI

	CALL  I_TIMRI
	CALL  I_TIMRI
	MOV   CINT25,#10
	CLR   A
	MOV   DPTR,#TIMRI
	MOVX  @DPTR,A
	MOV   DPTR,#TIME
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A

	CLR   A
	MOV   HW_FLG,A
	MOV   HW_FLG1,A
	MOV   LEB_FLG,A
	SETB  ITIM_RF

	CLR   A
	MOV   DPTR,#MR_UIAM
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#TPS_RQP
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#TPS_HYS
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#TPS_CFG
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#ADN_TIM
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#ADN_CNT
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#STATUS
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
    %IF(%WITH_TIMER)THEN(
	MOV   DPTR,#TIM_ACT
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#TIM_STP
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
    )FI

	MOV   DPTR,#REG_A+OMR_FLG
	CLR   A
	MOVX  @DPTR,A

	CALL  LCDINST
	CLR   FL_DIPR
	JNZ   RES_ST2		; Test pritomnosti LCD displaye
	SETB  FL_DIPR
	CALL  LEDWR
RES_ST2:

    %IF(NOT %WITH_ULAN)THEN(
	SETB   FL_RS232		; Inicializovat RS 232
    )FI

    %IF(%WITH_IIC)THEN(
	CALL  INI_IIC		; Inicializace IIC komunikace
    )FI

    %IF(%WITH_ULAN)THEN(
	MOV   A,#3
	CALL  I_U_LAN
	CALL  uL_OINI
    )FI

	MOV   DPTR,#REF_PER
	MOV   A,#C_R_PER
	MOVX  @DPTR,A

	JMP   L0

INPUTc:	CALL  SCANKEY
	JZ    INPUTc
	RET

; Pipnuti na klavese klavesnice
;KBDBEEP:JMP   KBDSTDB
KBDBEEP:MOV   A,#2
BEEP:	MOV   DPTR,#BEEPTIM
	MOVX  @DPTR,A
	SETB  %BEEP_FL
	MOV   DPTR,#LED       ; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
	MOV   A,R2
	RET
ERRBEEP:MOV   A,#8
	JMP   BEEP

%IF(%WITH_ULAN)THEN(
I_U_LAN:PUSH  ACC
	CLR   A
	;MOV   A,#BAUDDIV_9600
	MOV   A,#BAUDDIV_9600/2 ; Standardni rychlost 19200
	MOV   R0,#1
	CALL  uL_FNC	; Rychlost
	POP   ACC
	MOV   R0,#2
	CALL  uL_FNC	; Adresa
	MOV   R2,#0
	MOV   R0,#3
	CALL  uL_FNC	; Delka IB OB
	MOV   R2,#0
	MOV   R0,#4
	CALL  uL_FNC	; Rychle bloky
	MOV   R0,#0
	CALL  uL_FNC	; Start
%IF(%WITH_UL_DY) THEN (
	%LDR45i(STATUS)
	MOV   R6,#2
	CALL  UD_INIT
)FI
	RET

%IF(%WITH_UL_DY_EEP) THEN (
	DB    -'U',-'L',-'D',-'Y'
	%W   (0)
	%W   (SER_NUM)
	%W   (WR_SERNUM)
INI_SERNUM:
	MOV   R2,#010H	; pocet prenasenych byte
	MOV   R4,#0F0H	; pocatecni adresa v EEPROM
	MOV   DPTR,#SER_NUM
	CALL  EE_RD
	JZ    INI_SERNUM9
INI_SERNUM7:
	MOV   R2,#8
	MOV   DPTR,#SER_NUM
	MOV   A,#0FFH
INI_SERNUM8:
	MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R2,INI_SERNUM8
INI_SERNUM9:
	RET
WR_SERNUM:
        CLR   EAD
	CLR   ES
	MOV   PSW,#0
	MOV   SP,#80H
	CALL  I_TIMRI
	CALL  I_TIMRI
	CALL  WR_SERNUM1
	MOV   PWM0,#020H
	JMP   $
WR_SERNUM1:
	MOV   PWM0,#0C0H
	MOV   R2,#010H	; pocet prenasenych byte
	MOV   R4,#0F0H	; pocatecni adresa v EEPROM
	MOV   DPTR,#SER_NUM
	CALL  EE_WR
	RET
)FI
)FI

; *******************************************************************
;
; Casove preruseni

PUBLIC  KBDTIMR

RSEG	RVO___D

%IF (%TIME_INT_EX) THEN (
DINT25  EQU   45   ; Delitel EXTINT1 (18.432MHz/2^14) na 25 Hz
)ELSE(
DINT25  EQU   90   ; Delitel EXTINT1 (18.432MHz/2^13) na 25 Hz
)FI
CINT25: DS    1

RSEG	RVO___X

%IF(%WITH_TIMER)THEN(
TIM_MDI:DS    1    ; Deleni na minuty
TIM_ACT:DS    2    ; Aktualni cas v minutach
TIM_STP:DS    2    ; Cas konce odparovani
)FI

ADN_TIM:DS    2	   ; Doba pro sjizdeni
ADN_CNT:DS    2    ; Citac sjizdeni

BEEPTIM:DS    1	   ; Timer delky pipani

N_OF_T  EQU   6    ; Pocet timeru
TIMR1:  DS    1    ; Timery jsou decrementovany
RVK_TIM:
TIMR2:  DS    1    ; s frekvenci 25 Hz
RVK_TUNST:
TIMR_WAIT:
TIMR3:  DS    1
REF_TIM:DS    1	   ; Refresh timr
KBDTIMR:DS    1
TIMRI:  DS    1
TIME:   DS    2    ; Cas v 0.01 min              =====

RSEG	RVO___C

USING   2
I_TIME1:CLR   P4.5	; !!!!!!!!!!!!!!!!
	PUSH  ACC	; Cast s pruchodem 2250 Hz / 1125 Hz
	PUSH  PSW
	CLR   IE%TIME_INT_EX
	MOV   PSW,#AR0
	PUSH  B
	PUSH  DPL
	PUSH  DPH

	CLR   FL_25Hz
	DJNZ  CINT25,I_TIM10
	SETB  FL_25Hz
I_TIM10:
	CALL  DO_REG		; Regulace DC motoru
I_TIM20:
	CALL  ADC_D		; Cteni AD prevodniku

	JNB   FL_25Hz,I_TIMR1	; Konec casti spruchodem 600 Hz
	MOV   CINT25,#DINT25	; Pruchod s frekvenci 25 Hz

	CALL  TMC_REG		; Regulace teploty
	CALL  TPS_INT		; Mereni podtlaku
	CALL  ADC_S		; Spusteni cyklu prevodu ADC
	JB    FL_RS232,I_TIM70
    %IF(%WITH_ULAN)THEN(
	CALL  uL_STR
    )FI
I_TIM70:%WATCHDOG
	MOV   DPTR,#BEEPTIM
	MOVX  A,@DPTR
	JZ    I_TIM80
	DEC   A
	MOVX  @DPTR,A
	JNZ   I_TIM80
	CLR   %BEEP_FL
	MOV   DPTR,#LED       ; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
I_TIM80:MOV   DPTR,#TIMR1
	MOV   B,#N_OF_T-1
I_TIME2:MOVX  A,@DPTR
	JZ    I_TIME3
	DEC   A
	MOVX  @DPTR,A
I_TIME3:INC   DPTR
	DJNZ  B,I_TIME2
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	JNB   ITIM_RF,I_TIMR1
	JB    ACC.7,I_TIME4
I_TIMR1:POP   DPH
	POP   DPL
	POP   B
	POP   PSW
	POP   ACC
	SETB  EA
	SETB  P4.5	; !!!!!!!!!!!!!!!!
I_TIMRI:RETI

I_TIME4:CLR   ITIM_RF	      ; Pruchod 0.6 sec
;	CALL  I_TIMRI
;	MOV   PSW,#AR0        ; Banka 2
	ADD   A,#15
	MOVX  @DPTR,A
    %IF(%WITH_IIC)THEN(
	CALL  IIC_STR	      ; naprava zboreneho IIC
    )FI
	MOV   A,LEB_FLG	      ; blikani led
	JBC   LEB_PHA,I_TIM42
	ORL   LED_FLG,A
	SETB  LEB_PHA
	SJMP  I_TIM43
I_TIM42:CPL   A
	ANL   LED_FLG,A
I_TIM43:

    %IF(%WITH_TIMER)THEN(	; Hlidani minutky
      %IF (NOT %FOR_BATH) THEN (
	MOV   DPTR,#REG_A+OREG_A+OMR_FLG
	MOVX  A,@DPTR		; Stav motoru otaceni
	JNB   ACC.BMR_BSY,I_TIM59
      )ELSE(
	JNB   FL_TLAP,I_TIM59
      )FI
	MOV   DPTR,#TIM_MDI
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	CJNE  A,#100,I_TIM59
	CLR   A			; Cela minuta
	MOVX  @DPTR,A
	MOV   DPTR,#TIM_ACT
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	CJNE  R4,#0,I_TIM53
	INC   A
	MOVX  @DPTR,A
I_TIM53:MOV   R5,A
	MOV   DPTR,#TIM_STP
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	ORL   A,R2
	JZ    I_TIM59		; Neni limit casu
	CLR   C
	MOV   A,R4
	SUBB  A,R2
	MOV   A,R5
	SUBB  A,R3
	JC    I_TIM59
	CALL  STP_ALL		; Zastavit otaceni
	CALL  TMC_OFF		; Vypne topeni
	CLR   FL_TLAP
	CALL  HR_DOWN_TIM
I_TIM59:
    )FI

	JNB   FL_ADN_R,I_TIM60	; Automaticke sjizdeni dolu
	MOV   DPTR,#ADN_CNT
	MOVX  A,@DPTR
	ADD   A,#-1
	MOVX  @DPTR,A
	JC    I_TIM60
	INC   DPTR
	MOVX  A,@DPTR
	ADD   A,#-1
	MOVX  @DPTR,A
	JC    I_TIM60
	CALL  HR_STOP
I_TIM60:

	SETB  ITIM_RF
	JMP   I_TIMR1

WAIT_T:	MOV   DPTR,#TIMR_WAIT
	MOVX  @DPTR,A
WAIT_T1:MOV   DPTR,#TIMR_WAIT
	MOVX  A,@DPTR
	JNZ   WAIT_T1
	RET

; *******************************************************************
; Promenne multiregulatoru

%IF (%MR_REG_SEL) THEN (
  EXTRN	CODE(MR_PIDNLP,MR_PZP,MR_PIDLP)
  MR_REG_ARR:
	%W    (MR_PIDNLP)
	%W    (MR_PIDNLP)
	%W    (MR_PIDLP)
	%W    (MR_PZP)
	%W    (0)
)ELSE(
  EXTRN	CODE(%MR_REG_TYPE)
)FI

RSEG	RVO___B

MR_FLG:	DS    1
MR_FLGA:DS    1

RSEG	RVO___D

MR_BAS:	DS    2		; ukazatel na struktury regulatoru

%STRUCTM(OMR,SCM,2)	; zmena meritka - nasobeni
%STRUCTM(OMR,SCD,2)	; zmena meritka - deleni
%STRUCTM(OMR,RSPD,2)	; rychlost otaceni pro RVO

RSEG	RVO___X

REG_N	EQU   %REG_COUNT
REG_LEN	EQU   OMR_LEN
REG_A:	DS    REG_N*REG_LEN+1

OREG_A	EQU   0
OREG_B	EQU   REG_LEN
OREG_C	EQU   REG_LEN*2

PUBLIC	MR_BAS,MR_FLG,MR_FLGA
PUBLIC	REG_A,REG_LEN,REG_N
PUBLIC	GO_NOTIFY,DO_REGDBG
PUBLIC  FL_MRCE

RSEG	RVO___C

GO_NOTIFY:
    %IF (%WITH_RS232) THEN (
	SETB  RSF_BSYO
    )FI
	JNB   ACC.BMR_DBG,GO_NTF1
	JMP   MRMC_I 		; Start ukladani udaju
GO_NTF1:RET

DO_REGDBG:
	JMP   DO_MRMC

; *******************************************************************
; Nastaveni pro MARS

MR_INIS:CALL  MR_ZER
	%LDR45i (REG_A)
	%LDR23i (STDR_A)
	%LDR01i (STDR_AE-STDR_A)
	CALL  cxMOVE
	MOV   A,#MMR_ENI 		; OR MMR_ENR
	MOV   DPTR,#REG_A+OREG_A+OMR_FLG
	MOVX  @DPTR,A
	RET

STDR_A:	DB    0, 0,0,0, 0,0
	DB    0
	DB    0,0   ; 	(CASH_ZP)	; RP
	DB    0
	DB    0,0,0			; RS
	DB    10,  0,  6,  0, 30,  0	; P I D
	DB    002H,0, 002H,0, 0,060H	; 1 2 ME
	%W    (800)			; MS
	%W    (01)			; MA
	DB    0,0,0,0,0,0,0
	DB    1 ,1			; OMR_AIR, OMR_APW
	%W    (26+256)			; CFG
	DB    2				; JMP
	DW    %MR_REG_TYPE		; vektor na REGULATOR
	DB    2				; JMP
	DW    VR_REG			; skok na GENERATOR
	DS    STDR_A+OREG_A+OMR_SCM-$
	%W    (3*0)			; Meritko, nasobeni
	%W    (2)			; Meritko, deleni
	%W    (10)			; RPM pro RVO
STDR_AE:

TM_GEPT:DB    LCD_CLR,'Kam :',0

TM_GEPR:RET

TM_GEP: MOV   DPTR,#TM_GEPT
	CALL  cPRINT
	MOV   R6,#8
	MOV   R7,#0C0H
	CALL  INPUTi
	JB    F0,TM_GEPR
	MOV   A,R5
	MOV   R6,A
	MOV   A,R4
	MOV   R5,A
	CLR   A
	MOV   R4,A
	MOV   R7,A
	MOV   R1,#0		; motor A
	JMP   GO_GEP

GO_HHT: MOV   R1,#0
	CALL  GO_HH
    %IF(%REG_COUNT GE 2) THEN(
	MOV   R1,#1
	CALL  GO_HH
    )FI
	RET

%IF (%MR_REG_SEL) THEN (

; Nastaveni typu regulatoru pro R1 na typ R4
REG_SEL:MOV   DPTR,#MR_REG_ARR
	INC   R4
REG_SEL2:CALL cLDR23i
	ORL   A,R2
	JZ    REG_SEL9
	DJNZ  R4,REG_SEL2
	CALL  GO_PRPC		; odpojit generator
	JNB   F0,REG_SEL3
	RET
REG_SEL3:ANL  A,#NOT (MMR_ENR OR MMR_BSY)
	MOVX  @DPTR,A
	MOV   A,#OMR_VRJ	; JMP na regulator
	CALL  MR_GPA1
	MOV   C,EA
	CLR   EA
	MOV   A,#2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R2
	MOVX  @DPTR,A
	MOV   EA,C
	CLR   A			; nulova vystupni energie
	MOV   R4,A
	MOV   R5,A
	CALL  MR_GPA1
	CALL  MR_SENEP
	MOV   A,#OMR_FLG	; uvolneni zamku
	CALL  MR_GPA1
	MOVX  A,@DPTR
	ANL   A,#7FH
	MOVX  @DPTR,A
	RET
REG_SEL9:SETB F0
	RET
)FI

; *******************************************************************
; Zaznam deje do pameti ram

RSEG	RVO___X

MRMC_BAS:
MRMC_AP:DS    2		; Pozice kam ukladat
MRMC_EP:DS    2		; Koncova pozice

MRMC_BMB XDATA 08800H	; Pocatek oblasti pro ulozeni dat
MRMC_EMB XDATA 0A000H	; Konec oblasti pro ulozeni dat
MRMC_MAXL SET (MRMC_EMB-MRMC_BMB)/2

RSEG	RVO___C

; Uklada udaje v R4567
DO_MRMC:JNB   FL_MRMC,MRMC_R
	MOV   DPTR,#MRMC_BAS
	MOV   A,#MRMC_AP-MRMC_BAS
	MOVC  A,@A+DPTR
	MOV   R2,A
	ADD   A,#4
	MOV   R0,A
	MOV   A,#MRMC_AP-MRMC_BAS+1
	MOVC  A,@A+DPTR
	MOV   R3,A
	ADDC  A,#0
	MOV   R1,A
	CLR   C
	MOV   A,#MRMC_EP-MRMC_BAS
	MOVC  A,@A+DPTR
	SUBB  A,R0
	MOV   A,#MRMC_EP-MRMC_BAS+1
	MOVC  A,@A+DPTR
	SUBB  A,R1
	JC    MRMC_R1		; Pamet zaplnena
	MOV   A,R0
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A
	MOV   DPL,R2		; Ulozit data
	MOV   DPH,R3
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
MRMC_R:	RET
MRMC_R1:CLR   FL_MRMC
	RET

; Inicializace subsystemu
MRMC_I:	CLR   FL_MRMC
	%LDMXi(MRMC_AP,MRMC_BMB)
	%LDMXi(MRMC_EP,MRMC_EMB)
	SETB  FL_MRMC
	RET

; *******************************************************************
; Generator sumu pripraveneho v bufferu historie

; DPTR = MRMC_AP
; pokud MRMC_AP>=MRMC_EP pak CY=1
CD_GNMRA:MOV  DPTR,#MRMC_AP
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R1,A
	INC   DPTR
	SETB  C
	MOVX  A,@DPTR
	SUBB  A,R0
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,R1
	MOV   DPL,R0
	MOV   DPH,R1
	RET

; inicializace a beh
CI_GNS:	JNB   ACC.BMR_DBG,CD_GNS8
	SETB  MR_FLGA.BMR_BSY	; Busy
	%LDR45i(CD_GNS)
	CALL  CI_VG
	CLR   MR_FLGA.BMR_ENR
CD_GNS:	JNB   FL_MRMC,CD_GNS8
	CALL  CD_GNMRA		; DPTR=MRMC_AP
	JC    CD_GNS8
	MOV   A,#2
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#3
	MOVC  A,@A+DPTR
	MOV   R5,A
	JMP   VR_REG1
CD_GNS8:CLR   MR_FLGA.BMR_BSY
	%LDR45i(VR_REG)
CI_GNS9:CLR   MR_FLGA.BMR_ENR
	CALL  CI_VG
	CLR   A
	MOV   R4,A
	MOV   R5,A
	JMP   VR_REG1

; start
GO_GNS:	CALL  GO_PRPC		; odpojit generator
	JB    F0,GO_GNS1
	JB    ACC.BMR_DBG,GO_GNS3
	ANL   A,#7FH
	MOVX  @DPTR,A
GO_GNS1:RET
GO_GNS3:%LDR45i(CI_GNS)
	JMP   GO_VG

GO_GNSALL:
	MOV   R1,#0
GO_GNSA1:CALL GO_GNS
	INC   R1
	MOV   A,R1
	CJNE  A,#REG_N,GO_GNSA1
	RET

; *******************************************************************
; Generator pripraveneho sumu do regulatoru

; inicializace a beh
CI_GNR:	JNB   ACC.BMR_DBG,CD_GNR8
	SETB  MR_FLGA.BMR_BSY	; Busy
	%LDR45i(CD_GNR)
	CALL  CI_VG
CD_GNR:	JNB   FL_MRMC,CD_GNR8
	CALL  CD_GNMRA		; DPTR=MRMC_AP
	JC    CD_GNR8
	MOV   A,#2
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#3
	MOVC  A,@A+DPTR
	MOV   R5,A              ; R45 rychlost z bufferu
	MOV   R6,#0
	JNB   ACC.7,CD_GNR6	; rozsirit na R456
	DEC   R6
CD_GNR6:%MR_OFS2DP(OMR_RPI)	; OMR_RPI=OMR_RPI+R456
	MOVX  A,@DPTR
	ADD   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R6
	MOVX  @DPTR,A
	%MR_OFS2DP(OMR_RSI)	; OMR_RSI=R45
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	JMP   VR_REG
CD_GNR8:CLR   MR_FLGA.BMR_BSY
	%LDR45i(VR_REG)
CI_GNR9:CALL  CI_VG
	JMP   VR_REG

; start
GO_GNR:	CALL  GO_PRPC		; odpojit generator
	JB    F0,GO_GNR1
	JB    ACC.BMR_DBG,GO_GNR3
	ANL   A,#7FH
	MOVX  @DPTR,A
GO_GNR1:RET
GO_GNR3:JB    ACC.BMR_ENR,GO_GNR7
	CALL  GO_RSFT		; beznarazove pripojeni regulatoru
GO_GNR7:%LDR45i(CI_GNR)
	JMP   GO_VG

GO_GNRALL:
	MOV   R1,#0
GO_GNRA1:CALL GO_GNR
	INC   R1
	MOV   A,R1
	CJNE  A,#REG_N,GO_GNRA1
	RET

; *******************************************************************
; Generator pohybu konstantni rychlosti

CI_GSP:	%LDR45i(CD_GSP)
	CALL  CI_VG
	SETB  MR_FLGA.BMR_BSY
CD_GSP:	JB    MR_FLGA.BMR_ERR,CD_GSP9
	MOV   DPL,MR_BAS
	MOV   DPH,MR_BAS+1
	MOV   A,#OMR_RS
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#OMR_RS+1
	MOVC  A,@A+DPTR
	MOV   R5,A		; R45=OMR_RS
	MOV   A,#OMR_GEP
	MOVC  A,@A+DPTR
	MOV   R2,A
	MOV   A,#OMR_GEP+1
	MOVC  A,@A+DPTR
	MOV   R3,A		; R23=OMR_GEP=SPD
	CALL  CMPi
	JZ    CD_GSP7
	MOV   C,OV
	XRL   A,PSW
	JNB   ACC.7,CD_GSP4
	MOV   A,#OMR_ACC	; Zvysit rychlost
	MOVC  A,@A+DPTR
	ADD   A,R4
	MOV   R4,A
	MOV   A,#OMR_ACC+1
	MOVC  A,@A+DPTR
	ADDC  A,R5
	MOV   R5,A		; R45+=OMR_ACC
	JB    OV,CD_GSP3
	CALL  CMPi
	MOV   C,OV
	XRL   A,PSW
	JB    ACC.7,CD_GSP6
CD_GSP3:MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	SJMP  CD_GSP6
CD_GSP4:SETB  C			; Snizit rychlost
	MOV   A,#OMR_ACC
	MOVC  A,@A+DPTR
	CPL   A
	ADDC  A,R4
	MOV   R4,A
	MOV   A,#OMR_ACC+1
	MOVC  A,@A+DPTR
	CPL   A
	ADDC  A,R5
	MOV   R5,A		; R45-=OMR_ACC
	JB    OV,CD_GSP3
	CALL  CMPi
	MOV   C,OV
	XRL   A,PSW
	JB    ACC.7,CD_GSP3
CD_GSP6:
CD_GSP7:CALL  MR_GPSV	; R4567=R45+OMR_RP , meni R3
	CALL  MR_WRRP	; OMR_RP=R4567
	JMP   VR_REG
CD_GSP9:%MR_OFS2DP(OMR_RS)
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	CLR   MR_FLGA.BMR_BSY
	%LDR45i(VR_REG)
	CALL  CI_VG
	JMP   VR_REG

; Pozadavek na zmenu rychlosti
GO_GSP:	CALL  GO_PRPC		; odpojit generator
	JNB   F0,GO_GSP1
	RET
GO_GSP1:XCH   A,R3
	MOV   A,#OMR_GEP	; pozadovana rychlost
	CALL  MR_GPA1
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
GO_GSP3:MOV   A,R3
	JB    ACC.BMR_ENR,GO_GSP7
	CALL  GO_RSFT		; beznarazove pripojeni regulatoru
GO_GSP7:%LDR45i(CI_GSP)
	JMP   GO_VG

; *******************************************************************
; Jizda nahoru a dolu P1.0, P1.1

HR_UP:	CLR   FL_ADN_R
	SETB  P1.1
	CLR   P1.0
	RET

HR_DOWN:CLR   FL_ADN_R
	SETB  P1.0
	CLR   P1.1
	RET

HR_STOP:CLR   FL_ADN_R
	SETB  P1.0
	SETB  P1.1
	RET

HR_DOWN_TIM:
	MOV   DPTR,#ADN_TIM
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	ORL   A,R4
	JZ    HR_DOWN_T1
	MOV   C,EA
	CLR   EA
	MOV   DPTR,#ADN_CNT
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	CALL  HR_DOWN
	SETB  FL_ADN_R
	MOV   EA,C
HR_DOWN_T1:	
	RET

; *******************************************************************
; System prevodniku

RSEG	RVO___X

ADC0:	DS    2
ADC1:	DS    2
ADC2:	DS    2
ADC3:	DS    2
ADC4:	DS    2
ADC5:	DS    2
ADC6:	DS    2
ADC7:	DS    2

RSEG	RVO___C

ADC_D:	MOV   B,ADCON
	JNB   B.4,ADC_DR
	MOV   A,B
	ANL   A,#7
	RL    A
	ADD   A,#LOW  ADC0
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH ADC0
	MOV   DPH,A
	MOV   A,B
	ANL   A,#0C0H
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,ADCH
	MOVX  @DPTR,A
	MOV   A,B
	ANL   A,#7
	INC   A
	JNB   ACC.3,ADC_S2
	ANL   ADCON,#NOT 10H
ADC_DR:	RET

ADC_S:  CLR   A
	MOV   B,ADCON
	JB    B.3,ADC_DR
ADC_S2:	ANL   A,#07H
	MOV   ADCON,A
	SETB  ACC.3
	MOV   ADCON,A
	RET

; *******************************************************************
; Prevodnik LTC1286

LTA_CL	BIT   P4.0
LTA_DO	BIT   P4.1
LTA_CS1	BIT   P4.4
LTA_CS2	BIT   P4.5

; Nacte 8 bitu z periferie do ACC
LTA8IN:	MOV   A,#1
LTA8IN1:SETB  LTA_CL
	PUSH  ACC
	CLR   A
	MOV   C,LTA_DO
	ADDC  A,#0
	MOV   C,LTA_DO
	ADDC  A,#0
	MOV   C,LTA_DO
	ADDC  A,#-2
	POP   ACC
	CLR   LTA_CL
	RLC   A
	JNC   LTA8IN1
	RET

; Nacte do R45 hodnotu z ADC

LTA_RD:	CLR   LTA_CL
	NOP
	NOP
	CLR   LTA_CS1
	NOP
	MOV   A,#3
	CALL  LTA8IN1		; 3xCL SH a 4xCL DATA
	ANL   A,#0FH
	MOV   R5,A
	CALL  LTA8IN		; 8xCL DATA
	MOV   R4,A
	SETB  LTA_CS1
	SETB  LTA_CL
	RET

; *******************************************************************
; Rizeni teploty

%DEFINE (TMC_INT_GRAD)	(1)	; Gradient vzdy z interniho cidla
%DEFINE (TMC_EMC_IM)	(1)	; se zlepsenym EMC

EXTRN	CODE(MR_PIDTP)

TMC_OUT	BIT   P4.3
TC_ADC1	XDATA ADC2	; Vnejsi cidlo
TC_ADC2	XDATA ADC1	; Vnitrni havarijni cidlo

RSEG	RVO___X

; Hlavni mereni teploty a regulace
TMC1:	DS    OMR_LEN
TMC1_FLG XDATA TMC1+OMR_FLG
TMC1_AT	XDATA TMC1+OMR_AP
TMC1_AG	XDATA TMC1+OMR_AS
TMC1_RT	XDATA TMC1+OMR_RPI
TMC1_EN	XDATA TMC1+OMR_ENE
; Poradi nasledujicich promennych nelze menit ( TMC_FILT )
TMC1_FL:DS    2			; filtr merenych hodnot
TMC1_RD:DS    2			; prima data z prevodniku
TMC1_MC:DS    2			; sklon pro prepocet
TMC1_OC:DS    2			; offset pro prepocet

; Kontrolni mereni teploty
TMC2_AT:DS    2			; Kontrolni mereni teploty
; Poradi nasledujicich promennych nelze menit ( TMC_FILT )
TMC2_FL:DS    2			; filtr merenych hodnot
TMC2_RD:DS    2			; prima data z prevodniku
TMC2_MC:DS    2			; sklon pro prepocet
TMC2_OC:DS    2			; offset pro prepocet

; Promenne pro omezeni rozdilu mezi cidly
TMC12_DL:DS   2			; maximalni pracovni rozdil
; Omezeni rychlosti nabehu
%IF(%WITH_TMCCOR)THEN(
TMCC_CNT:DS   1
TMCC_OT: DS   2			; teplota1 v minule minute
TMCC_GT: DS   2			; gradient v 0.01 deg/min
TMCC_MGT:DS   2			; max pripustny gradient
TMCC_CET:DS   2			; teplota priblizeni
)FI


TPS_CNT:
TMC_PWC:DS    1			; Citani 25Hz do 255
TMC_FDIV:DS   1			; Frequency divission for EMC

RSEG	RVO___C

; Predpoklada ze DPTR ukazuje na TMCx_FL nebo TPS_FL
TMC_FILT:MOVX A,@DPTR	; TMCx_FL
	MOV   R2,A
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A		; R23 = TMCx_FL
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R2		; Ulozeni raw data
	MOVX  @DPTR,A	; TMCx_RD
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	INC   DPTR
	ORL   A,R2		; Kontrola zkratu
	JNZ   TMC_FI2
	SETB  F0
TMC_FI2:MOVX  A,@DPTR	; TMCx_MC
	MOV   R4,A		; R45=R23*TMCx_MC/2^16
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	JNB   ACC.7,TMC_FI5
	CALL  MULi		; Zaporna konstanta TMCx_MC
	MOV   A,R6
	SUBB  A,R2
	MOV   R4,A
	MOV   A,R7
	SUBB  A,R3
	MOV   R5,A
	SJMP  TMC_FI6
TMC_FI5:CALL  MULi		; Kladna konstanta TMCx_MC
	MOV   A,R6
	MOV   R4,A
	MOV   A,R7
	MOV   R5,A
TMC_FI6:
	MOVX  A,@DPTR	; TMCx_OC
	ADD   A,R4
	MOV   R4,A		; R45=R45+TMCx_OC
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOV   R5,A
	JNB   OV,TMC_FI8
	%LDR45i(7FFFH)
	JB    ACC.7,TMC_FI8
	%LDR45i(-7FFFH)
TMC_SKIP:	
TMC_FI8:RET

; Regulace teploty
TMC_REG:
   %IF(0)THEN(	; Pry je to nutne kvuli EMC, regulace pak ale nechodi
	MOV   DPTR,#TMC_FDIV
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	ADD   A,#-2
	JNC   TMC_SKIP
	CLR   A
	MOVX  @DPTR,A
   )FI
	MOV   DPTR,#TMC1_FLG
	MOVX  A,@DPTR
	JNB   ACC.BMR_ENI,TMC_39	; mereni vypnuto
	MOV   R7,A
; Mereni teploty TMC1 z LTC1286 nebo TC_ADC1
	MOV   DPTR,#TMC_PWC
	MOVX  A,@DPTR
    %IF(%TMC_EMC_IM)THEN(
	JNB   ACC.1,TMC_30
    )FI
%IF(0)THEN(
	INC   A
    %IF(%TMC_EMC_IM)THEN(
	ANL   A,#7H			; kazdy sedmy cyklus
    )ELSE(
	ANL   A,#3H			; kazdy ctvrty cyklus
    )FI
	JNZ   TMC_19			; => (64/4) x 12 bit
	CALL  LTA_RD
)ELSE(
	MOV   DPTR,#TC_ADC1
	MOVX  A,@DPTR
	INC   DPTR
	RL    A
	RL    A
	ANL   A,#3
	MOV   R4,A
	MOVX  A,@DPTR
	RL    A
	RL    A
	MOV   R5,A
	ANL   A,#3
	XCH   A,R5
	ANL   A,#NOT 3
	ORL   A,R4
	MOV   R4,A
)FI
	MOV   DPTR,#TMC1_FL		; do TMC1
	MOVX  A,@DPTR
	ADD   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOVX  @DPTR,A
TMC_19:
; Mereni teploty TMC2 z interniho TC_ADC2
TMC_20:	MOV   DPTR,#TC_ADC2
	MOVX  A,@DPTR
	INC   DPTR
	RL    A
	RL    A
	ANL   A,#3
	MOV   R4,A
	MOVX  A,@DPTR
	RL    A
	RL    A
	MOV   R5,A
	ANL   A,#3
	XCH   A,R5
	ANL   A,#NOT 3
	ORL   A,R4
	MOV   R4,A
	MOV   DPTR,#TMC2_FL		; do TMC2
	MOVX  A,@DPTR
	ADD   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOVX  @DPTR,A
; Generovani PWM
TMC_30:	MOV   DPTR,#TMC_PWC
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
    %IF(%TMC_EMC_IM)THEN(
	ANL   A,#7FH			; 7F .. 128x
    )ELSE(
	ANL   A,#3FH			; 3F .. 64x, 1F .. 32x
    )FI
	MOV   R4,A
	MOV   A,R7
	JNB   ACC.BMR_ENR,TMC_38	; energie vypnuta
	MOV   DPTR,#TMC1_EN+1
	MOVX  A,@DPTR
    %IF(0) THEN (                       ; pro chlazeni
	CPL   A
	INC   A
    )FI
	JNB   ACC.7,TMC_33
	SETB  TMC_OUT
	SJMP  TMC_35
TMC_33:
    %IF(0)THEN(
	CLR   C			; pro 32x
	RRC   A
    )FI
    %IF(NOT %TMC_EMC_IM)THEN(
	CLR   C
	RRC   A
	ADDC  A,#0
    )ELSE(
	CJNE  A,#7FH,TMC_34
	INC   A
    )FI
TMC_34:	SETB  C
	SUBB  A,R4
	MOV   TMC_OUT,C
TMC_35:
TMC_38:	MOV   DPTR,#TMC_PWC
	MOVX  A,@DPTR
    %IF(%TMC_EMC_IM)THEN(
	ANL   A,#7FH		; 7F .. 128x
    )ELSE(
	ANL   A,#3FH		; 3F .. 64x, 1F .. 32x
    )FI
	JZ    TMC_R50
TMC_39:	RET
; Nacteni filtru a prepocet pro TMC1 a TMC2
TMC_R50:CLR   F0
	MOV   DPTR,#TMC2_FL
	CALL  TMC_FILT		; vypocet teploty TMC2
    %IF(%TMC_INT_GRAD)THEN(	; Gradient vzdy z interniho cidla
	MOV   DPTR,#TMC2_AT	; TMC2_AT=R45
	CLR   C
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   A,R4
	MOVX  @DPTR,A
	SUBB  A,R3
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   A,R5
	MOVX  @DPTR,A
	SUBB  A,R3
	XCH   A,R2
	MOV   DPTR,#TMC1_AG	; Vypocteny gradient
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R2
	MOVX  @DPTR,A
    )ELSE(			; Gradient z regulacniho cidla
	MOV   DPTR,#TMC2_AT	; TMC2_AT=R45
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
    )FI
	JB    FL_TEMP2,TMC_R54
	MOV   DPTR,#TMC1_FL
	CALL  TMC_FILT		; vypocet teploty TMC1
	MOV   DPTR,#TMC2_AT	; R45=Nova Teplota
	MOVX  A,@DPTR
	SUBB  A,R4
	MOV   R2,A		; R23=TMC2_AT-R45
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,R5
	MOV   R3,A
	JB    OV,TMC_R56	; Rozdil >300 deg
	JB    ACC.7,TMC_R54	; TMC2_AT-TMC1_AT < 0
	MOV   DPTR,#TMC12_DL
	MOVX  A,@DPTR
	SUBB  A,R2
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,R3
	MOV   R3,A		; R23=TMC1_DL-R23
	JNC   TMC_R54		; jeste v mezich
    %IF(NOT %FOR_BATH)THEN(
	ADD   A,#HIGH(25*100)	; Pro RVO ochrana TMC12_DL+25 deg
    )ELSE(
	ADD   A,#HIGH(5*100)	; Pro vanu ochrana TMC12_DL+5 deg
    )FI
	JNC   TMC_R56		; Chyba
%IF(1)THEN(	; Omezeni integracni slozky
	MOV   A,R2
	MOV   B,#45
	MUL   AB
	MOV   R2,A		; R23=45*R23
	MOV   A,B
	XCH   A,R3
	MOV   B,#45
	MUL   AB
	ADD   A,R3
	MOV   R3,A
	JNB   ACC.7,TMC_R52
	MOV   A,B
	ADDC  A,#-(45-1)
	JNZ   TMC_R52
	MOV   DPTR,#TMC1+OMR_FOI
	MOVX  A,@DPTR
	ADD   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R3
	JNB   OV,TMC_R53
TMC_R52:MOV   DPTR,#TMC1+OMR_FOI+1
	MOV   A,#80H
TMC_R53:MOVX  @DPTR,A
)FI
TMC_R54:JNB   F0,TMC_R58
; Chyba v prevodniku
TMC_R56:MOV   DPTR,#TMC1_FLG
	MOVX  A,@DPTR
	JNB   ACC.BMR_ENR,TMC_R58
	CLR   ACC.BMR_ENR
	SETB  ACC.BMR_ERR
	SETB  TMC_OUT		; Vypnout topeni
	MOVX  @DPTR,A
TMC_R58:
; Regulovana teplota v R45
TMC_R60:			; R45 aktualni regulovana teplota
    %IF(%TMC_INT_GRAD)THEN(	; Gradient vzdy z interniho cidla
	MOV   DPTR,#TMC1_AT	; TMC1_AT=R45
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   C,ACC.7
	SUBB  A,R5
	MOVX  @DPTR,A
    )ELSE(			; Gradient z regulacniho cidla
	MOV   DPTR,#TMC1_AT	; TMC1_AT=R45
	CLR   C			; R45=TMC1_AT-old TMC1_AT
	MOVX  A,@DPTR
	XCH   A,R4
	MOVX  @DPTR,A
	SUBB  A,R4
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R5
	MOVX  @DPTR,A
	MOV   R3,#0
	JNB   ACC.7,TMC_R68	; znamenkove rozsireni
	DEC   R3
TMC_R68:SUBB  A,R5
	MOV   R5,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	MOV   DPTR,#TMC1_AG	; Vypocteny gradient
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
    )FI
; Vlastni regulator
TMC_R70:MOV   DPTR,#TMC1+OMR_ERC
	CLR   A
	MOVX  @DPTR,A
	MOV   DPTR,#TMC1	; Volani vlastniho regulatoru
	CALL  MR_PIDTP		; MR_PIDLP, MR_PIDP
	MOV   DPTR,#TMC1_EN
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
%IF(1)THEN(			; pouze kladna hodnota I+ENE
	JNB   ACC.7,TMC_R75	; energie kladna => OK
	MOV   DPTR,#TMC1+OMR_FOI
	MOV   A,#1
	MOVC  A,@A+DPTR
	JNB   ACC.7,TMC_R75	; I slozka kladna => OK
	CLR   C
	MOVX  A,@DPTR		; OMR_FOI-=TMC1_ENE
	SUBB  A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,R5
	MOVX  @DPTR,A
TMC_R75:
)FI
%IF(%WITH_TMCCOR)THEN(
	MOV   DPTR,#TMCC_CNT
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	JNZ   TMC_R89
    %IF(%TMC_EMC_IM)THEN(
	MOV   A,#12		; 60/(128/25)
    )ELSE(
	MOV   A,#24		; 60/(64/25)
    )FI
	MOVX  @DPTR,A
	MOV   DPTR,#TMC1_AT
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CLR   C
	MOV   DPTR,#TMCC_OT
	MOVX  A,@DPTR
	XCH   A,R4
	MOVX  @DPTR,A
	SUBB  A,R4
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R5
	MOVX  @DPTR,A
	SUBB  A,R5
	MOV   R5,A		; R45 = TMC_AT-TMCC_OT
	INC   DPTR
	MOV   A,R4
	MOVX  @DPTR,A	; TMCC_GT
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	JB    ACC.7,TMC_R89	; Pro zaporny TMCC_GT nekorigovat
	MOV   DPTR,#TMCC_MGT
	CALL  xLDR23i
	CALL  SUBi
	JB    ACC.7,TMC_R89	; Pro TMCC_GT < TMCC_MGT nekorigovat
	MOV   DPTR,#TMC1_RT
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	CLR   C
	MOV   DPTR,#TMC1_AT
	MOVX  A,@DPTR
	SUBB  A,R2
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,R3
	MOV   R3,A		; R23=TMC1_AT-TMC1_RT
	JB    OV,TMC_R89
	MOV   DPTR,#TMCC_CET
	MOVX  A,@DPTR
	ADD   A,R2
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R3
	MOV   R3,A		; R23=TMC1_AT-TMC1_RT+TMCC_CET
	JNB   OV,TMC_R83
	CPL   A
TMC_R83:JB    ACC.7,TMC_R89
	JNZ   TMC_R85
	MOV   A,R5
	JNZ   TMC_R85
	MOV   B,R2
	MOV   A,R4
	MUL   AB
	MOV   R4,A
	MOV   R5,B
	JNB   B.7,TMC_R86
TMC_R85:%LDR45i(7FFFH)
TMC_R86:CLR   C
	MOV   DPTR,#TMC1+OMR_FOI
	MOVX  A,@DPTR
	SUBB  A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,R5
	MOVX  @DPTR,A
	JNB   OV,TMC_R89
	MOV   A,#80H
	MOVX  @DPTR,A
TMC_R89:
)FI
	RET

TMC_INI:MOV   DPTR,#TMC1_FLG
	MOV   A,#80H
	MOVX  @DPTR,A
	%LDR45i (TMC1)
	%LDR23i (TMC1PPR)
	%LDR01i (OMR_LEN)
	CALL  cxMOVE
	CLR   A
	MOV   DPTR,#TMC1_FL
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#TMC2_FL
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	%LDMXi (TMC1_OC,120)	; T2
	%LDMXi (TMC1_MC,20705)	; ((T2-T1)*100*10000h)/7FE0h
	%LDMXi (TMC2_OC,120)	; T2
	%LDMXi (TMC2_MC,20705)	; ((T2-T1)*100*10000h)/7FE0h
	%LDMXi (TMC12_DL,1500)	; limit rozdilu T2-T1
%IF(%WITH_TMCCOR)THEN(
	MOV   DPTR,#TMCC_CNT
	MOV   A,#1
	MOVX  @DPTR,A
	%LDMXi (TMCC_MGT,50)	; Max gradient pri priblizeni
	%LDMXi (TMCC_CET,500)	; Rozdil RT-AT pri priblizeni
)FI
; Vypne topeni
TMC_OFF:CLR   FL_TLAP
	MOV   DPTR,#TMC1_FLG
	MOV   A,#MMR_ENI
	MOVX  @DPTR,A
	SETB  TMC_OUT		; Vypnout topeni
	MOV   A,#1
	RET

; Zapne regulaci teploty
TMC_ON: MOV   DPTR,#TC_ADC1
	MOVX  A,@DPTR
	ADD   A,#LOW  (-0FFC0H)
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#HIGH (-0FFC0H)
	MOV   FL_TEMP2,C		; Nepripojene vnejsi cidlo
	MOV   DPTR,#TMC1_FLG
	CLR   EA
	MOVX  A,@DPTR
	JB    ACC.BMR_ENR,TMC_ON5
	MOV   DPTR,#TMC1+OMR_FOI	; Nulovani slozky I
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
TMC_ON5:MOV   DPTR,#TMC1_FLG
	MOV   A,#MMR_ENI OR MMR_ENR
	MOVX  @DPTR,A
	SETB  EA
	RET

TMC_ONOFF:
	MOV   DPTR,#TMC1_FLG
	MOVX  A,@DPTR
	JB    ACC.BMR_ENR,TMC_OFF
	JB    ACC.BMR_ERR,TMC_OFF
	SJMP  TMC_ON

G_TMC_ST:MOV  DPTR,#TMC1_FLG
	MOVX  A,@DPTR
	JNB   ACC.BMR_ERR,TMC_ST1
	%LDR45i(0FFF1H)
	RET
TMC_ST1:MOV   R5,#0
	JNB   ACC.BMR_ENR,TMC_ST3
	MOV   A,#OMR_ENE+1
	MOVC  A,@A+DPTR
	MOV   R4,#1
	JZ    TMC_ST2
	MOV   R4,#2
	JNB   ACC.7,TMC_ST2
	MOV   R4,#3
TMC_ST2:RET
TMC_ST3:MOV   R4,#0
	RET

TMC1PPR:DB    0, 0,0,0, 0,0
	DB    0
	DB    0,0   			; RP
	DB    0
	DB    0,0,0			; RS
	DB    004H,0, 003H,0, 03H,0	; P I D
	DB    000H,0, 000H,0, 0,07FH	; 1 2 ME
	DB    080H,0, 010H,0		; MS MA
	DB    0,0,0,0,0,0,0

UR_TEMP:MOV   A,#OU_DP
	CALL  ADDATDP
G_TEMP:	%LDR23i(10)
	CALL  xMDPDP
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   EA,C
	JB    ACC.7,G_TEMP1
	CALL  DIVi	; R45=R45/R23
	MOV   A,R4
	ADDC  A,#0
	MOV   R4,A
	MOV   A,R5
	ADDC  A,#0
	MOV   R5,A
	ORL   A,#1
	RET
G_TEMP1:CALL  NEGi
	CALL  DIVi	; R45=R45/R23
	CLR   A
	SUBB  A,R4
	MOV   R4,A
	CLR   A
	SUBB  A,R5
	MOV   R5,A
	ORL   A,#1
	RET

UW_TEMP:MOV   A,#OU_DPSI
	CALL  ADDATDP
S_TEMP:	%LDR23i(10)
	CALL  MULsi
	CALL  xMDPDP
	MOV   C,EA
	CLR   EA
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   EA,C
	ORL   A,#1
	RET

; *******************************************************************
; Mereni podtlaku

RSEG	RVO___X

TPS_OUT BIT   P4.0		; Rizeni vyvevy

TPS_ADC	XDATA ADC4		; Prevodnik tenzometru

TPS_AP:	DS    2			; Aktualni hodnota podtlaku
; Poradi nasledujicich promennych nelze menit ( TMC_FILT )
TPS_FL:	DS    2			; filtr merenych hodnot
TPS_RD:	DS    2			; prima data z prevodniku
TPS_MC:	DS    2			; sklon pro prepocet
TPS_OC:	DS    2			; offset pro prepocet

; Pro rizeni vyvevy
TPS_RQP:DS    2			; pozadovana hodnota
TPS_HYS:DS    2			; hystereze rizeni podtlaku

TPS_CFG:DS    2			; konfigurace rizeni vyvey
MTPS_ABSP  EQU   1		; pouziva se absolutni tlak
MTPS_NEGRQ EQU   2		; nastav zaporny vstup

RSEG	RVO___C

TPS_INT:MOV   DPTR,#TPS_ADC
	MOVX  A,@DPTR
	INC   DPTR
	RL    A
	RL    A
	ANL   A,#3
	MOV   R4,A
	MOVX  A,@DPTR
	RL    A
	RL    A
	MOV   R5,A
	ANL   A,#3
	XCH   A,R5
	ANL   A,#NOT 3
	ORL   A,R4
	MOV   R4,A
	MOV   DPTR,#TPS_FL
	MOVX  A,@DPTR
	ADD   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOVX  @DPTR,A

	MOV   DPTR,#TPS_CNT	; inkrementuje v TPS_CNT
	MOVX  A,@DPTR
	ANL   A,#3FH
	CJNE  A,#2,TPS_IN9
	MOV   DPTR,#TPS_FL
	CALL  TMC_FILT
	MOV   DPTR,#TPS_AP
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
				; rizeni vyvevy
    %IF(0)THEN(	; Podminit otacenim
	MOV   DPTR,#REG_A+OREG_A+OMR_FLG
	MOVX  A,@DPTR
	ANL   A,#MMR_BSY
	JZ    TPS_IN8
    )FI
    %IF(1)THEN( ; Podminit teplotou
	MOV   DPTR,#TMC1_FLG
	MOVX  A,@DPTR
	ANL   A,#MMR_ENR
	JZ    TPS_IN8
    )FI
	CLR   C
	MOV   DPTR,#TPS_RQP
	MOVX  A,@DPTR		; R45=TPS_RQP-R45
	SUBB  A,R4
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,R5
	MOV   R5,A
    %IF(1)THEN(
	MOV   DPTR,#TPS_CFG
	MOVX  A,@DPTR
	ANL   A,#MTPS_ABSP
	JZ    TPS_IN6
	CALL  NEGi
TPS_IN6:MOV   A,R5
    )FI
	JB    ACC.7,TPS_IN8
	MOV   DPTR,#TPS_HYS	; TPS_HYS-R45
	CLR   C
	MOVX  A,@DPTR
	SUBB  A,R4
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,R5
	JNB   ACC.7,TPS_IN9
	CLR   TPS_OUT
	SJMP  TPS_IN9
TPS_IN8:SETB  TPS_OUT
TPS_IN9:RET

UW_TPS_RQ:
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#TPS_CFG
	MOVX  A,@DPTR
	POP   DPH
	POP   DPL
	ANL   A,#MTPS_NEGRQ
	JZ    UW_TPS_RQ5
	MOV   A,R5
	JNB   ACC.7,UW_TPS_RQ6
	SJMP  UW_TPS_RQ7
UW_TPS_RQ5:
	MOV   A,R5
	JNB   ACC.7,UW_TPS_RQ7
UW_TPS_RQ6:
	CLR   A
	MOV   R4,A
	MOV   R5,A
	SETB  F0
UW_TPS_RQ7:
	JMP   UW_TEMP

; *******************************************************************
; Komunikace IIC

%IF(%WITH_IIC)THEN(

; Typ v 1. byte IIC zpravy
; zpravy pro motory
IIC_CMD	EQU   40H	; Vnejsi prikazy
IIC_CMM	EQU   41H	; Prikaz pro motor
IIC_STM	EQU   42H	; Status motoru
; zpravy pro terminal
IIC_TEC	EQU   51H	; Rizeni IIC klavesnice a displaye
IIC_TEK	EQU   52H	; Informace o stisnutych klavesach
IIC_TED	EQU   53H	; Zobrazovani dat na display

; Adresa jednotky
IIC_ADR	EQU   10H

RSEG	RVO___X

SLAV_BL EQU   100
IIC_INP:DS    SLAV_BL
IIC_OUT:DS    SLAV_BL
IIC_BUF:DS    SLAV_BL

RSEG	RVO___C

INI_IIC:MOV   S1ADR,#IIC_ADR
	CALL  IIC_PRE		; S1CON 11000000B ; 11000001B
	MOV   S1CON,#01000000B	; S1CON pro X 24.000 MHz
	%LDR45i(IIC_INP)
	%LDR67i(SL_CMIC)
	MOV   R2,#SLAV_BL OR 080H
	CALL  IIC_SLX
	CLR   A			; Nulovani I2M pridelovace mastera
	MOV   R0,#6
	MOV   DPTR,#I2M_RQP	; a I2M_RQA a I2M_CB
INI_II3:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R0,INI_II3
	RET

SL_CM_R:JMP   SL_JRET

; Zpracovavani prikazu z IIC pod prerusenim
; registry  R0  .. funkce SJ_TSTA,SJ_TEND,SJ_RSTA,SJ_REND
;           R12 .. ukazatel na data
;           R3  .. pocet byte do konce S_BLEN
;	    S_DP   .. ukazatel na zacatek bufferu
;	    S_RLEN .. pro SJ_REND jiz naplnen poctem prijatych byte

SL_CMIC:CJNE  R0,#SJ_REND,SL_CM_R
	PUSH  DPL
	PUSH  DPH
	MOV   DPL,S_DP		; Bufer s prijmutymi daty
	MOV   DPH,S_DP+1
	MOVX  A,@DPTR		; Prijmuty prikaz
	INC   DPTR
	; zde pridavat
	JMP   SL_CM_N

SL_CM_A:CLR   ICF_SRC
	SETB  AA
SL_CM_N:MOV   R0,#SJ_REND
	POP   DPH
	POP   DPL
	JMP   SL_JRET

)FI

; *******************************************************************
; Pridelovani casu mastera

%IF(%WITH_IIC)THEN(

RSEG	RVO___X

OI2M_LEN SET  0
%STRUCTM(OI2M,NEXT,2)	; Dalsi pozadavek
%STRUCTM(OI2M,FNCP,2)	; Ukazatel na funkci

I2M_RQP:DS    2		; Ukazatel na prvni pozadovanou akci
I2M_RQA:DS    2		; Ukazatel na prave probihajici akci
I2M_CB:	DS    2		; Callback pri ukonceni prenosu

RSEG	RVO___C

; Test ukonceni prenosu
; pri volne sbernici vraci ACC=0
I2M_TBF:CLR   C
	JB    ICF_MER,I2M_TBF1
	JNB   ICF_MRQ,I2M_TBF2
	MOV   A,#1
I2M_RET1:RET
I2M_TBF1:CALL IIC_CER
I2M_TBF2:MOV  DPTR,#I2M_CB	; Konec
	MOVX  A,@DPTR
	MOV   R2,A
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	CLR   A
	MOVX  @DPTR,A
	MOV   A,R2
	ORL   A,R3
	JZ    I2M_RET1		; Je I2M_CB
I2M_TBF4:MOV  DPL,R2
	MOV   DPH,R3
	CLR   A
	JMP   @A+DPTR		; Pri chybe nastaveno CY

; Postupne pracuje podle I2M_RQP
I2M_POOL:CALL I2M_TBF
	JNZ   I2M_RET1
	MOV   DPTR,#I2M_RQA
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R0
	MOV   DPH,A
	ORL   A,R0
	JNZ   I2M_PO1
	MOV   DPTR,#I2M_RQP
I2M_PO1:MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   DPTR,#I2M_RQA
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	ORL   A,R4
	JZ    I2M_RET1
	MOV   DPL,R4
	MOV   DPH,R5
	INC   DPTR
	INC   DPTR
	JMP   xJMPDPP

; Stejne jako I2M_ADD ale nastavi funkci na R67
I2M_ADDF:MOV  DPL,R4
	MOV   DPH,R5
	INC   DPTR
	INC   DPTR
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A

; Prida OI2M v R45 do dotazovaciho cyklu
I2M_ADD:MOV   DPTR,#I2M_RQP
	MOVX  A,@DPTR
	MOV   R6,A
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   DPL,R4
	MOV   DPH,R5
	MOV   A,R6
	MOVX  @DPTR,A		; OI2M_NEXT
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	RET

; Vyjme OI2M v R45 z dotazovaciho cyklu
I2M_REM:MOV   DPTR,#I2M_RQA
	MOVX  A,@DPTR
	XRL   A,R4
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	XRL   A,R5
	ORL   A,R0
	JNZ   I2M_REM2
	CLR   A
	MOV   DPTR,#I2M_RQA
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
I2M_REM2:MOV  DPTR,#I2M_RQP
I2M_REM3:MOV  R2,DPL		; Vyjmuti z linkovaneho listu
	MOV   R3,DPH
	MOVX  A,@DPTR
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
	ORL   A,R6
	JZ    I2M_REM5
	MOV   DPL,R6
	MOV   DPH,R7
	MOV   A,R6
	XRL   A,R4
	JNZ   I2M_REM3
	MOV   A,R7
	XRL   A,R5
	JNZ   I2M_REM3
	MOVX  A,@DPTR
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
	MOV   DPL,R2
	MOV   DPH,R3
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
I2M_REM5:RET

)FI

; *******************************************************************
; Ovladani displeje pres 8577

%IF(%WITH_IICRVO)THEN(

RVLED_A EQU	40H
RVKBD_A EQU	42H
RVDIS_A EQU     74H
RVDIS_L EQU     4

; Definice segmentu
lcda    SET     02h             ;lcd segment a
lcdb    SET     01h             ;lcd segment b
lcdc    SET     04h             ;lcd segment c
lcdd    SET     10h             ;lcd segment d
lcde    SET     40h             ;lcd segment e
lcdf    SET     08h             ;lcd segment f
lcdg    SET     20h             ;lcd segment g
lcdh    SET     80h             ;lcd segment h colon

RSEG    RVO___D

WR_BUF: DS    RVDIS_L+1

RSEG    RVO___C

; Vypsani WR_BUF po IIC na DISPLAY
OUT_BUF:MOV   A,R6
	ANL   A,#0F8H
	MOV   R0,#WR_BUF
	MOV   @R0,A
	MOV   R6,#RVDIS_A
	MOV   R4,#WR_BUF
	MOV   R2,#RVDIS_L+1
	MOV   R3,#0
	JMP   IIC_RQI

;Vystup cisla na LCD display pres prevod LCD_TBL a LCD_POS
;Vstup: R45   vypisovane cislo
;       R7    .. format vystupu cisla
;                  SLLLxADD
;            S   - signed
;            A   - znamenko nebo cislice
;                  jinak znamenko nebo blank
;            LLL - delka vypisu > 0
;            DD  - pocet desetinych mist
;       R6    .. pozice vypisu

iPRTLi: MOV   A,R5
	MOV   C,ACC.7
	MOV   A,R7
	JNB   ACC.7,iPRTLi3
	JC    iPRTLi1
	JB    ACC.2,iPRTLi3
	MOV   A,#lcdBlnk
	JNC   iPRTLi2
iPRTLi1:CALL  NEGi
	MOV   A,#lcdSig
iPRTLi2:CALL  iPRTLc
	MOV   A,R7
	ADD   A,#-10H
	MOV   R7,A
iPRTLi3:MOV   A,R7
	SWAP  A
	ANL   A,#7
	MOV   R1,A
iPRTLi9:DEC   R1
	MOV   A,R1
	RL    A
	CPL   A
	ADD   A,#O10E4i-1+5*2
	MOV   R0,A
	CALL  rLDR23i
iPRTL10:MOV   R0,#-1
iPRTL11:CJNE  R0,#9,iPRTL12
iPRTLiE:MOV   A,#lcd_Err
	CALL  iPRTLc
	MOV   A,R7
	ANL   A,#70H
	ADD   A,#-10H
	MOV   R7,A
	JNZ   iPRTLiE
	RET
iPRTL12:CALL  SUBi
	INC   R0
	JNC   iPRTL11
	CALL  ADDi
	MOV   A,R7
	ANL   A,#3
	DEC   A
	XRL   A,R1
	JZ    iPRTL13
	CLR   C
iPRTL13:MOV   A,R0
	MOV   ACC.7,C
	CALL  iPRTLc
	MOV   A,R7
	CLR   ACC.7
	ADD   A,#-10H
	MOV   R7,A
	ANL   A,#70H
	JNZ   iPRTLi9
	RET

; Rotace vlevo o 4 bity

RL4R45: MOV   A,R4
	SWAP  A
	MOV   R4,A
	ANL   A,#0F0H
	XCH   A,R4
	ANL   A,#00FH
	XCH   A,R5
	SWAP  A
	XCH   A,R4
	XRL   A,R4
	XCH   A,R4
	ANL   A,#0F0H
	XCH   A,R5
	ORL   A,R5
	XCH   A,R5
	XRL   A,R4
	MOV   R4,A
	RET

)FI
%IF(%WITH_IICRVO)THEN(

; Vystup hexa
;Vstup: R45   vypisovane cislo
;       R6    .. pozice vypisu
iPRTLhw:MOV   R7,#4
iPRTLh: MOV   A,R5
	SWAP  A
	ANL   A,#00FH
	CALL  iPRTLc
	CALL  RL4R45
	DJNZ  R7,iPRTLh
	RET

iPRTLc: MOV     R0,#0
	JNB     ACC.7,iPRTLc1
	CLR     ACC.7
	MOV     R0,#lcdH
iPRTLc1:ADD     A,#LCD_TBL-iPRTLck
	MOVC    A,@A+PC
iPRTLck:ORL     A,R0
iPRTLg: MOV     R0,A
	MOV     A,R6
	INC     R6
	ANL     A,#07H
	ADD     A,#LCD_POS-iPRTLcl
	MOVC    A,@A+PC
iPRTLcl:ADD     A,#WR_BUF+1
	XCH     A,R0
	MOV     @R0,A
	RET

lcdBlnk SET     10H
lcdSig  SET     17h
lcd_Err SET     17h ; = '-' dalsi moznost 0Eh = 'E'

LCD_POS:DB      3
	DB      2
	DB      0
	DB      1

LCD_TBL:db      lcda+lcdb+lcdc+lcdd+lcde+lcdf           ;0
	db      lcdb+lcdc                               ;1
	db      lcda+lcdb+lcdg+lcde+lcdd                ;2
	db      lcda+lcdb+lcdg+lcdc+lcdd                ;3
	db      lcdf+lcdg+lcdb+lcdc                     ;4
	db      lcda+lcdf+lcdg+lcdc+lcdd                ;5
	db      lcda+lcdf+lcdg+lcde+lcdd+lcdc           ;6
	db      lcda+lcdb+lcdc                          ;7
	db      lcda+lcdb+lcdc+lcdd+lcde+lcdf+lcdg      ;8
	db      lcda+lcdb+lcdf+lcdg+lcdc+lcdd           ;9
	db      lcda+lcdb+lcdf+lcdg+lcdc+lcde           ;A
	db      lcdc+lcdd+lcde+lcdf+lcdg                ;b
	db      lcda+lcdd+lcde+lcdf                     ;C
	db      lcde+lcdg+lcdd+lcdc+lcdb                ;d
	db      lcda+lcdd+lcde+lcdf+lcdg                ;E
	db      lcda+lcde+lcdf+lcdg                     ;F
	db      0                                       ;blank
	db      lcda                                    ;segment a
	db      lcdb                                    ;segment b
	db      lcdc                                    ;segment c
	db      lcdd                                    ;segment d
	db      lcde                                    ;segment e
	db      lcdf                                    ;segment f
	db      lcdg                                    ;segment g
	db      lcdb+lcdc+lcde+lcdf+lcdg                ;H
	db      lcdd+lcde+lcdf                          ;L
	db      lcda+lcdb+lcdf+lcdg                     ;degree
	db      lcdc+lcdd+lcde+lcdg                     ;lower degree
	db      0                                       ;spare
	db      0                                       ;spare
	db      0                                       ;spare
	db      0                                       ;spare
	db      lcdh                                    ;colon

)FI
%IF(%WITH_IICRVO AND 0)THEN(

RVK_TST:
	CALL  IIC_WME		; cekat na ukonceni prenosu
	%LDR45i(1234)
	MOV   R6,#10H
;       CALL  iPRTLhw
	MOV   R7,#0C4H
	CALL  iPRTLi
	MOV   R6,#10H
	CALL  OUT_BUF
	CALL  IIC_WME		; cekat na ukonceni prenosu

	;JNZ
RVK_TST1:
	MOV   A,#055H
RVK_TST2:
	CALL  SCANKEY
	JNZ   RVK_TST9

	MOV   WR_BUF,A
	MOV   R6,#RVLED_A
	MOV   R4,#WR_BUF
	MOV   R2,#1
	MOV   R3,#0
	CALL  IIC_RQI
	CALL  IIC_WME		; cekat na ukonceni prenosu

	MOV   R6,#RVKBD_A
	MOV   R4,#WR_BUF
	MOV   R2,#0
	MOV   R3,#1
	CALL  IIC_RQI
	CALL  IIC_WME		; cekat na ukonceni prenosu
	JNZ   RVK_TST1
	MOV   A,WR_BUF
	CPL   A
	JZ    RVK_TST2

	PUSH  ACC
	MOV   R4,A
	MOV   R5,B
	INC   B
	MOV   R6,#10H
	CALL  iPRTLhw
	MOV   R6,#10H
	CALL  OUT_BUF
	CALL  IIC_WME		; cekat na ukonceni prenosu

	POP   ACC
	JMP   RVK_TST2

RVK_TST9:
	RET

)FI

; *******************************************************************
; Klavesnice na IIC

%IF(%WITH_IICRVO)THEN(

%IF (NOT %FOR_BATH) THEN (
RVKL_UP   EQU 040H	; Klavesnice pro RVO
RVKL_LESS EQU 047H
RVKL_MORE EQU 046H
RVKL_MODE EQU 044H
RVKL_DOWN EQU 041H
RVKL_ROT  EQU 042H
RVKL_TEMP EQU 043H
RVKL_12   EQU 045H
)ELSE(
RVKL_STRT EQU 040H	; Klavesnice pro VANU
RVKL_MODE EQU 044H
RVKL_LESS EQU 041H
RVKL_MORE EQU 042H
RVKL_TEMP EQU 043H
RVKL_12   EQU 045H
)FI

%IF (%FOR_BATH AND %WITH_TIMER) THEN (
BRVL_RPM  EQU 1
BRVL_TEMP EQU 0
)ELSE(
BRVL_RPM  EQU 0
BRVL_TEMP EQU 1
)FI
BRVL_HPA  EQU 2
BRVL_TIME EQU 3
BRVL_ROT  EQU 4
BRVL_HEAT EQU 5
BRVL_1    EQU 6
BRVL_2    EQU 7

RSEG    RVO___X

RVK_MLED:DS   4
RVK_MKBD:DS   4
RVK_MDIS:DS   4

RVK_FLG:DS    1		; priznaky klavesnice
RVK_CHG:DS    1		; detekovane zmeny oproti minulemu stavu
RVK_KEY:DS    1		; akceptovany stav klaves
RVK_PUS:DS    1		; prodleva po stisku
RVK_REP:DS    1		; prodleva pri opakovani

RVK_REPC SET  5		; Pocet taktu repeatu
RVK_PUSC SET  20	; Cekani po stisku
RVK_OFFC SET  2		; Delka uvolneni

RSEG    RVO___C

RVK_INI:%LDR45i(RVK_MLED)
	%LDR67i(RVK_FLED)
	CALL  I2M_ADDF
	%LDR45i(RVK_MKBD)
	%LDR67i(RVK_FKBD)
	CALL  I2M_ADDF
	%LDR45i(RVK_MDIS)
	%LDR67i(RVK_FDIS)
	CALL  I2M_ADDF
	MOV   RV_ACT,#LOW RVO_FRSD	; Pocatecni display
	MOV   RV_ACT+1,#HIGH RVO_FRSD
	CLR   A
	MOV   DPTR,#RV_EDREP
	MOVX  @DPTR,A
	MOV   DPTR,#RVK_TIM
	MOVX  @DPTR,A
	MOV   DPTR,#RVK_KEY
	MOVX  @DPTR,A
	MOV   DPTR,#RVK_FLG
	MOVX  @DPTR,A
	MOV   A,#RVK_PUSC
	MOV   DPTR,#RVK_PUS
	MOVX  @DPTR,A
	MOV   A,#RVK_REPC
	MOV   DPTR,#RVK_REP
	MOVX  @DPTR,A
	; Konfigurace obvodu klavesnice
	CALL  IIC_WME		; cekat na ukonceni prenosu
	MOV   WR_BUF,#0FFH
	MOV   R6,#RVKBD_A
	MOV   R4,#WR_BUF
	MOV   R2,#1
	MOV   R3,#1
	CALL  IIC_RQI
	CALL  IIC_WME		; cekat na ukonceni prenosu
	RET

)FI
%IF(%WITH_IICRVO)THEN(
; Vystup LED

RVK_FLED:
	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_LED
	MOVC  A,@A+DPTR		; LED z modu displaye
	MOV   B,A
	CLR   A			; konfigurece 1/2
	JB    FL_CONFK,RVK_FLED1
	MOV   A,#1 SHL BRVL_1
	JNB   FL_CONF2,RVK_FLED1
	MOV   A,#1 SHL BRVL_2
RVK_FLED1:
	ORL   B,A
	MOV   DPTR,#REG_A+OREG_A+OMR_FLG
	MOVX  A,@DPTR		; Stav motoru otaceni
	JNB   ACC.BMR_ERR,RVK_FLED2
	CLR   FL_TLAP
	MOV   C,LEB_PHA
	SJMP  RVK_FLED5
RVK_FLED2:
	MOV   C,ACC.BMR_BSY
    %IF(%WITH_TIMER AND NOT %FOR_BATH)THEN(
	JNC   RVK_FLED5
	CALL  RVK_FLED_TLAP	; Blikani TIME pri behu casovace
	SETB  C
    )FI
RVK_FLED5:
	MOV   B.BRVL_ROT,C
	MOV   DPTR,#TMC1_FLG
	MOVX  A,@DPTR		; Stav regulace teploty
	JNB   ACC.BMR_ERR,RVK_FLED6
	MOV   C,LEB_PHA
	SJMP  RVK_FLED7
RVK_FLED6:
	MOV   C,ACC.BMR_ENR
RVK_FLED7:
	MOV   B.BRVL_HEAT,C
    %IF(%WITH_TIMER AND %FOR_BATH)THEN(
	JNB   FL_TLAP,RVK_FLED8
	CALL  RVK_FLED_TLAP	; Blikani TIME pri behu casovace
RVK_FLED8:
    )FI
	MOV   A,B
	CPL   A
	MOV   WR_BUF,A
	MOV   R6,#RVLED_A
	MOV   R4,#WR_BUF
	MOV   R2,#1
	MOV   R3,#0
	JMP   IIC_RQI

%IF(%WITH_TIMER)THEN(
RVK_FLED_TLAP:
	MOV   DPTR,#TIM_STP
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	ORL   A,R0
	JZ    RVK_FLED_TLAP9
	JNB   LEB_PHA,RVK_FLED_TLAP9
	MOV   DPTR,#TIMRI
	MOVX  A,@DPTR
	ANL   A,#NOT 3
	JNZ   RVK_FLED_TLAP9
	XRL   B,#1 SHL BRVL_TIME
RVK_FLED_TLAP9:
	RET
)FI

)FI
%IF(%WITH_IICRVO)THEN(
; Zpracovani stisknutych klaves

RVK_FKBD:MOV  DPTR,#I2M_CB
	MOV   A,#LOW  RVK_CKBD
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH RVK_CKBD
	MOVX  @DPTR,A
	MOV   R6,#RVKBD_A
	MOV   R4,#WR_BUF
	MOV   R2,#0
	MOV   R3,#1
	JMP   IIC_RQI

RVK_CKBD:JNC  RVK_CK1
	RET
RVK_CK1:MOV   A,WR_BUF
	CPL   A
	MOV   R2,A		; Stisknute klavesy
	MOV   DPTR,#RVK_KEY
	MOVX  A,@DPTR
	XRL   A,R2
	MOV   R2,A
	MOV   DPTR,#RVK_CHG
	MOVX  A,@DPTR
	XCH   A,R2
	MOVX  @DPTR,A
	ANL   A,R2		; Zmena
	JZ    RVK_CR1
	MOV   R3,#7
RVK_CK2:RLC   A
	JC    RVK_CK3
	DJNZ  R3,RVK_CK2
RVK_CK3:MOV   DPTR,#RVK_FLG	; Test stisku klavesy
	MOVX  A,@DPTR
	MOV   R2,A
	XRL   A,R3
	ANL   A,#7
	JNZ   RVK_CK4
	MOV   DPTR,#RVK_TIM
	MOVX  A,@DPTR
	JNZ   RVK_CR6
RVK_CK4:MOV   A,R3
	ADD   A,#RVK_CKt-RVK_CKb
	MOVC  A,@A+PC
RVK_CKb:MOV   R2,A
	MOV   DPTR,#RVK_CHG	; Registrace zmeny
	MOVX  A,@DPTR
	XRL   A,R2
	MOVX  @DPTR,A
	MOV   DPTR,#RVK_KEY
	MOVX  A,@DPTR
	XRL   A,R2
	MOVX  @DPTR,A
	ANL   A,R2
	JZ    RVK_CK7
RVK_CK5:MOV   DPTR,#RVK_FLG	; Stisk klavesy
	MOV   A,R3
	ORL   A,#0C0H
	MOVX  @DPTR,A
	MOV   DPTR,#RVK_PUS
	MOVX  A,@DPTR
RVK_CK6:MOV   DPTR,#RVK_TIM	; Cas blokovani
	MOVX  @DPTR,A
	MOV   A,R3
	ADD   A,#040H
	MOV   R2,A
	CALL  KBDBEEP
	SJMP  RVK_CK8
RVK_CK7:MOV   DPTR,#RVK_FLG	; Uvolneni klavesy
	MOV   A,R3
	MOVX  @DPTR,A
	MOV   A,#RVK_OFFC
	MOV   DPTR,#RVK_TIM
	MOVX  @DPTR,A
	MOV   A,R3
	ADD   A,#0C0H
	MOV   R2,A
RVK_CK8:

	MOV   R7,#ET_KEY
	MOV   A,R2
	MOV   R4,A
	MOV   R5,#0
	JMP   EV_POST

RVK_CR1:MOV   DPTR,#RVK_FLG
	MOVX  A,@DPTR
	MOV   R2,A
	JNB   ACC.7,RVK_CR9
	JNB   ACC.6,RVK_CR4
	MOV   DPTR,#RVK_TIM
	MOVX  A,@DPTR
	JNZ   RVK_CR9
	MOV   A,R2		; Repeat
	ANL   A,#07H
	MOV   R3,A
	MOV   DPTR,#RVK_REP
	MOVX  A,@DPTR
	JZ    RVK_CR9
	SJMP  RVK_CK6
RVK_CR4:MOV   A,R2
	SETB  ACC.6
	MOV   R2,A
	MOV   DPTR,#RVK_PUS
	MOVX  A,@DPTR
	SJMP  RVK_CR8
RVK_CR6:MOV   A,R2
	JNB   ACC.6,RVK_CR9
	CLR   ACC.6
	MOV   R2,A
	MOV   A,#RVK_OFFC
RVK_CR8:MOV   DPTR,#RVK_TIM
	MOVX  @DPTR,A
	MOV   A,R2
	MOV   DPTR,#RVK_FLG
	MOVX  @DPTR,A
RVK_CR9:CLR   A
	MOV   R2,A
	RET

RVK_CKt:DB    00000001B
	DB    00000010B
	DB    00000100B
	DB    00001000B
	DB    00010000B
	DB    00100000B
	DB    01000000B
	DB    10000000B

)FI
%IF(%WITH_IICRVO)THEN(
; Numericky display na IIC

ORV_LEN	SET   0
%STRUCTM(ORV,NEXT,2)	; dalsi mod displeje
%STRUCTM(ORV,ALT ,2)	; alternativni mod displeje
%STRUCTM(ORV,DFN ,2)	; zobrazovaci rutina
%STRUCTM(ORV,EFN ,2)	; editacni rutina rutina
%STRUCTM(ORV,LED ,1)	; co ukazat na LED
%STRUCTM(ORV,SFT ,2)	; ukazatel na tabulku klaves
;----------------------   shodne s UI
%STRUCTM(ORV,A_RD,2)	; rutina volana pro nacteni dat
%STRUCTM(ORV,A_WR,2)	; rutina volana pro ulozeni dat
%STRUCTM(ORV,DPSI,2)	; info pro up todate a sit
%STRUCTM(ORV,DP  ,2)	; ukazatel na menena data
;----------------------
%STRUCTM(ORV,F   ,1)	; format cisla
%STRUCTM(ORV,L   ,2)	; spodni limit
%STRUCTM(ORV,H   ,2)	; horni limit

RVK_UNSTC EQU 25*2	; doba pocatecniho zobrazeni unst rezimu
RVK_UNSTE EQU 25*5	; doba pridrzeni pri editaci

RSEG    RVO___D

RV_ACT:	DS    2		; Aktualni popis pro zobrazeni

RSEG    RVO___X

RV_EDREP:DS   1		; Opakovani klavesy => zvetsit zmenu

RSEG    RVO___C

; Nacteni dat pres ORV_A_RD
RV_RD:	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_A_RD
	MOVC  A,@A+DPTR
	PUSH  ACC
	MOV   A,#ORV_A_RD+1
RV_RD1:	MOVC  A,@A+DPTR
	PUSH  ACC
	MOV   R1,#0
	MOV   A,DPL
	ADD   A,#LOW  (ORV_A_RD-OU_A_RD)
	MOV   DPL,A
	MOV   A,DPH
	ADDC  A,#HIGH (ORV_A_RD-OU_A_RD)
	MOV   DPH,A
	CLR   F0
	RET

; Ulozeni dat pres ORV_A_WR
RV_WR:	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_A_WR
	MOVC  A,@A+DPTR
	PUSH  ACC
	MOV   A,#ORV_A_WR+1
	SJMP  RV_RD1

; Periodicke zobrazovani
RVK_FDIS:
	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_DFN
	MOVC  A,@A+DPTR
	PUSH  ACC
	MOV   A,#ORV_DFN+1
	MOVC  A,@A+DPTR
	PUSH  ACC
	RET

; Casovane zobrazeni hodin a minut
RV_DFNhmU:
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#RVK_TUNST
	MOVX  A,@DPTR
	POP   DPH
	POP   DPL
	JNZ   RV_DFNhm
	JMP   RVK_ALT
; Zobrazeni hodin a minut
RV_DFNhm:
	CALL  RV_RD	; Nacteni dat, pri chybe F0
	MOV   A,R4
	PUSH  ACC
	MOV   A,R5
	PUSH  ACC
	%LDR23i(60)
	CALL  DIVi
	MOV   R7,#0A4H
	MOV   R6,#10H
	CALL  iPRTLi
	POP   ACC
	MOV   R5,A
	POP   ACC
	MOV   R4,A
	%LDR23i(60)
	CALL  MODi
	MOV   R7,#0A4H
	MOV   R6,#10H+2
	CALL  iPRTLi
	MOV   R0,#WR_BUF+1	; Pridani dvojtecky
	MOV   A,@R0
	ORL   A,#lcdh
	MOV   @R0,A
	MOV   R6,#10H
	JMP   OUT_BUF

)FI
%IF(%WITH_IICRVO)THEN(
; Casovane zobrazeni cisla integer
RV_DFNiU:
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#RVK_TUNST
	MOVX  A,@DPTR
	POP   DPH
	POP   DPL
	JZ    RVK_ALT
; Zobrazeni cisla integer
RV_DFNi:CALL  RV_RD	; Nacteni dat, pri chybe F0
	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_F
	MOVC  A,@A+DPTR
	MOV   R7,A	; Format, napr 0C4H
	MOV   R6,#10H
	CALL  iPRTLi
	MOV   R6,#10H
	JMP   OUT_BUF

; Nasledujici polozka
RVK_NEXT:
	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_NEXT	; nasledujici polozka
	MOVC  A,@A+DPTR
	MOV   RV_ACT,A
	MOV   A,#ORV_NEXT+1
	MOVC  A,@A+DPTR
	MOV   RV_ACT+1,A
RVK_NEXT1:
	MOV   A,#RVK_UNSTC
RVK_NEXT2:
	MOV   DPTR,#RVK_TUNST	; pro nestabilni rezimy
	MOVX  @DPTR,A
	CLR   A
	MOV   DPTR,#RV_EDREP	; nulovat pocitadlo opakovani
	MOVX  @DPTR,A
RVK_NEXT9:RET

; Alternativni nasledujici polozka
RVK_ALT:MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_ALT	; alternativni nasledujici polozka
	MOVC  A,@A+DPTR
	MOV   RV_ACT,A
	MOV   A,#ORV_ALT+1
	MOVC  A,@A+DPTR
	MOV   RV_ACT+1,A
	MOV   A,#RVK_UNSTE
	SJMP  RVK_NEXT2

; Prechod na ALT display pri editaci
RVK_EFNALT:
	CALL  RVK_ALT
; Editace
RVK_EDIT:
	MOV   DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   A,#ORV_EFN
	MOVC  A,@A+DPTR
	PUSH  ACC
	MOV   A,#ORV_EFN+1
	MOVC  A,@A+DPTR
	PUSH  ACC
	RET

)FI
%IF(%WITH_IICRVO)THEN(

; Editace cisla integer v docasnem rezimu
RV_EFNiU:
	PUSH  DPL
	PUSH  DPH
	MOV   A,#RVK_UNSTE	; podrzeni rezimu
	MOV   DPTR,#RVK_TUNST
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
; Editace cisla integer
RV_EFNi:
	MOV   A,R2
	PUSH  ACC
	CALL  RV_RD	; Nacteni dat, pri chybe F0
	JNB   F0,RV_Ei10
	CLR   A
	MOV   R4,A
	MOV   R5,A
RV_Ei10:POP  ACC
	CJNE  A,#RVKL_MORE,RV_Ei20
	CALL  RV_EPROF
	CALL  ADDi
	MOV   A,#ORV_H
	CALL  RV_ETSTL
	MOV   C,OV
	XRL   A,PSW
	JB    ACC.7,RV_Ei60
	SJMP  RV_Ei50
RV_Ei20:CJNE  A,#RVKL_LESS,RV_Ei80
	CALL  RV_EPROF
	CALL  SUBi
	MOV   A,#ORV_L
	CALL  RV_ETSTL
	MOV   C,OV
	XRL   A,PSW
	JNB   ACC.7,RV_Ei60
RV_Ei50:MOV   A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
RV_Ei60:JMP   RV_WR	; Ulozeni dat
RV_Ei80:MOV   DPTR,#RV_EDREP
	CLR   A
	MOVX  @DPTR,A
	RET

; Otestuje R45 proti limitu v ORV_L nebo ORV_H
RV_ETSTL:MOV  DPL,RV_ACT
	MOV   DPH,RV_ACT+1
	MOV   R2,A
	MOVC  A,@A+DPTR
	XCH   A,R2
	INC   A
	MOVC  A,@A+DPTR
	MOV   R3,A
	JMP   CMPi

; Profil zrychlovani editace
RV_EPROF:MOV  DPTR,#RV_EDREP
	MOVX  A,@DPTR
	INC   A
	CJNE  A,#8*6,RV_EPR1
	DEC   A
RV_EPR1:MOVX  @DPTR,A
	ANL   A,#NOT 7
	RR    A
	RR    A
	MOV   R2,A
	ADD   A,#RV_EPRt-RV_EPRb1
	MOVC  A,@A+PC
RV_EPRb1:XCH  A,R2
	ADD   A,#RV_EPRt-RV_EPRb2+1
	MOVC  A,@A+PC
RV_EPRb2:MOV  R3,A
	RET

RV_EPRt:%W    (1)
	%W    (10)
	%W    (20)
	%W    (50)
	%W    (100)
	%W    (200)

; Stisknuti klavesy RVKL_12
RV_CNFP:JB    FL_CONFK,RV_CF20
	SETB  FL_CONFK
	MOV   A,#RVK_UNSTE
RV_CF18:MOV   DPTR,#RVK_TIM
	MOVX  @DPTR,A
	RET
; Ulozit aktualni konfiguraci
RV_CF20:CLR   FL_CONFK
	MOV   A,#255
	CALL  RV_CF18
	%LDR23i(EEC_CF1)
	JNB   FL_CONF2,RV_CF21
	%LDR23i(EEC_CF2)
RV_CF21:JMP   EEP_WRS

; Uvolneni klavesy RVKL_12
RV_CNFR:JBC   FL_CONFK,RV_CF40
	RET
RV_CF40:CALL  TMC_OFF
	CALL  REL_ALL
	JNB   FL_CONF2,RV_CF42
RV_CF41:CLR   FL_CONF2
	%LDR23i(EEC_CF1)
	JMP   EEP_RDS
RV_CF42:SETB  FL_CONF2
	%LDR23i(EEC_CF2)
	JMP   EEP_RDS

)FI
%IF(%WITH_IICRVO)THEN(

RSEG    RVO___C

; ----- nastaveni RPM
%IF (NOT %FOR_BATH) THEN (
RVO_FRSD:			; Prvni display pro RVO
)FI
RVO_100:DS    RVO_100+ORV_NEXT-$
	%W    (RVO_115)		; dalsi mod displeje
	DS    RVO_100+ORV_ALT-$
	%W    (0)		; alternativni mod
	DS    RVO_100+ORV_DFN-$
	%W    (RV_DFNi)		; zobrazovaci rutina
	DS    RVO_100+ORV_EFN-$
	%W    (RV_EFNi)		; editacni rutina rutina
	DS    RVO_100+ORV_LED-$
	DB    1 SHL BRVL_RPM	; co ukazat na LED
	DS    RVO_100+ORV_SFT-$
	%W    (0)		; ukazatel na tabulku klaves
	DS    RVO_100+ORV_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_RSPD)		; A_WR
	DS    RVO_100+ORV_DPSI-$
	%W    (0)		; DPSI
	DB    0,OMR_RSPD	; DP
	DS    RVO_100+ORV_F-$
	DB     0C0H		; I_F
	%W    (-%MAX_RSPD)	; I_L
	%W    ( %MAX_RSPD)	; I_H

; ----- cteni teploty
%IF (%FOR_BATH) THEN (
RVO_FRSD:			; Prvni display pro VANU
)FI
RVO_110:DS    RVO_110+ORV_NEXT-$
   %IF (%FOR_BATH AND %WITH_TIMER) THEN (
	%W    (RVO_135)		; pro vanu s casem
   )ELSE(
	%W    (RVO_125)		; dalsi mod displeje
   )FI
	DS    RVO_110+ORV_ALT-$
	%W    (RVO_115)		; alternativni mod
	DS    RVO_110+ORV_DFN-$
	%W    (RV_DFNi)		; zobrazovaci rutina
	DS    RVO_110+ORV_EFN-$
	%W    (RVK_EFNALT)	; editacni rutina rutina
	DS    RVO_110+ORV_LED-$
	DB    1 SHL BRVL_TEMP	; co ukazat na LED
	DS    RVO_110+ORV_SFT-$
	%W    (0)		; ukazatel na tabulku klaves
	DS    RVO_110+ORV_A_RD-$
	%W    (UR_TEMP)		; A_RD
	%W    (UW_TEMP)		; A_WR
	DS    RVO_110+ORV_DPSI-$
	%W    (TMC1_RT)		; DPSI
	%W    (TMC1_AT)		; DP
	DS    RVO_110+ORV_F-$
	DB    0C5H		; I_F
	%W    (0)		; I_L
	%W    (%MAX_TMC1_RT)	; I_H

; ----- nastaveni teploty
RVO_115:DS    RVO_115+ORV_NEXT-$
   %IF (%FOR_BATH AND %WITH_TIMER) THEN (
	%W    (RVO_135)		; pro vanu s casem
   )ELSE(
	%W    (RVO_125)		; dalsi mod displeje
   )FI
	DS    RVO_115+ORV_ALT-$
	%W    (RVO_110)		; alternativni mod
	DS    RVO_115+ORV_DFN-$
	%W    (RV_DFNiU)	; zobrazovaci rutina
	DS    RVO_115+ORV_EFN-$
	%W    (RV_EFNiU)	; editacni rutina rutina
	DS    RVO_115+ORV_LED-$
	DB    1 SHL BRVL_TEMP	; co ukazat na LED
	DS    RVO_115+ORV_SFT-$
	%W    (0)		; ukazatel na tabulku klaves
	DS    RVO_115+ORV_A_RD-$
	%W    (UR_TEMP)		; A_RD
	%W    (UW_TEMP)		; A_WR
	DS    RVO_115+ORV_DPSI-$
	%W    (TMC1_RT)		; DPSI
	%W    (TMC1_RT)		; DP
	DS    RVO_115+ORV_F-$
	DB    045H		; I_F
	%W    (0)		; I_L
	%W    (%MAX_TMC1_RT)	; I_H

)FI
%IF(%WITH_IICRVO)THEN(
; -----  cteni podtlaku
RVO_120:DS    RVO_120+ORV_NEXT-$
    %IF(NOT %WITH_TIMER)THEN(
	%W    (RVO_100)		; dalsi mod displeje
    )ELSE(
	%W    (RVO_135)		; dalsi mod displeje
    )FI
	DS    RVO_120+ORV_ALT-$
	%W    (RVO_125)		; alternativni mod
	DS    RVO_120+ORV_DFN-$
	%W    (RV_DFNi)		; zobrazovaci rutina
	DS    RVO_120+ORV_EFN-$
	%W    (RVK_EFNALT)	; editacni rutina rutina
	DS    RVO_120+ORV_LED-$
	DB    1 SHL BRVL_HPA	; co ukazat na LED
	DS    RVO_120+ORV_SFT-$
	%W    (0)		; ukazatel na tabulku klaves
	DS    RVO_120+ORV_A_RD-$
	%W    (UR_TEMP)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    RVO_120+ORV_DPSI-$
	%W    (0)		; DPSI
	%W    (TPS_AP)		; DP
	DS    RVO_120+ORV_F-$
	DB     0C4H ; 0C5H	; I_F
	%W    (0)		; I_L
	%W    (1000)		; I_H

; -----  nastaveni pozadovaneho podtlaku
RVO_125:DS    RVO_125+ORV_NEXT-$
    %IF(NOT %WITH_TIMER)THEN(
	%W    (RVO_100)		; dalsi mod displeje
    )ELSE(
	%W    (RVO_135)		; dalsi mod displeje
    )FI
	DS    RVO_125+ORV_ALT-$
	%W    (RVO_120)		; alternativni mod
	DS    RVO_125+ORV_DFN-$
	%W    (RV_DFNiU)	; zobrazovaci rutina
	DS    RVO_125+ORV_EFN-$
	%W    (RV_EFNiU)	; editacni rutina rutina
	DS    RVO_125+ORV_LED-$
	DB    1 SHL BRVL_HPA	; co ukazat na LED
	DS    RVO_125+ORV_SFT-$
	%W    (0)		; ukazatel na tabulku klaves
	DS    RVO_125+ORV_A_RD-$
	%W    (UR_TEMP)		; A_RD
	%W    (UW_TPS_RQ)	; A_WR
	DS    RVO_125+ORV_DPSI-$
	%W    (TPS_RQP)		; DPSI
	%W    (TPS_RQP)		; DP
	DS    RVO_125+ORV_F-$
	DB     0C4H ; 0C5H	; I_F
	%W    (-999)		; I_L
	%W    (1000)		; I_H

)FI
%IF(%WITH_IICRVO)THEN(
%IF(%WITH_TIMER)THEN(
; -----  aktualni cas minutky
RVO_130:DS    RVO_130+ORV_NEXT-$
   %IF (NOT %FOR_BATH) THEN (
	%W    (RVO_100)		; dalsi mod displeje
   )ELSE(
	%W    (RVO_115)		; pro vanu
   )FI
	DS    RVO_130+ORV_ALT-$
	%W    (RVO_135)		; alternativni mod
	DS    RVO_130+ORV_DFN-$
	%W    (RV_DFNhm)	; zobrazovaci rutina
	DS    RVO_130+ORV_EFN-$
	%W    (RVK_EFNALT)	; editacni rutina rutina
	DS    RVO_130+ORV_LED-$
	DB    1 SHL BRVL_TIME	; co ukazat na LED
	DS    RVO_130+ORV_SFT-$
	%W    (0)		; ukazatel na tabulku klaves
	DS    RVO_130+ORV_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    RVO_130+ORV_DPSI-$
	%W    (0)		; DPSI
	%W    (TIM_ACT)		; DP
	DS    RVO_130+ORV_F-$
	DB     0C4H ; 0C5H	; I_F
	%W    (0)		; I_L
	%W    (99*60)		; I_H

; -----  nastavovani limitu minutky
RVO_135:DS    RVO_135+ORV_NEXT-$
   %IF (NOT %FOR_BATH) THEN (
	%W    (RVO_100)		; dalsi mod displeje
   )ELSE(
	%W    (RVO_115)		; pro vanu
   )FI
	DS    RVO_135+ORV_ALT-$
	%W    (RVO_130)		; alternativni mod
	DS    RVO_135+ORV_DFN-$
	%W    (RV_DFNhmU)	; zobrazovaci rutina
	DS    RVO_135+ORV_EFN-$
	%W    (RV_EFNiU)	; editacni rutina rutina
	DS    RVO_135+ORV_LED-$
	DB    1 SHL BRVL_TIME	; co ukazat na LED
	DS    RVO_135+ORV_SFT-$
	%W    (0)		; ukazatel na tabulku klaves
	DS    RVO_135+ORV_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_DIMi)		; A_WR
	DS    RVO_135+ORV_DPSI-$
	%W    (0)		; DPSI
	%W    (TIM_STP)		; DP
	DS    RVO_135+ORV_F-$
	DB     0C4H ; 0C5H	; I_F
	%W    (0)		; I_L
	%W    (99*60)		; I_H
)FI
)FI

RVO_FFROT:
	MOV   A,R2
	MOV   R1,A
	MOV   A,#OMR_FLG
	CALL  MR_GPA1
	MOVX  A,@DPTR
	JNB   ACC.BMR_ERR,RVO_FFR1
	CALL  CER_GEP
	SETB  FL_MRCE
	RET
RVO_FFR1:
	JNB   ACC.BMR_BSY,RVO_STROT1
	CLR   FL_TLAP
	JMP   REL_GEP
RVO_STROT:
	MOV   A,R2
	MOV   R1,A
RVO_STROT1:
    %IF(%WITH_TIMER)THEN(
	CALL  RVO_STLAP		; vynulovani a odstartovani casovani
    )FI
RVO_STROT2:
	MOV   A,#OMR_RSPD
	CALL  MR_GPA1
	CALL  xLDR45i
	%LDR23i(C_RPM2SPD)
	CALL  MULsi
	MOV   A,#C_RPM2SPD_E
	CALL  SHRls
	JMP   GO_GSP

%IF(%WITH_TIMER)THEN(
RVO_FFTLAP_TMC:
	CALL  TMC_ON
RVO_FFTLAP:
	JNB   FL_TLAP,RVO_STLAP
	CLR   FL_TLAP
	RET

RVO_STLAP:			; nulovani a start casovani
	MOV   DPTR,#TIM_MDI
	CLR   A
	MOVX  @DPTR,A
	MOV   DPTR,#TIM_ACT	; vynulovani casu odparovani
	MOV   C,EA
	CLR   EA
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   EA,C
	SETB  FL_TLAP
	RET
)FI

UW_RSPD:MOV   A,#OU_DP
	MOVC  A,@A+DPTR
	MOV   R1,A
	MOV   A,#OMR_RSPD
	CALL  MR_GPA1
	CALL  xSVR45i
	MOV   A,#OMR_FLG
	CALL  MR_GPA1
	MOVX  A,@DPTR
	JB    ACC.BMR_BSY,RVO_STROT2
	RET

; *******************************************************************
; Prace s pameti EEPROM 8582

%IF(%WITH_IIC)THEN(

EEP_ADR	EQU   0A0H	; Adresa EEPROM na IIC sbernici
C_S1CON EQU   11000000B ; 11000001B ; Pocatecni stav S1CON

EEA_RD	EQU   1		; akce cteni
EEA_WR	EQU   2		; akce zapisu

RSEG	RVO___D

RSEG	RVO___X

EE_CNT:	DS    1		; citac byte pro prenos z/do EEPROM
EE_PTR:	DS    2		; ukazatel do EE_MEM na data
EEP_TAB:DS    2		; popis dat ulozenych v bloku EEPROM

EE_MEM:	DS    100H	; Buffer pameti EEPROM

RSEG	RVO___C

XOR_SUM:CLR   A
XOR_SU1:MOV   R3,A
	MOVX  A,@DPTR
	INC   DPTR
	XRL   A,R3
	INC   A
	DJNZ  R2,XOR_SU1
	RET

; Cteni pameti EEPROM
;	DPTR .. pointer na buffer
;	R2   .. pocet ctenych byte
;	R4   .. adresa, od ktere se cte

EE_RD:	MOV   A,DPL
	MOV   R0,DPH
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A		; cilova adresa
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	MOV   DPTR,#IIC_BUF
	MOV   A,R4		; adresa v pameti EEPROM
	MOVX  @DPTR,A
	MOV   DPTR,#EE_CNT
	MOV   A,R2		; pocet prenasenych byte
	MOVX  @DPTR,A
	CALL  IIC_WME		; cekat na ukonceni prenosu
EE_RD1:	%LDR45i(IIC_BUF)
	MOV   R2,#1
	MOV   R3,#010H
	MOV   R6,#EEP_ADR
	CALL  IIC_RQX		; pozadavek na IIC
	JNZ   EE_RD1
	CALL  IIC_WME		; cekat na ukonceni prenosu
	JNZ   EE_RDR
	MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR		; cilova adresa
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	%LDR23i(IIC_BUF+1)
	%LDR01i(10H)
	CALL  xxMOVE
	MOV   DPTR,#EE_PTR
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   DPTR,#IIC_BUF
	MOVX  A,@DPTR		; nova adresa v EEPROM
	ADD   A,#10H
	MOVX  @DPTR,A
	MOV   DPTR,#EE_CNT
	MOVX  A,@DPTR
	ADD   A,#-10H-1
	INC   A
	MOVX  @DPTR,A
	JC    EE_RD1
EE_RDR:	RET

; Zapis do pameti EEPROM
;	DPTR .. pointer na buffer
;	R2   .. pocet zapisovanych byte
;	R4   .. adresa, od ktere se zapisuje

EE_WRAO EQU   4

EE_WR:	MOV   A,DPL
	MOV   R0,DPH
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A		; zdrojova adresa
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	MOV   DPTR,#IIC_BUF
	MOV   A,R4		; adresa v pameti EEPROM
	MOVX  @DPTR,A
	MOV   DPTR,#EE_CNT
	MOV   A,R2		; pocet prenasenych byte
	MOVX  @DPTR,A
	CALL  IIC_WME		; cekat na ukonceni prenosu
EE_WR1:	MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR		; zdrojova adresa
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	%LDR45i(IIC_BUF+1)
	%LDR01i(EE_WRAO)
	CALL  xxMOVE
	MOV   DPTR,#EE_PTR
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
EE_WR2:	%LDR45i(IIC_BUF)
	MOV   R2,#EE_WRAO+1
	MOV   R3,#0
	MOV   R6,#EEP_ADR
	CALL  IIC_RQX
	JNZ   EE_WR2
	CALL  IIC_WME
	JNZ   EE_WR2
	MOV   DPTR,#IIC_BUF
	MOVX  A,@DPTR		; adresa v EEPROM
	ADD   A,#EE_WRAO
	MOVX  @DPTR,A
	MOV   DPTR,#EE_CNT
	MOVX  A,@DPTR		; pocitani prenasenych byte
	ADD   A,#-EE_WRAO-1
	INC   A
	MOVX  @DPTR,A
	JC    EE_WR1
EE_WRR:	RET

)FI

%IF(%WITH_IIC)THEN(

; R45 := [EE_PTR++]
EEA_RDi:MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOV   R0,DPH
	MOV   A,DPL
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	RET

; [EE_PTR++] := R45
EEA_WRi:MOV   DPTR,#EE_PTR
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOV   R0,DPH
	MOV   A,DPL
	MOV   DPTR,#EE_PTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
	RET

; provadi EEA_RD, EEA_WR pro iteger cislo
EEA_Mi:	CJNE  R0,#EEA_RD,EEA_Mi5
	CALL  EEA_RDi
	MOV   DPL,R2
	MOV   DPH,R3
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	RET
EEA_Mi5:CJNE  R0,#EEA_WR,EEA_Mi9
	MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  EEA_WRi
EEA_Mi9:RET

EEA_PRO:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#EE_PTR
	MOV   A,#LOW EE_MEM
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH EE_MEM
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
EEA_PR2:MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	ORL   A,R4
	JZ    EEA_Mi9
	PUSH  DPL
	PUSH  DPH
	MOV   A,R0
	PUSH  ACC
	CALL  JMPR45
	POP   ACC
	MOV   R0,A
	POP   DPH
	POP   DPL
	JMP   EEA_PR2

JMPR45:	MOV   A,R4
	PUSH  ACC
	MOV   A,R5
	PUSH  ACC
	RET

; Nastavi EEP_TAB na R23
EEP_PTS:MOV   DPTR,#EEP_TAB
	MOV   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
	RET

; Nastavi
; R2=[EEP_TAB]   .. delka prenasenych dat
; R4=[EEP_TAB+1] .. pocatecni adresa EEPROM
; DPTR=EE_MEM    .. adresa bufferu pameti
EEP_GM:	MOV   DPTR,#EEP_TAB
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R2
	MOV   DPH,A
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   DPTR,#EE_MEM
	RET

; Ulozit data podle tabulky R23
EEP_WRS:CALL  EEP_PTS
%IF(1)THEN(
EEP_WR1:CALL  I2M_TBF		; Uvolneni po I2M_POOL
	JNZ   EEP_WR1
)FI
	MOV   DPTR,#EEP_TAB
	CALL  xMDPDP
	INC   DPTR
	INC   DPTR	; popis akci
	MOV   R0,#EEA_WR
	CALL  EEA_PRO
	CALL  EEP_GM
	DEC   R2
	CALL  XOR_SUM
	MOVX  @DPTR,A		; kontrolni byte
	CALL  EEP_GM
	JMP   EE_WR

; Nacist data podle tabulky R23
EEP_RDS:CALL  EEP_PTS
%IF(1)THEN(
EEP_RD1:CALL  I2M_TBF		; Uvolneni po I2M_POOL
	JNZ   EEP_RD1
)FI
	CALL  EEP_GM
	CALL  EE_RD
	JNZ   EEP_RD9
	CALL  EEP_GM
	DEC   R2
	CALL  XOR_SUM
	MOV   R3,A
	MOVX  A,@DPTR		; kontrolni byte
	XRL   A,R3
	JNZ   EEP_RD9
	MOV   DPTR,#EEP_TAB
	CALL  xMDPDP
	INC   DPTR
	INC   DPTR	; popis akci
	MOV   R0,#EEA_RD
	JMP   EEA_PRO
EEP_RD9:SETB  F0
	RET

)FI

; *******************************************************************
; Ukladani a cteni nastaveni z EEPROM

%IF(%WITH_IIC)THEN(

; Nacti parametry regulatoru
; R2 cislo regulatoru
EEA_MR:	MOV   A,R2	; cislo regulatoru
	MOV   R1,A
	CJNE  R0,#EEA_RD,EEA_MR5
	MOV   R2,#EEA_MRT-EEA_MR3
EEA_MR1:MOV   A,R2
	INC   R2
	MOVC  A,@A+PC
EEA_MR3:JZ    EEA_MR9
	MOV   R3,A
	CALL  EEA_RDi
	MOV   A,R3
	CALL  MR_GPA1
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	JMP   EEA_MR1
EEA_MR5:CJNE  R0,#EEA_WR,EEA_MR9
	MOV   R2,#EEA_MRT-EEA_MR8
EEA_MR6:MOV   A,R2
	INC   R2
	MOVC  A,@A+PC
EEA_MR8:JZ    EEA_MR9
	CALL  MR_GPA1
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	CALL  EEA_WRi
	JMP   EEA_MR6
EEA_MR9:RET

EEA_MRT:; parametry regulatoru RP_NUM*2B
	DB    OMR_P, OMR_I, OMR_D
	DB    OMR_S1, OMR_S2, OMR_ME, OMR_MS, OMR_ACC
	; dalsi parametry
	DB    OMR_CFG, OMR_SCM, OMR_SCD
	DB    0
)FI
%IF(%WITH_IIC)THEN(
; Tabulky popisu akci pro ulozeni a cteni dat

; Tabulka ukladanych parametru
EEC_SER:DB    040H	; pocet byte - musi by delitelny 16
	DB    010H	; pocatecni adresa v EEPROM

    %IF(0)THEN(
	%W    (COM_TYP)
	%W    (EEA_Mi)

	%W    (COM_ADR)
	%W    (EEA_Mi)

	%W    (COM_SPD)
	%W    (EEA_Mi)

	%W    (0)
	%W    (EEA_MR)
    )FI

    %IF(%REG_COUNT GE 1) THEN(
	%W    (0)		; regulator A
	%W    (EEA_MR)

    %IF(%REG_COUNT GE 2) THEN(
	%W    (1)		; regulator B
	%W    (EEA_MR)

    %IF(%REG_COUNT GE 3) THEN(
	%W    (2)		; regulator C
	%W    (EEA_MR)
    )FI )FI )FI

	%W    (TMC1_OC)		; offset teploty
	%W    (EEA_Mi)

	%W    (TMC1_MC)		; sklon teploty
	%W    (EEA_Mi)

	%W    (TMC1+OMR_P)	; teplota reg P
	%W    (EEA_Mi)

	%W    (TMC1+OMR_I)	; teplota reg I
	%W    (EEA_Mi)

	%W    (TMC1+OMR_D)	; teplota reg D
	%W    (EEA_Mi)

	%W    (TMC2_OC)		; offset teploty 2
	%W    (EEA_Mi)

	%W    (TMC2_MC)		; sklon teploty 2
	%W    (EEA_Mi)

	%W    (TPS_OC)		; offset podtlaku
	%W    (EEA_Mi)

	%W    (TPS_MC)		; sklon podtlaku
	%W    (EEA_Mi)

	%W    (TMC12_DL)	; limit rozdilu teploty 1 a 2
	%W    (EEA_Mi)

	%W    (TPS_HYS)		; hystereze rizeni podtlaku
	%W    (EEA_Mi)

	%W    (TPS_CFG)		; konfigurace rizeni podtlaku
	%W    (EEA_Mi)

	%W    (ADN_TIM)		; doba na odjeti do spodni polohy
	%W    (EEA_Mi)

    %IF(%WITH_TMCCOR)THEN(
	%W    (TMCC_MGT)	; max pripustrny gradient teploty
	%W    (EEA_Mi)

	%W    (TMCC_CET)	; kriticke priblizeni
	%W    (EEA_Mi)
    )FI

	%W    (0)
	%W    (0)

EEC_CF1:DB    010H	; pocet byte - musi by delitelny 16
	DB    080H	; pocatecni adresa v EEPROM

	%W    (REG_A+OREG_A+OMR_RSPD)
	%W    (EEA_Mi)		; otacky

	%W    (TMC1_RT)		; teplota
	%W    (EEA_Mi)

	%W    (TPS_RQP)		; pozadovany podtlak
	%W    (EEA_Mi)

    %IF(%WITH_TIMER)THEN(
	%W    (TIM_STP)		; cas konce odparovani
	%W    (EEA_Mi)
    )FI
	%W    (0)
	%W    (0)

EEC_CF2:DB    010H	; pocet byte - musi by delitelny 16
	DB    0A0H	; pocatecni adresa v EEPROM

	%W    (REG_A+OREG_A+OMR_RSPD)
	%W    (EEA_Mi)		; otacky

	%W    (TMC1_RT)		; teplota
	%W    (EEA_Mi)

	%W    (TPS_RQP)		; pozadovany podtlak
	%W    (EEA_Mi)

    %IF(%WITH_TIMER)THEN(
	%W    (TIM_STP)		; cas konce odparovani
	%W    (EEA_Mi)
    )FI

	%W    (0)
	%W    (0)

MR_EERD:%LDR23i(EEC_SER)
	JMP   EEP_RDS

MR_EEWR:%LDR23i(EEC_SER)
	JMP   EEP_WRS

)FI

; *******************************************************************
; Test pameti ram

%IF(%WITH_RAMT)THEN(
RAMT:	MOV   DPTR,#08000H

RAMT10:	MOV   C,EA
	PUSH  PSW
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#80H
RAMT11:	MOV   A,R5
	RL    A
	MOV   R5,A
	MOVX  @DPTR,A
	CLR   A
	MOVC  A,@A+DPTR
	XRL   A,R5
	JNZ   RAMTERR
	CJNE  R5,#80H,RAMT11
	MOV   A,R4
	MOVX  @DPTR,A
	POP   PSW
	MOV   EA,C

;	MOV   A,#LCD_HOM
;	CALL  LCDWCOM
;	MOV   R4,DPL
;	MOV   R5,DPH
;	CALL  PRINThw

	INC   DPTR
	MOV   A,DPH
	CJNE  A,#HIGH RAMT,RAMT14
	INC   DPH
	INC   DPH
RAMT14:	CJNE  A,#0FFH,RAMT10

	MOV   DPTR,#08000H

RAMT20:	MOV   C,EA
	PUSH  PSW
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#7FH
RAMT21:	MOV   A,R5
	RL    A
	MOV   R5,A
	MOVX  @DPTR,A
	CLR   A
	MOVC  A,@A+DPTR
	XRL   A,R5
	JNZ   RAMTERR
	CJNE  R5,#7FH,RAMT21
	MOV   A,R4
	MOVX  @DPTR,A
	POP   PSW
	MOV   EA,C

;	MOV   A,#LCD_HOM
;	CALL  LCDWCOM
;	MOV   R4,DPL
;	MOV   R5,DPH
;	CALL  PRINThw

	INC   DPTR
	MOV   A,DPH
	CJNE  A,#HIGH RAMT,RAMT24
	INC   DPH
	INC   DPH
RAMT24:	CJNE  A,#0FFH,RAMT20

	RET

RAMTERR:PUSH  ACC
	MOV   A,#LCD_HOM+8
	CALL  LCDWCOM
	POP   ACC
	CALL  PRINThb
	MOV   A,#':'
	CALL  LCDWR
	MOV   R4,DPL
	MOV   R5,DPH
	CALL  PRINThw
RAMEHLD:SJMP  RAMEHLD
)FI

; *******************************************************************

xPR_AD: MOV   A,#' '
	CALL  LCDWR
	MOV   R1,#3
	SJMP  xPRThl1

xPRThl:	MOV   R1,#4
xPRThl1:MOV   A,R1
	DEC   A
	MOVC  A,@A+DPTR
	CALL  PRINThb
	DJNZ  R1,xPRThl1
	RET

DB_W_10:MOV   R0,#10H
	SJMP  DB_WAI1

DB_WAIT:MOV   R0,#0H
DB_WAI1:%WATCHDOG
	DJNZ  R1,DB_WAI1
	DJNZ  R0,DB_WAI1
	RET

L0:	MOV   SP,#80H
	SETB  EA
	JNB   FL_DIPR,L004
	MOV   DPTR,#DEVER_T
	CALL  cPRINT
	MOV   A,#2*25
	CALL  WAIT_T

	MOV   A,#LCD_CLR
	CALL  LCDWCOM
L004:

    %IF (%WITH_RS232) THEN (
	JNB   FL_RS232,L010
	MOV   R7,#BAUDDIV_9600
	CALL  RS232_INI
	SETB  PS
	%LDMXi(RS_OPL,RS_OPL1)
	JNB   FL_DIPR,L010
	MOV   A,#K_PROG
	CALL  TESTKEY
	JNZ   L010
L_IHEX:	CALL  IHEXLD
	SJMP  L_IHEX
    )FI

L010:   CALL  IRC_INIT		; Inicializace CF32006
	CALL  MR_INIS		; Inicializace multi regu
	CALL  TMC_INI		; Inicializace regulace teploty

    %IF(%WITH_UL_DY_EEP) THEN (
	CALL  INI_SERNUM	; Initialize serial number
    )FI

    %IF(%WITH_IIC)THEN(
	%LDR23i(EEC_SER)	; Cteni parametru z EEPROM
	CALL  EEP_RDS
	%LDR23i(EEC_CF1)	; Cteni configurace z EEPROM
	CALL  EEP_RDS
    )FI

%IF(%WITH_IICRVO)THEN(
;	CALL  RVK_TST		; Jednoduchy test
	CALL  RVK_INI		; Inicializace IIC klavesnice
)FI
%IF(%WITH_RAMT)THEN(
	CALL  RAMT		; Kontrola pameti RAM
)FI
%IF(%WITH_IICRVO)THEN(
	JB    FL_DIPR,L090
	CLR   A			; Bez displeje
	MOV   DPTR,#EV_BUF
	MOVX  @DPTR,A
L080:   MOV   DPTR,#EV_BUF
	MOVX  A,@DPTR
	JZ    L085
	MOV   R2,A
	CLR   A
	MOVX  @DPTR,A
	CJNE  R2,#ET_KEY,L082
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
	MOV   DPTR,#RVO_SFT1
	CALL  SEL_FNC
	SJMP  L080
L082:
L085:	CALL  I2M_POOL		; I2C master
	MOV   DPTR,#EV_BUF
	MOVX  A,@DPTR
	JNZ   L080
    %IF(%WITH_ULAN)THEN(
	CALL  UD_OI		; uLan komunikace
    )FI
	JMP   L080
)FI
L090:	MOV   A,#K_DP
	CALL  TESTKEY
	JZ    L1

	JMP   UT

L1:	MOV   A,#LCD_HOM
	CALL  LCDWCOM

	MOV   DPTR,#REG_A+OREG_A+OMR_AP
	CALL  xPR_AD

	MOV   DPTR,#REG_A+OREG_B+OMR_AP
	MOV   DPTR,#REG_A+OREG_A+OMR_RS
	CALL  xPR_AD

	MOV   A,#LCD_HOM+40H
	CALL  LCDWCOM

	MOV   DPTR,#ADC0		; Prevodniky
	CALL  xLDR45i
	CALL  PRINThw

	MOV   DPTR,#ADC6
	CALL  xLDR45i
	CALL  PRINThw

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#ADC7
	CALL  xLDR45i
	CALL  PRINThw
%IF (0) THEN (
	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#REG_A+OREG_A+OMR_FOD
	CALL  xLDR45i
	CALL  PRINThw

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#REG_A+OREG_A+OMR_FOI
	CALL  xLDR45i
	CALL  PRINThw

	MOV   A,#' '
	CALL  LCDWR

	MOV   DPTR,#REG_A+OREG_A+OMR_RP
	CALL  xLDR45i
	CALL  PRINThw
) FI

L2:	CALL  SCANKEY
	JZ    L3
	MOV   R7,A
	MOV   DPTR,#SFT_D1
	CALL  SEL_FNC
L3:

	JMP   L1


T_RDVAL:MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR
	PUSH  ACC
	INC   DPTR
	MOVX  A,@DPTR
	PUSH  ACC
	INC   DPTR
	CALL  cPRINT
	MOV   R6,#8
	MOV   R7,#40H
	CALL  INPUTi
	POP   DPH
	POP   DPL
	JB    F0,T_RDV99
	CALL  xSVR45i
T_RDV99:RET

SFT_D1:	DB    K_END
	%W    (0)
	%W    (L0)

	DB    K_H_C
	%W    (0)
	%W    (TM_GEP)

	DB    K_H_A
	%W    (T_PWM0)
	%W    (T_RDVAL)

	DB    K_H_B
	%W    (T_PWM1)
	%W    (T_RDVAL)

	DB    K_6
	%W    (0)
	%W    (UT)

	DB    K_DP
	%W    (0)
	%W    (MONITOR)

	DB    0

T_PWM0:	%W    (0FFE0H)
	DB    LCD_CLR,'PWM0 :',0

T_PWM1:	%W    (0FFE1H)
	DB    LCD_CLR,'PWM1 :',0

%IF (NOT %FOR_BATH) THEN (
DEVER_T:DB    LCD_CLR,'%VERSION'
	DB    C_LIN2 ,' (c) PiKRON 1997',0
)ELSE(
DEVER_T:DB    LCD_CLR,'%VERSIONB'
	DB    C_LIN2 ,' (c) PiKRON 1997',0
)FI

; *******************************************************************
; RS232 pro MARS

RSEG	RVO___X

TMP_MSPC:
RS_TMP:	DS    4

%IF (%WITH_RS232) THEN (

RSEG	RVO___X
MR_RSAM:DS    1

%*DEFINE (RS_TID(NAME)) (
RST_%NAME:DB '%NAME',0
)

%*DEFINE (RS_TIDSUF(NAME)) (
RST_%(%NAME)_SUF:DB '%NAME#',0
)

%*DEFINE (RSD_NEW (NAME)) (
RSD_P	SET   RSD_T
RSD_T	SET   $
	DB   LOW (RSD_P),HIGH (RSD_P)
	DB   LOW (RST_%NAME),HIGH (RST_%NAME)
)

RSEG	RVO___C

RS_SAMA:CALL  RS_ACKL
RS_SAM:	MOV   A,R6
	ADD   A,#-'A'
	CJNE  A,#REG_N,RS_SAM1
RS_SAM1:JC    RS_SAM2
	SETB  F0
	RET
RS_SAM2:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#MR_RSAM
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
	JMP   RS_DOCM

RSO_VER_T:DB  'VER=%VERSION',0

RSO_VER:MOV   DPTR,#RSO_VER_T
	JMP   RS_WRL1

RSO_TEST_T:DB 'TEST OK',0
RSO_TEST:MOV  DPTR,#RSO_TEST_T
	JMP   RS_WRL1

; Zapina/vypina ohlas READY/FAILED
RS_SRDY:MOV   A,R4
	ADD   A,#-'1'-1
	JNC   RS_SRDY1
	SETB  F0
	RET
RS_SRDY1:ADD  A,#1
	MOV   RSF_RDYR,C
	SETB  RSF_BSYO
	CLR   FL_RDYR1
	RET

; Vysli pouze jedno ready
RS_RDYR1:SETB RSF_RDYR
	SETB  RSF_BSYO
	SETB  FL_RDYR1
	RET

; Zapina/vypina ohlas REPLY
RS_SRPLY:MOV  A,R4
	ADD   A,#-'1'-1
	JNC   RS_SRPL1
	SETB  F0
	RET
RS_SRPL1:ADD  A,#1
	MOV   RSF_RPLY,C
	MOV   RSF_LNT,C
	RET

; Blokovani klavesnice
RS_KEYLOCK:
	MOV   A,R4
	ADD   A,#-'1'-1
	JNC   RS_KEYL1
	SETB  F0
	RET
RS_KEYL1:ADD  A,#1
	MOV   R2,#GL_KEYLOCK
	JC    RS_KEYL2
	MOV   R2,#GL_KEYEN
RS_KEYL2:MOV  R3,#0
	JMP   GLOB_RQ23

; Nastaveni debug modu pro regulator
RS_REGDBG:
	MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	MOV   A,#OMR_FLG
	CALL  MR_GPA1
	MOV   A,R4
	ADD   A,#-'1'
	CLR   EA
	MOVX  A,@DPTR
	MOV   ACC.BMR_DBG,C
	MOVX  @DPTR,A
	SETB  EA
	RET

; Cteni historie
RS_REGDBGH:
	%LDMXi(RS_TMP+2,MRMC_BMB)
RS_DBG2:MOV   DPTR,#RS_TMP
	MOVX  A,@DPTR
	ADD   A,#-1
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#-1
	MOVX  @DPTR,A
	JNC   RS_DBG9
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R2,A
	ADD   A,#2
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	ADDC  A,#0
	MOVX  @DPTR,A
	MOV   DPL,R2
	MOV   DPH,R3
	CALL  xLDR45i
	CALL  L_APTR
	MOV   R2,#0
	MOV   R3,#80H
	CALL  xITOAF
	MOV   A,#0DH
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#0AH
	MOVX  @DPTR,A
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A
	CALL  L_APTR
RS_DBG6:MOVX  A,@DPTR
	JZ    RS_DBG2
	MOV   R0,A
	PUSH  DPL
	PUSH  DPH
	CLR   F0
	CALL  RS_WR
	POP   DPH
	POP   DPL
	JB    F0,RS_DBG6
	INC   DPTR
	SJMP  RS_DBG6
RS_DBG9:CLR   RSF_LNT
	CLR   F0
	RET

)FI
%IF (%WITH_RS232) THEN (

; Priprava dat pro identifikaci
RS_REGDBGP:
	%LDMXi(RS_TMP+2,MRMC_BMB)
RS_DBP2:MOV   DPTR,#RS_TMP
	MOVX  A,@DPTR
	ADD   A,#-1
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#-1
	MOVX  @DPTR,A
	JNC   RS_DBP5
	CLR   C
RS_DBP3:JC    RS_DBP9
	CALL  RS_RDLN
	JZ    RS_DBP3
	MOV   R2,#0
	MOV   R3,#80H
	CALL  xATOIF
	JB    F0,RS_DBP9
	MOV   DPTR,#RS_TMP+2
	MOVX  A,@DPTR
	MOV   R2,A
	ADD   A,#2
	MOV   R0,A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	ADDC  A,#0
	MOVX  @DPTR,A
	XCH   A,R0
	ADD   A,#LOW (-MRMC_EMB)
	MOV   A,R0
	ADDC  A,#HIGH (-MRMC_EMB)
	JC    RS_DBP5
	MOV   DPL,R2
	MOV   DPH,R3
	CALL  xSVR45i
	SJMP  RS_DBP2
RS_DBP5:MOV   DPTR,#RS_TMP+2
	CALL  xMDPDP
	SETB  C
	MOV   A,#LOW MRMC_EMB
	SUBB  A,DPL
	MOV   R2,A
	MOV   A,#HIGH MRMC_EMB
	SUBB  A,DPH
	MOV   R3,A
	JC    RS_DBP8
	INC   R2
	INC   R3
	CLR   A
RS_DBP6:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R2,RS_DBP6
	DJNZ  R3,RS_DBP6
RS_DBP8:CLR   RSF_LNT
	CLR   F0
RS_DBP9:RET

RS_HH:	MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	JMP   GO_HH

RS_CLEAR:MOV  DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	JMP   CLR_GEP

RS_STALL:MOV  R4,MR_FLG
	MOV   R5,#0
	RET

RS_RELEASE_ALL:
	JMP   REL_ALL

RS_RELEASE:
	MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	JMP   REL_GEP

%IF (%MR_REG_SEL) THEN (
RS_REGTYPE:
	MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	JMP   REG_SEL
)FI

)FI

; -----------------------------------
; Listy prikazu

%IF (%WITH_RS232) THEN (

RS_OPL1:DB    ':'
	%W    (RS_CML1)
	DB    '?'
	%W    (RS_CML2)
	DB    0


; -----------------------------------
; Jmena prikazu

%RS_TIDSUF(G)
%RS_TIDSUF(GR)
%RS_TIDSUF(AP)
%RS_TIDSUF(HH)
%RS_TIDSUF(ST)
%RS_TIDSUF(REGP)
%RS_TIDSUF(REGI)
%RS_TIDSUF(REGD)
%RS_TIDSUF(REGS1)
%RS_TIDSUF(REGS2)
%RS_TIDSUF(REGMS)
%RS_TIDSUF(REGACC)
%RS_TIDSUF(REGME)
%RS_TIDSUF(REGCFG)
%RS_TIDSUF(REGDBG)
%RS_TIDSUF(REGTYPE)
%RS_TIDSUF(CLEAR)
%RS_TIDSUF(RELEASE)
%RS_TID(STOP)
%RS_TID(HH)
%RS_TID(ST)
%RS_TID(PURGE)
%RS_TID(CLEAR)
%RS_TID(RELEASE)
%RS_TID(READY)
%RS_TID(R)
%RS_TID(REPLY)
%RS_TID(KEYLOCK)
%RS_TID(REGDBGHIS)
%RS_TID(REGDBGPRE)
%RS_TID(REGDBGGNS)
%RS_TID(REGDBGGNR)
%RS_TID(VER)
%RS_TID(IHEXLD)
%RS_TID(TEST)

)FI

; -----------------------------------
; Vstupni prikazy

%IF (%WITH_RS232) THEN (

RSD_T	SET   0

%RSD_NEW(TEST)
	%W    (RS_CMDA)
	%W    (RSO_TEST)

%RSD_NEW(IHEXLD)
	%W    (RS_CMDA)
	%W    (IHEXLD)

%IF (%MR_REG_SEL) THEN (
%RSD_NEW(REGTYPE_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RS_REGTYPE); Funkce
)FI

%RSD_NEW(REGDBGHIS)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (RS_TMP)	; Adresa
	%W    (RS_REGDBGH)

%RSD_NEW(REGDBGPRE)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (RS_TMP)	; Adresa
	%W    (RS_REGDBGP)

%RSD_NEW(REGDBGGNS)
	%W    (RS_CMDA)
	%W    (GO_GNSALL)

%RSD_NEW(REGDBGGNR)
	%W    (RS_CMDA)
	%W    (GO_GNRALL)

%RSD_NEW(KEYLOCK)
	%W    (RSI_ONOFA)
	%W    (RS_KEYLOCK)

%RSD_NEW(REPLY)
	%W    (RSI_ONOFA)
	%W    (RS_SRPLY)

%RSD_NEW(READY)
	%W    (RSI_ONOFA)
	%W    (RS_SRDY)

%RSD_NEW(R)
	%W    (RS_CMDA)
	%W    (RS_RDYR1)

%RSD_NEW(HH)
	%W    (RS_CMDA)
	%W    (GO_HHT)

%RSD_NEW(PURGE)
	%W    (RS_CMDA)
	%W    (CER_ALL)

%RSD_NEW(STOP)
	%W    (RS_CMDA)
	%W    (STP_ALL)

%RSD_NEW(CLEAR)
	%W    (RS_CMDA)
	%W    (CLR_ALL)

%RSD_NEW(RELEASE)
	%W    (RS_CMDA)
	%W    (RS_RELEASE_ALL)

%RSD_NEW(CLEAR_SUF)
	%W    (RS_SAMA)
	%W    (RS_CLEAR)

%RSD_NEW(RELEASE_SUF)
	%W    (RS_SAMA)
	%W    (RS_RELEASE)

%RSD_NEW(REGDBG_SUF)
	%W    (RS_SAM)
	%W    (RSI_ONOFA)
	%W    (RS_REGDBG)

%RSD_NEW(REGP_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_P)

%RSD_NEW(REGI_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_I)

%RSD_NEW(REGD_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_D)

%RSD_NEW(REGS1_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_S1)

%RSD_NEW(REGS2_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_S2)

%RSD_NEW(REGMS_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_MS)

%RSD_NEW(REGACC_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_ACC)

%RSD_NEW(REGME_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_ME)

%RSD_NEW(REGCFG_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSW_MRCi); Funkce
	DB    (OMR_CFG)

%RSD_NEW(HH_SUF)
	%W    (RS_SAMA)
	%W    (RS_HH)

%RSD_NEW(GR_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,0C3H
	%W    (0)	; Adresa
	%W    (RSW_MRPl); Funkce
	DB    1

%RSD_NEW(G_SUF)
	%W    (RS_SAM)
	%W    (RSI_INTA)
	DB    0,0C3H
	%W    (0)	; Adresa
	%W    (RSW_MRPl); Funkce
	DB    0

RS_CML1	SET   RSD_T

)FI

; -----------------------------------
; Vystupni prikazy

%IF (%WITH_RS232) THEN (

RSD_T	SET   0

%RSD_NEW(TEST)
	%W    (RSO_TEST)

%RSD_NEW(VER)
	%W    (RSO_VER)

%RSD_NEW(REGP_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_P

%RSD_NEW(REGI_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_I

%RSD_NEW(REGD_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_D

%RSD_NEW(REGS1_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_S1

%RSD_NEW(REGS2_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_S2

%RSD_NEW(REGMS_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_MS

%RSD_NEW(REGACC_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_ACC

%RSD_NEW(REGME_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_ME

%RSD_NEW(REGCFG_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRCi); Funkce
	DB    OMR_CFG

%RSD_NEW(ST_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RSR_MRb)	; Funkce
	DB    OMR_FLG

%RSD_NEW(AP_SUF)
	%W    (RS_SAM)
	%W    (RSO_INTE)
	DB    0,0C3H
	%W    (0)	; Adresa
	%W    (RSR_MRPl); Funkce

%RSD_NEW(ST)
	%W    (RSO_INTE)
	DB    0,000H
	%W    (0)	; Adresa
	%W    (RS_STALL); Funkce

RS_CML2	SET   RSD_T

)FI

; *******************************************************************
; Vyblokovano

%IF (%WITH_ULAN) THEN (

RSEG	RVO___B
UD_BBBB:DS    1
UDF_RDP	BIT   UD_BBBB.7

RSEG	RVO___C

%IF (%WITH_UL_OI) THEN (
uL_OINI:%LDR45i (OID_1IN)	; seznam prijimanych prikazu
	%LDR67i (OID_1OUT)	; seznam vysilanych prikazu
	JMP    US_INIT
UD_OI:  CALL   UI_PR
    %IF(%WITH_UL_DY) THEN (
	CALL  UD_RQ		; dynamicaka adresace
    )FI
UD_REFR:RET
)ELSE(
uL_OINI:RET
UD_OI:	RET
UD_REFR:RET
)FI

; Identifikace typu pristroje
PUBLIC	uL_IDB,uL_IDE
%IF (NOT %FOR_BATH) THEN (
uL_IDB: DB    '.mt %VERSION .uP 51x'
    %IF(%WITH_UL_DY) THEN (
	DB    ' .dy'
    )FI
	DB    0
uL_IDE:
)ELSE(
uL_IDB: DB    '.mt %VERSIONB .uP 51x'
    %IF(%WITH_UL_DY) THEN (
	DB    ' .dy'
    )FI
	DB    0
uL_IDE:
)FI

)FI

; *******************************************************************
; Prikazy pres uLan

%IF (%WITH_UL_OI) THEN (

; Pomocne prikazy

G_ATOMi:CALL  xMDPDP
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   EA,C
	RET

; Kodovani prikazu

%OID_ADES(AI_STATUS,STATUS,u2)
%OID_ADES(AI_ERRCLR,ERRCLR,e)
I_TEMP1	  EQU   301
%OID_ADES(AI_TEMP1,TEMP1,u2/.1)
I_TEMP2	  EQU   302
%OID_ADES(AI_TEMP2,TEMP2,u2/.1)
I_TEMP1RQ EQU   311
%OID_ADES(AI_TEMP1RQ,TEMP1RQ,u2/.1)
I_TEMP_OFF EQU  334
%OID_ADES(AI_TEMP_OFF,TEMP_OFF,e)
I_TEMP_ON EQU   335
%OID_ADES(AI_TEMP_ON,TEMP_ON,e)
I_TEMP_ST EQU	336
%OID_ADES(AI_TEMP_ST,TEMP_ST,e)
I_TEMP_ENE EQU	337
%OID_ADES(AI_TEMP_ENE,TEMP_ENE,s2)
I_TEMP_GT EQU	338
%OID_ADES(AI_TEMP_GT,TEMP_GT,s2/.2)
I_TEMP_TST EQU	339
%OID_ADES(AI_TEMP_TST,TEMP_TST,s2)
I_TEMP1MC EQU   351
%OID_ADES(AI_TEMP1MC,TEMP1MC,s2/.2)
I_TEMP2MC EQU   352
%OID_ADES(AI_TEMP2MC,TEMP2MC,s2/.2)
I_TEMP1OC EQU   361
%OID_ADES(AI_TEMP1OC,TEMP1OC,s2/.2)
I_TEMP2OC EQU   362
%OID_ADES(AI_TEMP2OC,TEMP2OC,s2/.2)
I_TEMP1RD EQU   371
%OID_ADES(AI_TEMP1RD,TEMP1RD,u2/.2)
I_TEMP2RD EQU   372
%OID_ADES(AI_TEMP2RD,TEMP2RD,u2/.2)

%IF (NOT %FOR_BATH) THEN (
I_VAC_AP EQU	401
%OID_ADES(AI_VAC_AP,VAC_PRESS,s2)
;I_VAC_RP EQU	402
;OID_ADES(AI_VAC_RP,VAC_PRESS_RQ,s2)
)FI

I_SAVECFG EQU   451
%OID_ADES(AI_SAVECFG,SAVECFG,e)
I_DEFAULTCFG EQU 452
%OID_ADES(AI_DEFAULTCFG,DEFAULTCFG,e)
I_SERVLEV EQU   458
%OID_ADES(AI_SERVLEV,SERVLEV,u2)

; Prijimane hodnoty

OID_T	SET   $
	%W    (I_TEMP_OFF)
	%W    (OID_ISTD)
	%W    (AI_TEMP_OFF)
	%W    (TMC_OFF)

%OID_NEW(I_TEMP_ON,AI_TEMP_ON)
	%W    (TMC_ON)

%OID_NEW(I_SAVECFG ,AI_SAVECFG)
	%W    (SAVECFG_U)

%OID_NEW(I_TEMP1RD,AI_TEMP1RD)
	%W    (UI_INT)
	%W    (TMC1_RD)
	%W    (0)

%OID_NEW(I_TEMP2RD,AI_TEMP2RD)
	%W    (UI_INT)
	%W    (TMC2_RD)
	%W    (0)

%OID_NEW(I_TEMP1MC,AI_TEMP1MC)
	%W    (UI_INT)
	%W    (TMC1_MC)
	%W    (0)

%OID_NEW(I_TEMP2MC,AI_TEMP2MC)
	%W    (UI_INT)
	%W    (TMC2_MC)
	%W    (0)

%OID_NEW(I_TEMP1OC,AI_TEMP1OC)
	%W    (UI_INT)
	%W    (TMC1_OC)
	%W    (0)

%OID_NEW(I_TEMP2OC,AI_TEMP2OC)
	%W    (UI_INT)
	%W    (TMC2_OC)
	%W    (0)

%OID_NEW(I_TEMP1,AI_TEMP1)
	%W    (UI_INT)
	%W    (0)
	%W    (S_TEMP)
	%W    (TMC1_RT)

%OID_NEW(I_TEMP1RQ,AI_TEMP1RQ)
	%W    (UI_INT)
	%W    (0)
	%W    (S_TEMP)
	%W    (TMC1_RT)

%IF (NOT %FOR_BATH) THEN (

)FI

OID_1IN SET   OID_T

)FI
%IF (%WITH_ULAN) THEN (
; Vysilane hodnoty

OID_T	SET   0

%OID_NEW(I_STATUS,AI_STATUS)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TMC_ST)

%OID_NEW(I_TEMP1RD,AI_TEMP1RD)
	%W    (UO_INT)
	%W    (TMC1_RD)
	%W    (0)

%OID_NEW(I_TEMP2RD,AI_TEMP2RD)
	%W    (UO_INT)
	%W    (TMC2_RD)
	%W    (0)

%OID_NEW(I_TEMP1MC,AI_TEMP1MC)
	%W    (UO_INT)
	%W    (TMC1_MC)
	%W    (0)

%OID_NEW(I_TEMP2MC,AI_TEMP2MC)
	%W    (UO_INT)
	%W    (TMC2_MC)
	%W    (0)

%OID_NEW(I_TEMP1OC,AI_TEMP1OC)
	%W    (UO_INT)
	%W    (TMC1_OC)
	%W    (0)

%OID_NEW(I_TEMP2OC,AI_TEMP2OC)
	%W    (UO_INT)
	%W    (TMC2_OC)
	%W    (0)

%OID_NEW(I_TEMP1,AI_TEMP1)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TEMP)
	%W    (TMC1_AT)

%OID_NEW(I_TEMP2,AI_TEMP2)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TEMP)
	%W    (TMC2_AT)

%OID_NEW(I_TEMP_ENE,AI_TEMP_ENE)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMC1_EN)

%IF(%WITH_TMCCOR)THEN(
%OID_NEW(I_TEMP_GT,AI_TEMP_GT)
	%W    (UO_INT)
	%W    (0)
	%W    (G_ATOMi)
	%W    (TMCC_GT)
)FI

%OID_NEW(I_TEMP_TST,AI_TEMP_TST)
	%W    (UO_INT)
	%W    (TMC1+OMR_FOI)
	;W    (TMCC_CNT)
	%W    (0)

%IF (NOT %FOR_BATH) THEN (
%OID_NEW(I_VAC_AP,AI_VAC_AP)
	%W    (UO_INT)
	%W    (0)
	%W    (G_TEMP)
	%W    (TPS_AP)
)FI

OID_1OUT SET  OID_T

)FI
%IF (%WITH_ULAN) THEN (
SAVECFG_U:%LDR23i(EEC_SER); Ulozeni parametru do EEPROM
	JMP   EEP_WRS
)FI

; *******************************************************************
; Propojeni s regulatory

RSEG	RVO___X

MR_UIAM:DS    2

RSEG	RVO___C

; Deli R7B registrem R3, pak posune R4567B
SCL_S:	MOV   R0,#1
	CLR   A
	XCH   A,R3
	MOV   R2,A
	CLR   C
SCL_S1:	MOV   A,R2	; R32 >>= 1
	RRC   A
	MOV   R2,A
	MOV   A,R3
	RRC   A
	MOV   R3,A
	MOV   A,R7	; R7B - R32 ?
	SUBB  A,R3
	PUSH  ACC
	MOV   A,B
	SUBB  A,R2
	JC    SCL_S3
	MOV   B,A	; R7B > R32
	POP   ACC
	MOV   R7,A
	MOV   A,R0	; R0 = (R0 << 1) | \0
	RLC   A
	MOV   R0,A
	JNC   SCL_S1
	SJMP  SCL_S4
SCL_S3: DEC   SP	; R7B < R32
	MOV   A,R0	; R0 = (R0 << 1) | \1
	RLC   A
	MOV   R0,A
	JNC   SCL_S1
SCL_S4: CPL   A
	XCH   A,R4
	XCH   A,R5
	XCH   A,R6
	XCH   A,R7
	XCH   A,B
SCL_S9:	RET

; Prevod meritka souradnic pro R1
; pro R0=0 .. z motoru, R0=1 .. na motor
SCL_MR:	CLR   A
	CALL  MR_GPA1
	MOV   A,#OMR_SCM+1
	MOVC  A,@A+DPTR
	MOV   B,A
	MOV   A,#OMR_SCM
	MOVC  A,@A+DPTR
	JNB   B.7,SCL_MR1
	CPL   A
	INC   A
SCL_MR1:JZ    SCL_S9
	MOV   R2,A
	MOV   A,#OMR_SCD+1
	MOVC  A,@A+DPTR
	XRL   B,A
	MOV   C,ACC.7
	MOV   A,#OMR_SCD
	MOVC  A,@A+DPTR
	JNC   SCL_MR2
	CPL   A
	INC   A
SCL_MR2:JZ    SCL_S9
	MOV   R3,A
	DJNZ  R0,SCL_MR3
	XCH   A,R2
	MOV   R3,A
SCL_MR3:JNB   B.7,SCL_MR4
	CLR   A
	CLR   C
	SUBB  A,R4
	MOV   R4,A
        CLR   A
	SUBB  A,R5
	MOV   R5,A
	CLR   A
	SUBB  A,R6
	MOV   R6,A
	CLR   A
	SUBB  A,R7
	MOV   R7,A
; operace R4567*R2/R3
SCL_MR4:MOV   B,R2
	MOV   A,R4
	MUL   AB
	MOV   R4,A
	MOV   A,R2
	XCH   A,B
	XCH   A,R5
	MUL   AB
	ADD   A,R5
	MOV   R5,A
	MOV   A,R2
	XCH   A,B
	ADDC  A,#0
	XCH   A,R6
	MUL   AB
	ADD   A,R6
	MOV   R6,A
	MOV   A,R2
	XCH   A,B
	ADDC  A,#0
	XCH   A,R7
	JB    ACC.7,SCL_MR5
	MOV   R2,#0	; Bez korekce pro kladne cislo
SCL_MR5:MUL   AB
	ADD   A,R7
	MOV   R7,A
	MOV   A,B
	ADDC  A,#0
	SUBB  A,R2
	JNC   SCL_MR61
	ADD   A,R3	; Zaporne cislo
	MOV   B,A
	CALL  SCL_S
	JNZ   SCL_MR8
	MOV   A,R4
	JB    ACC.7,SCL_MR63
	SJMP  SCL_MR8
SCL_MR61:MOV  B,A	; Kladne cislo
	CALL  SCL_S
	JNZ   SCL_MR8
	MOV   A,R4
	JB    ACC.7,SCL_MR8
SCL_MR63:CALL SCL_S
	CALL  SCL_S
	CALL  SCL_S
	MOV   A,B
	CLR   C
	RLC   A
	JC    SCL_MR65
	SUBB  A,R3
	JC    SCL_MR9
SCL_MR65:INC  R4
	MOV   A,R4
	JNZ   SCL_MR9
	INC   R5
	MOV   A,R5
	JNZ   SCL_MR9
	INC   R6
	MOV   A,R6
	JNZ   SCL_MR9
	INC   R7
	MOV   A,R7
	CJNE  A,#80H,SCL_MR9
SCL_MR8:SETB  F0
SCL_MR9:RET


%IF (%WITH_RS232) THEN (
RSR_MRPl:MOV  DPTR,#MR_RSAM
	SJMP  UR_MRPl1
)FI

; Nacte do R4567 aktualni polohu z regulatoru [DPTR+OU_DP]
UR_MRPl:MOV   A,#OU_DP
	MOVC  A,@A+DPTR
	CJNE  A,#-1,UR_MRPl2
	MOV   DPTR,#MR_UIAM
UR_MRPl1:MOVX A,@DPTR
UR_MRPl2:MOV  R1,A
	MOV   A,#OMR_AP
	CALL  MR_GPA1
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	SETB  EA
	MOVX  A,@DPTR
	MOV   R6,A
	MOV   R7,#0
	JNB   ACC.7,UR_MRPl5
	DEC   R7
UR_MRPl5:CLR  F0
	MOV   R0,#0		; z motoru na logicke souradnice
	CALL  SCL_MR		; prevod meritka
	MOV   A,#1
	RET

%IF (%WITH_RS232) THEN (
RSW_MRPl:MOVX A,@DPTR
	MOV   R3,A		; typ pohybu (absolutne/relativne)
	MOV   DPTR,#MR_RSAM
	SJMP  UW_MRPl1
)FI

; Spusti pohyb regulatoru [DPTR+OU_DP] na polohu R4567
UW_MRPl:MOV   A,#OU_DP+1
	MOVC  A,@A+DPTR         ; typ pohybu (absolutne/relativne)
	MOV   R3,A
	MOV   A,#OU_DP
	MOVC  A,@A+DPTR         ; cislo regulatoru
	CJNE  A,#-1,UW_MRPl2
	MOV   DPTR,#MR_UIAM
UW_MRPl1:MOVX A,@DPTR
UW_MRPl2:MOV  R1,A
%IF(1) THEN (
	CLR   F0
	MOV   R0,#1		; z logickych na motor souradnice
	MOV   A,R3
	PUSH  ACC
	CALL  SCL_MR		; prevod meritka
	POP   ACC
	MOV   R3,A
	JB    F0,UW_MRPl5
	MOV   A,R6
	MOV   C,ACC.7
	XCH   A,R7
	ADDC  A,#0
	JNZ   UW_MRPl5
	MOV   A,R5
	MOV   R6,A
	MOV   A,R4
	MOV   R5,A
	MOV   R4,#0
	CALL  GO_GEPT
	RET
UW_MRPl5:SETB F0
	RET
)ELSE(
	MOV   A,R5
	MOV   R7,A
	MOV   A,R4
	MOV   R6,A
	CLR   A
	MOV   R4,A
	MOV   R5,A
	CLR   F0
	CALL  GO_GEPT
	RET
)FI

%IF (%WITH_RS232) THEN (
; Vypocte adresu parametru regulatoru
; [DPTR]    .. offset parametru
; [MR_RSAM] .. urceni regulatoru
MR_RSCA:MOVX  A,@DPTR
	MOV   R0,A
	MOV   DPTR,#MR_RSAM
	MOVX  A,@DPTR
	MOV   R1,A
	MOV   A,R0
	JMP   MR_GPA1
)FI

; Vypocte adresu parametru regulatoru
; [DPTR+OU_DP+0] .. cislo regulatoru nebo -1 pro reg [MR_UIAM]
; [DPTR+OU_DP+1] .. offset parametru regulatoru
MR_UICA:MOV   A,#OU_DP+1
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OU_DP
	MOVC  A,@A+DPTR
	CJNE  A,#-1,MR_UIC4
	MOV   DPTR,#MR_UIAM
	MOVX  A,@DPTR
MR_UIC4:MOV   R1,A
	MOV   A,R0
	JMP   MR_GPA1

%IF (%WITH_RS232) THEN (
RSR_MRCi:CALL MR_RSCA
	SJMP  UR_MRCi1
)FI

UR_MRCi:CALL  MR_UICA
UR_MRCi1:MOVX A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   A,#1
	CLR   F0
	RET

%IF (%WITH_RS232) THEN (
RSW_MRCi:CALL MR_RSCA
	SJMP  UW_MRCi1
)FI

UW_MRCi:CALL  MR_UICA
UW_MRCi1:MOV  C,EA
	MOV   A,R4
	CLR   EA
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOV   EA,C
	MOVX  @DPTR,A
	CLR   F0
	RET

UW_DIMi:MOV   A,#OU_DP
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OU_DP+1
	MOVC  A,@A+DPTR
	MOV   DPL,R0
	MOV   DPH,A
	SJMP  UW_MRCi1

; Zmena rychlosti [DPTR+OU_DP] na hodnotu R45
UW_MRSPi:MOV  A,#OU_DP
	MOVC  A,@A+DPTR         ; cislo regulatoru
	CJNE  A,#-1,UW_MRSPi2
	MOV   DPTR,#MR_UIAM
UW_MRSPi1:MOVX A,@DPTR
UW_MRSPi2:MOV  R1,A
	JMP   GO_GSP

%IF (%WITH_RS232) THEN (
RSR_MRb:CALL  MR_RSCA
	SJMP  UR_MRb1
)FI

UR_MRb:	CALL  MR_UICA
UR_MRb1:MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#0
	RET

%IF (%WITH_RS232) THEN (
RSW_MRb:CALL  MR_RSCA
	SJMP  UW_MRb1
)FI

UW_MRb:	CALL MR_UICA
UW_MRb1:MOV   A,R4
	MOVX  @DPTR,A
	RET

UW_Mb:	MOV   A,#OU_DP
	MOVC  A,@A+DPTR
	MOV   R0,A
	MOV   A,#OU_DP+1
	MOVC  A,@A+DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOV   A,R4
	MOVX  @DPTR,A
	CLR   F0
	RET

STP_ALL:MOV   R1,#0
STP_ALL1:CALL STP_GEP
	INC   R1
	MOV   A,R1
	CJNE  A,#REG_N,STP_ALL1
	RET

CLR_ALL:MOV   R1,#0
CLR_ALL1:CALL CLR_GEP
	INC   R1
	MOV   A,R1
	CJNE  A,#REG_N,CLR_ALL1
	CLR   MR_FLG.BMR_ERR
	RET

CER_ALL:MOV   R1,#0
CER_ALL1:CALL CER_GEP
	INC   R1
	MOV   A,R1
	CJNE  A,#REG_N,CER_ALL1
	CLR   MR_FLG.BMR_ERR
	RET

; Odpojeni generatoru a regulatoru
REL_ALL:MOV   R1,#0
REL_ALL1:CALL REL_GEP
	INC   R1
	MOV   A,R1
	CJNE  A,#REG_N,REL_ALL1
	CLR   MR_FLG.BMR_ERR
	RET

; Cte hodnotu ze SDATA
UR_MSPCb:MOV  A,#OU_DP
	MOVC  A,@A+DPTR
	MOV   R1,A
	MOV   DPTR,#TMP_MSPC
	MOV   A,#0E5H         ; MOV  A,dir
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#022H         ; RET
	MOVX  @DPTR,A
	DB    12H	      ; CALL TMP_MSPC
	DW    TMP_MSPC
	MOV   R4,A
	MOV   R5,#0
	CLR   F0
	MOV   A,#1
	RET

; Primy zapis PWM
UW_PWMi:CALL  MR_UICA
	JMP   MR_SENEP

; *******************************************************************
; Globalni udalosti

; Posle globalni udalost
GLOB_RQ23:MOV A,R2
	MOV   R4,A
	MOV   A,R3
	MOV   R5,A
	MOV   R7,#ET_GLOB
	JMP   EV_POST

; Zpracovani globalnich udalosti
GLOB_DO:MOV   A,R4
	MOV   R7,A
	MOV   A,R5
	MOV   R4,A
	MOV   DPTR,#GLOB_SF1
	JMP   SEL_FNC

; Globalni udalosti

GL_KEYLOCK  SET  1
GL_KEYEN    SET  2

; Tabulka globalnich udalosti

GLOB_SF1:DB   GL_KEYLOCK
	%W    (UT_GR30)
	%W    (GR_RQ23)

	DB    GL_KEYEN
	%W    (UT_GR10)
	%W    (GR_RQ23)

	DB    0

; *******************************************************************
; Komunikace s uzivatelem

RSEG	RVO___X

UT_UIAD:DS    40
UT_DATA:DS    40

RSEG	RVO___C

UT_INIT:CLR   D4LINE
	SETB  FL_CMAV
	MOV   DPTR,#UI_MV_SX
	MOV   A,#20
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#2
	MOVX  @DPTR,A
	MOV   DPTR,#UT_UIAD
	MOV   UI_AD,DPL
	MOV   UI_AD+1,DPH
	CLR   A
	MOV   DPTR,#EV_BUF
	MOVX  @DPTR,A
	MOV   DPTR,#GR_ACT
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#REF_TIM
	MOVX  @DPTR,A
	RET

UT_TREF:MOV   DPTR,#REF_TIM
	MOVX  A,@DPTR
	JNZ   UT_TRE1
	MOV   DPTR,#REF_PER
	MOVX  A,@DPTR
	MOV   DPTR,#REF_TIM
	MOVX  @DPTR,A
	MOV   A,#1
	RET
UT_TRE1:CLR   A
	RET

UT:     CALL  UT_INIT
	MOV   R7,#ET_RQGR
	%LDR45i(UT_GR10)
	CALL  EV_POST

UT_ML:	CALL  EV_GET
	JZ    UT_ML50
	CJNE  R7,#ET_GLOB,UT_ML45
	CALL  GLOB_DO
	SJMP  UT_ML
UT_ML45:CALL  EV_DO
	JMP   UT_ML
UT_ML50:
    %IF (%WITH_RS232) THEN (
	JNB   FL_RS232,UT_ML53
	CALL  RS_POOL		; Smycka zpracovani prikazu RS232
	JNZ   UT_ML55
	MOV   A,MR_FLG
	MOV   C,ACC.BMR_ERR
	ANL   A,#MMR_BSY
	MOV   ACC.7,C
	CALL  RS_RDYSND		; Vysila R! a FAILED! po RS232
	JZ    UT_ML55
	JNB   FL_RDYR1,UT_ML55
	CLR   RSF_RDYR		; Nepokracuj v informaci o stavu
	SJMP  UT_ML55
    )FI
UT_ML53:
    %IF(%WITH_ULAN)THEN(
	CALL  UD_OI
	JB    uLF_INE,UT_ML55
	JB    UDF_RDP,UT_ML57
    )FI
UT_ML55:CALL  UT_TREF
	JZ    UT_ML60
    %IF(%WITH_ULAN)THEN(
	JB    FL_RS232,UT_ML57
	CALL  UD_REFR
UT_ML57:CLR   UDF_RDP
    )FI
	SETB  FL_REFR
	SJMP  UT_ML65
UT_ML60:CALL  UT_ULED
    %IF(%WITH_IIC)THEN(
	CALL  I2M_POOL
    )FI
UT_ML65:JMP   UT_ML

UT_ULED:
;       MOV   DPTR,#IP1_ST+1
;	MOV   R2,#1 SHL LFB_PUMP1
;	CALL  UT_USTS	; CALL  UT_UST1

	MOV   A,MR_FLG
	MOV   R2,#20H
	CALL  UT_MRST
%IF (0) THEN (
	MOV   DPTR,#REG_A+OREG_A+OMR_FLG
	MOVX  A,@DPTR
	MOV   R2,#G_A_LMSK
	CALL  UT_MRST
%IF(%REG_COUNT GE 2) THEN(
	MOV   DPTR,#REG_A+OREG_B+OMR_FLG
	MOVX  A,@DPTR
	MOV   R2,#G_B_LMSK
	CALL  UT_MRST
%IF(%REG_COUNT GE 3) THEN(
	MOV   DPTR,#REG_A+OREG_C+OMR_FLG
	MOVX  A,@DPTR
	MOV   R2,#G_C_LMSK
	CALL  UT_MRST
)FI )FI )FI
	JMP   LEDWR

UT_MRST:JB    ACC.BMR_ERR,UT_UST7
	JB    ACC.BMR_BSY,UT_UST3
	SJMP  UT_UST5

UT_USTS:MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
UT_UST1:MOV   A,R5
	JB    ACC.7,UT_UST7
	ORL   A,R4
UT_UST2:JZ    UT_UST5
UT_UST3:MOV   A,R2		; On
	CPL   A
	ANL   LEB_FLG,A
	CPL   A
	ORL   LED_FLG,A
	RET
UT_UST5:MOV   A,R2		; Off
	CPL   A
	ANL   LEB_FLG,A
	ANL   LED_FLG,A
	RET
UT_UST7:MOV   A,R2		; Error
	ORL   LEB_FLG,A
	RET

; Nastavi MR_UIAM na hodnotu v R2
UT_SAMG:%LDR45i(UT_GR20)
	MOV   R7,#ET_RQGR
	CALL  EV_POST
UT_SAM:	MOV   A,R2
	MOV   DPTR,#MR_UIAM
	MOVX  @DPTR,A
	RET

; ---------------------------------
; Definice klaves

UT_SF1:	DB    K_H_A
	DB    0,0
	%W    (UT_SAMG)

%IF(%REG_COUNT GE 2) THEN(
	DB    K_H_B
	DB    1,0
	%W    (UT_SAMG)

%IF(%REG_COUNT GE 3) THEN(
	DB    K_H_C
	DB    2,0
	%W    (UT_SAMG)

%IF(%REG_COUNT GE 4) THEN(
	DB    K_H_D
	DB    3,0
	%W    (UT_SAMG)

%IF(%REG_COUNT GE 5) THEN(
	DB    K_H_E
	DB    4,0
	%W    (UT_SAMG)

%IF(%REG_COUNT GE 6) THEN(
	DB    K_H_F
	DB    5,0
	%W    (UT_SAMG)
)FI )FI )FI )FI )FI

	DB    K_LIST
	%W    (UT_GR10)
	%W    (GR_RQ23)

	DB    K_PURGE
	%W    (0)
	%W    (CER_ALL)

	DB    K_STOP
	%W    (0)
	%W    (STP_ALL)

	DB    K_END
	%W    (0)
	%W    (CLR_ALL)

	DB    K_HOLD
	%W    (0)
	%W    (GO_HHT)

	DB    K_PROG
	%W    (0)
    %IF (%WITH_RS232) THEN (
	%W    (IHEXLD)
    )ELSE(
	%W    (MONITOR)
    )FI

UT_SFTN:DB    K_RIGHT
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    K_LEFT
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

	DB    K_DOWN
	DB    ET_CNAV,ETC_NXT
	%W    (EV_PO23)

	DB    K_UP
	DB    ET_CNAV,ETC_PRE
	%W    (EV_PO23)

%IF(%WITH_IICRVO)THEN(
RVO_SFT1:
    %IF (NOT %FOR_BATH) THEN (
	DB    RVKL_DOWN
	%W    (0)
	%W    (HR_DOWN)

	DB    RVKL_DOWN OR 80H
	%W    (0)
	%W    (HR_STOP)

	DB    RVKL_UP
	%W    (0)
	%W    (HR_UP)

	DB    RVKL_UP OR 80H
	%W    (0)
	%W    (HR_STOP)

	DB    RVKL_ROT
	%W    (0)
	%W    (RVO_FFROT)

    )ELSE(
      %IF(%WITH_TIMER)THEN(
	DB    RVKL_STRT
	%W    (0)
	%W    (RVO_FFTLAP_TMC)
      )FI
    )FI
    %IF  (NOT %FOR_BATH OR %WITH_TIMER) THEN (
	DB    RVKL_MODE
	%W    (0)
	%W    (RVK_NEXT)
    )FI
	DB    RVKL_TEMP
	%W    (0)
	%W    (TMC_ONOFF)

	DB    RVKL_MORE
	DB    RVKL_MORE,0
	%W    (RVK_EDIT)

	DB    RVKL_LESS
	DB    RVKL_LESS,0
	%W    (RVK_EDIT)

	DB    RVKL_MORE OR 80H
	DB    0,0
	%W    (RVK_EDIT)

	DB    RVKL_LESS OR 80H
	DB    0,0
	%W    (RVK_EDIT)

	DB    RVKL_12
	%W    (0)
	%W    (RV_CNFP)

	DB    RVKL_12 OR 80H
	%W    (0)
	%W    (RV_CNFR)

)FI

UT_SF0:	DB    0

; ---------------------------------
; Zakladni display
UT_GR10:DS    UT_GR10+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR10+OGR_BTXT-$
	%W    (UT_GT10)
	DS    UT_GR10+OGR_STXT-$
	%W    (0)
	DS    UT_GR10+OGR_HLP-$
	%W    (0)
	DS    UT_GR10+OGR_SFT-$
	%W    (UT_SF10)
	DS    UT_GR10+OGR_PU-$
	%W    (UT_U1001)
	%W    (UT_U1020)
	%W    (UT_U1021)
	%W    (UT_U1022)
	%W    (UT_U1023)
	%W    (UT_U1027)
	%W    (UT_U1028)
	%W    (UT_U1029)
	%W    (UT_U1030)
	%W    (UT_U1031)
	%W    (UT_U1032)
	%W    (UT_U1033)
	%W    (UT_U1034)
    %IF(%WITH_TMCCOR)THEN(
	%W    (UT_U1035)
	%W    (UT_U1036)
    )FI
	%W    (UT_U1040)
	%W    (UT_U1041)
	%W    (UT_U1042)
	%W    (UT_U1043)
	%W    (UT_U1044)
	%W    (UT_U1045)
	%W    (UT_U1046)
	%W    (UT_U1050)
    %IF(%WITH_IIC)THEN(
	%W    (UT_U1090)
    )FI
	%W    (0)

UT_GT10:DB    'RPM',C_NL
	DB    'TEMP',C_NL
	DB    'T1ADC',C_NL
	DB    'T1OFS',C_NL
	DB    'T1SLP',C_NL
	DB    'T P',C_NL
	DB    'T I',C_NL
	DB    'T D',C_NL
	DB    'TEMP2',C_NL
	DB    'T2ADC',C_NL
	DB    'T2OFS',C_NL
	DB    'T2SLP',C_NL
	DB    'T12DL',C_NL
	DB    'T1MGT  na',C_NL
	DB    'T1CET  na',C_NL
	DB    'Press',C_NL
	DB    'P_ADC',C_NL
	DB    'P_OFS',C_NL
	DB    'P_SLP',C_NL
	DB    'P_RQP',C_NL
	DB    'P_HYS',C_NL
	DB    'P_CFG',C_NL
	DB    'ADN_T',0

UT_SF10:
	DB    K_START
	DB    0,0
	%W    (RVO_STROT)

	DB    -1
	%W    (UT_SF1)

UT_U1001: ; Rychlost
	DS    UT_U1001+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1001+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1001+OU_X-$
	DB    6,0,6,1
	DS    UT_U1001+OU_HLP-$
	%W    (0)
	DS    UT_U1001+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1001+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_RSPD)		; A_WR
	DS    UT_U1001+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    0,OMR_RSPD	; DP
	DS    UT_U1001+OU_I_F-$
	DB    80H		; format I_F
	%W    (-%MAX_RSPD)	; I_L
	%W    ( %MAX_RSPD)	; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1020: ; Teplota
	DS    UT_U1020+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1020+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1020+OU_X-$
	DB    6,1,6,1
	DS    UT_U1020+OU_HLP-$
	%W    (0)
	DS    UT_U1020+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1020+OU_A_RD-$
	%W    (UR_TEMP)		; A_RD
	%W    (UW_TEMP)		; A_WR
	DS    UT_U1020+OU_DPSI-$
	%W    (TMC1_RT)		; DPSI
	%W    (TMC1_AT)		; DP
	DS    UT_U1020+OU_I_F-$
	DB    81H		; format I_F
	%W    (0)		; I_L
	%W    (%MAX_TMC1_RT)	; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1021: ; Teplota TMC1 ADC
	DS    UT_U1021+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1021+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1021+OU_X-$
	DB    6,2,6,1
	DS    UT_U1021+OU_HLP-$
	%W    (0)
	DS    UT_U1021+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1021+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1021+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TMC1_RD)		; DP
	DS    UT_U1021+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1022: ; Teplota TMC1 offset
	DS    UT_U1022+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1022+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1022+OU_X-$
	DB    6,3,6,1
	DS    UT_U1022+OU_HLP-$
	%W    (0)
	DS    UT_U1022+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1022+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1022+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TMC1_OC)		; DP
	DS    UT_U1022+OU_I_F-$
	DB    82H		; format I_F
	%W    (-30000)		; I_L
	%W    (30000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1023: ; Teplota TMC1 skoln
	DS    UT_U1023+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1023+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1023+OU_X-$
	DB    6,4,6,1
	DS    UT_U1023+OU_HLP-$
	%W    (0)
	DS    UT_U1023+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1023+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1023+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TMC1_MC)		; DP
	DS    UT_U1023+OU_I_F-$
	DB    82H		; format I_F
	%W    (-30000)		; I_L
	%W    (30000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1027: ; Teplota P
	DS    UT_U1027+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1027+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1027+OU_X-$
	DB    6,5,6,1
	DS    UT_U1027+OU_HLP-$
	%W    (0)
	DS    UT_U1027+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1027+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1027+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TMC1+OMR_P)	; DP
	DS    UT_U1027+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1028: ; Teplota I
	DS    UT_U1028+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1028+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1028+OU_X-$
	DB    6,6,6,1
	DS    UT_U1028+OU_HLP-$
	%W    (0)
	DS    UT_U1028+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1028+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1028+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TMC1+OMR_I)	; DP
	DS    UT_U1028+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1029: ; Teplota D
	DS    UT_U1029+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1029+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1029+OU_X-$
	DB    6,7,6,1
	DS    UT_U1029+OU_HLP-$
	%W    (0)
	DS    UT_U1029+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1029+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1029+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TMC1+OMR_D)	; DP
	DS    UT_U1029+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1030: ; Teplota TMC2
	DS    UT_U1030+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1030+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1030+OU_X-$
	DB    6,8,6,1
	DS    UT_U1030+OU_HLP-$
	%W    (0)
	DS    UT_U1030+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1030+OU_A_RD-$
	%W    (UR_TEMP)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1030+OU_DPSI-$
	%W    (0)		; DPSI
	%W    (TMC2_AT)		; DP
	DS    UT_U1030+OU_I_F-$
	DB    81H		; format I_F
	%W    (0)		; I_L
	%W    (1800)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1031: ; Teplota TMC2 ADC
	DS    UT_U1031+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1031+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1031+OU_X-$
	DB    6,9,6,1
	DS    UT_U1031+OU_HLP-$
	%W    (0)
	DS    UT_U1031+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1031+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1031+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TMC2_RD)		; DP
	DS    UT_U1031+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1032: ; Teplota TMC2 offset
	DS    UT_U1032+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1032+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1032+OU_X-$
	DB    6,10,6,1
	DS    UT_U1032+OU_HLP-$
	%W    (0)
	DS    UT_U1032+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1032+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1032+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TMC2_OC)		; DP
	DS    UT_U1032+OU_I_F-$
	DB    82H		; format I_F
	%W    (-30000)		; I_L
	%W    (30000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1033: ; Teplota TMC2 skoln
	DS    UT_U1033+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1033+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1033+OU_X-$
	DB    6,11,6,1
	DS    UT_U1033+OU_HLP-$
	%W    (0)
	DS    UT_U1033+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1033+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1033+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TMC2_MC)		; DP
	DS    UT_U1033+OU_I_F-$
	DB    82H		; format I_F
	%W    (-30000)		; I_L
	%W    (30000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1034: ; Teplota TMC12_DL offset
	DS    UT_U1034+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1034+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1034+OU_X-$
	DB    6,12,6,1
	DS    UT_U1034+OU_HLP-$
	%W    (0)
	DS    UT_U1034+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1034+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1034+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TMC12_DL)	; DP
	DS    UT_U1034+OU_I_F-$
	DB    02H		; format I_F
	%W    (0)		; I_L
   %IF (%FOR_BATH) THEN (
	%W    (5000)		; I_H
   )ELSE(
	%W    (2000)            ; I_H
   )FI
	%W    (0)		; I_K
	DB    0			; hlaska erroru

%IF(%WITH_TMCCOR)THEN(
UT_U1035: ; Max pripustny gradient TMCC_MGT
	DS    UT_U1035+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1035+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1035+OU_X-$
	DB    6,13,6,1
	DS    UT_U1035+OU_HLP-$
	%W    (0)
	DS    UT_U1035+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1035+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1035+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TMCC_MGT)	; DP
	DS    UT_U1035+OU_I_F-$
	DB    02H		; format I_F
	%W    (0)		; I_L
	%W    (1000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1036: ; Kriticke priblizeni TMCC_CET
	DS    UT_U1036+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1036+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1036+OU_X-$
	DB    6,14,6,1
	DS    UT_U1036+OU_HLP-$
	%W    (0)
	DS    UT_U1036+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1036+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1036+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TMCC_CET)	; DP
	DS    UT_U1036+OU_I_F-$
	DB    02H		; format I_F
	%W    (0)		; I_L
	%W    (1500)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru
)FI

UT_U1040: ; Podtlak
	DS    UT_U1040+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1040+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1040+OU_X-$
	DB    6,15,6,1
	DS    UT_U1040+OU_HLP-$
	%W    (0)
	DS    UT_U1040+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1040+OU_A_RD-$
	%W    (UR_TEMP)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1040+OU_DPSI-$
	%W    (0)		; DPSI
	%W    (TPS_AP)		; DP
	DS    UT_U1040+OU_I_F-$
	DB    80H ; 81H		; format I_F
	%W    (0)		; I_L
	%W    (2000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1041: ; Podtlak ADC
	DS    UT_U1041+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1041+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1041+OU_X-$
	DB    6,16,6,1
	DS    UT_U1041+OU_HLP-$
	%W    (0)
	DS    UT_U1041+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1041+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (NULL_A)		; A_WR
	DS    UT_U1041+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TPS_RD)		; DP
	DS    UT_U1041+OU_I_F-$
	DB    00H		; format I_F
	%W    (0)		; I_L
	%W    (0)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1042: ; Podtlak offset
	DS    UT_U1042+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1042+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1042+OU_X-$
	DB    6,17,6,1
	DS    UT_U1042+OU_HLP-$
	%W    (0)
	DS    UT_U1042+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1042+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1042+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TPS_OC)		; DP
	DS    UT_U1042+OU_I_F-$
	DB    81H ; 82H		; format I_F
	%W    (-30000)		; I_L
	%W    (30000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1043: ; Podtlak TM sklon
	DS    UT_U1043+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1043+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1043+OU_X-$
	DB    6,18,6,1
	DS    UT_U1043+OU_HLP-$
	%W    (0)
	DS    UT_U1043+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1043+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1043+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (TPS_MC)		; DP
	DS    UT_U1043+OU_I_F-$
	DB    81H ; 82H		; format I_F
	%W    (-30000)		; I_L
	%W    (30000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1044: ; Podtlak - pozadavek na vyvevu
	DS    UT_U1044+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1044+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1044+OU_X-$
	DB    6,19,6,1
	DS    UT_U1044+OU_HLP-$
	%W    (0)
	DS    UT_U1044+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1044+OU_A_RD-$
	%W    (UR_TEMP)		; A_RD
	%W    (UW_TEMP)		; A_WR
	DS    UT_U1044+OU_DPSI-$
	%W    (TPS_RQP)		; DPSI
	%W    (TPS_RQP)		; DP
	DS    UT_U1044+OU_I_F-$
	DB    80H ; 81H		; format I_F
	%W    (-999)		; I_L
	%W    (1000)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1045: ; Podtlak - hystereze
	DS    UT_U1045+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1045+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1045+OU_X-$
	DB    6,20,6,1
	DS    UT_U1045+OU_HLP-$
	%W    (0)
	DS    UT_U1045+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1045+OU_A_RD-$
	%W    (UR_TEMP)		; A_RD
	%W    (UW_TEMP)		; A_WR
	DS    UT_U1045+OU_DPSI-$
	%W    (TPS_HYS)		; DPSI
	%W    (TPS_HYS)		; DP
	DS    UT_U1045+OU_I_F-$
	DB    00H ; 81H		; format I_F
	%W    (0)		; I_L
	%W    (200)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1046: ; Podtlak - konfigurace
	DS    UT_U1046+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1046+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1046+OU_X-$
	DB    6,21,6,1
	DS    UT_U1046+OU_HLP-$
	%W    (0)
	DS    UT_U1046+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1046+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1046+OU_DPSI-$
	%W    (0)		; DPSI
	%W    (TPS_CFG)		; DP
	DS    UT_U1046+OU_I_F-$
	DB    80H ; 81H		; format I_F
	%W    (0)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U1050: ; Automatic down time
	DS    UT_U1050+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U1050+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1050+OU_X-$
	DB    6,22,6,1
	DS    UT_U1050+OU_HLP-$
	%W    (0)
	DS    UT_U1050+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U1050+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U1050+OU_DPSI-$
	%W    (0)		; DPSI
	%W    (ADN_TIM)		; DP
	DS    UT_U1050+OU_I_F-$
	DB    80H ; 81H		; format I_F
	%W    (0)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

%IF(%WITH_IIC)THEN(
UT_U1090: ; Save service
	DS    UT_U1090+OU_VEVJ-$
	DB    2
	DW    BUT_EV
	DS    UT_U1090+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U1090+OU_X-$
	DB    0,23,12,1
	DS    UT_U1090+OU_HLP-$
	%W    (0)
	DS    UT_U1090+OU_SFT-$
	%W    (0)		; UT_SFxxxx
	DS    UT_U1090+OU_B_S-$
	%W    (0)		; UT_USxxxx
	DS    UT_U1090+OU_B_P-$
	%W    (EEC_SER)		; Servisni nastaveni
	DS    UT_U1090+OU_B_F-$
	%W    (EEP_WRS)		; Zapis do EEPROM
	DS    UT_U1090+OU_B_T-$
	DB    'Save service',0
)FI

; ---------------------------------
; Zadavani konstant
UT_GR20:DS    UT_GR20+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR20+OGR_BTXT-$
	%W    (UT_GT20)
	DS    UT_GR20+OGR_STXT-$
	%W    (0)
	DS    UT_GR20+OGR_HLP-$
	%W    (0)
	DS    UT_GR20+OGR_SFT-$
	%W    (UT_SF20)
	DS    UT_GR20+OGR_PU-$
	%W    (UT_U2000)
	%W    (UT_U2001)
	%W    (UT_U2002)
	%W    (UT_U2003)
	%W    (UT_U2004)
	%W    (UT_U2005)
	%W    (UT_U2006)
	%W    (UT_U2007)
	%W    (UT_U2008)
	%W    (UT_U2009)
	%W    (UT_U2010)
	%W    (UT_U2011)
	%W    (UT_U2012)
	%W    (UT_U2013)
	%W    (UT_U2014)
	%W    (UT_U2015)
	%W    (0)

UT_GT20:DB    'Motor',C_NL
	DB    'POS',C_NL
	DB    'MS',C_NL
	DB    'MA',C_NL
	DB    'ME',C_NL
	DB    'P',C_NL
	DB    'I',C_NL
	DB    'D',C_NL
	DB    'S1',C_NL
	DB    'S2',C_NL
	DB    'FLG',C_NL
	DB    'CFG',C_NL
	DB    'S MUL',C_NL
	DB    'S DIV',C_NL
	DB    'PWM',C_NL
	DB    'SPD',0

UT_SF20:DB    -1
	%W    (UT_SF1)

UT_U2000:
	DS    UT_U2000+OU_VEVJ-$
	DB    2
	DW    MUT_EV
	DS    UT_U2000+OU_MSK-$
	DB    0 ; UFM_FOC
	DS    UT_U2000+OU_X-$
	DB    8,0,2,1
	DS    UT_U2000+OU_HLP-$
	%W    (0)
	DS    UT_U2000+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2000+OU_A_RD-$
	%W    (UR_Mi)		; A_RD
	%W    (UW_Mi)		; A_WR
	DS    UT_U2000+OU_DPSI-$
	%W    (0)               ; DPSI
	%W    (MR_UIAM)		; DP
	DS    UT_U2000+OU_M_S-$
	%W    (0)
	DS    UT_U2000+OU_M_P-$
	%W    (0)
	DS    UT_U2000+OU_M_F-$
	%W    (0)
	DS    UT_U2000+OU_M_T-$
	%W    (0FFH)
	%W    (000H)
	%W    (UT_U2000TA)
	%W    (0FFH)
	%W    (001H)
	%W    (UT_U2000TB)
	%W    (0FFH)
	%W    (002H)
	%W    (UT_U2000TC)
	%W    (0FFH)
	%W    (003H)
	%W    (UT_U2000TD)
	%W    (0FFH)
	%W    (004H)
	%W    (UT_U2000TE)
	%W    (0FFH)
	%W    (005H)
	%W    (UT_U2000TF)
	%W    (0)
UT_U2000TA:DB 'A',0
UT_U2000TB:DB 'B',0
UT_U2000TC:DB 'C',0
UT_U2000TD:DB 'D',0
UT_U2000TE:DB 'E',0
UT_U2000TF:DB 'F',0

UT_U2001:
	DS    UT_U2001+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2001+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2001+OU_X-$
	DB    3,1,8,1
	DS    UT_U2001+OU_HLP-$
	%W    (0)
	DS    UT_U2001+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2001+OU_A_RD-$
	%W    (UR_MRPl)		; A_RD
	%W    (UW_MRPl)		; A_WR
	DS    UT_U2001+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,0		; DP
	DS    UT_U2001+OU_I_F-$
	DB    0C3H		; format I_F
	%W    (8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2002:
	DS    UT_U2002+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2002+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2002+OU_X-$
	DB    5,2,6,1
	DS    UT_U2002+OU_HLP-$
	%W    (0)
	DS    UT_U2002+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2002+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2002+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_MS		; DP
	DS    UT_U2002+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2003:
	DS    UT_U2003+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2003+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2003+OU_X-$
	DB    5,3,6,1
	DS    UT_U2003+OU_HLP-$
	%W    (0)
	DS    UT_U2003+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2003+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2003+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_ACC	; DP
	DS    UT_U2003+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2004:
	DS    UT_U2004+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2004+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2004+OU_X-$
	DB    5,4,6,1
	DS    UT_U2004+OU_HLP-$
	%W    (0)
	DS    UT_U2004+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2004+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2004+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_ME		; DP
	DS    UT_U2004+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2005:
	DS    UT_U2005+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2005+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2005+OU_X-$
	DB    5,5,6,1
	DS    UT_U2005+OU_HLP-$
	%W    (0)
	DS    UT_U2005+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2005+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2005+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_P		; DP
	DS    UT_U2005+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2006:
	DS    UT_U2006+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2006+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2006+OU_X-$
	DB    5,6,6,1
	DS    UT_U2006+OU_HLP-$
	%W    (0)
	DS    UT_U2006+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2006+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2006+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_I		; DP
	DS    UT_U2006+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2007:
	DS    UT_U2007+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2007+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2007+OU_X-$
	DB    5,7,6,1
	DS    UT_U2007+OU_HLP-$
	%W    (0)
	DS    UT_U2007+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2007+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2007+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_D		; DP
	DS    UT_U2007+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2008:
	DS    UT_U2008+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2008+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2008+OU_X-$
	DB    5,8,6,1
	DS    UT_U2008+OU_HLP-$
	%W    (0)
	DS    UT_U2008+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2008+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2008+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_S1		; DP
	DS    UT_U2008+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2009:
	DS    UT_U2009+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2009+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2009+OU_X-$
	DB    5,9,6,1
	DS    UT_U2009+OU_HLP-$
	%W    (0)
	DS    UT_U2009+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2009+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2009+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_S2		; DP
	DS    UT_U2009+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2010:
	DS    UT_U2010+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2010+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2010+OU_X-$
	DB    5,10,6,1
	DS    UT_U2010+OU_HLP-$
	%W    (0)
	DS    UT_U2010+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2010+OU_A_RD-$
	%W    (UR_MRb)		; A_RD
	%W    (UW_MRb)		; A_WR
	DS    UT_U2010+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_FLG	; DP
	DS    UT_U2010+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (7FH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2011:
	DS    UT_U2011+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2011+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2011+OU_X-$
	DB    5,11,6,1
	DS    UT_U2011+OU_HLP-$
	%W    (0)
	DS    UT_U2011+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2011+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2011+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_CFG	; DP
	DS    UT_U2011+OU_I_F-$
	DB    00H		; format I_F
	%W    (0H)		; I_L
	%W    (0FFFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2012:
	DS    UT_U2012+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2012+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2012+OU_X-$
	DB    5,12,6,1
	DS    UT_U2012+OU_HLP-$
	%W    (0)
	DS    UT_U2012+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2012+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2012+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_SCM	; DP
	DS    UT_U2012+OU_I_F-$
	DB    80H		; format I_F
	%W    (-0FFH)		; I_L
	%W    ( 0FFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2013:
	DS    UT_U2013+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2013+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2013+OU_X-$
	DB    5,13,6,1
	DS    UT_U2013+OU_HLP-$
	%W    (0)
	DS    UT_U2013+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2013+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRCi)		; A_WR
	DS    UT_U2013+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_SCD	; DP
	DS    UT_U2013+OU_I_F-$
	DB    80H		; format I_F
	%W    (-0FFH)		; I_L
	%W    ( 0FFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2014:
	DS    UT_U2014+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2014+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2014+OU_X-$
	DB    5,14,6,1
	DS    UT_U2014+OU_HLP-$
	%W    (0)
	DS    UT_U2014+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2014+OU_A_RD-$
	%W    (NULL_A)		; A_RD
	%W    (UW_PWMi)		; A_WR
	DS    UT_U2014+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,0		; DP
	DS    UT_U2014+OU_I_F-$
	DB    82H		; format I_F
	%W    (-7FFFH)		; I_L
	%W    ( 7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U2015:
	DS    UT_U2015+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U2015+OU_MSK-$
	DB    UFM_FOC
	DS    UT_U2015+OU_X-$
	DB    5,15,6,1
	DS    UT_U2015+OU_HLP-$
	%W    (0)
	DS    UT_U2015+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U2015+OU_A_RD-$
	%W    (UR_MRCi)		; A_RD
	%W    (UW_MRSPi)	; A_WR
	DS    UT_U2015+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    -1,OMR_RS		; DP
	DS    UT_U2015+OU_I_F-$
	DB    80H		; format I_F
	%W    (-7FFFH)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

; ---------------------------------
; Lockovany display
UT_GR30:DS    UT_GR30+OGR_VEVJ-$
	DB    2
	DW    GR_EV
	DS    UT_GR30+OGR_BTXT-$
	%W    (UT_GT30)
	DS    UT_GR30+OGR_STXT-$
	%W    (0)
	DS    UT_GR30+OGR_HLP-$
	%W    (0)
	DS    UT_GR30+OGR_SFT-$
	%W    (UT_SF30)
	DS    UT_GR30+OGR_PU-$
	%W    (UT_U3001)
	%W    (UT_U3002)
	%W    (0)

UT_GT30:DB    'A            KEY',C_NL
	DB    'B           LOCK',C_NL
	DB    0

UT_SF30:DB    K_STOP
	%W    (0)
	%W    (STP_ALL)

	DB    K_END
	%W    (0)
	%W    (CLR_ALL)

	DB    0

UT_U3001:
	DS    UT_U3001+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U3001+OU_MSK-$
	DB    0
	DS    UT_U3001+OU_X-$
	DB    1,0,8,1
	DS    UT_U3001+OU_HLP-$
	%W    (0)
	DS    UT_U3001+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U3001+OU_A_RD-$
	%W    (UR_MRPl)		; A_RD
	%W    (UW_MRPl)		; A_WR
	DS    UT_U3001+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    0,0		; DP
	DS    UT_U3001+OU_I_F-$
	DB    0C3H		; format I_F
	%W    (8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

UT_U3002:
	DS    UT_U3002+OU_VEVJ-$
	DB    2
	DW    UIN_EV
	DS    UT_U3002+OU_MSK-$
	DB    0
	DS    UT_U3002+OU_X-$
	DB    1,1,8,1
	DS    UT_U3002+OU_HLP-$
	%W    (0)
	DS    UT_U3002+OU_SFT-$
	%W    (UT_SF0)
	DS    UT_U3002+OU_A_RD-$
	%W    (UR_MRPl)		; A_RD
	%W    (UW_MRPl)		; A_WR
	DS    UT_U3002+OU_DPSI-$
	%W    (0)               ; DPSI
	DB    1,0		; DP
	DS    UT_U3002+OU_I_F-$
	DB    0C3H		; format I_F
	%W    (8000H)		; I_L
	%W    (7FFFH)		; I_H
	%W    (0)		; I_K
	DB    0			; hlaska erroru

END
