$NOMOD51
;********************************************************************
;*                    LCP 4000 - LP.ASM                             *
;*                       Hlavni modul                               *
;*                  Stav ke dni 31.01.2003                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

%DEFINE (WITH_HELP) (1)
%DEFINE (WITH_MONITOR) (1)
%DEFINE (WITH_AUXVALV) (1)
%DEFINE (WITH_UL_DY_EEP) (1)	; vyrobnim cislem v EEPROM

EXTRN   CODE(RES_STAR,S_FLOW,S_FLOW1,M_STOP,M_START,M_PURGE,G_PRESS)
EXTRN   CODE(S_FLOW0,S_CFGFL)
EXTRN   XDATA(PRESS,PRESS_L,PRESS_H,AUXUAL)

EXTRN   CODE(ITIM_R,S_AUX,S_AUXVALV),BIT(ITIM_RF)
EXTRN   CODE(SET_GA,SET_GB,SET_GC,S_GRAD,S_GRAD0,S_GRDIR)
EXTRN   XDATA(TIME,M_COR,M_T,M_COR_F)
EXTRN   DATA(M_POS,M_PER,HW_FLG)
EXTRN   BIT(PRESS_O,PRESS_B,PRESS_E,PRESS_V,PRESS_S,PR_ENDD,FL_DIPR)
EXTRN   BIT(PR_ENDC,HW_CH_F,PR_GRER,ALRM_BB,SWEDG_F)
EXTRN   XDATA(TIMR3)

EXTRN   IDATA(STACK_BOTTOM)
EXTRN   BIT(FL_BRKUI)

%IF(%WITH_MONITOR)THEN(
EXTRN   CODE(MONITOR)
)FI
EXTRN   DATA(M_ENERG)

EXTRN	CODE(uL_OINI,uL_IDLE,LAN_MRK)

PROGRAM XDATA 09000H
PROGR_E XDATA 0C000H
PROGR_L	SET   PROGR_E-PROGRAM

PUBLIC  WR_UPDA,WR_MASK

PUBLIC  ERR_COD,START,I_TIME
PUBLIC  U_TIME,U_PRESS,U_AUX,U_CONC,U_FLOW
PUBLIC  FLOW,GRAD_B,GRAD_C

PUBLIC  WRT_1,LNT_DIR,ERR_CRY,HELP_HL,LNT_MAX
PUBLIC  PROGRAM,PROGR_E,PROGR_L

PUBLIC	ULP_ACTN,ULP_RUN,ULP_END,ULP_CLR,ULP_BRKUI
PUBLIC	GBF_CHRQ,GBF_ERR,GBF_ERRCL,GBF_SLAVE
PUBLIC  S_GBFMODE,G_GBFMODE

$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_ADR)
$INCLUDE(%INCH_UF)
$INCLUDE(%INCH_ULAN)
$INCLUDE(%INCH_REGS)
$INCLUDE(%INCH_MMAC)
$LIST

%IF (%EQS(%HW_VERSION,PB)) THEN (
EXTRN   BIT(FL_RERR,FL_ENOV)
)FI

%IF(%WITH_UL_G_BF) THEN (
EXTRN   XDATA(GBF_MODE)
EXTRN   XDATA(GBF_SLA)
EXTRN	XDATA(GBF_SLGR,GBF_SLAUX)
)FI

LP____C SEGMENT CODE
;LP____D SEGMENT DATA
LP____B SEGMENT DATA BITADDRESSABLE
LP____X SEGMENT XDATA
STACK   SEGMENT IDATA


USING   1
USING   0

CSEG    AT    00009H
EPRCHK1:DW    00000H          ; Kontrolni soucet eprom
CSEG    AT    00019H
EPRCHK2:DW    0FFFFH          ; CPL kontrolniho soucetu eprom

RSEG LP____B

WR_MASK:DS    1               ; Maska vypisu
M_F_LIN BIT   WR_MASK.7
M_TIME  BIT   WR_MASK.6
M_PRESS BIT   WR_MASK.5
M_CONC  BIT   WR_MASK.4
M_AUX   BIT   WR_MASK.3
M_CYCLE BIT   WR_MASK.2
M_FLOW  BIT   WR_MASK.1

WR_UPDA:DS    1               ; Update flagy
U_F_LIN BIT   WR_UPDA.7
U_TIME  BIT   WR_UPDA.6
U_PRESS BIT   WR_UPDA.5
U_CONC  BIT   WR_UPDA.4
U_AUX   BIT   WR_UPDA.3
U_CYCLE BIT   WR_UPDA.2
U_FLOW  BIT   WR_UPDA.1

ULP_FLG:DS    1		      ; Priznaky pro rizeni pres uLAN
UPF_RUN BIT   ULP_FLG.7	      ; Request to run program
UPF_END	BIT   ULP_FLG.6	      ; Request end of program mode
UPF_CLR BIT   ULP_FLG.5	      ; Request to clear prog mem
UPF_MSK	EQU   0E0H
GBF_CHRQ BIT  ULP_FLG.3	      ; Pozadavek na preposilani na slave
GBF_ERR BIT   ULP_FLG.2	      ; Chyba zpusobena slave pumpou
GBF_ERRCL BIT ULP_FLG.1	      ; Nulovami chyby ve slave
GBF_SLAVE BIT ULP_FLG.0	      ; Pumpa pracuje v rezimu slave

RSEG LP____X

ERR_COD:DS    2	   ; Error code

IC_DATA:           ; Udaje pro cas 0.00

PRE_OCF:DS    2
IC_DATF:DS    8    ; Prutok

PRE_OCB:DS    2
IC_DATB:DS    8    ; Grad B

PRE_OCC:DS    2
IC_DATC:DS    8    ; Grad C

IC_DAAX:DS    1    ; AUX (bit 7 = 1 Aux Valves)

IC_MCOR:DS    2    ; Press correction

IC_DAGR:DS    1	   ; Defaultni gradient pro G_BF_FL

IC_SLAUX:DS   1    ; Slave AUX (bit 7 = 1 Aux Valves)

IC_SLGR: DS   1    ; Slave gradient valves

	DS    2    ; Rezerva
IC_DATA_END:

CYCLE_A:DS    2    ; Cislo cyklu
CYCLE:  DS    2    ; Pocet cyklu

APROX_REGS:        ; Aproximacni registry

APROX_F:DS    2
FLOW   :DS    8    ; Prutok v 0.001 ml/min

APROX_B:DS    2
GRAD_B: DS    8    ; procenta B

APROX_C:DS    2
GRAD_C: DS    8    ; procenta C

MY_OC__:DS    2
PRE_OC_:DS    2
TIM_BUF:DS    2

ALRM_TM:DS    2    ; Cas programoveho budiku

RSEG LP____C
	JMP   RES_STAR
START:	SETB  ITIM_RF         ; Inicializace casu
	MOV   DPTR,#TIME
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#ERR_COD   ; Nulovani chyboveho kodu
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#CYCLE     ; Inicializace cyklu
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#ALRM_TM   ; Casovac vypnut
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	CLR   ALRM_BB
	MOV   ULP_FLG,#0      ; Vynulovani uLAN pozadavku

	MOV   DPTR,#07FFDH
	MOVX  A,@DPTR
	MOV   R4,A
	INC   A
	JZ    START03
	MOV   R5,#0
	CALL  S_CFGFL
START03:

	MOV   DPTR,#M_COR     ; Zadna korekce
	MOV   A,#000H
	MOVX  @DPTR,A
	MOV   A,#000H
	INC   DPTR
	MOVX  @DPTR,A
	CLR   A
	MOV   DPL,A
	MOV   DPH,A
	MOV   R4,A
	MOV   R5,A
	MOV   R3,A
	MOV   R0,A
	MOV   R1,#07FH
START1: MOVX  A,@DPTR
	MOV   R2,A
	CALL  ADDi
	INC   DPTR
	DJNZ  R0,START1
%IF (%EQS(%HW_VERSION,PB)) THEN (
	%WATCHDOG
)FI
	DJNZ  R1,START1
	MOV   DPTR,#EPRCHK1
	CALL  cLDR23i
	CALL  CMPi
	MOV   IE,#010000100B  ; Spusteni preruseni
	PUSH  ACC
	CALL  I_UL_SM	      ; Set uLAN memory locations
	CALL  uL_INIT	      ; Spusteni komunikace uLan
	CALL  uL_OINI	      ; Objektova komunikace
	POP   ACC
	JZ    START2
	MOV   DPTR,#ERR_EPR
	CALL  cPRINT
	MOV   A,#LCD_HOM+040H
	CALL  LCDWCOM
	MOV   R7,#50H
	CALL  PRINTi
START0:	CALL  SCANKEY
	JZ    START0

START2: JNB   FL_DIPR,START32
	CALL  FND_PGE
	MOV   DPTR,#PROG_SF
	MOVX  A,@DPTR
	XRL   A,#0AAH
	JZ    START5
	DEC   A
	JNZ   START3
	MOV   DPTR,#DATA_CH
	MOVX  A,@DPTR
	JZ    START5

START3: MOV   DPTR,#ERR_SRD
	CALL  ERR_DPT
	CALL  M_MCLR
START32:%LDR45i(0)
	MOV   DPTR,#07FFEH
	MOVX  A,@DPTR
	MOV   B,A
	INC   A
	JZ    START33
	MOV   A,#20	      ; Prednastavene PH
	MUL   AB
	MOV   R4,A
	MOV   R5,B
START33:MOV   DPTR,#PRESS_H   ; Horni tlakova mez
	CALL  xSVR45i

	MOV   DPTR,#PRESS_L   ; Dolni tlakova mez
	MOV   A,#000H
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#000H
	MOVX  @DPTR,A

	MOV   DPTR,#FLOW     ; Prutok
	MOV   A,#LOW 250
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#HIGH 250
	MOVX  @DPTR,A

	CLR   A
	MOV   DPTR,#GRAD_B   ; Gradienty
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#GRAD_C
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A

    %IF(%WITH_UL_G_BF) THEN (
	MOV   DPTR,#GBF_MODE
	MOVX  @DPTR,A	     ; Gradient ventily
    )FI
    
START4: CALL  SET_PROG
	MOV   DPTR,#PROGRAM
	MOVX  A,@DPTR
	JZ    START5
	JNB   FL_DIPR,START6
	MOV   DPTR,#T_ST_01
	CALL  QUES_YN
	JZ    START6
START5: CALL  SET_PROG
	CALL  M_KEND
START6: CALL  S_GBFMODES
	JNB   FL_DIPR,NDIPR
	MOV   DPTR,#LPVER_T
	CALL  cPRINT
	MOV   A,#0FFH
	CALL  WAIT_P

SET_DIT1:MOV  R2,#LOW DIT_1
	MOV   R3,#HIGH DIT_1
SET_DITx:PUSH ACC
	PUSH  ACC
	JMP   SET_DIT

; Cerpadlo bez displeje

NDIPR:
NODIL1:
	CALL  uL_IDLE
	CALL  ERR_CRY        ; Hlidani kritickych chyb
	JMP   NODIL1

; Obsluha programu pres uLAN


ULP_ACTN:
	JB    %RUN_FL,ULP_ACT9
	MOV   A,R4
	ORL   A,R5
	JZ    ULP_ACT1
	MOV   DPTR,#PROG_NU
	MOV   A,R4
	MOVX  @DPTR,A
ULP_ACT1:
	CALL  SET_PROG	      ; Nastaveni cisla programu
	CALL  TST_PRG	      ; Kontrola existence/zalozeni
	CALL  COMP_PP         ; Kompilace programu
	CALL  M_KEND0	      ; Nastaveni pocatecnich hodnot
	SETB  FL_BRKUI
	CLR   F0
ULP_ACT9:
	RET


ULP_RUN:JB    %RUN_FL,ULP_ACT9
	SETB  UPF_RUN
	SETB  FL_BRKUI
	CLR   F0
	RET

ULP_CLR:SETB  UPF_CLR
	JNB   %RUN_FL,ULP_END1
ULP_END:SETB  UPF_END
ULP_END1:CLR  UPF_RUN
	SETB  FL_BRKUI
	CLR   F0
	RET

ULP_BRKUI:
	MOV   SP,#STACK_BOTTOM
    %IF(%WITH_UL_G_BF) THEN (
	JB    GBF_SLAVE,ULP_BRKUI20
    )FI
	JB    %RUN_FL,ULP_BRKUI20
	CALL  COMP_PR
ULP_BRKUI20:
	CLR   %PROG_FL
	CLR   %ALRM_FL

	JBC   UPF_END,ULP_DO_END
	JBC   UPF_RUN,ULP_DO_RUN
	JBC   UPF_CLR,ULP_DO_CLR

ULP_BRKUIR:
	MOV   A,ULP_FLG
	ANL   A,#UPF_MSK
	JNZ   ULP_BRKUI
	JNB   FL_DIPR,NDIPR
%IF(%WITH_UL_G_BF) THEN (
	JNB   GBF_SLAVE,ULP_BRKUI75
	MOV   R2,#LOW  DIT_GBF1
	MOV   R3,#HIGH DIT_GBF1
	JMP   SET_DITx
ULP_BRKUI75:
)FI
	JB    %RUN_FL,ULP_BRKUI95
	JMP   SET_DIT1
ULP_BRKUI95:
	MOV   R2,#LOW  DIT_R1
	MOV   R3,#HIGH DIT_R1
	JMP   SET_DITx

ULP_DO_RUN:
	CLR   PR_ENDC
	CALL  COMP_PR
	CALL  M_KEND0
	CALL  M_START
	CLR   A
	MOV   DPTR,#CYCLE
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	CALL  SET_CYC
	SETB  %RUN_FL
	CALL  LEDWR
	JMP   ULP_BRKUIR

ULP_DO_END:
	CLR   %RUN_FL
	CLR   %HOLD_FL
	CALL  M_KEND0
	CALL  LEDWR
	JMP   ULP_BRKUIR

ULP_DO_CLR:
	MOV   DPTR,#PROGRAM
	MOV   R7,#HIGH PROGR_E
	MOV   A,#HIGH START
	CJNE  A,DPH,ULP_DO_CLR1
ULP_DO_CLR1:
	JC    ULP_DO_CLR2
	MOV   R7,A
ULP_DO_CLR2:
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,DPH
	XRL   A,R7
	JNZ   ULP_DO_CLR2
	MOV   DPTR,#PROG_NU
	CLR   A
	MOVX  @DPTR,A
	CALL  SET_PROG
	JMP   ULP_BRKUIR

; Posun kurzoru pri GRAD

MOD_POG:JB    %G_BF_FL,MOD_POR
	JMP   MOD_LNP

MOD_POR:RET

; ENTER pri GRAD

ENT_POG:MOV   A,#1
	JB    %G_BF_FL,ENT_PO1
	MOV   A,#2
ENT_PO1:JMP   ENT_PR1

; Prejde do manualniho modu
; POZOR : sp=sp-2

PROG_OU:CALL  COMP_PP         ; Kompilace programu
	POP   ACC
	POP   ACC
	CLR   %PROG_FL
	CALL  LEDWR
	JMP   SET_DIT1

; Kompilace dat

COMP_PR:MOV   DPTR,#PROG_SF
	MOVX  A,@DPTR
	XRL   A,#0AAH
	JNZ   COMP_PP
	RET
COMP_PP:MOV   DPTR,#T_PR_CO
	CALL  cPRINT
	MOV   WR_UPDA,#0FFH
COMP_P0:CALL  GO_PRBE
	MOV   DPTR,#PROG_SF
	MOV   A,#0ABH
	MOVX  @DPTR,A
	MOV   DPTR,#IC_DATA
	MOV   A,#IC_DATA_END-IC_DATA	; 3*10+1+2+1
	CALL  NUL_SPC

COMP_P1:MOV   R1,#0           ; Prutok
	MOV   R2,#NOT 0
	MOV   R3,#1
	MOV   R7,#0*10
	CALL  COMP_LN
	MOV   R1,#0           ; Grad B
	MOV   R2,#NOT 080H
	MOV   R3,#2
	MOV   R7,#1*10
	CALL  COMP_LN
	JB    %G_BF_FL,COMP_P1A
	MOV   R1,#8           ; Grad C
	MOV   R2,#NOT 040H
	MOV   R3,#2
	MOV   R7,#2*10
	CALL  COMP_LN
COMP_P1A:
	MOV   R1,#0           ; Aux output
	MOV   R2,#NOT 0
	MOV   R3,#3
	CALL  TST_LIN
	JNZ   COMP_P2
	MOV   A,R2
	ORL   A,R3
	JNZ   COMP_P2
	MOVX  A,@DPTR
	MOV   DPTR,#IC_DAAX
	MOVX  @DPTR,A
COMP_P2:
%IF(%WITH_AUXVALV)THEN(
	MOV   R1,#0           ; Aux valves
	MOV   R2,#NOT 0
	MOV   R3,#7
	CALL  TST_LIN
	JNZ   COMP_P3
	MOV   A,R2
	ORL   A,R3
	JNZ   COMP_P3
	MOVX  A,@DPTR
	JZ    COMP_P3
	ORL   A,#080H
	MOV   DPTR,#IC_DAAX
	MOVX  @DPTR,A
COMP_P3:MOV   R1,#0           ; Grad valves
	MOV   R2,#NOT 0
	MOV   R3,#8
	CALL  TST_LIN
	JNZ   COMP_P4
	MOV   A,R2
	ORL   A,R3
	JNZ   COMP_P4A
	MOVX  A,@DPTR
	MOV   DPTR,#IC_DAGR
	MOVX  @DPTR,A
COMP_P4A:JB   %G_BF_FL,COMP_P4
	CLR   A
	MOV   DPTR,#PRE_OCB
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#PRE_OCC
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
COMP_P4:
)FI
%IF(%PRESS_COR)THEN(
	MOV   R1,#0           ; Press correction
	MOV   R2,#NOT 0
	MOV   R3,#9
	CALL  TST_LIN
	JNZ   COMP_P5
	MOV   A,R2
	ORL   A,R3
	JNZ   COMP_P5
	CALL  xLDR45i
	MOV   DPTR,#IC_MCOR
	CALL  xSVR45i
COMP_P5:
)FI
%IF(%WITH_AUXVALV AND %WITH_UL_G_BF)THEN(
	MOV   R1,#0           ; Slave aux valves
	MOV   R2,#NOT 0
	MOV   R3,#10
	CALL  TST_LIN
	JNZ   COMP_P6
	MOV   A,R2
	ORL   A,R3
	JNZ   COMP_P6
	MOVX  A,@DPTR
	JZ    COMP_P6
	ORL   A,#080H
	MOV   DPTR,#IC_SLAUX
	MOVX  @DPTR,A
COMP_P6:MOV   R1,#0           ; Slave grad valves
	MOV   R2,#NOT 0
	MOV   R3,#11
	CALL  TST_LIN
	JNZ   COMP_P7
	MOV   A,R2
	ORL   A,R3
	JNZ   COMP_P7
	MOVX  A,@DPTR
	JZ    COMP_P7
	ORL   A,#0C0H
	MOV   DPTR,#IC_SLGR
	MOVX  @DPTR,A
COMP_P7:
)FI
	MOV   R2,#NOT 0       ; End programu
	MOV   R3,#4
	CALL  TST_LIN
	JZ    COMP_R1
	CALL  NEXT_LN
	PUSH  ACC
	CALL  SET_LAD
	POP   ACC
	JZ    COMP_R1	      ; Last line
	JMP   COMP_P1	      ; Procceed next line
COMP_R1:MOV   DPTR,#PROG_SF
	MOV   A,#0AAH
	MOVX  @DPTR,A
COMP_RE:RET

COMP_L0:JMP   COMP_L7

COMP_LN:CALL  TST_LIN
	JNZ   COMP_RE
	PUSH  DPL
	PUSH  DPH
	MOV   A,#6
	CALL  ADDATDP
	CALL  xSVR23i
	MOV   DPTR,#TIM_BUF
	CALL  xSVR23i
	MOV   A,R2
	ORL   A,R3
	MOV   R1,A
	POP   DPH
	POP   DPL
COMP_L1:MOV   R2,DPL
	MOV   R3,DPH
	MOV   DPTR,#MY_OC__
	CALL  xSVR23i
	MOV   DPTR,#IC_DATA
	MOV   A,R7
	CALL  ADDATDP
	CALL  xLDR45i
	CALL  xSVR23i
	MOV   A,R4
	ORL   A,R5
	JZ    COMP_L0
COMP_L2:MOV   DPTR,#PRE_OC_
	CALL  xSVR45i
	MOV   DPL,R4
	MOV   DPH,R5
	CALL  cLDR23i
	CLR   A
	CALL  TAKEIA1
	CALL  xLDR45i
	CALL  SUBi            ; Rozdil HODNOT do R45
	PUSH  PSW
	JNC   COMP_L3         ; Vytvorit absolutni hodnotu
	CALL  NEGi
COMP_L3:MOV   A,R4
	MOV   R6,A
	MOV   A,R5
	MOV   R7,A            ; V R67 abs rozdilu HODNOT
	MOV   A,#6
	CALL  TAKEIA2
	CALL  xLDR23i
	MOV   DPTR,#TIM_BUF
	CALL  xLDR45i
	MOV   A,#6
	CALL  TAKEIA1
	CALL  xSVR45i
	CALL  SUBi
	MOV   A,#6
	CALL  TAKEIA2
	CALL  xSVR45i         ; Rozdil CASU
	MOV   A,R4
	MOV   R2,A
	MOV   A,R5
	MOV   R3,A            ; R23 rozdil CASU
	CALL  MR45R67         ; R45 rozdil HODNOT
	CALL  MODi
	JNB   F0,COMPL_4
	CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   R6,A
	MOV   R7,A
COMPL_4:POP   PSW	      ; V CY znamenko rozdilu HODNOT
	PUSH  PSW
	JNC   COMP_L5
	CALL  NEGi
	CALL  xLDR23i
	CALL  ADDi
COMP_L5:MOV   A,#4
	CALL  TAKEIA2
	CALL  xSVR45i         ; Zapis ZBYTKU
	CALL  MR45R67
	MOV   A,#2
	CALL  TAKEIA2
	POP   PSW
	JNC   COMP_L6
	MOV   A,R4
	CPL   A
	MOV   R4,A
	MOV   A,R5
	CPL   A
	MOV   R5,A
COMP_L6:CALL  xSVR45i         ; Zapis KROKU
	MOV   A,#2
	CALL  TAKEIA1
	MOV   A,#4
	JMP   NUL_SPC

COMP_L7:MOV   R4,DPL
	MOV   R5,DPH
	MOV   A,R1
	JNZ   COMP_L8
	PUSH  DPL
	PUSH  DPH
	CLR   A
	CALL  TAKEIA1
	CALL  xLDR23i
	MOV   A,#6
	CALL  NUL_SPC
	POP   DPH
	POP   DPL
	CALL  xSVR23i
RUN_PRR:
COMP_LR:RET

COMP_L8:JMP   COMP_L2

; DPTR = [DPTR]+A

TAKEIA1:MOV   DPTR,#MY_OC__
	SJMP  TAKEIA
TAKEIA2:MOV   DPTR,#PRE_OC_
TAKEIA: JMP   xMDPDPA


RUN_PR2:MOV   DPTR,#T_RU_RC
	CALL  QUES_YN
	JZ    RUN_PRR
	CALL  M_KEND0
	JB    %START_FL,RUN_PRR
RUN_PR1:MOV   DPTR,#T_RU_MS
	CALL  QUES_YN
	JZ    RUN_PRR
	JMP   M_START

; Spusteni casoveho programu

RUN_PRG:CALL  COMP_PR
	MOV   DPTR,#EPRCHK1
	CALL  cLDR23i
	MOV   DPTR,#EPRCHK2
	CALL  xLDR45i
	CALL  ADDi
	ANL   A,R4
	CPL   A
	JNZ   RUN_PR1
	MOV   DPTR,#FLOW
	MOV   R0,#LOW  IC_DATF
	MOV   R1,#HIGH IC_DATF
	MOV   R7,#3
RUN_PR3:CALL  XCDPR01
	CALL  xLDR23i
	MOV   A,#8
	CALL  ADDATDP
	CALL  XCDPR01
	CALL  xLDR45i
	MOV   A,#10
	CALL  ADDATDP
	CALL  CMPi
	JNZ   RUN_PR2
	DJNZ  R7,RUN_PR3

	MOV   C,%START_FL
	ANL   C,/%PURGE_FL
	JNC   RUN_PR1
	SETB  %START_FL
	CLR   PR_ENDC
	CALL  M_KEND0
	CALL  SET_CYC
	SETB  %RUN_FL
	CALL  LEDWR
	MOV   R2,#LOW  DIT_R1
	MOV   R3,#HIGH DIT_R1
	JMP   SET_DIT

; Nastaveni cycle

SET_CYC:MOV   DPTR,#CYCLE
	CALL  xLDR45i
	ORL   A,R4
	JNZ   SET_CY1
	MOV   A,#1
	MOVX  @DPTR,A
SET_CY1:MOV   DPTR,#CYCLE_A
	MOV   A,#1
	MOVX  @DPTR,A
	INC   DPTR
	CLR   A
	MOVX  @DPTR,A
SET_CY2:MOV   DPTR,#APROX_F
	CALL  xMDPPM1
	MOV   DPTR,#APROX_B
	CALL  xMDPPM1
	MOV   DPTR,#APROX_C
	CALL  xMDPPM1
	MOV   DPTR,#TIME
	MOV   A,#0FFH
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	JMP   GO_PRBE

; Nastaveni pocatecnich parametru

M_KEND: CALL  COMP_PR
M_KEND0:CLR   %PURGE_FL
	CALL  M_KENDS		; initial interpolated vars
	CALL  S_GRAD
	CALL  S_FLOW0
	CALL  LEDWR
M_KEND1:
    %IF(%PRESS_COR)THEN(
	MOV   DPTR,#IC_MCOR	; initial motor correction
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R0
	MOV   DPTR,#M_COR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
    )FI
    %IF(%WITH_AUXVALV AND %WITH_UL_G_BF)THEN(
    	MOV   DPTR,#IC_SLAUX
	MOVX  A,@DPTR
	JZ    M_KEND1C
	MOV   DPTR,#GBF_SLAUX
	MOVX  @DPTR,A
M_KEND1C:
    	MOV   DPTR,#IC_SLGR
	MOVX  A,@DPTR
	JZ    M_KEND1D
	MOV   DPTR,#GBF_SLGR
	MOVX  @DPTR,A
M_KEND1D:
	JNB   %G_BF_FL,M_KEND1E
	MOV   DPTR,#IC_DAGR
	MOVX  A,@DPTR
	JZ    M_KEND1E
	MOV   R4,A
	MOV   R5,#0
	CALL  S_GRDIR
M_KEND1E:
    )FI
	MOV   DPTR,#IC_DAAX	; initial aux output
	MOVX  A,@DPTR
	JBC   ACC.7,M_KEND2
	MOV   R4,A
	JMP   S_AUX
M_KEND2:MOV   R4,A
	JMP   S_AUXVALV

M_KENDS:MOV   R2,#LOW  IC_DATA
	MOV   R3,#HIGH IC_DATA
	MOV   R4,#LOW  APROX_REGS
	MOV   R5,#HIGH APROX_REGS
	MOV   R0,#3*10
	JMP   xxMOVEt

; Preruseni behu programu

RU_BRK: MOV   DPTR,#T_RU_BR
	CALL  QUES_YN
	JNZ   RU_BRK1
RU_BRKR:RET
RU_STOP:MOV   DPTR,#T_RU_BR
	CALL  QUES_YN
	JZ    RU_BRKR
	CALL  M_STOP
RU_BRK1:POP   ACC
	POP   ACC
RU_BRK2:CALL  M_KEND0
	CLR   %RUN_FL
	CLR   %HOLD_FL
	CALL  LEDWR
	JMP   SET_DIT1

RU_CH_R:JNB   %RUN_FL,RU_BRK2
	JNB   %START_FL,RU_BRK2
	CALL  INPUTc1
	JNB   %RUN_FL,RU_BRK2
	JNB   %START_FL,RU_BRK2
	JMP   RD_RET

RU_IT_R:JNB   %RUN_FL,RU_BRK2
	JNB   %START_FL,RU_BRK2
	JMP   R_INTEG

RU_HOLD:JB    %HOLD_FL,RU_HOL1
	MOV   DPTR,#T_RU_HO
	CALL  QUES_YN
	JZ    RU_HOLR
	SETB  SWEDG_F
RU_HOLP:MOV   DPTR,#TIME
	CLR   EA
	CALL  xLDR45i
	MOV   DPTR,#TIM_BUF
	CALL  xSVR45i
	SETB  EA
	SETB  %HOLD_FL
	CALL  LEDWR
RU_HOLR:RET

RU_HOLC:
RU_HOL1:MOV   DPTR,#TIM_BUF
	CLR   EA
	CALL  xLDR45i
	MOV   DPTR,#TIME
	CALL  xSVR45i
	SETB  EA
	CLR   %HOLD_FL
	CALL  LEDWR
	RET

CYC_NEW:SETB  U_CYCLE
	MOV   DPTR,#CYCLE_A
	CLR   A
	CLR   EA
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	INC   DPTR
	CALL  xSVR45i
	SETB  EA
	RET

; Vyber programu

SEL_PRG:CALL  SET_PROG
	CALL  TST_PRG
	CALL  COMP_PP         ; Kompilace programu
	POP   ACC
	POP   ACC
	JMP   SET_DIT1

; !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
$NOLIST

M_MODE: MOV   R4,#LOW M_M_NH
	MOV   R5,#HIGH M_M_NH
	MOV   DPTR,#HLP_TXT
	CALL  xSVR45i
	MOV   R2,#LOW  M_MOMM1
	MOV   R3,#HIGH M_MOMM1
	JMP   SET_MENU

M_MOMM1:DB    LOW M_MOMT1,HIGH M_MOMT1
	DB    LOW M_MOMH1,HIGH M_MOMH1
	DB    LOW SFT_0,HIGH SFT_0
	DB    LOW DIT_1,HIGH DIT_1
	DB    LOW SET_DIT,HIGH SET_DIT
	DB    0,0
	DB    LOW M_MOHWS,HIGH M_MOHWS
	DB    14H,15H
    %IF(%WITH_MONITOR)THEN(
	DB    LOW MONITOR,HIGH MONITOR
    )ELSE(
	DB    LOW 0,HIGH 0
    )FI
	DB    0,0
	DB    LOW M_PSPD,HIGH M_PSPD
	DB    0,0
	DB    LOW M_MCLR,HIGH M_MCLR
	DB    LOW DIT_4,HIGH DIT_4
	DB    LOW SET_DIT,HIGH SET_DIT
	DB    0,0
	DB    LOW uL_CALL,HIGH uL_CALL
    %IF(%WITH_UL_G_BF) THEN (
	DB    LOW DIT_9,HIGH DIT_9
	DB    LOW SET_DIT,HIGH SET_DIT
    )ELSE(
	DB    0,0
	DB    LOW M_MOGF,HIGH M_MOGF
    )FI
	DB    0,0
	DB    LOW M_MOGV,HIGH M_MOGV
	DB    LOW DIT_6,HIGH DIT_6
	DB    LOW SET_DIT,HIGH SET_DIT
%IF(%PRESS_COR)THEN(
	DB    LOW DIT_8,HIGH DIT_8
	DB    LOW SET_DIT,HIGH SET_DIT
)ELSE(
	DB    0,0,0,0
)FI

	DB    0FEH,0FEH,0FEH
M_MOMT1:DB    0,0FFH,1
	DB    0FFH,0FFH,0FFH
	DB    'Exit',0
	DB    1,0FFH,1
	DB    0FFH,0FFH,0FFH
	DB    'Hw test',0
	DB    2,0FFh,1,0FFH,0FFH,0FFH
	DB    'Monitor',0
	DB    3,0FFh,1,0FFH,0FFH,0FFH
	DB    'Processor speed',0
	DB    4,0FFh,1,0FFH,0FFH,0FFH
	DB    'Memory clear',0
	DB    5,0FFh,1,0FFH,0FFH,0FFH
	DB    'Comunication',0
	DB    6,0FFh,1,0FFH,0FFH,0FFH
	DB    'uL Function',0
	DB    7,0FFh,1,0FFH,0FFH,0FFH
	DB    'Grad. by flow',0
	DB    8,0FFh,1,0FFH,0FFH,0FFH
	DB    'Gradient valves',0
	DB    9,0FFh,1,0FFH,0FFH,0FFH
	DB    'User timer/alarm',0
%IF(%PRESS_COR)THEN(
	DB    10,0FFh,1,0FFH,0FFH,0FFH
	DB    'Press correct',0
)FI
	DB    0FEH,0FEH,0FEH

M_MOMH1:DB    0FFH,0FFH,0FFH
	DB    'Select function',0
	DB    0FFH,0FFH,0FFH
	DB    'by cursors and',0
	DB    0FFH,0FFH,0FFH
	DB    'ENTER',0
	DB    0FEH,0FEH,0FEH

M_M_NH: DB    0FFH,0FFH,0FFH
	DB    'No help',0
	DB    0FFH,0FFH,0FFH
	DB    'avayable',0
	DB    0FEH,0FEH,0FEH

M_MCLRT:DB    C_CLR,'Clear all data?',0

M_MCLR: MOV   DPTR,#M_MCLRT
	CALL  QUES_YN
	JNZ   M_MCLR1
	RET
M_MCLR1:MOV   DPTR,#LP____X
	MOV   R4,#000H
	MOV   R5,#0F8H
M_MCLR2:CLR   A            ; !!!!!!!!!!!!!!
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	XRL   A,DPH
	JNZ   M_MCLR2
	MOV   A,R4
	XRL   A,DPL
	JNZ   M_MCLR2
	MOV   DPTR,#PROG_SF
	MOV   A,#0ABH
	MOVX  @DPTR,A
	MOV   A,#3      ; 19200 Bd pro krystal 11.0592
	MOV   R0,#1
	CALL  uL_FNC    ; Rychlost
	JMP   RESET

M_MOHWS:POP   ACC
	POP   ACC
	MOV   DPTR,#WR_TAB
        MOV   A,#LOW WRT_2
	MOVX  @DPTR,A
	INC   DPTR
        MOV   A,#HIGH WRT_2
	MOVX  @DPTR,A
	MOV   WR_MASK,#000H
	SETB  M_FLOW
M_MOHW: MOV   WR_UPDA,#0FFH
	MOV   DPTR,#T_MOHW2
	CALL  cPRINT
M_MOHW1:MOV   R7,#040H OR %C_FLOW_FDIG
	MOV   R6,#045H
	CALL  INPUTi
	SETB  U_FLOW
	JB    F0,M_MOHW2
	CALL  S_FLOW
	JMP   M_MOHW
M_MOHW2:MOV   DPTR,#SFT_MMH
	MOV   R7,A
        CALL  SEL_FN2
	JMP   M_MOHW

SFT_MMH:DB   K_MODE
	DB   0,0
	DB   LOW M_MOHWR,HIGH M_MOHWR

	DB   K_PROG
	DB   0,0
	DB   LOW RESET,HIGH RESET

	DB   K_AUX
	DB   0,0
	DB   LOW M_MOWA,HIGH M_MOWA

	DB   K_RUN
	DB   0,0
	DB   LOW M_MORUN,HIGH M_MORUN

	DB   K_END
	DB   0,0
	DB   LOW M_MOWE,HIGH M_MOWE

	DB   K_CYCLE
	DB   0,0
	DB   LOW M_MOCO,HIGH M_MOCO

	DB   0FFH
	DB   LOW SFT_0,HIGH SFT_0

M_MOCO: MOV   R7,#030H
	MOV   R6,#04BH
	CALL  INPUTi
	JB    F0,M_MOCOR
	MOV   DPTR,#M_COR
	CALL  xSVR45i
M_MOCOR:RET

M_MOWA: MOV   A,#LCD_HOM+00CH
	CALL  LCDWCOM
	MOV   R7,#040H
	MOV   R4,M_POS
	MOV   R5,M_POS+1
	CALL  PRINTi
	MOV   A,#LCD_HOM+040H
	CALL  LCDWCOM
	MOV   R7,#0C0H
	MOV   A,M_ENERG
	MOV   R4,A
	MOV   R5,#0
	JNB   ACC.7,M_MOWA1
	MOV   R5,#0FFH
M_MOWA1:CALL  PRINTi
	CALL  SCANKEY
	JZ    M_MOWA
	RET

M_MOWE:CALL  SCANKEY
	JZ    M_MOWE
	MOV   A,#LCD_CLR
	CALL  LCDWCOM
	RET

M_MORUN:MOV   DPTR,#TIME
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	SETB  M_TIME
	SETB  U_TIME
	RET

M_PSPD: MOV   DPTR,#T_MOHW1
	CALL  cPRINT
M_PSPD1:MOV   A,#LCD_HOM+048H
	CALL  LCDWCOM
	MOV   DPTR,#TIMR3
	MOV   A,#0FFH
	MOVX  @DPTR,A
	CLR   A
	MOV   R0,A
	MOV   R4,A
	MOV   R5,A
M_PSPD2:MOV   R1,#003H
M_PSPD3:DJNZ  R0,M_PSPD3
	DJNZ  R1,M_PSPD3
	MOV   A,R4
	ADD   A,#001H
	MOV   R4,A
	MOV   A,R5
	ADDC  A,#000H
	MOV   R5,A
	CALL  SCANKEY
	JNZ   M_PSPDR
	MOV   DPTR,#TIMR3
	MOVX  A,@DPTR
	JNZ   M_PSPD2
	MOV   R7,#070H
	CALL  PRINTi
	CALL  SCANKEY
	JZ    M_PSPD1
M_PSPDR:MOV   WR_UPDA,#0FFH
	RET

M_MOHWR:MOV  SP,#STACK
	JMP  SET_DIT1

S_GBFMODES:
    %IF(%WITH_UL_G_BF) THEN (
	MOV    DPTR,#GBF_MODE
	MOVX   A,@DPTR
	MOV    R4,A
    )FI
S_GBFMODE:
	MOV    A,R4
	JZ     M_MOGV
M_MOGF:	ORL    WR_UPDA,#07FH
	CALL   SET_GA
	SETB   %G_BF_FL
    %IF(%WITH_UL_G_BF) THEN (
	MOV    DPTR,#GBF_MODE
	MOV    A,#1
	MOVX   @DPTR,A
    )FI
	RET

M_MOGV: ORL   WR_UPDA,#07FH
	CLR    %G_BF_FL
	CALL   SET_GA
    %IF(%WITH_UL_G_BF) THEN (
	MOV    DPTR,#GBF_MODE
	CLR    A
	MOVX   @DPTR,A
    )FI
	RET
	
G_GBFMODE:
	CLR   A
	MOV   R4,A
	MOV   R5,A
	JNB   %G_BF_FL,G_GBFMODE1
	INC   R4
G_GBFMODE1:
	MOV   A,#1
	RET

%IF(%WITH_UL_G_BF) THEN (
GBF_BRK:MOV   DPTR,#T_GBF_BR
	CALL  QUES_YN
	JNZ   GBF_BRK1
GBF_BRKR:RET
GBF_STOP:MOV  DPTR,#T_GBF_BR
	CALL  QUES_YN
	JZ    GBF_BRKR
	CALL  M_STOP
GBF_BRK1:POP  ACC
	POP   ACC
	%LDMXi(ERR_COD,0FFF6H)
GBF_BRK2:CLR  GBF_SLAVE 
	CALL  LEDWR
	JMP   SET_DIT1

GBF_CH_R:JNB  GBF_SLAVE,GBF_BRK2
	CALL  INPUTc1
	JNB   GBF_SLAVE,GBF_BRK2
	JMP   RD_RET
)FI


uL_CALL:MOV  WR_MASK,#0
	MOV  DPTR,#uL_CALT
	CALL cPRINT
	MOV  R6,#041H
	MOV  R7,#030H
	CALL INPUTi
	MOV  A,R4
	PUSH ACC
	MOV  R6,#048H
	MOV  R7,#050H
	CALL INPUTi
	POP  ACC
	MOV  R0,A
	MOV  A,R4
	MOV  DPTR,#1234H
	JMP  uL_FNC

uL_CALT:DB   LCD_CLR,'Function Param',0
T_MOHW1:DB   LCD_CLR,'Speed in ',0F7H,'stones',0
T_MOHW2:DB   LCD_CLR,'Service mode'
	DB   C_LIN2 ,0

; Nastaveni bufferu a parametru komunikace uLan
I_UL_SM:JB    FL_DIPR,I_U_LA5
	MOV   DPTR,#7FFFH
	MOVX  A,@DPTR	; Adr
	CJNE  A,#0FFH,I_U_LA1
	MOV   A,#083H
I_U_LA1:MOV   R0,#3	; 19200 Bd pro krystal 11.0592
	JBC   ACC.7,I_U_LA2
	MOV   R0,#6	; 9600 Bd pro krystal 11.0592
I_U_LA2:PUSH  ACC
	MOV   A,R0
	SJMP  I_U_LA3
I_U_LAN:PUSH  ACC
	CLR   A
I_U_LA3:MOV   R0,#1
	CALL  uL_FNC	; Speed
	POP   ACC
	MOV   R0,#2
	CALL  uL_FNC	; Adr
I_U_LA5:MOV   R2,#0
	MOV   R0,#3
	CALL  uL_FNC	; IB, OB
	MOV   R2,#0
	MOV   R0,#4
	CALL  uL_FNC	; DO
	MOV   R0,#0
	CALL  uL_FNC
    %IF(%WITH_UL_DY_EEP) THEN (
	JMP   INI_SERNUM
    )FI
	RET

%IF(%WITH_UL_DY_EEP) THEN (

PUBLIC	SER_NUM
CSEG AT	8080H
SER_NUM:DS    10H	; Instrument unigue serial number
XSEG AT	8080H
	DS    10H	; XDATA overlay

RSEG	LP____C
	DB    -'U',-'L',-'D',-'Y'
	%W   (0)
	%W   (SER_NUM)
	%W   (WR_SERNUM)
INI_SERNUM:
	MOV   R2,#010H	; pocet prenasenych byte
	MOV   R4,#0F0H	; pocatecni adresa v EEPROM
	MOV   DPTR,#SER_NUM
	CALL  EE_RD
	JZ    INI_SERNUM9
INI_SERNUM7:
	MOV   R2,#8
	MOV   DPTR,#SER_NUM
	MOV   A,#0FFH
INI_SERNUM8:
	MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R2,INI_SERNUM8
INI_SERNUM9:
	RET
WR_SERNUM:
	CLR   ES
	MOV   PSW,#0
	MOV   SP,#80H
	;CALL  I_TIMRI
	;CALL  I_TIMRI
	CALL  WR_SERNUM1
	JMP   RESET
WR_SERNUM1:
	MOV   R2,#010H	; pocet prenasenych byte
	MOV   R4,#0F0H	; pocatecni adresa v EEPROM
	MOV   DPTR,#SER_NUM
	CALL  EE_WR
	RET
)FI

; *******************************************************************

%IF(%WITH_UL_DY_EEP) THEN (
; Prace s pameti EEPROM 8582

EEP_ADR	EQU   0A0H		; Adresa EEPROM na IIC sbernici
C_S1CON EQU   11000000B ; 11000001B ; Pocatecni stav S1CON
			; 11000000B ; S1CON s delenim 960
			; 01000000B ; S1CON pro X 24.000 MHz

EEA_RD	EQU   1		; akce cteni
EEA_WR	EQU   2		; akce zapisu

RSEG	LP____X

EE_PTR:	DS    2		; ukazatel do MEM_BUF na data
EEP_TAB:DS    2		; popis dat ulozenych v bloku EEPROM

MEM_BUF:DS    28H	; Buffer pameti EEPROM

RSEG	LP____C

EE_WAIT:JNB   SI,$
	MOV   A,S1STA
	RET

EE_WRA:	MOV   S1CON,#C_S1CON	; OR 20H
	SETB  STO
	CLR   SI
	SETB  STA		; START
	CALL  EE_WAIT
	CLR   STA
	CJNE  A,#008H,EE_WRA
	MOV   A,R5
	MOV   S1DAT,A		; SLA and W
	CLR   SI
	CALL  EE_WAIT
	XRL   A,#018H
	JNZ   EE_WRA9
	MOV   A,R4
	MOV   S1DAT,A		; Adresa v EEPROM
	CLR   SI
	CALL  EE_WAIT
	XRL   A,#028H
EE_WRA9:RET

; Cteni pameti EEPROM
;	DPTR .. pointer na buffer
;	R2   .. pocet ctenych byte
;	R4   .. adresa, od ktere se cte

EE_RD:	MOV   R5,#EEP_ADR
	MOV   R1,#0
EE_RD10:CLR   ES1
	CALL  EE_WRA
	JNZ   EE_ERR
	SETB  STA		; repeated START
	CLR   SI
	CALL  EE_WAIT
	CLR   STA
	CJNE  A,#010H,EE_ERR
	MOV   A,R5
	ORL   A,#1
	MOV   S1DAT,A		; SLA and R
	CLR   SI
	CALL  EE_WAIT
	CJNE  A,#040H,EE_ERR
	SETB  AA
EE_RD30:CLR   SI
	CALL  EE_WAIT
	CJNE  A,#050H,EE_ERR
	MOV   A,S1DAT		; DATA
	MOVX  @DPTR,A
	XRL   A,R1
	INC   A
	MOV   R1,A
	INC   DPTR
	DJNZ  R2,EE_RD30
	CLR   AA
	CLR   SI
	CALL  EE_WAIT
	CJNE  A,#058H,EE_ERR
	MOV   A,S1DAT		; XORSUM
	XRL   A,R1
	SETB  STO		; STOP
	CLR   SI
	RET

EE_ERR:	SETB  STO
	CLR   SI
	SETB  F0
	ORL   A,#07H
	RET

; Zapis do pameti EEPROM
;	DPTR .. pointer na buffer
;	R2   .. pocet zapisovanych byte
;	R4   .. adresa, od ktere se zapisuje

EE_WR:	MOV   R5,#EEP_ADR
	MOV   R1,#0
EE_WR10:CLR   ES1
EE_WR20:MOV   R0,#0
EE_WR21:NOP
	DJNZ  R0,EE_WR21
	MOV   R0,#100
	MOV   A,R2
	JZ    EE_WR30
EE_WR22:CALL  EE_WRA
	JZ    EE_WR23
	DJNZ  R0,EE_WR22
	SJMP  EE_ERR
EE_WR23:MOV   R0,#4
EE_WR24:MOVX  A,@DPTR
	MOV   S1DAT,A
	CLR   SI
	XRL   A,R1
	INC   A
	MOV   R1,A
	INC   DPTR
	DEC   R2
	INC   R4
	CALL  EE_WAIT
	CJNE  A,#028H,EE_ERR
	MOV   A,R2
	JZ    EE_WR26
	DJNZ  R0,EE_WR24
EE_WR26:SETB  STO
	CLR   SI
	SJMP  EE_WR20
EE_WR30:CALL  EE_WRA
	JZ    EE_WR34
	DJNZ  R0,EE_WR30
	SJMP  EE_ERR
EE_WR34:MOV   A,R1
	MOV   S1DAT,A
	CLR   SI
	CALL  EE_WAIT
	CJNE  A,#028H,EE_ERR
	SETB  STO
	CLR   SI
	RET

)FI

; !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

$LIST
I_TIMM :MOV   DPTR,#TIME      ; Manualni rezim
	CALL  xLDR23i
	MOV   DPTR,#ALRM_TM
	CALL  xLDR45i
	ORL   A,R4
	JZ    I_TIMEC
	CALL  CMPi
	JZ    I_TIMM1
	JNC   I_TIMEC
I_TIMM1:SETB  ALRM_BB
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	JMP   I_TIMEC

I_TIMEH:MOV   DPTR,#AUXUAL    ; Cekani na SW pri HOLD
	MOVX  A,@DPTR
	JB    ACC.4,I_TIMEH1
	JB    SWEDG_F,I_TIMEC
	CALL  RU_HOLC
	SJMP  I_TIMEC
I_TIMEH1:CLR  SWEDG_F
	SJMP  I_TIMEC

I_TI_GE:SETB  PR_GRER	      ; Chyba gradientu
I_TI_PE:SETB  PR_ENDD	      ; Konec programu
	CLR   %RUN_FL
I_TIMEC:JMP   ITIM_R

; Vstup casoveho preruseni

I_TIME: JNB   %RUN_FL,I_TIMM
	JB    %HOLD_FL,I_TIMEH ; Beh programu
	JB    PR_ENDC,I_TIMEC
	MOV   DPTR,#TIME
	CALL  xLDR23i
	ORL   A,R2
	JZ    I_TI_P3
I_TI_PR:MOV   DPTR,#APROX_B
	CALL  APROXIM
	MOV   F0,C
	JB    %G_BF_FL,I_TI_P1+2
	MOV   DPTR,#APROX_C
	CALL  APROXIM
	ORL   C,F0
	ORL   C,HW_CH_F
	JNC   I_TI_P1
	MOV   DPTR,#GRAD_B
	CALL  xLDR45i
	MOV   DPTR,#GRAD_C
	CALL  xLDR23i
	CALL  ADDi
	MOV   R2,#1
	MOV   R3,#101
	CALL  CMPi
	JNC   I_TI_GE
	CALL  S_GRAD0
I_TI_P1:CLR   F0
	MOV   DPTR,#APROX_F
	CALL  APROXIM
	ORL   C,F0
	ORL   C,HW_CH_F
	JNC   I_TI_P2
	SETB  U_FLOW
	CALL  S_FLOW0
I_TI_P2:CLR   HW_CH_F

I_TI_P3:JB    %HOLD_FL,I_TIMEH
	MOV   DPTR,#TIME
	CALL  xLDR45i
	CALL  GET_LAD
	MOVX  A,@DPTR
	JZ    I_TIMEC
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R1,A
	ANL   A,#03FH
	JZ    I_TIMEC
	INC   DPTR
	CALL  xLDR23i
	CALL  CMPi
	JC    I_TIMEC
	CJNE  R1,#4,I_TI_P4   ; End programu
	CALL  M_KENDS
	SETB  U_FLOW
	CALL  S_FLOW0
	CALL  S_GRAD
	CALL  M_KEND1

	MOV   DPTR,#CYCLE_A
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	MOV   R2,A
	INC   DPTR
	JNZ   I_TI_E1
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
I_TI_E1:MOVX  A,@DPTR
	MOV   R3,A
	INC   DPTR
	CALL  xLDR45i
	CALL  CMPi
	JNC   I_TI_E2
	CLR   M_CYCLE
	SETB  PR_ENDC
	JMP   I_TI_PE
I_TI_E2:CALL  SET_CY2         ; Dalsi cyklus
	SETB  U_CYCLE
	JMP   I_TIMEC

I_TI_P4:CJNE  R1,#1,I_TI_P5   ; Nastaveni prutoku
	MOV   R4,#LOW  FLOW
	MOV   R5,#HIGH FLOW
	MOV   R0,#8
	MOV   R1,#0
	CALL  xxMOVE1
	MOV   DPTR,#APROX_F
	CALL  xMDPPM1
	SETB  HW_CH_F
	JMP   I_TI_PX
I_TI_P5:MOV   A,R1
	MOV   R7,A
	ANL   A,#03FH
	CJNE  A,#2,I_TI_P7    ; Gradienty
	SETB  HW_CH_F
	MOV   A,R7
	JB    ACC.6,I_TI_P6

	MOV   R4,#LOW  GRAD_B
	MOV   R5,#HIGH GRAD_B
	MOV   R0,#8
	MOV   R1,#0
	CALL  xxMOVE1
	MOV   DPTR,#APROX_B
	CALL  xMDPPM1

I_TI_P6:MOV   A,R7
	JB    ACC.7,I_TI_PX
	CALL  GET_LAD
	MOV   A,#12
	CALL  ADDATDP

	MOV   R4,#LOW  GRAD_C
	MOV   R5,#HIGH GRAD_C
	MOV   R0,#8
	MOV   R1,#0
	CALL  xxMOVE1
	MOV   DPTR,#APROX_C
	CALL  xMDPPM1

	SJMP  I_TI_PX
I_TI_P7:CJNE  R1,#3,I_TI_P8   ; Aux output
	MOVX  A,@DPTR
	MOV   R4,A
	CALL  S_AUX
	SJMP  I_TI_PX
I_TI_P8:CJNE  R1,#5,I_TI_P9   ; Wait for mark or switch
	CLR   SWEDG_F
	CALL  RU_HOLP
	SJMP  I_TI_PX
I_TI_P9:CJNE  R1,#6,I_TI_P10  ; Send mark
	MOVX  A,@DPTR
	MOV   R4,A
	CALL  LAN_MRK
	SJMP  I_TI_PX
I_TI_P10:
%IF(%WITH_AUXVALV)THEN(
	CJNE  R1,#7,I_TI_P11  ; Aux valves
	MOVX  A,@DPTR
	JZ    I_TI_PX
	MOV   R4,A
	CALL  S_AUXVALV
	SJMP  I_TI_PX
I_TI_P11:CJNE R1,#8,I_TI_P12  ; Grad valves
	MOVX  A,@DPTR
	MOV   R4,A
	CALL  S_GRDIR
	SJMP  I_TI_PX
)FI
I_TI_P12:
%IF(%PRESS_COR)THEN(
	CJNE R1,#9,I_TI_P13   ; Pressure correction
	CALL  xLDR45i
	MOV   DPTR,#M_COR
	CALL  xSVR45i
	SJMP  I_TI_PX
)FI
I_TI_P13:
%IF(%WITH_AUXVALV AND %WITH_UL_G_BF)THEN(
	CJNE  R1,#10,I_TI_P14 ; Slave aux valves
	MOVX  A,@DPTR
	JZ    I_TI_PX
	SETB  ACC.7
	MOV   DPTR,#GBF_SLAUX
	MOVX  @DPTR,A
	SJMP  I_TI_PX
I_TI_P14:CJNE R1,#11,I_TI_PX  ; Slave grad valves
	MOVX  A,@DPTR
	ORL   A,#0C0H
	MOV   DPTR,#GBF_SLGR
	MOVX  @DPTR,A
	SJMP  I_TI_PX
)FI

I_TI_PX:CALL  NEXT_LN
	CALL  SET_LAD
	JMP   I_TI_P3

; Vypocty aprogimaci v buferu DPTR
; Kdyz zmeni VALUE pak CY = 1

APROXIM:MOV   R6,DPL
	MOV   R7,DPH
	CALL  xLDR45i         ; > APROX. REG
	MOV   A,#6
	CALL  ADDATDP
	MOV   R0,#0
	CALL  xLDR23i         ; > ZBYTEK
	CALL  ADDi
	JNC   APROXI1
	CALL  xLDR23i         ; > CASOVY ROZDIL
	CALL  SUBi
	MOV   R0,#1
APROXI1:MOV   DPL,R6          ; < APROX REG
	MOV   DPH,R7
	CALL  xSVR45i
	MOV   A,R0
	MOV   R2,A
	MOV   R3,#0
	MOV   A,#4
	CALL  ADDATDP
	CALL  xLDR45i         ; > KROK
	CALL  ADDi
	MOV   A,R4
	MOV   R2,A
	MOV   A,R5
	MOV   R3,A
	MOV   DPL,R6
	MOV   DPH,R7
	INC   DPTR
	INC   DPTR
	CALL  xLDR45i         ; > VALUE
	CALL  ADDi
	CALL  xSVR45i         ; < VALUE
	MOV   A,R2
	ORL   A,R3
	CLR   C
	JZ    APROXIR
	SETB  C
APROXIR:RET

xMDPPM1:MOV   A,#0FFH         ; x:[DPTR] = -1
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	INC   DPTR
	RET

; *******************************************************************

%IF (%WITH_UL_G_BF) THEN (
ERR_GBF:CALL   M_STOP
	SETB   GBF_ERRCL
	CLR    GBF_ERR
	%LDMXi(ERR_COD,0FFF5H)
	MOV    DPTR,#ERR_GBFT
	JMP    ERR_DPBDT
)FI

; Hlidani kritickych chyb

ERR_CRY:JB     PRESS_E,ERR_PH
	JB     PRESS_V,ERR_PL
	JB     PR_GRER,ERR_GRAD
	JB     PR_ENDD,PR_ENDO
	JB     ALRM_BB,ALRM_BE
%IF (%EQS(%HW_VERSION,PB)) THEN (
	JB     FL_RERR,ERR_RER
	JB     FL_ENOV,ERR_ENO
)FI
%IF (%WITH_UL_G_BF) THEN (
	JB     GBF_ERR,ERR_GBF
)FI
	CLR    A
	RET

%IF (%EQS(%HW_VERSION,PB)) THEN (
ERR_RER:CALL   M_STOP
	CLR    FL_RERR
	%LDMXi(ERR_COD,0FFF0H)
	MOV    DPTR,#ERR_RERT
	JMP    ERR_DPBDT
ERR_ENO:CALL   M_STOP
	CLR    FL_ENOV
	%LDMXi(ERR_COD,0FFF1H)
	MOV    DPTR,#ERR_RERT
	JMP    ERR_DPBDT
)FI

ALRM_BE:CLR    ALRM_BB
	MOV    DPTR,#ALRM_BT
	JMP    ERR_DPBDT

ERR_GRAD:CLR   PR_GRER
	%LDMXi(ERR_COD,0FFF2H)
	MOV    DPTR,#ERR_GRT
	JMP    ERR_DPBDT

ERR_PL: JNB    PRESS_S,ERR_PL1
	CALL   M_STOP
ERR_PL1:CLR    PRESS_V
	%LDMXi(ERR_COD,0FFF3H)
	MOV    DPTR,#ERR_PLT
	JMP    ERR_DPBDT
ERR_PH: CALL   M_STOP
	CLR    PRESS_E
	%LDMXi(ERR_COD,0FFF4H)
	MOV    DPTR,#ERR_PHT
	JMP    ERR_DPBDT
PR_ENDO:CLR    PR_ENDD
	MOV    DPTR,#T_PREND
	JNB    FL_DIPR,ERR_NDI
	JMP    ERR_DPT

ERR_DPBDT:JNB  FL_DIPR,ERR_NDI
	JMP    ERR_DPB
ERR_NDI:RET

; *******************************************************************

; Jednotlive vypisy na obrazovku

WR_TIME:MOV    DPTR,#TIME     ; Vypis casu
	CALL   xLDR45i        ; X.XX
	ANL    A,R4
	CPL    A
	JZ     WR_TIM2
	JNB    %HOLD_FL,WR_TIM1
	MOV    A,R4
	JB     ACC.0,WR_TIM2
WR_TIM1:MOV    R7,#042H
	JMP    PRINTi
WR_TIM2:MOV    DPTR,#WR_TIMT
	JMP    cPRINT
WR_TIMT:DB     '    ',0


WR_PRESS:MOV   DPTR,#PRESS    ; Vypis tlaku
	MOV    R7,#041H       ; XX.X
WR_PRE1:CALL   xLDR45i
	MOV    A,#1
	CALL   SHRi
	JMP    PRINTi

WR_CONC:CALL   S_W_RET
	MOV    DPTR,#GRAD_B
	CALL   xLDR45i
	MOV    DPTR,#GRAD_C
	CALL   xLDR23i
	CALL   ADDi
	CALL   NEGi
	MOV    R2,#0FFH
	MOV    R3,#100
	CALL   ADDi
	MOV    A,R5
	JNB    ACC.7,WR_CON0
	CLR    A
WR_CON0:CALL   WR_CONS+1
	SJMP   WR_CON1+3

WR_CON1:LCALL  S_W_RET
	MOV    DPTR,#GRAD_B+1
	CALL   WR_CONS
	JB     %G_BF_FL,WR_CONR
	MOV    DPTR,#GRAD_C+1
	CALL   WR_CONS
WR_CONR:MOV    DPTR,#WR_RETB
	JMP    xJMPDPP

WR_CONS:MOVX   A,@DPTR
	MOV    R7,#040H
	MOV    R4,A
	MOV    R5,#0
	JMP    PRINTi

GBF_POT:JB     %G_BF_FL,SET_PGR
	JMP    SET_POT
SET_PGR:RET

; Korekce zadani gradientu

SET_PGB:MOV    R7,#13
	MOV    R2,#40H
	SJMP   SET_PG1
SET_PGC:MOV    R7,#5
	MOV    R2,#80H
SET_PG1:MOV    A,R2
	MOV    R3,A
	CALL   SET_IMP
	MOV    R6,A
	CALL   GET_LAD
	MOV    A,R7
	CALL   ADDATDP
	CLR    C
	MOV    A,#100
	SUBB   A,R5
	MOV    R7,A
	MOVX   A,@DPTR
	SUBB   A,R7
	JZ     SET_PGR
	JC     SET_PGR
	MOV    A,R7
	MOVX   @DPTR,A
	MOV    A,R6
	ANL    A,#0C0H
	JNZ    SET_PGR
	JB     %G_BF_FL,SET_PGR
	SETB   F0

S_GRA_E:MOV    DPTR,#ERR_SGR
	JMP    ERR_DPT

S_GRA_C:MOV    DPTR,#GRAD_B+1
	MOVX   A,@DPTR
	MOV    DPTR,#GRAD_C
	SJMP   S_GRA_1
S_GRA_B:MOV    DPTR,#GRAD_C+1
	MOVX   A,@DPTR
	MOV    DPTR,#GRAD_B
S_GRA_1:ADD    A,R5
	ADD    A,#-101
	JC     S_GRA_E
	MOV    R4,#0
	MOV    C,EA
	CLR    EA
	CALL   xSVR45i
	MOV    EA,C
	JMP    S_GRAD

WR_AUX: MOV    DPTR,#AUXUAL   ; Vypis vystupu a mimo
	MOVX   A,@DPTR        ; PROG i vstupu
WR_AUX0:MOV    R0,A           ; 1234 56
	MOV    R1,#'0'
	MOV    R2,#4
	MOV    R3,#1
WR_AUX1:INC    R1
	MOV    A,R0
	RRC    A
	MOV    R0,A
	MOV    A,#'_'
	JNC    WR_AUX2
	MOV    A,R1
WR_AUX2:CALL   LCDWR
	DJNZ   R2,WR_AUX1
	JB     %PROG_FL,WR_AUXR
	DJNZ   R3,WR_AUXR
	MOV    A,#' '
	MOV    R2,#3
	JMP    WR_AUX2

WR_AUXR:RET

WR_CYCLE:
	MOV    DPTR,#CYCLE_A
	MOV    R7,#030H
	CALL   PRINTiP
	CALL   xLDR23i
	CALL   xLDR45i
	CALL   SUBi
	MOV    R7,#050H
	JMP    PRINTi

WR_CYC1:MOV    DPTR,#CYCLE
	MOV    R7,#030H
	JMP    PRINTiP

WR_FLOW:MOV    DPTR,#FLOW     ; Vypis prutoku
	MOV    R7,#040H OR %C_FLOW_FDIG ;     X.XX
	JMP    PRINTiP

WR_PHPL:CALL   S_W_RET
	MOV    A,#LCD_HOM+045H
	CALL   LCDWCOM
	MOV    DPTR,#PRESS_H  ; Vypis PH
	MOV    R7,#021H       ;       XX
	CALL   WR_PRE1
	MOV    A,#' '
	CALL   LCDWR
	MOV    DPTR,#PRESS_L  ; Vypis PL
	MOV    R7,#021H       ;       xx
	CALL   WR_PRE1
	MOV    DPTR,#WR_RETB
	JMP    xJMPDPP

WR_SCOM:MOV    A,#LCD_HOM+40H ; 'Adr kBd Form Set'
	CALL   LCDWCOM
	MOV    DPTR,#uL_ADR
	MOVX   A,@DPTR
	MOV    R4,A
	MOV    R5,#0
	MOV    R7,#20H
	CALL   PRINTi
	MOV    A,#LCD_HOM+43H
	CALL   LCDWCOM
	MOV    DPTR,#uL_SPD
	MOVX   A,@DPTR
	CPL    A
	INC    A
	MOV    R2,A
	MOV    R3,#0
	MOV    R4,#LOW 5760
	MOV    R5,#HIGH 5760
	CALL   DIVi
	MOV    R7,#42H
	CALL   PRINTi
	MOV    A,#LCD_HOM+48H
	CALL   LCDWCOM
	MOV    DPTR,#uL_FORM
	MOVX   A,@DPTR
	RET

WR_ALRM:CALL   LCDNBUS
	MOV    A,#LCD_HOM+45H
	CALL   LCDWCO1
	MOV    DPTR,#ALRM_TM
	MOV    R7,#42H
	JMP    PRINTiP

WR_TIMP:CALL   LCDNBUS
	MOV    A,#LCD_HOM+40H
	CALL   LCDWCO1
	CALL   GET_LAD
	INC    DPTR
	INC    DPTR
	CALL   xLDR45i
	INC    DPTR
	INC    DPTR
	MOV    R7,#042H
	JMP    PRINTi

%IF(%PRESS_COR)THEN( ; Tlakova korekce
WR_PRCOR:
	MOV    A,#LCD_HOM+44H
	CALL   LCDWCO1
	MOV    DPTR,#M_COR
	CALL   xLDR45i
	MOV    R7,#041H
	JMP    PRINTi
)FI

%IF(%WITH_UL_G_BF) THEN (
WR_GBF2:
	MOV    A,#LCD_HOM+40H
	CALL   LCDWCO1
	MOV    DPTR,#T_GBFBV
	JNB    %G_BF_FL,WR_GBF2A
	MOV    DPTR,#T_GBFBF
WR_GBF2A:
	CALL   cPRINT
	MOV    A,#LCD_HOM+4BH
	CALL   LCDWCO1
	MOV    DPTR,#GBF_SLA
	CALL   xLDR45i
	MOV    R7,#020H
	JMP    PRINTi
)FI

WR_P_1: CALL   WR_TIMP
	CALL   LCDNBUS
	MOV    A,#LCD_HOM+46H
	CALL   LCDWCO1
	MOV    R7,#040H OR %C_FLOW_FDIG
	JMP    PRINTiP

WR_P_2: CALL   WR_TIMP
	CALL   LCDNBUS
	MOV    A,#LCD_HOM+45H
	CALL   LCDWCO1
	MOV    R4,#040H
	MOV    R3,#4
	JB     %G_BF_FL,WR_P_2S
	CALL   WR_P_2S
	CALL   LCDNBUS
	MOV    A,#LCD_HOM+4AH
	CALL   LCDWCO1
	MOV    R4,#080H
	MOV    R3,#12

WR_P_2S:CALL   GET_LAD
	INC    DPTR
	MOVX   A,@DPTR
	ANL    A,R4
	JNZ    WR_P_2I
	MOV    A,R3
	CALL   ADDATDP
	JMP    WR_CONS

WR_P_2I:MOV    DPTR,#WR_P_2T
	JMP    cPRINT
WR_P_2T:DB     '  **',0

WR_P_3: CALL   WR_TIMP
	CALL   LCDNBUS
	MOV    A,#LCD_HOM+47H
	CALL   LCDWCO1
	MOVX   A,@DPTR
	JMP    WR_AUX0

; Send mark
WR_P_6:	CALL   WR_TIMP
	CALL   LCDNBUS
	MOV    A,#LCD_HOM+4DH
	CALL   LCDWCO1
	MOV    R7,#020H
	JMP    PRINTiP

%IF(%WITH_AUXVALV)THEN(
; Aux valves
WR_P_7:	CALL   WR_TIMP
	CALL   LCDNBUS
	MOV    A,#LCD_HOM+4BH
	CALL   LCDWCO1
	MOV    R7,#020H
	JMP    PRINTiP

; Grad valves
WR_P_8:	CALL   WR_TIMP
	CALL   LCDNBUS
	MOV    A,#LCD_HOM+4CH
	CALL   LCDWCO1
	MOVX   A,@DPTR
	ADD    A,#'A'
	JMP    LCDWR
)FI

%IF(%PRESS_COR)THEN(
; Press correction
WR_P_9:	CALL   WR_TIMP
	CALL   LCDNBUS
	MOV    A,#LCD_HOM+48H
	CALL   LCDWCO1
	MOV    R7,#041H
	JMP    PRINTiP
)FI

; *******************************************************************

S_W_RET:MOV    DPTR,#WR_RETB
	POP    ACC
	MOV    R3,A
	POP    ACC
	MOV    R2,A
	POP    ACC
	MOV    R1,A
	POP    ACC
	MOVX   @DPTR,A
	INC    DPTR
	MOV    A,R1
	MOVX   @DPTR,A
	MOV    DPL,R2
	MOV    DPH,R3
	CLR    A
	JMP    @A+DPTR

WR_INTE:MOV    DPL,R4
	MOV    DPH,R5
	MOV    C,EA
	CLR    EA
	CALL   xLDR45i
	MOV    EA,C
	CALL   LCDNBUS
	MOV    A,R6
	CALL   LCDWR1
	JMP    PRINTi

; *******************************************************************

$NOLIST

LNT_DIR:DB     LOW LNT_0,HIGH LNT_0
	DB     LOW LNT_1,HIGH LNT_1
	DB     LOW LNT_2,HIGH LNT_2
	DB     LOW LNT_3,HIGH LNT_3
	DB     LOW LNT_4,HIGH LNT_4
	DB     LOW LNT_5,HIGH LNT_5
	DB     LOW LNT_6,HIGH LNT_6
    %IF(%WITH_AUXVALV)THEN(
	DB     LOW LNT_7,HIGH LNT_7
	DB     LOW LNT_8,HIGH LNT_8
    )ELSE(
	DB     LOW LNT_UNK,HIGH LNT_UNK
	DB     LOW LNT_UNK,HIGH LNT_UNK
    )FI
    %IF(%PRESS_COR)THEN(
	DB     LOW LNT_9,HIGH LNT_9
    )ELSE(
	DB     LOW LNT_UNK,HIGH LNT_UNK
    )FI
    %IF(%WITH_AUXVALV AND %WITH_UL_G_BF)THEN(
	DB     LOW LNT_10,HIGH LNT_10
	DB     LOW LNT_11,HIGH LNT_11
    )FI
    

LNT_MAX SET    ($-LNT_DIR)/2

; Typy radek programu
;
; +0  .. delka tohoto typu radky
; +1  .. pocet pozic N
; +2  .. prvni radka F_LINE
; +4  .. vypis radky DP_FLEX
; +6  .. N * ukazatel na POT

; null line - End of program
LNT_0:  DB     16,1
	DB     LOW T_PRNUL,HIGH T_PRNUL
	DB     LOW WR_RET,HIGH WR_RET
	DB     LOW POT_P0,HIGH POT_P0
; Time flow line
LNT_1:  DB     12,2
	DB     LOW T_PR_1,HIGH T_PR_1
	DB     LOW WR_P_1,HIGH WR_P_1
	DB     LOW POT_P1,HIGH POT_P1
	DB     LOW POT_P2,HIGH POT_P2
; Time  grad A grad B
LNT_2:  DB     20,3
	DB     LOW T_PR_2,HIGH T_PR_2
	DB     LOW WR_P_2,HIGH WR_P_2
	DB     LOW POT_P1,HIGH POT_P1
	DB     LOW POT_P3,HIGH POT_P3
	DB     LOW POT_P4,HIGH POT_P4
; Aux output
LNT_3:  DB     5,2
	DB     LOW T_PR_3,HIGH T_PR_3
	DB     LOW WR_P_3,HIGH WR_P_3
	DB     LOW POT_P1,HIGH POT_P1
	DB     LOW POT_P5,HIGH POT_P5
; ** End **
LNT_4:  DB     4,1
	DB     LOW T_PR_4,HIGH T_PR_4
	DB     LOW WR_TIMP,HIGH WR_TIMP
	DB     LOW POT_P1,HIGH POT_P1
; Wait for mark or switch
LNT_5:	DB     6,1
	DB     LOW T_PR_5,HIGH T_PR_5
	DB     LOW WR_TIMP,HIGH WR_TIMP
	DB     LOW POT_P1,HIGH POT_P1
; Send mark
LNT_6:	DB     6,2
	DB     LOW T_PR_6,HIGH T_PR_6
	DB     LOW WR_P_6,HIGH WR_P_6
	DB     LOW POT_P1,HIGH POT_P1
	DB     LOW POT_P6,HIGH POT_P6
%IF(%WITH_AUXVALV)THEN(
; Aux valves
LNT_7:	DB     6,2
	DB     LOW T_PR_7,HIGH T_PR_7
	DB     LOW WR_P_7,HIGH WR_P_7
	DB     LOW POT_P1,HIGH POT_P1
	DB     LOW POT_P7,HIGH POT_P7
; Grad valves
LNT_8:	DB     6,2
	DB     LOW T_PR_8,HIGH T_PR_8
	DB     LOW WR_P_8,HIGH WR_P_8
	DB     LOW POT_P1,HIGH POT_P1
	DB     LOW POT_P8,HIGH POT_P8
)FI
%IF(%PRESS_COR)THEN(
; Press correction
LNT_9:	DB     6,2
	DB     LOW T_PR_9,HIGH T_PR_9
	DB     LOW WR_P_9,HIGH WR_P_9
	DB     LOW POT_P1,HIGH POT_P1
	DB     LOW POT_P9,HIGH POT_P9
)FI
%IF(%WITH_AUXVALV AND %WITH_UL_G_BF)THEN(
; Slave aux valves
LNT_10:	DB     6,2
	DB     LOW T_PR_10,HIGH T_PR_10
	DB     LOW WR_P_7,HIGH WR_P_7
	DB     LOW POT_P1,HIGH POT_P1
	DB     LOW POT_P7,HIGH POT_P7
; Slave grad valves
LNT_11:	DB     6,2
	DB     LOW T_PR_11,HIGH T_PR_11
	DB     LOW WR_P_8,HIGH WR_P_8
	DB     LOW POT_P1,HIGH POT_P1
	DB     LOW POT_P8,HIGH POT_P8
)FI
; unknown line type
LNT_UNK:DB     16,1
	DB     LOW T_PRUNK,HIGH T_PRUNK
	DB     LOW WR_RET,HIGH WR_RET
	DB     LOW POT_P0,HIGH POT_P0

; Vypisy na displej

; plati pro 2.010,2.040,3.020,3.030,3.040,4.060

WRT_1  :DB     LCD_CLR,LOW WR_FLIN,HIGH WR_FLIN
	DB     LCD_HOM+040H,LOW WR_TIME,HIGH WR_TIME
	DB     LCD_HOM+04CH,LOW WR_PRESS,HIGH WR_PRESS
	DB     LCD_HOM+044H,LOW WR_CONC,HIGH WR_CONC
	DB     LCD_HOM+047H,LOW WR_AUX,HIGH WR_AUX
	DB     LCD_HOM+040H,LOW WR_CYCLE,HIGH WR_CYCLE
	DB     LCD_HOM+040H,LOW WR_FLOW,HIGH WR_FLOW
	DB     LCD_HOM+040H,LOW WR_FLEX,HIGH WR_FLEX

; plati pro 3.010

WRT_2  :DB     LCD_CLR,LOW WR_FLIN,HIGH WR_FLIN
	DB     LCD_HOM+040H,LOW WR_TIME,HIGH WR_TIME
	DB     LCD_HOM+04BH,LOW WR_PRESS,HIGH WR_PRESS
	DB     LCD_HOM+044H,LOW WR_CONC,HIGH WR_CONC
	DB     LCD_HOM+047H,LOW WR_AUX,HIGH WR_AUX
	DB     LCD_HOM+045H,LOW WR_CYCLE,HIGH WR_CYCLE
	DB     LCD_HOM+045H,LOW WR_FLOW,HIGH WR_FLOW
	DB     LCD_HOM+040H,LOW WR_FLEX,HIGH WR_FLEX

; plati pro 4.010

WRT_3  :DB     LCD_CLR,LOW WR_FLIN,HIGH WR_FLIN
	DB     LCD_HOM+040H,LOW WR_TIME,HIGH WR_TIME
	DB     LCD_HOM+04CH,LOW WR_PRESS,HIGH WR_PRESS
	DB     LCD_HOM+046H,LOW WR_CON1,HIGH WR_CON1
	DB     LCD_HOM+047H,LOW WR_AUX,HIGH WR_AUX
	DB     LCD_HOM+040H,LOW WR_CYCLE,HIGH WR_CYCLE
	DB     LCD_HOM+04CH,LOW WR_FLOW,HIGH WR_FLOW
	DB     LCD_HOM,LOW WR_FLEX,HIGH WR_FLEX

; Jednotlive displeje

; plati pro 2.010

DIT_1:  DB    10100011B       ; F_LINE,PRESS,FLOW,WR_FLEX
	DB    LOW T_MANP,HIGH T_MANP
	DB    LOW WRT_1,HIGH WRT_1
	DB    LOW WR_PHPL,HIGH WR_PHPL
	DB    LOW POT_1,HIGH POT_1

; plati pro 2.040

DIT_2:  DB    10001000B       ; F_LINE,AUX
        DB    LOW T_MANA,HIGH T_MANA
	DB    LOW WRT_1,HIGH WRT_1
	DB    LOW WR_RET,HIGH WR_RET
	DB    LOW POT_4,HIGH POT_4

; koncentrace v manualnim modu

DIT_3:  DB    10010010B       ; F_LINE,FLOW,CONC
	DB    LOW T_MANC,HIGH T_MANC
	DB    LOW WRT_1,HIGH WRT_1
	DB    LOW WR_RET,HIGH WR_RET
	DB    LOW POT_5,HIGH POT_5

; nastaveni seriove komunikace

DIT_4:  DB    10000001B       ; F_LINE,WR_FLEX
	DB    LOW T_MSCOM,HIGH T_MSCOM
	DB    LOW WRT_1,HIGH WRT_1
	DB    LOW WR_SCOM,HIGH WR_SCOM
	DB    LOW POT_8,HIGH POT_8

; nastaveni cycle v manualnim rezimu

DIT_5:  DB    10000001B       ; F_LINE,WR_CYC1
	DB    LOW T_RUNR,HIGH T_RUNR
	DB    LOW WRT_1,HIGH WRT_1
	DB    LOW WR_CYC1,HIGH WR_CYC1
	DB    LOW POT_10,HIGH POT_10

; uzivatelsky budik

DIT_6:  DB    11000001B       ; F_LINE,WR_TIME,WR_ARLM
	DB    LOW T_ALRM,HIGH T_ALRM
	DB    LOW WRT_1,HIGH WRT_1
	DB    LOW WR_ALRM,HIGH WR_ALRM
	DB    LOW POT_11,HIGH POT_11

; vyber programu z menu

DIT_7:  DB    10000001B       ; F_LINE,WR_PROG
	DB    LOW T_PRSL,HIGH T_PRSL
	DB    LOW WRT_1,HIGH WRT_1
	DB    LOW WR_PROG,HIGH WR_PROG
	DB    LOW POT_13,HIGH POT_13

%IF(%PRESS_COR)THEN(
; nastaveni tlakove korekce
DIT_8:  DB    10000001B       ; F_LINE,WR_PROG
	DB    LOW T_PRCOR,HIGH T_PRCOR
	DB    LOW WRT_1,HIGH WRT_1
	DB    LOW WR_PRCOR,HIGH WR_PRCOR
	DB    LOW POT_15,HIGH POT_15
)FI

%IF(%WITH_UL_G_BF) THEN (
; Nastaveni master/slave
DIT_9:  DB    10000001B       ; F_LINE, WR_GBF2
	DB    LOW T_GBF2,HIGH T_GBF2
	DB    LOW WRT_1,HIGH WRT_1
	DB    LOW WR_GBF2,HIGH WR_GBF2
	DB    LOW POT_16,HIGH POT_16
)FI

; pri run vypisuje cas a prutok

DIT_R1: DB    11100010B       ; F_LINE,WR_TIME,WR_PRESS,WR_FLOW
	DB    LOW T_RUNF,HIGH T_RUNF
	DB    LOW WRT_2,HIGH WRT_2
	DB    LOW WR_RET,HIGH WR_RET
	DB    LOW POT_R1,HIGH POT_R1

DIT_R2: DB    11001000B       ; F_LINE,WR_AUX
	DB    LOW T_RUNA,HIGH T_RUNA
	DB    LOW WRT_2,HIGH WRT_2
	DB    LOW WR_RET,HIGH WR_RET
	DB    LOW POT_R1,HIGH POT_R1

DIT_R3: DB    11010000B       ; F_LINE,WR_TIME,WR_CONC
	DB    LOW T_RUNC,HIGH T_RUNC
	DB    LOW WRT_2,HIGH WRT_2
	DB    LOW WR_RET,HIGH WR_RET
	DB    LOW POT_R1,HIGH POT_R1

DIT_R4: DB    10000101B       ; F_LINE,WR_CYCLE,WR_CYC1
	DB    LOW T_RUNR,HIGH T_RUNR
	DB    LOW WRT_2,HIGH WRT_2
	DB    LOW WR_CYC1,HIGH WR_CYC1
	DB    LOW POT_R2,HIGH POT_R2

%IF(%WITH_UL_G_BF) THEN (
DIT_GBF1:
	DB    10100010B       ; F_LINE,PRESS,FLOW
	DB    LOW T_GBF1,HIGH T_GBF1
	DB    LOW WRT_1,HIGH WRT_1
	DB    LOW WR_PHPL,HIGH WR_PHPL
	DB    LOW POT_GBF1,HIGH POT_GBF1
)FI

; Jednotlive pozice cursoru
;
; +0  .. ukazatel pro rutinu zpracovani klaves
; +2  .. ukazatel na prislusny help
; +4  .. editovana pozice na displeji
; +5  .. ukazatel na vstupni rutinu
; +7  .. ukazatel na menena data
; +9  .. ukazatel pro vstupni rutinu
; +10 .. akce volana po cteni

; Pozice 2.011

POT_1:  DB    LOW SFT_1,HIGH SFT_1
	DB    LOW P2011_H,HIGH P2011_H
	DB    040H
	DB    LOW R_INTEG,HIGH R_INTEG
	DB    LOW FLOW,HIGH FLOW
	DB    LOW RT_FLOW,HIGH RT_FLOW
	DB    LOW S_FLOW,HIGH S_FLOW

; Pozice 2.012

POT_2:  DB    LOW SFT_2,HIGH SFT_2
	DB    LOW P2012_H,HIGH P2012_H
	DB    045H
	DB    LOW R_INTEG,HIGH R_INTEG
	DB    LOW PRESS_H,HIGH PRESS_H
	DB    LOW RT_PRES,HIGH RT_PRES
	DB    0,0

; Pozice 2.013

POT_3:  DB    LOW SFT_3,HIGH SFT_3
	DB    LOW P2012_H,HIGH P2012_H
	DB    048H
	DB    LOW R_INTEG,HIGH R_INTEG
	DB    LOW PRESS_L,HIGH PRESS_L
	DB    LOW RT_PRES,HIGH RT_PRES
	DB    0,0

; Pozice 2.040

POT_4:  DB    LOW SFT_4,HIGH SFT_4
	DB    LOW P2040_H,HIGH P2040_H
	DB    04AH
	DB    LOW R_CHAR,HIGH R_CHAR
	DB    LOW AUXUAL,HIGH AUXUAL
	DB    0,0
	DB    LOW S_AUX,HIGH S_AUX

; koncentrace v manualnim modu

POT_5:  DB    LOW SFT_5,HIGH SFT_5
	DB    LOW MCON_HL,HIGH MCON_HL
	DB    049H
	DB    LOW R_INTEG,HIGH R_INTEG
	DB    LOW 0,HIGH 0
	DB    LOW RT_GR,HIGH RT_GR
	DB    LOW S_GRA_B,HIGH S_GRA_B

POT_6:  DB    LOW SFT_6,HIGH SFT_6
	DB    LOW MCON_HL,HIGH MCON_HL
	DB    04DH
	DB    LOW R_INTEG,HIGH R_INTEG
	DB    LOW 0,HIGH 0
	DB    LOW RT_GR,HIGH RT_GR
	DB    LOW S_GRA_C,HIGH S_GRA_C

POT_7:  DB    LOW SFT_7,HIGH SFT_7
	DB    LOW P2011_H,HIGH P2011_H
	DB    040H
	DB    LOW R_INTEG,HIGH R_INTEG
	DB    LOW FLOW,HIGH FLOW
	DB    LOW RT_FLOW,HIGH RT_FLOW
	DB    LOW S_FLOW,HIGH S_FLOW

POT_8:  DB    LOW SFT_8,HIGH SFT_8
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    040H
	DB    LOW R_INTEG,HIGH R_INTEG
	DB    LOW uL_ADR,HIGH uL_ADR
	DB    LOW RT_uL_A,HIGH RT_uL_A
	DB    0,0

POT_9:  DB    LOW SFT_9,HIGH SFT_9
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    046H
	DB    LOW R_CHAR,HIGH R_CHAR
	DB    LOW uL_SPD,HIGH uL_SPD
	DB    0,0
	DB    0,0

POT_10: DB    LOW SFT_0,HIGH SFT_0
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    040H
	DB    LOW R_INTEG,HIGH R_INTEG
	DB    0,0
	DB    LOW RT_CYCL,HIGH RT_CYCL
	DB    LOW CYC_NEW,HIGH CYC_NEW

POT_11: DB    LOW SFT_10,HIGH SFT_10
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    045H
	DB    LOW R_INTEG,HIGH R_INTEG
	DB    LOW ALRM_TM,HIGH ALRM_TM
	DB    LOW RT_TIME,HIGH RT_TIME
	DB    0,0

POT_12: DB    LOW SFT_11,HIGH SFT_11
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    04BH
	DB    LOW R_CHAR,HIGH R_CHAR

POT_13: DB    LOW SFT_13,HIGH SFT_13
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    042H
	DB    LOW R_CHAR,HIGH R_CHAR
	DB    LOW PROG_NU,HIGH PROG_NU
	DB    0,0
	DB    LOW SET_PROG,HIGH SET_PROG

POT_14: DB    LOW SFT_12,HIGH SFT_12
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    044H
	DB    LOW R_TEXTP,HIGH R_TEXTP
	DB    0,0
	DB    10,0
	DB    LOW SET_PROG,HIGH SET_PROG

%IF(%PRESS_COR)THEN( ; Tlakova korekce
POT_15: DB    LOW SFT_0,HIGH SFT_0
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    044H
	DB    LOW R_INTEG,HIGH R_INTEG
	DB    LOW M_COR,HIGH M_COR
	DB    LOW RT_PRCOR,HIGH RT_PRCOR
	DB    0,0
)FI

%IF(%WITH_UL_G_BF) THEN ( ; Nastaveni master slave
POT_16: DB    LOW SFT_15,HIGH SFT_15
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    040H
	DB    LOW R_CHAR,HIGH R_CHAR
	DB    LOW GBF_MODE,HIGH GBF_MODE
	DB    0,0
	DB    0,0

POT_17: DB    LOW SFT_14,HIGH SFT_14
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    04BH
	DB    LOW R_INTEG,HIGH R_INTEG
	DB    LOW GBF_SLA,HIGH GBF_SLA
	DB    LOW RT_uL_A,HIGH RT_uL_A
	DB    0,0
)FI

POT_P0: DB    LOW SFT_P1,HIGH SFT_P1
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    040H
	DB    LOW R_CHAR,HIGH R_CHAR
	DB    0,0
	DB    0,0

POT_P1: DB    LOW SFT_P1,HIGH SFT_P1
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    040H
	DB    LOW R_INTEG,HIGH R_INTEG
	DB    2,0
	DB    LOW RT_TIME,HIGH RT_TIME
	DB    LOW SP_TIME,HIGH SP_TIME

POT_P2: DB    LOW SFT_P1,HIGH SFT_P1
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    046H
	DB    LOW R_INTEG,HIGH R_INTEG
	DB    4,0
	DB    LOW RT_FLOW,HIGH RT_FLOW
	DB    0,0

POT_P3: DB    LOW SFT_P2,HIGH SFT_P2
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    046H
	DB    LOW R_INTEG,HIGH R_INTEG
	DB    4,0
	DB    LOW RT_GR,HIGH RT_GR
	DB    LOW SET_PGB,HIGH SET_PGB

POT_P4: DB    LOW SFT_P3,HIGH SFT_P3
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    04BH
	DB    LOW R_INTEG,HIGH R_INTEG
	DB    12,0
	DB    LOW RT_GR,HIGH RT_GR
	DB    LOW SET_PGC,HIGH SET_PGC

POT_P5: DB    LOW SFT_P4,HIGH SFT_P4
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    04AH
	DB    LOW R_CHAR,HIGH R_CHAR
	DB    4,0
	DB    0,0
	DB    0,0

POT_P6: DB    LOW SFT_P1,HIGH SFT_P1
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    04DH
	DB    LOW R_INTEG,HIGH R_INTEG
	DB    4,0
	DB    LOW RT_uL_A,HIGH RT_uL_A
	DB    0,0

%IF(%WITH_AUXVALV)THEN(
POT_P7: DB    LOW SFT_P1,HIGH SFT_P1
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    04BH
	DB    LOW R_INTEG,HIGH R_INTEG
	DB    4,0
	DB    LOW RT_AUXV,HIGH RT_AUXV
	DB    0,0

POT_P8: DB    LOW SFT_P8,HIGH SFT_P8
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    04CH
	DB    LOW R_CHAR,HIGH R_CHAR
	DB    4,0
	DB    0,0
	DB    0,0
)FI
%IF(%PRESS_COR)THEN(
POT_P9: DB    LOW SFT_P1,HIGH SFT_P1
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    048H
	DB    LOW R_INTEG,HIGH R_INTEG
	DB    4,0
	DB    LOW RT_PRCOR,HIGH RT_PRCOR
	DB    0,0
)FI

POT_R1: DB    LOW SFT_R1,HIGH SFT_R1
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    020H
	DB    LOW RU_CH_R,HIGH RU_CH_R
	DB    4,0
	DB    0,0
	DB    0,0

POT_R2: DB    LOW SFT_R1,HIGH SFT_R1
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    040H
	DB    LOW RU_IT_R,HIGH RU_IT_R
	DB    LOW CYCLE,HIGH CYCLE
	DB    LOW RT_CYCL,HIGH RT_CYCL
	DB    LOW CYC_NEW,HIGH CYC_NEW


%IF(%WITH_UL_G_BF) THEN (
POT_GBF1:
	DB    LOW SFT_GBF1,HIGH SFT_GBF1
	DB    LOW M_M_NH,HIGH M_M_NH
	DB    020H
	DB    LOW GBF_CH_R,HIGH GBF_CH_R
	DB    4,0
	DB    0,0
	DB    0,0
)FI

; Cteni jednotlivych cisel
;
; +0  .. SLLLMMDD - S znamenkove,LLL pocet pozic,DD desetin
;                   pri MM=0 nic,1 vys*K,2 vys*K/256,3 vys*K/65536
; +1  .. spodni limit
; +3  .. horni limit
; +5  .. konstanta nasobeni
; +7  .. hlaska pri chybe

RT_FLOW:DB    040H OR %C_FLOW_FDIG
	DB    LOW 0,HIGH 0
	DB    LOW (%C_MAX_FLOW*%C_FLOW_FMUL),HIGH (%C_MAX_FLOW*%C_FLOW_FMUL)
	DB    0,0
	DB    LCD_HOME,'Out of range    ',0
	DB    LCD_HOME,'Value 0.01 to %C_MAX_FLOW ',0,0

RT_PRES:DB    024H
	DB    LOW 0,HIGH 0
	DB    LOW %C_MAX_PRESS,HIGH %C_MAX_PRESS
	DB    LOW 20,HIGH 20
	DB    LCD_HOME,'Out of range    ',0
	DB    LCD_HOME,'Maximum  %C_MAX_PRESS MPa ',0,0

RT_GR  :DB    034H
	DB    LOW 0,HIGH 0
	DB    LOW 100,HIGH 100
	DB    LOW 256,HIGH 256
	DB    LCD_HOME,'Out of range    ',0
	DB    LCD_HOME,'Value 1 to 100  ',0,0

RT_uL_A:DB    020H
	DB    LOW 1,HIGH 1
	DB    LOW 99,HIGH 99
	DB    0,0
	DB    LCD_HOME,'Out of range    ',0
	DB    LCD_HOME,'Range 1 to 99',0,0

%IF(%WITH_AUXVALV)THEN(
RT_AUXV:DB    020H
	DB    LOW 1,HIGH 1
	DB    LOW 8,HIGH 8
	DB    0,0
	DB    LCD_HOME,'Out of range    ',0
	DB    LCD_HOME,'Range 1 to 8    ',0,0
)FI

RT_TIME:DB    042H
	DB    LOW 0,HIGH 0
	DB    LOW 0FFFEH,HIGH 0FFFEH
	DB    0,0
	DB    LCD_HOME,'Out of range    ',0
	DB    LCD_HOME,'Value 0. to 600.',0,0

RT_CYCL:DB    030H
	DB    LOW 0,HIGH 0
	DB    LOW 999,HIGH 999
	DB    0,0
	DB    LCD_HOME,'Out of range    ',0
	DB    LCD_HOME,'Value 0 to 999  ',0,0

RT_PRCOR:
	DB    041H
	DB    LOW 0,HIGH 0
	DB    LOW 400,HIGH 400
	DB    0,0
	DB    LCD_HOME,'Out of range    ',0
	DB    LCD_HOME,'Value 0. to 40.',0,0

; Tabulky funkci

%IF(%WITH_UL_G_BF) THEN (
SFT_15: DB    K_UP
	DB    0,0
	DB    LOW M_MOGF,HIGH M_MOGF

	DB    K_1
	DB    0,0
	DB    LOW M_MOGF,HIGH M_MOGF

	DB    K_DOWN
	DB    0,0
	DB    LOW M_MOGV,HIGH M_MOGV

	DB    K_0
	DB    0,0
	DB    LOW M_MOGV,HIGH M_MOGV

SFT_14:	DB    K_LEFT
	DB    LOW POT_16,HIGH POT_16
	DB    LOW SET_POT,HIGH SET_POT

	DB    K_RIGHT
	DB    LOW POT_17,HIGH POT_17
	DB    LOW SET_POT,HIGH SET_POT

	DB    0FFH
	DB    LOW SFT_0,HIGH SFT_0
)FI

SFT_13: DB    K_DOWN
	DB    251,250
	DB    LOW UP_BDP,HIGH UP_BDP

	DB    K_UP
	DB    0,1
	DB    LOW DO_BDP,HIGH DO_BDP

SFT_12:	DB    K_INS
	DB    0,0
	DB    LOW INS_PRG,HIGH INS_PRG

	DB    K_DEL
	DB    0,0
	DB    LOW DEL_PRG,HIGH DEL_PRG

	DB    K_ENTER
	DB    1,0
	DB    LOW SEL_PRG,HIGH SEL_PRG

	DB    K_LEFT
	DB    LOW POT_13,HIGH POT_13
	DB    LOW SET_POT,HIGH SET_POT

	DB    K_RIGHT
	DB    LOW POT_14,HIGH POT_14
	DB    LOW SET_POT,HIGH SET_POT

	DB    0FFH
	DB    LOW SFT_0,HIGH SFT_0

SFT_11: DB    K_ENTER
	DB    0,0
	DB    LOW M_MORUN,HIGH M_MORUN

SFT_10: DB    K_LEFT
	DB    LOW POT_11,HIGH POT_11
	DB    LOW SET_POT,HIGH SET_POT

	DB    K_RIGHT
	DB    LOW POT_12,HIGH POT_12
	DB    LOW SET_POT,HIGH SET_POT

	DB    0FFH
	DB    LOW SFT_0,HIGH SFT_0

SFT_9:  DB    K_UP
	DB    0,-48
	DB    LOW UP_BDP,HIGH UP_BDP

	DB    K_DOWN
	DB    -49,-1
	DB    LOW DO_BDP,HIGH DO_BDP

	DB    K_LEFT
	DB    LOW POT_8,HIGH POT_8
	DB    LOW SET_POT,HIGH SET_POT

SFT_8:  DB    K_RIGHT
	DB    LOW POT_9,HIGH POT_9
	DB    LOW SET_POT,HIGH SET_POT

	DB    K_ENTER
	DB    0,0
	DB    LOW uL_INIT,HIGH uL_INIT

	DB    0FFH
	DB    LOW SFT_0,HIGH SFT_0

SFT_7:  DB    K_RIGHT
	DB    LOW POT_5,HIGH POT_5
	DB    LOW SET_POT,HIGH SET_POT

SFT_5:  DB    K_RIGHT
	DB    LOW POT_6,HIGH POT_6
	DB    LOW GBF_POT,HIGH GBF_POT

	DB    K_LEFT
	DB    LOW POT_7,HIGH POT_7
	DB    LOW SET_POT,HIGH SET_POT

SFT_6:  DB    K_LEFT
	DB    LOW POT_5,HIGH POT_5
        DB    LOW SET_POT,HIGH SET_POT

	DB    0FFH
	DB    LOW SFT_0,HIGH SFT_0

SFT_4:  DB    K_1
	DB    0,001H
	DB    LOW R_AUXU,HIGH R_AUXU

	DB    K_2
	DB    0,002H
	DB    LOW R_AUXU,HIGH R_AUXU

	DB    K_3
	DB    0,004H
	DB    LOW R_AUXU,HIGH R_AUXU

	DB    K_4
	DB    0,008H
	DB    LOW R_AUXU,HIGH R_AUXU

	DB    0FFH
	DB    LOW SFT_0,HIGH SFT_0

SFT_3:  DB    K_LEFT
	DB    LOW POT_2,HIGH POT_2
	DB    LOW SET_POT,HIGH SET_POT

SFT_2:  DB    K_LEFT
	DB    LOW POT_1,HIGH POT_1
	DB    LOW SET_POT,HIGH SET_POT

	DB    K_RIGHT
	DB    LOW POT_3,HIGH POT_3
	DB    LOW SET_POT,HIGH SET_POT

SFT_1:  DB    K_RIGHT
	DB    LOW POT_2,HIGH POT_2
	DB    LOW SET_POT,HIGH SET_POT

SFT_0:  DB    K_MODE
	DB    0,0
	DB    LOW M_MODE,HIGH M_MODE

	DB    K_PROG
	DB    0,0
	DB    LOW PROG_IN,HIGH PROG_IN

	DB    K_END
	DB    0,0
	DB    LOW M_KEND,HIGH M_KEND

	DB    K_RUN
	DB    0,0
	DB    LOW RUN_PRG,HIGH RUN_PRG

	DB    K_LIST
	DB    LOW DIT_7,HIGH DIT_7
	DB    LOW SET_DIT,HIGH SET_DIT

	DB    K_CYCLE
	DB    LOW DIT_5,HIGH DIT_5
	DB    LOW SET_DIT,HIGH SET_DIT

	DB    K_CONC
	DB    LOW DIT_3,HIGH DIT_3
	DB    LOW SET_DIT,HIGH SET_DIT

	DB    K_AUX
	DB    LOW DIT_2,HIGH DIT_2
	DB    LOW SET_DIT,HIGH SET_DIT

	DB    K_FLOW
	DB    LOW DIT_1,HIGH DIT_1
	DB    LOW SET_DIT,HIGH SET_DIT

	DB    K_PURGE
	DB    0,0
	DB    LOW M_PURGE,HIGH M_PURGE

	DB    K_START
	DB    0,0
	DB    LOW M_START,HIGH M_START

	DB    K_STOP
	DB    0,0
	DB    LOW M_STOP,HIGH M_STOP

SFT_VAL:DB    K_A
	DB    0,0
	DB    LOW SET_GA,HIGH SET_GA

	DB    K_B
	DB    0,0
	DB    LOW SET_GB,HIGH SET_GB

	DB    K_C
	DB    0,0
	DB    LOW SET_GC,HIGH SET_GC

	DB    0

%IF(%WITH_AUXVALV)THEN(
SFT_P8: DB    K_A
	DB    NOT 0,NOT 0
	DB    LOW R_AUXU,HIGH R_AUXU

	DB    K_B
	DB    NOT 0,NOT 1
	DB    LOW R_AUXU,HIGH R_AUXU

	DB    K_C
	DB    NOT 0,NOT 2
	DB    LOW R_AUXU,HIGH R_AUXU

	DB    0FFH
	DB    LOW SFT_P1,HIGH SFT_P1
)FI

SFT_P4: DB    K_1
	DB    0,001H
	DB    LOW R_AUXU,HIGH R_AUXU

	DB    K_2
	DB    0,002H
	DB    LOW R_AUXU,HIGH R_AUXU

	DB    K_3
	DB    0,004H
	DB    LOW R_AUXU,HIGH R_AUXU

	DB    K_4
	DB    0,008H
	DB    LOW R_AUXU,HIGH R_AUXU

	DB    0FFH
	DB    LOW SFT_P1,HIGH SFT_P1

SFT_P3:	DB    K_IMPL
	DB    080H,0
	DB    LOW SET_IMP,HIGH SET_IMP

SFT_P2: DB    K_IMPL
	DB    040H,0
	DB    LOW SET_IMP,HIGH SET_IMP

	DB    K_RIGHT
	DB    1,0
	DB    LOW MOD_POG,HIGH MOD_POG

	DB    K_ENTER
	DB    1,0
	DB    LOW ENT_POG,HIGH ENT_POG

SFT_P1: DB    K_DOWN
	DB    0,0
	DB    LOW SET_NEX,HIGH SET_NEX

	DB    K_UP
	DB    0,0
	DB    LOW SET_PRE,HIGH SET_PRE

	DB    K_LEFT
	DB    -1,0
	DB    LOW MOD_LNP,HIGH MOD_LNP

	DB    K_RIGHT
	DB    1,0
	DB    LOW MOD_LNP,HIGH MOD_LNP

	DB    K_ENTER
	DB    1,0
	DB    LOW ENT_PRG,HIGH ENT_PRG

	DB    K_DEL
	DB    0,0
	DB    LOW DEL_LN,HIGH DEL_LN

	DB    K_INS
	DB    LOW MENU_P1,HIGH MENU_P1
	DB    LOW SET_MENU,HIGH SET_MENU

SFT_P0:	DB    K_FLOW
	DB    0,1
	DB    LOW INS_LN,HIGH INS_LN

	DB    K_CONC
	DB    0,2
	DB    LOW INS_LN,HIGH INS_LN

	DB    K_AUX
	DB    0,3
	DB    LOW INS_LN,HIGH INS_LN

	DB    K_END
	DB    0,4
	DB    LOW INS_LN,HIGH INS_LN

	DB    K_HOLD
	DB    0,5
	DB    LOW INS_LN,HIGH INS_LN

	DB    K_PROG
	DB    1,0
	DB    LOW PROG_OU,HIGH PROG_OU

	DB    K_STOP
	DB    0,0
	DB    LOW M_STOP,HIGH M_STOP

	DB    0

SFT_R1: DB    K_IMPL
	DB    0,0
	DB    LOW M_PSPD,HIGH M_PSPD

	DB    K_CONC
	DB    LOW DIT_R3,HIGH DIT_R3
	DB    LOW SET_DIT,HIGH SET_DIT

	DB    K_AUX
	DB    LOW DIT_R2,HIGH DIT_R2
	DB    LOW SET_DIT,HIGH SET_DIT

	DB    K_FLOW
	DB    LOW DIT_R1,HIGH DIT_R1
	DB    LOW SET_DIT,HIGH SET_DIT

	DB    K_CYCLE
	DB    LOW DIT_R4,HIGH DIT_R4
	DB    LOW SET_DIT,HIGH SET_DIT

	DB    K_END
	DB    0,0
	DB    LOW RU_BRK,HIGH RU_BRK

	DB    K_STOP
	DB    0,0
	DB    LOW RU_STOP,HIGH RU_STOP

	DB    K_HOLD
	DB    0,0
	DB    LOW RU_HOLD,HIGH RU_HOLD

	DB    0

%IF(%WITH_UL_G_BF) THEN (
SFT_GBF1:
	DB    K_STOP
	DB    0,0
	DB    LOW GBF_STOP,HIGH GBF_STOP

	DB    0FFh
	DB    LOW SFT_VAL,HIGH SFT_VAL
)FI

T_VALID:DB    'Data platna       ',0
T_NOTV: DB    'Ignoruj data      ',0

T_MANP: DB    LCD_CLR,'FLOW PH PL P MPa',0

%IF(%WITH_UL_G_BF) THEN (
T_GBF1: DB    LCD_CLR,'FLOW SLAVE P MPa'
	DB    C_LIN2, '     PUMP',0

T_GBF2: DB    LCD_CLR,'GRADIENT  SLAVE',0

T_GBFBV:DB    'By Valves',0

T_GBFBF:DB    'By Flow  ',0
)FI

T_MANA: DB    LCD_CLR,'AUX    -S-  -R- '
	DB    C_LIN2, 'OUTPUT',0

T_MANC: DB    LCD_CLR,'FLOW  ',025H,'A  ',025H,'B  ',025H,'C',0

T_MSCOM:DB    LCD_CLR,'Adr kBd Form Set',0

T_ALRM: DB    LCD_CLR,'TIME ALARM'
	DB    C_LIN2, '           NULL',0

T_PRCOR:DB    LCD_CLR,'Press Corr.',0

T_PRSL: DB    LCD_CLR,'NUM PROGRAM',0

T_PRNUL:DB    LCD_CLR,' End of program',0

T_PRUNK:DB    LCD_CLR,' Unknown line',0

T_PR_1: DB    LCD_CLR,'TIME  FLOW',0

T_PR_2: DB    LCD_CLR,'TIME   ',025H,'B   ',025H,'C',0

T_PR_3: DB    LCD_CLR,'TIME   -S-  AUX'
	DB    C_LIN2, '            OUTP',0

T_PR_4:	DB    LCD_CLR,'TIME'
	DB    C_LIN2, '      ** End **',0

T_PR_5:	DB    LCD_CLR,'TIME'
	DB    C_LIN2, '     Wait for SW',0

T_PR_6:	DB    LCD_CLR,'TIME Mark to ADR',0

T_PR_7:	DB    LCD_CLR,'TIME   Aux Valve',0

T_PR_8:	DB    LCD_CLR,'TIME  Grad Valve',0

T_PR_9:	DB    LCD_CLR,'TIME  Press Cor.',0

%IF(%WITH_AUXVALV AND %WITH_UL_G_BF)THEN(
T_PR_10:DB    LCD_CLR,'TIME   Slave Aux',0

T_PR_11:DB    LCD_CLR,'TIME  Slave Grad',0
)FI

T_RUNF: DB    LCD_CLR,'TIME FLOW P MPa',0

T_RUNA: DB    LCD_CLR,'TIME   -S-  -R-',0

T_RUNC: DB    LCD_CLR,'TIME  ',025H,'A  ',025H,'B  ',025H,'C',0

T_RUNR: DB    LCD_CLR,'CYCLE ACTU FINAL',0

T_PR_CO:DB    LCD_CLR,'  Compiling'
	DB    C_LIN2, '   program',0

T_RU_BR:DB    LCD_CLR,'Break program ?',0

T_RU_HO:DB    LCD_CLR,'Hold settings ?',0

%IF (%WITH_UL_G_BF) THEN (
ERR_GBFT:DB   LCD_HOME,'Slave pump error',0
	DB    LCD_CLR,'Check cabling'
	DB    C_LIN2, 'and tubing',0,0

T_GBF_BR:DB   LCD_CLR,'Break M/S ctrl ?',0
)FI

ERR_RERT:DB   LCD_HOME,'Motor overloaded',0
	DB    LCD_CLR,'Check tubing'
	DB    C_LIN2, 'or need repair',0,0

ERR_ENOT:DB   LCD_HOME,'Motor error',0
	DB    LCD_CLR,'Try again'
	DB    C_LIN2, 'or need repair',0,0

ALRM_BT:DB    LCD_HOME,'** User timer **',0,0

T_PREND:DB    LCD_HOME,'End of program. ',0,0

ERR_GRT:DB    LCD_HOME,'Impl. gradient !',0
	DB    LCD_HOME,'In impl. grad.  '
	DB    C_LIN2,  'ln sum over 100',025H,0,0

ERR_PHT:DB    LCD_HOME,'Press too high !',0
	DB    LCD_HOME,'Check PH or Flow',0,0

ERR_PLT:DB    LCD_HOME,'Press low !     ',0
	DB    LCD_HOME,'Check PL or tube',0,0

ERR_SGR:DB    LCD_HOME,'Sum over 100',025H,' ! ',0
	DB    LCD_HOME,'B+C must be less'
	DB    C_LIN2,  'or equal 100',025H,'   ',0,0

ERR_SRD:DB    LCD_HOME,'SRAM data may be'
	DB    C_LIN2,  'wrong. Press key',0,0

T_ST_01:DB    LCD_CLR,'Compile data now',0

T_RU_MS:DB    LCD_CLR,'Start motor now?',0

T_RU_RC:DB    LCD_CLR,'Prepare for run?',0

LPVER_T:DB    LCD_CLR,'%VERSION'
	DB    C_LIN2 ,' (c) PiKRON 2002',0

ERR_EPR:DB    LCD_CLR,'EPROM error !',0

$NOLIST

; *******************************************************************
; Texty jednotlivych menu

; +0 .. Ukazatel na text menu - format HELP
; +2 .. Ukazatel na text helpu
; +4 .. Ukazatel na SFT
; +6 .. Seznam operaci pri funkcich (4byte)

MENU_P1:DB    LOW MENT_P1,HIGH MENT_P1
	DB    LOW M_MOMH1,HIGH M_MOMH1
	DB    LOW SFT_P0,HIGH SFT_P0
	DB    0,0
	DB    LOW SET_LN,HIGH SET_LN
	DB    0,1
	DB    LOW INS_LN,HIGH INS_LN
	DB    0,2
	DB    LOW INS_LN,HIGH INS_LN
	DB    0,3
	DB    LOW INS_LN,HIGH INS_LN
	DB    0,4
	DB    LOW INS_LN,HIGH INS_LN
	DB    0,5
	DB    LOW INS_LN,HIGH INS_LN
	DB    0,6
	DB    LOW INS_LN,HIGH INS_LN
	DB    0,7
	DB    LOW INS_LN,HIGH INS_LN
	DB    0,8
	DB    LOW INS_LN,HIGH INS_LN
	DB    0,9
	DB    LOW INS_LN,HIGH INS_LN
	DB    0,10
	DB    LOW INS_LN,HIGH INS_LN
	DB    0,11
	DB    LOW INS_LN,HIGH INS_LN
	DB    1,0
	DB    LOW PROG_OU,HIGH PROG_OU

	DB    0FEH,0FEH,0FEH
MENT_P1:DB    0,0FFH,1
	DB    0FFH,0FFH,0FFH
	DB    'Exit',0
	DB    1,0FFH,1
	DB    0FFH,0FFH,0FFH
	DB    'Flow',0
	DB    2,0FFh,1,0FFH,0FFH,0FFH
	DB    'Concentration',0
	DB    3,0FFh,1,0FFH,0FFH,0FFH
	DB    'Auxual outputs',0
	DB    4,0FFh,1,0FFH,0FFH,0FFH
	DB    'End of cycle',0
	DB    5,0FFh,1,0FFH,0FFH,0FFH
	DB    'Wait for switch',0
	DB    6,0FFh,1,0FFH,0FFH,0FFH
	DB    'Send mark',0
    %IF(%WITH_AUXVALV)THEN(
	DB    7,0FFh,1,0FFH,0FFH,0FFH
	DB    'Aux valves',0
	DB    8,0FFh,1,0FFH,0FFH,0FFH
	DB    'Grad valves',0
    )FI
    %IF(%PRESS_COR)THEN(
	DB    9,0FFh,1,0FFH,0FFH,0FFH
	DB    'Press correction',0
    )FI
    %IF(%WITH_AUXVALV AND %WITH_UL_G_BF)THEN(
	DB    10,0FFh,1,0FFH,0FFH,0FFH
	DB    'Slave aux valves',0
	DB    11,0FFh,1,0FFH,0FFH,0FFH
	DB    'Slave grad valv.',0
    )FI
	DB    12,0FFh,1,0FFH,0FFH,0FFH
	DB    'Exit program mode',0
	DB    0FEH,0FEH,0FEH

; *******************************************************************
; Napovedne texty

%IF(%WITH_HELP)THEN(
P2011_H:DB    LOW FLOW_HL,HIGH FLOW_HL,1,0FFH,0FFH,0FFH
	DB    'Flow range',0
	DB    LOW MANU_HL,HIGH MANU_HL,1,0FFH,0FFH,0FFH
	DB    'Manual mode',0
	DB    0FFH,0FFH,0FFH
	DB    'Section 2.011',0
	DB    0FFH,0FFH,0FFH
	DB    'of manual.',0
	DB    0FEH,0FEH,0FEH

P2012_H:DB    LOW PRLI_HL,HIGH PRLI_HL,1,0FFH,0FFH,0FFH
	DB    'Press limits',0
	DB    LOW MANU_HL,HIGH MANU_HL,1,0FFH,0FFH,0FFH
	DB    'Manual mode',0
	DB    0FEH,0FEH,0FEH

P2040_H:DB    LOW MAUX_HL,HIGH MAUX_HL,1,0FFH,0FFH,0FFH
	DB    'Aux output/input',0
	DB    LOW MANU_HL,HIGH MANU_HL,1,0FFH,0FFH,0FFH
	DB    'Manual mode',0
	DB    0FEH,0FEH,0FEH

MCON_HL:DB    LOW MCO1_HL,HIGH MCO1_HL,1,0FFH,0FFH,0FFH
	DB    'Isocr. gradient',0
	DB    LOW MANU_HL,HIGH MANU_HL,1,0FFH,0FFH,0FFH
	DB    'Manual mode',0
	DB    0FEH,0FEH,0FEH

MCO1_HL:DB    0FFH,0FFH,0FFH
	DB    'Isocr. gradient :',0
	DB    0FFH,0FFH,0FFH
	DB    'You can set',0
	DB    0FFH,0FFH,0FFH
	DB    'concentration',0
	DB    0FFH,0FFH,0FFH
	DB    'of component C,B',0
	DB    0FEH,0FEH,0FEH

PRLI_HL:DB    0FFH,0FFH,0FFH
	DB    ' PRESS LIMITS',0
	DB    0FFH,0FFH,0FFH
	DB    'You can set high',0
	DB    LOW PRLH_HL,HIGH PRLH_HL,13,0FFH,0FFH,0FFH
	DB    'press limit PH',0
	DB    LOW PRLL_HL,HIGH PRLL_HL,15,0FFH,0FFH,0FFH
	DB    'and low limit PL',0
	DB    0FEH,0FEH,0FEH

PRLH_HL:DB    LOW PRLR_HL,HIGH PRLR_HL,1,0FFH,0FFH,0FFH
	DB    'Press range',0
	DB    0FFH,0FFH,0FFH
	DB    'When press grow',0
	DB    0FFH,0FFH,0FFH
	DB    'up over PH motor',0
	DB    0FFH,0FFH,0FFH
	DB    'is stopped.',0
	DB    0FEH,0FEH,0FEH

PRLL_HL:DB    LOW PRLR_HL,HIGH PRLR_HL,1,0FFH,0FFH,0FFH
	DB    'Press range',0
	DB    0FFH,0FFH,0FFH
	DB    'When press is',0
	DB    0FFH,0FFH,0FFH
	DB    'falling down PL',0
	DB    0FFH,0FFH,0FFH
	DB    'warning apears',0
	DB    0FFH,0FFH,0FFH
	DB    'and when it is',0
	DB    0FFH,0FFH,0FFH
	DB    'set motor stop',0
	DB    0FEH,0FEH,0FEH
)FI
%IF(%WITH_HELP)THEN(
PRLR_HL:DB    0FFH,0FFH,0FFH
        DB    'Possible value',0
        DB    0FFH,0FFH,0FFH
	DB    'is flom 0 to %C_MAX_PRESS',0
        DB    0FFH,0FFH,0FFH
	DB    ' MPa.',0
	DB    0FEH,0FEH,0FEH

FLOW_HL:DB    0FFH,0FFH,0FFH
        DB    'You can set flow',0
        DB    0FFH,0FFH,0FFH
	DB    'from 0.01 to %C_MAX_FLOW',0
        DB    0FFH,0FFH,0FFH
	DB    'ml/min.',0
	DB    0FEH,0FEH,0FEH

MANU_HL:DB    0FFH,0FFH,0FFH
	DB    'Folowing keys',0
        DB    0FFH,0FFH,0FFH
	DB    'are avayable in',0
        DB    0FFH,0FFH,0FFH
        DB    'manual mode :',0
	DB    LOW MOTS_HL,HIGH MOTS_HL,1,0FFH,0FFH,0FFH
        DB    'MOTOR_START',0
        DB    LOW PURG_HL,HIGH PURG_HL,1,0FFH,0FFH,0FFH
	DB    'PURGE',0
	DB    LOW MOTT_HL,HIGH MOTT_HL,1,0FFH,0FFH,0FFH
        DB    'MOTOR_STOP',0
	DB    LOW MFLO_HL,HIGH MFLO_HL,1,0FFH,0FFH,0FFH
	DB    'FLOW_PRESS',0
	DB    LOW CONC_HL,HIGH CONC_HL,1,0FFH,0FFH,0FFH
	DB    'CONC',0
	DB    LOW MAUX_HL,HIGH MAUX_HL,1,0FFH,0FFH,0FFH
	DB    'AUX_OUTPUT',0
	DB    LOW MABC_HL,HIGH MABC_HL,1,0FFH,0FFH,0FFH
	DB    'A B C   RUN',0
	DB    0FFH,0FFH,0FFH
	DB    'PROG    END',0
	DB    0FFH,0FFH,0FFH
	DB    'MODE    HELP',0
	DB    0FEH,0FEH,0FEH

MOTS_HL:DB    0FFH,0FFH,0FFH
	DB    ' MOTOR_START :',0
	DB    0FFH,0FFH,0FFH   ;
	DB    'Start delivery',0
	DB    LOW FLOW_HL,HIGH FLOW_HL,10,0FFH,0FFH,0FFH
	DB    'with set FLOW',0
	DB    LOW PRLI_HL,HIGH PRLI_HL,7,0FFH,0FFH,0FFH
	DB    'watch PRESS lim.',0
	DB    0FEH,0FEH,0FEH

PURG_HL:DB    0FFH,0FFH,0FFH
	DB    ' PURGE :',0
	DB    0FFH,0FFH,0FFH
	DB    'Start/stop',0
	DB    0FFH,0FFH,0FFH
	DB    'cleaning with',0
	DB    0FFH,0FFH,0FFH
	DB    'max flow.',0
	DB    0FFH,0FFH,0FFH
	DB    '( 4 ml/min )',0
	DB    0FFH,0FFH,0FFH
	DB    'Watching only PH',0
	DB    0FEH,0FEH,0FEH
)FI
%IF(%WITH_HELP)THEN(
MOTT_HL:DB    0FFH,0FFH,0FFH
	DB    ' MOTOR_STOP :',0
	DB    0FFH,0FFH,0FFH
	DB    'Stop motor moves',0
	DB    0FEH,0FEH,0FEH

MFLO_HL:DB    0FFH,0FFH,0FFH
	DB    ' FLOW_PRESS :',0
	DB    0FFH,0FFH,0FFH
	DB    'This key active',0
	DB    0FFH,0FFH,0FFH
	DB    'mode in which',0
	DB    0FFH,0FFH,0FFH
	DB    'you can control',0
	DB    LOW FLOW_HL,HIGH FLOW_HL,1
	DB    LOW PRLI_HL,HIGH PRLI_HL,7,0FFH,0FFH,0FFH
	DB    'FLOW, PRESS_LIM.',0
	DB    0FFH,0FFH,0FFH
	DB    'and read actual',0
	DB    0FFH,0FFH,0FFH
	DB    'press.',0
	DB    0FEH,0FEH,0FEH

CONC_HL:DB    0FFH,0FFH,0FFH
	DB    ' CONC :',0
	DB    0FFH,0FFH,0FFH
	DB    'Set mode of',0
	DB    0FFH,0FFH,0FFH
	DB    'setting of',0
	DB    LOW MCO1_HL,HIGH MCO1_HL,8,0FFH,0FFH,0FFH
	DB    'isokr. GRADIENT.',0
	DB    0FEH,0FEH,0FEH

MAUX_HL:DB    0FFH,0FFH,0FFH
	DB    ' AUX_OUTPUT :',0
	DB    0FFH,0FFH,0FFH
	DB    'By keys 1 2 3 4',0
	DB    0FFH,0FFH,0FFH
	DB    'control outputs',0
	DB    0FFH,0FFH,0FFH
	DB    'and read inputs.',0
	DB    0FEH,0FEH,0FEH

MABC_HL:DB    0FFH,0FFH,0FFH
	DB    ' A B C :',0
	DB    0FFH,0FFH,0FFH
	DB    'Select solvent',0
	DB    0FEH,0FEH,0FEH

HELP_HL:DB    LOW ABOU_HL,HIGH ABOU_HL,1,0FFH,0FFH,0FFH
	DB    'About',0
	DB    LOW HELP_H1,HIGH HELP_H1,1,0FFH,0FFH,0FFH
	DB    'Use cursor+ENTER',0
HELP_H1:DB    LOW HELP_H2,HIGH HELP_H2,1,0FFH,0FFH,0FFH
	DB    'Choose topic',0
	DB    LOW HELP_H2,HIGH HELP_H2,1,0FFH,0FFH,0FFH
	DB    'and press ENTER',0
HELP_H2:DB    0FFH,0FFH,0FFH
	DB    'When you want',0
	DB    0FFH,0FFH,0FFH
	DB    'leave help press',0
	DB    0FFH,0FFH,0FFH
	DB    'any other key',0
	DB    0FFH,0FFH,0FFH
	DB    'This help you',0
	DB    0FFH,0FFH,0FFH
	DB    'every pop-up by',0
	DB    0FFH,0FFH,0FFH
	DB    'twicestroke HELP',0
	DB    0FFH,0FFH,0FFH
	DB    'New help apears',0
	DB    0FFH,0FFH,0FFH
	DB    'after every',0
	DB    0FFH,0FFH,0FFH
	DB    'change of cursor',0
	DB    0FFH,0FFH,0FFH
	DB    'position.',0
	DB    0FEH,0FEH,0FEH
ABOU_HL:DB    LOW ABOU_H1,HIGH ABOU_H1,1,0FFH,0FFH,0FFH
	DB    '%VERSION',0
	DB    0FFH,0FFH,0FFH
	DB    ' PiKRON s.r.o',0
ABOU_H1:DB    0FFH,0FFH,0FFH
	DB    'www.pikron.com',0
	DB    0FFH,0FFH,0FFH
	DB    'general design',0
	DB    0FFH,0FFH,0FFH
	DB    '    L.Pisa',0
	DB    0FFH,0FFH,0FFH
	DB    'hardware',0
	DB    0FFH,0FFH,0FFH
	DB    'P.Pisa P.Porazil',0
	DB    0FFH,0FFH,0FFH
	DB    'software',0
	DB    0FFH,0FFH,0FFH
	DB    0F7H,'soft P.Pisa',0
	DB    0FFH,0FFH,0FFH
	DB    'Praha   (c) 1996',0
	DB    0FFH,0FFH,0FFH
	DB    '14.11.2006',0
	DB    0FFH,0FFH,0FFH
	DB    'Configured',0
	DB    0FFH,0FFH,0FFH
	DB    'hw:  %HW_VERSION',0
	DB    0FFH,0FFH,0FFH
	DB    'reg: %REG_STRUCT_TYPE',0
	DB    0FFH,0FFH,0FFH
	DB    'out: %REG_OUT_TYPE',0
	DB    0FFH,0FFH,0FFH
	DB    'proc: %PROCESSOR_TYPE',0
	DB    0FFH,0FFH,0FFH
	DB    'vector: %VECTOR_TYPE',0
	DB    0FFH,0FFH,0FFH
	DB    'press_cor: %PRESS_COR',0
	DB    0FEH,0FEH,0FEH
)FI


%IF(NOT %WITH_HELP)THEN(
	DB    0FEH,0FEH,0FEH
P2011_H:
P2012_H:
P2040_H:
MCON_HL:
MCO1_HL:
PRLI_HL:
PRLH_HL:
PRLL_HL:
PRLR_HL:
FLOW_HL:
MANU_HL:
MOTS_HL:
PURG_HL:
MOTT_HL:
MFLO_HL:
CONC_HL:
MAUX_HL:
MABC_HL:
HELP_HL:
HELP_H1:
HELP_H2:
ABOU_HL:
ABOU_H1:
	DB    0FFH,0FFH,0FFH
	DB    'Help not compiled',0
	DB    0FEH,0FEH,0FEH

)FI

END
