#   Project file pro cerpadlo LCP4000
#         (C) Pisoft 1992

par=debug

lp_hw.obj : lp_hw.asm
	a51 lp_hw.asm $(par)

lp_gc.obj : lp_gc.asm
	a51 lp_gc.asm $(par)

lp.obj    : lp.asm
	a51 lp.asm  $(par)

lp_uf.obj : lp_uf.asm
	a51 lp_uf.asm $(par)

lp_lan.obj: lp_lan.asm
	a51 lp_lan.asm $(par)

lp.       : lp.obj lp_uf.obj lp_hw.obj lp_gc.obj lp_lan.obj ..\pblib\pb.lib
	l51 lp.obj,lp_uf.obj,lp_hw.obj,lp_gc.obj,lp_lan.obj,..\pblib\pb.lib code(9400H) xdata(8000H) ramsize(100h) ixref

lp.hex    : lp.
	ohs51 lp

	  : lp.hex
#	sendhex lp.hex /p3 /m5 /t2 /g37888
	unixcmd -d ul_sendhex -m 4 -g 0
	pause
	unixcmd -d ul_sendhex lp.hex -m 4 -g 0x9400
	unixcmd -d ul_sendhex -m 5 -g 0
	pause
	unixcmd -d ul_sendhex lp.hex -m 5 -g 0x9400
