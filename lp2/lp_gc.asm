;********************************************************************
;*                    LCP 4000 - LP_GC.ASM                          *
;*     Tabulka pro korekci gradientu a konstanty vacky              *
;*                  Stav ke dni 18.06.1994                          *
;*                      (C) Pisoft 1991 Pavel Pisa Praha            *
;********************************************************************

$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$LIST

PUBLIC  GR_TAB,GR_A41,GR_A40,GR_P4,GR_P5,GR_P45,GR_MINI,GR_MAXI
LP_GC_C SEGMENT CODE

%IF(0) THEN(
GR_A41	EQU   849   ; smernice primkoveho useku v 1/256 procenta
		    ; nasobena 2^16
GR_A40  EQU   398   ; prusecik primkoveho useku
GR_MINI EQU   0
GR_MAXI EQU   0

GR_P4   EQU   24747 ; procenta primkoveho useku *256
GR_P5   EQU   0     ; procenta zpomalovaciho useku *256
GR_P45	EQU   GR_P4+GR_P5

RSEG LP_GC_C

GR_TAB:	%W(0)
	%W(59)
	%W(60)
	%W(61)
	%W(62)
	%W(63)
	%W(63)
	%W(64)
	%W(64)
	%W(65)
	%W(65)
	%W(66)
	%W(66)
	%W(67)
	%W(67)
	%W(68)
	%W(68)
	%W(68)
	%W(69)
	%W(69)
	%W(69)
	%W(70)
	%W(70)
	%W(70)
	%W(71)
	%W(71)
	%W(71)
	%W(72)
	%W(72)
	%W(72)
	%W(72)
	%W(73)
	%W(73)
	%W(73)
	%W(73)
	%W(74)
	%W(74)
	%W(74)
	%W(74)
	%W(75)
	%W(75)
	%W(75)
	%W(75)
	%W(76)
	%W(76)
	%W(76)
	%W(76)
	%W(77)
	%W(77)
	%W(77)
	%W(77)
	%W(77)
	%W(78)
	%W(78)
)ELSE(

GR_A41	EQU   21015 ; smernice primkoveho useku na 1/256 procenta
		    ; nasobena 2^16
GR_A40	EQU   9907  ; prusecik primkoveho useku
GR_MINI EQU   1416
GR_MAXI EQU   10191

GR_P4	EQU   23834 ; procenta primkoveho useku *256
GR_P5	EQU   883   ; procenta zpomalovaciho useku *256
GR_P45	EQU   GR_P4+GR_P5

RSEG LP_GC_C

GR_TAB:	%W(0)
	%W(76)
	%W(108)
	%W(132)
	%W(152)
	%W(170)
	%W(187)
	%W(202)
	%W(215)
	%W(229)
	%W(241)
	%W(253)
	%W(264)
	%W(275)
	%W(285)
	%W(295)
	%W(305)
	%W(314)
	%W(323)
	%W(332)
	%W(341)
	%W(349)
	%W(357)
	%W(365)
	%W(373)
	%W(381)
	%W(389)
	%W(396)
	%W(403)
	%W(410)
	%W(417)
	%W(424)
	%W(431)
	%W(438)
	%W(444)
	%W(451)
	%W(457)
	%W(464)
	%W(470)
	%W(476)
	%W(482)
	%W(488)
	%W(494)
	%W(500)
	%W(505)
	%W(511)
	%W(517)
	%W(522)
	%W(528)
	%W(533)
	%W(539)
	%W(544)
	%W(550)
	%W(555)
	%W(560)
	%W(565)
	%W(570)
	%W(575)
)FI

%IF(%PRESS_COR)THEN(

PUBLIC	COR_TAB, COR_TSH, COR_TIRC

COR_TSH	EQU   3	    ; posun polohy/indexu doleva
		    ; musi byt >=2

RSEG LP_GC_C

COR_TAB:%W(186)
	%W(561)
	%W(940)
	%W(1323)
	%W(1711)
	%W(2103)
	%W(2500)
	%W(2902)
	%W(3308)
	%W(3719)
	%W(4135)
	%W(4556)
	%W(4983)
	%W(5414)
	%W(5851)
	%W(6293)
	%W(6741)
	%W(7194)
	%W(7653)
	%W(8118)
	%W(8589)
	%W(9066)
	%W(9549)
	%W(10039)
	%W(10534)
	%W(11037)
	%W(11546)
	%W(12062)
	%W(12585)
	%W(13114)
	%W(13652)
	%W(14196)
	%W(14748)
	%W(15308)
	%W(15876)
	%W(16451)
	%W(17035)
	%W(17627)
	%W(18228)
	%W(18838)
	%W(19456)
	%W(20084)
	%W(20721)
	%W(21367)
	%W(22023)
	%W(22690)
	%W(23366)
	%W(24053)
	%W(24750)
	%W(25459)
	%W(26179)
	%W(26910)
	%W(27653)
	%W(28408)
	%W(29175)
	%W(29955)
	%W(30748)
	%W(31555)
	%W(32374)
	%W(33208)
	%W(34056)
	%W(34919)
	%W(35797)
	%W(36691)
	%W(37600)
	%W(38525)
	%W(39468)
	%W(40427)
	%W(41405)
	%W(42400)
	%W(43414)
	%W(44448)
	%W(45501)
	%W(46575)
	%W(47669)
	%W(48785)
	%W(49924)
	%W(51085)
	%W(52270)
	%W(53479)
	%W(54713)
	%W(55974)
	%W(57260)
	%W(58575)
	%W(59918)
	%W(61290)
	%W(62692)
	%W(64126)
COR_TAB_E:

COR_TIRC EQU (COR_TAB_E-COR_TAB)*(1 SHL (COR_TSH-1))

)FI


	END