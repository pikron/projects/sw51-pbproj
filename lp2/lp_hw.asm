$NOMOD51
;********************************************************************
;*                    LCP 4000 - LP_HW.ASM                          *
;*     Rutiny LCP 4000 pro ovladani hardware                        *
;*                  Stav ke dni 31.01.2003                          *
;*                      (C) Pisoft 1991 Pavel Pisa Praha            *
;********************************************************************

;$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_AI)
$INCLUDE(%INCH_AL)
$INCLUDE(%INCH_ADR)
$INCLUDE(%INCH_REGS)
$INCLUDE(%INCH_MMAC)
;$LIST

	   EXTRN   CODE(START,I_TIME)
	   EXTRN   BIT(U_TIME,U_PRESS,U_AUX,U_FLOW,U_CONC,GBF_CHRQ)
	   EXTRN   XDATA(FLOW,GRAD_B,GRAD_C)

	   EXTRN   CODE(GR_TAB)
	   EXTRN   NUMBER(GR_A41,GR_A40,GR_P4,GR_P5,GR_P45)
	   EXTRN   NUMBER(GR_MINI,GR_MAXI)

	   EXTRN   CODE(uL_STR)
	   
	%IF(%WITH_UL_G_BF) THEN (
	   EXTRN   XDATA(GBF_FLOWB)
	)FI

	   PUBLIC  RES_STAR,ITIM_RF,TIME,ITIM_R,WR_GRAD
	   PUBLIC  S_FLOW,S_FLOW0,S_FLOW1,G_PRESS,S_CFGFL
	   PUBLIC  M_STOP,M_START,M_PURGE
	   PUBLIC  SET_GA,SET_GB,SET_GC,S_GRAD,S_GRAD0,S_GRDIR
	   PUBLIC  PRESS,PRESS_L,PRESS_H,PRESS_B,PRESS_O,PRESS_E
	   PUBLIC  PRESS_Q
	   PUBLIC  PRESS_V,PRESS_S,PR_ENDD,PR_ENDC,PR_GRER,FL_DIPR

	   PUBLIC  M_COR,M_POS,M_T,M_PER,M_COR_F,HW_FLG,M_ENERG ; !!!!!!!!!!!!!!
	   PUBLIC  KBDTIMR,TIMR3,BEEPTIM,TIMR_WAIT
	   PUBLIC  TIMR_GBF,TIMR_GBFSL

	   PUBLIC  AUXUAL,S_AUX,S_AUXVALV,G_AUXVALV,FL_GRLT,FL_PURL
	   PUBLIC  HW_CH_F,ALRM_BB,SWEDG_F

	   PUBLIC  FL_BRKUI

	   PUBLIC  STACK_BOTTOM

LP_SRAM SEGMENT XDATA
LP_HW_C SEGMENT CODE
LP_HW_X SEGMENT XDATA
LP_HW_D SEGMENT DATA
LP_HW_B SEGMENT DATA BITADDRESSABLE
STACK   SEGMENT IDATA

RSEG	STACK
STACK_S EQU   40H
STACK_BOTTOM:
	DS    STACK_S

USING   0

%SVECTOR(RESET,RES_STAR)      ; Pocatek programu
%IF (%EQS(%HW_VERSION,LP)) THEN (
%SVECTOR(TIMER0,MOTINC)       ; Buzeni motoru
%SVECTOR(EXTI0,MOTIMP)	      ; Ohlasy motoru
)FI
%SVECTOR(EXTI1,I_TIME1)	      ; Realny cas

RSEG    LP_HW_B
HW_FLG: DS    1
M_POSFL BIT   HW_FLG.7
ITIM_RF BIT   HW_FLG.6
M_CORFL BIT   HW_FLG.5
M_EN_ER BIT   HW_FLG.4
PRESS_O BIT   HW_FLG.3		; pretlak
PRESS_B BIT   HW_FLG.2		; priznak podtlaku
PRESS_E BIT   HW_FLG.1		; press high error
PRESS_V BIT   HW_FLG.0          ; press lo error

HW_FLG1:DS    1
PRESS_S BIT   HW_FLG1.7		; zastavit na press lo
SWEDG_F BIT   HW_FLG1.6		; ukoncit HOLD pouze pri hrane na vstupu
PR_ENDC BIT   HW_FLG1.5
PR_ENDD BIT   HW_FLG1.4
HW_CH_F BIT   HW_FLG1.3
PR_GRER BIT   HW_FLG1.2
ALRM_BB BIT   HW_FLG1.1
FL_DIPR BIT   HW_FLG1.0		; Je pripojen display

HW_FLG2:DS    1
FL_AUXV BIT   HW_FLG2.7		; 8 ventilu na AUX
FL_GRLT	BIT   HW_FLG2.6		; Pomale gradientove ventily
PRESS_Q	BIT   HW_FLG2.5		; Okamzite mereni tlaku pri cerpani
FL_PURL	BIT   HW_FLG2.4		; Neprekrocit 1 MPa pri purge
FL_P25	BIT   HW_FLG2.3		; Rozsah tlaku 25 MPa, jinak 50 MPa
FL_FLM2	BIT   HW_FLG2.2		; Nasobeni prutoku 2
FL_CLCK	BIT   HW_FLG2.1		; Blokovani rekonfigurace
FL_BRKUI BIT  HW_FLG2.0		; Break all loops of UI

RSEG    LP_HW_D
M_ENERG:DS    1
M_PER:  DS    3

M_BYTE3:DS    1

M_POS:  DS    2

RSEG    LP_SRAM

RAM_T_B:DS    1
CHEK_SM:DS    1
BEG_SRAM:

END_SRAM:

RSEG    LP_HW_X

GRAD_BUF:
	DS    1    ; Buffer gradientu
AUX_BUF:DS    1    ; Buffer ADC a vystupu
		   ; Cteni a rizeni AUX
AUXUAL: DS    1    ; X  X  I2 I1 O4 O3 O2 O1

AUXV_TIM:DS   1	   ; Timr delky sepnuti pritahu pro AUXVALV

BEEPTIM:DS    1	   ; Timer delky pipani

N_OF_T  EQU   5    ; Pocet timeru
TIMR1:  DS    1    ; Timery jsou decrementovany
TIMR2:  DS    1    ; s frekvenci 25 Hz
TIMR_GBF:
TIMR_GBFSL:
TIMR_WAIT:
TIMR3:  DS    1
KBDTIMR:DS    1
TIMRI:  DS    1
TIME:   DS    2    ; Cas v 0.01 min              =====

M_COR:  DS    2    ; Pocet pulzu korekce
M_COR_F:DS    2    ; Okamzity korekcni faktor
G_A_E:  DS    2    ; Koncovy pulz gradientu A =398-(pB+pC)*3.31
G_B_E:  DS    2    ; Koncovy pulz gradientu B =398-pC*3.31

PRESS:  DS    2    ; Tlak 0.05 MPa               =====
PRESS_H:DS    2    ; Horni tlakova mez
PRESS_L:DS    2    ; Dolni tlakova mez
PRES_OL:DS    2    ; Stary tlak pro vyhodnoceni PL

M_T:    DS    2    ; Vypocitana perioda


RSEG    LP_HW_C


; Inicializace hardware
; =====================

RES_STAR:
%IF (%EQS(%HW_VERSION,LP)) THEN (
	MOV   IE,#00000100B   ; zakaz preruseni
	MOV   TMOD,#00100001B ; citac 1 mod 2; citac 0 mod 1
	MOV   TCON,#01010101B ; citac 0 a 1 cita ; interapy hranou
	MOV   SCON,#11011000B ; dva stopbity
	MOV   PCON,#10000000B ; Bd = OSC/12/16/(256-TH1)
	MOV   TH1,#0FAH       ; 9600Bd
	MOV   IP,#00010011B   ; priority preruseni
	MOV   PSW,#0          ; banka registru 0
	MOV   SP,#STACK

	%VECTOR(TIMER0,MOTINC); Buzeni motoru
	%VECTOR(EXTI0,MOTIMP) ; Ohlasy motoru
	%VECTOR(EXTI1,I_TIME1); Realny cas

	MOV   A,#000H
	MOV   DPTR,#AUX_OUT
	MOVX  @DPTR,A         ; vypnuti vystupu

	MOV   LED_FLG,#0FFH
	CALL  LEDWR

	MOV   DPTR,#CNT53_CW  ; inicializace 8253
	MOV   A,#MOD53_0
	MOVX  @DPTR,A
	MOV   A,#MOD53_1
	MOVX  @DPTR,A
	MOV   A,#MOD53_2
	MOVX  @DPTR,A
	MOV   DPTR,#CNT53_0
	MOV   A,#VAL53_0L
	MOVX  @DPTR,A
	MOV   A,#VAL53_0H
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#VAL53_1
	MOVX  @DPTR,A

	CLR   A               ; nulovani gradientu
	MOV   DPTR,#GRAD_OUT
	MOVX  @DPTR,A
)FI
%IF (%EQS(%HW_VERSION,PB)) THEN (
	MOV   IEN0,#00000100B ; zakaz preruseni
	MOV   IEN1,#00000000B
	MOV   IP0, #00000000B ; priority preruseni
	%WATCHDOG	      ; Nulovani watch-dogu
	MOV   TMOD,#00100101B ; timer 1 mod 2; counter 0 mod 1
	MOV   TCON,#01010101B ; citac 0 a 1 cita ; interapy hranou
	MOV   TM2CON,#00100011B; counter 2 from T2, TR2 enabled
	MOV   SCON,#11011000B ; dva stopbity
	MOV   PCON,#10000000B ; Bd = OSC/12/16/(256-TH1)
	MOV   TH1,#0FAH       ; 9600Bd
	MOV   PSW,#0          ; banka registru 0
	MOV   SP,#STACK
	MOV   P1,#0FFH
	MOV   P3,#0FFH
	MOV   P4,#0FFH
%IF (%EQS(%REG_OUT_TYPE,PWM0_UNIDIR)) THEN (
	CLR   P4.2	      ; Povoleni vystupu PWM na spinac
)FI
	MOV   CTCON,#20H      ; Pocatek otacky, sestupna na P1.2

	%VECTOR(EXTI1,I_TIME1); Realny cas z vnejsiho zdroje

	MOV   PWMP,#0         ; PWM frekvence 23 kHz
	MOV   PWM0,#0

	MOV   CINT25,#1
	MOV   MOT_FLG,#0

  %IF(%PRESS_COR)THEN(
     %IF(1) THEN (
	MOV   DPTR,#M_CORSP
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#0F8H
	MOVX  @DPTR,A
     )FI
  )FI
)FI
	MOV   HW_FLG,#040H    ; povoleni casu smazani chyb
	MOV   HW_FLG1,#000H
	MOV   HW_FLG2,#000H

	CLR   A               ; nulovani gradientu
	MOV   LED_FLG,A       ; nulovani LED diod a modu
	CLR   %G_BF_FL
	CALL  LEDWR

	CALL  LCDINST         ; inicializace displeje
	JNZ   LP_INI0
	SETB  FL_DIPR
LP_INI0:
	MOV   R2,#000H        ; RAM test
	SJMP  LP_INI2

LP_INI1:JNB   FL_DIPR,LP_INID
	MOV   DPTR,#T_RAM_E
	CALL  cPRINT
LP_INID:DJNZ  R4,LP_INID
	DJNZ  R3,LP_INID
	DJNZ  R2,LP_INI2
	JMP   RESET

LP_INI2:MOV   DPTR,#RAM_T_B
	MOV   A,#055H
	MOVX  @DPTR,A
	MOVX  A,@DPTR
	CJNE  A,#055H,LP_INI1
	MOV   A,#-55H
	MOVX  @DPTR,A
	MOVX  A,@DPTR
	CJNE  A,#-55H,LP_INI1
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
;	CALL  SRAMSUM  ; !!!!!!!!!!!!!!!!
	MOV   A,R2
	CLR   C
	SUBB  A,R3
	JZ    LP_INI4
	JNB   FL_DIPR,LP_INI4
	MOV   DPTR,#T_SRAME
	CALL  cPRINT
LP_INI3:CALL  SCANKEY

;        JZ    LP_INI3         ; !!!!!!!!

	ADD   A,#-K_1
	JNZ   LP_INI4
	MOV   DPTR,#BEG_SRAM+3
	MOV   DPTR,#BEG_SRAM+3
	MOV   R2,#LOW(END_SRAM-BEG_SRAM)
	MOV   R3,#HIGH(END_SRAM-BEG_SRAM)
;        CALL  xNULs                      ; !!!!!!!!!!!!
	MOV   A,#55
	MOV   DPTR,#CHEK_SM
	MOVX  @DPTR,A

LP_INI4:CALL  C_PRESS
	CLR   A
	MOV   DPTR,#GRAD_BUF
	MOVX  @DPTR,A
	MOV   DPTR,#AUX_BUF
	MOVX  @DPTR,A
	MOV   DPTR,#AUX_OUT
	MOVX  @DPTR,A
	MOV   DPTR,#BEEPTIM
	MOVX  @DPTR,A

%IF (%EQS(%HW_VERSION,PB)) THEN (

%IF (%EQS(%REG_STRUCT_TYPE,PI)) THEN (
	%LDMXi(REG_P,0020H)
	%LDMXi(REG_I,0080H)
	%LDMXi(MAX_ENE,7F00H)
)FI
%IF (%EQS(%REG_STRUCT_TYPE,PD)) THEN (
  %IF(1)THEN(
	%LDMXi(REG_P,0060H)	; Standardni LCP 50xx
	%LDMXi(REG_D,0100H)
	%LDMXi(REG_I,0000H)
	%LDMXi(MAX_ENE,7F00H)
  )ELSE(
	%LDMXi(REG_P,0030H)	; Pro testy BDC motoru
	%LDMXi(REG_D,0000H)
	%LDMXi(REG_I,0000H)
	%LDMXi(MAX_ENE,7F00H)
  )FI
)FI
%IF(%SMOOTH_ACC)THEN(
	%LDMXi(MAX_ACC,5)
)FI
	CLR   A
	MOV   DPTR,#REG_ERC
	MOVX  @DPTR,A
	MOV   DPTR,#REG_FOI
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#REG_FOD
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#POS_RQI
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#ROT_BEG
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	CALL  M_RESP
)FI
	CLR   A
	MOV   R4,A
	MOV   R5,A
	CALL  S_FLOW

	CALL  SET_GA
	JMP   START

; Nastaveni konfigurace cerpadla
; ==============================
S_CFGFL:JB    FL_CLCK,S_CFGF9
	MOV   A,R4
	RRC   A
	MOV   PRESS_Q,C	      ; Trvaly vypis tlaku
	RRC   A
	MOV   FL_PURL,C	      ; Neprekrocit 1 MPa pri purge
	RRC   A
	MOV   FL_GRLT,C	      ; Pomale gradienty
	RRC   A
	MOV   FL_AUXV,C	      ; 8 ventilu na AUX
	RRC   A
	MOV   FL_P25,C	      ; Rozsah tlaku 25 MPa
	RRC   A
	MOV   FL_FLM2,C	      ; Nasobeni prutoku 2
	MOV   A,R4
	RLC   A
	MOV   FL_CLCK,C	      ; Zablokovani zmen konfigurace
S_CFGF9:RET

; Vypocet kontrolniho souctu SRAM
; ===============================
; vystup : R2
; meni   : DPTR,R4,R5

SRAMSUM:MOV   DPTR,#BEG_SRAM+3
	MOV   R4,#LOW(END_SRAM-BEG_SRAM)
	MOV   R5,#HIGH(END_SRAM-BEG_SRAM)
	MOV   R2,#055H
SRAMSU1:MOV   A,R4
	ORL   A,R5
	JZ    SRAMRET
	MOVX  A,@DPTR
	ADD   A,R2
	MOV   R2,A
	MOV   A,R4
	DEC   R4
	JNZ   SRAMSU1
	DEC   R5
	SJMP  SRAMSU1
SRAMRET:RET

;********************************************************************
;
;       Rozhrani gradientu s hardwarem

; Prima zmena sepnuteho gradientu
; ===============================
; Vstup R1 .. maska daneho gradientu
%IF (%EQS(%HW_VERSION,LP)) THEN (
WR_GRAD:MOV   DPTR,#GRAD_BUF  ; Zapis gradientu
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	ANL   A,#NOT GRAD_MSK
	ORL   A,R1
	MOVX  @DPTR,A
	MOV   DPTR,#GRAD_OUT
	MOVX  @DPTR,A
	MOV   EA,C
	RET
)FI
%IF (%EQS(%HW_VERSION,PB)) THEN (
G_A_MSK     SET   001H      ; gradient A
G_B_MSK     SET   002H      ; gradient B
G_C_MSK     SET   003H      ; gradient C
G_D_MSK     SET   004H      ; gradient D
GRAD_PMSK   SET   0F8H      ; vymaskovani gradientu
G_A_PMSK    SET   010H      ; gradient A .. P4.4 aktivni v 0
G_B_PMSK    SET   020H      ; gradient B .. P4.5
G_C_PMSK    SET   040H      ; gradient C .. P4.6
G_D_PMSK    SET   080H      ; gradient D .. P4.7
G_HC_MSK    SET   008H      ; zvyseny proud aktivni v 0
G_CUR_S	    BIT   P1.0      ; 0 curent sense
G_HC_TIM    SET   5         ; pocet tiku vysokeho proudu+1
G_HC_LTIM   SET   250       ; pro pomale ventily s FL_GRLT
RSEG	LP_HW_X
G_HC_TIMR:  DS    1         ; timer
RSEG	LP_HW_C
WR_GRAD:MOV   DPTR,#GRAD_BUF; Zapis gradientu
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	XRL   A,R1
	JZ    WR_GR_R
	MOV   A,R1
	MOVX  @DPTR,A
	RL    A
	MOV   R1,A
	MOV   DPTR,#WR_GR_T
	MOVC  A,@A+DPTR
	ANL   LED_FLG,#NOT GRAD_LMSK
	ORL   LED_FLG,A
	MOV   A,R1
	INC   A
	MOVC  A,@A+DPTR
	ORL   P4,#GRAD_PMSK
	ORL   A,#G_HC_MSK
	XRL   P4,A
	MOV   A,#G_HC_TIM	; pro rychle ventily
	JNB   FL_GRLT,WR_GRA1
	MOV   A,#G_HC_LTIM	; pro pomale ventily
WR_GRA1:MOV   DPTR,#G_HC_TIMR
	MOVX  @DPTR,A
	MOV   EA,C
	JMP   LEDWR
WR_GR_R:MOV   EA,C
	RET

WR_GR_T:DB    0,0
	DB    G_A_LMSK,G_A_PMSK
	DB    G_B_LMSK,G_B_PMSK
	DB    G_C_LMSK,G_C_PMSK
	DB    G_D_LMSK,G_D_PMSK
)FI

;********************************************************************
;
;       Zpracovani gradientu

; Spinani jednotlivy gradientu
; ============================

SET_GB: MOV   R1,#G_B_MSK
	SJMP  SET_G1
SET_GC: MOV   R5,#100
	MOV   R1,#G_C_MSK
	JMP   SET_G2
SET_GA: MOV   R1,#G_A_MSK
SET_G1: MOV   R5,#0
SET_G2: MOV   R4,#0
	JB    %G_BF_FL,WR_GRAD
	MOV   DPTR,#GRAD_C
	MOV   C,EA
	CLR   EA
	CALL  xSVR45i
	MOV   EA,C
	MOV   R5,#0
	CJNE  R1,#G_B_MSK,SET_G3
	MOV   R5,#100
SET_G3: MOV   DPTR,#GRAD_B
	MOV   C,EA
	CLR   EA
	CALL  xSVR45i
	MOV   EA,C
	CALL  WR_GRAD

; Zmena gradientu podle GRAD_B,GRAD_C
; ===================================

S_GRAD: JNB   %G_BF_FL,S_GRAD0
	JMP   S_FLOW0
S_GRAD0:MOV   DPTR,#GRAD_C
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A	      ; R45:=GRAD_C
	MOV   DPTR,#GRAD_B
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   EA,C
	XCH   A,R0
	ADD   A,R4
	XCH   A,R0
	ADDC  A,R5
	MOV   R1,A	      ; R01:=GRAD_B+GRAD_C
	CALL  S_GRADS	      ; vypocet G_B_E
	MOV   A,R0
	XCH   A,R4
	MOV   R0,A
	MOV   A,R1
	XCH   A,R5	      ; R45:=GRAD_B+GRAD_C
	MOV   R1,A	      ; R01:=G_B_E
	CALL  S_GRADS	      ; vypocet G_A_E
	MOV   DPTR,#G_A_E
	MOV   C,EA
	CLR   EA
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
	MOV   DPTR,#G_B_E
	MOV   A,R0
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A
	MOV   EA,C
	SETB  U_CONC
	RET

S_GRADS:MOV   R2,#LOW  (GR_P45)
	MOV   R3,#HIGH (GR_P45)
	CALL  CMPi
	JNC   S_GRADT
	MOV   R2,#LOW  (GR_P5+1)
	MOV   R3,#HIGH (GR_P5+1)
	CALL  CMPi
	JC    S_GRADX
	MOV   R2,#LOW  GR_A41
	MOV   R3,#HIGH GR_A41
	CALL  MULi
	CLR   C
	MOV   A,#LOW  GR_A40
	SUBB  A,R6
	MOV   R4,A
	MOV   A,#HIGH GR_A40
	SUBB  A,R7
	MOV   R5,A
	RET
S_GRADZ:CLR   A
	MOV   R5,A
	MOV   R4,A
	RET
S_GRADV:MOV   R5,#0FFH
	MOV   R4,#0FFH
	RET
S_GRADX:MOV   A,R4
	ORL   A,R5
	JZ    S_GRADV
	MOV   DPTR,#GR_TAB
	SETB  F0
	JMP   S_GRADU
S_GRADT:MOV   A,R5
	ADD   A,#-100
	JC    S_GRADZ
	CLR   F0
	CLR   C
	CLR   A
	SUBB  A,R4
	MOV   R4,A
	MOV   A,#100
	SUBB  A,R5
	MOV   R5,A
	MOV   DPTR,#GR_TAB
S_GRADU:MOV   A,R4
	SWAP  A
	ANL   A,#00FH
	RL    A
	MOV   R4,A
	MOV   A,R5
	SWAP  A
	RL    A
	MOV   R5,A
	ANL   A,#01FH
	XCH   A,R5
	XRL   A,R5
	ORL   A,R4
	ADD   A,DPL
	MOV   DPL,A
	MOV   A,R5
	ADDC  A,DPH
	MOV   DPH,A
	CLR   A
	MOVC  A,@A+DPTR
	MOV   R4,A
	MOV   A,#1
	MOVC  A,@A+DPTR
	MOV   R5,A
	JBC   F0,S_GRADY
	MOV   A,#LOW  GR_MINI
	ADD   A,R4
	MOV   R4,A
	MOV   A,#HIGH GR_MINI
	ADDC  A,R5
	MOV   R5,A
	RET
S_GRADY:CLR   C
	MOV   A,#LOW  GR_MAXI
	SUBB  A,R4
	MOV   R4,A
	MOV   A,#HIGH GR_MAXI
	SUBB  A,R5
	MOV   R5,A
	RET


; Spinani jednotlivych gradientu podle R4
; =======================================

S_GRDIR:MOV   A,R4
	JNB   ACC.7,S_GRDI1
	MOV   C,ACC.6
	MOV   FL_GRLT,C
S_GRDI1:MOV   A,R4
	ANL   A,#0FH
	MOV   R4,A
	DJNZ  R4,S_GRDI2
	JMP   SET_GB
S_GRDI2:DJNZ  R4,S_GRDI3
	JMP   SET_GC
S_GRDI3:JMP   SET_GA

;********************************************************************
;
;       Zpracovani vstupu a vystupu

; Nastaveni AUX podle R4
; ======================

S_AUX:  MOV   A,R4
	ANL   A,#00FH
	MOV   R2,A
	MOV   DPTR,#AUX_BUF
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	ANL   A,#0F0H
	ORL   A,R2
	MOVX  @DPTR,A
%IF (%EQS(%HW_VERSION,PB)) THEN (
	SWAP  A
)FI
	MOV   DPTR,#AUX_OUT
	MOVX  @DPTR,A
	MOV   EA,C
G_AUX:  MOV   DPTR,#AUX_BUF
	MOVX  A,@DPTR
	ANL   A,#00FH
	MOV   R2,A
%IF (%EQS(%HW_VERSION,LP)) THEN (
	MOV   DPTR,#ADC_HI
	MOVX  A,@DPTR
	RR    A
	RR    A
	ANL   A,#030H
)FI
%IF (%EQS(%HW_VERSION,PB)) THEN (
	MOV   A,P4
	SWAP  A
	ANL   A,#030H
)FI
	ORL   A,R2
	MOV   R2,A
	MOV   DPTR,#AUXUAL
	MOVX  A,@DPTR
	XRL   A,R2
	JZ    G_AUXR
	SETB  U_AUX
	MOV   A,R2
	MOVX  @DPTR,A
G_AUXR: RET


; Pouziti AUX vystupu pro 8 ventilu
; =================================

S_AUXVALV:MOV A,R4
	DEC   A
	ANL   A,#7
	MOV   R4,A
	CALL  S_AUX
	CLR   FL_AUXV
	MOV   A,#15	; cas vyssiho proudu 1/25s
	MOV   DPTR,#AUXV_TIM
	MOVX  @DPTR,A
	SETB  FL_AUXV
	RET

G_AUXVALV:MOV DPTR,#AUXUAL
	MOVX  A,@DPTR
	MOV   C,ACC.4
	ANL   A,#7
	INC   A
	MOV   R4,A
	JB    FL_AUXV,G_AUXV2
	JNC   G_AUXV2
	MOV   R4,#0
G_AUXV2:MOV   R5,#0
	RET

;********************************************************************
;
;       Zpracovani tlaku

; Nacteni a kontrola tlaku
; ========================

G_PRESS:
%IF (%EQS(%HW_VERSION,LP)) THEN (
	MOV   DPTR,#ADC_HI
	SETB  F0
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	ANL   A,#ADC_DR
	JNZ   G_PRESR
	MOVX  A,@DPTR
	ANL   A,#003H
	MOV   R5,A
	MOV   DPTR,#ADC_LO
	MOVX  A,@DPTR
	MOV   EA,C
	CLR   F0
	MOV   R4,A
)FI
%IF (%EQS(%HW_VERSION,PB)) THEN (
	SETB  F0
	MOV   C,EA
	CLR   EA
	MOV   A,ADCON
	JNB   ACC.4,G_PRESR
	RL    A
	RL    A
	ANL   A,#03H
	MOV   R4,A
	MOV   A,ADCH
	ANL   ADCON,#NOT 10H
	MOV   EA,C
	CLR   F0
	RL    A
	RL    A
	MOV   R5,A
	ANL   A,#003H
	XCH   A,R5
	ANL   A,#0FCH
	ORL   A,R4
	MOV   R4,A
	JNB   FL_P25,G_PRES2  ; Rozsah 50 MPa
	CLR   C		      ; Rozsah 25 MPa
	MOV   A,R5
	RRC   A
	MOV   R5,A
	MOV   A,R4
	RRC   A
	MOV   R4,A
G_PRES2:
)FI
G_PRES4:%WATCHDOG
	MOV   DPTR,#PRESS_L   ; Dolni tlakova mez
	MOVX  A,@DPTR
	INC   DPTR
	SETB  C
	SUBB  A,R4
	MOVX  A,@DPTR
	INC   DPTR
	SUBB  A,R5
	CPL   C
	MOV   PRESS_B,C
	MOV   C,FL_PURL
	ANL   C,%PURGE_FL
	JNC   G_PRES6
	CLR   C		      ; Neprekrocit 1 MPa pri purge
	MOV   A,#LOW  (1*20)
	SUBB  A,R4
	MOV   A,#HIGH (1*20)
	SUBB  A,R5
	JC    G_PRES7
G_PRES6:MOV   DPTR,#PRESS_H   ; Horni tlakova mez
	MOVX  A,@DPTR
	INC   DPTR
	CLR   C
	SUBB  A,R4
	MOVX  A,@DPTR
	INC   DPTR
	SUBB  A,R5
G_PRES7:MOV   PRESS_O,C
	JC    M_STOPE
	MOV   C,EA
G_PRESR:MOV   EA,C
	RET

; Spusteni dalsiho prevodu
; ========================

%IF (%EQS(%HW_VERSION,LP)) THEN (
C_PRESS:MOV   DPTR,#AUX_BUF   ; Spusteni dalsiho prevodu
	MOVX  A,@DPTR
	ORL   A,#ADC_ST
	MOV   DPTR,#AUX_OUT
	MOVX  @DPTR,A
	ANL   A,#NOT ADC_ST
	MOVX  @DPTR,A
	RET
)FI
%IF (%EQS(%HW_VERSION,PB)) THEN (
C_PRESS:MOV   ADCON,#1
	ORL   ADCON,#8
	RET
)FI

;********************************************************************
;
;       Rizeni motoru

; Zastaveni motoru
; ================

M_STOPE:JNB   %START_FL,M_STOP ; Zastaveni pri chybe
	SETB  PRESS_E
M_STOP: CLR   %START_FL        ; Zastaveni motoru
	CLR   %PURGE_FL
M_STOP1:
%IF (%EQS(%HW_VERSION,PB)) THEN (
	CLR   FL_ENE
	CALL  S_ENERC
)FI
%IF (%EQS(%HW_VERSION,LP)) THEN (
	CLR   EX0
	CLR   ET0
	MOV   DPTR,#GRAD_BUF
	MOVX  A,@DPTR
	ANL   A,#NOT MOT_ON
	MOVX  @DPTR,A
	MOV   DPTR,#GRAD_OUT
	MOVX  @DPTR,A
	MOV   DPTR,#CNT53_CW
	MOV   A,#MOD53_2
	MOVX  @DPTR,A
)FI
	JNB   %G_BF_FL,M_STOP9
	SETB  U_CONC           ; Gradient prutokem
	SETB  GBF_CHRQ	       ; Pro update stavu slave
M_STOP9:JMP   LEDWR

; Rezim purge
; ===========

M_PURGE:JB    %PURGE_FL,M_START
	SETB  %START_FL
	SETB  %PURGE_FL
	CALL  LEDWR
	MOV   R4,#LOW  PURGE_K
	MOV   R5,#HIGH PURGE_K
    %IF(0)THEN(	
	JB    FL_FLM2,S_PURG2
	JMP   S_FLOW1
S_PURG2:MOV   R4,#LOW  (PURGE_K/2)
	MOV   R5,#HIGH (PURGE_K/2)
    )FI
	JMP   S_FLOW1

; Chybne zadany prutok
S_FLOWE:SETB  F0
	RET

; Nastaveni prutoku do FLOW a aktualizace
; =======================================

S_FLOW: MOV   DPTR,#FLOW
	%LDR23i((%C_MAX_FLOW*%C_FLOW_FMUL)+1)
	CALL  CMPi
	JNC   S_FLOWE
	CALL  xSVR45i
	SETB  U_FLOW
	DB    090H    ; MOV  DPTR,#d16

; Spusteni motoru - prutok podle FLOW
; ============================

M_START:SETB  %START_FL
	CLR   %PURGE_FL
	CALL  LEDWR
S_FLOW0:MOV   DPTR,#FLOW
	CALL  xLDR45i
S_FLOW1:JNB   %START_FL,M_STOP
	JB    PRESS_O,M_STOPE
	MOV   A,R5            ; Nastaveni prutoku podle R45
	ORL   A,R4
	JNZ   S_FLOW1_1
	JNB   %G_BF_FL,M_STOP1; Zastaveni motoru
%IF(%WITH_UL_G_BF) THEN (
	MOV   DPTR,#GBF_FLOWB  ; Zastavit slave
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
)FI
	SJMP  M_STOP1
S_FLOW1_1:
	JNB   %G_BF_FL,S_FLONG
	SETB  U_CONC          ; Gradient prutokem
	; Vypocet vysokotlakeho gradientu
%IF(%WITH_UL_G_BF) THEN (
	MOV   DPTR,#GRAD_B
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	ORL   A,R2
	JNZ   S_FLOW1_2
	MOV   DPTR,#GBF_FLOWB  ; Slave na nule
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	SETB  GBF_CHRQ
	SJMP  S_FLOW1_9
S_FLOW1_2:MOV A,R3
	ADD   A,#-100
	JNC   S_FLOW1_3
	MOV   DPTR,#GBF_FLOWB  ; Slave na 100 procent
	CALL  xSVR45i
	SETB  GBF_CHRQ
	JMP   M_STOP1
S_FLOW1_3:
	MOV   A,R4
	MOV   R0,A
	MOV   A,R5
	MOV   R1,A
	%LDR45i (41943)
	CALL  MULi
	MOV   A,#16-2	
	CALL  SHRl
	MOV   A,R0
	MOV   R2,A
	MOV   A,R1
	MOV   R3,A
	CALL  MULi
	MOV   A,R5
	MOV   C,ACC.7
	MOV   DPTR,#GBF_FLOWB  ; Prutok pro slave
	MOV   A,R6
	ADDC  A,#0
	MOV   R4,A
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	ADDC  A,#0
	MOV   R5,A
	MOVX  @DPTR,A
	SETB  GBF_CHRQ
	CLR   C
	MOV   A,R0
	SUBB  A,R4
	MOV   R4,A
	MOV   A,R1
	SUBB  A,R5
	MOV   R5,A
S_FLOW1_9:
)FI	
%IF (%EQS(%HW_VERSION,PB)) THEN (
; Cv=183883.6 IRC/ml
; Cf=Cv*2^16/(1000*60*675)=297.55545=38087*2^-7
S_FLONG:%LDR23i (FLOW_K)      ; 38087
	CALL  MULi
	MOV   A,#FLOW_K_E     ; 7
	JNB   FL_FLM2,S_FLON2
	DEC   A
S_FLON2:CALL  SHRl
    %IF(%PRESS_COR)THEN(
	MOV   B,#0
      %IF(%SMOOTH_ACC)THEN(
	CLR   C
	MOV   DPTR,#SPD_RQ1+2
	MOVX  A,@DPTR
	SUBB  A,R6
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,R7
	MOV   R3,A
	JNB   ACC.7,S_FLON2_3
	CLR   C
	CLR   A
	SUBB  A,R2
	MOV   R2,A
	CLR   A
	SUBB  A,R3
	MOV   R3,A
S_FLON2_3:
	MOV   A,R2
	ADD   A,#LOW(-6)
	MOV   A,R3
	ADDC  A,#HIGH(-6)
	MOV   B.7,C
      )FI
	MOV   DPTR,#SPD_RQ1   ; konstantni cast rychlosti
    )ELSE(
	MOV   DPTR,#SPD_RQ
    )FI
	MOV   C,EX1
	CLR   EX1
    %IF(%SMOOTH_ACC)THEN(
	JNB   B.7,S_FLON2_4
	SETB  FL_SPDC
S_FLON2_4:
    )FI
	CALL  xSVl
	MOV   EX1,C
	JB    FL_ENE,S_FLON5
    %IF(%SMOOTH_ACC AND %PRESS_COR)THEN(
	MOV   DPTR,#SPD_RQ
	MOV   R2,#POS_SD+2
	CLR   A
S_FLON3:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R2,S_FLON3
	SETB  FL_SPDC		; Plynuly rozbeh
    )FI
	MOV   DPTR,#POS_RQ
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	INC   DPTR
	MOV   R0,#POS_ACT
	MOV   R2,#3
S_FLON4:MOV   A,@R0
	MOVX  @DPTR,A
	INC   DPTR
	INC   R0
	DJNZ  R2,S_FLON4
	MOV   DPTR,#REG_FOI
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#REG_FOD
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	MOV   DPTR,#PRES_OL
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
S_FLON5:SETB  FL_ENE
	CLR   FL_RERR
	CLR   FL_ENOV
	RET
)FI
%IF (%EQS(%HW_VERSION,LP)) THEN (
S_FLONG:MOV   A,R4
	MOV   R2,A
	MOV   A,R5
	MOV   R3,A
	MOV   R4,#LOW  FLOW_K
	MOV   R5,#HIGH FLOW_K
	SETB  F0
	MOV   R1,#FLOW_K_E
	CALL  DIVi1
	MOV   A,R6
	MOV   R4,A
	MOV   A,R7
	MOV   R5,A
S_FLOW2:MOV   A,R1
	JNB   ACC.7,S_FLOW3
	CPL   A
	INC   A
	CALL  SHRi
	MOV   R1,A
S_FLOW3:MOV   R2,#LOW  S_T0_D
	MOV   R3,#HIGH S_T0_D
	CALL  SUBi
	MOV   A,#080H
	INC   R1
S_FLOW4:RL    A
	DJNZ  R1,S_FLOW4
	CLR   EX0             ; Presun do vnitrnich promennych
	MOV   M_PER+2,A
	CLR   C
	RLC   A
	JC    S_FLOW5
	SUBB  A,M_BYTE3
	JNC   S_FLOW5
	ADD   A,M_BYTE3
	MOV   M_BYTE3,A
S_FLOW5:MOV   DPTR,#M_T
	CALL  xSVR45i
	SETB  M_CORFL
	JB    ET0,S_FLOW6
	CLR   M_EN_ER         ; Rozbehnuti motoru po stani
	%WATCHDOG
	MOV   M_PER,R4
	MOV   M_PER+1,R5
	MOV   M_ENERG,#00H
	MOV   M_BYTE3,#01H
	MOV   TL0,#0F0H
	MOV   TH0,#0FFH
S_FLOW6:SETB  ET0
	SETB  EX0
	RET
)FI

%IF (%EQS(%HW_VERSION,LP)) THEN (

; Buzeni motoru
; =============
; Vyuziva 6 byte staku

MOTINC: PUSH  ACC
	PUSH  PSW
	JNB   %START_FL,MOTINCR
	CLR   C
	CLR   TR0

S_T0_D  SET   7               ; zpozdeni rutiny

	MOV   A,TL0           ; 7 Cyklu
	SUBB  A,M_PER
	MOV   TL0,A
	MOV   A,TH0
	SUBB  A,M_PER+1
	MOV   TH0,A
	SETB  TR0
	CLR   TF0
	DJNZ  M_BYTE3,MOTINCR
	MOV   M_BYTE3,M_PER+2
	MOV   A,M_ENERG
	ADD   A,#ENERG_ST
	SETB  M_EN_ER
	JB    OV,MOTINCR
	CLR   M_EN_ER
	MOV   M_ENERG,A
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#CNT53_2   ; Zvyseni energie
	MOVX  @DPTR,A         ; pro motor
	ADD   A,#-ENERG_ST
	JNZ   MOTINC1
	MOV   DPTR,#GRAD_BUF
	MOVX  A,@DPTR
	ORL   A,#MOT_ON
	MOVX  @DPTR,A
	MOV   DPTR,#GRAD_OUT
	MOVX  @DPTR,A
MOTINC1:POP   DPH
	POP   DPL
MOTINCR:POP   PSW
	POP   ACC
	RETI

USING   3

; Zpracovani ohlasu z motoru
; ==========================
; Vyuziva RB3, 9 byte staku

MOTIMP: PUSH  IE
	PUSH  ACC
	PUSH  PSW
	ANL   IE,#NOT 00001101B
	SETB  EA              ; Zakazani interapu od motoru, casu
	CLR   IE0             ;   a  IT1
	MOV   PSW,#00011000B  ; Banka registru 3
	INC   M_POS
	MOV   A,M_POS
	JNZ   MOTIMP1
	INC   M_POS+1
MOTIMP1:PUSH  DPL
	PUSH  DPH
	PUSH  IE
	CLR   ET0             ; Snizeni energie
	MOV   A,M_ENERG
	CLR   C
	SUBB  A,#ENERG_ST
	JB    OV,MOTIMP3      ; Podcitani energie
MOTIMP2:MOV   M_ENERG,A
	MOV   DPTR,#CNT53_2
	MOVX  @DPTR,A
	DEC   A
	JNB   ACC.7,MOTIMP3
	MOV   DPTR,#GRAD_BUF
	MOVX  A,@DPTR
	ANL   A,#NOT MOT_ON
	MOVX  @DPTR,A
	MOV   DPTR,#GRAD_OUT
	MOVX  @DPTR,A
MOTIMP3:POP   IE
	MOV   DPTR,#ADC_HI    ; Sledovani pulotocky
	MOVX  A,@DPTR
	ANL   A,#MOT_PUL
	MOV   C,M_POSFL
	SETB  M_POSFL
	JNZ   MOTIMP4
	CLR   M_POSFL
	JNC   MOTIMP4
	CLR   A
	MOV   M_POS,A
	MOV   M_POS+1,A
	MOV   DPTR,#M_COR_F
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A

	CALL  G_PRESS         ; Zpracovani tlaku
	JB    F0,MOTIMP4
	MOV   DPTR,#PRESS
	CALL  xSVR45i
	SETB  U_PRESS

MOTIMP4:MOV   DPTR,#M_COR     ; Provadi se korekce ?
	CALL  CMP_POS
	JC    MOTIMP6         ; ne

	SETB  M_CORFL         ; Korekce
	MOV   DPTR,#M_T
	CALL  xLDR45i
	MOV   DPTR,#M_COR_F
	MOVX  A,@DPTR         ; Cteni M_COR_F
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	JZ    MOTIMP5
	MOV   R1,B
	CALL  SMULi
	MOV   B,R1
	CLR   EA
	MOV   M_PER,R5
	MOV   M_PER+1,R6
	SETB  EA
MOTIMP5:MOV   R4,#LOW  M_COR_K
	MOV   R5,#HIGH M_COR_K
	CALL  ADDi           ; Linearni zmena korekce
	MOV   DPTR,#M_COR_F
	CALL  xSVR45i
	SJMP  MOTIMP7

MOTIMP6:JNB   M_CORFL,MOTIMP7
	CLR   M_CORFL         ; Ukonceni korekce
	MOV   DPTR,#M_T
	MOVX  A,@DPTR
	CLR   EA
	MOV   M_PER,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   M_PER+1,A
	SETB  EA

MOTIMP7:JB    %G_BF_FL,MOTIMP9 ; Gradienty prutokem
	CLR   C
	MOV   A,M_POS
	SUBB  A,#LOW (M_MAXPOS)
	MOV   A,M_POS+1
	SUBB  A,#HIGH (M_MAXPOS)
	JNC   MOTIMP9
	MOV   DPTR,#G_A_E     ; Gradienty ventily
	MOV   R1,#G_A_MSK
	CALL  CMP_POS
	JNC   MOTIMP8         ; A
	MOV   R1,#G_B_MSK
	CALL  CMP_POS
	JNC   MOTIMP8         ; B
	MOV   R1,#G_C_MSK     ; C
MOTIMP8:CALL  WR_GRAD
MOTIMP9:CALL  C_PRESS
	POP   DPH
	POP   DPL
	POP   PSW
	POP   ACC
	POP   IE              ; Povoleni interapu
	RETI
)FI

CMP_POS:MOVX  A,@DPTR
	INC   DPTR
	SETB  C
	SUBB  A,M_POS
	MOVX  A,@DPTR
	INC   DPTR
	INC   A
	JNZ   CMP_PO1
	CLR   C
	RET
CMP_PO1:DEC   A
	SUBB  A,M_POS+1
	RET

;********************************************************************
;
;       System rizeni pro desku PB

%IF (%EQS(%HW_VERSION,PB)) THEN (
EXTRN	CODE(G_POS,REG_%REG_STRUCT_TYPE)
%IF(%PRESS_COR)THEN(
EXTRN	CODE(COR_TAB)
EXTRN	NUMBER(COR_TSH)
EXTRN	NUMBER(COR_TIRC)
)FI

PUBLIC	FL_ENOV,FL_RERR,FL_RSP,POS_ACT,SPD_ACT
PUBLIC	POS_RQ,POS_RQI,SPD_RQ,SPD_RQI
PUBLIC	REG_P,REG_I,REG_D,MAX_ENE,MAX_SPD,MAX_ACC
PUBLIC	REG_FOI,REG_FOD,REG_ENE,REG_ERC

RSEG LP_HW_B
MOT_FLG:DS    1
FL_RSP	BIT   MOT_FLG.1
FL_RERR	BIT   MOT_FLG.2  ; Rozpad regulace
FL_ENOV	BIT   MOT_FLG.3  ; Preteceni energie pro rozbeh motoru
FL_ENE	BIT   MOT_FLG.4	 ; Povolen vystup energie
FL_LGNR	BIT   MOT_FLG.5	 ; Posledni gradient nesepnul
FL_GPRE	BIT   MOT_FLG.6	 ; Pruchod pulotocky, sejmi tlak
FL_SPDC	BIT   MOT_FLG.7	 ; Velka zmena rychlosti pro SMOOTH_ACC

RSEG LP_HW_D
DINT25  EQU   27   ; Delitel EXINT1 na 25 Hz
CINT25: DS    1
POS_ACT:DS    3
SPD_ACT:DS    2

RSEG LP_HW_X

POS_SD	SET   2		; Pocet byte do jednoho impulsu IRC
; Pozadavky na PID
POS_RQ: DS    POS_SD	; Pozadavek na pozici pro regulator
POS_RQI:DS    3
SPD_RQ: DS    POS_SD	; Pozadovana rychlost pro regulator
SPD_RQI:DS    2

; Konstanty regulatoru
REG_P:  DS    2   ; Konstanty regulatoru
REG_I:  DS    2
REG_D:  DS    2   ; Nevyuzito
MAX_ENE:DS    2	  ; Maximalni povolena hodnota energie
MAX_SPD:DS    2   ; Maximalni rychlost
MAX_ACC:DS    2   ; Maximalni zrychleni

REG_FOI:DS    2   ; Minula slozka I
REG_FOD:DS    2   ; Minula slozka D
REG_ENE:DS    2   ; Vypoctena energie pro motor
REG_ERC:DS    1   ; Cas po ktery se nehybe motor pri max energii

%IF(%PRESS_COR)THEN(
SPD_RQ1:DS    4   ; konstantni cast rychlosti
M_CORAD:DS    4	  ; Pridavek od korekce prutoku
M_CORSP:DS    2	  ; Ulozeni prubehu pohybu
M_CORTM:DS    2   ; Pomocna promenna pro debug
)FI

RSEG LP_HW_C

M_RESP: PUSH  IE
	CLR   EA
	CALL  S_ENERC
	MOV   DPTR,#POS_RQ
	MOV   A,#POS_SD
	JZ    M_RESP2
	MOV   R2,A
	CLR   A
M_RESP1:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R2,M_RESP1
M_RESP2:MOV   DPTR,#POS_RQI
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R6,A
	MOV   POS_ACT,R4
	MOV   POS_ACT+1,R5
	MOV   POS_ACT+2,R6
	CLR   TR0
	MOV   R3,TMH2
	MOV   R2,TML2
	MOV   A,TMH2
	XRL   A,R3
	JZ    M_RESP4
	MOV   A,R2
	JB    ACC.7,M_RESP4
	INC   R3
M_RESP4:CALL  NEGi
	CALL  ADDi
	MOV   TL0,R4
	MOV   TH0,R5
	SETB  TR0
	POP   IE
	RET

; Nastavi vystupni energii podle R45

S_ENERG:MOV   A,R4
	RLC   A
	MOV   A,R5
	RLC   A
	JZ    S_ENERC
	JC    S_ENER1
%IF (%EQS(%REG_OUT_TYPE,PWM0_BIDIR)) THEN (
	JNB   MDIRM,S_ENERC
;	SETB  MDIRM
)FI
	MOV   PWM0,A
%IF (%EQS(%REG_OUT_TYPE,PWM0_BIDIR)) THEN (
	CLR   MDIRP
)FI
	RET
S_ENER1:
%IF (%EQS(%REG_OUT_TYPE,PWM0_BIDIR)) THEN (
	JNB   MDIRP,S_ENERC
;	SETB  MDIRP
	MOV   PWM0,A
	CLR   MDIRM
	RET
)FI
S_ENERC:
%IF (%EQS(%REG_OUT_TYPE,PWM0_BIDIR)) THEN (
	SETB  MDIRP
	SETB  MDIRM
)FI
	MOV   PWM0,#0
	RET

)FI
%IF (%EQS(%HW_VERSION,PB)) THEN (

INC_POS:
  %IF(%PRESS_COR)THEN(
	MOV   DPTR,#SPD_RQ1
	CALL  xLDl
    %IF(%SMOOTH_ACC)THEN(
	JB    FL_SPDC,INC_P60v
    )FI
	MOV   DPTR,#SPD_RQ
	CALL  xSVl
	JB    M_CORFL,INC_P40
  )ELSE(
	MOV   DPTR,#SPD_RQ
	CALL  xLDl
  )FI
	MOV   DPTR,#POS_RQ
	MOV   A,R0
	ADD   A,#-4
	MOV   R0,A
	MOV   R2,#4
	CLR   C
INC_P10:MOVX  A,@DPTR
	ADDC  A,@R0
	MOVX  @DPTR,A
	INC   DPTR
	INC   R0
	DJNZ  R2,INC_P10
	DEC   R0
	MOV   A,@R0
	MOV   R7,#0
	JNB   ACC.7,INC_P20
	DEC   R7
INC_P20:MOVX  A,@DPTR
	ADDC  A,R7
	MOVX  @DPTR,A
INC_P29:RET

    %IF(%SMOOTH_ACC)THEN(
INC_P60v:JMP  INC_P60
    )FI
)FI
%IF (%EQS(%HW_VERSION,PB)) THEN (
%IF(%PRESS_COR)THEN(

INC_P40:MOV   DPTR,#POS_RQ
	MOVX  A,@DPTR	      ; POS_RQ += SPD_RQ
	ADD   A,R4	      ; pouze pro kladne SPD_RQ
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R2,A	      ; R23 = POS_RQI
	ADDC  A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	ADDC  A,R7
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#0
	MOVX  @DPTR,A
	MOV   DPTR,#ROT_BEG   ; R23 += ROT_BEG
	MOVX  A,@DPTR
	ADD   A,R2
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R3	      ; R23 .. pozice v IRC
	MOV   R3,A	      ; R4567 .. SPD_RQ
	JB    ACC.7,INC_P29
	CLR   C
	MOV   A,R2
	SUBB  A,#LOW COR_TIRC
	MOV   A,R3
	SUBB  A,#HIGH COR_TIRC
	JNC   INC_P46
	MOV   R1,#COR_TSH-1
INC_P45:CLR   C		      ; R23 >>= R1
	MOV   A,R3
	RRC   A
	MOV   R3,A
	MOV   A,R2
	RRC   A
	MOV   R2,A
	DJNZ  R1,INC_P45
	ANL   A,#NOT 1
	ADD   A,#LOW COR_TAB
	MOV   DPL,A
	MOV   A,R3
	ADDC  A,#HIGH COR_TAB
	MOV   DPH,A
	MOVX  A,@DPTR
	MOV   R2,A	      ; R23 = COR_TAB[R23]
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	MOV   A,R6	     ; R4567 = (R4567*R23)/65536
	MOV   R0,A
	MOV   A,R7
	MOV   R1,A
	CALL  MULi
	MOV   A,R6
	XCH   A,R0
	MOV   R4,A
	MOV   A,R7
	XCH   A,R1
	MOV   R5,A
	CALL  MULi
	MOV   A,R0
	ADD   A,R4
	MOV   R4,A
	MOV   A,R1
	ADDC  A,R5
	MOV   R5,A
	CLR   A
	ADDC  A,R6
	MOV   R6,A
	CLR   A
	ADDC  A,R7
	MOV   R7,A
INC_P46:MOV   DPTR,#M_CORAD   ; M_CORAD += R4567
	MOVX  A,@DPTR	      ; R0123 = M_CORAD
	ADD   A,R4
	MOV   R0,A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOV   R1,A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R6
	MOV   R2,A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R7
	MOV   R3,A
	MOVX  @DPTR,A
	MOV   DPTR,#M_COR     ; R23 -= M_COR
	MOVX  A,@DPTR
	CLR   C
	XCH   A,R2
	SUBB  A,R2
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R3
	SUBB  A,R3
	MOV   R3,A
	JC    INC_P47
	CLR   M_CORFL	      ; Dosazen konec korekce
	CLR   C
	MOV   A,R4	      ; R4567 -= R0123
	SUBB  A,R0
	MOV   R4,A
	MOV   A,R5
	SUBB  A,R1
	MOV   R5,A
	MOV   A,R6
	SUBB  A,R2
	MOV   R6,A
	MOV   A,R7
	SUBB  A,R3
	MOV   R7,A
	JC    INC_P49
)FI
)FI
%IF (%EQS(%HW_VERSION,PB)) THEN (
%IF(%PRESS_COR)THEN(
INC_P47:MOV   DPTR,#SPD_RQ    ; SPD_RQ += R4567
	MOVX  A,@DPTR
	ADD   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R7
	MOVX  @DPTR,A
INC_P48:MOV   DPTR,#POS_RQ    ; POS_RQ += R4567
	MOVX  A,@DPTR
	ADD   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R5
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R7
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,#0
	MOVX  @DPTR,A
INC_P49:RET

)FI
)FI
%IF(%EQS(%HW_VERSION,PB)) THEN (
%IF(%SMOOTH_ACC)THEN(
; Plynuly rozjezd - R4567=SPD_RQ1
INC_P60:MOV   DPTR,#SPD_RQ+1
	CLR   C
	MOVX  A,@DPTR
	MOV   R1,A
	SUBB  A,R5
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R2,A
	SUBB  A,R6
	MOV   B,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A		; R123=SPD_RQ>>8
	SUBB  A,R7              ; R0BA=(SPD_RQ>>8)-R567
	JNB   ACC.7,INC_P65
	CJNE  A,#0FFH,INC_P64
	MOV   DPTR,#MAX_ACC	; Zrychleni
	MOVX  A,@DPTR
	ADD   A,R0
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,B
	JC    INC_P67
INC_P64:MOV   DPTR,#MAX_ACC
	MOVX  A,@DPTR
	ADD   A,R1
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R2
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R3
	MOV   R7,A
	SJMP  INC_P68
INC_P65:JNZ   INC_P66		; Zpomaleni
	CLR   C
	MOV   DPTR,#MAX_ACC
	MOVX  A,@DPTR
	SUBB  A,R0
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,B
	JNC   INC_P67
INC_P66:SETB  C
	MOV   DPTR,#MAX_ACC
	MOVX  A,@DPTR
	CPL   A
	ADDC  A,R1
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	CPL   A
	ADDC  A,R2
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	CPL   A
	ADDC  A,R3
	MOV   R7,A
	SJMP  INC_P68
INC_P67:CLR   FL_SPDC
INC_P68:MOV   DPTR,#SPD_RQ
	CALL  xSVl
	CLR   M_CORFL
	JMP   INC_P48
)FI
)FI

%IF (%EQS(%HW_VERSION,PB)) THEN (
RSEG LP_HW_X

ROT_BEG:DS    2	  ; Pocatek otacky
ROT_CHK:DS    2	  ; Pocet impulsu za otacku

RSEG LP_HW_C

S_GRO:  MOV   A,M_POS+1
	JZ    S_GRO10
	JNB   CTI2,S_GRO10	; Pocatek otacky, sestupna na P1.2
	CLR   CTI2
	JB    P1.2,S_GRO10

	SETB  FL_GPRE
	MOV   DPTR,#ROT_CHK
	MOV   A,M_POS
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,M_POS+1
	MOVX  @DPTR,A

	MOV   DPTR,#ROT_BEG
	CLR   C
	CLR   A
	SUBB  A,POS_ACT
	MOVX  @DPTR,A
	INC   DPTR
	CLR   A
	SUBB  A,POS_ACT+1
	MOVX  @DPTR,A
   %IF(%PRESS_COR)THEN(
	CLR   M_CORFL	      ; Pocatek korekce tlaku
	MOV   DPTR,#M_COR
	MOVX  A,@DPTR
	MOV   R1,A
	INC   DPTR
	MOVX  A,@DPTR
	ORL   A,R1
	JZ    S_GRO10
	SETB  M_CORFL
	MOV   DPTR,#M_CORAD
	CLR   A
	MOV   R1,#4
S_GRO05:MOVX  @DPTR,A
	INC   DPTR
	DJNZ  R1,S_GRO05
	MOV   M_POS,A
	MOV   M_POS+1,A
     %IF(1) THEN (
	MOV   DPTR,#M_CORSP
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#0F8H
	MOVX  @DPTR,A
     )FI
	SJMP  S_GRO15
   )FI
S_GRO10:MOV   DPTR,#ROT_BEG
	MOVX  A,@DPTR
	ADD   A,POS_ACT
	MOV   M_POS,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,POS_ACT+1
	MOV   M_POS+1,A

	JB    %G_BF_FL,S_GRO_R; Gradienty prutokem
   %IF(%PRESS_COR)THEN(
	JB    M_CORFL,S_GRO_R
   )FI
S_GRO15:MOV   DPTR,#G_A_E     ; Gradienty ventily
	MOV   R1,#G_A_MSK
	CALL  CMP_POS
	JNC   MOTIMP8         ; A
	MOV   R1,#G_B_MSK
	CALL  CMP_POS
	JNC   MOTIMP8         ; B
	MOV   R1,#G_C_MSK     ; C
MOTIMP8:CALL  WR_GRAD
S_GRO_R:RET
)FI

;********************************************************************
;
;       Casove preruseni

USING   2

%IF (%EQS(%HW_VERSION,PB)) THEN (
USING   3
I_TIME1:CLR   IE1		; Cast s pruchodem 675 Hz
	PUSH  ACC
	PUSH  PSW
	MOV   PSW,#AR0
	PUSH  B
	PUSH  DPL
	PUSH  DPH
	CALL  G_POS
  %IF(%PRESS_COR)THEN(
     %IF(1) THEN (
	MOV   R4,SPD_ACT
       %IF(0)THEN(
	MOV   DPTR,#POS_RQI
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   DPTR,#M_CORTM   ; Pro debugging pohybu pri korekci
	MOVX  A,@DPTR
	CPL   A
	INC   A
	ADD   A,R5
	XCH   A,R5
	MOVX  @DPTR,A
       )ELSE(
	;SETB  PRESS_Q
	MOV   DPTR,#PRESS
	MOVX  A,@DPTR
	MOV   R5,A
       )FI
	MOV   DPTR,#M_CORSP   ; Ukazatel do debugu
	MOVX  A,@DPTR
	MOV   R0,A
	ADD   A,#2
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	XRL   A,#0FFH
	JZ    M_TIM24
	XRL   A,#0FFH
	JNC   M_TIM23
	INC   A
	MOVX  @DPTR,A
	DEC   A
M_TIM23:MOV   DPH,A
	MOV   DPL,R0
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	MOVX  @DPTR,A
M_TIM24:
     )FI
  )FI
	JNB   FL_ENE,M_TIM55	; ????????
	JNB   %START_FL,M_TIM40
	CALL  INC_POS
M_TIM40:
	CALL  REG_%REG_STRUCT_TYPE
%IF (%EQS(%REG_STRUCT_TYPE,PD)) THEN (
	JNB   FL_RERR,M_TIM45	; !!!!!!!!!!!!!!!!!!!
	CJNE  R1,#80H,M_TIM45	; Potlaceni chyby pri spatnem brzdeni
	CLR   FL_RERR
	MOV   DPTR,#POS_RQI
	MOV   A,POS_ACT
	MOVX  @DPTR,A	; POS_RQI
	INC   DPTR
	MOV   A,POS_ACT+1
	MOVX  @DPTR,A	; POS_RQI+1
	INC   DPTR
	MOV   A,POS_ACT+2
	MOVX  @DPTR,A	; POS_RQI+2
M_TIM45:
)FI
	MOV   DPTR,#REG_ENE
	CALL  xSVR45i
	JNB   FL_ENE,M_TIM55
	CALL  S_ENERG
	CALL  S_GRO
	SJMP  M_TIM60
M_TIM55:CALL  S_ENERC
M_TIM60:MOV   DPTR,#G_HC_TIMR	; Konec vysokeho proudu gradientem
	MOVX  A,@DPTR
	JZ    M_TIM65
	DEC   A
	MOVX  @DPTR,A
	CJNE  A,#1,M_TIM64
	ORL   P4,#G_HC_MSK	; Snizit proud gradientem
M_TIM64:JNC   M_TIM65
	ORL   P4,#G_HC_MSK	; Snizit proud gradientem
	JNB   G_CUR_S,M_TIM65
	SETB  FL_LGNR		; Gradientem netece proud
	ANL   LED_FLG,#NOT GRAD_LMSK
	CALL  LEDWR
M_TIM65:
	DJNZ  CINT25,I_TIMR1	; Konec casti spruchodem 675 Hz
	MOV   CINT25,#DINT25	; Pruchod s frekvenci 25 Hz
	CALL  uL_STR
	JNB   FL_AUXV,M_TIM82
	MOV   DPTR,#AUXV_TIM	; Ventily pripojene na AUX
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	JNZ   M_TIM82
	CLR   FL_AUXV
	MOV   DPTR,#AUXUAL
	MOVX  A,@DPTR
	ORL   A,#8
	MOV   R4,A
	CALL  S_AUX
M_TIM82:CALL  G_PRESS		; Cteni tlaku a kontrola mezi
	JB    F0,M_TIM85
	JB    PRESS_Q,M_TIM83	; okamzite mereni tlaku
	JB    FL_GPRE,M_TIM83
	MOV   C,%START_FL
	ANL   C,FL_ENE
	JC    M_TIM84
M_TIM83:CLR   FL_GPRE
	MOV   DPTR,#PRESS
	CALL  xSVR45i
	SETB  U_PRESS
M_TIM84:CALL  C_PRESS
M_TIM85:
)FI
%IF (%EQS(%HW_VERSION,LP)) THEN (
I_TIME1:CLR   IE1
	PUSH  ACC
	PUSH  PSW
	PUSH  B
	CALL  uL_STR
	PUSH  DPL
	PUSH  DPH
)FI
	MOV   DPTR,#BEEPTIM
	MOVX  A,@DPTR
	JZ    I_TIM80
	DEC   A
	MOVX  @DPTR,A
	JNZ   I_TIM80
	CLR   %BEEP_FL
	MOV   DPTR,#LED       ; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
I_TIM80:MOV   DPTR,#TIMR1
	MOV   B,#N_OF_T-1
I_TIME2:MOVX  A,@DPTR
	JZ    I_TIME3
	DEC   A
	MOVX  @DPTR,A
I_TIME3:INC   DPTR
	DJNZ  B,I_TIME2
	MOVX  A,@DPTR
	DEC   A
	MOVX  @DPTR,A
	JNB   ITIM_RF,I_TIMR1
	JB    ACC.7,I_TIME4
I_TIMR1:POP   DPH
	POP   DPL
	POP   B
	POP   PSW
	POP   ACC
	SETB  EA
I_TIMRI:RETI

ITIM_R: CLR   EA
	SETB  ITIM_RF
	SJMP  I_TIMR1

USING   2

I_TIME4:CLR   ITIM_RF
	CLR   ET1
	CALL  I_TIMRI
	MOV   PSW,#AR0        ; Banka 2
	ADD   A,#15
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
	INC   DPTR
	JNZ   I_TIME5
	MOVX  A,@DPTR
	INC   A
	MOVX  @DPTR,A
I_TIME5:SETB  U_TIME
I_TIME6:JNB   %ALRM_FL,I_TIME7
	XRL   LED_FLG,#080H
	CALL  LEDWR
I_TIME7:
%IF (%EQS(%HW_VERSION,LP)) THEN (
	MOV   C,%START_FL
	ANL   C,FL_ENE
	JC    I_TIME8
	SETB  U_PRESS
)FI
%IF (%EQS(%HW_VERSION,LP)) THEN (
	CALL  G_PRESS
	MOV   C,EX0
	ANL   C,%START_FL
	ORL   C,F0
	JC    I_TIME8
	MOV   DPTR,#PRESS
	CALL  xSVR45i
	SETB  U_PRESS
	CALL  C_PRESS
)FI
I_TIME8:CALL  G_AUX
	MOV   C,PRESS_B
	ANL   C,%START_FL
	ANL   C,/%PURGE_FL
	JNC   I_TIME9
	CLR   EA
	MOV   DPTR,#PRESS
	CALL  xLDR45i
	SETB  EA
	MOV   DPTR,#PRES_OL	; hodnota tlaku v minule otacce
	CALL  xLDR23i
	CALL  CMPi
	JNC   I_TIME9
	SETB  PRESS_V
	JNB   PRESS_S,I_TIME9
	CALL  M_STOP
I_TIME9:CLR   EA
	MOV   DPTR,#PRESS
	CALL  xLDR45i
	SETB  EA
	MOV   DPTR,#PRES_OL
	CALL  xSVR45i
	JMP   I_TIME

; *******************************************************************
;
; Pipnuti na klavese klavesnice

PUBLIC	KBDBEEP

;KBDBEEP:JMP    KBDSTDB
KBDBEEP:MOV   DPTR,#BEEPTIM
	MOV   A,#2
	MOVX  @DPTR,A
	SETB  %BEEP_FL
	MOV   DPTR,#LED       ; Pipnuti
	MOV   A,LED_FLG
	MOVX  @DPTR,A
	MOV   A,R2
	RET

; *******************************************************************
;

T_RAM_E:DB    C_CLR, 'RAM unreachable'
	DB    C_LIN2,' Trying again ',0
T_SRAME:DB    C_CLR,0, 'SRAM data error'
	DB    C_LIN2,'0-Ignore 1-Clear',0

END
