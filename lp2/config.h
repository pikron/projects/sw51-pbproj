;********************************************************************
;*                     CONFIG.ASM                                   *
;*     Konfiguracni soubor pro kompilaci LCP4000                    *
;*                  Stav ke dni 08.03.1996                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

%IF(1)THEN(
%DEFINE (VERSION) (LCP5020 v1.7)
%DEFINE (C_MAX_FLOW) (20)
%DEFINE (C_FLOW_FDIG)(3)
%DEFINE (C_FLOW_FMUL)(1000)
%DEFINE (C_MAX_PRESS)(40)
PURGE_K     EQU   8000      ; prutok pri cisteni
; Cv=183883.6 IRC/ml
; Cf=Cv*2^16/(1000*60*675)=297.55545=38087*2^-7
FLOW_K   EQU 38087	    ; prepoctova konstanta prutoku
FLOW_K_E EQU 7		    ; dvojkovy rad FLOW_K
)ELSE(
%DEFINE (VERSION) (LCP5080 v1.7)
%DEFINE (C_MAX_FLOW) (100)
%DEFINE (C_FLOW_FDIG)(2)
%DEFINE (C_FLOW_FMUL)(100)
%DEFINE (C_MAX_PRESS)(20)
PURGE_K     EQU   80*%C_FLOW_FMUL ; prutok pri cisteni
; Cv=45970.9 IRC/ml
; Cf=Cv*2^16/(100*60*675)=743.889=47609*2^-6
FLOW_K   EQU 47609	    ; prepoctova konstanta prutoku
FLOW_K_E EQU 6		    ; dvojkovy rad FLOW_K
)FI

%DEFINE (HW_VERSION) (PB)
%DEFINE (USE_WR_MASK) (1)
%DEFINE (WITH_UL_G_BF) (1)  ;  s podporou pro rizeni slave pumpy
%DEFINE (REG_STRUCT_TYPE) (PD)
%DEFINE (PBLIBDIR) (..\PBLIB\)

%IF (%EQS(%HW_VERSION,LP)) THEN (
; Stara verze cerpadla
%DEFINE (IN_TTY)(0)
%DEFINE (INCH_TTY) (LP_TTY.H)
%DEFINE (INCH_LCD) (%PBLIBDIR%()PB_LCD.H)
%DEFINE (INCH_AI)  (%PBLIBDIR%()PB_AI.H)
%DEFINE (INCH_AL)  (%PBLIBDIR%()PB_AL.H)
%DEFINE (INCH_ADR) (LP_ADR.H)
%DEFINE (INCH_UF)  (LP_UF.H)
%DEFINE (INCH_ULAN)(%PBLIBDIR%()ULAN.H)
%DEFINE (INCH_UL_OI)(%PBLIBDIR%()UL_OI.H)
%DEFINE (INCH_MMAC)(%PBLIBDIR%()PB_MMAC.H)
%DEFINE (PROCESSOR_TYPE) (31)
%DEFINE (VECTOR_TYPE) (STATIC)
%DEFINE (REG_OUT_TYPE) (OLD)
%DEFINE (PRESS_COR) (0)
%DEFINE (SMOOTH_ACC)(0)
%DEFINE (WATCHDOG) (
	MOV   DPTR,#WATCH_D
	MOVX  @DPTR,A
)
)FI

%IF (%EQS(%HW_VERSION,PB)) THEN (
; Nova verze
%DEFINE (INCH_TTY) (%PBLIBDIR%()PB_TTY.H)
%DEFINE (INCH_LCD) (%PBLIBDIR%()PB_LCD.H)
%DEFINE (INCH_AI)  (%PBLIBDIR%()PB_AI.H)
%DEFINE (INCH_AL)  (%PBLIBDIR%()PB_AL.H)
%DEFINE (INCH_ADR) (%PBLIBDIR%()PB_ADR.H)
%DEFINE (INCH_UF)  (LP_UF.H)
%DEFINE (INCH_ULAN)(%PBLIBDIR%()ULAN.H)
%DEFINE (INCH_UL_OI)(%PBLIBDIR%()UL_OI.H)
%DEFINE (INCH_MMAC)(%PBLIBDIR%()PB_MMAC.H)

%DEFINE (PROCESSOR_TYPE) (552)
%DEFINE (VECTOR_TYPE) (DYNAMIC)
%DEFINE (REG_OUT_TYPE) (PWM0_UNIDIR)
%DEFINE (PRESS_COR) (1)
%DEFINE (SMOOTH_ACC)(1)
%DEFINE (WATCHDOG) (
	ORL   PCON,#10H
	MOV   T3,#080H
)
)FI

; Processor depended features

%IF (%EQS(%PROCESSOR_TYPE,552)) THEN (
%DEFINE (INCH_REGS) (%PBLIBDIR%()REG552.H)
)FI

%IF (%EQS(%PROCESSOR_TYPE,31)) THEN (
%DEFINE (INCH_REGS) (%PBLIBDIR%()NULL.H)
)FI

; Zpusob definice vektoru preruseni a sluzeb

%IF (%EQS(%VECTOR_TYPE,STATIC)) THEN (
%*DEFINE (VECTOR  (VECNUM,VECADR)) ()
%*DEFINE (SVECTOR (VECNUM,VECADR)) (
CSEG    AT    %VECNUM
	JMP   %VECADR
)
)FI

%IF (%EQS(%VECTOR_TYPE,DYNAMIC)) THEN (
	EXTRN CODE(VEC_SET)
%*DEFINE (SVECTOR (VECNUM,VECADR)) ()
%*DEFINE (VECTOR  (VECNUM,VECADR)) (
	MOV   R4,#%VECNUM
	MOV   DPTR,#%VECADR
	CALL  VEC_SET
)
)FI

