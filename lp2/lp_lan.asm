;********************************************************************
;*                    LCP 4000 - LP_LAN.ASM                         *
;*     Vykonavani rizeni ze site uLAN a vysilani dat                *
;*                  Stav ke dni 31.01.2003                          *
;*                      (C) Pisoft 1991                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

PUBLIC uL_IDLE,uL_IDLK,uL_OINI,LAN_MRK

$NOLIST
$INCLUDE(CONFIG.H)
$INCLUDE(%INCH_MMAC)
$INCLUDE(%INCH_TTY)
$INCLUDE(%INCH_AI)
;INCLUDE(%INCH_ADR)
;INCLUDE(%INCH_UF)
$INCLUDE(%INCH_ULAN)
$INCLUDE(%INCH_UL_OI)
$LIST

EXTRN	XDATA(PROG_NU,PROG_SF)
EXTRN	CODE(xMDPDP,FND_PGE)

%DEFINE (WITH_UL_DY) (1)

%IF(%WITH_UL_DY) THEN (
EXTRN	CODE(UD_INIT,UD_RQ)
)FI

%IF(%PRESS_COR)THEN(
EXTRN	XDATA(M_COR)
)FI


EXTRN   IDATA(STACK_BOTTOM)
EXTRN   BIT(FL_BRKUI)

LAN___B SEGMENT DATA BITADDRESSABLE

LAN___C SEGMENT CODE

LAN___X SEGMENT XDATA

RSEG LAN___X

RSEG LAN___C

; Identifikace typu pristroje
PUBLIC	uL_IDB,uL_IDE
uL_IDB: DB    '.mt %VERSION .uP 51x'
    %IF(%WITH_UL_DY) THEN (
	DB    ' .dy'
    )FI
	DB    0
uL_IDE:

uL_IDLE:

uL_IDLK:
    %IF(%WITH_UL_DY) THEN (
	CALL  UD_RQ		; dynamicaka adresace
    )FI
	CLR   C
    %IF(%WITH_UL_G_BF) THEN (
        JNB   %G_BF_FL,uL_IDLL
	CALL  GBF_CTRL
	SJMP  uL_IDLM
uL_IDLL:JNB   GBF_SLAVE,uL_IDLM
	CALL  GBF_SLCK
uL_IDLM:	
    )FI
	JNB   uLF_INE,uL_IDLR
    %IF(NOT %WITH_UL_G_BF) THEN (
	CALL  UI_PR		; objektova komunikace
    )ELSE(
	CALL  GBF_PR		; objektova a m/s komunikace
    )FI
	SETB  C
uL_IDLR:CLR   A
	JBC   FL_BRKUI,uL_IDLBRKUI
	RET

uL_IDLBRKUI:
	JMP   ULP_BRKUI


;=================================================================
; Promenne cerpadla

EXTRN	NUMBER(PROGR_L)
EXTRN	XDATA(ERR_COD,PROGRAM)
EXTRN   XDATA(FLOW,GRAD_B,GRAD_C,PRESS,PRESS_L,PRESS_H,AUXUAL)
EXTRN   CODE(S_FLOW,S_FLOW1,S_AUX,S_AUXVALV,G_AUXVALV)
EXTRN	CODE(S_GRAD,S_GRDIR,S_CFGFL)
EXTRN   CODE(M_START,M_STOP,M_PURGE)
EXTRN	DATA(WR_UPDA)
EXTRN	CODE(ULP_ACTN,ULP_RUN,ULP_END,ULP_CLR,ULP_BRKUI)
EXTRN	CODE(CAL_DPc)
EXTRN	BIT(GBF_CHRQ,GBF_ERR,GBF_ERRCL,GBF_SLAVE)
EXTRN   XDATA(TIMR_GBF,TIMR_GBFSL)
EXTRN	CODE(G_GBFMODE,S_GBFMODE)
EXTRN	XDATA(REG_P,REG_I,REG_D)
EXTRN	CODE(xLDl)


;=================================================================
; Master slave komunikace

%IF(%WITH_UL_G_BF) THEN (

RSEG LAN___X
;XSEG AT 8400H 

PUBLIC	GBF_FLOWB,GBF_MODE,GBF_SLA,GBF_SLGR,GBF_SLAUX

GBF_PROT_TYPE EQU 50H+%C_FLOW_FDIG
GBF_PROT_VER  EQU 1

OGBF_LEN SET   0
%STRUCTM(OGBF,PROT,1)   ; Typ protokolu
%STRUCTM(OGBF,VER,1)    ; Verze protokolu
%STRUCTM(OGBF,ST,2)     ; Requested/reported state
%STRUCTM(OGBF,FLOW,2)   ; Prutok
%STRUCTM(OGBF,PRES,2)   ; Tlak mez/tlak
%STRUCTM(OGBF,GRV,1)    ; Gradient valves
%STRUCTM(OGBF,AUX,1)    ; Aux output

GBF_MODE:DS   1		; Pro ulozeni bitu G_BF_FL do SRAM
GBF_NCC:DS    1		; Pocet nepotvrzenych zprav
GBF_SLA:DS    2		; Adresa slave pumpy
GBF_MSA:DS    1		; Adresa nadrizene pumpy
GBF_SLC:DS    OGBF_LEN	; Slave command buffer
GBF_SLS:DS    OGBF_LEN	; Slave state buffer
GBF_FLOWB:DS  2		; Prutok pro slave
GBF_SLGR:DS   1		; Request for slave gradients
GBF_SLAUX:DS  1		; Request for slave aux valves

	DS    16	; Rezerva pro pridavani
)FI

RSEG LAN___C

%IF(%WITH_UL_G_BF) THEN (
GBF_PR: CLR   F0
        CLR   uLF_INE
        CALL  uL_I_OP   ; vraci R4 Adr a R5 Com
        JNB   F0,GBF_P10
GBF_P00:CLR   A
        RET
GBF_P90v1:JMP GBF_P90
GBF_P30v1:JMP GBF_P30
GBF_P20v1:JMP GBF_P20

GBF_P10:SETB  uLF_INE
        CJNE  R5,#03DH,GBF_P30v1
	; Prot Ver StateL StateH FlowL FlowH PresL PresH
	MOV   DPTR,#GBF_MSA
	MOV   A,R4
	MOVX  @DPTR,A
	INC   DPTR		; GBF_SLC
	%LDR45i (OGBF_LEN)
	CALL  UL_RD
	JB    F0,GBF_P90v1
	CALL  UL_I_CL
				; Kontrola verze protokolu
	MOV   DPTR,#GBF_SLC+OGBF_PROT
	MOVX  A,@DPTR
	CJNE  A,#GBF_PROT_TYPE,GBF_P20v1
	INC   DPTR
	MOVX  A,@DPTR
	XRL   A,#GBF_PROT_VER
	ANL   A,#0F0H
	JNZ   GBF_P20v1

	MOV   DPTR,#TIMR_GBFSL	; Timer pro kontrolu zivota mastera
	MOV   A,#25*5
	MOVX  @DPTR,A
	JB    GBF_SLAVE,GBF_P11
	SETB  GBF_SLAVE
	SETB  FL_BRKUI
	CLR   %G_BF_FL
GBF_P11:
				; Zpracovani prikazu pro slave
	MOV   DPTR,#GBF_SLC+OGBF_FLOW
	CALL  xLDR45i
	CALL  S_FLOW

	MOV   DPTR,#GBF_SLC+OGBF_PRES
	CALL  xLDR45i
	MOV   DPTR,#PRESS_H
	CALL  S_PRES_V
	CLR   A
	MOV   R4,A
	MOV   R5,A
	MOV   DPTR,#PRESS_L
	CALL  S_PRES_V

	CALL  GBF_GRST		; Stav gradientovych ventilu
	MOV   DPTR,#GBF_SLS+OGBF_GRV
	MOVX  @DPTR,A
	MOV   DPTR,#AUXUAL	; Stav AUX
	MOVX  A,@DPTR
	MOV   DPTR,#GBF_SLS+OGBF_AUX
	MOVX  @DPTR,A

	MOV   DPTR,#GBF_SLC+OGBF_GRV
	MOVX  A,@DPTR
	JZ    GBF_P12
	MOV   DPTR,#GBF_SLS+OGBF_GRV
	MOVX  @DPTR,A
	MOV   R4,A
	MOV   R5,#0
	CALL  S_GRDIR
GBF_P12:MOV   DPTR,#GBF_SLC+OGBF_AUX
	MOVX  A,@DPTR
	JZ    GBF_P14
	MOV   DPTR,#GBF_SLS+OGBF_AUX
	MOVX  @DPTR,A
	MOV   R4,A
	MOV   R5,#0
	JBC   ACC.7,GBF_P13
	CALL  S_AUX
	SJMP  GBF_P14
GBF_P13:CALL  S_AUXVALV
GBF_P14:
				; Nastaveni stavu
	MOV   DPTR,#GBF_SLC+OGBF_ST
	CALL  xLDR45i
	MOV   A,R5
	JB    ACC.7,GBF_P18	; Error
	JB    ACC.6,GBF_P17	; Error CLEAR
	JB    ACC.0,GBF_P15	; Program 
	MOV   A,R4
	JZ    GBF_P18
GBF_P15:MOV   DPTR,#ERR_COD+1
	MOVX  A,@DPTR
	JB    ACC.7,GBF_P18
	JB    %PURGE_FL,GBF_P16
	JB    %START_FL,GBF_P20
GBF_P16:CALL  M_START
	SJMP  GBF_P20
GBF_P17:CALL  CLRERR_U		; Motor stop
GBF_P18:JNB   %START_FL,GBF_P20
	CALL  M_STOP

	; ---------------

GBF_P20:MOV   DPTR,#GBF_SLS+OGBF_PROT
	MOV   A,#GBF_PROT_TYPE
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#GBF_PROT_VER OR 80H
	MOVX  @DPTR,A
	
	CALL  G_STATUS		; Aktualni stav
	MOV   DPTR,#GBF_SLS+OGBF_ST
	CALL  xSVR45i
	MOV   DPTR,#FLOW	; Aktualni prutok
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	SETB  EA
	MOV   R5,A
	MOV   DPTR,#GBF_SLS+OGBF_FLOW
	CALL  xSVR45i
	MOV   DPTR,#PRESS	; Aktualni tlaku
	CALL  G_PRES_V
	MOV   DPTR,#GBF_SLS+OGBF_PRES
	CALL  xSVR45i
	; Stav gradientovych ventilu a aux presunut na zacatek
	
	MOV   DPTR,#GBF_MSA	; Odeslani stavu slave
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#03EH
	CLR   F0
	CALL  UL_O_OP
	MOV   DPTR,#GBF_SLS
	%LDR45i (OGBF_LEN)
	CALL  UL_WR
	CALL  UL_O_CL
GBF_P29:MOV   A,#1
	RET

GBF_GRST:
	MOV   R4,#0		; Stav gradientovych ventilu
	MOV   DPTR,#GRAD_B
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	ORL   A,R0
	JZ    GBF_GRST1
	INC   R4
GBF_GRST1:
	MOV   DPTR,#GRAD_C
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	ORL   A,R0
	JZ    GBF_GRST2
	INC   R4
	INC   R4
GBF_GRST2:
	RET
)FI
%IF(%WITH_UL_G_BF) THEN (
GBF_P30:CJNE  R5,#03EH,GBF_P40
	JNB   %G_BF_FL,GBF_P90
	MOV   DPTR,#GBF_SLA
	MOVX  A,@DPTR
	XRL   A,R4
	JNZ   GBF_P90
	MOV   DPTR,#GBF_SLS
	%LDR45i (OGBF_LEN)
	CALL  UL_RD
	CALL  UL_I_CL
	MOV   DPTR,#GBF_SLS+OGBF_PROT
	MOVX  A,@DPTR
	CJNE  A,#GBF_PROT_TYPE,GBF_P80
	INC   DPTR
	MOVX  A,@DPTR
	XRL   A,#GBF_PROT_VER OR 80H
	ANL   A,#0F0H
	JNZ   GBF_P80
	MOV   DPTR,#GBF_NCC
	CLR   A
	MOVX  @DPTR,A

	MOV   DPTR,#GBF_SLS+OGBF_GRV
	MOVX  A,@DPTR		; Kontrola grad ventilu ve slave
	MOV   R0,A
	MOV   DPTR,#GBF_SLGR
	MOVX  A,@DPTR
	XRL   A,R0
	JNZ   GBF_P35
	MOVX  @DPTR,A
GBF_P35:MOV   DPTR,#GBF_SLS+OGBF_AUX
	MOVX  A,@DPTR		; Kontrola aux ventilu ve slave
	MOV   R0,A
	MOV   DPTR,#GBF_SLAUX
	MOVX  A,@DPTR
	XRL   A,R0
	JNZ   GBF_P36
	MOVX  @DPTR,A
GBF_P36:
	MOV   DPTR,#GBF_SLS+OGBF_ST
	CALL  xLDR45i
	MOV   A,R5
	JB    ACC.7,GBF_P80

	JMP   GBF_P90

GBF_P40:JMP   UI_PRAO

GBF_P80:SETB  GBF_ERR		; Zastavit master pri chybe
	CALL  M_STOP
GBF_P90:CLR   F0
	CALL  UL_I_CL
        MOV   A,#1
	RET

)FI

%IF(%WITH_UL_G_BF) THEN (
GBF_CTRL:MOV  DPTR,#TIMR_GBF
	MOVX  A,@DPTR
	JZ    GBF_C21
GBF_C20:JB    GBF_CHRQ,GBF_C21
	JMP   GBF_C99
GBF_C21:MOV   A,#25*5
	JNB   %START_FL,GBF_C23
	MOV   DPTR,#GBF_NCC	; Kontrola pritomnosti slave
	MOVX  A,@DPTR
	ADD   A,#-4
	JNC   GBF_C22
	SETB  GBF_ERR		; Zastavit master odpojeni slave
	CALL  M_STOP
GBF_C22:MOV   A,#25*2
GBF_C23:MOV   DPTR,#TIMR_GBF
	MOVX  @DPTR,A

	CLR   GBF_CHRQ

	MOV   DPTR,#GBF_SLC+OGBF_PROT
	MOV   A,#GBF_PROT_TYPE
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#GBF_PROT_VER
	MOVX  @DPTR,A

	;CALL  G_STATUS		; Pozadovany stav
	;MOV   DPTR,#GBF_SLC+OGBF_ST
	;CALL  xSVR45i
	MOV   DPTR,#GBF_SLC+OGBF_ST
	CLR   A
	MOV   C,%START_FL
	MOV   ACC.7,C
	MOVX  @DPTR,A
	INC   DPTR
	CLR   A
	MOV   C,GBF_ERRCL	; Nulovani chyby slave
	MOV   ACC.6,C
	CLR   GBF_ERRCL
	MOVX  @DPTR,A
	
	MOV   DPTR,#GBF_FLOWB	; Prutok pro slave
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	SETB  EA
	MOV   R5,A
	MOV   DPTR,#GBF_SLC+OGBF_FLOW
	CALL  xSVR45i
	MOV   DPTR,#PRESS_H	; Horni tlakova mez
	CALL  G_PRES_V
	MOV   A,R4
	ADD   A,#20
	MOV   R4,A
	MOV   A,R5
	ADDC  A,#0
	MOV   R5,A
	MOV   DPTR,#GBF_SLC+OGBF_PRES
	CALL  xSVR45i
	MOV   DPTR,#GBF_SLGR	; Rizeni grad ventilu ve slave
	MOVX  A,@DPTR
	MOV   DPTR,#GBF_SLC+OGBF_GRV
	MOVX  @DPTR,A
	MOV   DPTR,#GBF_SLAUX	; Rizeni aux ventilu ve slave
	MOVX  A,@DPTR
	MOV   DPTR,#GBF_SLC+OGBF_AUX
	MOVX  @DPTR,A
	
	MOV   DPTR,#GBF_SLA
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#03DH
	CLR   F0
	CALL  UL_O_OP
	MOV   DPTR,#GBF_SLC
	%LDR45i (OGBF_LEN)
	CALL  UL_WR
	CALL  UL_O_CL
	MOV   DPTR,#GBF_NCC
	MOVX  A,@DPTR
	INC   A
	JZ    GBF_C98
	MOVX  @DPTR,A
GBF_C98:SETB  C
GBF_C99:RET
)FI

%IF(%WITH_UL_G_BF) THEN (
GBF_SLCK:
	JNB   %START_FL,GBF_SLCK9
	MOV   DPTR,#TIMR_GBFSL	; Timer pro kontrolu zivota mastera
	MOVX  A,@DPTR
	JNZ   GBF_SLCK9
	%LDMXi(ERR_COD,0FFF7H)	; Master error
	CALL  M_STOP
	SETB  C
GBF_SLCK9:
	RET
)FI
;=================================================================
; Prikazy objektove komunikace

uL_OINI:
    %IF(%WITH_UL_DY) THEN (
	%LDR45i(ERR_COD)
	MOV   R6,#2
	CALL  UD_INIT
    )FI
	%LDR45i (OID_1IN)
	%LDR67i (OID_1OUT)
	JMP    US_INIT

G_INTR: MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	INC   DPTR
	PUSH  DPL
	PUSH  DPH
	MOV   DPH,A
	MOV   DPL,R0
	MOV   C,EA
	CLR   EA
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	MOV   EA,C
	POP   DPH
	POP   DPL
	RET

G_STATUS:MOV  DPTR,#ERR_COD
	CALL  xLDR45i
	JB    ACC.7,G_STAT9
	MOV   R5,#1
	MOV   R4,#0
	JB    %RUN_FL,G_STAT9
	MOV   R5,#0
	JNB   %START_FL,G_STAT2
	MOV   R4,#1
	JNB   %PURGE_FL,G_STAT9
	MOV   R4,#2
	SJMP  G_STAT9
G_STAT2:
G_STST8:CLR   A
	MOV   R4,A
	MOV   R5,A
G_STAT9:RET

G_PRES_U:MOVX A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	INC   DPTR
	MOV   DPH,A
	MOV   DPL,R0
G_PRES_V:MOV  C,EA
	CLR   EA
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   EA,C
	CLR   C
	RRC   A
	XCH   A,R5
	RRC   A
	MOV   R4,A
	RET

S_PRES_U:MOVX A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	INC   DPTR
	MOV   DPH,A
	MOV   DPL,R0
S_PRES_V:CLR  C
	MOV   A,R4
	RLC   A
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R5
	RLC   A
	MOVX  @DPTR,A
	SETB  WR_UPDA.0
	RET

CLRERR_U:CLR  A
	MOV   DPTR,#ERR_COD
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  @DPTR,A
	RET

; Zmena konfigurace pumpy
S_PCFG_U:JMP  S_CFGFL
; Bit
; 0	PRESS_Q		Trvaly vypis tlaku
; 1	FL_PURL		Neprekrocit 1 MPa pri purge
; 2	FL_GRLT		Pomale gradienty
; 3	FL_AUXV		8 ventilu na AUX
; 4	FL_P25		Rozsah tlaku 25MPa
; 5	FL_FLM2		Nasobeni prutoku 2


G_PROGN_U:
	MOV   DPTR,#PROG_NU
	MOVX  A,@DPTR
	MOV   R4,A
	MOV   R5,#0
	RET

PROGCH_U:
	MOV   DPTR,#PROG_SF
	MOV   A,#0ABH
	MOVX  @DPTR,A
	CALL  FND_PGE
	CLR   F0
	RET


; Inicializace servisni urovne
uL_SLINI:
	MOV   A,R4
	CJNE  A,#(uL_SLINIte-uL_SLINIt)/4,uL_SLINI3
uL_SLINI3:JC  uL_SLINI5
	CLR   A
uL_SLINI5:
	RL    A
	RL    A
	ADD   A,#LOW uL_SLINIt
	MOV   DPL,A
	CLR   A
	ADDC  A,#HIGH uL_SLINIt
	MOV   DPH,A
	CALL  xLDl
	JMP   US_INIO
uL_SLINIt:
	%W (OID_1IN)		; Uroven 0 prijimane prikazu
	%W (OID_1OUT)		;          vysilane prikazu
	%W (OID_1INSL1)		; Uroven 1 prijimane prikazu
	%W (OID_1OUTSL1)	;          vysilane prikazu
uL_SLINIte:

; Pristup do poli

RSEG LAN___X

UA_CNT:	DS    2	; delka, bit 15=0 =>neni delku treba vysilat
UA_INDX:DS    2	; pocatecni index v poli
UA_FLG:	DS    2 ; priznaky pro zpracovani
UA_T1:	DS    2
UA_T2:	DS    2

RSEG LAN___C

; Priprava na cteni nebo zapis pole
; ret:	R23	index
;	R45	delka, pozor R45.15
;	R67	priznaky
;		  R67.9 => kontrola rozsahu
;		  R67.9 and R67.8 => delka neprimo
;		  R67.10 and R67.10 => delka muze byt zkracena

UIO_ARR:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#UA_INDX
	%LDR45i(2)
	%VCALL(UV_RD)
	MOV   DPTR,#UA_INDX
	CALL  xLDR23i		; R23=UA_INDX nebo UA_CNT
	MOV   DPTR,#UA_CNT
	JNB   ACC.7,UIO_AR2
	CALL  xSVR23i		; UA_CNT=R23
	MOV   DPTR,#UA_INDX
	%LDR45i(2)
	%VCALL(UV_RD)
	MOV   DPTR,#UA_CNT
	CALL  xLDR45i		; R45=UA_INDX
	MOV   DPTR,#UA_INDX
	CALL  xLDR23i		; R23=UA_INDX
	SJMP  UIO_AR3
UIO_AR2:%LDR45i(1)
	CALL  xSVR45i		; UA_CNT=1
UIO_AR3:POP   DPH
	POP   DPL
	MOVX  A,@DPTR
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
	INC   DPTR
	PUSH  DPL
	PUSH  DPH
	JNB   ACC.1,UIO_AR9
	JNB   ACC.0,UIO_AR5
	CALL  xMDPDP		; delka neprimo
UIO_AR5:CLR   C
	MOVX  A,@DPTR		; kontrola delky
	SUBB  A,R2
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	SUBB  A,R3
	MOV   R1,A
	JC    UIO_AR6		; UA_INDX > max indx
	MOV   A,R5
	XRL   A,#0FFH
	JZ    UIO_AR9		; specialni vyznam
	SETB  C
	MOV   A,R4
	SUBB  A,R0
	MOV   A,R5
	ANL   A,#07FH
	SUBB  A,R1
	JC    UIO_AR9
	MOV   A,R0
	MOV   R4,A
	MOV   A,R1
	MOV   R5,A
	SJMP  UIO_AR8
UIO_AR6:MOV   A,R2		; max povolena hodnota indexu
	ADD   A,R0
	MOV   R2,A
	MOV   A,R3
	ADDC  A,R1
	MOV   R3,A
	MOV   DPTR,#UA_INDX
	CALL  xSVR23i
UIO_AR7:%LDR23i(8000H)		; nulova delka
UIO_AR8:MOV   DPTR,#UA_CNT
	CALL  xSVR45i		; UA_CNT=R45
	MOV   A,R7
	JB    ACC.2,UIO_AR9
	SETB  F0
UIO_AR9:MOV   DPTR,#UA_FLG
	MOV   A,R6
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A
	POP   DPH
	POP   DPL
	RET

; Pomocna funkce

UIO_ARRT:PUSH DPL
	PUSH  DPH
	MOV   DPTR,#UA_CNT
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR	; UA_INDX
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	INC   DPTR
	MOVX  A,@DPTR	; UA_FLG
	MOV   R6,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R7,A
	POP   DPH
	POP   DPL
UIO_ARRS:INC  DPTR		; Preskocit delku
	INC   DPTR
	MOV   A,R4
	MOV   B,R6
	MUL   AB
	MOV   R4,A
	MOV   A,R5
	ANL   A,#07FH
	MOV   R5,B
	MOV   B,R6
	MUL   AB
	ADD   A,R5
	MOV   R5,A		; R45=R45*R6
	MOV   A,R2
	MOV   B,R6
	MUL   AB
	MOV   R2,A
	MOV   A,R3
	MOV   R3,B
	MOV   B,R6
	MUL   AB
	ADD   A,R3
	MOV   R3,A		; R23=R23*R6
	MOVX  A,@DPTR
	ADD   A,R2
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	ADDC  A,R3
	MOV   R3,A
	INC   DPTR		; Pricist pocatek
UI_ARR9:RET

; Prijem pole

UI_ARR:	CALL  UIO_ARR
	JB    F0,UI_ARR9
	CALL  UIO_ARRS
	PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#UA_T1
	CALL  xSVR23i
	MOV   DPTR,#UA_T2
	CALL  xSVR45i
	MOV   DPL,R2
	MOV   DPH,R3
	%VCALL(UV_RD)
	POP   DPH
	POP   DPL
	JMP   CAL_DPc

; Prijem pole

UO_ARR:	CALL  UIO_ARR
	JB    F0,UO_ARR9
	PUSH  DPL
	PUSH  DPH
	INC   DPTR
	INC   DPTR
	INC   DPTR
	INC   DPTR
	CALL  CAL_DPc
	JB    F0,UO_ARR8
	MOV   DPTR,#UA_CNT+1
	MOVX  A,@DPTR
	MOV   DPTR,#UA_CNT
	MOV   R4,#4
	MOV   R5,#0
	JB    ACC.7,UO_ARR4
	MOV   DPTR,#UA_INDX
	MOV   R4,#2
UO_ARR4:%VCALL(UV_WR)
	POP   DPH
	POP   DPL
	CALL  UIO_ARRT
	MOV   DPTR,#UA_T1
	CALL  xSVR23i
	MOV   DPTR,#UA_T2
	CALL  xSVR45i
	MOV   DPL,R2
	MOV   DPH,R3
	%VJMP(UV_WR)
UO_ARR8:POP   DPH
	POP   DPL
UO_ARR9:RET

; Kody prikazu

%OID_ADES(AI_STATUS,STATUS,u2)
%OID_ADES(AI_ERRCLR,ERRCLR,e)
I_FLOW	  EQU   220
%OID_ADES(AI_FLOW,FLOW,u2/.%C_FLOW_FDIG)
I_PCFG	  EQU   224
%OID_ADES(AI_PCFG,PCFG,u2)
I_PRESS	  EQU   225
%OID_ADES(AI_PRESS,PRESS,u2/.1)
I_PRESS_H EQU   226
%OID_ADES(AI_PRESS_H,PRESS_H,u2/.1)
I_PRESS_L EQU   227
%OID_ADES(AI_PRESS_L,PRESS_L,u2/.1)
%IF(%PRESS_COR)THEN(
I_PRESCOR EQU   228
%OID_ADES(AI_PRESCOR,PRESCOR,u2/.1)
)FI
I_GRAD_B  EQU   231
%OID_ADES(AI_GRAD_B,GRAD_B,u2/*256)
I_GRAD_C  EQU   232
%OID_ADES(AI_GRAD_C,GRAD_C,u2/*256)
I_GRADDIR EQU   239
%OID_ADES(AI_GRADDIR,GRADDIR,u2)
I_AUXUAL  EQU	240
%OID_ADES(AI_AUXUAL,AUXUAL,u2)
I_AUXVALV EQU	241
%OID_ADES(AI_AUXVALV,AUXVALV,u2)
I_STOP	  EQU   250
%OID_ADES(AI_STOP,STOP,e)
I_START	  EQU   251
%OID_ADES(AI_START,START,e)
I_PURGE	  EQU   252
%OID_ADES(AI_PURGE,PURGE,e)

I_SERVLEV EQU   458
%OID_ADES(AI_SERVLEV,SERVLEV,u2)
I_PBREG_P EQU   461
%OID_ADES(AI_PBREG_P,PBREG_P,u2)
I_PBREG_I EQU   462
%OID_ADES(AI_PBREG_I,PBREG_I,u2)
I_PBREG_D EQU   463
%OID_ADES(AI_PBREG_D,PBREG_D,u2)

I_PRGACTN EQU   520
%OID_ADES(AI_PRGACTN,PRGACTN,u2)
I_PRGRUN  EQU   521
%OID_ADES(AI_PRGRUN,PRGRUN,e)
I_PRGEND  EQU   522
%OID_ADES(AI_PRGEND,PRGEND,e)
I_PRGCLR  EQU   523
%OID_ADES(AI_PRGCLR,PRGCLR,e)
I_PRGBYTES EQU  524
%IF(%C_FLOW_FDIG EQ 3)THEN(
%OID_ADES(AI_PRGBYTES,PRGBYTES,prgbyteslcp1)
)ELSE(
%OID_ADES(AI_PRGBYTES,PRGBYTES,prgbyteslcp1a%C_FLOW_FDIG)
)FI

%IF(%WITH_UL_G_BF) THEN (
I_GBFSLA  EQU  525
%OID_ADES(AI_GBFSLA,GBFSLA,u2)
I_GBFMODE EQU  526
%OID_ADES(AI_GBFMODE,GBFMODE,u2)
)FI

; Prijimane prikazy

OID_T	SET   $
	%W    (I_FLOW)
	%W    (OID_ISTD)
	%W    (AI_FLOW)
	%W    (UI_INT)
	%W    (0)
	%W    (S_FLOW)

%OID_NEW(I_SERVLEV,AI_SERVLEV)
	%W    (UI_INT)
	%W    (0)
	%W    (uL_SLINI)

%IF(%WITH_UL_G_BF) THEN (
%OID_NEW(I_GBFSLA,AI_GBFSLA)
	%W    (UI_INT)
	%W    (GBF_SLA)
	%W    (0)

%OID_NEW(I_GBFMODE,AI_GBFMODE)
	%W    (UI_INT)
	%W    (0)
	%W    (S_GBFMODE)
)FI

%OID_NEW(I_PRGBYTES,AI_PRGBYTES)
	%W    (UI_ARR)		; priprava prijmu pole
	%W    (0201h)		; priznaky
	%W    (PROGR_L)		; pocet polozek
	%W    (PROGRAM)		; pocatek
	%W    (PROGCH_U)	; funkce

%OID_NEW(I_PRGCLR,AI_PRGCLR)
	%W    (ULP_CLR)

%OID_NEW(I_PRGACTN,AI_PRGACTN)
	%W    (UI_INT)
	%W    (0)
	%W    (ULP_ACTN)

%OID_NEW(I_PRGRUN,AI_PRGRUN)
	%W    (ULP_RUN)

%OID_NEW(I_PRGEND,AI_PRGEND)
	%W    (ULP_END)

%OID_NEW(I_PCFG,AI_PCFG)
	%W    (UI_INT)
	%W    (0)
	%W    (S_PCFG_U)

%OID_NEW(I_PRESS_H,AI_PRESS_H)
	%W    (UI_INT)
	%W    (0)
	%W    (S_PRES_U)
	%W    (PRESS_H)

%OID_NEW(I_PRESS_L,AI_PRESS_L)
	%W    (UI_INT)
	%W    (0)
	%W    (S_PRES_U)
	%W    (PRESS_L)

%IF(%PRESS_COR)THEN(
%OID_NEW(I_PRESCOR,AI_PRESCOR)
	%W    (UI_INT)
	%W    (M_COR)
	%W    (0)
)FI

%OID_NEW(I_GRAD_B,AI_GRAD_B)
	%W    (UI_INT)
	%W    (GRAD_B)
	%W    (S_GRAD)

%OID_NEW(I_GRAD_C,AI_GRAD_C)
	%W    (UI_INT)
	%W    (GRAD_C)
	%W    (S_GRAD)

%OID_NEW(I_GRADDIR,AI_GRADDIR)
	%W    (UI_INT)
	%W    (0)
	%W    (S_GRDIR)

%OID_NEW(I_AUXUAL,AI_AUXUAL)
	%W    (UI_INT)
	%W    (0)
	%W    (S_AUX)

%OID_NEW(I_AUXVALV,AI_AUXVALV)
	%W    (UI_INT)
	%W    (0)
	%W    (S_AUXVALV)

%OID_NEW(I_STOP,AI_STOP)
	%W    (M_STOP)

%OID_NEW(I_START,AI_START)
	%W    (M_START)

%OID_NEW(I_PURGE,AI_PURGE)
	%W    (M_PURGE)

%OID_NEW(I_ERRCLR,AI_ERRCLR)
	%W    (CLRERR_U)

OID_1IN SET   OID_T

; -------------------------------------
; Servisni prijimane hodnoty

%OID_NEW(I_PBREG_D,AI_PBREG_D)
	%W    (UI_INT)
	%W    (REG_D)
	%W    (0)

%OID_NEW(I_PBREG_I,AI_PBREG_I)
	%W    (UI_INT)
	%W    (REG_I)
	%W    (0)

%OID_NEW(I_PBREG_P,AI_PBREG_P)
	%W    (UI_INT)
	%W    (REG_P)
	%W    (0)

OID_1INSL1 SET   OID_T

; -------------------------------------
; Vysilane hodnoty

OID_T	SET   0

%IF(%WITH_UL_G_BF) THEN (
%OID_NEW(I_GBFSLA,AI_GBFSLA)
	%W    (UO_INT)
	%W    (GBF_SLA)
	%W    (0)

%OID_NEW(I_GBFMODE,AI_GBFMODE)
	%W    (UO_INT)
	%W    (0)
	%W    (G_GBFMODE)
)FI

%OID_NEW(I_PRGBYTES,AI_PRGBYTES)
	%W    (UO_ARR)		; priprava prijmu pole
	%W    (0601h)		; priznaky
	%W    (PROGR_L)		; pocet polozek
	%W    (PROGRAM)		; pocatek
	%W    (0)		; funkce

%OID_NEW(I_PRGACTN,AI_PRGACTN)
	%W    (UO_INT)
	%W    (0)
	%W    (G_PROGN_U)

%OID_NEW(I_FLOW,AI_FLOW)
	%W    (UO_INT)
	%W    (0)
	%W    (G_INTR)
	%W    (FLOW)

%OID_NEW(I_PRESS,AI_PRESS)
	%W    (UO_INT)
	%W    (0)
	%W    (G_PRES_U)
	%W    (PRESS)

%OID_NEW(I_PRESS_H,AI_PRESS_H)
	%W    (UO_INT)
	%W    (0)
	%W    (G_PRES_U)
	%W    (PRESS_H)

%OID_NEW(I_PRESS_L,AI_PRESS_L)
	%W    (UO_INT)
	%W    (0)
	%W    (G_PRES_U)
	%W    (PRESS_L)

%IF(%PRESS_COR)THEN(
%OID_NEW(I_PRESCOR,AI_PRESCOR)
	%W    (UO_INT)
	%W    (M_COR)
	%W    (0)
)FI

%OID_NEW(I_AUXUAL,AI_AUXUAL)
	%W    (UO_INT)
	%W    (AUXUAL)
	%W    (0)

%OID_NEW(I_AUXVALV,AI_AUXVALV)
	%W    (UO_INT)
	%W    (0)
	%W    (G_AUXVALV)

%OID_NEW(I_GRAD_B,AI_GRAD_B)
	%W    (UO_INT)
	%W    (0)
	%W    (G_INTR)
	%W    (GRAD_B)

%OID_NEW(I_GRAD_C,AI_GRAD_C)
	%W    (UO_INT)
	%W    (0)
	%W    (G_INTR)
	%W    (GRAD_C)

%OID_NEW(I_STATUS,AI_STATUS)
	%W    (UO_INT)
	%W    (0)
	%W    (G_STATUS)

OID_1OUT SET  OID_T

; -------------------------------------
; Servisni vysilane hodnoty

%OID_NEW(I_PBREG_D,AI_PBREG_D)
	%W    (UO_INT)
	%W    (REG_D)
	%W    (0)

%OID_NEW(I_PBREG_I,AI_PBREG_I)
	%W    (UO_INT)
	%W    (REG_I)
	%W    (0)

%OID_NEW(I_PBREG_P,AI_PBREG_P)
	%W    (UO_INT)
	%W    (REG_P)
	%W    (0)

OID_1OUTSL1 SET   OID_T


;=================================================================
; Vyslani znacky zacatku analyzy

; Vysle znacku na adresu v R4
LAN_MRK:JNB   ES,L_MRKR
	;MOV   DPTR,#uL_FORM
	;MOVX  A,@DPTR
	;CJNE  A,#2,L_MRKR

	MOV   DPTR,#uL_SBP
	MOVX  A,@DPTR
	PUSH  ACC
	CLR   A
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	PUSH  ACC
	CLR   A
	MOVX  @DPTR,A

	;MOV   DPTR,#uL_GRP
	;MOVX  A,@DPTR
	;MOV   A,#2
	;MOV   R4,A
	MOV   R5,#4EH
	CLR   F0
	CALL  uL_S_OP
	MOV   DPTR,#L_MRKD
	MOV   R4,#4
	MOV   R5,#0
	CALL  uL_S_WR
	CALL  uL_S_CL

	JNB   F0,L_MRK1
	; informovat o chybe
	;SETB  uLE_ABS
L_MRK1:
	MOV   DPTR,#uL_SBP+1
	POP   ACC
	MOVX  @DPTR,A
	CALL  DECDPTR
	POP   ACC
	MOVX  @DPTR,A
L_MRKR:	MOV   A,#1
	RET

L_MRKD: DB    0,7,0,0

END
