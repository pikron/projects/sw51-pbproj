#   Project file pro cerpadlo LCP4000
#         (C) Pisoft 1992

lp_hw.obj : lp_hw.asm
	a51 lp_hw.asm $(par)

lp_gc.obj : lp_gc.asm
	a51 lp_gc.asm $(par)

lp.obj    : lp.asm
	a51 lp.asm  $(par)

lp_uf.obj : lp_uf.asm
	a51 lp_uf.asm $(par)

lp_lan.obj: lp_lan.asm
	a51 lp_lan.asm $(par)

	  : lp.obj
	del lp.

lp.       : lp.obj lp_uf.obj lp_hw.obj lp_gc.obj lp_lan.obj ..\pblib\pb.lib
	l51 lp.obj,lp_uf.obj,lp_hw.obj,lp_gc.obj,lp_lan.obj,..\pblib\pb.lib xdata(8000H) ramsize(100h) ixref

lp.hex    : lp.
	ohs51 lp
	copy lp.hex lp.he1
	ihexcsum -o lp.hex -l 0x8000 -L 0x7f00 -A 0x9 -F lp lp.hex
	del lp.
